﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : CCS관리                                               */
/* 프로그램ID   : frmCCSZ0001.cs                                        */
/* 프로그램명   : CCS 의뢰                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPCCS.UI
{
    public partial class frmCCSZ0001_S : Form, IToolbar
    {
        private const string Form_Name = "frmCCSZ0001_S";
        private const string Form_Version = "2016.03.21.01";

        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수

        private bool m_bolDebugMode = false;
        private string m_strDBConn = string.Empty;

        private int m_intRow = 0;
        private int m_intTabIndex = 0;
        private int m_intStart = 0;
        private string m_Pakcage = string.Empty;
        private List<string> m_strBOMCode = new List<string>(); // BOM자재정보
        private Infragistics.Win.UltraWinGrid.UltraGrid m_grdCCS;

        private string strFormName;

        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }

        public frmCCSZ0001_S()
        {
            InitializeComponent();
        }

        private void frmCCSZ0001_Activated(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser InitToolBar = new QRPBrowser();
                InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_SysRes.GetString("SYS_USERID"), this.Name);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmCCSZ0001_Load(object sender, EventArgs e)
        {
            try
            {
                //checkFormVersion
                if (!checkVersion())
                {
                    return;
                }

                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
                // Title 설정
                titleArea.mfSetLabelText("CCS 의뢰", m_SysRes.GetString("SYS_FONTNAME"), 12);

                InitEvent();
                SetToolAuth();

                // 컨트롤 초기화

                //SetRunMode();
                InitTab();
                InitLabel();
                InitGrid();
                InitComboBox();
                InitEtc();
                

                if (strFormName != null)
                    this.uGroupBoxContentsArea.Expanded = true;
                else
                    this.uGroupBoxContentsArea.Expanded = false;

                this.uDateSearchReqFromDate.MaskInput = "yyyy-mm-dd hh:mm:ss";
                this.uDateSearchReqToDate.MaskInput = "yyyy-mm-dd hh:mm:ss";

                this.uDateSearchReqFromDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 22:00:00");
                this.uDateSearchReqToDate.Value = DateTime.Now.ToString("yyyy-MM-dd 21:59:59");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 공통 이벤트

        // ContentsGroupBox 상태변화 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 180);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridCCSReqList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridCCSReqList.Height = 690;

                    for (int i = 0; i < uGridCCSReqList.Rows.Count; i++)
                    {
                        uGridCCSReqList.Rows[i].Fixed = false;
                    }
                    InitClear();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 디버깅 Method
        /// </summary>
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    //MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화

        /// </summary>
        private void InitEtc()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 문자입력 대문자로만 입력되게 하기
                this.uTextLotNo1.ImeMode = ImeMode.Disable;
                this.uTextLotNo1.CharacterCasing = CharacterCasing.Upper;

                this.uTextLotNo2.ImeMode = ImeMode.Disable;
                this.uTextLotNo2.CharacterCasing = CharacterCasing.Upper;

                this.uTextLotNo3.ImeMode = ImeMode.Disable;
                this.uTextLotNo3.CharacterCasing = CharacterCasing.Upper;

                this.uTextReqUserID1.ImeMode = ImeMode.Disable;
                this.uTextReqUserID1.CharacterCasing = CharacterCasing.Upper;

                this.uTextReqUserID2.ImeMode = ImeMode.Disable;
                this.uTextReqUserID2.CharacterCasing = CharacterCasing.Upper;

                this.uTextReqUserID3.ImeMode = ImeMode.Disable;
                this.uTextReqUserID3.CharacterCasing = CharacterCasing.Upper;

                this.uTextStepUserID1.ImeMode = ImeMode.Disable;
                this.uTextStepUserID1.CharacterCasing = CharacterCasing.Upper;

                this.uTextStepUserID2.ImeMode = ImeMode.Disable;
                this.uTextStepUserID2.CharacterCasing = CharacterCasing.Upper;

                this.uTextStepUserID3.ImeMode = ImeMode.Disable;
                this.uTextStepUserID3.CharacterCasing = CharacterCasing.Upper;

                //if (m_resSys.GetString("SYS_DEPTCODE").Equals("S5100"))
                //{
                //    this.uButtonCancelReq1.Visible = true;
                //    this.uButtonCancelReq2.Visible = true;
                //    this.uButtonCancelReq3.Visible = true;
                //}
                //else
                //{
                //    this.uButtonCancelReq1.Visible = false;
                //    this.uButtonCancelReq2.Visible = false;
                //    this.uButtonCancelReq3.Visible = false;
                //}

                //this.uButtonCancelReq1.Visible = true;
                //this.uButtonCancelReq2.Visible = true;
                //this.uButtonCancelReq3.Visible = true;
                this.uButtonCancelReq1.Visible = false;
                this.uButtonCancelReq2.Visible = false;
                this.uButtonCancelReq3.Visible = false;

                this.uTextEtcDesc1.MaxLength = 200;
                this.uTextEtcDesc2.MaxLength = 200;
                this.uTextEtcDesc3.MaxLength = 200;

                this.uTextCorrectAction1.MaxLength = 200;
                this.uTextCorrectAction2.MaxLength = 200;
                this.uTextCorrectAction3.MaxLength = 200;

                this.uTextCauseReason1.MaxLength = 200;
                this.uTextCauseReason2.MaxLength = 200;
                this.uTextCauseReason3.MaxLength = 200;

                // LockingFlag Hidden 처리
                this.uTextMESLockingFlag1.Visible = false;
                this.uTextMESLockingFlag2.Visible = false;
                this.uTextMESLockingFlag3.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Event
        /// </summary>
        private void InitEvent()
        {
            this.uCheckCHASE1_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);
            this.uCheckCHASE2_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);
            this.uCheckCHASE3_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);


            this.uCheckCHASE1_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
        }

        /// <summary>
        /// Tab 초기화

        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabCCS, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab1, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab2, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab3, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();
                // 조회 Label
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchComplete, "완료CCS포함", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectResult, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 헤더 Label
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCCSNo, "CCSNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkProcess, "작업공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNowProcess, "현재공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProduct, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 1차 Label
                wLabel.mfSetLabel(this.uLabelReqType1, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotNo1, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqUser1, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelStepUser1, "조치자명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqDate1, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason1, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction1, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack1, "차수선택", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE1, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc1, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource1, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                // 상세 2차 Label
                wLabel.mfSetLabel(this.uLabelReqType2, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotNo2, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqUser2, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelStepUser2, "조치자명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqDate2, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason2, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction2, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack2, "차수선택", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE2, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc2, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource2, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 3차 Label
                wLabel.mfSetLabel(this.uLabelReqType3, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotNo3, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqUser3, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelStepUser3, "조치자명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqDate3, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason3, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction3, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack3, "차수선택", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE3, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc3, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource3, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                // ComboBox 설정 메소드


                // SearchArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // ContentsArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // CCS의뢰사유1 연결                
                DataTable dtCCSReaType = new DataTable();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
                //QRPMAS.BL.MASQUA.CCSReqType clsCCSReaType = new QRPMAS.BL.MASQUA.CCSReqType();
                //brwChannel.mfCredentials(clsCCSReaType);

                //dtCCSReaType = clsCCSReaType.mfReadMASReqTypeCombo(this.uComboPlant.Value.ToString(), "", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboCCSReqType1, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReaType);

                wCombo.mfSetComboEditor(this.uComboCCSReqType2, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReaType);
                this.uComboCCSReqType2.ReadOnly = true;

                wCombo.mfSetComboEditor(this.uComboCCSReqType3, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReaType);
                this.uComboCCSReqType3.ReadOnly = true;

                //// 완료여부
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(clsCom);

                //DataTable dtComplete = clsCom.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));

                //wCombo.mfSetComboEditor(this.uComboSearchComplete, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "F", "", "전체"
                //    , "ComCode", "ComCodeName", dtComplete);

                //// 검사결과
                //DataTable dtInspectResult = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                //wCombo.mfSetComboEditor(this.uComboSearchInspectResult, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                //    , "ComCode", "ComCodeName", dtInspectResult);

                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerCombo = new DataTable();
                if (dtCustomer.Rows.Count > 0)
                    dtCustomerCombo = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "전체", "CustomerCode", "CustomerName", dtCustomerCombo);

                // 설비콤보 임시 설정
                DataTable dtEquip = new DataTable();
                wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "선택", "EquipCode", "EquipName", dtEquip);
                this.uComboEquip.Appearance.BackColor = Color.Gainsboro;
                
                //공정타입
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text.Trim();

                this.uComboSearchProcessGroup.Items.Clear();


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtProcessGroup = clsEquip.mfReadEquipArea_WithDetailProcessOperationType(strPlantCode, strEquipCode, strAreaCode);

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComboCode", "ComboName", dtProcessGroup);

                this.uComboEquip.ReadOnly = true;
                this.uComboEquip.Appearance.BackColor = Color.Gainsboro;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회그리드

                // 검색 리스트 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSReqList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqNo", "CCSNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqState", "상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "NowProcessCode", "현재공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "NowProcessName", "현재공정명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqUserID", "의뢰자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqDate", "의뢰일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReceiptDate", "접수일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CompleteDate", "완료일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqTime", "의뢰시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CCSReqTypeName", "의뢰사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CCSCreateType", "CreateType", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "MESTFlag", "MES요청여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqLotSeq", "의뢰Lot차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "StepUserID", "조치자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "StepUserName", "조치자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridCCSReqList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSReqList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region 1차
                // 1차 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSReq1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq1, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건

                wGrid.mfInitGeneralGrid(this.uGridPara1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "QualityValue", "품질적용값", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara1, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 2차
                // 2차 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSReq2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건
                wGrid.mfInitGeneralGrid(this.uGridPara2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "QualityValue", "품질적용값", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara2, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 3차

                // 3차 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSReq3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridCCSReq3, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건
                wGrid.mfInitGeneralGrid(this.uGridPara3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "QualityValue", "품질적용값", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPara3, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 1,2,3차 품질ItemGrid설정
                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid1_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectIngFlag", "검사여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid2_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectIngFlag", "검사여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid3_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectIngFlag", "검사여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                // Set Font
                this.uGridCCSReq1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSReq1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridCCSReq2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSReq2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridCCSReq3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSReq3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPara1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPara1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPara2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPara2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPara3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPara3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid1_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // Grid DropDown Column 설정
                DataTable dtUnit = new DataTable();
                // 단위
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);
                dtUnit = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGridCCSReq1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq3, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUnit);

                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                wGrid.mfSetGridColumnValueList(this.uGridCCSReq1, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq3, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                // DataRange
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                // 검사결과

                DataTable dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                //wGrid.mfSetGridColumnValueList(this.uGridCCSReqList, 0, "InspectResult", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq1, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq3, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                wGrid.mfSetGridColumnValueList(this.uGridPara1, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridPara2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridPara3, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                this.uGrid1_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGrid2_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGrid3_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // DataError 설정
                this.uGridCCSReq1.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridCCSReq1.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridCCSReq1.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGridCCSReq2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridCCSReq2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridCCSReq2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGridCCSReq3.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridCCSReq3.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridCCSReq3.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid1_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid1_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid1_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid2_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid2_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid2_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid3_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid3_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid3_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                //Key Mapping Clear
                this.uGridCCSReq1.KeyActionMappings.Clear();
                this.uGridCCSReq2.KeyActionMappings.Clear();
                this.uGridCCSReq3.KeyActionMappings.Clear();
                this.uGridPara1.KeyActionMappings.Clear();
                this.uGridPara2.KeyActionMappings.Clear();
                this.uGridPara3.KeyActionMappings.Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 화면 초기화

        /// </summary>
        private void InitClear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strLogInPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                String strLogInUserID = m_resSys.GetString("SYS_USERID");
                String strLogInUserName = m_resSys.GetString("SYS_USERNAME");
                //CreateType,MESTFlag 텍스트
                this.uTextCreateType.Text = "";
                this.uTextMESTFlag.Text = "";
                this.uTextComplete1.Clear();
                this.uTextComplete2.Clear();
                this.uTextComplete3.Clear();
                this.uTextCompleteFlag.Clear();
                this.uTextInspectResult1.Clear();
                this.uTextInspectResult2.Clear();
                this.uTextInspectResult3.Clear();

                //헤더
                this.uComboPlant.Value = strLogInPlantCode;
                this.uTextReqNo.Text = "";
                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextWorkProcessCode.Text = "";
                this.uTextWorkProcessName.Text = "";
                this.uTextNowProcessCode.Clear();
                this.uTextNowProcessName.Clear();
                this.uComboEquip.Value = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextProductCode.Text = "";
                this.uTextProductName.Text = "";
                this.uTextCustomerProductCode.Text = "";
                this.uTextPackage.Text = "";
                this.uTabCCS.Tabs[0].Selected = true;

                DataTable dtEquip = new DataTable();
                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "EquipCode", "EquipName", dtEquip);
                //this.uComboEquip.Enabled = true;
                this.uComboEquip.ReadOnly = true;
                this.uComboEquip.Appearance.BackColor = Color.Gainsboro;

                //1차 탭
                this.uComboCCSReqType1.Value = "";
                this.uTextReqLotSeq1.Text = "";
                this.uTextLotNo1.Text = "";
                this.uTextLotNo1.ReadOnly = false;
                this.uTextReqUserID1.Text = strLogInUserID;
                this.uTextReqUserName1.Text = strLogInUserName;
                this.uTextStepUserID1.Text = strLogInUserID;
                this.uTextStepUserName1.Text = strLogInUserName;
                this.uDateReqDate1.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextReqTime1.Clear();
                this.uTextCauseReason1.Text = "";
                this.uTextCorrectAction1.Text = "";
                this.uTextCauseReason1.Appearance.BackColor = Color.Gainsboro;
                this.uTextCauseReason1.ReadOnly = true;
                this.uTextCorrectAction1.Appearance.BackColor = Color.Gainsboro;
                this.uTextCorrectAction1.ReadOnly = true;
                while (this.uGridCCSReq1.Rows.Count > 0)
                {
                    this.uGridCCSReq1.Rows[0].Delete(false);
                }
                while (this.uGridPara1.Rows.Count > 0)
                {
                    this.uGridPara1.Rows[0].Delete(false);
                }
                while (this.uGrid1_2.Rows.Count > 0)
                {
                    this.uGrid1_2.Rows[0].Delete(false);
                }

                this.uGridCCSReq1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridPara1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uTextReqUserID1.Enabled = true;
                this.uTextStepUserID1.Enabled = true;
                this.uComboStackSeq1.Enabled = true;

                this.uCheckCHASE1_1.Checked = false;
                this.uCheckCHASE1_2.Checked = false;
                this.uCheckCHASE1_3.Checked = false;
                this.uCheckCHASE1_4.Checked = false;
                this.uCheckCHASE1_5.Checked = false;
                this.uCheckCHASE1_6.Checked = false;
                this.uCheckCHASE1_All.Checked = false;

                this.uLabelCHASE1.Visible = false;
                this.uCheckCHASE1_1.Visible = false;
                this.uCheckCHASE1_2.Visible = false;
                this.uCheckCHASE1_3.Visible = false;
                this.uCheckCHASE1_4.Visible = false;
                this.uCheckCHASE1_5.Visible = false;
                this.uCheckCHASE1_6.Visible = false;
                this.uCheckCHASE1_All.Visible = false;

                this.uLabelCHASE1.Enabled = true;
                this.uCheckCHASE1_1.Enabled = true;
                this.uCheckCHASE1_2.Enabled = true;
                this.uCheckCHASE1_3.Enabled = true;
                this.uCheckCHASE1_4.Enabled = true;
                this.uCheckCHASE1_5.Enabled = true;
                this.uCheckCHASE1_6.Enabled = true;
                this.uCheckCHASE1_All.Enabled = true;

                this.uDateReceiptDate1.Value = null;
                this.uTextReceiptTime1.Text = string.Empty;

                this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                this.uTextEtcDesc1.Clear();

                //this.uButtonCancelReq1.Visible = false;

                //2차 탭
                this.uComboCCSReqType2.Value = "";
                this.uTextReqLotSeq2.Text = "";
                this.uTextLotNo2.Text = "";
                this.uTextLotNo2.ReadOnly = false;
                this.uTextReqUserID2.Text = strLogInUserID;
                this.uTextReqUserName2.Text = strLogInUserName;
                this.uTextStepUserID2.Text = strLogInUserID;
                this.uTextStepUserName2.Text = strLogInUserName;
                this.uDateReqDate2.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextReqTime2.Clear();
                this.uTextCauseReason2.Text = "";
                this.uTextCorrectAction2.Text = "";
                this.uTextCauseReason2.Appearance.BackColor = Color.Gainsboro;
                this.uTextCauseReason2.ReadOnly = true;
                this.uTextCorrectAction2.Appearance.BackColor = Color.Gainsboro;
                this.uTextCorrectAction2.ReadOnly = true;
                while (this.uGridCCSReq2.Rows.Count > 0)
                {
                    this.uGridCCSReq2.Rows[0].Delete(false);
                }
                while (this.uGridPara2.Rows.Count > 0)
                {
                    this.uGridPara2.Rows[0].Delete(false);
                }
                while (this.uGrid2_2.Rows.Count > 0)
                {
                    this.uGrid2_2.Rows[0].Delete(false);
                }

                this.uGridCCSReq2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridPara2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uTextReqUserID2.Enabled = true;
                this.uTextStepUserID2.Enabled = true;
                this.uComboStackSeq2.Enabled = true;

                this.uCheckCHASE2_1.Checked = false;
                this.uCheckCHASE2_2.Checked = false;
                this.uCheckCHASE2_3.Checked = false;
                this.uCheckCHASE2_4.Checked = false;
                this.uCheckCHASE2_5.Checked = false;
                this.uCheckCHASE2_6.Checked = false;
                this.uCheckCHASE2_All.Checked = false;

                this.uLabelCHASE2.Visible = false;
                this.uCheckCHASE2_1.Visible = false;
                this.uCheckCHASE2_2.Visible = false;
                this.uCheckCHASE2_3.Visible = false;
                this.uCheckCHASE2_4.Visible = false;
                this.uCheckCHASE2_5.Visible = false;
                this.uCheckCHASE2_6.Visible = false;
                this.uCheckCHASE2_All.Visible = false;

                this.uLabelCHASE2.Enabled = true;
                this.uCheckCHASE2_1.Enabled = true;
                this.uCheckCHASE2_2.Enabled = true;
                this.uCheckCHASE2_3.Enabled = true;
                this.uCheckCHASE2_4.Enabled = true;
                this.uCheckCHASE2_5.Enabled = true;
                this.uCheckCHASE2_6.Enabled = true;
                this.uCheckCHASE2_All.Enabled = true;

                this.uDateReceiptDate2.Value = null;
                this.uTextReceiptTime2.Text = string.Empty;

                this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                this.uTextEtcDesc2.Clear();

                //3차 탭
                this.uComboCCSReqType3.Value = "";
                this.uTextReqLotSeq3.Text = "";
                this.uTextLotNo3.Text = "";
                this.uTextLotNo3.ReadOnly = false;
                this.uTextReqUserID3.Text = strLogInUserID;
                this.uTextReqUserName3.Text = strLogInUserName;
                this.uTextStepUserID3.Text = strLogInUserID;
                this.uTextStepUserName3.Text = strLogInUserName;
                this.uDateReqDate3.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextReqTime3.Clear();
                this.uTextCauseReason3.Text = "";
                this.uTextCorrectAction3.Text = "";
                this.uTextCauseReason3.Appearance.BackColor = Color.Gainsboro;
                this.uTextCauseReason3.ReadOnly = true;
                this.uTextCorrectAction3.Appearance.BackColor = Color.Gainsboro;
                this.uTextCorrectAction3.ReadOnly = true;
                while (this.uGridCCSReq3.Rows.Count > 0)
                {
                    this.uGridCCSReq3.Rows[0].Delete(false);
                }
                while (this.uGridPara3.Rows.Count > 0)
                {
                    this.uGridPara3.Rows[0].Delete(false);
                }
                while (this.uGrid3_2.Rows.Count > 0)
                {
                    this.uGrid3_2.Rows[0].Delete(false);
                }
                this.uGridCCSReq3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridPara3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uTextReqUserID3.Enabled = true;
                this.uTextStepUserID3.Enabled = true;
                this.uComboStackSeq3.Enabled = true;

                this.uCheckCHASE3_1.Checked = false;
                this.uCheckCHASE3_2.Checked = false;
                this.uCheckCHASE3_3.Checked = false;
                this.uCheckCHASE3_4.Checked = false;
                this.uCheckCHASE3_5.Checked = false;
                this.uCheckCHASE3_6.Checked = false;
                this.uCheckCHASE3_All.Checked = false;

                this.uLabelCHASE3.Visible = false;
                this.uCheckCHASE3_1.Visible = false;
                this.uCheckCHASE3_2.Visible = false;
                this.uCheckCHASE3_3.Visible = false;
                this.uCheckCHASE3_4.Visible = false;
                this.uCheckCHASE3_5.Visible = false;
                this.uCheckCHASE3_6.Visible = false;
                this.uCheckCHASE3_All.Visible = false;

                this.uLabelCHASE3.Enabled = true;
                this.uCheckCHASE3_1.Enabled = true;
                this.uCheckCHASE3_2.Enabled = true;
                this.uCheckCHASE3_3.Enabled = true;
                this.uCheckCHASE3_4.Enabled = true;
                this.uCheckCHASE3_5.Enabled = true;
                this.uCheckCHASE3_6.Enabled = true;
                this.uCheckCHASE3_All.Enabled = true;

                this.uDateReceiptDate3.Value = null;
                this.uTextReceiptTime3.Text = string.Empty;

                this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                this.uTextEtcDesc3.Clear();

                this.uTabCCS.Tabs[1].Enabled = false;
                this.uTabCCS.Tabs[2].Enabled = false;

                this.uTab1.Tabs["Quality"].Visible = false;
                this.uTab2.Tabs["Quality"].Visible = false;
                this.uTab3.Tabs["Quality"].Visible = false;

                this.uTextLotNo1.Enabled = true;
                this.uComboCCSReqType1.Enabled = true;
                this.uTextLotNo2.Enabled = true;
                this.uComboCCSReqType2.Enabled = true;
                this.uTextLotNo3.Enabled = true;
                this.uComboCCSReqType3.Enabled = true;

                // PSTS 추가분
                this.uTextMESLockingFlag1.Text = "F";
                this.uTextMESLockingFlag2.Text = "F";
                this.uTextMESLockingFlag3.Text = "F";
                if(m_strBOMCode != null)
                        m_strBOMCode.Clear();   // BOM 자재코드 초기화
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar Method
        // 검색

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 생성
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLotNo = this.uTextSearchLotNo.Text;
                String strEquipCode = this.uTextSearchEquipCode.Text;
                String strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();
                String strReqFromDate = Convert.ToDateTime(this.uDateSearchReqFromDate.Value).ToString("yyyy-MM-dd HH:mm:ss");
                String strReqToDate = Convert.ToDateTime(this.uDateSearchReqToDate.Value).ToString("yyyy-MM-dd HH:mm:ss");
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strCompleteFlag = this.uCheckSearchCompleteFlag.CheckedValue.ToString().ToUpper().Substring(0, 1);
                //string strInspectResultFlag = this.uComboSearchInspectResult.Value.ToString();  --> 저장시 공통함수에서 콤보입력값 체크시 오류가 발생하여 지웠음
                string strInspectResultFlag = string.Empty;
                string strAreaCode = this.uComboSearchArea.Value.ToString();

                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;

                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색 Method 호출
                DataTable dtSearch = clsHeader.mfReadCCSInspectReqH_Req(strPlantCode, strLotNo, strEquipCode, strProcessGroup, strReqFromDate, strReqToDate, strPackage, strCustomerCode
                                                                    , strCompleteFlag, strInspectResultFlag, m_resSys.GetString("SYS_LANG"), this.Name, strAreaCode);

                //완료를 체크한 경우 완료인 의뢰건만 보이게 한다.
                DataRow[] dr;
                DataTable dt = dtSearch.Clone();
                if (this.uCheckSearchCompleteFlag.Checked == true)
                {
                    dt = dtSearch;
                }
                else
                {
                    dr = dtSearch.Select("ReqState <> '완료'");
                    if (dr.Length > 0)
                    {
                        DataTable dtr = dr.CopyToDataTable();

                        DataView dv = new DataView(dtr);
                        //dv.Sort = "ReqDate DESC";
                        dv.Sort = "ReqNo DESC";
                        dt = dv.ToTable();
                    }
                }

                this.uGridCCSReqList.DataSource = dt; //dtSearch;
                this.uGridCCSReqList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 정보가 있을 시 MESTFlag가 F,CSSCreateType이 M 인 경우 줄의 색을 변경하라 //
                for (int i = 0; i < this.uGridCCSReqList.Rows.Count; i++)
                {
                    //if (this.uGridCCSReqList.Rows[i].Cells["MESTFlag"].Value.ToString() == "F" && this.uGridCCSReqList.Rows[i].Cells["CCSCreateType"].Value.ToString() == "M")
                    if (this.uGridCCSReqList.Rows[i].Cells["InspectResult"].Value.ToString().Equals("NG") && this.uGridCCSReqList.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("T"))
                    {
                        this.uGridCCSReqList.Rows[i].Appearance.BackColor = Color.Salmon;
                    }
                }
                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridCCSReqList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로

                this.uGroupBoxContentsArea.Expanded = false;

                //CreateTypeText,MESTFlag 초기화

                this.uTextCreateType.Text = "";
                this.uTextMESTFlag.Text = "";
                this.uTextStdNumber.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 저장

        public void mfSave()
        {
            try
            {
                //checkFormVersion
                if (!checkVersion())
                {
                    return;
                }

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                bool bolCheck = false;
                bool bolCheckPara = false;
                string strLang = m_resSys.GetString("SYS_LANG");

                if (this.uTabCCS.Tabs[2].Enabled == true && this.uTextLotNo3.Text != string.Empty)
                    this.uTabCCS.Tabs[2].Selected = true;
                else if (this.uTabCCS.Tabs[1].Enabled == true && this.uTextLotNo2.Text != string.Empty)
                    this.uTabCCS.Tabs[1].Selected = true;
                else
                    this.uTabCCS.Tabs[0].Selected = true;

                int intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString());

                #region 입력사항 확인
                // 헤더 필수사항 확인
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M001042", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (this.uTextCompleteFlag.Text.Equals("T") && this.uTextInspectResultFlag.Text.Equals("OK"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000980", "M000824", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (this.uComboPlant.Value== null || this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000266", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }
                if (this.uComboEquip.Value == null
                    || this.uComboEquip.Value.ToString().Equals(string.Empty)
                    || this.uComboEquip.Value.ToString().Trim().Equals("-")) //MES "-" : 설비번호없음
                    //|| this.uComboEquip.Items.get.Count == 0) //설비코드
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M001503", Infragistics.Win.HAlign.Right);
                    // Focus
                    //this.uComboEquip.DropDown();
                    return;
                }
                if (intTabIndex == 0)
                {
                    if (this.uTextLotNo1.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000075", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextLotNo1.Focus();
                        return;
                    }
                    if (this.uComboCCSReqType1.Value == null || this.uComboCCSReqType1.Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000037", Infragistics.Win.HAlign.Right);
                        // Focus
                        this.uComboCCSReqType1.DropDown();
                        return;
                    }
                    if (this.uGridCCSReq1.Rows.Count <= 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000190", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextComplete1.Text.Equals("T") && this.uTextInspectResult1.Text.Equals("OK"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000020", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    if (this.uTextNowProcessName.Text.Contains("W/B") && (this.uComboProgramSource1.Value == null || this.uComboProgramSource1.Value.ToString().Equals(string.Empty)))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M001578", Infragistics.Win.HAlign.Right);
                        this.uComboProgramSource1.DropDown();
                        return;
                    }

                    int intIngCount = 0;
                    this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    
                    // 불량유형 콤보 정보 검색 
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFT);
                    DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                    clsFT.Dispose();

                    //查询设备所属工程类型
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    DataTable dtEquipProcessGroup = clsEquip.mfReadEquipProcessGroup(this.uComboPlant.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    clsEquip.Dispose();
                    string strProcessGroupCode = dtEquipProcessGroup.Rows[0]["ProcessGroupCode"].ToString();

                    for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCCSReq1.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGridCCSReq1.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            if (this.uGridCCSReq1.Rows[i].Cells["InspectFaultTypeCode"].Value == null || 
                                this.uGridCCSReq1.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty) ||
                                dtFaultType.Select("InspectFaultTypeCode ='" + this.uGridCCSReq1.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString() + "'").Count() == 0) // 선택한 불량 유형정보가 BD정보와 틀릴경우 메세지
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000606",strLang)
                                                    , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridCCSReq1.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
                    {
                        if (this.uGridPara1.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }

                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGridPara1.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara1.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGridPara1.Rows[i].Hidden==false && (this.uGridPara1.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara1.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }

                    }
                }
                else if (intTabIndex == 1)
                {
                    if (this.uTextLotNo2.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000078", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextLotNo2.Focus();
                        return;
                    }
                    if (this.uComboCCSReqType2.Value == null || this.uComboCCSReqType2.Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000037", Infragistics.Win.HAlign.Right);
                        // Focus
                        this.uComboCCSReqType2.DropDown();
                        return;
                    }
                    if (this.uGridCCSReq2.Rows.Count <= 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000190", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextComplete2.Text.Equals("T") && this.uTextInspectResult2.Text.Equals("OK"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000022", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextCauseReason1.Text.Equals(string.Empty) || this.uTextCorrectAction1.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001225", "M001422", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    if (this.uTextNowProcessName.Text.Contains("W/B") && (this.uComboProgramSource2.Value == null || this.uComboProgramSource2.Value.ToString().Equals(string.Empty)))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M001578", Infragistics.Win.HAlign.Right);
                        this.uComboProgramSource2.DropDown();
                        return;
                    }

                    // 불량유형 콤보 정보 검색 
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFT);
                    DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                    clsFT.Dispose();

                    //查询设备所属工程类型
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    DataTable dtEquipProcessGroup = clsEquip.mfReadEquipProcessGroup(this.uComboPlant.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    clsEquip.Dispose();
                    string strProcessGroupCode = dtEquipProcessGroup.Rows[0]["ProcessGroupCode"].ToString();

                    int intIngCount = 0;
                    this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    for (int i = 0; i < this.uGridCCSReq2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCCSReq2.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGridCCSReq2.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            if (this.uGridCCSReq2.Rows[i].Cells["InspectFaultTypeCode"].Value == null 
                                || this.uGridCCSReq2.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty)
                                || dtFaultType.Select("InspectFaultTypeCode ='" + this.uGridCCSReq2.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString() + "'").Count() == 0) // 선택한 불량 유형정보가 BD정보와 틀릴경우 메세지
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000606",strLang)
                                                    , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridCCSReq2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }


                    if (this.uTextCauseReason1.Text.Equals(string.Empty) & this.uTextCorrectAction1.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001021", "M001303", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
                    {
                        if (this.uGridPara2.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            //규격을 벗어난 검사값이 있습니다. 저장하시겠습니까?
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }

                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGridPara2.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara2.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGridPara2.Rows[i].Hidden == false && (this.uGridPara2.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara2.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }

                }
                else if (intTabIndex == 2)
                {
                    if (this.uTextLotNo3.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000078", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextLotNo3.Focus();
                        return;
                    }
                    if (this.uComboCCSReqType3.Value==null || this.uComboCCSReqType3.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000037", Infragistics.Win.HAlign.Right);
                        // Focus
                        this.uComboCCSReqType3.DropDown();
                        return;
                    }
                    if (this.uGridCCSReq3.Rows.Count <= 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000190", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextComplete3.Text.Equals("T") && this.uTextInspectResult3.Text.Equals("OK"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000025", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextCauseReason2.Text.Equals(string.Empty) || this.uTextCorrectAction2.Text.Equals(string.Empty))
                    {
                        //2차 원인 / 대책사항이 없으면 3차 의뢰를 할 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001423", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    if (this.uTextNowProcessName.Text.Contains("W/B") && (this.uComboProgramSource3.Value == null || this.uComboProgramSource3.Value.ToString().Equals(string.Empty)))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M001578", Infragistics.Win.HAlign.Right);
                        this.uComboProgramSource3.DropDown();
                        return;
                    }

                    // 불량유형 콤보 정보 검색 
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFT);
                    DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                    clsFT.Dispose();

                    //查询设备所属工程类型
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    DataTable dtEquipProcessGroup = clsEquip.mfReadEquipProcessGroup(this.uComboPlant.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    clsEquip.Dispose();
                    string strProcessGroupCode = dtEquipProcessGroup.Rows[0]["ProcessGroupCode"].ToString();

                    int intIngCount = 0;
                    this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    for (int i = 0; i < this.uGridCCSReq3.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridCCSReq3.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGridCCSReq3.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            if (this.uGridCCSReq3.Rows[i].Cells["InspectFaultTypeCode"].Value == null 
                                || this.uGridCCSReq3.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty)
                                || dtFaultType.Select("InspectFaultTypeCode ='" + this.uGridCCSReq3.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString() + "'").Count() == 0) // 선택한 불량 유형정보가 BD정보와 틀릴경우 메세지
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000606",strLang)
                                                    , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridCCSReq3.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    if (this.uTextCauseReason1.Text == "" & this.uTextCorrectAction1.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001021", "M001304", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    for (int i = 0; i < this.uGridPara3.Rows.Count; i++)
                    {
                        if (this.uGridPara3.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            //규격을 벗어난 검사값이 있습니다. 저장하시겠습니까?
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }

                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGridPara3.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGridPara3.Rows[i].Hidden == false && (this.uGridPara3.Rows[i].Cells["InspectValue"].Value == null || this.uGridPara3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                }
 
                #endregion

               
                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                ////// 저장여부 확인 메세지
                ////if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                ////{
                string strLotNo = ""; //LotNo저장
                bool bolLotText= true; // LotNo Text 활성화상태
                // ActiveCell 첫번째 행 첫번째 열로 이동                                        
                if (intTabIndex == 0)
                {
                    this.uGridCCSReq1.ActiveCell = this.uGridCCSReq1.Rows[0].Cells[0];
                    m_intStart = this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["1"].Index;
                    strLotNo = this.uTextLotNo1.Text.Trim();
                    bolLotText = this.uTextLotNo1.Enabled;
                }
                else if (intTabIndex == 1)
                {
                    this.uGridCCSReq2.ActiveCell = this.uGridCCSReq2.Rows[0].Cells[0];
                    m_intStart = this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["1"].Index;
                    strLotNo = this.uTextLotNo2.Text.Trim();
                    bolLotText = this.uTextLotNo2.Enabled;
                }
                else if (intTabIndex == 2)
                {
                    this.uGridCCSReq3.ActiveCell = this.uGridCCSReq3.Rows[0].Cells[0];
                    m_intStart = this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["1"].Index;
                    strLotNo = this.uTextLotNo3.Text.Trim();
                    bolLotText = this.uTextLotNo3.Enabled;
                }

                //CCS등록 중복체크
                if (bolLotText)
                {
                    bool bolChk = RtnReqHCheck(this.uComboPlant.Value.ToString(), this.uComboEquip.Value.ToString().Trim(), this.uTextNowProcessCode.Text, strLotNo, (intTabIndex + 1).ToString());

                    //정보가 등록되어 있다면 Return
                    if (!bolChk)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "M001264", "M000877", "M001517", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

                // 저장할 DataTable 설정 Method 호출
                DataTable dtHeader = SaveHeaderInfo();
                DataTable dtLot = SaveLotInfo();
                DataTable dtItem = SaveItemInfo();
                DataTable dtMeasure = SaveResultMeasure(m_intStart);
                DataTable dtCount = SaveResultCount(m_intStart);
                DataTable dtOkNo = SaveResultOkNg(m_intStart);
                DataTable dtDesc = SaveResultDesc(m_intStart);
                DataTable dtSelect = SaveResultSelect(m_intStart);
                DataTable dtPara = SaveParaInfo();

                String strErrRtn = "";
                String strLotErrRtn = "";

                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;

                if (m_bolDebugMode == false)
                {
                    // BL연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);

                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                if (intTabIndex != 0)
                {
                    string strPlantCode = dtLot.Rows[0]["PlantCode"].ToString();
                    string strReqNo = dtLot.Rows[0]["ReqNo"].ToString();
                    string strReqSeq = dtLot.Rows[0]["ReqSeq"].ToString();
                    //int intReqLotSeq = intTabIndex - 1;
                    string strReqLotSeq = (intTabIndex).ToString();
                    string strCauseReason = string.Empty;
                    string strCorrectAction = string.Empty;
                    if (intTabIndex == 1)
                    {
                        strCauseReason = this.uTextCauseReason1.Text;
                        strCorrectAction = this.uTextCorrectAction1.Text;
                    }
                    else if (intTabIndex == 2)
                    {
                        strCauseReason = this.uTextCauseReason2.Text;
                        strCorrectAction = this.uTextCorrectAction2.Text;
                    }

                    strLotErrRtn = clsLot.mfSaveINSCCSInspectReqLotDesc(strPlantCode, strReqNo, strReqSeq, strReqLotSeq, strCauseReason, strCorrectAction, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                }

                // Method 호출
                strErrRtn = clsHeader.mfSaveCCSInsepctReqH_PSTS(dtHeader, dtLot, dtItem, dtMeasure, dtCount, dtOkNo, dtDesc, dtSelect, dtPara
                                    , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 저장 성공이면 MES I/F 진행
                if (ErrRtn.ErrNum == 0)
                {
                    string strRtnReqNo = ErrRtn.mfGetReturnValue(0);
                    string strRtnReqSeq = ErrRtn.mfGetReturnValue(1);

                    this.uTextReqNo.Text = strRtnReqNo + strRtnReqSeq;

                    DataTable dtEqpDown = SaveEQPDowninfo();
                    if (dtEqpDown.Rows.Count > 0)
                    {
                        // MES I/F 메소드 호출
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                        QRPCCS.BL.INSCCS.CCSMESInterface clsMES = new QRPCCS.BL.INSCCS.CCSMESInterface();
                        brwChannel.mfCredentials(clsMES);
                        
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAcce);
                        DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), clsMES.MesCode);

                        DataTable dtMESResult = clsMES.mfSend_EQP_DOWN4QC(dtEqpDown, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtSysAcce);

                        if (dtMESResult.Rows.Count > 0)
                        {
                            int intReqLotSeq = m_intTabIndex + 1;
                            if (dtMESResult.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                            {
                                // MES Locking 성공Flag Update 메소드 호출
                                strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(this.uComboPlant.Value.ToString()
                                                                                        , strRtnReqNo, strRtnReqSeq, intReqLotSeq, "LOCKING"
                                                                                        , "T", m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum == 0)
                                {
                                    // 모든 정보 성공
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                                    mfSearch();
                                }
                            }
                            else
                            {
                                // I/F 실패시 관리번호 초기화 1차 의뢰인경우
                                if (intReqLotSeq.Equals(1))
                                    this.uTextReqNo.Clear();                                

                                // I/F 실패시 CCS 의뢰정보 삭제
                                strErrRtn = clsHeader.mfDeleteCCSInspectReq_PSTS(this.uComboPlant.Value.ToString(), strRtnReqNo, strRtnReqSeq, intReqLotSeq);

                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum == 0)
                                {
                                    if (dtMESResult.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M001474",
                                                Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                                , dtMESResult.Rows[0]["returnmessage"].ToString()
                                                , Infragistics.Win.HAlign.Right);
                                    }

                                    mfSearch();
                                }
                                else
                                {
                                    if (dtMESResult.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001475", m_resSys.GetString("SYS_LANG")) + "<br/> KEY : " + this.uTextReqNo.Text
                                                , Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                                , dtMESResult.Rows[0]["returnmessage"].ToString()
                                                , Infragistics.Win.HAlign.Right);
                                    }
                                    mfSearch();
                                }
                            }
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M000057", "M000096",
                                               Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //checkVersion
        private bool checkVersion()
        {
            bool bolLatestVersion = true;
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                //QRPSYS.BL.SYSPGM.Menu clsmenu = new QRPSYS.BL.SYSPGM.Menu();
                //brwChannel.mfCredentials(clsmenu);
                //DataTable dtMenuVersion = clsmenu.mfReadFormVersion(Form_Name, Form_Version);
                //clsmenu.Dispose();

                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(clsCom);

                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtMenuVersion = clsHeader.mfReadFormVersion(Form_Name, Form_Version);
                //clsCom.Dispose();

                string strVersion = "";
                if (dtMenuVersion != null)
                {
                    strVersion = dtMenuVersion.Rows[0]["FORMVERSION"].ToString();
                    if (strVersion != Form_Version)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000794", "M001347", "M001582", Infragistics.Win.HAlign.Right);

                        this.Enabled = false;
                        bolLatestVersion = false;
                    }
                }
                return bolLatestVersion;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return bolLatestVersion;
            }
            finally
            {
            }
        }

        // 자동의뢰 임시 버튼 (MES I/F)
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                String strProductCode = "P2";
                String strProcessCode = "A100";
                String strEquipCode = "Equip001";
                String strLotNo = "LN092604";
                String strWorkUserID = "admin";

                //자동의뢰 저장 Method 호출
                mfSaveAuto(strProductCode, strProcessCode, strEquipCode, strLotNo, strWorkUserID);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자동의뢰 저장(MES I/F)
        public void mfSaveAuto(String strProductCode, String strProcessCode, String strEquipCode, String strLotNo, String strWorkUserID)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            String strLoginPlantCode = m_resSys.GetString("SYS_PLANTCODE");
            DataTable dtSaveHeader = new DataTable();
            DataTable dtSaveLot = new DataTable();
            DataTable dtSaveItem = new DataTable();
            DataTable dtSaveMeasure = new DataTable();
            DataTable dtSaveCount = new DataTable();
            DataTable dtSaveOkNo = new DataTable();
            DataTable dtSaveDesc = new DataTable();
            DataTable dtSaveSelect = new DataTable();
            DataTable dtPara = new DataTable();
            try
            {
                //헤더 정보 BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                // Header 정보 Column 설정
                dtSaveHeader = clsHeader.mfSetDataInfo();
                DataRow drRow = dtSaveHeader.NewRow();
                drRow["PlantCode"] = strLoginPlantCode;
                drRow["ReqNo"] = "";
                drRow["ReqSeq"] = "";
                drRow["ProductCode"] = strProductCode;
                drRow["ProcessCode"] = strProcessCode;
                drRow["EquipCode"] = strEquipCode;
                drRow["EtcDesc"] = "";
                drRow["InspectResultFlag"] = "";
                dtSaveHeader.Rows.Add(drRow);

                //Lot 정보 BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);
                }
                else
                {
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                // Lot 정보 Column 설정
                dtSaveLot = clsLot.mfSetDataInfo();
                DataRow drRowLot;
                // 데이터 저장

                drRowLot = dtSaveLot.NewRow();
                drRowLot["PlantCode"] = strLoginPlantCode;
                drRowLot["ReqNo"] = "";
                drRowLot["ReqSeq"] = "";
                drRowLot["ReqLotSeq"] = 1;  // 자동의뢰되는 경우 이므로 무조건 1        
                drRowLot["LotNo"] = strLotNo;
                drRowLot["CCSReqTypeCode"] = "";
                drRowLot["ReqUserId"] = strWorkUserID;
                drRowLot["ReqDate"] = DateTime.Today;
                drRowLot["ReqTime"] = "";
                drRowLot["CauseReason"] = "";
                drRowLot["CorrectAction"] = "";
                drRowLot["LotMonthCount"] = 0;
                drRowLot["LotYearCount"] = 0;
                dtSaveLot.Rows.Add(drRowLot);


                // 헤더, Lot 정보만 저장 한다.                
                WinMessageBox msg = new WinMessageBox();
                String strErrRtn = "";

                // Method 호출
                strErrRtn = clsHeader.mfSaveCCSInsepctReqH(dtSaveHeader, dtSaveLot, dtSaveItem, dtSaveMeasure, dtSaveCount, dtSaveOkNo, dtSaveDesc, dtSaveSelect, dtPara
                                    , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_LANG"), this.Name);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //// 처리결과에 따른 메세지 박스
                //if (ErrRtn.ErrNum == 0)
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                //                        Infragistics.Win.HAlign.Right);
                //}
                //else
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                //                        Infragistics.Win.HAlign.Right);
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제
        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 생성
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    // ContentsArea 접힌상태를 펼침상태로 변경
                    this.uGroupBoxContentsArea.Expanded = true;
                    InitClear();
                }
                else
                {
                    // 이미 펼친 상태이면 초기화
                    InitClear();
                    //this.uButtonCancelReq1.Visible = false;
                }
                this.uTabCCS.Tabs["1st"].Selected = true;
                this.uTabCCS.Tabs["1st"].Active = true;
                this.uTextLotNo1.Focus();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 출력
        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        // 엑셀
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                ////if (this.uGridCCSReq1.Rows.Count > 0)
                ////{
                ////    wGrid.mfDownLoadGridToExcel(this.uGridCCSReq1);

                ////    if (this.uGridCCSReq2.Rows.Count > 0)
                ////    {
                ////        wGrid.mfDownLoadGridToExcel(this.uGridCCSReq2);
                ////    }
                ////    if (this.uGridCCSReq3.Rows.Count > 0)
                ////    {
                ////        wGrid.mfDownLoadGridToExcel(this.uGridCCSReq3);
                ////    }
                ////}
                if (this.uGridCCSReqList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridCCSReqList);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 저장 메소드

        /// <summary>
        /// 헤더정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>        
        private DataTable SaveHeaderInfo()
        {
            DataTable dtSaveHeader = new DataTable();
            try
            {
                //BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                // DataTable Column 설정
                dtSaveHeader = clsHeader.mfSetDataInfo();

                DataRow drRow = dtSaveHeader.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["ReqNo"] = strReqNo;
                drRow["ReqSeq"] = strReqSeq;
                drRow["ProductCode"] = this.uTextProductCode.Text;
                drRow["WorkProcessCode"] = this.uTextWorkProcessCode.Text;
                drRow["NowProcessCode"] = this.uTextNowProcessCode.Text;
                //drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["EquipCode"] = this.uComboEquip.Value.ToString();
                drRow["EtcDesc"] = "";
                drRow["InspectResultFlag"] = "";
                drRow["MESTFlag"] = this.uTextMESTFlag.Text;
                drRow["CCSCreateType"] = this.uTextCreateType.Text;
                dtSaveHeader.Rows.Add(drRow);

                return dtSaveHeader;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveHeader;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot 정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>                
        private DataTable SaveLotInfo()
        {
            DataTable dtSaveLot = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtLot = new DataTable();

                m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString());

                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);
                }
                else
                {
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                    dtLot = clsLot.mfReadCCSInspectReqLot(this.uComboPlant.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                }

                // Column 설정
                dtSaveLot = clsLot.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                drRow = dtSaveLot.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["ReqNo"] = strReqNo;
                drRow["ReqSeq"] = strReqSeq;
                drRow["ReqLotSeq"] = m_intTabIndex + 1;                             //Convert.ToInt32(this.uTabCCS.SelectedTab.Index.ToString()) + 1; 
                drRow["CCSCreateType"] = "Q";           //QRP화면등록:Q, MES I/F등록:M

                //1차탭 선택된 경우
                if (m_intTabIndex == 0)
                {
                    drRow["LotNo"] = this.uTextLotNo1.Text;
                    drRow["CCSReqTypeCode"] = this.uComboCCSReqType1.Value.ToString();
                    drRow["ReqUserId"] = this.uTextReqUserID1.Text;
                    drRow["StepUserId"] = this.uTextStepUserID1.Text;
                    //drRow["ReqDate"] = Convert.ToDateTime(this.uDateReqDate1.Value).ToString("yyyy-MM-dd");
                    drRow["ReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                    if (this.uTextReqTime1.Text.Equals(string.Empty))
                        drRow["ReqTime"] = DateTime.Now.ToString("HH:mm:ss");
                    else
                        drRow["ReqTime"] = this.uTextReqTime1.Text;
                    drRow["CauseReason"] = this.uTextCauseReason1.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction1.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;
                    drRow["StackSeq"] = this.uComboStackSeq1.Value.ToString();

                    drRow["Chase1"] = this.uCheckCHASE1_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE1_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE1_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE1_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE1_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE1_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE1_All.Checked.ToString().ToUpper().Substring(0, 1);

                    drRow["CompleteFlag"] = this.uTextComplete1.Text;
                    drRow["InspectResultFlag"] = this.uTextInspectResult1.Text;
                    if (this.uDateReceiptDate1.Value == null)
                        drRow["ReceiptDate"] = string.Empty;
                    else
                        drRow["ReceiptDate"] = this.uDateReceiptDate1.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReceiptTime"] = this.uTextReceiptTime1.Text;

                    drRow["EtcDesc"] = this.uTextEtcDesc1.Text;

                    // Update 시 의뢰현황에서 입력한값이 공백으로 Update되는것 방지
                    //if (!strFullReqNo.Equals(string.Empty))
                    if (dtLot.Rows.Count > 0)
                    {
                        //drRow["InspectFaultTypeCode"] = dtLot.Rows[0]["InspectFaultTypeCode"].ToString();
                        drRow["CompleteFlag"] = dtLot.Rows[m_intTabIndex]["CompleteFlag"].ToString();
                        drRow["CompleteDate"] = dtLot.Rows[m_intTabIndex]["CompleteDate"].ToString();
                        drRow["CompleteTime"] = dtLot.Rows[m_intTabIndex]["CompleteTime"].ToString();
                        drRow["InspectUserID"] = dtLot.Rows[m_intTabIndex]["InspectUserID"].ToString();
                        drRow["InspectFaultTypeCode"] = dtLot.Rows[m_intTabIndex]["InspectFaultTypeCode"].ToString();
                        //drRow["EtcDesc"] = dtLot.Rows[m_intTabIndex]["EtcDesc"].ToString();
                    }
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag1.Text;

                    if (this.uTextNowProcessName.Text.Contains("W/B"))
                    {
                        drRow["ProgramSource"] = this.uComboProgramSource1.Text;
                    }

                }
                //2차탭 선택된 경우
                else if (m_intTabIndex == 1)
                {
                    drRow["LotNo"] = this.uTextLotNo2.Text;
                    drRow["CCSReqTypeCode"] = this.uComboCCSReqType2.Value.ToString();
                    drRow["ReqUserId"] = this.uTextReqUserID2.Text;
                    drRow["StepUserId"] = this.uTextStepUserID2.Text;
                    //drRow["ReqDate"] = Convert.ToDateTime(this.uDateReqDate2.Value).ToString("yyyy-MM-dd");
                    drRow["ReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                    if (this.uTextReqTime2.Text.Equals(string.Empty))
                        drRow["ReqTime"] = DateTime.Now.ToString("HH:mm:ss");
                    else
                        drRow["ReqTime"] = this.uTextReqTime2.Text;
                    drRow["CauseReason"] = this.uTextCauseReason2.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction2.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;
                    drRow["StackSeq"] = this.uComboStackSeq2.Value.ToString();

                    drRow["Chase1"] = this.uCheckCHASE2_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE2_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE2_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE2_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE2_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE2_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE2_All.Checked.ToString().ToUpper().Substring(0, 1);

                    drRow["CompleteFlag"] = this.uTextComplete2.Text;
                    drRow["InspectResultFlag"] = this.uTextInspectResult2.Text;
                    if (this.uDateReceiptDate2.Value == null)
                        drRow["ReceiptDate"] = string.Empty;
                    else
                        drRow["ReceiptDate"] = this.uDateReceiptDate2.Value.ToString();
                    drRow["ReceiptTime"] = this.uTextReceiptTime2.Text;

                    drRow["EtcDesc"] = this.uTextEtcDesc2.Text;

                    // Update 시 의뢰현황에서 입력한값이 공백으로 Update되는것 방지
                    //if (!strFullReqNo.Equals(string.Empty))
                    if (dtLot.Rows.Count > 1)
                    {
                        //drRow["InspectFaultTypeCode"] = dtLot.Rows[1]["InspectFaultTypeCode"].ToString();
                        drRow["CompleteFlag"] = dtLot.Rows[m_intTabIndex]["CompleteFlag"].ToString();
                        drRow["CompleteDate"] = dtLot.Rows[m_intTabIndex]["CompleteDate"].ToString();
                        drRow["CompleteTime"] = dtLot.Rows[m_intTabIndex]["CompleteTime"].ToString();
                        drRow["InspectUserID"] = dtLot.Rows[m_intTabIndex]["InspectUserID"].ToString();
                        drRow["InspectFaultTypeCode"] = dtLot.Rows[m_intTabIndex]["InspectFaultTypeCode"].ToString();
                        //drRow["EtcDesc"] = dtLot.Rows[m_intTabIndex]["EtcDesc"].ToString();
                    }
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag2.Text;

                    if (this.uTextNowProcessName.Text.Contains("W/B"))
                    {
                        drRow["ProgramSource"] = this.uComboProgramSource2.Text;
                    }

                }
                //3차탭 선택된 경우
                else if (m_intTabIndex == 2)
                {
                    drRow["LotNo"] = this.uTextLotNo3.Text;
                    drRow["CCSReqTypeCode"] = this.uComboCCSReqType3.Value.ToString();
                    drRow["ReqUserId"] = this.uTextReqUserID3.Text;
                    drRow["StepUserId"] = this.uTextStepUserID3.Text;
                    //drRow["ReqDate"] = Convert.ToDateTime(this.uDateReqDate3.Value).ToString("yyyy-MM-dd");
                    drRow["ReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                    if (this.uTextReqTime3.Text.Equals(string.Empty))
                        drRow["ReqTime"] = DateTime.Now.ToString("HH:mm:ss");
                    else
                        drRow["ReqTime"] = this.uTextReqTime3.Text;
                    drRow["CauseReason"] = this.uTextCauseReason3.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction3.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;
                    drRow["StackSeq"] = this.uComboStackSeq3.Value.ToString();

                    drRow["Chase1"] = this.uCheckCHASE3_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE3_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE3_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE3_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE3_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE3_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE3_All.Checked.ToString().ToUpper().Substring(0, 1);

                    drRow["CompleteFlag"] = this.uTextComplete3.Text;
                    drRow["InspectResultFlag"] = this.uTextInspectResult3.Text;
                    if (this.uDateReceiptDate3.Value == null)
                        drRow["ReceiptDate"] = string.Empty;
                    else
                        drRow["ReceiptDate"] = this.uDateReceiptDate3.Value.ToString();
                    drRow["ReceiptTime"] = this.uTextReceiptTime3.Text;

                    drRow["EtcDesc"] = this.uTextEtcDesc3.Text;

                    // Update 시 의뢰현황에서 입력한값이 공백으로 Update되는것 방지
                    //if (!strFullReqNo.Equals(string.Empty))
                    if (dtLot.Rows.Count > 2)
                    {
                        drRow["CompleteFlag"] = dtLot.Rows[m_intTabIndex]["CompleteFlag"].ToString();
                        drRow["CompleteDate"] = dtLot.Rows[m_intTabIndex]["CompleteDate"].ToString();
                        drRow["CompleteTime"] = dtLot.Rows[m_intTabIndex]["CompleteTime"].ToString();
                        drRow["InspectUserID"] = dtLot.Rows[m_intTabIndex]["InspectUserID"].ToString();
                        drRow["InspectFaultTypeCode"] = dtLot.Rows[m_intTabIndex]["InspectFaultTypeCode"].ToString();
                        //drRow["EtcDesc"] = dtLot.Rows[m_intTabIndex]["EtcDesc"].ToString();
                    }
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag3.Text;

                    if (this.uTextNowProcessName.Text.Contains("W/B"))
                    {
                        drRow["ProgramSource"] = this.uComboProgramSource3.Text;
                    }

                }
                //drRow["CompleteDate"] = "";
                //drRow["CompleteTime"] = "";
                //drRow["InspectUserID"] = "";
                //drRow["FaultTypeCode"] = "";
                //drRow["InspectResultFlag"] = "";
                //drRow["CompleteFlag"] = "";
                //drRow["EtcDesc"] = "";
                dtSaveLot.Rows.Add(drRow);

                return dtSaveLot;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveLot;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable SaveItemInfo()
        {
            DataTable dtSaveItem = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                // 사용자가 선택한 탭Index 및 탭에 속한 그리드명 가져오기

                if (m_intTabIndex == 0)
                {
                    m_intRow = this.uGridCCSReq1.Rows.Count;
                    m_grdCCS = this.uGridCCSReq1;
                }
                else if (m_intTabIndex == 1)
                {
                    m_intRow = this.uGridCCSReq2.Rows.Count;
                    m_grdCCS = this.uGridCCSReq2;
                }
                else if (m_intTabIndex == 2)
                {
                    m_intRow = this.uGridCCSReq3.Rows.Count;
                    m_grdCCS = this.uGridCCSReq3;
                }

                dtSaveItem = clsItem.mfSetDataInfo();
                DataRow drRow;
                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_intRow; i++)
                    {
                        drRow = dtSaveItem.NewRow();

                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = m_intTabIndex + 1;
                        drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                        drRow["ReqItemType"] = "P";
                        drRow["Seq"] = m_grdCCS.Rows[i].Cells["Seq"].Value;
                        drRow["LotNo"] = m_grdCCS.Rows[i].Cells["LotNo"].Value;
                        drRow["InspectFaultTypeCode"] = m_grdCCS.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString();

                        drRow["InspectIngFlag"] = m_grdCCS.Rows[i].Cells["InspectIngFlag"].Value.ToString().ToUpper().Substring(0, 1);
                        drRow["ProductInspectSS"] = m_grdCCS.Rows[i].Cells["ProductItemSS"].Value;
                        drRow["QualityInspectSS"] = 0;
                        drRow["FaultQty"] = m_grdCCS.Rows[i].Cells["FaultQty"].Value;
                        drRow["EtcDesc"] = m_grdCCS.Rows[i].Cells["EtcDesc"].Value.ToString();

                        if (this.m_grdCCS.Rows[i].Cells["Mean"].Value == null || this.m_grdCCS.Rows[i].Cells["Mean"].Value == DBNull.Value)
                            drRow["Mean"] = 0.0;
                        else
                            drRow["Mean"] = this.m_grdCCS.Rows[i].Cells["Mean"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value != null)
                            drRow["InspectResultFlag"] = this.m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value.ToString();

                        if (this.m_grdCCS.Rows[i].Cells["StdDev"].Value == null || this.m_grdCCS.Rows[i].Cells["StdDev"].Value == DBNull.Value)
                            drRow["StdDev"] = 0.0;
                        else
                            drRow["StdDev"] = this.m_grdCCS.Rows[i].Cells["StdDev"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["MaxValue"].Value == null || this.m_grdCCS.Rows[i].Cells["MaxValue"].Value == DBNull.Value)
                            drRow["MaxValue"] = 0.0;
                        else
                            drRow["MaxValue"] = this.m_grdCCS.Rows[i].Cells["MaxValue"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["MinValue"].Value == null || this.m_grdCCS.Rows[i].Cells["MinValue"].Value == DBNull.Value)
                            drRow["MinValue"] = 0.0;
                        else
                            drRow["MinValue"] = this.m_grdCCS.Rows[i].Cells["MinValue"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["DataRange"].Value == null || this.m_grdCCS.Rows[i].Cells["DataRange"].Value == DBNull.Value)
                            drRow["DataRange"] = 0.0;
                        else
                            drRow["DataRange"] = this.m_grdCCS.Rows[i].Cells["DataRange"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["Cp"].Value == null || this.m_grdCCS.Rows[i].Cells["Cp"].Value == DBNull.Value)
                            drRow["Cp"] = 0.0;
                        else
                            drRow["Cp"] = this.m_grdCCS.Rows[i].Cells["Cp"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["Cpk"].Value == null || this.m_grdCCS.Rows[i].Cells["Cpk"].Value == DBNull.Value)
                            drRow["Cpk"] = 0.0;
                        else
                            drRow["Cpk"] = this.m_grdCCS.Rows[i].Cells["Cpk"].Value;

                        if (!this.uTextStdNumber.Text.Equals(string.Empty))
                        {
                            String strStdNumber = this.uTextStdNumber.Text.Substring(0, 9);
                            String strStdSeq = this.uTextStdNumber.Text.Substring(9, 4);
                            drRow["StdNumber"] = strStdNumber;
                            drRow["StdSeq"] = strStdSeq;
                        }

                        dtSaveItem.Rows.Add(drRow);
                    }
                }
                return dtSaveItem;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveItem;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계량형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable SaveResultMeasure(int intStart)
        {
            DataTable dtMeasure = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultMeasure clsMeasure;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultMeasure), "CCSInspectResultMeasure");
                    clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure();
                    brwChannel.mfCredentials(clsMeasure);
                }
                else
                {
                    clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure(m_strDBConn);
                }

                // Column 설정
                dtMeasure = clsMeasure.mfSetDataInfo();
                DataRow drRow;

                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["ProductItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "1" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtMeasure.NewRow();

                                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "P";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value == null || this.m_grdCCS.Rows[i].Cells[j].Value == DBNull.Value)
                                    drRow["InspectValue"] = 0.0;
                                else
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value;
                                dtMeasure.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtMeasure;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMeasure;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계수형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultCount(int intStart)
        {
            DataTable dtCount = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultCount clsCount;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultCount), "CCSInspectResultCount");
                    clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount();
                    brwChannel.mfCredentials(clsCount);
                }
                else
                {
                    clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount(m_strDBConn);
                }
                // Column 설정
                dtCount = clsCount.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["ProductItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "2" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtCount.NewRow();

                                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "P";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value == null || this.m_grdCCS.Rows[i].Cells[j].Value == DBNull.Value)
                                    drRow["InspectValue"] = 0.0;
                                else
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value;
                                dtCount.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtCount;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtCount;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 Ok/Ng인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultOkNg(int intStart)
        {
            DataTable dtOkNg = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultOkNg clsOkNg;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultOkNg), "CCSInspectResultOkNg");
                    clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg();
                    brwChannel.mfCredentials(clsOkNg);
                }
                else
                {
                    clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg(m_strDBConn);
                }

                // Column 설정
                dtOkNg = clsOkNg.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["ProductItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "3" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtOkNg.NewRow();

                                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "P";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtOkNg.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtOkNg;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtOkNg;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 설명인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultDesc(int intStart)
        {
            DataTable dtDesc = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultDesc clsDesc;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultDesc), "CCSInspectResultDesc");
                    clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc();
                    brwChannel.mfCredentials(clsDesc);
                }
                else
                {
                    clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc(m_strDBConn);
                }

                // Column 설정
                dtDesc = clsDesc.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["ProductItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "4" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtDesc.NewRow();

                                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "P";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtDesc.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtDesc;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDesc;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 선택인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultSelect(int intStart)
        {
            DataTable dtSelect = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultSelect clsSelect;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultSelect), "CCSInspectResultSelect");
                    clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect();
                    brwChannel.mfCredentials(clsSelect);
                }
                else
                {
                    clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect(m_strDBConn);
                }
                // Column 설정
                dtSelect = clsSelect.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["ProductItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "5" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtSelect.NewRow();


                                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "P";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtSelect.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtSelect;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSelect;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 가동조건 정보를 반환하는 메소드

        /// </summary>
        /// <returns></returns>
        DataTable SaveParaInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = string.Empty;
                String strReqSeq = string.Empty;
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);

                dtRtn = clsPara.mfSetDataInfo();
                DataRow drRow;

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                // 사용자가 선택한 탭Index 및 탭에 속한 그리드명 가져오기

                if (m_intTabIndex == 0)
                {
                    uGrid = this.uGridPara1;
                }
                else if (m_intTabIndex == 1)
                {
                    uGrid = this.uGridPara2;
                }
                else if (m_intTabIndex == 2)
                {
                    uGrid = this.uGridPara3;
                }

                if (uGrid.Rows.Count > 0)
                {
                    // ActiveCell 이동
                    uGrid.ActiveCell = uGrid.Rows[0].Cells[0];
                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = m_intTabIndex + 1;
                        drRow["ParaSeq"] = ReturnIntegerValue(uGrid.Rows[i].Cells["ParaSeq"].Value);
                        drRow["LinkId"] = uGrid.Rows[i].Cells["LinkId"].Value.ToString();
                        drRow["CCSParameterCode"] = uGrid.Rows[i].Cells["CCSParameterCode"].Value.ToString();
                        drRow["DATATYPE"] = uGrid.Rows[i].Cells["DATATYPE"].Value.ToString();
                        drRow["VALIDATIONTYPE"] = uGrid.Rows[i].Cells["VALIDATIONTYPE"].Value.ToString();
                        drRow["ParaVALUE"] = uGrid.Rows[i].Cells["ParaVALUE"].Value.ToString();
                        drRow["LOWERLIMIT"] = uGrid.Rows[i].Cells["LOWERLIMIT"].Value.ToString();
                        drRow["UPPERLIMIT"] = uGrid.Rows[i].Cells["UPPERLIMIT"].Value.ToString();
                        drRow["InspectValue"] = uGrid.Rows[i].Cells["InspectValue"].Value.ToString();
                        drRow["QualityValue"] = uGrid.Rows[i].Cells["QualityValue"].Value.ToString();
                        drRow["InspectResultFlag"] = uGrid.Rows[i].Cells["InspectResultFlag"].Value.ToString();
                        drRow["MaterialSpecName"] = uGrid.Rows[i].Cells["MaterialSpecName"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }
        #endregion

        #region Action 이벤트


        //// 상세정보 : 의뢰사유정보 Load (공장 선택 시)
        //private void uComboPlant_ValueChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        // SystemInfo 리소스

        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinComboEditor wCombo = new WinComboEditor();

        //        DataTable dtCCSReaType = new DataTable();
        //        String strPlantCode = this.uComboPlant.Value.ToString();

        //        this.uComboCCSReqType1.Items.Clear();

        //        if (strPlantCode != "")
        //        {
        //            //BL연결
        //            QRPBrowser brwChannel = new QRPBrowser();
        //            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
        //            QRPMAS.BL.MASQUA.CCSReqType clsCCSReaType = new QRPMAS.BL.MASQUA.CCSReqType();
        //            brwChannel.mfCredentials(clsCCSReaType);

        //            //dtCCSReaType = clsCCSReaType.mfReadMASReqTypeCombo(strPlantCode, this.uTextProcessCode.Text, m_resSys.GetString("SYS_LANG"));dtCCSReaType = clsCCSReaType.mfReadMASReqTypeCombo(strPlantCode, this.uTextProcessCode.Text, m_resSys.GetString("SYS_LANG"));
        //            dtCCSReaType = clsCCSReaType.mfReadMASReqTypeCombo(strPlantCode, "PC01", m_resSys.GetString("SYS_LANG"));
        //        }

        //        wCombo.mfSetComboEditor(this.uComboCCSReqType1, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
        //            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
        //            , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReaType);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// 상세정보 : 1차 LotNo 입력 후 엔터시 해당 공정검사 규격서 정보를 가지고온다.        
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextLotNo1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                //{
                //    this.uTextCustomerCode.Text = "";
                //    this.uTextCustomerName.Text = "";
                //    this.uTextWorkProcessCode.Text = "";
                //    this.uTextWorkProcessName.Text = "";
                //    this.uTextNowProcessCode.Clear();
                //    this.uTextNowProcessName.Clear();
                //    this.uTextEquipCode.Text = "";
                //    this.uTextEquipName.Text = "";
                //    this.uTextProductCode.Text = "";
                //    this.uTextProductName.Text = "";
                //    this.uTextCustomerProductCode.Text = "";
                //    this.uTextPackage.Text = "";
                //    while (this.uGridCCSReq1.Rows.Count > 0)
                //    {
                //        this.uGridCCSReq1.Rows[0].Delete(false);
                //    }
                //    while (this.uGridPara1.Rows.Count > 0)
                //    {
                //        this.uGridPara1.Rows[0].Delete(false);
                //    }
                //}
                if (e.KeyCode != Keys.Enter)
                    return;

                // SystemsInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextLotNo1.Text != "")
                {
                    // 그리드 이벤트 헤제
                    this.uGridCCSReq1.EventManager.AllEventsEnabled = false;

                    //////////string strLot = this.uTextLotNo1.Text.Trim().ToUpper();
                    //////////this.uTextLotNo1.Text = strLot;

                    ////////// MES Lot 정보 요청 매서드 실행
                    ////////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
                    ////////DataTable dtLotNo = clsTibrv.LOT_INFO_REQ(this.uTextLotNo1.Text.ToUpper().Trim());

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                    QRPCCS.BL.INSCCS.CCSMESInterface clsMes = new QRPCCS.BL.INSCCS.CCSMESInterface();
                    brwChannel.mfCredentials(clsMes);

                    // 화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S04");        //Live Server
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S07");        //Test Server
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), clsMes.MesCode);        //Test Server

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strLotNo = this.uTextLotNo1.Text.ToUpper().Trim();
                    DataTable dtLotNo = clsMes.mfRead_LOT_INFO_REQ(strLotNo, dtSysAccess);

                    // 요청하여 정보가 있을 경우 
                    if (dtLotNo.Rows.Count > 0)
                    {
                        if (dtLotNo.Rows[0]["returnmessage"].ToString() == "")
                        {
                            // 제품코드 I/F 저장
                            string strProduct = dtLotNo.Rows[0]["PRODUCTSPECNAME"].ToString();    // 실제 소스
                            string strEquipCode = dtLotNo.Rows[0]["EQPID"].ToString();  //설비코드
                            string strNowProcessCode = dtLotNo.Rows[0]["OPERID"].ToString(); // 현재공정코드


                            // Lot 상태가 TrackIn 상태(RUN)인 경우만 의뢰 가능. 2012-10-05 추가
                            string strLotProcessState = dtLotNo.Rows[0]["LOTPROCESSSTATE"].ToString();
                            if (strLotProcessState.ToString() != "RUN")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000877", "M001518", Infragistics.Win.HAlign.Right);
                                // 화면 초기화
                                InitClear(); 
                                return;              
                            }

                            //설비 상태가 "RUN"인 경우만 CCS의뢰 가능하도록 체크 2012.10.16
                            bool bolEquipChk = CheckEquipState(this.uComboPlant.Value.ToString(), strEquipCode, strLotNo,"1");
                            if (!bolEquipChk)  // 설비상태가 "RUN"이 아니면 Return 처리 한다.
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000877", "M001520", Infragistics.Win.HAlign.Right);
                                // 화면 초기화
                                InitClear();
                                return;    
                            }

                            //CCS등록 중복체크
                            bool bolChk = RtnReqHCheck(strPlantCode, strEquipCode, strNowProcessCode, strLotNo, "1");

                            //정보가 등록되어 있다면 Return
                            if (!bolChk)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000877", "M001517", Infragistics.Win.HAlign.Right);
                                // 화면 초기화
                                InitClear();
                                return;
                            }

                            // 제품 정보 BL 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                            brwChannel.mfCredentials(clsProduct);

                            // 제품코드로 고객사코드,명 조회 매서드 실행
                            DataTable dtCustomer = clsProduct.mfReadMaterialCustomer("", strProduct, m_resSys.GetString("SYS_LANG"));

                            // MES I/F 받는 항목 및 고객사 코드 명 삽입
                            this.uTextNowProcessCode.Text = strNowProcessCode;      // 현재공정코드
                            this.uTextNowProcessName.Text = dtLotNo.Rows[0]["OPERDESC"].ToString();    // 현재공정명

                            this.uTextWorkProcessCode.Text = dtLotNo.Rows[0]["WORKOPERID"].ToString();
                            this.uTextEquipCode.Text = strEquipCode;                             //설비코드
                            this.uTextEquipName.Text = dtLotNo.Rows[0]["EQPDESC"].ToString();       //설비명

                            this.uTextProductCode.Text = strProduct;                                //제품코드
                            this.uTextProductName.Text = dtLotNo.Rows[0]["PRODUCTSPECDESC"].ToString();   //제품명

                            this.uTextCustomerProductCode.Text = dtLotNo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품코드

                            this.uTextPackage.Text = dtLotNo.Rows[0]["PACKAGE"].ToString();             //PACKAGE

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                            QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                            brwChannel.mfCredentials(clsHeader);

                            DataTable dtProcess = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));
                            if (dtProcess.Rows.Count > 0)
                                this.uTextWorkProcessName.Text = dtProcess.Rows[0]["ProcessName"].ToString();

                            // 설비콤보 설정 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            brwChannel.mfCredentials(clsEquipGroup);

                            DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlant.Value.ToString()
                                                                                                , this.uTextNowProcessCode.Text
                                                                                                , m_resSys.GetString("SYS_LANG"));

                            WinComboEditor wCombo = new WinComboEditor();
                            wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                                , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                            this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                            this.uComboEquip.Appearance.BackColor = Color.Gainsboro;
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            // 차수(Stack) 콤보박스 Data 조회 메소드
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", 1);


                            //제품코드의 고객사정보가 있을경우 삽입한다.
                            if (dtCustomer.Rows.Count > 0)
                            {
                                this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString(); //고객사코드

                                this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString(); //고객사명
                                //m_Pakcage = dtCustomer.Rows[0]["Package"].ToString();   // Package
                            }

                            // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다
                            if (this.uTextNowProcessCode.Text.Equals("A7100"))
                            {
                                this.uLabelCHASE1.Visible = true;
                                this.uCheckCHASE1_All.Visible = true;
                                this.uCheckCHASE1_1.Visible = true;
                                this.uCheckCHASE1_2.Visible = true;
                                this.uCheckCHASE1_3.Visible = true;
                                this.uCheckCHASE1_4.Visible = true;
                                this.uCheckCHASE1_5.Visible = true;
                                this.uCheckCHASE1_6.Visible = true;
                            }
                            else
                            {
                                this.uLabelCHASE1.Visible = false;
                                this.uCheckCHASE1_All.Visible = false;
                                this.uCheckCHASE1_1.Visible = false;
                                this.uCheckCHASE1_2.Visible = false;
                                this.uCheckCHASE1_3.Visible = false;
                                this.uCheckCHASE1_4.Visible = false;
                                this.uCheckCHASE1_5.Visible = false;
                                this.uCheckCHASE1_6.Visible = false;
                            }

                            if (this.uTextNowProcessName.Text.Contains("W/B"))
                            {
                                this.uLabelProgramSource1.Visible = true;
                                this.uComboProgramSource1.Visible = true;

                                this.uComboProgramSource1.Enabled = true;

                                //init uComboProgramSource
                                DataTable dtProgramSource = new DataTable();
                                dtProgramSource.Columns.Add("ProgramSourceId");
                                dtProgramSource.Columns.Add("ProgramSource");

                                DataRow dr1 = dtProgramSource.NewRow();
                                dr1["ProgramSourceId"] = "1";
                                dr1["ProgramSource"] = "Copy";
                                dtProgramSource.Rows.Add(dr1);

                                DataRow dr2 = dtProgramSource.NewRow();
                                dr2["ProgramSourceId"] = "2";
                                dr2["ProgramSource"] = "Downloading";
                                dtProgramSource.Rows.Add(dr2);

                                DataRow dr3 = dtProgramSource.NewRow();
                                dr3["ProgramSourceId"] = "3";
                                dr3["ProgramSource"] = "编程";
                                dtProgramSource.Rows.Add(dr3);

                                this.uComboProgramSource1.Items.Clear();

                                wCombo.mfSetComboEditor(this.uComboProgramSource1, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                    , "ProgramSourceId", "ProgramSource", dtProgramSource);

                            }
                            else
                            {
                                this.uLabelProgramSource1.Visible = false;
                                this.uComboProgramSource1.Visible = false;
                            }

                            CheckEquipLocing();
                        }
                        else
                        {
                            //MES서버에서 온 메세지를 보여줌
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG"))
                                                   , msg.GetMessge_Text("M000092", m_resSys.GetString("SYS_LANG"))
                                                   , dtLotNo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000092", "M001109", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    
                    String strProductCode = this.uTextProductCode.Text;
                    int intReqLotSeq = 1;
                    // 등록된 규격서 정보가 존재하는지 검사  --> LotInfo(MES I/F) 정보가 없으면 검사규격서 정보를 불러올 필요가 없을것 같음. 추후 확인 필요
                    Boolean bolcheck = CheckStdNumber(intReqLotSeq, "");
                    if (bolcheck == false)
                    {
                        this.uTextReqNo.Text = "";
                        while (this.uGridCCSReq1.Rows.Count > 0)
                        {
                            this.uGridCCSReq1.Rows[0].Delete(false);
                        }

                        if (!this.uTextStdNumber.Text.Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000325"
                                                                    , "M000380"
                                                                    , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001208", "M000379",
                                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    // CCS 의뢰유형 설정 메소드 호출
                    comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, intReqLotSeq);

                    if (this.uComboStackSeq1.Items.Count > 1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001131", "M001130",
                                                                    Infragistics.Win.HAlign.Right);
                    }
                    // 그리드 이벤트 헤제
                    this.uGridCCSReq1.EventManager.AllEventsEnabled = true;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 : 2차 LotNo 입력 후 엔터시 해당 공정검사 규격서 정보를 가지고온다.       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextLotNo2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                //{
                //    this.uTextCustomerCode.Text = "";
                //    this.uTextCustomerName.Text = "";
                //    this.uTextWorkProcessCode.Text = "";
                //    this.uTextWorkProcessName.Text = "";
                //    this.uTextEquipCode.Text = "";
                //    this.uTextEquipName.Text = "";
                //    this.uTextProductCode.Text = "";
                //    this.uTextProductName.Text = "";
                //    this.uTextCustomerProductCode.Text = "";
                //    this.uTextPackage.Text = "";
                //    while (this.uGridCCSReq2.Rows.Count > 0)
                //    {
                //        this.uGridCCSReq2.Rows[0].Delete(false);
                //    }
                //    while (this.uGridPara2.Rows.Count > 0)
                //    {
                //        this.uGridPara2.Rows[0].Delete(false);
                //    }
                //}
                if (e.KeyCode != Keys.Enter)
                    return;
                // SystemsInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextLotNo2.Text != "")
                {
                    // 이벤트 해제
                    this.uGridCCSReq2.EventManager.AllEventsEnabled = false;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                    QRPCCS.BL.INSCCS.CCSMESInterface clsMes = new QRPCCS.BL.INSCCS.CCSMESInterface();
                    brwChannel.mfCredentials(clsMes);

                    // 화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S04");        //Live Server
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S07");        //Test Server
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), clsMes.MesCode);        //Test Server

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strLotNo = this.uTextLotNo2.Text.ToUpper().Trim();
                    DataTable dtLotNo = clsMes.mfRead_LOT_INFO_REQ(strLotNo, dtSysAccess);

                    // 요청하여 정보가 있을 경우 
                    if (dtLotNo.Rows.Count > 0)
                    {
                        if (dtLotNo.Rows[0]["returnmessage"].ToString() == "")
                        {
                            //if (!this.uTextPackage.Text.Equals(dtLotNo.Rows[0]["PACKAGE"].ToString()))
                            //    //|| !this.uTextNowProcessCode.Text.Equals(dtLotNo.Rows[0]["OPERID"].ToString())
                            //    ////|| !this.uTextEquipCode.Text.Equals(dtLotNo.Rows[0]["EQPID"].ToString()))
                            //    //|| !this.uComboEquip.Value.ToString().Equals(dtLotNo.Rows[0]["EQPID"].ToString()))
                            //{
                            //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            //                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "LotNo정보 확인", "1차 LotNo 정보와 2차 LotNo 정보가 다릅니다."
                            //                                    , "1차 현재공정 : "+ this.uTextNowProcessCode.Text + " / 1차 Package : "+ this.uTextPackage.Text
                            //                                        + " / 1차 설비번호 : " + this.uComboEquip.Value.ToString() + "<br/>"
                            //                                        + "2차 현재공정 : " + dtLotNo.Rows[0]["OPERID"].ToString() 
                            //                                        + " / 2차 Package : " + dtLotNo.Rows[0]["PACKAGE"].ToString()
                            //                                        + " / 2차 설비번호 : " + dtLotNo.Rows[0]["EQPID"].ToString()
                            //                                    , Infragistics.Win.HAlign.Right);

                            //    return;
                            //}

                            //제품코드 저장

                            string strProduct = dtLotNo.Rows[0]["PRODUCTSPECNAME"].ToString();
                            string strEquipCode = dtLotNo.Rows[0]["EQPID"].ToString();  //설비코드
                            string strNowProcessCode = dtLotNo.Rows[0]["OPERID"].ToString(); // 현재공정코드

                            //CCS등록 중복체크
                            bool bolChk = RtnReqHCheck(strPlantCode, strEquipCode, strNowProcessCode, strLotNo, "2");

                            //정보가 등록되어 있다면 Return
                            if (!bolChk)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000877", "M001517", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            // 제품 정보 BL 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                            brwChannel.mfCredentials(clsProduct);

                            // 제품코드로 고객사코드,명 조회 매서드 실행
                            DataTable dtCustomer = clsProduct.mfReadMaterialCustomer("", strProduct, m_resSys.GetString("SYS_LANG"));

                            // MES I/F 받는 항목 및 고객사 코드 명 삽입
                            this.uTextNowProcessCode.Text = dtLotNo.Rows[0]["OPERID"].ToString();      // 현재공정코드
                            this.uTextNowProcessName.Text = dtLotNo.Rows[0]["OPERDESC"].ToString();    // 현재공정명

                            this.uTextWorkProcessCode.Text = dtLotNo.Rows[0]["WORKOPERID"].ToString();
                            //this.uTextEquipCode.Text = dtLotNo.Rows[0]["EQPID"].ToString();         //설비코드
                            //this.uTextEquipName.Text = dtLotNo.Rows[0]["EQPDESC"].ToString();       //설비명

                            this.uTextProductCode.Text = strProduct;                                //제품코드
                            this.uTextProductName.Text = dtLotNo.Rows[0]["PRODUCTSPECDESC"].ToString();   //제품명

                            this.uTextCustomerProductCode.Text = dtLotNo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품코드

                            this.uTextPackage.Text = dtLotNo.Rows[0]["PACKAGE"].ToString();             //PACKAGE

                            // 가동조건 설정
                            SearchInspectReqPara(this.uComboPlant.Value.ToString(), "", "", 2);
                            //SetParaInfo();
                            // 설비콤보 설정 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            //brwChannel.mfCredentials(clsEquipGroup);

                            //DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlant.Value.ToString()
                            //                                                                    , this.uTextNowProcessCode.Text
                            //                                                                    , m_resSys.GetString("SYS_LANG"));

                            //WinComboEditor wCombo = new WinComboEditor();
                            //wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            //                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                            //                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                            //this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                            QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                            brwChannel.mfCredentials(clsHeader);

                            DataTable dtProcess = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));
                            if (dtProcess.Rows.Count > 0)
                                this.uTextWorkProcessName.Text = dtProcess.Rows[0]["ProcessName"].ToString();
                            //제품코드의 고객사정보가 있을경우 삽입한다.
                            if (dtCustomer.Rows.Count > 0)
                            {
                                this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString(); //고객사코드

                                this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString(); //고객사명
                                //m_Pakcage = dtCustomer.Rows[0]["Package"].ToString();   // Package
                            }

                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", 2);

                            // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                            if (this.uTextNowProcessCode.Text.Equals("A7100"))
                            {
                                this.uLabelCHASE2.Visible = true;
                                this.uCheckCHASE2_All.Visible = true;
                                this.uCheckCHASE2_1.Visible = true;
                                this.uCheckCHASE2_2.Visible = true;
                                this.uCheckCHASE2_3.Visible = true;
                                this.uCheckCHASE2_4.Visible = true;
                                this.uCheckCHASE2_5.Visible = true;
                                this.uCheckCHASE2_6.Visible = true;
                            }
                            else
                            {
                                this.uLabelCHASE2.Visible = false;
                                this.uCheckCHASE2_All.Visible = false;
                                this.uCheckCHASE2_1.Visible = false;
                                this.uCheckCHASE2_2.Visible = false;
                                this.uCheckCHASE2_3.Visible = false;
                                this.uCheckCHASE2_4.Visible = false;
                                this.uCheckCHASE2_5.Visible = false;
                                this.uCheckCHASE2_6.Visible = false;
                            }

                            if (this.uTextNowProcessName.Text.Contains("W/B"))
                            {
                                this.uLabelProgramSource2.Visible = true;
                                this.uComboProgramSource2.Visible = true;

                                this.uComboProgramSource2.Enabled = true;

                                //init uComboProgramSource
                                DataTable dtProgramSource = new DataTable();
                                dtProgramSource.Columns.Add("ProgramSourceId");
                                dtProgramSource.Columns.Add("ProgramSource");

                                DataRow dr1 = dtProgramSource.NewRow();
                                dr1["ProgramSourceId"] = "1";
                                dr1["ProgramSource"] = "Copy";
                                dtProgramSource.Rows.Add(dr1);

                                DataRow dr2 = dtProgramSource.NewRow();
                                dr2["ProgramSourceId"] = "2";
                                dr2["ProgramSource"] = "Downloading";
                                dtProgramSource.Rows.Add(dr2);

                                DataRow dr3 = dtProgramSource.NewRow();
                                dr3["ProgramSourceId"] = "3";
                                dr3["ProgramSource"] = "编程";
                                dtProgramSource.Rows.Add(dr3);

                                this.uComboProgramSource2.Items.Clear();

                                WinComboEditor wCombo = new WinComboEditor();

                                wCombo.mfSetComboEditor(this.uComboProgramSource2, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                    , "ProgramSourceId", "ProgramSource", dtProgramSource);

                            }
                            else
                            {
                                this.uLabelProgramSource2.Visible = false;
                                this.uComboProgramSource2.Visible = false;
                            }

                            CheckEquipLocing();
                        }
                        //MES서버에서 온 메세지를 보여줌
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG"))
                                                   , msg.GetMessge_Text("M000092", m_resSys.GetString("SYS_LANG"))
                                                   , dtLotNo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000092", "M001109", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    String strProductCode = this.uTextProductCode.Text;
                    int intReqLotSeq = 2;

                    // 등록된 규격서 정보가 존재하는지 검사

                    Boolean bolcheck = CheckStdNumber(intReqLotSeq, "");
                    if (bolcheck == false)
                    {
                        //this.uTextReqNo.Text = "";
                        while (this.uGridCCSReq2.Rows.Count > 0)
                        {
                            this.uGridCCSReq2.Rows[0].Delete(false);
                        }

                        if (!this.uTextStdNumber.Text.Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000325"
                                                                    , "M000380"
                                                                    , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001208", "M000379",
                                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    // CCS 의뢰유형 설정 메소드 호출
                    comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, intReqLotSeq);

                    if (this.uComboStackSeq2.Items.Count > 1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001131", "M001130",
                                                                    Infragistics.Win.HAlign.Right);
                    }
                    // 이벤트 등록
                    this.uGridCCSReq2.EventManager.AllEventsEnabled = true;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 : 3차 LotNo 입력 후 엔터시 해당 공정검사 규격서 정보를 가지고온다.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextLotNo3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                //{
                //    this.uTextCustomerCode.Text = "";
                //    this.uTextCustomerName.Text = "";
                //    this.uTextWorkProcessCode.Text = "";
                //    this.uTextWorkProcessName.Text = "";
                //    this.uTextEquipCode.Text = "";
                //    this.uTextEquipName.Text = "";
                //    this.uTextProductCode.Text = "";
                //    this.uTextProductName.Text = "";
                //    this.uTextCustomerProductCode.Text = "";
                //    this.uTextPackage.Text = "";
                //    while (this.uGridCCSReq3.Rows.Count > 0)
                //    {
                //        this.uGridCCSReq3.Rows[0].Delete(false);
                //    }
                //    while (this.uGridPara3.Rows.Count > 0)
                //    {
                //        this.uGridPara3.Rows[0].Delete(false);
                //    }
                //}
                if (e.KeyCode != Keys.Enter)
                    return;

                // SystemsInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextLotNo3.Text != "")
                {
                    // 이벤트 헤제
                    this.uGridCCSReq3.EventManager.AllEventsEnabled = false;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                    QRPCCS.BL.INSCCS.CCSMESInterface clsMes = new QRPCCS.BL.INSCCS.CCSMESInterface();
                    brwChannel.mfCredentials(clsMes);

                    // 화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S04");        //Live Server
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S07");        //Test Server
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), clsMes.MesCode);        //Test Server

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strLotNo = this.uTextLotNo3.Text.ToUpper().Trim();
                    DataTable dtLotNo = clsMes.mfRead_LOT_INFO_REQ(strLotNo, dtSysAccess);

                    // 요청하여 정보가 있을 경우
                    if (dtLotNo.Rows.Count > 0)
                    {
                        if (dtLotNo.Rows[0]["returnmessage"].ToString() == "")
                        {

                            ////if (!this.uTextPackage.Text.Equals(dtLotNo.Rows[0]["PACKAGE"].ToString())
                            ////    || !this.uTextNowProcessCode.Text.Equals(dtLotNo.Rows[0]["OPERID"].ToString())
                            ////    //|| !this.uTextEquipCode.Text.Equals(dtLotNo.Rows[0]["EQPID"].ToString()))
                            ////    || !this.uComboEquip.Value.ToString().Equals(dtLotNo.Rows[0]["EQPID"].ToString()))
                            ////{
                            ////    //DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            ////    //                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "LotNo 정보 확인"
                            ////    //                                , "1, 2차 LotNo 정보와 3차 LotNo 정보가 다릅니다."
                            ////    //                                , Infragistics.Win.HAlign.Right);
                            ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            ////                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "LotNo정보 확인", "2차 LotNo 정보와 3차 LotNo 정보가 다릅니다."
                            ////                                    , "2차 현재공정 : " + this.uTextNowProcessCode.Text + " / 2차 Package : " + this.uTextPackage.Text
                            ////                                        + " / 2차 설비번호 : " + this.uComboEquip.Value.ToString() + "<br/>"
                            ////                                        + "3차 현재공정 : " + dtLotNo.Rows[0]["OPERID"].ToString()
                            ////                                        + " / 3차 Package : " + dtLotNo.Rows[0]["PACKAGE"].ToString()
                            ////                                        + " / 3차 설비번호 : " + dtLotNo.Rows[0]["EQPID"].ToString()
                            ////                                    , Infragistics.Win.HAlign.Right);

                            ////    return;
                            ////}

                            //제품코드 저장
                            string strProduct = dtLotNo.Rows[0]["PRODUCTSPECNAME"].ToString();
                            string strEquipCode = dtLotNo.Rows[0]["EQPID"].ToString();  //설비코드
                            string strNowProcessCode = dtLotNo.Rows[0]["OPERID"].ToString(); // 현재공정코드

                            //CCS등록 중복체크
                            bool bolChk = RtnReqHCheck(strPlantCode, strEquipCode, strNowProcessCode, strLotNo, "3");

                            //정보가 등록되어 있다면 Return
                            if (!bolChk)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000877", "M001517", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            // 제품 정보 BL 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                            brwChannel.mfCredentials(clsProduct);

                            // 제품코드로 고객사코드,명 조회 매서드 실행
                            DataTable dtCustomer = clsProduct.mfReadMaterialCustomer("", strProduct, m_resSys.GetString("SYS_LANG"));

                            // MES I/F 받는 항목 및 고객사 코드 명 삽입
                            this.uTextNowProcessCode.Text = dtLotNo.Rows[0]["OPERID"].ToString();      // 현재공정코드
                            this.uTextNowProcessName.Text = dtLotNo.Rows[0]["OPERDESC"].ToString();    // 현재공정명

                            this.uTextWorkProcessCode.Text = dtLotNo.Rows[0]["WORKOPERID"].ToString();
                            //this.uTextEquipCode.Text = dtLotNo.Rows[0]["EQPID"].ToString();         //설비코드
                            //this.uTextEquipName.Text = dtLotNo.Rows[0]["EQPDESC"].ToString();       //설비명

                            this.uTextProductCode.Text = strProduct;                                //제품코드
                            this.uTextProductName.Text = dtLotNo.Rows[0]["PRODUCTSPECDESC"].ToString();   //제품명

                            this.uTextCustomerProductCode.Text = dtLotNo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품코드

                            this.uTextPackage.Text = dtLotNo.Rows[0]["PACKAGE"].ToString();             //PACKAGE

                            // 가동조건 설정
                            SearchInspectReqPara(this.uComboPlant.Value.ToString(), "", "", 3);
                            //SetParaInfo();
                            // 설비콤보 설정 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            //brwChannel.mfCredentials(clsEquipGroup);

                            //DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlant.Value.ToString()
                            //                                                                    , this.uTextNowProcessCode.Text
                            //                                                                    , m_resSys.GetString("SYS_LANG"));

                            //WinComboEditor wCombo = new WinComboEditor();
                            //wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            //                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                            //                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                            //this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                            QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                            brwChannel.mfCredentials(clsHeader);

                            DataTable dtProcess = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));
                            if (dtProcess.Rows.Count > 0)
                                this.uTextWorkProcessName.Text = dtProcess.Rows[0]["ProcessName"].ToString();
                            //제품코드의 고객사정보가 있을경우 삽입한다.
                            if (dtCustomer.Rows.Count > 0)
                            {
                                this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString(); //고객사코드

                                this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString(); //고객사명
                                //m_Pakcage = dtCustomer.Rows[0]["Package"].ToString();   // Package
                            }

                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", 3);

                            // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                            if (this.uTextNowProcessCode.Text.Equals("A7100"))
                            {
                                this.uLabelCHASE3.Visible = true;
                                this.uCheckCHASE3_All.Visible = true;
                                this.uCheckCHASE3_1.Visible = true;
                                this.uCheckCHASE3_2.Visible = true;
                                this.uCheckCHASE3_3.Visible = true;
                                this.uCheckCHASE3_4.Visible = true;
                                this.uCheckCHASE3_5.Visible = true;
                                this.uCheckCHASE3_6.Visible = true;
                            }
                            else
                            {
                                this.uLabelCHASE3.Visible = false;
                                this.uCheckCHASE3_All.Visible = false;
                                this.uCheckCHASE3_1.Visible = false;
                                this.uCheckCHASE3_2.Visible = false;
                                this.uCheckCHASE3_3.Visible = false;
                                this.uCheckCHASE3_4.Visible = false;
                                this.uCheckCHASE3_5.Visible = false;
                                this.uCheckCHASE3_6.Visible = false;
                            }

                            if (this.uTextNowProcessName.Text.Contains("W/B"))
                            {
                                this.uLabelProgramSource3.Visible = true;
                                this.uComboProgramSource3.Visible = true;

                                this.uComboProgramSource3.Enabled = true;

                                //init uComboProgramSource
                                DataTable dtProgramSource = new DataTable();
                                dtProgramSource.Columns.Add("ProgramSourceId");
                                dtProgramSource.Columns.Add("ProgramSource");

                                DataRow dr1 = dtProgramSource.NewRow();
                                dr1["ProgramSourceId"] = "1";
                                dr1["ProgramSource"] = "Copy";
                                dtProgramSource.Rows.Add(dr1);

                                DataRow dr2 = dtProgramSource.NewRow();
                                dr2["ProgramSourceId"] = "2";
                                dr2["ProgramSource"] = "Downloading";
                                dtProgramSource.Rows.Add(dr2);

                                DataRow dr3 = dtProgramSource.NewRow();
                                dr3["ProgramSourceId"] = "3";
                                dr3["ProgramSource"] = "编程";
                                dtProgramSource.Rows.Add(dr3);

                                this.uComboProgramSource3.Items.Clear();

                                WinComboEditor wCombo = new WinComboEditor();

                                wCombo.mfSetComboEditor(this.uComboProgramSource3, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                    , "ProgramSourceId", "ProgramSource", dtProgramSource);

                            }
                            else
                            {
                                this.uLabelProgramSource3.Visible = false;
                                this.uComboProgramSource3.Visible = false;
                            }

                            CheckEquipLocing();
                        }
                        //MES서버에서 온 메세지를 보여줌
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG"))
                                                   , msg.GetMessge_Text("M000092", m_resSys.GetString("SYS_LANG"))
                                                   , dtLotNo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000092", "M001109", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    String strProductCode = this.uTextProductCode.Text;
                    int intReqLotSeq = 3;

                    // 등록된 규격서 정보가 존재하는지 검사

                    Boolean bolcheck = CheckStdNumber(intReqLotSeq, "");
                    if (bolcheck == false)
                    {
                        //this.uTextReqNo.Text = "";
                        while (this.uGridCCSReq3.Rows.Count > 0)
                        {
                            this.uGridCCSReq3.Rows[0].Delete(false);
                        }

                        if (!this.uTextStdNumber.Text.Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000325"
                                                                    , "M000380"
                                                                    , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001208", "M000379",
                                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    // CCS 의뢰유형 설정 메소드 호출
                    comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, intReqLotSeq);

                    if (this.uComboStackSeq3.Items.Count > 1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001131", "M001130",
                                                                    Infragistics.Win.HAlign.Right);
                    }
                    // 이벤트 등록
                    this.uGridCCSReq3.EventManager.AllEventsEnabled = true;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기존에 등록된 표준번호가 있는지 확인하는 Method
        private Boolean CheckStdNumber(int intReqLotSeq, string strStackSeq)
        {
            Boolean bolCheck = false;
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strProductCode = this.uTextProductCode.Text;
                String strPackage = this.uTextPackage.Text;

                // Method 호출에 필요한 모든 정보가 입력되었을때
                if (strPlantCode != "" && strProductCode != "")
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                    QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                    brwChannel.mfCredentials(clsHeader);

                    // 기존에 존재하는 표준번호가 있는지 확인하는 Method 호출
                    DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode, this.uTextPackage.Text, this.uTextCustomerCode.Text);
                    //DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode,  m_Pakcage);                    
                    if (dtHeader.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                        {
                            this.uTextStdNumber.Text = dtHeader.Rows[i]["StdNumber"].ToString() + dtHeader.Rows[i]["StdSeq"].ToString();
                        }

                        String strStdNumber = this.uTextStdNumber.Text.Substring(0, 9);
                        String strStdSeq = this.uTextStdNumber.Text.Substring(9, 4);

                        // ItemTable 조회                                        
                        QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                        if (m_bolDebugMode == false)
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                            clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                            brwChannel.mfCredentials(clsItem);
                        }
                        else
                        {
                            clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                        }

                        // 상세정보 Method 호출
                        // 그리드에 검사항목 정보 Display
                        InspectSpecDetail(strPlantCode, intReqLotSeq, strStackSeq);

                        if (intReqLotSeq == 1)
                        {
                            if (this.uGridCCSReq1.Rows.Count > 0)
                            {
                                this.uGridCCSReq1.DisplayLayout.Bands[0].Columns.ClearUnbound();
                                // SampleSize 만큼 컬럼생성 Method 호출
                                //String[] strLastColKey = { "InspectFaultTypeCode" };
                                String[] strLastColKey = { };
                                CreateColumn(this.uGridCCSReq1, 0, "SampleSize", "ProductItemSS", strLastColKey);

                                SetSamplingGridColumn(this.uGridCCSReq1, strPlantCode, "ProductItemSS");
                                //// 데이터 유형에 따라 셀 속성 설정
                                //WinGrid wGrid = new WinGrid();

                                //// BL호출
                                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                                //QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                                //brwChannel.mfCredentials(clsCom);

                                //// 합/부 데이터 테이블

                                //DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                                //// 선택 DropDown 적용용 DataTable
                                //int intStart = this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["1"].Index;
                                //int intSampleSize = 0;

                                //// 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                                //Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                                //MaskString.DataType = typeof(String);
                                //MaskString.MaxLength = 50;
                                //MaskString.MaskInput = "";

                                //Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                                //for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                                //{
                                //    if (this.uGridCCSReq1.Rows[i].Hidden == false)
                                //    {
                                //        intSampleSize = Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["ProductItemSS"].Value) * 
                                //                        Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["SampleSize"].Value);
                                //        // 계량
                                //        if (this.uGridCCSReq1.Rows[i].Cells["DataType"].Value.ToString() == "1")
                                //        {
                                //            this.uGridCCSReq1.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq1.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq1.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq1.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // 계수
                                //        else if (this.uGridCCSReq1.Rows[i].Cells["DataType"].Value.ToString() == "2")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq1.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq1.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // OK/NG
                                //        else if (this.uGridCCSReq1.Rows[i].Cells["DataType"].Value.ToString() == "3")
                                //        {
                                //            this.uGridCCSReq1.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq1.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, this.uGridCCSReq1.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                                //                if (this.uGridCCSReq1.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq1.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 설명
                                //        else if (this.uGridCCSReq1.Rows[i].Cells["DataType"].Value.ToString() == "4")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                                //                // MaxLength 지정방법;;;;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Editor = editorString;
                                //                if (this.uGridCCSReq1.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq1.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 선택
                                //        else if (this.uGridCCSReq1.Rows[i].Cells["DataType"].Value.ToString() == "5")
                                //        {
                                //            //// 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출                            
                                //            //int intReqItemSeq = Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["Seq"].Value);
                                //            //DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect2(strPlantCode, strStdNumber, strStdSeq, intReqItemSeq, m_resSys.GetString("SYS_LANG"));

                                //            //for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            //{
                                //            //    this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //            //    wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, this.uGridCCSReq1.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //            //}
                                //            // 선택항목 가져오는 메소드 호출
                                //            string strInspectItemCode = this.uGridCCSReq1.Rows[i].Cells["InspectItemCode"].Value.ToString();
                                //            DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect3(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq1.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, this.uGridCCSReq1.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //                if (this.uGridCCSReq1.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq1.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //    }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else if (intReqLotSeq == 2)
                        {
                            if (this.uGridCCSReq2.Rows.Count > 0)
                            {
                                this.uGridCCSReq2.DisplayLayout.Bands[0].Columns.ClearUnbound();
                                // SampleSize 만큼 컬럼생성 Method 호출
                                //String[] strLastColKey = { "InspectFaultTypeCode" };
                                String[] strLastColKey = { };
                                CreateColumn(this.uGridCCSReq2, 0, "SampleSize", "ProductItemSS", strLastColKey);

                                SetSamplingGridColumn(this.uGridCCSReq2, strPlantCode, "ProductItemSS");

                                //// 검사항목의 데이터유형(DataType)에 따라 입력 할수 있는 정보 설정.
                                //if (this.uGridCCSReq2.Rows.Count > 0)
                                //{
                                //    // 데이터 유형에 따라 셀 속성 설정
                                //    WinGrid wGrid = new WinGrid();

                                //    // BL호출
                                //    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                                //    QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                                //    brwChannel.mfCredentials(clsCom);

                                //    // 합/부 데이터 테이블

                                //    DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                                //    // 선택 DropDown 적용용 DataTable
                                //    int intStart = this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["1"].Index;
                                //    int intSampleSize = 0;

                                //    // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                                //    Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                                //    MaskString.DataType = typeof(String);
                                //    MaskString.MaxLength = 50;
                                //    MaskString.MaskInput = "";

                                //    Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                                //    for (int i = 0; i < this.uGridCCSReq2.Rows.Count; i++)
                                //    {
                                //        intSampleSize = Convert.ToInt32(this.uGridCCSReq2.Rows[i].Cells["ProductItemSS"].Value) *
                                //                        Convert.ToInt32(this.uGridCCSReq2.Rows[i].Cells["SampleSize"].Value);
                                //        // 계량
                                //        if (this.uGridCCSReq2.Rows[i].Cells["DataType"].Value.ToString() == "1")
                                //        {
                                //            this.uGridCCSReq2.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq2.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq2.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq2.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // 계수
                                //        else if (this.uGridCCSReq2.Rows[i].Cells["DataType"].Value.ToString() == "2")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq2.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq2.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // OK/NG
                                //        else if (this.uGridCCSReq2.Rows[i].Cells["DataType"].Value.ToString() == "3")
                                //        {
                                //            this.uGridCCSReq2.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq2.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, this.uGridCCSReq2.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                                //                if (this.uGridCCSReq2.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq2.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 설명
                                //        else if (this.uGridCCSReq2.Rows[i].Cells["DataType"].Value.ToString() == "4")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                                //                // MaxLength 지정방법;;;;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Editor = editorString;
                                //                if (this.uGridCCSReq2.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq2.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 선택
                                //        else if (this.uGridCCSReq2.Rows[i].Cells["DataType"].Value.ToString() == "5")
                                //        {
                                //            //// 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출                            
                                //            //int intReqItemSeq = Convert.ToInt32(this.uGridCCSReq2.Rows[i].Cells["Seq"].Value);
                                //            //DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect2(strPlantCode, strStdNumber, strStdSeq, intReqItemSeq, m_resSys.GetString("SYS_LANG"));

                                //            //for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            //{
                                //            //    this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //            //    wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, this.uGridCCSReq2.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //            //}
                                //            // 선택항목 가져오는 메소드 호출
                                //            string strInspectItemCode = this.uGridCCSReq2.Rows[i].Cells["InspectItemCode"].Value.ToString();
                                //            DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect3(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq2.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, this.uGridCCSReq2.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //                if (this.uGridCCSReq2.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq2.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else if (intReqLotSeq == 3)
                        {
                            if (this.uGridCCSReq3.Rows.Count > 0)
                            {
                                this.uGridCCSReq3.DisplayLayout.Bands[0].Columns.ClearUnbound();
                                // SampleSize 만큼 컬럼생성 Method 호출
                                //String[] strLastColKey = { "InspectFaultTypeCode" };
                                String[] strLastColKey = { };
                                CreateColumn(this.uGridCCSReq3, 0, "SampleSize", "ProductItemSS", strLastColKey);

                                SetSamplingGridColumn(this.uGridCCSReq3, strPlantCode, "ProductItemSS");

                                //// 검사항목의 데이터유형(DataType)에 따라 입력 할수 있는 정보 설정.
                                //if (this.uGridCCSReq3.Rows.Count > 0)
                                //{
                                //    // 데이터 유형에 따라 셀 속성 설정
                                //    WinGrid wGrid = new WinGrid();

                                //    // BL호출
                                //    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                                //    QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                                //    brwChannel.mfCredentials(clsCom);

                                //    // 합/부 데이터 테이블

                                //    DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                                //    // 선택 DropDown 적용용 DataTable
                                //    int intStart = this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["1"].Index;
                                //    int intSampleSize = 0;

                                //    // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                                //    Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                                //    MaskString.DataType = typeof(String);
                                //    MaskString.MaxLength = 50;
                                //    MaskString.MaskInput = "";

                                //    Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                                //    for (int i = 0; i < this.uGridCCSReq3.Rows.Count; i++)
                                //    {
                                //        intSampleSize = Convert.ToInt32(this.uGridCCSReq3.Rows[i].Cells["ProductItemSS"].Value) *
                                //                        Convert.ToInt32(this.uGridCCSReq3.Rows[i].Cells["SampleSize"].Value);
                                //        // 계량
                                //        if (this.uGridCCSReq3.Rows[i].Cells["DataType"].Value.ToString() == "1")
                                //        {
                                //            this.uGridCCSReq3.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq3.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq3.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq3.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // 계수
                                //        else if (this.uGridCCSReq3.Rows[i].Cells["DataType"].Value.ToString() == "2")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                                //                if (this.uGridCCSReq3.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq3.Rows[i].Cells[j].Value = "0.0";
                                //                }
                                //            }
                                //        }
                                //        // OK/NG
                                //        else if (this.uGridCCSReq3.Rows[i].Cells["DataType"].Value.ToString() == "3")
                                //        {
                                //            this.uGridCCSReq3.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //            this.uGridCCSReq3.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq3, i, this.uGridCCSReq1.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                                //                if (this.uGridCCSReq3.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq3.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 설명
                                //        else if (this.uGridCCSReq3.Rows[i].Cells["DataType"].Value.ToString() == "4")
                                //        {
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                                //                // MaxLength 지정방법;;;;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Editor = editorString;
                                //                if (this.uGridCCSReq3.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq3.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //        // 선택
                                //        else if (this.uGridCCSReq3.Rows[i].Cells["DataType"].Value.ToString() == "5")
                                //        {
                                //            //// 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출                            
                                //            //int intReqItemSeq = Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["Seq"].Value);
                                //            //DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect2(strPlantCode, strStdNumber, strStdSeq, intReqItemSeq, m_resSys.GetString("SYS_LANG"));

                                //            //for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            //{
                                //            //    this.uGridCCSReq1.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //            //    wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, this.uGridCCSReq1.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //            //}
                                //            // 선택항목 가져오는 메소드 호출
                                //            string strInspectItemCode = this.uGridCCSReq3.Rows[i].Cells["InspectItemCode"].Value.ToString();
                                //            DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect3(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));
                                //            for (int j = intStart; j < intStart + intSampleSize; j++)
                                //            {
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                                //                this.uGridCCSReq3.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                                //                wGrid.mfSetGridCellValueList(this.uGridCCSReq3, i, this.uGridCCSReq3.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                                //                if (this.uGridCCSReq3.Rows[i].Cells[j].Value == null)
                                //                {
                                //                    this.uGridCCSReq3.Rows[i].Cells[j].Value = "";
                                //                }
                                //            }
                                //        }
                                //    }
                                //}
                            }
                            else
                            {
                                return false;
                            }
                        }
                        // 공장 콤보박스 편집불가상태로

                        this.uComboPlant.ReadOnly = true;

                        bolCheck = true;
                    }
                }
                if (bolCheck == false)
                {
                    this.uComboPlant.ReadOnly = false;
                }
                return bolCheck;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return bolCheck;
            }
            finally
            {
            }
        }

        // 신규시 해당 검사규격서 상세정보 Display Method
        private void InspectSpecDetail(String strPlantCode, int intReqLotSeq, string strStackSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 팝업창 호출
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검사규격서 조회중");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //// 표준번호와 표준번호순번으로 구분
                //String strStdNumber = strFullStdNumber.Substring(0, 9);
                //String strStdSeq = strFullStdNumber.Substring(9, 4);

                //// BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecD), "ProcessInspectSpecD");
                //QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new QRPISO.BL.ISOPRC.ProcessInspectSpecD();
                //brwChannel.mfCredentials(clsDetail);

                //DataTable dtDetail = clsDetail.mfReadISOProcessInspectSpecD(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                //// 생산아이템 플래그가 T인것만 바인딩 시키기 위한 구문 추가
                //string strFilter = string.Format("ProductItemFlag = 'true' AND ProcessCode = '{0}'", this.uTextProcessCode.Text);
                //DataRow[] drProduct = dtDetail.Select(strFilter);

                //DataTable dtProduct = dtDetail.Clone();
                //for (int i = 0; i < drProduct.Length; i++)
                //{
                //    dtProduct.ImportRow(drProduct[i]);
                //}
                ///////////////////////////////////////////////////////////////

                string strProductCode = this.uTextProductCode.Text;
                string strProcessCode = this.uTextNowProcessCode.Text;
                string strReqItemType = "P";
                string strPackage = this.uTextPackage.Text;
                //string strEquipCode = this.uTextEquipCode.Text;
                string strEquipCode = this.uComboEquip.Value.ToString();
                string strCustomerCode = this.uTextCustomerCode.Text;
                string strReqNo = string.Empty;
                string strReqSeq = string.Empty;
                if (this.uTextReqNo.Text.Length > 12)
                {
                    strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                    strReqSeq = this.uTextReqNo.Text.Substring(8, 4);
                }
                string strNowProcessCode = this.uTextNowProcessCode.Text;
                string strCoustormer = this.uTextCustomerCode.Text;

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtProduct = clsItem.mfReadCCSInspectReqItem_Init(strPlantCode, strProcessCode, strProductCode, strReqItemType, strStackSeq, m_resSys.GetString("SYS_LANG"));

                //// Stack ComboBox 설정
                //DataTable dtStack = dtProduct.DefaultView.ToTable(true, "StackSeq", "StackCode");

                //// 가동조건

                //brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                //QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                //brwChannel.mfCredentials(clsPara);

                ////DataTable dtPara = clsPara.mfReadCCSInspectReqPara_Init(strPlantCode, strPackage, strProcessCode, strEquipCode, strCustomerCode);
                //DataTable dtPara = clsPara.mfReadCCSInspectReqPara_PSTS(strPlantCode, strReqNo, strReqSeq, intReqLotSeq
                //                                                        , strProductCode, strNowProcessCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                DataTable dtPara = SetParaInfo(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, strProductCode, strPackage, strNowProcessCode, strEquipCode, strCoustormer, m_resSys.GetString("SYS_LANG"));

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                WinGrid wGrid = new WinGrid();
                if (intReqLotSeq == 1)
                {
                    this.uGridCCSReq1.DataSource = dtProduct;
                    this.uGridCCSReq1.DataBind();

                    this.uGridPara1.DataSource = dtPara;
                    this.uGridPara1.DataBind();

                    // 가동조건이 'DIETEMP' 인 경우 텍스트 입력
                    Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                    MaskString.DataType = typeof(String);
                    MaskString.MaxLength = 40;
                    MaskString.MaskInput = "";

                    Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                    for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
                    {
                        if (this.uGridPara1.Rows[i].Cells["CCSparameterCode"].Value.ToString().Equals("DIETEMP"))
                        {
                            this.uGridPara1.Rows[i].Cells["InspectValue"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridPara1.Rows[i].Cells["InspectValue"].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridPara1.Rows[i].Cells["InspectValue"].Editor = editorString;
                            if (this.uGridPara1.Rows[i].Cells["InspectValue"].Value == null)
                            {
                                this.uGridPara1.Rows[i].Cells["InspectValue"].Value = "";
                            }
                        }
                    }

                    //if (dtProduct.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridCCSReq1, 0);
                    //if (dtPara.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridPara1, 0);

                    //this.uComboStackSeq1.Items.Clear();
                    //wCombo.mfSetComboEditor(this.uComboStackSeq1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "StackCode", "StackSeq", dtStack);

                    //// DropDown 설정
                    //for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                    //{
                    //    // 변수

                    //    String strProcessCode = this.uGridCCSReq1.Rows[i].Cells["ProcessCode"].Value.ToString();
                    //    String strInspectGroupCode = this.uGridCCSReq1.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                    //    String strInspectTypeoCode = this.uGridCCSReq1.Rows[i].Cells["InspectTypeCode"].Value.ToString();

                    //    // 공정정보 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    //    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    //    brwChannel.mfCredentials(clsProcess);
                    //    DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, "ProcessCode", "", "선택", dtProcess);

                    //    // 검사유형 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    //    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    //    brwChannel.mfCredentials(clsIType);
                    //    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, "InspectTypeCode", "", "선택", dtInspectType);

                    //    // 검사항목 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    //    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    //    brwChannel.mfCredentials(clsItem);
                    //    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, strInspectGroupCode, strInspectTypeoCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq1, i, "InspectItemCode", "", "선택", dtItem);

                    //    //// 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
                    //    //if (this.uGridCCSReq1.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "true" &&
                    //    //    this.uGridCCSReq1.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
                    //    //{
                    //    //    this.uGridCCSReq1.Rows[i].Hidden = false;
                    //    //}
                    //    //else
                    //    //{
                    //    //    this.uGridCCSReq1.Rows[i].Hidden = true;
                    //    //}
                    //}
                }
                else if (intReqLotSeq == 2)
                {
                    //DataTable dtDetail = clsDetail.mfReadISOProcessInspectSpecD(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                    this.uGridCCSReq2.DataSource = dtProduct;
                    this.uGridCCSReq2.DataBind();

                    this.uGridPara2.DataSource = dtPara;
                    this.uGridPara2.DataBind();

                    // 가동조건이 'DIETEMP' 인 경우 텍스트 입력
                    Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                    MaskString.DataType = typeof(String);
                    MaskString.MaxLength = 40;
                    MaskString.MaskInput = "";

                    Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                    for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
                    {
                        if (this.uGridPara2.Rows[i].Cells["CCSparameterCode"].Value.ToString().Equals("DIETEMP"))
                        {
                            this.uGridPara2.Rows[i].Cells["InspectValue"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridPara2.Rows[i].Cells["InspectValue"].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridPara2.Rows[i].Cells["InspectValue"].Editor = editorString;
                            if (this.uGridPara2.Rows[i].Cells["InspectValue"].Value == null)
                            {
                                this.uGridPara2.Rows[i].Cells["InspectValue"].Value = "";
                            }
                        }
                    }

                    //if (dtProduct.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridCCSReq2, 0);
                    //if (dtPara.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridPara2, 0);

                    //this.uComboStackSeq2.Items.Clear();
                    //wCombo.mfSetComboEditor(this.uComboStackSeq2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "StackCode", "StackSeq", dtStack);

                    //// DropDown 설정
                    //for (int i = 0; i < this.uGridCCSReq2.Rows.Count; i++)
                    //{
                    //    // 변수

                    //    String strProcessCode = this.uGridCCSReq2.Rows[i].Cells["ProcessCode"].Value.ToString();
                    //    String strInspectGroupCode = this.uGridCCSReq2.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                    //    String strInspectTypeoCode = this.uGridCCSReq2.Rows[i].Cells["InspectTypeCode"].Value.ToString();

                    //    // 공정정보 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    //    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    //    brwChannel.mfCredentials(clsProcess);
                    //    DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, "ProcessCode", "", "선택", dtProcess);

                    //    // 검사유형 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    //    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    //    brwChannel.mfCredentials(clsIType);
                    //    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, "InspectTypeCode", "", "선택", dtInspectType);

                    //    // 검사항목 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    //    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    //    brwChannel.mfCredentials(clsItem);
                    //    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, strInspectGroupCode, strInspectTypeoCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq2, i, "InspectItemCode", "", "선택", dtItem);

                    //    //// 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
                    //    //if (this.uGridCCSReq2.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "true" &&
                    //    //    this.uGridCCSReq2.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
                    //    //{
                    //    //    this.uGridCCSReq2.Rows[i].Hidden = false;
                    //    //}
                    //    //else
                    //    //{
                    //    //    this.uGridCCSReq2.Rows[i].Hidden = true;
                    //    //}
                    //}
                }
                else if (intReqLotSeq == 3)
                {
                    //DataTable dtDetail = clsDetail.mfReadISOProcessInspectSpecD(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                    this.uGridCCSReq3.DataSource = dtProduct;
                    this.uGridCCSReq3.DataBind();

                    this.uGridPara3.DataSource = dtPara;
                    this.uGridPara3.DataBind();

                    // 가동조건이 'DIETEMP' 인 경우 텍스트 입력
                    Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                    MaskString.DataType = typeof(String);
                    MaskString.MaxLength = 40;
                    MaskString.MaskInput = "";

                    Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                    for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
                    {
                        if (this.uGridPara3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Equals("DIETEMP"))
                        {
                            this.uGridPara3.Rows[i].Cells["InspectValue"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridPara3.Rows[i].Cells["InspectValue"].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridPara3.Rows[i].Cells["InspectValue"].Editor = editorString;
                            if (this.uGridPara3.Rows[i].Cells["InspectValue"].Value == null)
                            {
                                this.uGridPara3.Rows[i].Cells["InspectValue"].Value = "";
                            }
                        }
                    }

                    //if (dtProduct.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridCCSReq3, 0);
                    //if (dtPara.Rows.Count > 0)
                    //    wGrid.mfSetAutoResizeColWidth(this.uGridPara3, 0);

                    //this.uComboStackSeq3.Items.Clear();
                    //wCombo.mfSetComboEditor(this.uComboStackSeq3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    //    , "StackCode", "StackSeq", dtStack);

                    //// DropDown 설정
                    //for (int i = 0; i < this.uGridCCSReq3.Rows.Count; i++)
                    //{
                    //    // 변수

                    //    String strProcessCode = this.uGridCCSReq3.Rows[i].Cells["ProcessCode"].Value.ToString();
                    //    String strInspectGroupCode = this.uGridCCSReq3.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                    //    String strInspectTypeoCode = this.uGridCCSReq3.Rows[i].Cells["InspectTypeCode"].Value.ToString();

                    //    // 공정정보 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    //    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    //    brwChannel.mfCredentials(clsProcess);
                    //    DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq3, i, "ProcessCode", "", "선택", dtProcess);

                    //    // 검사유형 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    //    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    //    brwChannel.mfCredentials(clsIType);
                    //    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq3, i, "InspectTypeCode", "", "선택", dtInspectType);

                    //    // 검사항목 BL 연결
                    //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    //    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    //    brwChannel.mfCredentials(clsItem);
                    //    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, strInspectGroupCode, strInspectTypeoCode, m_resSys.GetString("SYS_LANG"));
                    //    wGrid.mfSetGridCellValueList(this.uGridCCSReq3, i, "InspectItemCode", "", "선택", dtItem);

                    //    //// 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
                    //    //if (this.uGridCCSReq3.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "true" &&
                    //    //    this.uGridCCSReq3.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
                    //    //{
                    //    //    this.uGridCCSReq3.Rows[i].Hidden = false;
                    //    //}
                    //    //else
                    //    //{
                    //    //    this.uGridCCSReq3.Rows[i].Hidden = true;
                    //    //}
                    //}
                }

                // 불량유형 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, strProcessCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridCCSReq1, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq2, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                wGrid.mfSetGridColumnValueList(this.uGridCCSReq3, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(this.uGridCCSReq1, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(this.uGridCCSReq2, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(this.uGridCCSReq3, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 지정된 Size만큼 컬럼추가하는 Method
        private void CreateColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, int intBandIndex, String strSampleSizeColKey, string strProductSS, String[] strLastIndexColKey)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // SampleSize가 될 최대값 찾기
                int intSampleMax = 0;
                int intProductSS = 0;
                int inttotMax = 0;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (ReturnIntegerValue(uGrid.Rows[i].Cells[strProductSS].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) > inttotMax)
                    {
                        inttotMax = ReturnIntegerValue(uGrid.Rows[i].Cells[strProductSS].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                        intProductSS = ReturnIntegerValue(uGrid.Rows[i].Cells[strProductSS].Value);
                        intSampleMax = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    }
                }

                if (inttotMax > 99)
                    inttotMax = 99;

                // 상세 SampleSize 만큼 컬럼생성
                for (int i = 1; i <= inttotMax; i++)
                {
                    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Hidden = false;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].MaskInput = "{double:7.5}";
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Width = 70;
                    }
                    else
                    {
                        wGrid.mfSetGridColumn(uGrid, intBandIndex, i.ToString(), "X" + i.ToString(), false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:7.5}", "0.0");
                    }
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].PromptChar = ' ';
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }

                // 추가된 컬럼보다 뒤에 있어야 하는 컬럼들이 있으면 뒤로 보냄
                if (strLastIndexColKey.Length > 0)
                {
                    int LastIndex = uGrid.DisplayLayout.Bands[intBandIndex].Columns.Count;
                    for (int i = 0; i < strLastIndexColKey.Length; i++)
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[strLastIndexColKey[i]].Header.VisiblePosition = LastIndex + i;
                    }
                }

                // Size 만큼의 Cell만 입력 가능하도록나머지는 편집불가처리
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    int ActivateNum = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strProductSS].Value);
                    if (ActivateNum > 99)
                        ActivateNum = 99;

                    for (int j = 1; j <= inttotMax; j++)
                    {
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(j.ToString()))
                        {
                            if (j <= ActivateNum)
                            {
                                uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Empty;
                                //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                uGrid.Rows[i].Cells[j.ToString()].Hidden = false;
                            }
                            else
                            {
                                uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Gainsboro;
                                //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Empty;
                                uGrid.Rows[i].Cells[j.ToString()].Hidden = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형에 맞게 Xn컬럼 설정
        /// </summary>
        private void SetSamplingGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, string strPlantCode, string strSampleColKey)
        {
            try
            {
                //this.uGridCCSReq1.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq1_AfterCellUpdate);
                //this.uGridCCSReq2.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq2_AfterCellUpdate);
                //this.uGridCCSReq3.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq3_AfterCellUpdate);
                this.uGridCCSReq1.EventManager.AllEventsEnabled = false;
                this.uGridCCSReq2.EventManager.AllEventsEnabled = false;
                this.uGridCCSReq3.EventManager.AllEventsEnabled = false;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 선택 DropDown 적용용 DataTable
                int intStart = -99;
                if(uGrid.DisplayLayout.Bands[0].Columns.Exists("1"))
                    intStart = uGrid.DisplayLayout.Bands[0].Columns["1"].Index;
                int intSampleSize = 0;

                // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 50;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                // 합/부 데이터 테이블
                DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                // ToolValue 기본값 데이터 테이블
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);

                DataTable dtToolValue = clsPara.mfReadCCSInspectReqPara_ToolValue(strPlantCode
                    , this.uComboEquip.Value.ToString()
                    , this.uTextPackage.Text
                    , this.uTextCustomerCode.Text);

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    intSampleSize = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleColKey].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;

                    // 계량
                    if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "1")
                    {
                        // 계량인경우 불량수량/검사결과 자동입력
                        uGrid.Rows[i].Cells["FaultQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        uGrid.Rows[i].Cells["FaultQty"].Appearance.BackColor = Color.Gainsboro;
                        uGrid.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        uGrid.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            //uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            if (uGrid.Rows[i].Cells[j].Value == null || uGrid.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                uGrid.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // 계수
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (uGrid.Rows[i].Cells[j].Value == null || uGrid.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                uGrid.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // OK/NG
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "3")
                    {
                        //uGrid.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //uGrid.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(uGrid, i, uGrid.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                            if (uGrid.Rows[i].Cells[j].Value == null || uGrid.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                uGrid.Rows[i].Cells[j].Value = "OK";
                            }
                        }
                    }
                    // 설명
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "4")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            uGrid.Rows[i].Cells[j].Editor = editorString;
                            if (uGrid.Rows[i].Cells[j].Value == null || uGrid.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                uGrid.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    // 선택
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "5")
                    {
                        // 선택항목 가져오는 메소드 호출
                        string strInspectItemCode = uGrid.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect3(strPlantCode
                                                                                            , strInspectItemCode
                                                                                            , this.uTextProductCode.Text
                                                                                            , this.uTextNowProcessCode.Text
                                                                                            , m_resSys.GetString("SYS_LANG"));
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            //uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            wGrid.mfSetGridCellValueList(uGrid, i, uGrid.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                            if (uGrid.Rows[i].Cells[j].Value == null || uGrid.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                uGrid.Rows[i].Cells[j].Value = "";
                            }

                            if (dtToolValue.Rows.Count > 0)
                            {
                                if (strInspectItemCode.Equals("II02060704"))
                                {
                                    uGrid.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["HEAD"].ToString();
                                }
                                else if (strInspectItemCode.Equals("II02060703"))
                                {
                                    uGrid.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["STAGE"].ToString();
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    for (int j = intStart; j < intStart + intSampleSize; j++)
                    //    {
                    //        uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //        uGrid.Rows[i].Cells[j].Appearance.BackColor = Color.Gainsboro;
                    //        if (uGrid.Rows[i].Cells[j].Value == null)
                    //        {
                    //            if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    //                uGrid.Rows[i].Cells[j].Value = 0;
                    //            else
                    //                uGrid.Rows[i].Cells[j].Value = "";
                    //        }
                    //    }
                    //}
                }
                //this.uGridCCSReq1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq1_AfterCellUpdate);
                //this.uGridCCSReq2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq2_AfterCellUpdate);
                //this.uGridCCSReq3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCCSReq3_AfterCellUpdate);
                this.uGridCCSReq1.EventManager.AllEventsEnabled = true;
                this.uGridCCSReq2.EventManager.AllEventsEnabled = true;
                this.uGridCCSReq3.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        #region 테이블 조회 Method

        // 헤더 정보 조회 
        private void SearchInspectReqHDetail(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHaeder;
                QRPBrowser brwChannel = new QRPBrowser();

                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHaeder = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHaeder);
                }
                else
                {
                    clsHaeder = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                DataTable dtHeader = clsHaeder.mfReadCCSInspectReqHDetail(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"].ToString();              //공장코드
                this.uTextReqNo.Text = dtHeader.Rows[0]["ReqNo"].ToString();                    //관리번호
                this.uTextCustomerCode.Text = dtHeader.Rows[0]["CustomerCode"].ToString();      //거래처코드
                this.uTextCustomerName.Text = dtHeader.Rows[0]["CustomerName"].ToString();      //거래처명
                this.uTextWorkProcessCode.Text = dtHeader.Rows[0]["WorkProcessCode"].ToString();//작업공정코드
                this.uTextWorkProcessName.Text = dtHeader.Rows[0]["WorkProcessName"].ToString();//작업공정명
                this.uTextNowProcessCode.Text = dtHeader.Rows[0]["NowProcessCode"].ToString();  //현재공정코드
                this.uTextNowProcessName.Text = dtHeader.Rows[0]["NowProcessName"].ToString();  //현재공정명
                this.uTextEquipCode.Text = dtHeader.Rows[0]["EquipCode"].ToString();            //설비코드
                this.uTextEquipName.Text = dtHeader.Rows[0]["EquipName"].ToString();            //설비명
                this.uTextProductCode.Text = dtHeader.Rows[0]["ProductCode"].ToString();        //제품코드
                this.uTextProductName.Text = dtHeader.Rows[0]["ProductName"].ToString();        //제품명
                this.uTextCustomerProductCode.Text = dtHeader.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextPackage.Text = dtHeader.Rows[0]["PACKAGE"].ToString();                //패키디

                // 설비콤보 설정 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlant.Value.ToString()
                                                                                    , this.uTextNowProcessCode.Text
                                                                                    , m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                this.uComboEquip.Appearance.BackColor = Color.Gainsboro;
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                this.uComboEquip.Value = dtHeader.Rows[0]["EquipCode"].ToString(); //설비코드

                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                brwChannel.mfCredentials(clsHeader);

                // 기존에 존재하는 표준번호가 있는지 확인하는 Method 호출
                DataTable dtInsSpec = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode, this.uTextPackage.Text, this.uTextCustomerCode.Text);
                if (dtInsSpec.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsSpec.Rows.Count; i++)
                    {
                        this.uTextStdNumber.Text = dtInsSpec.Rows[i]["StdNumber"].ToString() + dtInsSpec.Rows[i]["StdSeq"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Lot 정보 조회 
        private void SearchInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);
                }
                else
                {
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                DataTable dtLot = clsLot.mfReadCCSInspectReqLot_PSTS(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        // CCS의뢰유형 콤보박스 설정 메소드 호출
                        comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, i + 1);

                        this.uComboStackSeq1.ValueChanged -= new EventHandler(this.uComboStackSeq1_ValueChanged);
                        if (dtLot.Rows[i]["StackSeq"] != null && dtLot.Rows[i]["StackSeq"] != DBNull.Value && !dtLot.Rows[i]["StackSeq"].ToString().Equals(string.Empty))
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", dtLot.Rows[i]["StackSeq"].ToString(), i + 1);
                        }
                        else
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", i + 1);
                        }
                        this.uComboStackSeq1.ValueChanged += new EventHandler(this.uComboStackSeq1_ValueChanged);

                        this.uTextReqLotSeq1.Text = dtLot.Rows[i]["ReqLotSeq"].ToString();
                        this.uComboCCSReqType1.Value = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextLotNo1.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uTextLotNo1.ReadOnly = true;
                        this.uTextReqUserID1.Text = dtLot.Rows[i]["ReqUserId"].ToString();
                        this.uTextReqUserName1.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uTextStepUserID1.Text = dtLot.Rows[i]["StepUserId"].ToString();
                        this.uTextStepUserName1.Text = dtLot.Rows[i]["StepUserName"].ToString();
                        this.uDateReqDate1.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextCauseReason1.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction1.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uTextEtcDesc1.Text = dtLot.Rows[i]["EtcDesc"].ToString();

                        this.uTextReqTime1.Text = dtLot.Rows[i]["ReqTime"].ToString();

                        this.uDateReceiptDate1.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime1.Text = dtLot.Rows[i]["ReceiptTime"].ToString();

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE1.Visible = true;
                            this.uCheckCHASE1_All.Visible = true;
                            this.uCheckCHASE1_1.Visible = true;
                            this.uCheckCHASE1_2.Visible = true;
                            this.uCheckCHASE1_3.Visible = true;
                            this.uCheckCHASE1_4.Visible = true;
                            this.uCheckCHASE1_5.Visible = true;
                            this.uCheckCHASE1_6.Visible = true;

                            this.uCheckCHASE1_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE1_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE1_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE1_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE1_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE1_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE1_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource1.Visible = true;
                            this.uComboProgramSource1.Visible = true;

                            this.uComboProgramSource1.Text = dtLot.Rows[i]["ProgramSource"].ToString();
                            this.uComboProgramSource1.Enabled = false;
                        }
                        else
                        {
                            this.uLabelProgramSource1.Visible = false;
                            this.uComboProgramSource1.Visible = false;
                        }

                        if (dtLot.Rows[i]["ReqSaveFlag"].ToString().Equals("F") && this.uComboStackSeq1.Items.Count > 1)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001131", "M001130"
                                                                    , Infragistics.Win.HAlign.Center);
                        }

                        //1차탭 아이템 검사항목 정보 조회 --> 필요없을듯 추후 상태 보고 삭제 요망
                        //SearchInspectReqItem(strPlantCode, strReqNo, strReqSeq, i+1);
                        //1차탭 아이템 검사값 정보 조회
                        SearchInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, i + 1);
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        this.uTextComplete1.Text = dtLot.Rows[i]["CompleteFlag"].ToString();
                        this.uTextInspectResult1.Text = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        this.uTextMESLockingFlag1.Text = dtLot.Rows[i]["MESLockFlag"].ToString();                        

                        this.uTextLotNo1.Enabled = false;
                        this.uComboCCSReqType1.Enabled = false;

                        if (this.uTextInspectResult1.Text.Equals("NG") && this.uTextComplete1.Text.Equals("T"))
                        {
                            // Tab 활성화
                            this.uTabCCS.Tabs[1].Enabled = true;
                            this.uTab1.Tabs["Quality"].Visible = true;

                            SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);

                            this.uTextCauseReason1.Appearance.BackColor = Color.Empty;
                            this.uTextCauseReason1.ReadOnly = false;
                            this.uTextCorrectAction1.Appearance.BackColor = Color.Empty;
                            this.uTextCorrectAction1.ReadOnly = false;

                            //this.uComboEquip.Enabled = false;
                        }

                        if (this.uTextComplete1.Text.Equals("T"))
                        {
                            this.uGridCCSReq1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGridPara1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uTextReqUserID1.Enabled = false;
                            this.uTextStepUserID1.Enabled = false;
                            this.uComboStackSeq1.Enabled = false;

                            this.uLabelCHASE1.Enabled = false;
                            this.uCheckCHASE1_1.Enabled = false;
                            this.uCheckCHASE1_2.Enabled = false;
                            this.uCheckCHASE1_3.Enabled = false;
                            this.uCheckCHASE1_4.Enabled = false;
                            this.uCheckCHASE1_5.Enabled = false;
                            this.uCheckCHASE1_6.Enabled = false;
                            this.uCheckCHASE1_All.Enabled = false;
                        }
                        else
                        {
                            this.uGridCCSReq1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGridPara1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uTextReqUserID1.Enabled = true;
                            this.uTextStepUserID1.Enabled = true;
                            this.uComboStackSeq1.Enabled = true;

                            this.uLabelCHASE1.Enabled = true;
                            this.uCheckCHASE1_1.Enabled = true;
                            this.uCheckCHASE1_2.Enabled = true;
                            this.uCheckCHASE1_3.Enabled = true;
                            this.uCheckCHASE1_4.Enabled = true;
                            this.uCheckCHASE1_5.Enabled = true;
                            this.uCheckCHASE1_6.Enabled = true;
                            this.uCheckCHASE1_All.Enabled = true;
                        }
                    }
                    else if (i == 1)
                    {
                        // CCS의뢰유형 콤보박스 설정 메소드 호출
                        comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, i + 1);

                        this.uComboStackSeq2.ValueChanged -= new EventHandler(this.uComboStackSeq2_ValueChanged);
                        if (dtLot.Rows[i]["StackSeq"] != null && dtLot.Rows[i]["StackSeq"] != DBNull.Value && !dtLot.Rows[i]["StackSeq"].ToString().Equals(string.Empty))
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", dtLot.Rows[i]["StackSeq"].ToString(), i + 1);
                        }
                        else
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", i + 1);
                        }
                        this.uComboStackSeq2.ValueChanged += new EventHandler(this.uComboStackSeq2_ValueChanged);

                        //ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", i + 1);

                        this.uTextReqLotSeq2.Text = dtLot.Rows[i]["ReqLotSeq"].ToString();
                        this.uTextLotNo2.ReadOnly = true;
                        this.uComboCCSReqType2.Value = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextLotNo2.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uTextReqUserID2.Text = dtLot.Rows[i]["ReqUserId"].ToString();
                        this.uTextReqUserName2.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uTextStepUserID2.Text = dtLot.Rows[i]["StepUserId"].ToString();
                        this.uTextStepUserName2.Text = dtLot.Rows[i]["StepUserName"].ToString();
                        this.uDateReqDate2.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextCauseReason2.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction2.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uTextEtcDesc2.Text = dtLot.Rows[i]["EtcDesc"].ToString();

                        this.uTextReqTime2.Text = dtLot.Rows[i]["ReqTime"].ToString();

                        this.uDateReceiptDate2.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime2.Text = dtLot.Rows[i]["ReceiptTime"].ToString();

                        //if (dtLot.Rows[i]["StackSeq"] != null && dtLot.Rows[i]["StackSeq"] != DBNull.Value && !dtLot.Rows[i]["StackSeq"].ToString().Equals(string.Empty))
                        //{
                        //    this.uComboStackSeq2.ValueChanged -= new EventHandler(this.uComboStackSeq2_ValueChanged);
                        //    this.uComboStackSeq2.Value = dtLot.Rows[i]["StackSeq"].ToString();
                        //    this.uComboStackSeq2.ValueChanged += new EventHandler(this.uComboStackSeq2_ValueChanged);
                        //}

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE2.Visible = true;
                            this.uCheckCHASE2_All.Visible = true;
                            this.uCheckCHASE2_1.Visible = true;
                            this.uCheckCHASE2_2.Visible = true;
                            this.uCheckCHASE2_3.Visible = true;
                            this.uCheckCHASE2_4.Visible = true;
                            this.uCheckCHASE2_5.Visible = true;
                            this.uCheckCHASE2_6.Visible = true;

                            this.uCheckCHASE2_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE2_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE2_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE2_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE2_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE2_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE2_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource2.Visible = true;
                            this.uComboProgramSource2.Visible = true;

                            this.uComboProgramSource2.Text = dtLot.Rows[i]["ProgramSource"].ToString();
                            this.uComboProgramSource2.Enabled = false;
                        }
                        else
                        {
                            this.uLabelProgramSource2.Visible = false;
                            this.uComboProgramSource2.Visible = false;
                        }

                        if (dtLot.Rows[i]["ReqSaveFlag"].ToString().Equals("F") && this.uComboStackSeq2.Items.Count > 1)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001131", "M001130"
                                                                    , Infragistics.Win.HAlign.Center);
                        }

                        //2차탭 아이템 검사항목 정보 조회 --> 필요없을듯 추후 상태 보고 삭제 요망
                        //SearchInspectReqItem(strPlantCode, strReqNo, strReqSeq, i + 1);
                        //2차탭 아이템 검사값 정보 조회
                        SearchInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, i + 1);
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        this.uTextComplete2.Text = dtLot.Rows[i]["CompleteFlag"].ToString();
                        this.uTextInspectResult2.Text = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        this.uTextMESLockingFlag2.Text = dtLot.Rows[i]["MESLockFlag"].ToString();

                        this.uTextLotNo2.Enabled = false;
                        this.uComboCCSReqType2.Enabled = false;

                        if (this.uTextInspectResult2.Text.Equals("NG") && this.uTextComplete2.Text.Equals("T"))
                        {
                            // Tab 활성화

                            this.uTabCCS.Tabs[2].Enabled = true;
                            this.uTab2.Tabs["Quality"].Visible = true;

                            SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);

                            this.uTextCauseReason2.Appearance.BackColor = Color.Empty;
                            this.uTextCauseReason2.ReadOnly = false;
                            this.uTextCorrectAction2.Appearance.BackColor = Color.Empty;
                            this.uTextCorrectAction2.ReadOnly = false;
                        }
                        if (this.uTextComplete2.Text.Equals("T"))
                        {
                            this.uGridCCSReq2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGridPara2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uTextReqUserID2.Enabled = false;
                            this.uTextStepUserID2.Enabled = false;
                            this.uComboStackSeq2.Enabled = false;

                            this.uLabelCHASE2.Enabled = false;
                            this.uCheckCHASE2_1.Enabled = false;
                            this.uCheckCHASE2_2.Enabled = false;
                            this.uCheckCHASE2_3.Enabled = false;
                            this.uCheckCHASE2_4.Enabled = false;
                            this.uCheckCHASE2_5.Enabled = false;
                            this.uCheckCHASE2_6.Enabled = false;
                            this.uCheckCHASE2_All.Enabled = false;
                        }
                        else
                        {
                            this.uGridCCSReq2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGridPara2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uTextReqUserID2.Enabled = true;
                            this.uTextStepUserID2.Enabled = true;
                            this.uComboStackSeq2.Enabled = true;

                            this.uLabelCHASE2.Enabled = true;
                            this.uCheckCHASE2_1.Enabled = true;
                            this.uCheckCHASE2_2.Enabled = true;
                            this.uCheckCHASE2_3.Enabled = true;
                            this.uCheckCHASE2_4.Enabled = true;
                            this.uCheckCHASE2_5.Enabled = true;
                            this.uCheckCHASE2_6.Enabled = true;
                            this.uCheckCHASE2_All.Enabled = true;
                        }
                    }
                    else if (i == 2)
                    {
                        // CCS의뢰유형 콤보박스 설정 메소드 호출
                        comboCCSReqTypeInfo(this.uComboPlant.Value.ToString(), this.uTextNowProcessCode.Text, i + 1);
                        //ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", i + 1);
                        this.uComboStackSeq3.ValueChanged -= new EventHandler(this.uComboStackSeq3_ValueChanged);
                        if (dtLot.Rows[i]["StackSeq"] != null && dtLot.Rows[i]["StackSeq"] != DBNull.Value && !dtLot.Rows[i]["StackSeq"].ToString().Equals(string.Empty))
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", dtLot.Rows[i]["StackSeq"].ToString(), i + 1);
                        }
                        else
                        {
                            ComboStackSeq(this.uComboPlant.Value.ToString(), this.uTextProductCode.Text, this.uTextNowProcessCode.Text, "P", "", i + 1);
                        }
                        this.uComboStackSeq3.ValueChanged += new EventHandler(this.uComboStackSeq3_ValueChanged);


                        this.uTextReqLotSeq3.Text = dtLot.Rows[i]["ReqLotSeq"].ToString();
                        this.uTextLotNo3.ReadOnly = true;
                        this.uComboCCSReqType3.Value = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextLotNo3.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uTextReqUserID3.Text = dtLot.Rows[i]["ReqUserId"].ToString();
                        this.uTextReqUserName3.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uTextStepUserID3.Text = dtLot.Rows[i]["StepUserId"].ToString();
                        this.uTextStepUserName3.Text = dtLot.Rows[i]["StepUserName"].ToString();
                        this.uDateReqDate3.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextCauseReason3.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction3.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uTextEtcDesc3.Text = dtLot.Rows[i]["EtcDesc"].ToString();

                        this.uTextReqTime3.Text = dtLot.Rows[i]["ReqTime"].ToString();

                        this.uDateReceiptDate3.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime3.Text = dtLot.Rows[i]["ReceiptTime"].ToString();

                        //if (dtLot.Rows[i]["StackSeq"] != null && dtLot.Rows[i]["StackSeq"] != DBNull.Value && !dtLot.Rows[i]["StackSeq"].ToString().Equals(string.Empty))
                        //{
                        //    this.uComboStackSeq3.ValueChanged -= new EventHandler(this.uComboStackSeq3_ValueChanged);
                        //    this.uComboStackSeq3.Value = dtLot.Rows[i]["StackSeq"].ToString();
                        //    this.uComboStackSeq3.ValueChanged += new EventHandler(this.uComboStackSeq3_ValueChanged);
                        //}

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE3.Visible = true;
                            this.uCheckCHASE3_All.Visible = true;
                            this.uCheckCHASE3_1.Visible = true;
                            this.uCheckCHASE3_2.Visible = true;
                            this.uCheckCHASE3_3.Visible = true;
                            this.uCheckCHASE3_4.Visible = true;
                            this.uCheckCHASE3_5.Visible = true;
                            this.uCheckCHASE3_6.Visible = true;

                            this.uCheckCHASE3_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE3_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE3_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE3_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE3_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE3_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE3_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource3.Visible = true;
                            this.uComboProgramSource3.Visible = true;

                            this.uComboProgramSource3.Text = dtLot.Rows[i]["ProgramSource"].ToString();
                            this.uComboProgramSource3.Enabled = false;
                        }
                        else
                        {
                            this.uLabelProgramSource3.Visible = false;
                            this.uComboProgramSource3.Visible = false;
                        }

                        if (dtLot.Rows[i]["ReqSaveFlag"].ToString().Equals("F") && this.uComboStackSeq3.Items.Count > 1)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001131", "M001130"
                                                                    , Infragistics.Win.HAlign.Center);
                        }

                        //3차탭 아이템 검사항목 정보 조회 --> 필요없을듯 추후 상태 보고 삭제 요망
                        //SearchInspectReqItem(strPlantCode, strReqNo, strReqSeq, i + 1);
                        //3차탭 아이템 검사값 정보 조회
                        SearchInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, i + 1);
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        this.uTextComplete3.Text = dtLot.Rows[i]["CompleteFlag"].ToString();
                        this.uTextInspectResult3.Text = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        this.uTextMESLockingFlag3.Text = dtLot.Rows[i]["MESLockFlag"].ToString();

                        this.uTextLotNo3.Enabled = false;
                        this.uComboCCSReqType3.Enabled = false;

                        if (this.uTextInspectResult3.Text.Equals("NG") && this.uTextComplete3.Text.Equals("T"))
                        {
                            this.uTab3.Tabs["Quality"].Visible = true;

                            SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);

                            this.uTextCauseReason3.Appearance.BackColor = Color.Empty;
                            this.uTextCauseReason3.ReadOnly = false;
                            this.uTextCorrectAction3.Appearance.BackColor = Color.Empty;
                            this.uTextCorrectAction3.ReadOnly = false;
                        }
                        if (this.uTextComplete3.Text.Equals("T"))
                        {
                            this.uGridCCSReq3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGridPara3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uTextReqUserID3.Enabled = false;
                            this.uTextStepUserID3.Enabled = false;
                            this.uComboStackSeq3.Enabled = false;

                            this.uLabelCHASE3.Enabled = false;
                            this.uCheckCHASE3_1.Enabled = false;
                            this.uCheckCHASE3_2.Enabled = false;
                            this.uCheckCHASE3_3.Enabled = false;
                            this.uCheckCHASE3_4.Enabled = false;
                            this.uCheckCHASE3_5.Enabled = false;
                            this.uCheckCHASE3_6.Enabled = false;
                            this.uCheckCHASE3_All.Enabled = false;
                        }
                        else
                        {
                            this.uGridCCSReq3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGridPara3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uTextReqUserID3.Enabled = true;
                            this.uTextStepUserID3.Enabled = true;
                            this.uComboStackSeq3.Enabled = true;

                            this.uLabelCHASE3.Enabled = true;
                            this.uCheckCHASE3_1.Enabled = true;
                            this.uCheckCHASE3_2.Enabled = true;
                            this.uCheckCHASE3_3.Enabled = true;
                            this.uCheckCHASE3_4.Enabled = true;
                            this.uCheckCHASE3_5.Enabled = true;
                            this.uCheckCHASE3_6.Enabled = true;
                            this.uCheckCHASE3_All.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //// Item 정보 조회 1  Method --> 필요없음. TEST 완료 후 삭제 요망
        //private void SearchInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        //{
        //    try
        //    {
        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);                
        //        // ItemTable 조회
        //        QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
        //        if (m_bolDebugMode == false)
        //        {
        //            QRPBrowser brwChannel = new QRPBrowser();
        //            brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
        //            clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
        //            brwChannel.mfCredentials(clsItem);
        //        }
        //        else
        //        {
        //            clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
        //        }

        //        // 해당 차수 설정(1차, 2차 , 3차)                
        //        if (intReqLotSeq == 1)
        //        {
        //            DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
        //            this.uGridCCSReq1.DataSource = dtItem;
        //            this.uGridCCSReq1.DataBind();

        //            // 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
        //            for (int i = 0; i < uGridCCSReq1.Rows.Count; i++)
        //            {
        //                if (this.uGridCCSReq1.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "T" &&
        //                    this.uGridCCSReq1.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
        //                {
        //                    this.uGridCCSReq1.Rows[i].Hidden = false;
        //                }
        //                else
        //                {
        //                    this.uGridCCSReq1.Rows[i].Hidden = true;
        //                }
        //            }
        //        }
        //        else if (intReqLotSeq == 2)
        //        {
        //            DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
        //            this.uGridCCSReq2.DataSource = dtItem;
        //            this.uGridCCSReq2.DataBind();

        //            // 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
        //            for (int i = 0; i < uGridCCSReq2.Rows.Count; i++)
        //            {
        //                if (this.uGridCCSReq2.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "T" &&
        //                    this.uGridCCSReq2.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
        //                {
        //                    this.uGridCCSReq2.Rows[i].Hidden = false;
        //                }
        //                else
        //                {
        //                    this.uGridCCSReq2.Rows[i].Hidden = true;
        //                }
        //            }
        //        }
        //        else if (intReqLotSeq == 3)
        //        {
        //            DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
        //            this.uGridCCSReq3.DataSource = dtItem;
        //            this.uGridCCSReq3.DataBind();

        //            // 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
        //            for (int i = 0; i < uGridCCSReq3.Rows.Count; i++)
        //            {
        //                if (this.uGridCCSReq3.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "T" &&
        //                    this.uGridCCSReq3.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
        //                {
        //                    this.uGridCCSReq3.Rows[i].Hidden = false;
        //                }
        //                else
        //                {
        //                    this.uGridCCSReq3.Rows[i].Hidden = true;
        //                }
        //            }
        //        }                
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        // ITem 정보 조회 2 (검사값 데이터 조회) Method
        private void SearchInspectReqItemValue(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                Infragistics.Win.UltraWinEditors.UltraComboEditor uCombo = new Infragistics.Win.UltraWinEditors.UltraComboEditor();

                if (intReqLotSeq == 1)
                {
                    m_grdCCS = this.uGridCCSReq1;
                    uCombo = this.uComboStackSeq1;
                }
                else if (intReqLotSeq == 2)
                {
                    m_grdCCS = this.uGridCCSReq2;
                    uCombo = this.uComboStackSeq2;
                }
                else if (intReqLotSeq == 3)
                {
                    m_grdCCS = this.uGridCCSReq3;
                    uCombo = this.uComboStackSeq3;
                }

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                DataTable dtItem = clsItem.mfReadCCSInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, "P", m_resSys.GetString("SYS_LANG"));

                //// 자동의뢰된 경우 Item정보가 저장이 안되어 있으므로 Item정보를 확인하기 위해 CheckStdNumber 메소드로 이동한다.
                //if (dtItem.Rows.Count <= 0)
                //{
                //    string strStackSeq = uCombo.Value.ToString();
                //    CheckStdNumber(intReqLotSeq, strStackSeq);
                //    return;
                //}

                this.m_grdCCS.DataSource = dtItem;
                this.m_grdCCS.DataBind();

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.DisplayLayout.Bands[0].Columns.ClearUnbound();
                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.m_grdCCS, 0, "SampleSize", "ProductItemSS", strLastColKey);

                    SetSamplingGridColumn(this.m_grdCCS, strPlantCode, "ProductItemSS");
                }
                WinGrid wGrid = new WinGrid();
                // 불량유형 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 품질 Item(검사값) 정보 조회 Method
        private void SearchInspectReqItemValue_Qualaity(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //Infragistics.Win.UltraWinGrid.UltraGrid m_grdCCS = new Infragistics.Win.UltraWinGrid.UltraGrid();

                if (intReqLotSeq == 1)
                {
                    m_grdCCS = this.uGrid1_2;
                }
                else if (intReqLotSeq == 2)
                {
                    m_grdCCS = this.uGrid2_2;
                }
                else if (intReqLotSeq == 3)
                {
                    m_grdCCS = this.uGrid3_2;
                }

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                DataTable dtItem = clsItem.mfReadCCSInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, "Q", m_resSys.GetString("SYS_LANG"));

                this.m_grdCCS.DataSource = dtItem;
                this.m_grdCCS.DataBind();

                WinGrid wGrid = new WinGrid();
                // 불량유형 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.DisplayLayout.Bands[0].Columns.ClearUnbound();

                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(m_grdCCS, 0, "SampleSize", "QualityItemSS", strLastColKey);

                    SetSamplingGridColumn(m_grdCCS, strPlantCode, "QualityItemSS");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 가동조건 테이블 검사

        private void SearchInspectReqPara(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                if (intReqLotSeq == 1)
                {
                    uGrid = this.uGridPara1;
                }
                else if (intReqLotSeq == 2)
                {
                    uGrid = this.uGridPara2;
                }
                else if (intReqLotSeq == 3)
                {
                    uGrid = this.uGridPara3;
                }

                // BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                //QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                //brwChannel.mfCredentials(clsPara);

                string strProductCode = this.uTextProductCode.Text;
                string strNowProcessCode = this.uTextNowProcessCode.Text;
                string strEquipCode = this.uComboEquip.Value.ToString();
                string strPackage = this.uTextPackage.Text;
                string strCoustormer = this.uTextCustomerCode.Text;

                //DataTable dtPara = clsPara.mfReadCCSInspectReqPara_PSTS(strPlantCode, strReqNo, strReqSeq, intReqLotSeq
                //                                                        , strProductCode, strNowProcessCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                DataTable dtPara = SetParaInfo(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, strProductCode, strPackage, strNowProcessCode, strEquipCode, strCoustormer, m_resSys.GetString("SYS_LANG"));

                uGrid.DataSource = dtPara;
                uGrid.DataBind();

                ArryCCSPara(intReqLotSeq);

                // 가동조건이 'DIETEMP' 인 경우 텍스트 입력
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 40;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells["CCSparameterCode"].Value.ToString().Equals("DIETEMP"))
                    {
                        uGrid.Rows[i].Cells["InspectValue"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                        uGrid.Rows[i].Cells["InspectValue"].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        // MaxLength 지정방법;;;;
                        uGrid.Rows[i].Cells["InspectValue"].Editor = editorString;
                        if (uGrid.Rows[i].Cells["InspectValue"].Value == null)
                        {
                            uGrid.Rows[i].Cells["InspectValue"].Value = "";
                        }
                    }

                    double dbUSL = 0;
                    double dbLSL = 0;
                    double dbValue = 0;

                    if (!uGrid.Rows[i].Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                        dbLSL = Convert.ToDouble(uGrid.Rows[i].Cells["LOWERLIMIT"].Value.ToString());
                    if (!uGrid.Rows[i].Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                        dbUSL = Convert.ToDouble(uGrid.Rows[i].Cells["UPPERLIMIT"].Value.ToString());
                    if (!uGrid.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                        dbValue = Convert.ToDouble(uGrid.Rows[i].Cells["InspectValue"].Value.ToString());

                    if (uGrid.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                        uGrid.Rows[i].Appearance.BackColor = Color.White;
                    //else if (dbValue < dbLSL || dbValue > dbUSL)
                    //    uGrid.Rows[i].Appearance.BackColor = Color.Tomato;
                    else if (uGrid.Rows[i].Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        if (dbValue > dbUSL)
                            uGrid.Rows[i].Appearance.BackColor = Color.Tomato;
                        else
                            uGrid.Rows[i].Appearance.BackColor = Color.White;
                    }
                    else if (uGrid.Rows[i].Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        if (dbValue < dbLSL)
                            uGrid.Rows[i].Appearance.BackColor = Color.Tomato;
                        else
                            uGrid.Rows[i].Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        if (dbValue < dbLSL || dbValue > dbUSL)
                            uGrid.Rows[i].Appearance.BackColor = Color.Tomato;
                        else
                            uGrid.Rows[i].Appearance.BackColor = Color.White;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 리스트 더블클릭시 상세정보 조회
        private void uGridCCSReqList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 화면 초기화
                InitClear();
                // 더블클릭된 행 고정
                e.Row.Fixed = true;
                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strReqNo = e.Row.Cells["ReqNo"].Value.ToString().Substring(0, 8);
                String strReqSeq = e.Row.Cells["ReqNo"].Value.ToString().Substring(8, 4);
                int intReqLotSeq = ReturnIntegerValue(e.Row.Cells["ReqLotSeq"].Value.ToString());

                this.uTextCreateType.Text = e.Row.Cells["CCSCreateType"].Value.ToString(); //CreateType
                this.uTextMESTFlag.Text = e.Row.Cells["MESTFlag"].Value.ToString();  //MESTFlag
                this.uTextCompleteFlag.Text = e.Row.Cells["CompleteFlag"].Value.ToString();
                this.uTextInspectResultFlag.Text = e.Row.Cells["InspectResult"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 헤더 상세조회 Method 호출
                SearchInspectReqHDetail(strPlantCode, strReqNo, strReqSeq);

                // Lot Grid 데이터 조회 Method 호출
                SearchInspectReqLot(strPlantCode, strReqNo, strReqSeq);

                //WinGrid grd = new WinGrid();
                //grd.mfSetAutoResizeColWidth(this.uGridCCSReq1, 0);
                //grd.mfSetAutoResizeColWidth(this.uGridCCSReq2, 0);
                //grd.mfSetAutoResizeColWidth(this.uGridCCSReq3, 0);
                //grd.mfSetAutoResizeColWidth(this.uGridPara1, 0);
                //grd.mfSetAutoResizeColWidth(this.uGridPara2, 0);
                //grd.mfSetAutoResizeColWidth(this.uGridPara3, 0);
                //grd.mfSetAutoResizeColWidth(this.uGrid1_2, 0);
                //grd.mfSetAutoResizeColWidth(this.uGrid2_2, 0);
                //grd.mfSetAutoResizeColWidth(this.uGrid3_2, 0);
                // Item Grid 데이터 조회 Method 호출 --> 위 Lot Grid 데이터 조회 Method 호출 부분에서 진행함.
                //SearchInspectReqItem(strPlantCode, strReqNo, strReqSeq);                
                // Item Grid(Value) 데이터 조회 Method 호출 --> 위 Lot Grid 데이터 조회 Method 호출 부분에서 진행함.
                //SearchInspectReqItemValue(strPlantCode, strReqNo, strReqSeq);

                if (uTextLotNo2.ReadOnly == true)
                {
                    this.uTextCauseReason1.ReadOnly = true;
                    this.uTextCauseReason1.Appearance.BackColor = Color.Gainsboro;
                    this.uTextCorrectAction1.ReadOnly = true;
                    this.uTextCorrectAction1.Appearance.BackColor = Color.Gainsboro;
                }
                else
                {
                    this.uTextCauseReason1.ReadOnly = false;
                    this.uTextCauseReason1.Appearance.BackColor = Color.White;
                    this.uTextCorrectAction1.ReadOnly = false;
                    this.uTextCorrectAction1.Appearance.BackColor = Color.White;
                }

                if (this.uTextLotNo3.ReadOnly == true)
                {
                    this.uTextCauseReason2.ReadOnly = true;
                    this.uTextCauseReason2.Appearance.BackColor = Color.Gainsboro;
                    this.uTextCorrectAction2.ReadOnly = true;
                    this.uTextCorrectAction2.Appearance.BackColor = Color.Gainsboro;
                }
                if (this.uTextCauseReason3.Text.Equals(string.Empty) && this.uTextCorrectAction3.Text.Equals(string.Empty))
                {
                    this.uTextCauseReason3.ReadOnly = false;
                    this.uTextCauseReason3.Appearance.BackColor = Color.White;
                    this.uTextCorrectAction3.ReadOnly = false;
                    this.uTextCorrectAction3.Appearance.BackColor = Color.White;
                }
                else
                {
                    this.uTextCauseReason3.ReadOnly = true;
                    this.uTextCauseReason3.Appearance.BackColor = Color.Gainsboro;
                    this.uTextCorrectAction3.ReadOnly = true;
                    this.uTextCorrectAction3.Appearance.BackColor = Color.Gainsboro;
                }

                if (this.uTabCCS.Tabs[1].Enabled == true)
                {
                    for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                    {
                        if (this.uGridCCSReq1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGridCCSReq1.Rows[i].Hidden = true;
                        }
                        else
                            this.uGridCCSReq1.Rows[i].Hidden = false;
                    }
                    for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                    {
                        if (this.uGrid1_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid1_2.Rows[i].Hidden = true;
                        }
                        else
                            this.uGrid1_2.Rows[i].Hidden = false;
                    }
                    this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
                else
                    this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                if (this.uTabCCS.Tabs[2].Enabled == true)
                {
                    for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                    {
                        if (this.uGridCCSReq2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGridCCSReq2.Rows[i].Hidden = true;
                        }
                        else
                            this.uGridCCSReq2.Rows[i].Hidden = false;
                    }
                    for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                    {
                        if (this.uGrid2_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid2_2.Rows[i].Hidden = true;
                        }
                        else
                            this.uGrid2_2.Rows[i].Hidden = false;
                    }
                    this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
                else
                    this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                if (this.uTabCCS.Tabs[2].Enabled == true && this.uTextLotNo3.Text != string.Empty)
                {
                    for (int i = 0; i < this.uGridCCSReq3.Rows.Count; i++)
                    {
                        if (this.uGridCCSReq3.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGridCCSReq3.Rows[i].Hidden = true;
                        }
                        else
                            this.uGridCCSReq3.Rows[i].Hidden = false;
                    }
                    for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                    {
                        if (this.uGrid3_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid3_2.Rows[i].Hidden = true;
                        }
                        else
                            this.uGrid3_2.Rows[i].Hidden = false;
                    }
                    this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
                else
                    this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uTabCCS.Tabs[intReqLotSeq - 1].Selected = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 의뢰사유정보 Load 
        private void comboCCSReqTypeInfo(String strPlantCode, String strProcessCode, int intReqLotSeq)
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtCCSReqType = new DataTable();

                if (strPlantCode != "")
                {
                    //BL연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
                    QRPMAS.BL.MASQUA.CCSReqType clsCCSReaType = new QRPMAS.BL.MASQUA.CCSReqType();
                    brwChannel.mfCredentials(clsCCSReaType);

                    dtCCSReqType = clsCCSReaType.mfReadMASCCSReqType_CCSInspect(strPlantCode, strProcessCode, m_resSys.GetString("SYS_LANG"));
                    //dtCCSReqType = clsCCSReaType.mfReadMASCCSReqTypeCombo(strPlantCode, strProcessCode, m_resSys.GetString("SYS_LANG"));
                }

                if (intReqLotSeq == 1)
                {
                    this.uComboCCSReqType1.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboCCSReqType1, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReqType);
                }
                else if (intReqLotSeq == 2)
                {
                    this.uComboCCSReqType2.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboCCSReqType2, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, this.uComboCCSReqType1.Value.ToString()
                        , "", "선택"
                        , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReqType);

                    this.uComboCCSReqType2.ReadOnly = true;
                }
                else if (intReqLotSeq == 3)
                {
                    this.uComboCCSReqType3.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboCCSReqType3, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, this.uComboCCSReqType1.Value.ToString()
                        , "", "선택"
                        , "CCSReqTypeCode", "CCSReqTypeName", dtCCSReqType);

                    this.uComboCCSReqType3.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 스택콤보설정 메소드
        private void ComboStackSeq(string strPlantCode, string strProductCode, string strProcessCode, string strReqItemType, string strStackSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                DataTable dtStack = clsLot.mfReadCCSInspectReq_ForStackCombo(strPlantCode, strProductCode, strProcessCode, strReqItemType, "");

                if (intReqLotSeq == 1)
                {
                    this.uComboStackSeq1.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboStackSeq1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, strStackSeq, "", "전체"
                        , "StackCode", "StackName", dtStack);

                    for (int i = 1; i < this.uComboStackSeq1.Items.Count; i++)
                    {
                        if (this.uComboStackSeq1.Items[0].DataValue.Equals(this.uComboStackSeq1.Items[i].DataValue))
                        {
                            this.uComboStackSeq1.Items.Remove(i);
                            i--;
                        }
                    }
                }
                else if (intReqLotSeq == 2)
                {
                    this.uComboStackSeq2.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboStackSeq2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, strStackSeq, "", "전체"
                        , "StackCode", "StackName", dtStack);

                    for (int i = 1; i < this.uComboStackSeq2.Items.Count; i++)
                    {
                        if (this.uComboStackSeq2.Items[0].DataValue.Equals(this.uComboStackSeq2.Items[i].DataValue))
                        {
                            this.uComboStackSeq2.Items.Remove(i);
                            i--;
                        }
                    }
                }
                else if (intReqLotSeq == 3)
                {
                    this.uComboStackSeq3.Items.Clear();
                    wCombo.mfSetComboEditor(this.uComboStackSeq3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, strStackSeq, "", "전체"
                        , "StackCode", "StackName", dtStack);

                    for (int i = 1; i < this.uComboStackSeq3.Items.Count; i++)
                    {
                        if (this.uComboStackSeq3.Items[0].DataValue.Equals(this.uComboStackSeq3.Items[i].DataValue))
                        {
                            this.uComboStackSeq3.Items.Remove(i);
                            i--;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 일반 조회 Method
        // 제품명 조회 
        private DataTable GetProductInfo(String strPlantCode, String strProductCode)
        {
            DataTable dtProduct = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                dtProduct = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                return dtProduct;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtProduct;
            }
            finally
            {
            }
        }

        // UserName 조회
        private String GetUserName(String strPlantCode, String strCreateUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCreateUserID, m_resSys.GetString("SYS_LANG"));

                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }

                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }


        #endregion

        #region 검색조건 Event, Method
        // 검색조건 : 공정정보 Load (공장 선택 시 )
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtPackage = new DataTable();
                DataTable dtArea = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                //this.uComboSearchProcessGroup.Items.Clear();
                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchArea.Items.Clear();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    // 검색조건 Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // Area
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //// Area 콤보 추가로 인한 AreaCombo ValueChanged 로 이동
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    ////QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    ////brwChannel.mfCredentials(clsProcess);

                    ////dtProcessGroup = clsProcess.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }

                ////wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                ////    , "ProcessGroup", "ComboName", dtProcessGroup);

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "Package", "ComboName", dtPackage);

                wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "AreaCode", "AreaName", dtArea);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        // 검색조건 : 설비정보 팝업창

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0005 frmPOP = new frmPOP0005();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchEquipCode.Text = frmPOP.EquipCode;
                this.uTextSearchEquipName.Text = frmPOP.EquipName;

                // 공정그룹박스 콤보설정 메소드 호출
                SetSearchProcessGroupCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 : 제품코드 입력 후 엔터 시 제품명을 가지고 온다.
        private void uTextSearchProductCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchProductCode.Text == "")
                    {
                        this.uTextSearchProductName.Text = "";
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strProductCode = this.uTextSearchProductCode.Text;

                            //제품명 조회 메소드 호출하여 처리결과 정보를 리턴 받는다.
                            DataTable dtProduct = GetProductInfo(strPlantCode, strProductCode);

                            if (dtProduct.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtProduct.Rows.Count; i++)
                                {
                                    this.uTextSearchProductName.Text = dtProduct.Rows[i]["ProductName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M001089",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchProductName.Text = "";
                                this.uTextSearchProductCode.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchProductCode.TextLength <= 1 || this.uTextSearchProductCode.Text == this.uTextSearchProductCode.SelectedText)
                    {
                        this.uTextSearchProductName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 : 제품코드 팝업창

        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 입력 항목 체크
        // CCS의뢰유형1 선택시 LotNo 필수 항목 체크
        private void uComboCCSReqType1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uTextLotNo1.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000078", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo1.Focus();
                    return;
                }
                if (this.uTextNowProcessCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000282", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo1.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // CCS의뢰유형2 선택시 LotNo 필수 항목 체크
        private void uComboCCSReqType2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uTextLotNo2.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000078", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo2.Focus();
                    return;
                }
                if (this.uTextNowProcessCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000282", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo2.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // CCS의뢰유형3 선택시 LotNo 필수 항목 체크
        private void uComboCCSReqType3_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uTextLotNo3.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000078", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo3.Focus();
                    return;
                }
                if (this.uTextNowProcessCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M000282", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextLotNo3.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 통계 Method
        /// <summary>
        /// 계수/계량형 결과값 검사 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementMeasureCount(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Instance 객체 생성
                    QRPSTA.STABAS clsSTABAS = new QRPSTA.STABAS();
                    // 구조체 변수 생성
                    QRPSTA.STABasic structSTA = new QRPSTA.STABasic();

                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;

                    int intSampleSize = ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                    int intLastIndex = intSampleSize + intStart;

                    // Double형 배열 생성
                    double[] dblXn = new double[intLastIndex - intStart];

                    // Loop돌며 배열에 값 저장

                    for (int i = 0; i < intLastIndex - intStart; i++)
                    {
                        int intIndex = intStart + i;
                        dblXn[i] = Convert.ToDouble(e.Cell.Row.Cells[intIndex].Value);
                    }

                    structSTA = clsSTABAS.mfCalcBasicStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                                                        , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString());

                    // 합부판정
                    if (structSTA.AcceptFlag)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        //e.Cell.Row.Appearance.BackColor = Color.Empty;
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                        //e.Cell.Row.Appearance.BackColor = Color.Salmon;
                    }

                    // 불량수량 자동 입력
                    e.Cell.Row.Cells["FaultQty"].Value = structSTA.FalutCount;
                    // 평균
                    e.Cell.Row.Cells["Mean"].Value = structSTA.Mean;
                    // MaxValue
                    e.Cell.Row.Cells["MaxValue"].Value = structSTA.Max;
                    // MinValue
                    e.Cell.Row.Cells["MinValue"].Value = structSTA.Min;
                    // Range
                    e.Cell.Row.Cells["DataRange"].Value = structSTA.Range;
                    // StdDev
                    e.Cell.Row.Cells["StdDev"].Value = structSTA.StdDev;
                    // Cp
                    e.Cell.Row.Cells["Cp"].Value = structSTA.Cp;
                    // Cpk
                    e.Cell.Row.Cells["Cpk"].Value = structSTA.Cpk;

                    // 검사값이 불량일때 폰트색 변경

                    //if (Convert.ToDecimal(e.Cell.Value) > Convert.ToDecimal(e.Cell.Row.Cells["UpperSpec"].Value) ||
                    //    Convert.ToDecimal(e.Cell.Value) < Convert.ToDecimal(e.Cell.Row.Cells["LowerSpec"].Value))
                    //{
                    //    e.Cell.Appearance.ForeColor = Color.Red;
                    //}
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// OK/NG 합/부 판정 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementOkNg(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;

                    int intSampleSize = ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                    int intLastIndex = intSampleSize + intStart;

                    int intFaultCount = 0;
                    // Loop 돌며 결과값 검사

                    for (int i = intStart; i < intLastIndex; i++)
                    {
                        if (e.Cell.Row.Cells[i].Value.ToString() == "NG")
                        {
                            intFaultCount += 1;
                        }
                    }

                    e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                    if (intFaultCount > 0)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                        //e.Cell.Row.Appearance.BackColor = Color.Salmon;
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        //e.Cell.Row.Appearance.BackColor = Color.Empty;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형 선택 합부판정(Bom인경우 Bom테이블에 자재코드가 존재하면 OK아니면 NG BomCheckFlag가 false이면 기존방식대로 선택
        /// </summary>
        /// <param name="e"></param>
        /// <param name="intLotSeq"> Lot의뢰 차수 2012-12-14 가동조건 변경을위해 추가 </param>
        private void JudgementSelect(Infragistics.Win.UltraWinGrid.CellEventArgs e, int intLotSeq)
        {
            try
            {
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;

                    int intSampleSize = ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                    int intLastIndex = intSampleSize + intStart;

                    string strMaterialCode = string.Empty; // 자재코드 저장 변수 ( 자재코드에 따라 가동조건변경을 위한 변수) 2012-12-14
                    
                    if(m_strBOMCode != null)
                        m_strBOMCode.Clear();   // BOM 자재코드 초기화

                    // BomCheckFlag 확인
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsType);

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    string strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                    DataTable dtBomCheck = clsType.mfReadMASInspectType_BomChecFlag(strPlantCode, strInspectGroupCode, strInspectTypeCode);

                    // BomCheckFlag 가 True 이면
                    if (dtBomCheck.Rows[0]["BomCheckFlag"].ToString().Equals("T"))
                    {
                        bool bolCheck = true;
                        int intFaultCount = 0;
                        // Loop 돌며 결과값 검사

                        for (int i = intStart; i < intLastIndex; i++)
                        {
                            if (!e.Cell.Row.Cells[i].Value.ToString().Equals(string.Empty))
                            {
                                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                                brwChannel.mfCredentials(clsItem);

                                DataTable dtInspectResult = clsItem.mfReadCCSInspectReqItem_DataTypeSelectInspectResult_MASBOM(strPlantCode, this.uTextProductCode.Text
                                                                                                                            , this.uTextNowProcessCode.Text, e.Cell.Row.Cells[i].Value.ToString());
                                if (dtInspectResult.Rows[0]["InspectResult"].ToString().Equals("NG"))
                                {
                                    bolCheck = false;
                                    //break;
                                    intFaultCount += 1;
                                }

                                // 2012-12-14 선택된 자재코드 저장
                                //if (strMaterialCode.Equals(string.Empty))
                                //    strMaterialCode = e.Cell.Row.Cells[i].Value.ToString();
                                //else
                                //    strMaterialCode = strMaterialCode + "," + e.Cell.Row.Cells[i].Value.ToString();

                                m_strBOMCode.Add(e.Cell.Row.Cells[i].Value.ToString()); // 2013-01-25 A7100
                                ///////////////////////////////////////////////////////////// 2012-12-14
                            }
                        }

                        if (bolCheck)
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                            //e.Cell.Row.Appearance.BackColor = Color.Empty;
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                            //e.Cell.Row.Appearance.BackColor = Color.Salmon;
                        }

                        e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                        // 2012-12-14 자재코드에 따라 가동조건 변경
                        CCSParaRefresh(intLotSeq, e.Cell.Row.Index);
                        //////////////////////////////// 2012-12-14
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 합부판정
        private void uGridCCSReq1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGridCCSReq1.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e,1);
                        }

                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("ProductItemSS"))
                    {

                        int intNewSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < intNewSampleSize)
                        {
                            //this.uGridCCSReq1.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.OriginalValue)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGridCCSReq1.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGridCCSReq1.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGridCCSReq1, 0, "SampleSize", "ProductItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGridCCSReq1, this.uComboPlant.Value.ToString(), "ProductItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                        
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    
                    // 이벤트 등록
                    this.uGridCCSReq1.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGridCCSReq2.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e,2);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("ProductItemSS"))
                    {
                        //if (Convert.ToInt32(e.Cell.Row.Cells["FaultQty"].Value) > (Convert.ToInt32(Convert.ToInt32(e.Cell.Value)) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)))
                        //{
                        //    WinMessageBox msg = new WinMessageBox();
                        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        //                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                                            , "입력값 확인", "검사수량 입력값 확인", "입력하신 검사수량이 불량수량보다 작습니다."
                        //                                            , Infragistics.Win.HAlign.Center);
                        //    //e.Cell.Row.Cells["FaultQty"].Value = "0";
                        //    e.Cell.Value = e.Cell.OriginalValue;
                        //}
                        int intNewSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < intNewSampleSize)
                        {
                            //this.uGridCCSReq2.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.OriginalValue)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGridCCSReq2.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGridCCSReq2.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGridCCSReq2, 0, "SampleSize", "ProductItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGridCCSReq2, this.uComboPlant.Value.ToString(), "ProductItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    // 이벤트 등록
                    this.uGridCCSReq2.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGridCCSReq3.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e,3);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("ProductItemSS"))
                    {
                        int intNewSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < intNewSampleSize)
                        {
                            //this.uGridCCSReq3.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = (Convert.ToInt32(ReturnIntegerValue(e.Cell.OriginalValue)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGridCCSReq3.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGridCCSReq3.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGridCCSReq3, 0, "SampleSize", "ProductItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGridCCSReq3, this.uComboPlant.Value.ToString(), "ProductItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    // 이벤트 등록
                    this.uGridCCSReq3.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 의뢰자 팝업창

        // 상세정보 : 의뢰자1 팝업창

        private void uTextReqUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextReqUserID1.Text = frmPOP.UserID;
                this.uTextReqUserName1.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReqUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextReqUserID2.Text = frmPOP.UserID;
                this.uTextReqUserName2.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReqUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextReqUserID3.Text = frmPOP.UserID;
                this.uTextReqUserName3.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region StepUserID 弹出窗口方法

        private void uTextStepUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextStepUserID1.Text = frmPOP.UserID;
                this.uTextStepUserName1.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextStepUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextStepUserID2.Text = frmPOP.UserID;
                this.uTextStepUserName2.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextStepUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextStepUserID3.Text = frmPOP.UserID;
                this.uTextStepUserName3.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 의뢰자 키다운 이벤트

        // 상세정보 : 의뢰자ID1 입력후 엔터시 의뢰자명을 가지고 온다.
        private void uTextReqUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReqUserID1.Text == "")
                    {
                        this.uTextReqUserName1.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextReqUserID1.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextReqUserID1.Text = "";
                                this.uTextReqUserName1.Text = "";
                            }
                            else
                            {
                                this.uTextReqUserName1.Text = strRtnUserName;
                            }
                        }
                    }
                }



                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReqUserID1.TextLength <= 1 || this.uTextReqUserID1.Text == this.uTextReqUserID1.SelectedText)
                    {
                        this.uTextReqUserName1.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReqUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReqUserID2.Text == "")
                    {
                        this.uTextReqUserName2.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextReqUserID2.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextReqUserID2.Text = "";
                                this.uTextReqUserName2.Text = "";
                            }
                            else
                            {
                                this.uTextReqUserName2.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReqUserID2.TextLength <= 1 || this.uTextReqUserID2.Text == this.uTextReqUserID2.SelectedText)
                    {
                        this.uTextReqUserName2.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReqUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReqUserID3.Text == "")
                    {
                        this.uTextReqUserName3.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextReqUserID3.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextReqUserID3.Text = "";
                                this.uTextReqUserName3.Text = "";
                            }
                            else
                            {
                                this.uTextReqUserName3.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReqUserID3.TextLength <= 1 || this.uTextReqUserID3.Text == this.uTextReqUserID3.SelectedText)
                    {
                        this.uTextReqUserName3.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region StepUserID输入后回车使用的方法

        // 상세정보 : 의뢰자ID1 입력후 엔터시 의뢰자명을 가지고 온다.
        private void uTextStepUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextStepUserID1.Text == "")
                    {
                        this.uTextStepUserName1.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextStepUserID1.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextStepUserID1.Text = "";
                                this.uTextStepUserName1.Text = "";
                            }
                            else
                            {
                                this.uTextStepUserName1.Text = strRtnUserName;
                            }
                        }
                    }
                }



                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextStepUserID1.TextLength <= 1 || this.uTextStepUserID1.Text == this.uTextStepUserID1.SelectedText)
                    {
                        this.uTextStepUserName1.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextStepUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextStepUserID2.Text == "")
                    {
                        this.uTextStepUserName2.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextStepUserID2.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextStepUserID2.Text = "";
                                this.uTextStepUserName2.Text = "";
                            }
                            else
                            {
                                this.uTextStepUserName2.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextStepUserID2.TextLength <= 1 || this.uTextStepUserID2.Text == this.uTextStepUserID2.SelectedText)
                    {
                        this.uTextStepUserName2.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextStepUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextStepUserID3.Text == "")
                    {
                        this.uTextStepUserName3.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strUserID = this.uTextStepUserID3.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextStepUserID3.Text = "";
                                this.uTextStepUserName3.Text = "";
                            }
                            else
                            {
                                this.uTextStepUserName3.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextStepUserID3.TextLength <= 1 || this.uTextStepUserID3.Text == this.uTextStepUserID3.SelectedText)
                    {
                        this.uTextStepUserName3.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Stack 콤보박스 이벤트

        // 1차 스택 콤보박스 값변경 이벤트
        private void uComboStackSeq1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strProductCode = this.uTextProductCode.Text;
                string strProcessCode = this.uTextNowProcessCode.Text;
                string strReqItemType = "P";
                string strStackSeq = this.uComboStackSeq1.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtProduct = clsItem.mfReadCCSInspectReqItem_Init(strPlantCode, strProcessCode, strProductCode, strReqItemType, strStackSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridCCSReq1.DataSource = dtProduct;
                this.uGridCCSReq1.DataBind();

                if (dtProduct.Rows.Count > 0)
                {
                    this.uGridCCSReq1.DisplayLayout.Bands[0].Columns.ClearUnbound();
                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.uGridCCSReq1, 0, "SampleSize", "ProductItemSS", strLastColKey);

                    SetSamplingGridColumn(this.uGridCCSReq1, strPlantCode, "ProductItemSS");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2차 스택 콤보박스 값변경 이벤트

        private void uComboStackSeq2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strProductCode = this.uTextProductCode.Text;
                string strProcessCode = this.uTextNowProcessCode.Text;
                string strReqItemType = "P";
                string strStackSeq = this.uComboStackSeq2.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtProduct = clsItem.mfReadCCSInspectReqItem_Init(strPlantCode, strProcessCode, strProductCode, strReqItemType, strStackSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridCCSReq2.DataSource = dtProduct;
                this.uGridCCSReq2.DataBind();

                if (dtProduct.Rows.Count > 0)
                {
                    this.uGridCCSReq2.DisplayLayout.Bands[0].Columns.ClearUnbound();
                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.uGridCCSReq2, 0, "SampleSize", "ProductItemSS", strLastColKey);

                    SetSamplingGridColumn(this.uGridCCSReq1, strPlantCode, "ProductItemSS");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 3차 스택 콤보박스 값변경 이벤트

        private void uComboStackSeq3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strProductCode = this.uTextProductCode.Text;
                string strProcessCode = this.uTextNowProcessCode.Text;
                string strReqItemType = "P";
                string strStackSeq = this.uComboStackSeq3.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtProduct = clsItem.mfReadCCSInspectReqItem_Init(strPlantCode, strProcessCode, strProductCode, strReqItemType, strStackSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridCCSReq3.DataSource = dtProduct;
                this.uGridCCSReq3.DataBind();

                if (dtProduct.Rows.Count > 0)
                {
                    this.uGridCCSReq3.DisplayLayout.Bands[0].Columns.ClearUnbound();
                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.uGridCCSReq3, 0, "SampleSize", "ProductItemSS", strLastColKey);

                    SetSamplingGridColumn(this.uGridCCSReq1, strPlantCode, "ProductItemSS");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Stack 선택전에 LotInfo MES I/F 가 선행되었는지확인하기 위한 이벤트

        private void uComboStackSeq3_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextProductCode.Text.Equals(string.Empty) || this.uTextLotNo3.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000070", "M000069", "M000072", Infragistics.Win.HAlign.Right);

                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Stack 선택전에 LotInfo MES I/F 가 선행되었는지확인하기 위한 이벤트

        private void uComboStackSeq2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextProductCode.Text.Equals(string.Empty) || this.uTextLotNo2.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000070", "M000069", "M000072", Infragistics.Win.HAlign.Right);

                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Stack 선택전에 LotInfo MES I/F 가 선행되었는지확인하기 위한 이벤트

        private void uComboStackSeq1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextProductCode.Text.Equals(string.Empty) || this.uTextLotNo1.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000070", "M000069", "M000072", Infragistics.Win.HAlign.Right);

                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void frmCCSZ0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그리드 Row Initialize 이벤트

        private void uGridCCSReq1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq3_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 작성완료 안된 의뢰건에 대해서 취소(Data삭제) 한다.
        private void DeleteCCSInsepctReq()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTabCCS.SelectedTab.Index == 0)
                {
                    if (this.uTextComplete1.Text.Equals("T"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000021", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                else if (this.uTabCCS.SelectedTab.Index == 1)
                {
                    if (this.uTextComplete2.Text.Equals("T"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000023", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                else if (this.uTabCCS.SelectedTab.Index == 2)
                {
                    if (this.uTextComplete3.Text.Equals("T"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000026", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000665",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //CCS 의뢰 차수
                    string strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                    string strReqSeq = this.uTextReqNo.Text.Substring(8, 4);
                    int intReqLotSeq = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    QRPCCS.BL.INSCCS.CCSInspectReqH clsDel = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsDel);

                    string strErrRtn = clsDel.mfDeleteCCSInsepctReqH(this.uComboPlant.Value.ToString(), strReqNo, strReqSeq, intReqLotSeq);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 결과검사

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M000638", "M000666",
                                            Infragistics.Win.HAlign.Right);

                        // 리스트 갱신
                        mfSearch();
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M000638", "M000667",
                                            Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonCancelReq1_Click(object sender, EventArgs e)
        {
            DeleteCCSInsepctReq();
        }

        private void uButtonCancelReq2_Click(object sender, EventArgs e)
        {
            DeleteCCSInsepctReq();
        }

        private void uButtonCancelReq3_Click(object sender, EventArgs e)
        {
            DeleteCCSInsepctReq();
        }

        private void uComboSearchArea_ValueChanged(object sender, EventArgs e)
        {
            //SetSearchProcessGroupCombo();
        }

        // 검색조건 공정그룹 콤보박스 설정 메소드

        private void SetSearchProcessGroupCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                DataTable dtProcessGroup = new DataTable();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text.Trim();

                this.uComboSearchProcessGroup.Items.Clear();

                if (!strAreaCode.Equals(string.Empty) && !strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    dtProcessGroup = clsEquip.mfReadEquipArea_WithDetailProcessOperationType(strPlantCode, strEquipCode, strAreaCode);
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComboCode", "ComboName", dtProcessGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboEquip_ValueChanged(object sender, EventArgs e)
        {
            if (this.uComboEquip.Value != null && !this.uComboEquip.Value.ToString().Equals(string.Empty))
            {
                if (this.uTextReqNo.Text.Equals(string.Empty))
                    SearchInspectReqPara(this.uComboPlant.Value.ToString(), "", "", 1);
                
                SetParaInfo();

                //CheckEQPLocking();
            }
        }

        private void SetParaInfo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                ////string strPlantCode = this.uComboPlant.Value.ToString();
                ////string strPackage = this.uTextPackage.Text;
                ////string strProcessCode = this.uTextNowProcessCode.Text;
                ////string strEquipCode = this.uComboEquip.Value.ToString();
                ////string strCustomerCode = this.uTextCustomerCode.Text;

                // 가동조건
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);

                //DataTable dtPara = clsPara.mfReadCCSInspectReqPara_Init(strPlantCode, strPackage, strProcessCode, strEquipCode, strCustomerCode);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strReqNo = string.Empty;
                string strReqSeq = string.Empty;
                int intReqLotSeq = 0;
                if (this.uTextReqNo.Text.Length > 12)
                {
                    strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                    strReqSeq = this.uTextReqNo.Text.Substring(8, 4);
                }
                string strProductCode = this.uTextProductCode.Text;
                string strNowProcessCode = this.uTextNowProcessCode.Text;
                string strEquipCode = this.uComboEquip.Value.ToString();
                string strPackage = this.uTextPackage.Text;
                string strCoustormer = this.uTextCustomerCode.Text;

                //DataTable dtPara = clsPara.mfReadCCSInspectReqPara_PSTS(strPlantCode, strReqNo, strReqSeq, intReqLotSeq
                //                                                        , strProductCode, strNowProcessCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                DataTable dtPara = SetParaInfo(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, strProductCode, strPackage,strNowProcessCode, strEquipCode, strCoustormer, m_resSys.GetString("SYS_LANG"));

                // 검사유형 금형/치공구 Tool Value 설정
                DataTable dtToolValue = clsPara.mfReadCCSInspectReqPara_ToolValue(strPlantCode, strEquipCode, strPackage, strCoustormer);

                if (this.uTabCCS.SelectedTab.Index.Equals(0))
                {
                    this.uGridPara1.DataSource = dtPara;
                    this.uGridPara1.DataBind();

                    if (dtToolValue.Rows.Count > 0)
                    {
                        int intStart = this.uGridCCSReq1.DisplayLayout.Bands[0].Columns["1"].Index;

                        for (int i = 0; i < this.uGridCCSReq1.Rows.Count; i++)
                        {
                            int intSampleSize = Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["ProductItemSS"].Value) * Convert.ToInt32(this.uGridCCSReq1.Rows[i].Cells["SampleSize"].Value);
                            if (intSampleSize > 99)
                                intSampleSize = 99;

                            if (this.uGridCCSReq1.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060704"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq1.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["HEAD"].ToString();
                                }
                            }
                            else if (this.uGridCCSReq1.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060703"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq1.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["STAGE"].ToString();
                                }
                            }
                        }
                    }

                }
                else if (this.uTabCCS.SelectedTab.Index.Equals(1))
                {
                    this.uGridPara2.DataSource = dtPara;
                    this.uGridPara2.DataBind();

                    if (dtToolValue.Rows.Count > 0)
                    {
                        int intStart = this.uGridCCSReq2.DisplayLayout.Bands[0].Columns["1"].Index;

                        for (int i = 0; i < this.uGridCCSReq2.Rows.Count; i++)
                        {
                            int intSampleSize = Convert.ToInt32(this.uGridCCSReq2.Rows[i].Cells["ProductItemSS"].Value) * Convert.ToInt32(this.uGridCCSReq2.Rows[i].Cells["SampleSize"].Value);
                            if (intSampleSize > 99)
                                intSampleSize = 99;

                            if (this.uGridCCSReq2.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060704"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq2.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["HEAD"].ToString();
                                }
                            }
                            else if (this.uGridCCSReq2.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060703"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq2.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["STAGE"].ToString();
                                }
                            }
                        }
                    }
                }
                else if (this.uTabCCS.SelectedTab.Index.Equals(2))
                {
                    this.uGridPara3.DataSource = dtPara;
                    this.uGridPara3.DataBind();

                    if (dtToolValue.Rows.Count > 0)
                    {
                        int intStart = this.uGridCCSReq3.DisplayLayout.Bands[0].Columns["1"].Index;

                        for (int i = 0; i < this.uGridCCSReq3.Rows.Count; i++)
                        {
                            int intSampleSize = Convert.ToInt32(this.uGridCCSReq3.Rows[i].Cells["ProductItemSS"].Value) * Convert.ToInt32(this.uGridCCSReq3.Rows[i].Cells["SampleSize"].Value);
                            if (intSampleSize > 99)
                                intSampleSize = 99;

                            if (this.uGridCCSReq3.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060704"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq3.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["HEAD"].ToString();
                                }
                            }
                            else if (this.uGridCCSReq3.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02060703"))
                            {
                                for (int j = intStart; j < intStart + intSampleSize; j++)
                                {
                                    this.uGridCCSReq3.Rows[i].Cells[j].Value = dtToolValue.Rows[0]["STAGE"].ToString();
                                }
                            }
                        }
                    }                
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ParaMeter 조회매서드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="intReqLotSeq">의뢰순차</param>
        /// <param name="strProductCode">제품</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strNowProcessCode">공정</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strCoustomer">고객</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>ParaMeter정보</returns>
        private DataTable SetParaInfo(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, string strProductCode, string strPackage, string strNowProcessCode, string strEquipCode, string strCoustomer, string strLang)
        {
            DataTable dtPara = new DataTable();
            try
            {
                // 가동조건
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);


                dtPara = clsPara.mfReadCCSInspectReqPara_PSTS(strPlantCode, strReqNo, strReqSeq, intReqLotSeq
                                                                        , strProductCode, strNowProcessCode, strEquipCode, strLang);

                if (dtPara.Rows.Count == 0)
                {
                    dtPara = clsPara.mfReadCCSInspectReqPara_MASCCSPara(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, strProductCode,
                                                                            strPackage, strNowProcessCode, strEquipCode, strCoustomer, strLang);
                }

                clsPara.Dispose();
                return dtPara;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtPara;
            }
            finally
            {
                dtPara.Dispose();
            }
        }

        #region 생산Item 그리드 Key 이벤트 처리
        private void uGridCCSReq1_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridCCSReq1 == null ? null : uGridCCSReq1.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGridCCSReq1.ActiveCell.Row.Index;

                if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGridCCSReq1.Rows[intRowIndex].Cells["ProductItemSS"].Value) * ReturnIntegerValue(uGridCCSReq1.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGridCCSReq1.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGridCCSReq1.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "ProductItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq1.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGridCCSReq1.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGridCCSReq1.ActiveCell.Row.Index < this.uGridCCSReq1.Rows.Count - 1)
                                    nextCell = this.uGridCCSReq1.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGridCCSReq1.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGridCCSReq1.ActiveCell.Row.Index < this.uGridCCSReq1.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGridCCSReq1.Rows[intRowIndex + 1].Cells["ProductItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGridCCSReq1.Rows[0].Cells["ProductItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGridCCSReq1.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                else if (activeCell.Column.Key.Equals("ProductItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq1.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGridCCSReq1.ActiveCell = nextCell;
                        this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq1.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGridCCSReq1.ActiveCell = nextCell;
                        this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridCCSReq2 == null ? null : uGridCCSReq2.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGridCCSReq2.ActiveCell.Row.Index;

                if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGridCCSReq2.Rows[intRowIndex].Cells["ProductItemSS"].Value) * ReturnIntegerValue(uGridCCSReq2.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGridCCSReq2.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGridCCSReq2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "ProductItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq2.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGridCCSReq2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGridCCSReq2.ActiveCell.Row.Index < this.uGridCCSReq2.Rows.Count - 1)
                                    nextCell = this.uGridCCSReq2.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGridCCSReq2.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGridCCSReq2.ActiveCell.Row.Index < this.uGridCCSReq2.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGridCCSReq2.Rows[intRowIndex + 1].Cells["ProductItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGridCCSReq2.Rows[0].Cells["ProductItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGridCCSReq2.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                else if (activeCell.Column.Key.Equals("ProductItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq2.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGridCCSReq2.ActiveCell = nextCell;
                        this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq2.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGridCCSReq2.ActiveCell = nextCell;
                        this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq3_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridCCSReq3 == null ? null : uGridCCSReq3.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGridCCSReq3.ActiveCell.Row.Index;

                if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGridCCSReq3.Rows[intRowIndex].Cells["ProductItemSS"].Value) * ReturnIntegerValue(uGridCCSReq3.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGridCCSReq3.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGridCCSReq3.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "ProductItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq3.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGridCCSReq3.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGridCCSReq3.ActiveCell.Row.Index < this.uGridCCSReq3.Rows.Count - 1)
                                    nextCell = this.uGridCCSReq3.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGridCCSReq3.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGridCCSReq3.ActiveCell.Row.Index < this.uGridCCSReq3.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGridCCSReq3.Rows[intRowIndex + 1].Cells["ProductItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGridCCSReq3.Rows[0].Cells["ProductItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGridCCSReq3.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("ProductItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq3.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGridCCSReq3.ActiveCell = nextCell;
                        this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridCCSReq3.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGridCCSReq3.ActiveCell = nextCell;
                        this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridCCSReq3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 가동조건 그리드 Key 이벤트 처리
        //가동조건1 엔터치면 바로 아래로 이동하도록 처리
        private void uGridPara1_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridPara1 == null ? null : uGridPara1.ActiveCell;
                if (activeCell == null) return;

                //가동조건1 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("InspectValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGridPara1.ActiveCell.Row.Index < this.uGridPara1.Rows.Count - 1)
                            this.uGridPara1.ActiveCell = this.uGridPara1.Rows[this.uGridPara1.ActiveCell.Row.Index + 1].Cells["InspectValue"];
                        else
                            this.uGridPara1.ActiveCell = this.uGridPara1.Rows[0].Cells["InspectValue"];
                        this.uGridPara1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //가동조건2 엔터치면 바로 아래로 이동하도록 처리
        private void uGridPara2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridPara2 == null ? null : uGridPara2.ActiveCell;
                if (activeCell == null) return;

                //가동조건2 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("InspectValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGridPara2.ActiveCell.Row.Index < this.uGridPara2.Rows.Count - 1)
                            this.uGridPara2.ActiveCell = this.uGridPara2.Rows[this.uGridPara2.ActiveCell.Row.Index + 1].Cells["InspectValue"];
                        else
                            this.uGridPara2.ActiveCell = this.uGridPara2.Rows[0].Cells["InspectValue"];
                        this.uGridPara2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //가동조건3 엔터치면 바로 아래로 이동하도록 처리
        private void uGridPara3_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridPara3 == null ? null : uGridPara3.ActiveCell;
                if (activeCell == null) return;

                //가동조건3 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("InspectValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //가동조건 Spec을 벗어나게 입력한 경우 메세지 처리


                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGridPara3.ActiveCell.Row.Index < this.uGridPara3.Rows.Count - 1)
                            this.uGridPara3.ActiveCell = this.uGridPara3.Rows[this.uGridPara3.ActiveCell.Row.Index + 1].Cells["InspectValue"];
                        else
                            this.uGridPara3.ActiveCell = this.uGridPara3.Rows[0].Cells["InspectValue"];
                        this.uGridPara3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uGridCCSReq1_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridCCSReq1.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGridCCSReq1.ActiveCell.Row.Index;
                    if (this.uGridCCSReq1.ActiveCell.Value.Equals(0))
                    {
                        this.uGridCCSReq1.EventManager.AllEventsEnabled = false;
                        this.uGridCCSReq1.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGridCCSReq1.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGridCCSReq1.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGridCCSReq1.Rows[intIndex].Cells["ProductItemSS"].Value) *
                                                ReturnIntegerValue(this.uGridCCSReq1.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGridCCSReq1.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGridCCSReq1.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGridCCSReq1.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGridCCSReq1.ActiveCell.Column.Key.Equals("ProductItemSS"))
                {
                    int intIndex = this.uGridCCSReq1.ActiveCell.Row.Index;
                    this.uGridCCSReq1.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq2_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridCCSReq2.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGridCCSReq2.ActiveCell.Row.Index;
                    if (this.uGridCCSReq2.ActiveCell.Value.Equals(0))
                    {
                        this.uGridCCSReq2.EventManager.AllEventsEnabled = false;
                        this.uGridCCSReq2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGridCCSReq2.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGridCCSReq2.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGridCCSReq2.Rows[intIndex].Cells["ProductItemSS"].Value) *
                                                ReturnIntegerValue(this.uGridCCSReq2.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGridCCSReq2.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGridCCSReq2.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGridCCSReq2.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGridCCSReq2.ActiveCell.Column.Key.Equals("ProductItemSS"))
                {
                    int intIndex = this.uGridCCSReq2.ActiveCell.Row.Index;
                    this.uGridCCSReq2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSReq3_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridCCSReq3.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGridCCSReq3.ActiveCell.Row.Index;
                    if (this.uGridCCSReq3.ActiveCell.Value.Equals(0))
                    {
                        this.uGridCCSReq3.EventManager.AllEventsEnabled = false;
                        this.uGridCCSReq3.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGridCCSReq3.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGridCCSReq3.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGridCCSReq3.Rows[intIndex].Cells["ProductItemSS"].Value) *
                                                ReturnIntegerValue(this.uGridCCSReq3.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGridCCSReq3.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGridCCSReq3.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGridCCSReq3.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGridCCSReq3.ActiveCell.Column.Key.Equals("ProductItemSS"))
                {
                    int intIndex = this.uGridCCSReq3.ActiveCell.Row.Index;
                    this.uGridCCSReq3.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPara1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.ToString().Equals("InspectValue"))
                {
                    double dbLSL = 0;
                    double dbUSL = 0;
                    double dbValue = 0;

                    if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbUSL < dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbLSL > dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbValue < dbLSL || dbValue > dbUSL)
                        {
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        }
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }

                    //if (!e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    //    dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                    //if (!e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    //    dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                    //if (!e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    //    dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                    //if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    //{
                    //    e.Cell.Row.Appearance.BackColor = Color.White;
                    //}
                    //else if (dbValue < dbLSL || dbValue > dbUSL)
                    //{
                    //    e.Cell.Row.Appearance.BackColor = Color.Tomato;
                    //}
                    //else
                    //    e.Cell.Row.Appearance.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridPara2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.ToString().Equals("InspectValue"))
                {
                    double dbLSL = 0;
                    double dbUSL = 0;
                    double dbValue = 0;

                    if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbUSL < dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbLSL > dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbValue < dbLSL || dbValue > dbUSL)
                        {
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        }
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }

                    //if (!e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    //    dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                    //if (!e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    //    dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                    //if (!e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    //    dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                    //if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    //{
                    //    e.Cell.Row.Appearance.BackColor = Color.White;
                    //}
                    //else if (dbValue < dbLSL || dbValue > dbUSL)
                    //{
                    //    e.Cell.Row.Appearance.BackColor = Color.Tomato;
                    //}
                    //else
                    //    e.Cell.Row.Appearance.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridPara3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.ToString().Equals("InspectValue"))
                {
                    double dbLSL = 0;
                    double dbUSL = 0;
                    double dbValue = 0;

                    if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbUSL < dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else if (e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbLSL > dbValue)
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                        dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                        dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());

                        if (dbValue < dbLSL || dbValue > dbUSL)
                        {
                            e.Cell.Row.Appearance.BackColor = Color.Tomato;
                        }
                        else
                            e.Cell.Row.Appearance.BackColor = Color.White;
                    }

                    ////if (!e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString().Equals(string.Empty))
                    ////    dbLSL = Convert.ToDouble(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString());
                    ////if (!e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString().Equals(string.Empty))
                    ////    dbUSL = Convert.ToDouble(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString());
                    ////if (!e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    ////    dbValue = Convert.ToDouble(e.Cell.Row.Cells["InspectValue"].Value.ToString());


                    ////if (e.Cell.Row.Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                    ////{
                    ////    e.Cell.Row.Appearance.BackColor = Color.White;
                    ////}
                    ////else if (dbValue < dbLSL || dbValue > dbUSL)
                    ////{
                    ////    e.Cell.Row.Appearance.BackColor = Color.Tomato;
                    ////}
                    ////else
                    ////    e.Cell.Row.Appearance.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 Locking/UnLocking I/F 용 데이터 테이블

        /// </summary>
        /// <returns></returns>
        private DataTable Set_Datatable_EQPDowninfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // 컬럼생성
                dtRtn.Columns.Add("FormName", typeof(string));              // 화면ID
                dtRtn.Columns.Add("PlantCode", typeof(string));             // 공장코드
                dtRtn.Columns.Add("InspectUserID", typeof(string));         // 의뢰 : 의뢰자, 의뢰현황 : 검사원
                dtRtn.Columns.Add("LotNo", typeof(string));                 // LotNo
                dtRtn.Columns.Add("EquipCode", typeof(string));             // 설비번호
                dtRtn.Columns.Add("DownCode", typeof(string));              // DOWNCODE
                dtRtn.Columns.Add("LockingFlag", typeof(string));           // LockingFlag (Y/N)
                dtRtn.Columns.Add("Comment", typeof(string));               // 비고
                dtRtn.Columns.Add("InterfaceType", typeof(string));         // InterFace유형(LOCKING/UNLOCKING)

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 설비 Locking 필요한경우 데이터 정보 만들어 반환
        /// </summary>
        /// <returns></returns>
        private DataTable SaveEQPDowninfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {

                
                // 1차 의뢰이고 설비가 WB 공정이 아닐 시 Locking Interface                
                // 설비에 따른 Locking 여부는 EquipCombo_ValueChanged 에서 처리 -- 주석처리. CheckEquipLocing 메소드에서 체크함.
                if (this.uTextMESLockingFlag1.Text.Equals("T") && !this.uTextComplete1.Text.Equals("T") &&
                    this.uTabCCS.Tabs["2nd"].Active.Equals(false) &&
                    this.uTabCCS.Tabs["3th"].Active.Equals(false))                                 
                {

                    string strLotNo = this.uTextLotNo1.Text.Trim();
                    // MES CCS의뢰 여부 2012-10-15
                    bool bolChk = CheckCCS(this.uComboPlant.Value.ToString(), this.uComboEquip.Value.ToString(), strLotNo,"1"); 
                    // CCS 의뢰 상태면 Return
                    if(bolChk)
                        return dtRtn;

                    dtRtn = Set_Datatable_EQPDowninfo();
                    DataRow _dr = dtRtn.NewRow();
                    _dr["FormName"] = this.Name;
                    _dr["PlantCode"] = this.uComboPlant.Value.ToString();
                    _dr["InspectUserID"] = this.uTextReqUserID1.Text.ToUpper();
                    _dr["LotNo"] = strLotNo;
                    _dr["EquipCode"] = this.uComboEquip.Value.ToString();
                    _dr["DownCode"] = "D160";
                    _dr["LockingFlag"] = "Y";
                    _dr["Comment"] = this.uTextEtcDesc1.Text;
                    _dr["InterfaceType"] = "LOCKING";
                    dtRtn.Rows.Add(_dr);
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        // 설비선택시 설비유형정보 받아오는 메소드
        private void CheckEQPLocking()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uComboEquip.Value.ToString().ToUpper();

                if (!strEquipCode.Equals("-") && !strEquipCode.Equals(string.Empty))
                {
                    // BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquipInfo = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquipInfo.Rows.Count > 0)
                    {
                        if (!dtEquipInfo.Rows[0]["EquipTypeCode"].ToString().Equals("WB"))
                        {
                            if (!this.uTextReqLotSeq3.Text.Equals(string.Empty))
                                this.uTextMESLockingFlag3.Text = "T";
                            else if (!this.uTextReqLotSeq2.Text.Equals(string.Empty))
                                this.uTextMESLockingFlag2.Text = "T";
                            else
                                this.uTextMESLockingFlag1.Text = "T";
                        }
                        else
                        {
                            //modify by SC
                            if (!this.uTextReqLotSeq3.Text.Equals(string.Empty))
                                this.uTextMESLockingFlag3.Text = "T";
                            else if (!this.uTextReqLotSeq2.Text.Equals(string.Empty))
                                this.uTextMESLockingFlag2.Text = "T";
                            else
                                this.uTextMESLockingFlag1.Text = "T";
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult result = new DialogResult();
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000715", "M000716", Infragistics.Win.HAlign.Right);

                        this.uComboEquip.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void CheckEquipLocing()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strNowProcessCode = this.uTextNowProcessCode.Text;
                //string strWorkProcessCode = this.uTextWorkProcessCode.Text;

                if (!strPlantCode.Equals(string.Empty) && !strNowProcessCode.Equals(string.Empty))
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    DataTable dtProcessInfo = clsProcess.mfReadMASProcess_Detail(strPlantCode, strNowProcessCode, m_resSys.GetString("SYS_LANG"));

                    if (dtProcessInfo.Rows.Count > 0)
                    {
                        var query = from process in dtProcessInfo.AsEnumerable()
                                         where process.Field<string>("ProcessCode").Equals(strNowProcessCode)
                                         select new
                                         {
                                             DETAILPROCESSOPERATIONTYPE = process.Field<string>("DETAILPROCESSOPERATIONTYPE")
                                         };

                        foreach (var CheckWB in query)
                        {
                            if (CheckWB.DETAILPROCESSOPERATIONTYPE == null)
                            {
                                if (!this.uTextReqLotSeq3.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag3.Text = "T";
                                else if (!this.uTextReqLotSeq2.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag2.Text = "T";
                                else
                                    this.uTextMESLockingFlag1.Text = "T";
                            }
                            else if (CheckWB.DETAILPROCESSOPERATIONTYPE.Equals("WB"))
                            {
                                //modify by SC
                                if (!this.uTextReqLotSeq3.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag3.Text = "T";
                                else if (!this.uTextReqLotSeq2.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag2.Text = "T";
                                else
                                    this.uTextMESLockingFlag1.Text = "T";
                            }
                            else
                            {
                                if (!this.uTextReqLotSeq3.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag3.Text = "T";
                                else if (!this.uTextReqLotSeq2.Text.Equals(string.Empty))
                                    this.uTextMESLockingFlag2.Text = "T";
                                else
                                    this.uTextMESLockingFlag1.Text = "T";
                            }
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult result = new DialogResult();
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000715", "M000377", Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private Int32 ReturnIntegerValue(object value)
        {
            Int32 result = 0;

            string[] arrValue = value.ToString().Split('.');

            string strValue;

            if (arrValue.Length > 1)
                strValue = arrValue[0];
            else
                strValue = value.ToString();

            if (Int32.TryParse(strValue, out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// 중복체크
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strReqLotNo">의뢰차수</param>
        /// <returns>처리여부</returns>
        private bool RtnReqHCheck(string strPlantCode, string strEquipCode, string strProcessCode, string strLotNo, string strReqLotNo)
        {
            try
            {
                bool bolCheck = false;
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH),"CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsH = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsH);


                //CCS등록 중복체크 메서드 실행
                DataTable dtRtn = clsH.mfReadCCSInspectReqH_Check(strPlantCode, strEquipCode, strProcessCode, strLotNo, strReqLotNo, 
                                                                m_resSys.GetString("SYS_LANG"));
                clsH.Dispose();

                //정보가 없는경우 Retrun Treu
                if (dtRtn.Rows.Count == 0)
                    bolCheck = true;

                //처리결과 Return
                return bolCheck;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// CCS 의뢰상태여부 검색 (의뢰상태면 Lacking처리 불가) true : CCS 의뢰상태
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strReqLotSeq">의뢰차수</param>
        /// <returns>true : CCS 의뢰상태</returns>
        private bool CheckCCS(string strPlantCode, string strEquipCode, string strLotNo, string strReqLotSeq)
        {
            try
            {
                bool bolChk = false;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsEquip = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsEquip);

                // MES 설비상태관리 검색 매서드 실행
                DataTable dtEquip = clsEquip.mfReadCCSInspectReq_MESDB(strPlantCode, strEquipCode, this.uTextNowProcessCode.Text, strLotNo, strReqLotSeq, m_resSys.GetString("SYS_LANG"));
                clsEquip.Dispose();

                //CCS 의뢰상태인 경우 요청 불가처리 2012-10-15
                if (dtEquip.Rows.Count > 0 && dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                    bolChk = true;

                m_resSys.Close();
                return bolChk;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// 현재 설비상태 체크 : 설비상태가 "RUN"일 경우만 CCS 의뢰가능.
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strReqLotSeq">의뢰차수</param>
        /// <returns>true : "RUN"</returns>
        private bool CheckEquipState(string strPlantCode, string strEquipCode, string strLotNo, string strReqLotSeq)
        {
            try
            {
                bool bolChk = false;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsEquip = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsEquip);

                // MES 설비상태관리 검색 매서드 실행
                DataTable dtEquip = clsEquip.mfReadCCSInspectReq_MESDB(strPlantCode, strEquipCode, this.uTextNowProcessCode.Text, strLotNo, strReqLotSeq, m_resSys.GetString("SYS_LANG"));
                clsEquip.Dispose();

                //CCS 의뢰상태인 경우 요청 불가처리 2012-10-15
                if (dtEquip.Rows.Count > 0 && dtEquip.Rows[0]["EquipState"].ToString().ToUpper().Equals("RUN"))
                    bolChk = true;

                m_resSys.Close();
                return bolChk;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// 자재코드에 따라 CCS가동조건 변경하여 화면에 뿌려줌.
        /// </summary>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="intLotSeq">Lot의뢰순번</param>
        private void CCSParaRefresh(int intLotSeq, int intIndex)
        {
            try
            {
                // 자재코드가 공백인경우 Return
                if (m_strBOMCode.Count == 0)//.Equals(string.Empty))
                    return;

                Infragistics.Win.UltraWinGrid.UltraGrid uGridLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
                Infragistics.Win.UltraWinGrid.UltraGrid uGridLotSeq = new Infragistics.Win.UltraWinGrid.UltraGrid();

                // Lot의뢰 차수에 따라서 Gird저장
                if (intLotSeq == 1)
                {
                    uGridLot = this.uGridCCSReq1;
                    uGridLotSeq = this.uGridPara1;
                }
                else if (intLotSeq == 2)
                {
                    uGridLot = this.uGridCCSReq2;
                    uGridLotSeq = this.uGridPara2;
                }
                else if (intLotSeq == 3)
                {
                    uGridLot = this.uGridCCSReq3;
                    uGridLotSeq = this.uGridPara3;
                }
                else
                    return;

                // Grid Row Loop
                for (int i = 0; i < uGridLot.Rows.Count; i++)
                {
                    // Hidden Row PASS
                    if (uGridLot.Rows[i].Hidden)
                        continue;

                    // 검사항목 데이터 타입이 "선택" 항목이 아니면 PASS
                    if (!uGridLot.Rows[i].GetCellValue("DataType").ToString().Equals("5"))
                        continue;

                    // 자재코드를 담은 Row는 제외
                    if (uGridLot.Rows[i].Index == intIndex)
                        continue;

                    // Start Cell Index
                    int intStart = uGridLot.Rows[i].Cells["1"].Column.Index;

                    // Last Cell Index
                    int intSampleSize = ReturnIntegerValue(uGridLot.Rows[i].Cells["ProductItemSS"].Value)
                                        * ReturnIntegerValue(uGridLot.Rows[i].Cells["SampleSize"].Value);

                    // Last Cell Index > 99 -> 99
                    if (intSampleSize > 99)
                        intSampleSize = 99;

                    int intLastIndex = intSampleSize + intStart;

                    // Start Index ~ Last Index Loop
                    for (int x = intStart; x < intLastIndex; x++)
                    {
                        // 선택된 항목이 없는경우 PASS
                        if (uGridLot.Rows[i].Cells[x].Value.ToString().Trim().Equals(string.Empty))
                            continue;

                        // 선택된 자재코드 저장
                        //if (strMaterialCode.Equals(string.Empty))
                        //    strMaterialCode = uGridLot.Rows[i].Cells[x].Value.ToString();
                        //else
                        //    strMaterialCode = strMaterialCode + "," + uGridLot.Rows[i].Cells[x].Value.ToString();

                        // 선택한 BOM 자재코드 저장
                        m_strBOMCode.Add(uGridLot.Rows[i].Cells[x].Value.ToString());
                        
                    }
                }
                // strMaterialCode ("'A','C','D','E'")

                
                //// 선택한 BOM 자재코드 저장
                //if (strMaterialCode.Equals(string.Empty))
                //    m_strBOMCode = null;
                //else
                //    m_strBOMCode = strMaterialCode.Split(',');

                ArrayList arrChase = CheckCHASE(intLotSeq);

                for (int i = 0; i < uGridLotSeq.Rows.Count; i++)
                {

                    // MaterialSpecName 공백인경우 PASS
                    if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(string.Empty))
                        continue;

                    // BOM MaterialCode 선택되지 않았다면 숨겨있던 Row표시(왠만하면 적용안됌)
                    if (m_strBOMCode.Count == 0)
                    {
                        uGridLotSeq.Rows[i].Hidden = false;
                        continue;
                    }

                    bool _bol = true;

                    for (int r = 0; r < m_strBOMCode.Count; r++)
                    {
                        if (!_bol)
                            break;

                        if (arrChase.Count > 0)
                        {
                            for (int intChese = 0; intChese < arrChase.Count; intChese++)
                            {

                                string strTemp = arrChase[intChese].ToString();

                                if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r])
                                    && uGridLotSeq.Rows[i].GetCellValue("CCSparameterCode").ToString().Contains(strTemp))
                                {
                                    _bol = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r]))
                            {
                                _bol = false;
                                break;
                            }
                        }

                        uGridLotSeq.Rows[i].Hidden = _bol;
                    }

                }

                SetParaSeq(intLotSeq);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region CHASE Checkbox Events... 2012-12-13 천안STS 소스 참고

        /// <summary>
        /// CHASE_All CheckBox Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckCHASE_All_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // 이벤트 걸린 체크박스를 Check
                Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck = (Infragistics.Win.UltraWinEditors.UltraCheckEditor)sender;

                // 체크박스 명을 "_" 기준으로 분리를 한다.
                string[] strCheckName = uCheck.Name.Split('_');

                // 
                if (strCheckName[0].Contains("3"))
                {
                    this.uCheckCHASE3_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_6.Checked = uCheck.Checked == true ? true : false;
                  
                }
                else if (strCheckName[0].Contains("2"))
                {
                    this.uCheckCHASE2_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_6.Checked = uCheck.Checked == true ? true : false;
                }
                else if (strCheckName[0].Contains("1"))
                {
                    this.uCheckCHASE1_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_6.Checked = uCheck.Checked == true ? true : false;
                }
                else
                    return;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CHASE CheckBox Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uCheckCHASE_CheckedValueChanged(object sender, EventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck = (Infragistics.Win.UltraWinEditors.UltraCheckEditor)sender;

            // 체크박스 명을 "_" 기준으로 분리를 한다.
            string[] strCheckName = uCheck.Name.Split('_');

            // 체크박스에 따라 Lot의뢰차수를 판단.
            int intLotSeq = 0;

            if (strCheckName[0].Contains("3"))
            {
                intLotSeq = 2;
            }
            else if (strCheckName[0].Contains("2"))
            {
                intLotSeq = 1;
            }
            else if (strCheckName[0].Contains("1"))
            {
                intLotSeq = 0;
            }
            else
                return;

            //Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            //ArrayList uStr = new ArrayList();

            //if (strCheckName[0].Contains("1"))
            //    uGrid = this.uGridPara1;
            //else if (strCheckName[0].Contains("2"))
            //    uGrid = this.uGridPara2;
            //else if (strCheckName[0].Contains("3"))
            //    uGrid = this.uGridPara3;

            //for (int i = 0; i < uGrid.Rows.Count; i++)
            //{
            //    if (uGrid.Rows[i].Hidden == true && !uGrid.Rows[i].Cells["CCSParameterCode"].Value.ToString().Contains("_/"))
            //    {
            //        uStr.Add(Convert.ToString(uGrid.Rows[i].Cells["CCSParameterCode"].Value));
            //    }
            //}

            //체크박스 상태가 체크된 상태 인경우 파라미터를 추가한다.
            if (uCheck.Checked)
            {
                SetParaData_AddRow(intLotSeq, uCheck);
                SetHiddenRow(intLotSeq + 1);
            }
            else
            {
                SetParaData_DelRow(intLotSeq, uCheck);
                SetUnHiddenRow(intLotSeq + 1);
            }

        }

        /// <summary>
        /// CHASE 체크에 따라 CCS파라미터 정보 추가
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수 Tab Index</param>
        /// <param name="uCheck">체크된 CHASE 체크박스</param>
        private void SetParaData_AddRow(int intReqLotSeq, Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck)
        {
            try
            {
                DataTable dtPara = new DataTable();
                if (intReqLotSeq.Equals(0))
                {
                    dtPara = (DataTable)this.uGridPara1.DataSource;
                }
                else if (intReqLotSeq.Equals(1))
                {
                    dtPara = (DataTable)this.uGridPara2.DataSource;
                }
                else if (intReqLotSeq.Equals(2))
                {
                    dtPara = (DataTable)this.uGridPara3.DataSource;
                }

                // 그리드 정보가 없으면 Return
                if (dtPara == null || !(dtPara.Rows.Count > 0))
                    return;

                // 배열
                string[] strSeparater = { "_" };
                string str_ = "_/";
                // strSeparater 기준으로 uCheck.Name 을 배열로 나눈다.
                string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);
                // 조건설정 (CCS파라미터 정보중 "-*" 존재 하지 않은 파라미터 정보)  ?????
                IEnumerable<DataRow> rows = from Para in dtPara.AsEnumerable()
                                            where !Para["CCSParameterCode"].ToString().Contains(str_)
                                            select Para;

                // CCS파라미터 정보를 DataTable에 복사
                DataTable dtNewPara = dtPara.Copy();
                // 줄번호 저장
                int intRowCount = dtPara.Rows.Count;

                // CCS파라미터 정보중 "_" 존재 하지 않은 파라미터 정보 중에서 각 컬럼별 정보를 테이블에 저장 체크번호에 따라 저장. 
                foreach (DataRow drRow in rows)
                {
                    intRowCount++;
                    DataRow _dr = dtPara.NewRow();

                    if (drRow["CCSPARAMETERCODE"].ToString().Contains("TEMPERATURE"))
                    {
                        for (int i = 1; i < 9; i++)
                        {
                            foreach (DataColumn dtCol in _dr.Table.Columns)
                            {
                                if (dtCol.ColumnName.ToUpper().Equals("CCSPARAMETERCODE"))
                                    _dr.SetField(dtCol, drRow[dtCol] + str_ + strCtlName[1] + "-" + i);
                                else if (dtCol.ColumnName.ToUpper().Equals("PARASEQ"))
                                    _dr.SetField(dtCol, intRowCount);
                                else if (dtCol.ColumnName.ToUpper().Equals("INSPECTVALUE")) // 2013-01-25입력된 검사값은 초기화 시킴
                                    continue;
                                else
                                    _dr.SetField(dtCol, drRow[dtCol]);
                            }

                            dtNewPara.ImportRow(_dr);   // 원래 값의 현재 값 과 모든 속성을 그대로 유지한 상태로 DataRow를 DataTable에복사
                            dtNewPara.Rows.Add(_dr.ItemArray); // 배열의 모든항목을 저장
                            intRowCount++;
                        }
                    }
                    else
                    {
                        foreach (DataColumn dtCol in _dr.Table.Columns)
                        {
                            if (dtCol.ColumnName.ToUpper().Equals("CCSPARAMETERCODE"))
                                _dr.SetField(dtCol, drRow[dtCol] + str_ + strCtlName[1]);
                            else if (dtCol.ColumnName.ToUpper().Equals("PARASEQ"))
                                _dr.SetField(dtCol, intRowCount);
                            else if (dtCol.ColumnName.ToUpper().Equals("INSPECTVALUE")) // 2013-01-25입력된 검사값은 초기화 시킴
                                continue;
                            else
                                _dr.SetField(dtCol, drRow[dtCol]);
                        }

                        dtNewPara.ImportRow(_dr);   // 원래 값의 현재 값 과 모든 속성을 그대로 유지한 상태로 DataRow를 DataTable에복사
                        dtNewPara.Rows.Add(_dr.ItemArray); // 배열의 모든항목을 저장
                    }
                }


                // 해당 CCS파라미터정보 그리드에 바인드
                if (intReqLotSeq.Equals(0))
                {
                    this.uGridPara1.SetDataBinding(dtNewPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(1))
                {
                    this.uGridPara2.SetDataBinding(dtNewPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(2))
                {
                    this.uGridPara3.SetDataBinding(dtNewPara, string.Empty);
                }

                // 순번 재등록
                SetParaSeq(intReqLotSeq + 1);  
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CHASE 체크해제에 따라 CCS파라미터 정보 추가
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수 Tab Indx</param>
        /// <param name="uCheck">체크해제된 CHASE 체크박스</param>
        private void SetParaData_DelRow(int intReqLotSeq, Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck)
        {
            try
            {
                // CCS 파라미터 정보 DataTable에 저장
                DataTable dtPara = new DataTable();
                if (intReqLotSeq.Equals(0))
                {
                    dtPara = (DataTable)this.uGridPara1.DataSource;
                }
                else if (intReqLotSeq.Equals(1))
                {
                    dtPara = (DataTable)this.uGridPara2.DataSource;
                }
                else if (intReqLotSeq.Equals(2))
                {
                    dtPara = (DataTable)this.uGridPara3.DataSource;
                }

                if (dtPara == null || !(dtPara.Rows.Count > 0))
                    return;

                // 배열 기준 설정
                string[] strSeparater = { "_" };
                string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);
                string strName = "_/" + strCtlName[1];
                // CCS파라미터 중 ("_" + CHASE 체크번호) Row정보 저장
                DataRow[] drRows = dtPara.Select("CCSparameterCode Like '%" + strName + "%'");

                // 저장된 Row정보와 동일한 정보 삭제
                foreach (DataRow dr in drRows)
                {
                    dr.BeginEdit();
                    dr.Delete();
                    dr.EndEdit();
                }

                // ????
                dtPara.AcceptChanges();

                // 해당 CCS 파라미터 그리드에 바인드
                if (intReqLotSeq.Equals(0))
                {
                    this.uGridPara1.SetDataBinding(dtPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(1))
                {
                    this.uGridPara2.SetDataBinding(dtPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(2))
                {
                    this.uGridPara3.SetDataBinding(dtPara, string.Empty);
                }

                // 순번 정리
                SetParaSeq(intReqLotSeq + 1);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// CHASE 체크박스가 체크 되었을 경우 CCS의뢰 정보에 "_" 문자가 없는 정보 Hidden 처리 -> _[
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수</param>
        private void SetHiddenRow(int intReqLotSeq)
        {
            string strContains = "_/";
            
            ArrayList dinosaurs = CheckCHASE(intReqLotSeq);

            #region intReqLotSeq == 1

            if (intReqLotSeq == 1)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGridPara1.Rows.Count; j++)
                    {
                        if (!this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGridPara1.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < dinosaurs.Count; i++) // Check1_1, Check1_2
                        {
                           
                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null 
                                && m_strBOMCode.Count > 0
                                && !this.uGridPara1.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(string.Empty))
                            {
                                if (this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp)) // _/1 ? _1
                                {
                                    // TEMPERATURE -> _/1, _/2
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGridPara1.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        this.uGridPara1.Rows[j].Hidden = _bol;

                    }

                    SetParaSeq(1);
                }
            }
            #endregion
            #region intReqLotSeq == 2
            else if (intReqLotSeq == 2)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGridPara2.Rows.Count; j++)
                    {
                        if (!this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGridPara2.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < dinosaurs.Count; i++)
                        {
                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();
                            // MaterialCode
                            if (m_strBOMCode != null 
                                && m_strBOMCode.Count > 0
                                && !this.uGridPara2.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(string.Empty))
                            {
                                if (this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                {
                                    // TEMPERATURE -> _/1-1, _/1-2 .... _/1-8
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGridPara2.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        this.uGridPara2.Rows[j].Hidden = _bol;
                    }
                    SetParaSeq(2);
                }
            }
            #endregion
            #region intReqLotSeq == 3
            else if (intReqLotSeq == 3)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGridPara3.Rows.Count; j++)
                    {
                        if (!this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGridPara3.Rows[j].Hidden = true;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;
                        for (int i = 0; i < dinosaurs.Count; i++)
                        {
                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null 
                                && m_strBOMCode.Count > 0
                                && !this.uGridPara3.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(string.Empty))
                            {
                                if (this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                {
                                    // TEMPERATURE -> _/1-1, _/1-2 .... _/1-8
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGridPara3.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }

                        }

                        this.uGridPara3.Rows[j].Hidden = _bol;
                    }
                    SetParaSeq(3);
                }
            }
            #endregion

        }

        /// <summary>
        /// CHASE 체크박스가 체크안되었을 경우 CCS파라미터에 ("_" + Lot의뢰차수)를 제외한 정보 Hidden처리해제 -> _[
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수</param>
        private void SetUnHiddenRow(int intReqLotSeq)
        {
            try
            {
                string strContains = "_/";

                if (intReqLotSeq == 1)
                {
                    //uGrid1_3
                    if (this.uCheckCHASE1_1.Checked || this.uCheckCHASE1_2.Checked || this.uCheckCHASE1_3.Checked || this.uCheckCHASE1_4.Checked || this.uCheckCHASE1_5.Checked || this.uCheckCHASE1_6.Checked || this.uCheckCHASE1_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
                    {
                        if (m_strBOMCode != null 
                            && m_strBOMCode.Count > 0
                            && !this.uGridPara1.Rows[i].Cells["MaterialSpecName"].Value.ToString().Trim().Equals(string.Empty))
                        {
                            // 자재코드가 있는경우 "_/" 가 없고 BOM선택된 자재인것만 Hidden 해제
                            foreach (string _str in m_strBOMCode)
                            {
                                if (!this.uGridPara1.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains)
                                    && this.uGridPara1.Rows[i].Cells["MaterialSpecName"].Value.ToString().Equals(_str))
                                {
                                    this.uGridPara1.Rows[i].Hidden = false;
                                    break;
                                }
                                else
                                    continue;
                            }
                        }
                        else
                            if (!this.uGridPara1.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                                this.uGridPara1.Rows[i].Hidden = false;
                    }
                    SetParaSeq(1);
                }
                else if (intReqLotSeq == 2)
                {
                    //uGrid2_3
                    if (this.uCheckCHASE2_1.Checked || this.uCheckCHASE2_2.Checked || this.uCheckCHASE2_3.Checked || this.uCheckCHASE2_4.Checked || this.uCheckCHASE2_5.Checked || this.uCheckCHASE2_6.Checked || this.uCheckCHASE2_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
                    {
                        if (m_strBOMCode != null 
                            && m_strBOMCode.Count > 0
                            && !this.uGridPara2.Rows[i].Cells["MaterialSpecName"].Value.ToString().Trim().Equals(string.Empty))
                        {
                            // 자재코드가 있는경우 "_/" 가 없고 BOM선택된 자재인것만 Hidden 해제
                            foreach (string _str in m_strBOMCode)
                            {
                                if (!this.uGridPara2.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains)
                                    && this.uGridPara2.Rows[i].Cells["MaterialSpecName"].Value.ToString().Equals(_str))
                                {
                                    this.uGridPara2.Rows[i].Hidden = false;
                                    break;
                                }
                                else
                                    continue;
                            }
                        }
                        else
                            if (!this.uGridPara2.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                                this.uGridPara2.Rows[i].Hidden = false;
                    }
                    SetParaSeq(2);
                }
                else if (intReqLotSeq == 3)
                {
                    //uGrid3_3
                    if (this.uCheckCHASE3_1.Checked || this.uCheckCHASE3_2.Checked || this.uCheckCHASE3_3.Checked || this.uCheckCHASE3_4.Checked || this.uCheckCHASE3_5.Checked || this.uCheckCHASE3_6.Checked || this.uCheckCHASE3_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGridPara3.Rows.Count; i++)
                    {
                        if (m_strBOMCode != null 
                            && m_strBOMCode.Count > 0
                            && !this.uGridPara3.Rows[i].Cells["MaterialSpecName"].Value.ToString().Trim().Equals(string.Empty))
                        {
                            // 자재코드가 있는경우 "_/" 가 없고 BOM선택된 자재인것만 Hidden 해제
                            foreach (string _str in m_strBOMCode)
                            {
                                if (!this.uGridPara3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains)
                                    && this.uGridPara3.Rows[i].Cells["MaterialSpecName"].Value.ToString().Equals(_str))
                                {
                                    this.uGridPara3.Rows[i].Hidden = false;
                                    break;
                                }
                                else
                                    continue;
                            }
                        }
                        else
                            if (!this.uGridPara3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                                this.uGridPara3.Rows[i].Hidden = false;
                    }

                    SetParaSeq(3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot의뢰 차수, Row hidden에 따라 순번을 정해준다.
        /// </summary>
        /// <param name="intLotReqSeq">Lot의뢰차수</param>
        private void SetParaSeq(int intLotReqSeq)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                if (intLotReqSeq == 1)
                    uGrid = this.uGridPara1;
                else if (intLotReqSeq == 2)
                    uGrid = this.uGridPara2;
                else if (intLotReqSeq == 3)
                    uGrid = this.uGridPara3;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Hidden == false)
                    {
                        uGrid.Rows[i].Cells["ParaSeq"].Value = uGrid.Rows[i].RowSelectorNumber;
                    }
                    else
                        uGrid.Rows[i].Cells["ParaSeq"].Value = i + 100;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// CHASE 체크박스확인
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수</param>
        /// <returns>Check된 CHASE정보</returns>
        private ArrayList CheckCHASE(int intReqLotSeq)
        {
            ArrayList dinosaurs = new ArrayList();
            string strContains = "_/";
            if (intReqLotSeq == 1)
            {
                // Check Box Name
                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE1_1, this.uCheckCHASE1_2, this.uCheckCHASE1_3, 
                                                                             this.uCheckCHASE1_4, this.uCheckCHASE1_5, this.uCheckCHASE1_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    // Check Box Name Add
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }
            }
            
            
            else if (intReqLotSeq == 2)
            {
                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE2_1, this.uCheckCHASE2_2, this.uCheckCHASE2_3, 
                                                                             this.uCheckCHASE2_4, this.uCheckCHASE2_5, this.uCheckCHASE2_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }

               
            }
            
            else if (intReqLotSeq == 3)
            {

                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE3_1, this.uCheckCHASE3_2, this.uCheckCHASE3_3, 
                                                                             this.uCheckCHASE3_4, this.uCheckCHASE3_5, this.uCheckCHASE3_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }

                
            }

            return dinosaurs;
            
        }

        /// <summary>
        /// CCS가동조건 정렬
        /// </summary>
        /// <param name="intLotSeq">Lot의뢰차수</param>
        private void ArryCCSPara(int intLotSeq)
        {
            Infragistics.Win.UltraWinGrid.UltraGrid uGridLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
            Infragistics.Win.UltraWinGrid.UltraGrid uGridLotSeq = new Infragistics.Win.UltraWinGrid.UltraGrid();

            // Lot의뢰 차수에 따라서 Gird저장
            if (intLotSeq == 1)
            {
                uGridLot = this.uGridCCSReq1;
                uGridLotSeq = this.uGridPara1;
            }
            else if (intLotSeq == 2)
            {
                uGridLot = this.uGridCCSReq2;
                uGridLotSeq = this.uGridPara2;
            }
            else if (intLotSeq == 3)
            {
                uGridLot = this.uGridCCSReq3;
                uGridLotSeq = this.uGridPara3;
            }
            else
                return;

            ArrayList arrChase = CheckCHASE(intLotSeq);


            // Grid Row Loop
            for (int i = 0; i < uGridLot.Rows.Count; i++)
            {
                // Hidden Row PASS
                if (uGridLot.Rows[i].Hidden)
                    continue;

                // 검사항목 데이터 타입이 "선택" 항목이 아니면 PASS
                if (!uGridLot.Rows[i].GetCellValue("DataType").ToString().Equals("5"))
                    continue;

                // Start Cell Index
                int intStart = uGridLot.Rows[i].Cells["1"].Column.Index;

                // Last Cell Index
                int intSampleSize = ReturnIntegerValue(uGridLot.Rows[i].Cells["ProductItemSS"].Value)
                                    * ReturnIntegerValue(uGridLot.Rows[i].Cells["SampleSize"].Value);

                // Last Cell Index > 99 -> 99
                if (intSampleSize > 99)
                    intSampleSize = 99;

                int intLastIndex = intSampleSize + intStart;

                // Start Index ~ Last Index Loop
                for (int x = intStart; x < intLastIndex; x++)
                {
                    // 선택된 항목이 없는경우 PASS
                    if (uGridLot.Rows[i].Cells[x].Value.ToString().Trim().Equals(string.Empty))
                        continue;

                    // 선택한 BOM 자재코드 저장
                    m_strBOMCode.Add(uGridLot.Rows[i].Cells[x].Value.ToString());

                }
            }

            if (arrChase.Count == 0)
            {
                for (int i = 0; i < uGridLotSeq.Rows.Count; i++)
                {

                    // MaterialSpecName 공백인경우 PASS
                    if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(string.Empty))
                        continue;

                    // BOM MaterialCode 선택되지 않았다면 숨겨있던 Row표시(왠만하면 적용안됌)
                    if (m_strBOMCode.Count == 0)
                    {
                        uGridLotSeq.Rows[i].Hidden = false;
                        continue;
                    }

                    bool _bol = true;

                    for (int r = 0; r < m_strBOMCode.Count; r++)
                    {
                        if (!_bol)
                            break;

                        if (arrChase.Count > 0)
                        {
                            for (int intChese = 0; intChese < arrChase.Count; intChese++)
                            {

                                string strTemp = arrChase[intChese].ToString();

                                if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r])
                                    && uGridLotSeq.Rows[i].GetCellValue("CCSparameterCode").ToString().Contains(strTemp))
                                {
                                    _bol = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r]))
                            {
                                _bol = false;
                                break;
                            }
                        }

                        uGridLotSeq.Rows[i].Hidden = _bol;
                    }

                }

                SetParaSeq(intLotSeq);

            }
            else
            {

                string strContains = "_/";


                if (arrChase.Count > 0)
                {
                    for (int j = 0; j < uGridLotSeq.Rows.Count; j++)
                    {
                        if (!uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            uGridLotSeq.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < arrChase.Count; i++) // Check1_1, Check1_2
                        {

                            if (!_bol)
                                break;

                            strTemp = arrChase[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null 
                                && m_strBOMCode.Count > 0
                                && !uGridLotSeq.Rows[j].Cells["MaterialSpecName"].Value.ToString().Trim().Equals(string.Empty))
                            {
                                if (uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp)) // _/1 ? _1
                                {
                                    // TEMPERATURE -> _/1, _/2
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (uGridLotSeq.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        uGridLotSeq.Rows[j].Hidden = _bol;

                    }

                    SetParaSeq(intLotSeq);
                }
            }

        }

        #endregion

        #region 注释掉 SetHiddenRow()
        ////string strContains = "_/";
        ////#region List
        ////List<string> dinosaurs = new List<string>();
        ////#region uCheckCHASE1_*
        ////if (this.uCheckCHASE1_1.Checked)
        ////{
        ////    dinosaurs.Add("1");
        ////}
        ////if (this.uCheckCHASE1_2.Checked)
        ////{
        ////    dinosaurs.Add("2");
        ////}
        ////if (this.uCheckCHASE1_3.Checked)
        ////{
        ////    dinosaurs.Add("3");
        ////}
        ////if (this.uCheckCHASE1_4.Checked)
        ////{
        ////    dinosaurs.Add("4");
        ////}
        ////if (this.uCheckCHASE1_5.Checked)
        ////{
        ////    dinosaurs.Add("5");
        ////}
        ////if (this.uCheckCHASE1_6.Checked)
        ////{
        ////    dinosaurs.Add("6");
        ////}
        ////#endregion
        ////#region uCheckCHASE2_*
        ////if (this.uCheckCHASE2_1.Checked)
        ////{
        ////    dinosaurs.Add("1");
        ////}
        ////if (this.uCheckCHASE2_2.Checked)
        ////{
        ////    dinosaurs.Add("2");
        ////}
        ////if (this.uCheckCHASE2_3.Checked)
        ////{
        ////    dinosaurs.Add("3");
        ////}
        ////if (this.uCheckCHASE2_4.Checked)
        ////{
        ////    dinosaurs.Add("4");
        ////}
        ////if (this.uCheckCHASE2_5.Checked)
        ////{
        ////    dinosaurs.Add("5");
        ////}
        ////if (this.uCheckCHASE2_6.Checked)
        ////{
        ////    dinosaurs.Add("6");
        ////}
        ////#endregion
        ////#region uCheckCHASE3_*
        ////if (this.uCheckCHASE3_1.Checked)
        ////{
        ////    dinosaurs.Add("1");
        ////}
        ////if (this.uCheckCHASE3_2.Checked)
        ////{
        ////    dinosaurs.Add("2");
        ////}
        ////if (this.uCheckCHASE3_3.Checked)
        ////{
        ////    dinosaurs.Add("3");
        ////}
        ////if (this.uCheckCHASE3_4.Checked)
        ////{
        ////    dinosaurs.Add("4");
        ////}
        ////if (this.uCheckCHASE3_5.Checked)
        ////{
        ////    dinosaurs.Add("5");
        ////}
        ////if (this.uCheckCHASE3_6.Checked)
        ////{
        ////    dinosaurs.Add("6");
        ////}
        ////#endregion

        ////#endregion

        ////#region intReqLotSeq == 1
        ////if (intReqLotSeq == 1)
        ////{
        ////    //uGrid1_3
        ////    if (this.uCheckCHASE1_1.Checked || this.uCheckCHASE1_2.Checked || this.uCheckCHASE1_3.Checked || this.uCheckCHASE1_4.Checked || this.uCheckCHASE1_5.Checked || this.uCheckCHASE1_6.Checked)
        ////    {
        ////        for (int i = 0; i < dinosaurs.Count; i++)
        ////        {
        ////            string temp = dinosaurs[i];
        ////            string temp1 = string.Format("{0}{1}-{1}", strContains, temp);
        ////            for (int j = 0; j < this.uGridPara1.Rows.Count; j++)
        ////            {
        ////                if (!this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara1.Rows[j].Hidden = true;
        ////                }
        ////                if (this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains("TEMPERATURE"))
        ////                {

        ////                    if (!this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(temp1))
        ////                    {
        ////                        if (!this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", temp)))
        ////                        {
        ////                            this.uGridPara1.Rows[j].Hidden = true;
        ////                        }
        ////                    }
        ////                    else { this.uGridPara1.Rows[j].Hidden = false; }
        ////                }
        ////                for (int k = 0; k < dinosaurs.Count; k++)
        ////                {
        ////                    if (this.uGridPara1.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", dinosaurs[k])))
        ////                    {
        ////                        this.uGridPara1.Rows[j].Hidden = false;
        ////                    }
        ////                }

        ////            }
        ////        }
        ////        if (this.uCheckCHASE1_All.Checked)
        ////        {
        ////            for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
        ////            {
        ////                if (!this.uGridPara1.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara1.Rows[i].Hidden = true;
        ////                }
        ////                else
        ////                {
        ////                    this.uGridPara1.Rows[i].Hidden = false;
        ////                }
        ////            }
        ////        }
        ////        SetParaSeq(1);
        ////    }
        ////}
        ////#endregion
        ////#region intReqLotSeq == 2
        ////else if (intReqLotSeq == 2)
        ////{
        ////    //uGrid1_3
        ////    if (this.uCheckCHASE2_1.Checked || this.uCheckCHASE2_2.Checked || this.uCheckCHASE2_3.Checked || this.uCheckCHASE2_4.Checked || this.uCheckCHASE2_5.Checked || this.uCheckCHASE2_6.Checked)
        ////    {
        ////        for (int i = 0; i < dinosaurs.Count; i++)
        ////        {
        ////            string temp = dinosaurs[i];
        ////            string temp1 = string.Format("{0}{1}-{1}", strContains, temp);
        ////            for (int j = 0; j < this.uGridPara2.Rows.Count; j++)
        ////            {
        ////                if (!this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara2.Rows[j].Hidden = true;
        ////                }
        ////                if (this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains("TEMPERATURE"))
        ////                {
        ////                    if (!this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(temp1))
        ////                    {
        ////                        if (!this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", temp)))
        ////                        {
        ////                            this.uGridPara2.Rows[j].Hidden = true;
        ////                        }
        ////                    }
        ////                    else { this.uGridPara2.Rows[j].Hidden = false; }
        ////                }
        ////                for (int k = 0; k < dinosaurs.Count; k++)
        ////                {
        ////                    if (this.uGridPara2.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", dinosaurs[k])))
        ////                    {
        ////                        this.uGridPara2.Rows[j].Hidden = false;
        ////                    }
        ////                }
        ////            }
        ////        }
        ////        if (this.uCheckCHASE2_All.Checked)
        ////        {
        ////            for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
        ////            {
        ////                if (!this.uGridPara2.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara2.Rows[i].Hidden = true;
        ////                }
        ////                else
        ////                {
        ////                    this.uGridPara2.Rows[i].Hidden = false;
        ////                }
        ////            }
        ////        }
        ////        SetParaSeq(2);
        ////    }
        ////}
        ////#endregion
        ////#region intReqLotSeq == 3
        ////else if (intReqLotSeq == 3)
        ////{
        ////    //uGrid1_3
        ////    if (this.uCheckCHASE3_1.Checked || this.uCheckCHASE3_2.Checked || this.uCheckCHASE3_3.Checked || this.uCheckCHASE3_4.Checked || this.uCheckCHASE3_5.Checked || this.uCheckCHASE3_6.Checked)
        ////    {
        ////        for (int i = 0; i < dinosaurs.Count; i++)
        ////        {
        ////            string temp = dinosaurs[i];
        ////            string temp1 = string.Format("{0}{1}-{1}", strContains, temp);
        ////            for (int j = 0; j < this.uGridPara3.Rows.Count; j++)
        ////            {
        ////                if (!this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara3.Rows[j].Hidden = true;
        ////                }
        ////                if (this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains("TEMPERATURE"))
        ////                {

        ////                    if (!this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(temp1))
        ////                    {
        ////                        if (!this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", temp)))
        ////                        {
        ////                            this.uGridPara3.Rows[j].Hidden = true;
        ////                        }
        ////                    }
        ////                    else { this.uGridPara3.Rows[j].Hidden = false; }
        ////                }
        ////                for (int k = 0; k < dinosaurs.Count; k++)
        ////                {
        ////                    if (this.uGridPara3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(string.Format("{0}-{0}", dinosaurs[k])))
        ////                    {
        ////                        this.uGridPara3.Rows[j].Hidden = false;
        ////                    }
        ////                }

        ////            }
        ////        }
        ////        if (this.uCheckCHASE3_All.Checked)
        ////        {
        ////            for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
        ////            {
        ////                if (!this.uGridPara3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        ////                {
        ////                    this.uGridPara3.Rows[i].Hidden = true;
        ////                }
        ////                else
        ////                {
        ////                    this.uGridPara3.Rows[i].Hidden = false;
        ////                }
        ////            }
        ////        }
        ////        SetParaSeq(3);
        ////    }
        ////}
        ///#endregion
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //if (intReqLotSeq == 1)
        //{
        //    //uGrid1_3
        //    if (this.uCheckCHASE1_1.Checked || this.uCheckCHASE1_2.Checked || this.uCheckCHASE1_3.Checked || this.uCheckCHASE1_4.Checked || this.uCheckCHASE1_5.Checked || this.uCheckCHASE1_6.Checked || this.uCheckCHASE1_All.Checked)
        //    {
        //        for (int i = 0; i < this.uGridPara1.Rows.Count; i++)
        //        {
        //            if (!this.uGridPara1.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        //            {
        //                this.uGridPara1.Rows[i].Hidden = true;
        //            }
        //        }
        //    }
        //    SetParaSeq(1);
        //}
        //else if (intReqLotSeq == 2)
        //{
        //    //uGrid2_3
        //    if (this.uCheckCHASE2_1.Checked || this.uCheckCHASE2_2.Checked || this.uCheckCHASE2_3.Checked || this.uCheckCHASE2_4.Checked || this.uCheckCHASE2_5.Checked || this.uCheckCHASE2_6.Checked || this.uCheckCHASE2_All.Checked)
        //    {
        //        for (int i = 0; i < this.uGridPara2.Rows.Count; i++)
        //        {
        //            if (!this.uGridPara2.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        //            {
        //                this.uGridPara2.Rows[i].Hidden = true;
        //            }
        //        }
        //    }
        //    SetParaSeq(2);
        //}
        //else if (intReqLotSeq == 3)
        //{
        //    //uGrid3_3
        //    if (this.uCheckCHASE3_1.Checked || this.uCheckCHASE3_2.Checked || this.uCheckCHASE3_3.Checked || this.uCheckCHASE3_4.Checked || this.uCheckCHASE3_5.Checked || this.uCheckCHASE3_6.Checked || this.uCheckCHASE3_All.Checked)
        //        for (int i = 0; i < this.uGridPara3.Rows.Count; i++)
        //        {
        //            if (!this.uGridPara3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
        //            {
        //                this.uGridPara3.Rows[i].Hidden = true;
        //            }
        //        }
        //    SetParaSeq(3);
        //}

        #endregion
    }
}