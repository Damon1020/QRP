﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : CCS관리                                               */
/* 프로그램ID   : frmCCSZ0003.cs                                        */
/* 프로그램명   : CCS 등록                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// using add
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

using QRPINS.UI;
using System.IO;

using System.Net;

namespace QRPCCS.UI
{
    public partial class frmCCSZ0003_S : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        // 그리드 전역변수 선언
        private Infragistics.Win.UltraWinGrid.UltraGrid m_grdCCS;

        private int m_intTabIndex = 0;
        private int m_intStart = 0;
        private int m_intRow = 0;
        private List<string> m_strBOMCode = new List<string>(); // BOM자재정보
        //Debug모드를 위한 변수


        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        public frmCCSZ0003_S()
        {
            InitializeComponent();
        }

        private void frmCCSZ0003_Activated(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser InitToolBar = new QRPBrowser();
                InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_SysRes.GetString("SYS_USERID"), this.Name);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmCCSZ0003_Load(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
                // Title 설정
                titleArea.mfSetLabelText("CCS 의뢰현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

                //InitEvent(); 2013-01-25 CCS의뢰에서 의뢰한 테이터로 확인.
                SetToolAuth();

                // 컨트롤 초기화

                //SetRunMode();       // 디버깅 모드
                InitLabel();
                InitComboBox();
                InitGrid();
                initTab();
                InitText();
                InitButton();
                SetUpperChar();
                

                // 불량유형 Hidden처리
                this.uLabelFaultType1.Hide();
                this.uLabelFaultType2.Hide();
                this.uLabelFaultType3.Hide();
                this.uComboFaultType1.Hide();
                this.uComboFaultType2.Hide();
                this.uComboFaultType3.Hide();

                this.uGroupBoxContentsArea.Expanded = false;

                this.uDateSearchReqFromDate.MaskInput = "yyyy-mm-dd hh:mm:ss";
                this.uDateSearchReqToDate.MaskInput = "yyyy-mm-dd hh:mm:ss";

                this.uDateSearchReqFromDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 22:00:00");
                this.uDateSearchReqToDate.Value = DateTime.Now.ToString("yyyy-MM-dd 21:59:59");

                // 2012-10-10 합부판정컨트롤 편집불가로 변경(합격정보인데 사용자 입력실수로 불합격으로 보낼수있음)
                this.uOptionInspectResult1.Enabled = false;
                this.uOptionInspectResult1.Appearance.BackColor = Color.White;
                this.uOptionInspectResult2.Enabled = false;
                this.uOptionInspectResult2.Appearance.BackColor = Color.White;
                this.uOptionInspectResult3.Enabled = false;
                this.uOptionInspectResult3.Appearance.BackColor = Color.White;

                this.uOptionInspectResult3.ValueChanged += new EventHandler(uOptionInspectResult3_ValueChanged);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetUpperChar()
        {
            try
            {
                this.uTextInspectUserID1.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID1.CharacterCasing = CharacterCasing.Upper;

                this.uTextInspectUserID2.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID2.CharacterCasing = CharacterCasing.Upper;

                this.uTextInspectUserID3.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID3.CharacterCasing = CharacterCasing.Upper;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitEvent()
        {
            
            this.uCheckCHASE1_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);
            this.uCheckCHASE2_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);
            this.uCheckCHASE3_All.CheckedValueChanged += new System.EventHandler(this.uCheckCHASE_All_CheckedValueChanged);
            

            this.uCheckCHASE1_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE1_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE2_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_1.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_2.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_3.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_4.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_5.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);
            this.uCheckCHASE3_6.CheckedValueChanged += new EventHandler(uCheckCHASE_CheckedValueChanged);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 공통 이벤트

        // ContentsGroupBox 상태변화 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 200);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridCCSReqList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridCCSReqList.Height = 690;

                    for (int i = 0; i < uGridCCSReqList.Rows.Count; i++)
                    {
                        uGridCCSReqList.Rows[i].Fixed = false;
                    }
                    InitClear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 초기화 Method

        /// <summary>
        /// 디버깅 Method
        /// </summary>
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    //MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스타박스 초기화

        /// </summary>
        private void InitText()
        {
            try
            {
                this.uTextEtcDesc1.MaxLength = 200;
                this.uTextEtcDesc2.MaxLength = 200;
                this.uTextEtcDesc3.MaxLength = 200;
                this.uTextInspectUserID1.MaxLength = 20;
                this.uTextInspectUserID2.MaxLength = 20;
                this.uTextInspectUserID3.MaxLength = 20;
                this.uTextSearchEquipCode.MaxLength = 10;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextReceiptTime1.Text = "";
                this.uTextReceiptTime1.MaxLength = 10;
                this.uTextReceiptTime2.Text = "";
                this.uTextReceiptTime2.MaxLength = 10;
                this.uTextReceiptTime3.Text = "";
                this.uTextReceiptTime3.MaxLength = 10;

                //접수시간 초기하

                this.uDateReceiptDate1.Value = "";
                this.uDateReceiptDate2.Value = "";
                this.uDateReceiptDate3.Value = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                // 조회 Label
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchComplete, "완료CCS포함", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 헤더 Label
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCCSNo, "CCSNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkProcess, "작업공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelNowProcess, "현재공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProduct, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 1차 Label
                wLabel.mfSetLabel(this.uLabelReqType1, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo1, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqDate1, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason1, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction1, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultType1, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc1, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser1, "검사원", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInspectResult1, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEndDate1, "완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack1, "Stack", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser1, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE1, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDate1, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCCSCancel_1, "CCS취소여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile1, "上传文件1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelErrorImage1, "不良图片", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource1, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 2차 Label
                wLabel.mfSetLabel(this.uLabelReqType2, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo2, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqDate2, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason2, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction2, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultType2, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc2, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser2, "검사원", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInspectResult2, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEndDate2, "완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack2, "Stack", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser2, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE2, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDate2, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCCSCancel_2, "CCS취소여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile2, "上传文件2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelErrorImage2, "不良图片", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource2, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 3차 Label
                wLabel.mfSetLabel(this.uLabelReqType3, "의뢰사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo3, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqDate3, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCauseReason3, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCorrectAction3, "시정조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultType3, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc3, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser3, "검사원", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInspectResult3, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEndDate3, "완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStack3, "Stack", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser3, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCHASE3, "CHASE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDate3, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCCSCancel_3, "CCS취소여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile3, "上传文件3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelErrorImage3, "不良图片", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgramSource3, "程序来源", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonReceipt1, "접수", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonReceipt2, "접수", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonReceipt3, "접수", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCancleReceipt1, "접수 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCancleReceipt2, "접수 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCancleReceipt3, "접수 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonCCSCancel_1, "의뢰 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCCSCancel_2, "의뢰 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCCSCancel_3, "의뢰 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonProcExcel1, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonProcExcel2, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonProcExcel3, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);

                wButton.mfSetButton(this.uButtonQualExcel1, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonQualExcel2, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonQualExcel3, "Excel", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);

                this.uButtonCancleReceipt1.Click += new EventHandler(uButtonCancleReceipt1_Click);
                this.uButtonCancleReceipt2.Click += new EventHandler(uButtonCancleReceipt2_Click);
                this.uButtonCancleReceipt3.Click += new EventHandler(uButtonCancleReceipt3_Click);
                this.uButtonProcExcel1.Click += new EventHandler(uButtonProcExcel1_Click);
                this.uButtonProcExcel2.Click += new EventHandler(uButtonProcExcel2_Click);
                this.uButtonProcExcel3.Click += new EventHandler(uButtonProcExcel3_Click);
                this.uButtonQualExcel1.Click += new EventHandler(uButtonQualExcel_Click);
                this.uButtonQualExcel2.Click += new EventHandler(uButtonQualExcel2_Click);
                this.uButtonQualExcel3.Click += new EventHandler(uButtonQualExcel3_Click);
                this.uButtonReceipt1.Click += new EventHandler(uButtonReceipt1_Click);
                this.uButtonReceipt2.Click += new EventHandler(uButtonReceipt2_Click);
                this.uButtonReceipt3.Click += new EventHandler(uButtonReceipt3_Click);

                this.uButtonCCSCancel_1.Click += new EventHandler(uButtonCCSCancel_1_Click);
                this.uButtonCCSCancel_2.Click += new EventHandler(uButtonCCSCancel_2_Click);
                this.uButtonCCSCancel_3.Click += new EventHandler(uButtonCCSCancel_3_Click);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFaultType);

                String strLogInPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                DataTable dtFaultType = clsFaultType.mfReadInspectFaultTypeCombo(strLogInPlantCode, m_resSys.GetString("SYS_LANG"));

                // 1차 불량유형 콤보박스
                wCombo.mfSetComboEditor(this.uComboFaultType1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", false, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                // 2차 불량유형 콤보박스
                wCombo.mfSetComboEditor(this.uComboFaultType2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", false, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                // 3차 불량유형 콤보박스
                wCombo.mfSetComboEditor(this.uComboFaultType3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", false, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerCombo = new DataTable();
                if(dtCustomer.Rows.Count > 0)
                    dtCustomerCombo = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "전체", "CustomerCode", "CustomerName", dtCustomerCombo);

                //공정타입

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text.Trim();

                this.uComboSearchProcessGroup.Items.Clear();


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtProcessGroup = clsEquip.mfReadEquipArea_WithDetailProcessOperationType(strPlantCode, strEquipCode, strAreaCode);

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComboCode", "ComboName", dtProcessGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 탭 초기화

        /// </summary>
        private void initTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabCCS, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab1, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab2, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTab3, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region List(uGridCCSReqList) 그리드 초기화

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSReqList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqNo", "CCSNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqState", "상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "NowProcessCode", "현재공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "NowProcessName", "현재공정명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqUserID", "의뢰자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqDate", "의뢰일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "WaitTime", "대기시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReceiptDate", "접수일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ActionTime", "조치시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CompleteDate", "완료일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "TotalTime", "TotalTime", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqTime", "의뢰시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CCSReqTypeName", "의뢰사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "CCSCreateType", "CreateType", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "MESTFlag", "MES요청여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "ReqLotSeq", "의뢰Lot차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "MESTCompleteFlag", "MES완료전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "StepUserID", "조치자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSReqList, 0, "StepUserName", "조치자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridCCSReqList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSReqList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region 1차 생산Item(uGrid1_1), 품질Item(uGrid1_2) 그리드 초기화

                /// <summary>
                /// 생산Item Grid
                /// </summary>
                wGrid.mfInitGeneralGrid(this.uGrid1_1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME")); 

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_1, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid1_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건 그리드

                wGrid.mfInitGeneralGrid(this.uGrid1_3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "QualityValue", "品质适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "InspectResultFlag", "检查结果", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1_3, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1_1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1_1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid1_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid1_3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1_3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region 2차 생산Item(uGrid2_1), 품질Item(uGrid2_2) 그리드 초기화

                /// <summary>
                /// 생산Item Grid
                /// </summary>
                wGrid.mfInitGeneralGrid(this.uGrid2_1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_1, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid2_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid2_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건 그리드

                wGrid.mfInitGeneralGrid(this.uGrid2_3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "QualityValue", "品质适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "InspectResultFlag", "检查结果", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2_3, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid2_1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2_1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2_3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2_3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region 3차 생산Item(uGrid3_1), 품질Item(uGrid3_2) 그리드 초기화

                /// <summary>
                /// 생산Item Grid
                /// </summary>
                wGrid.mfInitGeneralGrid(this.uGrid3_1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "ProductItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_1, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                /// <summary>
                /// 품질Item Grid
                /// </summary>                
                wGrid.mfInitGeneralGrid(this.uGrid3_2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectCondition", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "QualityItemSS", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "InspectFaultTypeCode", "불량", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGrid3_2, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 가동조건 그리드

                wGrid.mfInitGeneralGrid(this.uGrid3_3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "ParaSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "LinkId", "连接Id", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "CCSparameterCode", "稼动条件", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "DATATYPE", "DATATYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "VALIDATIONTYPE", "VALIDATIONTYPE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "ParaVALUE", "ParaVALUE", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "LOWERLIMIT", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "UPPERLIMIT", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "InspectValue", "生产适用值", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "QualityValue", "品质适用值", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn.nnnnn", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "InspectResultFlag", "检查结果", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3_3, 0, "MaterialSpecName", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid3_1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3_1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3_2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3_2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3_3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3_3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                // 단위 Grid DropDown Column 설정
                DataTable dt = new DataTable();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);
                dt = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGrid1_1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid2_1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid3_1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // 규격단위
                wGrid.mfSetGridColumnValueList(this.uGrid1_1, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid2_1, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid3_1, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // 검사결과 드랍다운 설정                
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1_1, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid1_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid2_1, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid2_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid3_1, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid3_2, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);

                wGrid.mfSetGridColumnValueList(this.uGrid1_3, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid2_3, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGrid3_3, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);

                this.uGrid1_1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGrid2_1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGrid3_1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                //
                this.uGrid1_1.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid1_1.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid1_1.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid1_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid1_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid1_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid2_1.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid2_1.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid2_1.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid2_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid2_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid2_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid3_1.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid3_1.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid3_1.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGrid3_2.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGrid3_2.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGrid3_2.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                //Key Mapping Clear
                this.uGrid1_2.KeyActionMappings.Clear();
                this.uGrid2_2.KeyActionMappings.Clear();
                this.uGrid3_2.KeyActionMappings.Clear();

                this.uGrid1_3.KeyActionMappings.Clear();
                this.uGrid2_3.KeyActionMappings.Clear();
                this.uGrid3_3.KeyActionMappings.Clear();

                this.uGrid1_1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid1_1_InitializeRow);
                
                this.uGrid1_2.AfterCellUpdate +=new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid1_2_AfterCellUpdate);
                this.uGrid1_2.AfterExitEditMode += new EventHandler(uGrid1_2_AfterExitEditMode);
                this.uGrid1_2.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid1_2_InitializeRow);
                this.uGrid1_2.KeyUp += new KeyEventHandler(uGrid1_2_KeyUp);
                this.uGrid1_2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridQCItem_ClickCellButton);

                this.uGrid1_3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid1_3_AfterCellUpdate);
                this.uGrid1_3.KeyUp += new KeyEventHandler(uGrid1_3_KeyUp);
                
                this.uGrid2_1.InitializeRow +=new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid2_1_InitializeRow);
                
                this.uGrid2_2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid2_2_AfterCellUpdate);
                this.uGrid2_2.AfterExitEditMode += new EventHandler(uGrid2_2_AfterExitEditMode);
                this.uGrid2_2.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid2_2_InitializeRow);
                this.uGrid2_2.KeyUp += new KeyEventHandler(uGrid2_2_KeyUp);
                this.uGrid2_2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridQCItem_ClickCellButton);

                this.uGrid2_3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid2_3_AfterCellUpdate);
                this.uGrid2_3.KeyUp += new KeyEventHandler(uGrid2_3_KeyUp);
                
                this.uGrid3_1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid3_1_InitializeRow);
                
                this.uGrid3_2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid3_2_AfterCellUpdate);
                this.uGrid3_2.AfterExitEditMode += new EventHandler(uGrid3_2_AfterExitEditMode);
                this.uGrid3_2.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGrid3_2_InitializeRow);
                this.uGrid3_2.KeyUp += new KeyEventHandler(uGrid3_2_KeyUp);
                this.uGrid3_2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridQCItem_ClickCellButton);

                this.uGrid3_3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid3_3_AfterCellUpdate);
                this.uGrid3_3.KeyUp += new KeyEventHandler(uGrid3_3_KeyUp);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화면 초기화

        /// </summary>
        private void InitClear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strLogInPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                String strLogInUserID = m_resSys.GetString("SYS_USERID");
                String strLogInUserName = m_resSys.GetString("SYS_USERNAME");

                //헤더            
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextReqNo.Text = "";
                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextWorkProcessCode.Text = "";
                this.uTextWorkProcessName.Text = "";
                this.uTextNowProcessCode.Clear();
                this.uTextNowProcessName.Clear();
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextProductCode.Text = "";
                this.uTextProductName.Text = "";
                this.uTextPackage.Clear();
                this.uTextCustomerProductCode.Clear();
                //this.uTabCCS.Tabs[0].Selected = true;
                //1차 탭            
                this.uTextLotNo1.Text = "";
                this.uTextCCSReqTypeName1.Text = "";
                this.uTextCauseReason1.Text = "";
                this.uDateReqDate1.Value = DateTime.Now;
                this.uTextReqTime1.Text = "";
                this.uTextCorrectAction1.Text = "";
                //this.uComboFaultType1.Value = "";
                this.uDateCompleteDate1.Value = DateTime.Now;
                this.uTextCompleteTime1.Text = "";
                this.uTextInspectUserID1.Text = strLogInUserID;
                this.uTextInspectUserName1.Text = strLogInUserName;
                this.uTextEtcDesc1.Text = "";
                this.uOptionInspectResult1.CheckedIndex = 0;
                this.uCheckCompleteFlag1.CheckedValue = false;
                this.uTextReqUserID1.Clear();
                this.uTextReqUserName1.Clear();

                while (this.uGrid1_1.Rows.Count > 0)
                {
                    this.uGrid1_1.Rows[0].Delete(false);
                }
                while (this.uGrid1_2.Rows.Count > 0)
                {
                    this.uGrid1_2.Rows[0].Delete(false);
                }
                while (this.uGrid1_3.Rows.Count > 0)
                {
                    this.uGrid1_3.Rows[0].Delete(false);
                }

                this.uCheckCHASE1_1.Checked = false;
                this.uCheckCHASE1_2.Checked = false;
                this.uCheckCHASE1_3.Checked = false;
                this.uCheckCHASE1_4.Checked = false;
                this.uCheckCHASE1_5.Checked = false;
                this.uCheckCHASE1_6.Checked = false;
                this.uCheckCHASE1_All.Checked = false;

                this.uLabelCHASE1.Visible = false;
                this.uCheckCHASE1_1.Visible = false;
                this.uCheckCHASE1_2.Visible = false;
                this.uCheckCHASE1_3.Visible = false;
                this.uCheckCHASE1_4.Visible = false;
                this.uCheckCHASE1_5.Visible = false;
                this.uCheckCHASE1_6.Visible = false;
                this.uCheckCHASE1_All.Visible = false;

                this.uTextMESHoldTFlag1.Clear();
                this.uTextMESTResultFlag1.Clear();

                //2012-10-19 CCS취소
                this.uCheckCCSCancel_1.Visible = false;
                this.uCheckCCSCancel_1.Checked = false;
                this.uLabelCCSCancel_1.Visible = false;
                this.uButtonCCSCancel_1.Visible = false;

                //2차 탭
                this.uTextLotNo2.Text = "";
                this.uTextCCSReqTypeName2.Text = "";
                this.uTextCauseReason2.Text = "";
                this.uDateReqDate2.Value = DateTime.Now;
                this.uTextReqTime2.Text = "";
                this.uTextCorrectAction2.Text = "";
                //this.uComboFaultType2.Value = "";
                this.uDateCompleteDate2.Value = DateTime.Now;
                this.uTextCompleteTime2.Text = "";
                this.uTextInspectUserID2.Text = strLogInUserID;
                this.uTextInspectUserName2.Text = strLogInUserName;
                this.uTextEtcDesc2.Text = "";
                this.uOptionInspectResult2.CheckedIndex = 0;
                this.uCheckCompleteFlag2.CheckedValue = true;
                this.uTextReqUserID2.Clear();
                this.uTextReqUserName2.Clear();

                while (this.uGrid2_1.Rows.Count > 0)
                {
                    this.uGrid2_1.Rows[0].Delete(false);
                }
                while (this.uGrid2_2.Rows.Count > 0)
                {
                    this.uGrid2_2.Rows[0].Delete(false);
                }
                while (this.uGrid2_3.Rows.Count > 0)
                {
                    this.uGrid2_3.Rows[0].Delete(false);
                }

                this.uCheckCHASE2_1.Checked = false;
                this.uCheckCHASE2_2.Checked = false;
                this.uCheckCHASE2_3.Checked = false;
                this.uCheckCHASE2_4.Checked = false;
                this.uCheckCHASE2_5.Checked = false;
                this.uCheckCHASE2_6.Checked = false;
                this.uCheckCHASE2_All.Checked = false;

                this.uLabelCHASE2.Visible = false;
                this.uCheckCHASE2_1.Visible = false;
                this.uCheckCHASE2_2.Visible = false;
                this.uCheckCHASE2_3.Visible = false;
                this.uCheckCHASE2_4.Visible = false;
                this.uCheckCHASE2_5.Visible = false;
                this.uCheckCHASE2_6.Visible = false;
                this.uCheckCHASE2_All.Visible = false;

                this.uTextMESHoldTFlag2.Clear();
                this.uTextMESTResultFlag2.Clear();

                //2012-10-19 CCS취소
                this.uCheckCCSCancel_2.Visible = false;
                this.uCheckCCSCancel_2.Checked = false;
                this.uLabelCCSCancel_2.Visible = false;
                this.uButtonCCSCancel_2.Visible = false;

                //3차 탭
                this.uTextLotNo3.Text = "";
                this.uTextCCSReqTypeName3.Text = "";
                this.uTextCauseReason3.Text = "";
                this.uDateReqDate3.Value = DateTime.Now;
                this.uTextReqTime3.Text = "";
                this.uTextCorrectAction3.Text = "";
                //this.uComboFaultType3.Value = "";
                this.uDateCompleteDate3.Value = DateTime.Now;
                this.uTextCompleteTime3.Text = "";
                this.uTextInspectUserID3.Text = strLogInUserID;
                this.uTextInspectUserName3.Text = strLogInUserName;
                this.uTextEtcDesc3.Text = "";
                this.uOptionInspectResult3.CheckedIndex = 0;
                this.uOptionInspectResult3.Enabled = false;
                this.uCheckCompleteFlag3.CheckedValue = true;
                this.uTextReqUserID3.Clear();
                this.uTextReqUserName3.Clear();

                while (this.uGrid3_1.Rows.Count > 0)
                {
                    this.uGrid3_1.Rows[0].Delete(false);
                }
                while (this.uGrid3_2.Rows.Count > 0)
                {
                    this.uGrid3_2.Rows[0].Delete(false);
                }
                while (this.uGrid3_3.Rows.Count > 0)
                {
                    this.uGrid3_3.Rows[0].Delete(false);
                }

                this.uCheckCHASE3_1.Checked = false;
                this.uCheckCHASE3_2.Checked = false;
                this.uCheckCHASE3_3.Checked = false;
                this.uCheckCHASE3_4.Checked = false;
                this.uCheckCHASE3_5.Checked = false;
                this.uCheckCHASE3_6.Checked = false;
                this.uCheckCHASE3_All.Checked = false;

                this.uLabelCHASE3.Visible = false;
                this.uCheckCHASE3_1.Visible = false;
                this.uCheckCHASE3_2.Visible = false;
                this.uCheckCHASE3_3.Visible = false;
                this.uCheckCHASE3_4.Visible = false;
                this.uCheckCHASE3_5.Visible = false;
                this.uCheckCHASE3_6.Visible = false;
                this.uCheckCHASE3_All.Visible = false;

                this.uTextMESHoldTFlag3.Clear();
                this.uTextMESTResultFlag3.Clear();

                this.uTabCCS.Tabs[1].Enabled = false;
                this.uTabCCS.Tabs[2].Enabled = false;

                this.uDateReceiptDate1.Value = null;
                this.uDateReceiptDate2.Value = null;
                this.uDateReceiptDate3.Value = null;

                this.uTextReceiptTime1.Text = string.Empty;
                this.uTextReceiptTime2.Text = string.Empty;
                this.uTextReceiptTime3.Text = string.Empty;

                // PSTS 추가분
                this.uTextMESLockingFlag1.Text = "F";
                this.uTextMESLockingFlag2.Text = "F";
                this.uTextMESLockingFlag3.Text = "F";
                if (m_strBOMCode != null)
                    m_strBOMCode.Clear();   // BOM 자재코드 초기화

                //2012-10-19 CCS취소
                this.uCheckCCSCancel_3.Visible = false;
                this.uCheckCCSCancel_3.Checked = false;
                this.uLabelCCSCancel_3.Visible = false;
                this.uButtonCCSCancel_3.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        // 검색

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 생성
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLotNo = this.uTextSearchLotNo.Text;
                String strEquipCode = this.uTextSearchEquipCode.Text;
                String strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();
                String strReqFromDate = Convert.ToDateTime(this.uDateSearchReqFromDate.Value).ToString("yyyy-MM-dd HH:mm:ss");
                String strReqToDate = Convert.ToDateTime(this.uDateSearchReqToDate.Value).ToString("yyyy-MM-dd HH:mm:ss");
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strCompleteFlag = this.uCheckSearchCompleteFlag.CheckedValue.ToString().ToUpper().Substring(0, 1);
                //string strInspectResultFlag = this.uComboSearchInspectResult.Value.ToString();
                string strInspectResultFlag = string.Empty;
                string strAreaCode = this.uComboSearchArea.Value.ToString();

                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;

                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색 Method 호출
                DataTable dtSearch = clsHeader.mfReadCCSInspectReqH_Req(strPlantCode, strLotNo, strEquipCode, strProcessGroup, strReqFromDate, strReqToDate, strPackage, strCustomerCode
                                                                    , strCompleteFlag, strInspectResultFlag, m_resSys.GetString("SYS_LANG"), this.Name, strAreaCode);

                //완료를 체크한 경우 완료인 의뢰건만 보이게 한다.
                DataRow[] dr;
                DataTable dt = dtSearch.Clone();
                if (this.uCheckSearchCompleteFlag.Checked == true)
                {
                    dt = dtSearch;
                }
                else
                {
                    dr = dtSearch.Select("ReqState <> '완료'");
                    if (dr.Length > 0)
                    {
                        DataTable dtr = dr.CopyToDataTable();

                        DataView dv = new DataView(dtr);
                        //dv.Sort = "ReqDate DESC";
                        dv.Sort = "ReqNo DESC";
                        dt = dv.ToTable();
                    }
                }

                this.uGridCCSReqList.DataSource = dt; //dtSearch;
                this.uGridCCSReqList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                //////// MES 완료전송 안된것 색으로 표시
                //////for (int i = 0; i < this.uGridCCSReqList.Rows.Count; i++)
                //////{
                //////    if(this.uGridCCSReqList.Rows[i].Cells["MESTCompleteFlag"].Value.ToString().Equals("F") && this.uGridCCSReqList.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("T"))
                //////    {
                //////        this.uGridCCSReqList.Rows[i].Appearance.BackColor = Color.Salmon;
                //////    }
                //////}

                //// 정보가 있을 시 MESTFlag가 F,CSSCreateType이 M 인 경우 줄의 색을 변경하라 //
                //for (int i = 0; i < this.uGridCCSReqList.Rows.Count; i++)
                //{
                //    //if (this.uGridCCSReqList.Rows[i].Cells["MESTFlag"].Value.ToString() == "F" && this.uGridCCSReqList.Rows[i].Cells["CCSCreateType"].Value.ToString() == "M")
                //    if (this.uGridCCSReqList.Rows[i].Cells["InspectResult"].Value.ToString().Equals("NG") && this.uGridCCSReqList.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("T"))
                //    {
                //        this.uGridCCSReqList.Rows[i].Appearance.BackColor = Color.Salmon;
                //    }
                //}
                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridCCSReqList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장

        public void mfSave()
        {
            string strErrRtn = string.Empty;
            string strReqNo = string.Empty;
            string strReqSeq = string.Empty;
            string strUserIP = string.Empty;
            string strUserID = string.Empty;

            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                bool bolCheck = false;
                bool bolCheckPara = false;
                string strLang = m_resSys.GetString("SYS_LANG");
                strUserID = m_resSys.GetString("SYS_USERID");
                strUserIP = m_resSys.GetString("SYS_USERIP");

                if (this.uTabCCS.Tabs[2].Enabled == true && this.uTextLotNo3.Text != string.Empty)
                    this.uTabCCS.Tabs[2].Selected = true;
                else if (this.uTabCCS.Tabs[1].Enabled == true && this.uTextLotNo2.Text != string.Empty)
                    this.uTabCCS.Tabs[1].Selected = true;
                else
                    this.uTabCCS.Tabs[0].Selected = true;

                int intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString());

                Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEditor = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();

                #region 입력사항 확인

                // 헤더 필수사항 확인
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M001050", Infragistics.Win.HAlign.Right);
                    return;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty), "CCSInspectReqItemFaulty");
                QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty clsFaulty = new QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty();
                brwChannel.mfCredentials(clsFaulty);
                strReqNo = this.uTextReqNo.Text.Trim().Substring(0, 8);
                strReqSeq = this.uTextReqNo.Text.Trim().Substring(8, 4);
                //불량유형 정보 조회 매서드 실행
                DataTable dtFaulty = clsFaulty.mfReadInspectReqItemFaulty(this.uTextPlantCode.Text, strReqNo,
                                                                        strReqSeq, (intTabIndex+1).ToString(),
                                                                        "0","Q",m_resSys.GetString("SYS_LANG"));
                clsFaulty.Dispose();
                ///////////////////////////////////////////////////////////////////////////////////////////////////////// 2012-09-01 권종구

                //查询设备所属工程类型
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);
                DataTable dtEquipProcessGroup = clsEquip.mfReadEquipProcessGroup(this.uTextPlantCode.Text, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                clsEquip.Dispose();
                string strProcessGroupCode = dtEquipProcessGroup.Rows[0]["ProcessGroupCode"].ToString();

                if (intTabIndex == 0)
                {
                    // LotNo 확인
                    if (this.uTextLotNo1.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000036", Infragistics.Win.HAlign.Right);
                        //Focus
                        this.uTextLotNo1.Focus();
                        return;
                    }

                    ////// 불량시 불량명 입력 확인
                    ////if (this.uOptionInspectResult1.CheckedIndex.Equals(1))
                    ////{
                    ////    if (string.IsNullOrEmpty(this.uComboFaultType1.Value.ToString()) || this.uComboFaultType1.Value == DBNull.Value)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "불량명을 선택하여 주세요.", Infragistics.Win.HAlign.Right);
                    ////        this.uComboFaultType1.DropDown();
                    ////        return;
                    ////    }
                    ////}

                    // 검사진행여부 확인
                    int intIngCount = 0;
                    this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid1_2.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGrid1_2.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            //불량정보등록 정보 확인
                            if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid1_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , msg.GetMessge_Text("M000613", strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                                this.uGrid1_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                //this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            //if (this.uGrid1_2.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty))
                            //{
                            //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                        , msg.GetMessge_Text("M001264",strLang)
                            //                        , msg.GetMessge_Text("M000606",strLang)
                            //                        , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                            //                        , Infragistics.Win.HAlign.Right);

                            //    this.uGrid1_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                            //    this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}
                        }
                        else if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid1_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() > 0)
                        {
                            // 불량 수량이 0인데 POPUP창의 불량유형 정보가 있는경우 메세지 출력
                            // 번째 줄의 불량유형상세정보에 정보가 존재합니다. 확인해주십시요.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , (i + 1).ToString() + msg.GetMessge_Text("M001525", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                            this.uGrid1_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();


                            return;
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    ////if (this.uCheckCompleteFlag1.Enabled.Equals(false) && this.uTextMESTResultFlag1.Text.Equals("T") &&
                    ////    (this.uTextMESHoldTFlag1.Text.Equals("T") || !this.uOptionInspectResult1.CheckedIndex.Equals(1)))
                    ////{
                    ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "작성완료되어 수정할 수 없습니다.", Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}
                    ////else if (this.uCheckCompleteFlag1.Enabled.Equals(false))
                    ////{
                    ////    bool bolResultCheck = false;
                    ////    bool bolHoldCheck = false;

                    ////    if(this.uTextMESTResultFlag1.Text.Equals("F"))
                    ////        bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                    ////    if (!this.uTextMESHoldTFlag1.Text.Equals("T") && this.uOptionInspectResult1.CheckedIndex.Equals(1))
                    ////        bolHoldCheck = Save_MESHold(intTabIndex + 1);

                    ////    if (bolResultCheck && bolHoldCheck)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                                       "처리결과", "MES처리결과", "MES에 CCS결과정보와 Hold요청을 성공적으로 전송하였습니다.", Infragistics.Win.HAlign.Right);
                    ////    }
                    ////}

                    //FTP方式上传FailFile 2015-09-17
                    FileUpload_Ftp_FailFile(this.uTextReqNo.Text, m_intTabIndex);
                    //FTP方式上传ErrorImage 2015-09-17
                    FileUpload_Ftp_ErrorImage(this.uTextReqNo.Text, m_intTabIndex);

                     // 邹美娟等人可以修改已经制作完成的数据，其他人不可以
                    if (this.uCheckCompleteFlag1.Enabled.Equals(false) && this.uTextMESTResultFlag1.Text.Equals("T") && !(m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000991", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckCompleteFlag1.Enabled.Equals(false))
                    {
                        bool bolResultCheck = false;

                        if (this.uTextMESTResultFlag1.Text.Equals("F"))
                            bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                        if (bolResultCheck)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M000095", "M000094", Infragistics.Win.HAlign.Right);

                            mfSearch();
                            return;
                        }
                    }
                    if (this.uTextReceiptTime1.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001053", "M001320", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    for (int i = 0; i < this.uGrid1_3.Rows.Count; i++)
                    {
                        if (this.uGrid1_3.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            //규격을 벗어난 검사값이 있습니다. 저장하시겠습니까?
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }
                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGrid1_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid1_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGrid1_3.Rows[i].Hidden==false && (this.uGrid1_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid1_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                }
                else if (intTabIndex == 1)
                {
                    if (this.uTextLotNo2.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000036", Infragistics.Win.HAlign.Right);
                        //Focus
                        this.uTextLotNo2.Focus();
                        return;
                    }

                    ////// 불량시 불량명 입력 확인
                    ////if (this.uOptionInspectResult2.CheckedIndex.Equals(1))
                    ////{
                    ////    if (string.IsNullOrEmpty(this.uComboFaultType2.Value.ToString()) || this.uComboFaultType2.Value == DBNull.Value)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "불량명을 선택하여 주세요.", Infragistics.Win.HAlign.Right);
                    ////        this.uComboFaultType2.DropDown();
                    ////        return;
                    ////    }
                    ////}

                    // 검사진행여부 확인
                    int intIngCount = 0;
                    this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid2_2.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGrid2_2.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            //불량정보등록 정보 확인
                            if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid2_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , msg.GetMessge_Text("M000613", strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                                this.uGrid2_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                //this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            //if (this.uGrid2_2.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty))
                            //{
                            //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                        , msg.GetMessge_Text("M001264",strLang)
                            //                        , msg.GetMessge_Text("M000606",strLang)
                            //                        , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                            //                        , Infragistics.Win.HAlign.Right);

                            //    this.uGrid2_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                            //    this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}
                        }
                        else if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid2_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() > 0)
                        {
                            // 불량 수량이 0인데 POPUP창의 불량유형 정보가 있는경우 메세지 출력
                            // 번째 줄의 불량유형상세정보에 정보가 존재합니다. 확인해주십시요.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , (i + 1).ToString() + msg.GetMessge_Text("M001525", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                            this.uGrid2_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();


                            return;
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    ////if (this.uCheckCompleteFlag2.Enabled.Equals(false) && this.uTextMESTResultFlag2.Text.Equals("T") &&
                    ////    (this.uTextMESHoldTFlag2.Text.Equals("T") || !this.uOptionInspectResult2.CheckedIndex.Equals(1)))
                    ////{
                    ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "작성완료되어 수정할 수 없습니다.", Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}
                    ////else if (this.uCheckCompleteFlag2.Enabled.Equals(false))
                    ////{
                    ////    bool bolResultCheck = false;
                    ////    bool bolHoldCheck = false;

                    ////    if (this.uTextMESTResultFlag2.Text.Equals("F"))
                    ////        bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                    ////    if (!this.uTextMESHoldTFlag2.Text.Equals("T") && this.uOptionInspectResult2.CheckedIndex.Equals(1))
                    ////        bolHoldCheck = Save_MESHold(intTabIndex + 1);

                    ////    if (bolResultCheck && bolHoldCheck)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                                       "처리결과", "MES처리결과", "MES에 CCS결과정보와 Hold요청을 성공적으로 전송하였습니다.", Infragistics.Win.HAlign.Right);
                    ////    }
                    ////}

                    // 邹美娟等人可以修改已经制作完成的数据,其他人不可以
                    if (this.uCheckCompleteFlag2.Enabled.Equals(false) && this.uTextMESTResultFlag2.Text.Equals("T") && !(m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000991", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckCompleteFlag2.Enabled.Equals(false))
                    {
                        bool bolResultCheck = false;

                        if (this.uTextMESTResultFlag2.Text.Equals("F"))
                            bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                        if (bolResultCheck)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M000095", "M000094", Infragistics.Win.HAlign.Right);

                            mfSearch();
                            return;
                        }
                    }

                    if (this.uTextReceiptTime2.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001053", "M001320", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    for (int i = 0; i < this.uGrid2_3.Rows.Count; i++)
                    {
                        if (this.uGrid2_3.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            //규격을 벗어난 검사값이 있습니다. 저장하시겠습니까?
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }
                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGrid2_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid2_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGrid2_3.Rows[i].Hidden == false && (this.uGrid2_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid2_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                }
                else if (intTabIndex == 2)
                {
                    if (this.uTextLotNo3.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000036", Infragistics.Win.HAlign.Right);
                        //Focus
                        this.uTextLotNo3.Focus();
                        return;
                    }

                    ////// 불량시 불량명 입력 확인
                    ////if (this.uOptionInspectResult3.CheckedIndex.Equals(1))
                    ////{
                    ////    if (string.IsNullOrEmpty(this.uComboFaultType3.Value.ToString()) || this.uComboFaultType3.Value == DBNull.Value)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "불량명을 선택하여 주세요.", Infragistics.Win.HAlign.Right);
                    ////        this.uComboFaultType3.DropDown();
                    ////        return;
                    ////    }
                    ////}

                    // 검사진행여부 확인
                    int intIngCount = 0;
                    this.uGrid3_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid3_2.Rows[i].Cells["InspectIngFlag"].Value))
                            intIngCount++;

                        if (ReturnIntegerValue(this.uGrid3_2.Rows[i].Cells["FaultQty"].Value) > 0)
                        {
                            //불량정보등록 정보 확인
                            if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid3_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , msg.GetMessge_Text("M000613", strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                                this.uGrid3_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                                //this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            //if (this.uGrid3_2.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Equals(string.Empty))
                            //{
                            //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                        , msg.GetMessge_Text("M001264",strLang)
                            //                        , msg.GetMessge_Text("M000606",strLang)
                            //                        , msg.GetMessge_Text("M000613",strLang) + (i + 1).ToString() + msg.GetMessge_Text("M000566",strLang)
                            //                        , Infragistics.Win.HAlign.Right);

                            //    this.uGrid3_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();
                            //    this.uGrid3_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}
                        }
                        else if (dtFaulty.Select("ReqLotSeq ='" + (intTabIndex + 1) + "' AND  ReqItemSeq = '" + this.uGrid3_2.Rows[i].Cells["ReqItemSeq"].Value + "'").Count() > 0)
                        {
                            // 불량 수량이 0인데 POPUP창의 불량유형 정보가 있는경우 메세지 출력
                            // 번째 줄의 불량유형상세정보에 정보가 존재합니다. 확인해주십시요.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001264", strLang)
                                                            , msg.GetMessge_Text("M000606", strLang)
                                                            , (i + 1).ToString() + msg.GetMessge_Text("M001525", strLang)
                                                            , Infragistics.Win.HAlign.Right);

                            this.uGrid3_2.Rows[i].Cells["InspectFaultTypeCode"].Activate();


                            return;
                        }
                    }

                    if (!(intIngCount > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000183", "M001057", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    ////if (this.uCheckCompleteFlag3.Enabled.Equals(false) && this.uTextMESTResultFlag3.Text.Equals("T") &&
                    ////    (this.uTextMESHoldTFlag3.Text.Equals("T") || !this.uOptionInspectResult3.CheckedIndex.Equals(1)))
                    ////{
                    ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                                , "확인창", "저장 필수 입력사항 확인", "작성완료되어 수정할 수 없습니다.", Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}
                    ////else if (this.uCheckCompleteFlag3.Enabled.Equals(false))
                    ////{
                    ////    bool bolResultCheck = false;
                    ////    bool bolHoldCheck = false;

                    ////    if (this.uTextMESTResultFlag3.Text.Equals("F"))
                    ////        bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                    ////    if (!this.uTextMESHoldTFlag3.Text.Equals("T") && this.uOptionInspectResult3.CheckedIndex.Equals(1))
                    ////        bolHoldCheck = Save_MESHold(intTabIndex + 1);

                    ////    if (bolResultCheck && bolHoldCheck)
                    ////    {
                    ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                                       "처리결과", "MES처리결과", "MES에 CCS결과정보와 Hold요청을 성공적으로 전송하였습니다.", Infragistics.Win.HAlign.Right);
                    ////    }
                    ////}

                    // 邹美娟等人可以修改已经制作完成的数据,其他人不可以
                    if (this.uCheckCompleteFlag3.Enabled.Equals(false) && this.uTextMESTResultFlag3.Text.Equals("T") && !(m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000991", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    if (this.uTextReceiptTime3.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001053", "M001320", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckCompleteFlag3.Enabled.Equals(false))
                    {
                        bool bolResultCheck = false;

                        if (this.uTextMESTResultFlag3.Text.Equals("F"))
                            bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                        if (bolResultCheck)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M000095", "M000094", Infragistics.Win.HAlign.Right);

                            mfSearch();
                            return;
                        }
                    }
                    for (int i = 0; i < this.uGrid3_3.Rows.Count; i++)
                    {
                        if (this.uGrid3_3.Rows[i].Appearance.BackColor == Color.Tomato && bolCheck == false)
                        {
                            //규격을 벗어난 검사값이 있습니다. 저장하시겠습니까?
                            bolCheck = true;
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001053", "M001420", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                            {
                                bolCheck = false;
                                return;
                            }
                        }
                        //后道的CCS委托稼动条件必须全部输入，其他工程的稼动条件不必全部输入 2016-03-08
                        if (strProcessGroupCode != "BACK")
                        {
                            if (this.uGrid3_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid3_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty))
                            {
                                if (bolCheckPara == false)
                                {
                                    bolCheckPara = true;
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001053", "M001421", Infragistics.Win.HAlign.Right);

                                    if (Result == DialogResult.No)
                                    {
                                        bolCheckPara = false;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (this.uGrid3_3.Rows[i].Hidden == false && (this.uGrid3_3.Rows[i].Cells["InspectValue"].Value == null || this.uGrid3_3.Rows[i].Cells["InspectValue"].Value.ToString().Equals(string.Empty)))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001571", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                }

                #endregion
                ////// 저장여부 확인 메세지
                ////if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                ////{
                // ActiveCell 첫번째 행 첫번째 열로 이동                                        
                if (intTabIndex == 0)
                {
                    this.uGrid1_2.ActiveCell = this.uGrid1_2.Rows[0].Cells[0];
                    m_intStart = this.uGrid1_2.DisplayLayout.Bands[0].Columns["1"].Index;
                    uCheckEditor = this.uCheckCompleteFlag1;
                }
                else if (intTabIndex == 1)
                {
                    this.uGrid2_2.ActiveCell = this.uGrid2_2.Rows[0].Cells[0];
                    m_intStart = this.uGrid2_2.DisplayLayout.Bands[0].Columns["1"].Index;
                    uCheckEditor = this.uCheckCompleteFlag2;
                }
                else if (intTabIndex == 2)
                {
                    this.uGrid3_2.ActiveCell = this.uGrid3_2.Rows[0].Cells[0];
                    m_intStart = this.uGrid3_2.DisplayLayout.Bands[0].Columns["1"].Index;
                    uCheckEditor = this.uCheckCompleteFlag3;
                }

                // 저장할 DataTable 설정 Method 호출
                DataTable dtHeader = SaveHeaderInfo();
                DataTable dtLot = SaveLotInfo();
                DataTable dtItem = SaveItemInfo();
                DataTable dtMeasure = SaveResultMeasure(m_intStart);
                DataTable dtCount = SaveResultCount(m_intStart);
                DataTable dtOkNo = SaveResultOkNg(m_intStart);
                DataTable dtDesc = SaveResultDesc(m_intStart);
                DataTable dtSelect = SaveResultSelect(m_intStart);
                DataTable dtPara = SaveParaInfo();
                //DataTable dtEQPDown = SaveEQPDowninfo(); // 따로 MES 전송함.

                int intFault = 0;
                for (int i = 0; i < dtItem.Rows.Count; i++)
                {
                    string strFaultQty = string.Format("{0:n0}", dtItem.Rows[i]["FaultQty"]);
                    if (!strFaultQty.Equals("0"))
                    {
                        intFault++;
                    }
                }

                if (intFault != 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001298", "M001309", Infragistics.Win.HAlign.Right);

                    if (DResult == DialogResult.No)
                        return;
                }

                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;

                if (m_bolDebugMode == false)
                {
                    // BL연결
                    //QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // Method 호출
                strErrRtn = clsHeader.mfSaveCCSInsepctReqH_PSTS(dtHeader, dtLot, dtItem, dtMeasure, dtCount, dtOkNo, dtDesc, dtSelect, dtPara
                                    , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                clsHeader.Dispose();

                string strMes = string.Empty;
                // 처리결과에 따른 메세지 박스

                if (ErrRtn.ErrNum == 0)
                {
                    //FileUpload_Header(this.uTextReqNo.Text, m_intTabIndex);

                    bool bolResultCheck = false;
                    bool bolDownCheck = false;

                    if (uCheckEditor.Checked)
                    {
                        bolResultCheck = Save_MESResult(m_intTabIndex + 1);

                        bolDownCheck = Save_MES_EQPDown();

                        // 2012-11-08 추가 Start 
                        // 2012-11-08 bolDownCheck = False 이면(MES전송 실패시) CompleteFlag = 'F', 완료일시 Null 처리 
                        // (1차의뢰시 설비 Lacking을 건상태에서 MES전송이 실패됬을 경우)
                        //if (this.uTextMESLockingFlag1.Text.Equals("T") && !bolDownCheck)  // 2012-11-19 주석 기존 설비UnLacking 실패시 가능 - > 설비Lacking or UnLacking
                        if (!bolDownCheck)
                        {
                            int intReqLotSeq = m_intTabIndex + 1;

                            // 2012-11-19 추가
                            string strFail = string.Empty;
                            if (this.uTextMESLockingFlag1.Text.Equals("T"))     // 1차의뢰에서 Lacking 발생된설비면 FAIL
                                strFail = "FAIL";
                            else if (this.uTextMESLockingFlag1.Text.Equals("F")) // W/B Lacking 실패시 쿼리에서 MES 설비상태 확인 후 MESLackFlag 설정
                                strFail = "CHECK";
                            else
                                strFail = "FAIL";
                            // 2012-11-19 추가 끝

                            CCSCATCH(strFail);  // 2012-12-03 추가

                        }
                        // 2012-11-08 추가 End

                        //if (bolResultCheck && bolDownCheck)
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                                   "M001135", "M000095", "M000938", Infragistics.Win.HAlign.Right);

                        //    // List 갱신
                        //    mfSearch();
                        //}
                       
                    }
                    //else
                    //{
                    //    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                    //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                        "M001135", "M001037", "M000930",
                    //                        Infragistics.Win.HAlign.Right);
                    //    // List 갱신
                    //    mfSearch();
                    //}


                    bool bolResult = CCSStateCheck(uCheckEditor.Checked);    // false면 매서드 실패 or I/F실패

                    // 작성완료를 체크안한경우
                    if (!uCheckEditor.Checked)
                    {
                        if (bolResult)   // 최종확인 완료
                            strMes = "M000930";
                        else            // 최종확인 실패
                            strMes = "M000953";

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M000095", strMes, Infragistics.Win.HAlign.Right);
                    }
                    else // 작성완료를 체크한 경우
                    {
                        //if (bolResultCheck && bolDownCheck)
                        //{
                        //    if (bolResult)   // 최종확인 완료
                        //        strMes = "M000938";
                        //    else            // 최종확인 실패
                        //        strMes = "M000953";
                        //}
                        //else
                        //{
                        //    if (bolResult)   // 최종확인 완료
                        //        strMes = "M000938";
                        //    else            // 최종확인 실패
                        //        strMes = "M000953";
                        //}

                        if (bolResult)   // 최종확인 완료
                            strMes = "M000938";
                        else            // 최종확인 실패
                            strMes = "M000953";

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M000095", strMes, Infragistics.Win.HAlign.Right);
                    }

                    if (bolResult)   // 최종확인 완료
                        mfSearch();  // List 갱신
                }
                else
                {

                        
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135",strLang)
                                            , msg.GetMessge_Text("M001037", strLang), strMes,
                                            Infragistics.Win.HAlign.Right);
                    
                }
                
                ////}
            }
            catch (Exception ex)
            {
                // 2012-12-03 추가
                string strFail = string.Empty;
                if (this.uTextMESLockingFlag1.Text.Equals("T"))     // 1차의뢰에서 Lacking 발생된설비면 FAIL
                    strFail = "CATCH";
                else if (this.uTextMESLockingFlag1.Text.Equals("F")) // W/B Lacking 실패시 쿼리에서 MES 설비상태 확인 후 MESLackFlag 설정
                    strFail = "WBCATCH";
                else
                    strFail = "CATCH";

                CCSCATCH(strFail);
                // 2012-12-03 추가 끝

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제
        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 생성
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 출력
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 엑셀
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridCCSReqList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridCCSReqList);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 1.검색조건 이벤트, 메소드


        // 검색조건 : 공정정보 Load (공장 선택 시 )
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtPackage = new DataTable();
                DataTable dtArea = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcessGroup.Items.Clear();
                this.uComboSearchPackage.Items.Clear();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    // 검색조건 Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // AreaCombo
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);

                    dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "Package", "ComboName", dtPackage);

                wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "AreaCode", "AreaName", dtArea);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 : 설비정보 팝업창


        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0005 frmPOP = new frmPOP0005();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchEquipCode.Text = frmPOP.EquipCode;
                this.uTextSearchEquipName.Text = frmPOP.EquipName;

                SetSearchProcessGroupCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 2.리스트 더블클릭시 상세정보 조회

        // 리스트 더블클릭시 상세정보 조회
        private void uGridCCSReqList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["CCSReqTypeName"].Value.ToString() != "")
                {
                    //상세정보 초기화

                    InitClear();

                    // 더블클릭된 행 고정
                    e.Row.Fixed = true;
                    // ContentsArea 펼침 상태로

                    this.uGroupBoxContentsArea.Expanded = true;

                    String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                    String strReqNo = e.Row.Cells["ReqNo"].Value.ToString().Substring(0, 8);
                    String strReqSeq = e.Row.Cells["ReqNo"].Value.ToString().Substring(8, 4);
                    int intReqLotSeq = ReturnIntegerValue(e.Row.Cells["ReqLotSeq"].Value.ToString());
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 헤더 상세조회 Method 호출
                    SearchInspectReqHDetail(strPlantCode, strReqNo, strReqSeq);

                    // Lot Grid 데이터 조회 Method 호출, Item Grid 데이터 조회 Method 호출
                    SearchInspectReqLot(strPlantCode, strReqNo, strReqSeq);

                    //WinGrid grd = new WinGrid();
                    //grd.mfSetAutoResizeColWidth(this.uGrid1_1, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid1_2, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid1_3, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid2_1, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid2_2, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid2_3, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid3_1, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid3_2, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGrid3_3, 0);

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    this.uTabCCS.Tabs[intReqLotSeq - 1].Selected = true;

                    SetUnvisivleGrid(intReqLotSeq);
                }
                else
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001096", "M000035", Infragistics.Win.HAlign.Right);
                    return;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strReqNo"></param>
        /// <param name="strReqSeq"></param>
 
        private void SearchInspectReqHDetail(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHaeder;

                QRPBrowser brwChannel = new QRPBrowser();
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHaeder = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHaeder);
                }
                else
                {
                    clsHaeder = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                DataTable dtHeader = clsHaeder.mfReadCCSInspectReqHDetail(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
               

                if (dtHeader.Rows.Count == 0)
                    return;

                this.uTextPlantCode.Text = dtHeader.Rows[0]["PlantCode"].ToString();
                this.uTextPlantName.Text = dtHeader.Rows[0]["PlantName"].ToString();
                this.uTextReqNo.Text = dtHeader.Rows[0]["ReqNo"].ToString();
                this.uTextCustomerCode.Text = dtHeader.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtHeader.Rows[0]["CustomerName"].ToString();
                this.uTextWorkProcessCode.Text = dtHeader.Rows[0]["WorkProcessCode"].ToString();
                this.uTextWorkProcessName.Text = dtHeader.Rows[0]["WorkProcessName"].ToString();
                this.uTextNowProcessCode.Text = dtHeader.Rows[0]["NowProcessCode"].ToString();
                this.uTextNowProcessName.Text = dtHeader.Rows[0]["NowProcessName"].ToString();
                this.uTextEquipCode.Text = dtHeader.Rows[0]["EquipCode"].ToString();
                this.uTextEquipName.Text = dtHeader.Rows[0]["EquipName"].ToString();
                this.uTextProductCode.Text = dtHeader.Rows[0]["ProductCode"].ToString();
                this.uTextProductName.Text = dtHeader.Rows[0]["ProductName"].ToString();
                this.uTextCustomerProductCode.Text = dtHeader.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextPackage.Text = dtHeader.Rows[0]["PACKAGE"].ToString();

                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                brwChannel.mfCredentials(clsHeader);

                // 기존에 존재하는 표준번호가 있는지 확인하는 Method 호출
                DataTable dtInsSpec = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode, this.uTextPackage.Text, this.uTextCustomerCode.Text);
                //DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode,  m_Pakcage);                    
                
                if (dtInsSpec.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsSpec.Rows.Count; i++)
                    {
                        this.uTextStdNumber.Text = dtInsSpec.Rows[i]["StdNumber"].ToString() + dtInsSpec.Rows[i]["StdSeq"].ToString();
                    }
                }

                clsHaeder.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot 상세정보 조회 
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strReqNo"></param>
        /// <param name="strReqSeq"></param>
 
        private void SearchInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                ResourceSet m_ResSys = new ResourceSet(SysRes.SystemInfoRes);

                String strLogInPlantCode = m_ResSys.GetString("SYS_PLANTCODE");
                String strLogInUserID = m_ResSys.GetString("SYS_USERID");
                String strLogInUserName = m_ResSys.GetString("SYS_USERNAME");

                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);
                }
                else
                {
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                DataTable dtLot = clsLot.mfReadCCSInspectReqLot_PSTS(strPlantCode, strReqNo, strReqSeq, m_ResSys.GetString("SYS_LANG"));

                // 루프를 돌며 의뢰차수별 정보를 컨트롤에 바인드
                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    // 1차의뢰 정보
                    if (i == 0)
                    {
                        this.uTextCCSReqTypeCode1.Text = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextCCSReqTypeName1.Text = dtLot.Rows[i]["CCSReqTypeName"].ToString();
                        this.uTextLotNo1.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uDateReqDate1.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextReqTime1.Text = dtLot.Rows[i]["ReqTime"].ToString();
                        this.uTextCompleteTime1.Text = dtLot.Rows[i]["CompleteTime"].ToString();
                        this.uTextCauseReason1.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction1.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uComboFaultType1.Value = dtLot.Rows[i]["InspectFaultTypeCode"].ToString();
                        this.uTextReqUserID1.Text = dtLot.Rows[i]["ReqUserID"].ToString();
                        this.uTextReqUserName1.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uDateReceiptDate1.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime1.Value = dtLot.Rows[i]["ReceiptTime"].ToString();
                        if (dtLot.Rows[i]["CompleteDate"].ToString() != "")
                        {
                            this.uDateCompleteDate1.Value = dtLot.Rows[i]["CompleteDate"].ToString();
                        }
                        if (dtLot.Rows[i]["InspectUserID"].ToString() == "")
                        {
                            this.uTextInspectUserID1.Text = strLogInUserID;
                            this.uTextInspectUserName1.Text = strLogInUserName;
                        }
                        else
                        {
                            this.uTextInspectUserID1.Text = dtLot.Rows[i]["InspectUserID"].ToString();
                            this.uTextInspectUserName1.Text = dtLot.Rows[i]["InspectUserName"].ToString();
                        }
                        this.uTextEtcDesc1.Text = dtLot.Rows[i]["EtcDesc"].ToString();
                        if (dtLot.Rows[i]["InspectResultFlag"].ToString() != "")
                            this.uOptionInspectResult1.Value = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // 邹美娟等人可以修改已经制作完成的数据
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T" && (m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                        {
                            this.uCheckCompleteFlag1.CheckedValue = true;
                            this.uCheckCompleteFlag1.Enabled = false;
                            this.uGrid1_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid1_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                        }
                        else if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T")
                        {
                            this.uCheckCompleteFlag1.CheckedValue = true;
                            this.uCheckCompleteFlag1.Enabled = false;
                            this.uGrid1_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGrid1_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                        }
                        else
                        {
                            this.uCheckCompleteFlag1.CheckedValue = false;
                            this.uCheckCompleteFlag1.Enabled = true;
                            this.uGrid1_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid1_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                        }
                        this.uTextStackSeq1.Text = dtLot.Rows[i]["StackSeq"].ToString();
                        this.uTextMESHoldTFlag1.Text = dtLot.Rows[i]["MESHoldTFlag"].ToString();
                        this.uTextMESTResultFlag1.Text = dtLot.Rows[i]["MESTCompleteFlag"].ToString();

                        // 접수시간이 있을경우는 접수취소버튼 활성화, 접수시간이 업을경우 접수버튼 활성화
                        if (this.uTextReceiptTime1.Text.Equals(string.Empty))
                        {
                            this.uButtonCancleReceipt1.Visible = false;
                            this.uButtonReceipt1.Visible = true;

                            // 2012-11-01 CCS의뢰정보가 접수전이면 CCS의뢰취소가능 
                            this.uCheckCCSCancel_1.Visible = true;
                            this.uLabelCCSCancel_1.Visible = true;
                            this.uButtonCCSCancel_1.Visible = true;
                        }
                        else
                        {
                            this.uButtonReceipt1.Visible = false;
                            this.uButtonCancleReceipt1.Visible = true;

                            // 2012-11-01 CCS의뢰정보가 접수전이면 CCS의뢰취소가능 
                            this.uCheckCCSCancel_1.Visible = false;
                            this.uLabelCCSCancel_1.Visible = false;
                            this.uButtonCCSCancel_1.Visible = false;
                        }

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE1.Visible = true;
                            this.uCheckCHASE1_All.Visible = true;
                            this.uCheckCHASE1_1.Visible = true;
                            this.uCheckCHASE1_2.Visible = true;
                            this.uCheckCHASE1_3.Visible = true;
                            this.uCheckCHASE1_4.Visible = true;
                            this.uCheckCHASE1_5.Visible = true;
                            this.uCheckCHASE1_6.Visible = true;

                            this.uCheckCHASE1_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE1_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE1_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE1_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE1_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE1_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE1_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource1.Visible = true;
                            this.uTextProgramSource1.Visible = true;

                            this.uTextProgramSource1.Text = dtLot.Rows[i]["ProgramSource"].ToString();

                        }
                        else
                        {
                            this.uLabelProgramSource1.Visible = false;
                            this.uTextProgramSource1.Visible = false;
                        }

                        //1차탭 생산 아이템 검사값 정보 조회
                        SearchInspectReqItemValue_Product(strPlantCode, strReqNo, strReqSeq, i + 1);
                        //1차탭 품질 아이템 검사값 정보 조회
                        SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);
                        // 1차탭 가동조건 정보 조회
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        if (dtLot.Rows[i]["InspectResultFlag"].ToString().Equals("NG") && dtLot.Rows[i]["CompleteFlag"].ToString().Equals("T"))
                        {
                            this.uTabCCS.Tabs[1].Enabled = true;
                        }

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        this.uTextMESLockingFlag1.Text = dtLot.Rows[i]["MESLockFlag"].ToString();

                        this.uTextFile1.Text = dtLot.Rows[i]["CCSFailFile"].ToString();
                        this.uTextErrorImage1.Text = dtLot.Rows[i]["CCSErrorImage"].ToString();
                        // 2012-11-01 주석처리.
                        //if (this.uCheckCompleteFlag1.Checked
                        //    && this.uOptionInspectResult1.Value.ToString().Equals("OK"))
                        //{
                        //    // CCS 완료된정보 일경우 CCS으뢰취소가능 2012-10-19 --> 접수전 CCS취소시 정보삭제
                        //    this.uCheckCCSCancel_1.Visible = true;
                        //    this.uLabelCCSCancel_1.Visible = true;
                        //    this.uButtonCCSCancel_1.Visible = true;

                        //    // CCSCancelFlag 추가 2012-10-19
                        //    if (dtLot.Rows[i]["CCSCancelFlag"].ToString().Trim().Equals("T"))
                        //    {
                        //        this.uCheckCCSCancel_1.Checked = true;
                        //        this.uCheckCCSCancel_1.Enabled = false;
                        //    }
                        //    else
                        //    {
                        //        this.uCheckCCSCancel_1.Checked = false;
                        //        this.uCheckCCSCancel_1.Enabled = true;
                        //    }

                        //}
                        //else
                        //{
                        //    // CCS 완료 X 정보 일경우 CCS의뢰취소불가능 2012-10-19
                        //    this.uCheckCCSCancel_1.Visible = false;
                        //    this.uLabelCCSCancel_1.Visible = false;
                        //    this.uButtonCCSCancel_1.Visible = false;
                        //}


                    }
                    else if (i == 1)
                    {
                        this.uTextCCSReqTypeCode2.Text = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextCCSReqTypeName2.Text = dtLot.Rows[i]["CCSReqTypeName"].ToString();
                        this.uTextLotNo2.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uDateReqDate2.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextCauseReason2.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction2.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uComboFaultType2.Value = dtLot.Rows[i]["InspectFaultTypeCode"].ToString();
                        this.uTextReqUserID2.Text = dtLot.Rows[i]["ReqUserID"].ToString();
                        this.uTextReqUserName2.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uDateReceiptDate2.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime2.Value = dtLot.Rows[i]["ReceiptTime"].ToString();
                        if (dtLot.Rows[i]["CompleteDate"].ToString() != "")
                        {
                            this.uDateCompleteDate2.Value = dtLot.Rows[i]["CompleteDate"].ToString();
                        }
                        this.uTextReqTime2.Text = dtLot.Rows[i]["ReqTime"].ToString();
                        this.uTextCompleteTime2.Text = dtLot.Rows[i]["CompleteTime"].ToString();
                        if (dtLot.Rows[i]["InspectUserID"].ToString() == "")
                        {
                            this.uTextInspectUserID2.Text = strLogInUserID;
                            this.uTextInspectUserName2.Text = strLogInUserName;
                        }
                        else
                        {
                            this.uTextInspectUserID2.Text = dtLot.Rows[i]["InspectUserID"].ToString();
                            this.uTextInspectUserName2.Text = dtLot.Rows[i]["InspectUserName"].ToString();
                        }
                        this.uTextEtcDesc2.Text = dtLot.Rows[i]["EtcDesc"].ToString();
                        if (dtLot.Rows[i]["InspectResultFlag"].ToString() != "")
                            this.uOptionInspectResult2.Value = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // 邹美娟等人可以修改已经制作完成的数据
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T" && (m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                        {
                            this.uCheckCompleteFlag2.CheckedValue = true;
                            this.uCheckCompleteFlag2.Enabled = false;
                            this.uGrid2_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid2_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                        }
                        else if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T")
                        {
                            this.uCheckCompleteFlag2.CheckedValue = true;
                            this.uCheckCompleteFlag2.Enabled = false;
                            this.uGrid2_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGrid2_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        }
                        else
                        {
                            this.uCheckCompleteFlag2.CheckedValue = false;
                            this.uCheckCompleteFlag2.Enabled = true;
                            this.uGrid2_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid2_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                        }
                        // 접수시간이 있을경우는 접수취소버튼 활성화, 접수시간이 업을경우 접수버튼 활성화

                        if (this.uTextReceiptTime2.Text == "")
                        {
                            this.uButtonCancleReceipt2.Visible = false;
                            this.uButtonReceipt2.Visible = true;
                        }
                        else
                        {
                            this.uButtonReceipt2.Visible = false;
                            this.uButtonCancleReceipt2.Visible = true;
                        }
                        this.uTextStackSeq2.Text = dtLot.Rows[i]["StackSeq"].ToString();
                        this.uTextMESHoldTFlag2.Text = dtLot.Rows[i]["MESHoldTFlag"].ToString();
                        this.uTextMESTResultFlag2.Text = dtLot.Rows[i]["MESTCompleteFlag"].ToString();

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE2.Visible = true;
                            this.uCheckCHASE2_All.Visible = true;
                            this.uCheckCHASE2_1.Visible = true;
                            this.uCheckCHASE2_2.Visible = true;
                            this.uCheckCHASE2_3.Visible = true;
                            this.uCheckCHASE2_4.Visible = true;
                            this.uCheckCHASE2_5.Visible = true;
                            this.uCheckCHASE2_6.Visible = true;

                            this.uCheckCHASE2_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE2_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE2_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE2_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE2_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE2_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE2_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource2.Visible = true;
                            this.uTextProgramSource2.Visible = true;

                            this.uTextProgramSource2.Text = dtLot.Rows[i]["ProgramSource"].ToString();

                        }
                        else
                        {
                            this.uLabelProgramSource2.Visible = false;
                            this.uTextProgramSource2.Visible = false;
                        }

                        //2차탭 아이템 검사항목 정보 조회
                        SearchInspectReqItemValue_Product(strPlantCode, strReqNo, strReqSeq, i + 1);
                        //2차탭 아이템 검사값 정보 조회
                        SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);
                        // 2차탭 가동조건 정보 조회
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        if (dtLot.Rows[i]["InspectResultFlag"].ToString().Equals("NG") && dtLot.Rows[i]["CompleteFlag"].ToString().Equals("T"))
                        {
                            this.uTabCCS.Tabs[2].Enabled = true;
                        }

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        //this.uTextMESLockingFlag2.Text = dtLot.Rows[i]["MESLockFlag"].ToString();
                        this.uTextMESHoldTFlag2.Text = this.uTextMESLockingFlag1.Text;

                        this.uTextFile2.Text = dtLot.Rows[i]["CCSFailFile"].ToString();
                        this.uTextErrorImage2.Text = dtLot.Rows[i]["CCSErrorImage"].ToString();

                        //// CCSCancelFlag 추가 2012-10-19 -> 2012-11-01 주석
                        //if (this.uCheckCompleteFlag2.Checked
                        //    && this.uOptionInspectResult2.Value.ToString().Equals("OK"))
                        //{
                        //    // CCS 완료된정보 일경우 CCS으뢰취소가능 2012-10-19
                        //    this.uCheckCCSCancel_2.Visible = true;
                        //    this.uLabelCCSCancel_2.Visible = true;
                        //    this.uButtonCCSCancel_2.Visible = true;

                        //    if (dtLot.Rows[i]["CCSCancelFlag"].ToString().Trim().Equals("T"))
                        //    {
                        //        this.uCheckCCSCancel_2.Checked = true;
                        //        this.uCheckCCSCancel_2.Enabled = false;
                        //    }
                        //    else
                        //    {
                        //        this.uCheckCCSCancel_2.Checked = false;
                        //        this.uCheckCCSCancel_2.Enabled = true;
                        //    }

                        //}
                        //else
                        //{
                        //    // CCS 완료 X 정보 일경우 CCS의뢰취소불가능 2012-10-19
                        //    this.uCheckCCSCancel_2.Visible = false;
                        //    this.uLabelCCSCancel_2.Visible = false;
                        //    this.uButtonCCSCancel_2.Visible = false;
                        //}
                    }
                    else if (i == 2)
                    {
                        this.uTextCCSReqTypeCode3.Text = dtLot.Rows[i]["CCSReqTypeCode"].ToString();
                        this.uTextCCSReqTypeName3.Text = dtLot.Rows[i]["CCSReqTypeName"].ToString();
                        this.uTextLotNo3.Text = dtLot.Rows[i]["LotNo"].ToString();
                        this.uDateReqDate3.Value = dtLot.Rows[i]["ReqDate"].ToString();
                        this.uTextCauseReason3.Text = dtLot.Rows[i]["CauseReason"].ToString();
                        this.uTextCorrectAction3.Text = dtLot.Rows[i]["CorrectAction"].ToString();
                        this.uComboFaultType3.Value = dtLot.Rows[i]["InspectFaultTypeCode"].ToString();
                        this.uTextReqUserID3.Text = dtLot.Rows[i]["ReqUserID"].ToString();
                        this.uTextReqUserName3.Text = dtLot.Rows[i]["ReqUserName"].ToString();
                        this.uDateReceiptDate3.Value = dtLot.Rows[i]["ReceiptDate"].ToString();
                        this.uTextReceiptTime3.Value = dtLot.Rows[i]["ReceiptTime"].ToString();
                        if (dtLot.Rows[i]["CompleteDate"].ToString() != "")
                        {
                            this.uDateCompleteDate3.Value = dtLot.Rows[i]["CompleteDate"].ToString();
                        }
                        this.uTextReqTime3.Text = dtLot.Rows[i]["ReqTime"].ToString();
                        this.uTextCompleteTime3.Text = dtLot.Rows[i]["CompleteTime"].ToString();
                        if (dtLot.Rows[i]["InspectUserID"].ToString() == "")
                        {
                            this.uTextInspectUserID3.Text = strLogInUserID;
                            this.uTextInspectUserName3.Text = strLogInUserName;
                        }
                        else
                        {
                            this.uTextInspectUserID3.Text = dtLot.Rows[i]["InspectUserID"].ToString();
                            this.uTextInspectUserName3.Text = dtLot.Rows[i]["InspectUserName"].ToString();
                        }
                        this.uTextEtcDesc3.Text = dtLot.Rows[i]["EtcDesc"].ToString();
                        if (dtLot.Rows[i]["InspectResultFlag"].ToString() != "")
                            this.uOptionInspectResult3.Value = dtLot.Rows[i]["InspectResultFlag"].ToString();

                        // 邹美娟等人可以修改已经制作完成的数据
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T" && (m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829")))
                        {
                            this.uCheckCompleteFlag3.CheckedValue = true;
                            this.uCheckCompleteFlag3.Enabled = false;
                            this.uGrid3_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid3_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                        }
                        else if (dtLot.Rows[i]["CompleteFlag"].ToString() == "T")
                        {
                            this.uCheckCompleteFlag3.CheckedValue = true;
                            this.uCheckCompleteFlag3.Enabled = false;
                            this.uGrid3_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGrid3_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                        }
                        else
                        {
                            this.uCheckCompleteFlag3.CheckedValue = false;
                            this.uCheckCompleteFlag3.Enabled = true;
                            this.uGrid3_2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGrid3_3.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                        }

                        // 접수시간이 있을경우는 접수취소버튼 활성화, 접수시간이 업을경우 접수버튼 활성화

                        if (this.uTextReceiptTime3.Text == "")
                        {
                            this.uButtonCancleReceipt3.Visible = false;
                            this.uButtonReceipt3.Visible = true;
                        }
                        else
                        {
                            this.uButtonReceipt3.Visible = false;
                            this.uButtonCancleReceipt3.Visible = true;
                        }
                        this.uTextStack3.Text = dtLot.Rows[i]["StackSeq"].ToString();
                        this.uTextMESHoldTFlag3.Text = dtLot.Rows[i]["MESHoldTFlag"].ToString();
                        this.uTextMESTResultFlag3.Text = dtLot.Rows[i]["MESTCompleteFlag"].ToString();

                        // 공정이 Mold(A7100)이면 CHASE 체크박스를 보여준다

                        if (this.uTextNowProcessCode.Text.Equals("A7100"))
                        {
                            this.uLabelCHASE3.Visible = true;
                            this.uCheckCHASE3_All.Visible = true;
                            this.uCheckCHASE3_1.Visible = true;
                            this.uCheckCHASE3_2.Visible = true;
                            this.uCheckCHASE3_3.Visible = true;
                            this.uCheckCHASE3_4.Visible = true;
                            this.uCheckCHASE3_5.Visible = true;
                            this.uCheckCHASE3_6.Visible = true;

                            this.uCheckCHASE3_1.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase1"]);
                            this.uCheckCHASE3_2.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase2"]);
                            this.uCheckCHASE3_3.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase3"]);
                            this.uCheckCHASE3_4.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase4"]);
                            this.uCheckCHASE3_5.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase5"]);
                            this.uCheckCHASE3_6.Checked = Convert.ToBoolean(dtLot.Rows[i]["Chase6"]);
                            this.uCheckCHASE3_All.Checked = Convert.ToBoolean(dtLot.Rows[i]["ChaseAll"]);
                        }

                        if (this.uTextNowProcessName.Text.Contains("W/B"))
                        {
                            this.uLabelProgramSource3.Visible = true;
                            this.uTextProgramSource3.Visible = true;

                            this.uTextProgramSource3.Text = dtLot.Rows[i]["ProgramSource"].ToString();

                        }
                        else
                        {
                            this.uLabelProgramSource3.Visible = false;
                            this.uTextProgramSource3.Visible = false;
                        }

                        //3차탭 아이템 검사항목 정보 조회
                        SearchInspectReqItemValue_Product(strPlantCode, strReqNo, strReqSeq, i + 1);
                        //3차탭 아이템 검사값 정보 조회
                        SearchInspectReqItemValue_Qualaity(strPlantCode, strReqNo, strReqSeq, i + 1);
                        // 3차탭 가동조건 정보 조회
                        SearchInspectReqPara(strPlantCode, strReqNo, strReqSeq, i + 1);

                        // MESLockFlag 추가 --2012.07.18 <LEE>
                        //this.uTextMESLockingFlag3.Text = dtLot.Rows[i]["MESLockFlag"].ToString();
                        this.uTextMESHoldTFlag3.Text = this.uTextMESLockingFlag1.Text;

                        this.uTextFile3.Text = dtLot.Rows[i]["CCSFailFile"].ToString();
                        this.uTextErrorImage3.Text = dtLot.Rows[i]["CCSErrorImage"].ToString();

                        //// CCSCancelFlag 추가 2012-10-19 -> 2012-11-01 주석 
                        //if (this.uCheckCompleteFlag3.Checked
                        //    && this.uOptionInspectResult3.Value.ToString().Equals("OK"))
                        //{
                        //    // CCS 완료된정보 일경우 CCS으뢰취소가능 2012-10-19
                        //    this.uCheckCCSCancel_3.Visible = true;
                        //    this.uLabelCCSCancel_3.Visible = true;
                        //    this.uButtonCCSCancel_3.Visible = true;

                        //    if (dtLot.Rows[i]["CCSCancelFlag"].ToString().Trim().Equals("T"))
                        //    {
                        //        this.uCheckCCSCancel_3.Checked = true;
                        //        this.uCheckCCSCancel_3.Enabled = false;
                        //    }
                        //    else
                        //    {
                        //        this.uCheckCCSCancel_3.Checked = false;
                        //        this.uCheckCCSCancel_3.Enabled = true;
                        //    }

                        //}
                        //else
                        //{
                        //    // CCS 완료 X 정보 일경우 CCS의뢰취소불가능 2012-10-19
                        //    this.uCheckCCSCancel_3.Visible = false;
                        //    this.uLabelCCSCancel_3.Visible = false;
                        //    this.uButtonCCSCancel_3.Visible = false;
                        //}


                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Item(검사항목)정보 조회 Method --> 필요없음. Test 완료 후 삭제 할것.
        /**private void SearchInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // ItemTable 조회
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                // 해당 차수 설정(1차, 2차 , 3차)                

                if (intReqLotSeq == 1)
                {
                    DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
                    this.uGrid1_1.DataSource = dtItem;
                    this.uGrid1_1.DataBind();


                    for (int i = 0; i < dtItem.Rows.Count; i++)
                    {
                        // 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
                        if (this.uGrid1_1.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "T" &&
                            this.uGrid1_1.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
                        {
                            this.uGrid1_1.Rows[i].Hidden = false;
                        }
                        else
                        {
                            this.uGrid1_1.Rows[i].Hidden = true;
                        }
                    }
                }
                else if (intReqLotSeq == 2)
                {
                    DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
                    this.uGrid2_1.DataSource = dtItem;
                    this.uGrid2_1.DataBind();
                }
                else if (intReqLotSeq == 3)
                {
                    DataTable dtItem = clsItem.mfReadCCSInspectReqItem(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, m_resSys.GetString("SYS_LANG"));
                    this.uGrid3_1.DataSource = dtItem;
                    this.uGrid3_1.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        **/

        // 생산 Item(검사값) 정보 조회 Method
        private void SearchInspectReqItemValue_Product(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Infragistics.Win.UltraWinEditors.UltraOptionSet uOption = new Infragistics.Win.UltraWinEditors.UltraOptionSet();

                if (intReqLotSeq == 1)
                {
                    m_grdCCS = this.uGrid1_1;
                    uOption = this.uOptionInspectResult1;
                }
                else if (intReqLotSeq == 2)
                {
                    m_grdCCS = this.uGrid2_1;
                    uOption = this.uOptionInspectResult2;
                }
                else if (intReqLotSeq == 3)
                {
                    m_grdCCS = this.uGrid3_1;
                    uOption = this.uOptionInspectResult3;
                }

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                DataTable dtItem = clsItem.mfReadCCSInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, "P", m_resSys.GetString("SYS_LANG"));

                this.m_grdCCS.DataSource = dtItem;
                this.m_grdCCS.DataBind();

                //// 생산Item 검사항목 Display(생산아이템이며 공정코드가 일치하는 검사항목만 보여준다.)
                //for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                //{                    
                //    if (this.m_grdCCS.Rows[i].Cells["ProductItemFlag"].Value.ToString() == "T" &&
                //        this.m_grdCCS.Rows[i].Cells["ProcessCode"].Value.ToString() == this.uTextProcessCode.Text)
                //    {
                //        this.m_grdCCS.Rows[i].Hidden = false;
                //    }
                //    else
                //    {
                //        this.m_grdCCS.Rows[i].Hidden = true;
                //    }
                //}

                WinGrid wGrid = new WinGrid();
                // 불량유형 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    // 검사결과가 선택되어있지 않으면 품질 아이템 검사결과로 검사결과 설정
                    if (uOption.CheckedIndex.Equals(-1))
                    {
                        for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                        {
                            if (!m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value.Equals(DBNull.Value) &&
                                !m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value.Equals(null))
                            {
                                if (m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                                {
                                    uOption.Value = "NG";
                                    break;
                                }
                                else
                                {
                                    uOption.Value = "OK";
                                }
                            }
                        }
                    }

                    this.m_grdCCS.DisplayLayout.Bands[0].Columns.ClearUnbound();
                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.m_grdCCS, 0, "SampleSize", "ProductItemSS", strLastColKey);

                    SetSamplingGridColumn(this.m_grdCCS, strPlantCode, "ProductItemSS");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 품질 Item(검사값) 정보 조회 Method
        private void SearchInspectReqItemValue_Qualaity(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strItemLotNo = "";

                if (intReqLotSeq == 1)
                {
                    m_grdCCS = this.uGrid1_2;
                    strItemLotNo = this.uTextLotNo1.Text;
                }
                else if (intReqLotSeq == 2)
                {
                    m_grdCCS = this.uGrid2_2;
                    strItemLotNo = this.uTextLotNo2.Text;
                }
                else if (intReqLotSeq == 3)
                {
                    m_grdCCS = this.uGrid3_2;
                    strItemLotNo = this.uTextLotNo3.Text;
                }

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                DataTable dtItem = clsItem.mfReadCCSInspectReqItemValue(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, "Q", m_resSys.GetString("SYS_LANG"));

                //this.uGrid1_2.EventManager.AllEventsEnabled = false;
                //this.uGrid2_2.EventManager.AllEventsEnabled = false;
                //this.uGrid3_2.EventManager.AllEventsEnabled = false;

                this.m_grdCCS.DataSource = dtItem;
                this.m_grdCCS.DataBind();
                ////}

                WinGrid wGrid = new WinGrid();
                // 불량유형 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));

                //wGrid.mfSetGridColumnValueList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //wGrid.mfSetGridColumnValueGridList(m_grdCCS, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "InspectFaultTypeCode,InspectFaultTypeName", "불량Code,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.DisplayLayout.Bands[0].Columns.ClearUnbound();

                    // SampleSize 만큼 컬럼생성 Method 호출
                    //String[] strLastColKey = { "InspectFaultTypeCode" };
                    String[] strLastColKey = { };
                    CreateColumn(this.m_grdCCS, 0, "SampleSize", "QualityItemSS", strLastColKey);

                    SetSamplingGridColumn(this.m_grdCCS, strPlantCode, "QualityItemSS");
                }
                //this.uGrid1_2.EventManager.AllEventsEnabled = true;
                //this.uGrid2_2.EventManager.AllEventsEnabled = true;
                //this.uGrid3_2.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 가동조건 테이블 검사

        private void SearchInspectReqPara(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                if (intReqLotSeq == 1)
                {
                    uGrid = this.uGrid1_3;
                }
                else if (intReqLotSeq == 2)
                {
                    uGrid = this.uGrid2_3;
                }
                else if (intReqLotSeq == 3)
                {
                    uGrid = this.uGrid3_3;
                }

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);

                //DataTable dtPara = clsPara.mfReadCCSInspectReqPara(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, 0, m_resSys.GetString("SYS_LANG"));
                string strProductCode = this.uTextProductCode.Text;
                string strNowProcessCode = this.uTextNowProcessCode.Text;
                string strEquipCode = this.uTextEquipCode.Text;
                string strPackage = this.uTextPackage.Text;
                string strCustomer = this.uTextCustomerCode.Text;

                DataTable dtPara = clsPara.mfReadCCSInspectReqPara_PSTS(strPlantCode, strReqNo, strReqSeq, intReqLotSeq
                                                                        , strProductCode, strNowProcessCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                // 2012-12-13 가동조건이 없는경우 MASCCSPara테이블에서 검색(완료된 정보 제외) -> 2013-01-25 CCS의뢰에서 의뢰한 데이터로 확인.
                //if (dtPara.Rows.Count == 0)
                //{
                //    dtPara = clsPara.mfReadCCSInspectReqPara_MASCCSPara(strPlantCode, strReqNo, strReqSeq, intReqLotSeq.ToString(),
                //                                                        strPackage, strNowProcessCode, strEquipCode, strCustomer, m_resSys.GetString("SYS_LANG"));
                //}
                /////////////////////////////////////////////////////////////// 2012-12-13

                uGrid.DataSource = dtPara;
                uGrid.DataBind();

                clsPara.Dispose();

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                    {
                        uGrid.Rows[i].Appearance.BackColor = Color.Tomato;
                    }
                }

                ArryCCSPara(intReqLotSeq);

                // 가동조건이 'DIETEMP' 인 경우 텍스트 입력
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 40;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells["CCSparameterCode"].Value.ToString().Equals("DIETEMP"))
                    {
                        uGrid.Rows[i].Cells["QualityValue"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                        uGrid.Rows[i].Cells["QualityValue"].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                        // MaxLength 지정방법;;;;
                        uGrid.Rows[i].Cells["QualityValue"].Editor = editorString;
                        if (uGrid.Rows[i].Cells["QualityValue"].Value == null)
                        {
                            uGrid.Rows[i].Cells["QualityValue"].Value = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 지정된 SampleSize만큼 컬럼추가하는 Method(X1...Xn)
        private void CreateColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, int intBandIndex, String strSampleSizeColKey, string strSampleSizeColKey2, String[] strLastIndexColKey)
        {
            try
            {
                //uGrid.DisplayLayout.Bands[0].Columns.ClearUnbound();
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // SampleSize가 될 최대값 찾기
                int intSampleMax = 0;
                int intSampleMax2 = 0;
                int inttotMax = 0;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey2].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) > inttotMax)
                    {
                        inttotMax = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey2].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                        intSampleMax2 = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey2].Value);
                        intSampleMax = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    }
                }

                if (inttotMax > 99)
                    inttotMax = 99;

                // 상세 SampleSize 만큼 컬럼생성
                for (int i = 1; i <= inttotMax; i++)
                {
                    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                    {
                        //uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Width = 70;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Hidden = false;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].MaskInput = "{double:7.5}";
                    }
                    else
                    {
                        wGrid.mfSetGridColumn(uGrid, intBandIndex, i.ToString(), "X" + i.ToString(), false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:7.5}", "0.0");
                    }
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].PromptChar = ' ';
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }

                // 추가된 컬럼보다 뒤에 있어야 하는 컬럼들이 있으면 뒤로 보냄
                if (strLastIndexColKey.Length > 0)
                {
                    int LastIndex = uGrid.DisplayLayout.Bands[intBandIndex].Columns.Count;
                    for (int i = 0; i < strLastIndexColKey.Length; i++)
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[strLastIndexColKey[i]].Header.VisiblePosition = LastIndex + i;
                    }
                }

                // Size 만큼의 Cell만 입력 가능하도록나머지는 편집불가처리
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    int ActivateNum = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey2].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    if (ActivateNum > 99)
                        ActivateNum = 99;

                    for (int j = 1; j <= inttotMax; j++)
                    {
                        if (uGrid.DisplayLayout.Bands[0].Columns.Exists(j.ToString()))
                        {
                            if (j <= ActivateNum)
                            {
                                uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                uGrid.Rows[i].Cells[j.ToString()].Hidden = false;
                            }
                            else
                            {
                                uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                uGrid.Rows[i].Cells[j.ToString()].Hidden = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형에 맞게 Xn컬럼 설정
        /// </summary>
        private void SetSamplingGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, string strPlantCode, string strSampleColKey)
        {
            try
            {
                //this.uGrid1_2.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid1_2_AfterCellUpdate);
                //this.uGrid2_2.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid2_2_AfterCellUpdate);
                //this.uGrid3_2.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid3_2_AfterCellUpdate);
                this.uGrid1_2.EventManager.AllEventsEnabled = false;
                this.uGrid2_2.EventManager.AllEventsEnabled = false;
                this.uGrid3_2.EventManager.AllEventsEnabled = false;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 선택 DropDown 적용용 DataTable
                int intStart = -99;
                if (uGrid.DisplayLayout.Bands[0].Columns.Exists("1"))
                    intStart = uGrid.DisplayLayout.Bands[0].Columns["1"].Index;
                int intSampleSize = 0;

                // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 50;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                // 합/부 데이터 테이블

                DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    intSampleSize = ReturnIntegerValue(uGrid.Rows[i].Cells[strSampleColKey].Value) * ReturnIntegerValue(uGrid.Rows[i].Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // 계량
                    if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "1")
                    {
                        // 계량인경우 불량수량/검사결과 자동입력
                        uGrid.Rows[i].Cells["FaultQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        uGrid.Rows[i].Cells["FaultQty"].Appearance.BackColor = Color.Gainsboro;
                        uGrid.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        uGrid.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            //uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            if (uGrid.Rows[i].Cells[j].Value == null)
                            {
                                uGrid.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // 계수
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (uGrid.Rows[i].Cells[j].Value == null)
                            {
                                uGrid.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // OK/NG
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "3")
                    {
                        //uGrid.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //uGrid.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(uGrid, i, uGrid.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                            if (uGrid.Rows[i].Cells[j].Value == null)
                            {
                                uGrid.Rows[i].Cells[j].Value = "OK";
                            }
                        }
                    }
                    // 설명
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "4")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            uGrid.Rows[i].Cells[j].Editor = editorString;
                            if (uGrid.Rows[i].Cells[j].Value == null)
                            {
                                uGrid.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    // 선택
                    else if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "5")
                    {
                        // 선택항목 가져오는 메소드 호출
                        string strInspectItemCode = uGrid.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        DataTable dtSelect = clsItem.mfReadCCSInspectReqItem_DataTypeSelect3(strPlantCode
                                                                                            , strInspectItemCode
                                                                                            , this.uTextProductCode.Text
                                                                                            , this.uTextNowProcessCode.Text
                                                                                            , m_resSys.GetString("SYS_LANG"));
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            uGrid.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            uGrid.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            //uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            wGrid.mfSetGridCellValueList(uGrid, i, uGrid.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                            if (uGrid.Rows[i].Cells[j].Value == null)
                            {
                                uGrid.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    //else
                    //{
                    //    for (int j = intStart; j < intStart + intSampleSize; j++)
                    //    {
                    //        uGrid.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //        uGrid.Rows[i].Cells[j].Appearance.BackColor = Color.Gainsboro;
                    //        if (uGrid.Rows[i].Cells[j].Value == null)
                    //        {
                    //            if (uGrid.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    //                uGrid.Rows[i].Cells[j].Value = 0;
                    //            else
                    //                uGrid.Rows[i].Cells[j].Value = "";
                    //        }
                    //    }
                    //}
                }
                
                //this.uGrid1_2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid1_2_AfterCellUpdate);
                //this.uGrid2_2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid2_2_AfterCellUpdate);
                //this.uGrid3_2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGrid3_2_AfterCellUpdate);
                this.uGrid1_2.EventManager.AllEventsEnabled = true;
                this.uGrid2_2.EventManager.AllEventsEnabled = true;
                this.uGrid3_2.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 2.1상세정보 검사자 관련 이벤트

        // 상세정보 : 1차 검사자 팝업창

        private void uTextInspectUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text.ToString().Trim();
                frmPOP.ShowDialog();

                this.uTextInspectUserID1.Text = frmPOP.UserID;
                this.uTextInspectUserName1.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 2차 검사자 팝업창


        private void uTextInspectUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text.ToString().Trim();
                frmPOP.ShowDialog();

                this.uTextInspectUserID2.Text = frmPOP.UserID;
                this.uTextInspectUserName2.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 3차 검사자 팝업창


        private void uTextInspectUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text.ToString().Trim();
                frmPOP.ShowDialog();

                this.uTextInspectUserID3.Text = frmPOP.UserID;
                this.uTextInspectUserName3.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 1차 검사자 입력후 엔터시 검사자명을 가지고 온다.
        private void uTextInspectUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID1.Text == "")
                    {
                        this.uTextInspectUserName1.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextInspectUserID1.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID1.Text = "";
                            this.uTextInspectUserName1.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName1.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID1.TextLength <= 1 || this.uTextInspectUserID1.Text == this.uTextInspectUserID1.SelectedText)
                    {
                        this.uTextInspectUserName1.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 2차 검사자 입력후 엔터시 검사자명을 가지고 온다.
        private void uTextInspectUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID2.Text == "")
                    {
                        this.uTextInspectUserName2.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextInspectUserID2.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID2.Text = "";
                            this.uTextInspectUserName2.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName2.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID2.TextLength <= 1 || this.uTextInspectUserID2.Text == this.uTextInspectUserID2.SelectedText)
                    {
                        this.uTextInspectUserName2.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세정보 : 3차 검사자 입력후 엔터시 검사자명을 가지고 온다.
        private void uTextInspectUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID3.Text == "")
                    {
                        this.uTextInspectUserName3.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextInspectUserID3.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID3.Text = "";
                            this.uTextInspectUserName3.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName3.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID3.TextLength <= 1 || this.uTextInspectUserID3.Text == this.uTextInspectUserID3.SelectedText)
                    {
                        this.uTextInspectUserName3.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 2.2상세정보 품질Item 검사항목 검사값 --> 자동합부판정 및 검사값 입력 순서 체크 이벤트

        // 1차 품질Item 자동으로 합부판정
        private void uGrid1_2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }
                else if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGrid1_2.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("QualityItemSS"))
                    {
                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)))
                        {
                            //this.uGrid1_2.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = ReturnIntegerValue(e.Cell.OriginalValue) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                            int intNewSampleSize = ReturnIntegerValue(e.Cell.Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGrid1_2.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGrid1_2.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGrid1_2, 0, "SampleSize", "QualityItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGrid1_2, this.uTextPlantCode.Text, "QualityItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                this.uOptionInspectResult1.CheckedIndex = 1;
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }

                    // 상단 검사결과 업데이트
                    int intCount = 0;
                    for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid1_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }

                    //uGrid1_3
                    int intCount1_3 = 0;
                    for (int i = 0; i < this.uGrid1_3.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid1_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount1_3++;
                        }
                    }

                    // 하나라도 불량일경우 헤더 합부판정 불합격

                    if (intCount > 0)
                    {
                        this.uOptionInspectResult1.CheckedIndex = 1;
                    }
                    else if(intCount==0 && intCount1_3==0)
                    {
                        this.uOptionInspectResult1.CheckedIndex = 0;
                    }

                    // 이벤트 등록
                    this.uGrid1_2.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2차 품질Item 자동으로 합부판정
        private void uGrid2_2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }
                else if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGrid2_2.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("QualityItemSS"))
                    {
                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)))
                        {
                            //this.uGrid2_2.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = ReturnIntegerValue(e.Cell.OriginalValue) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                            int intNewSampleSize = ReturnIntegerValue(e.Cell.Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGrid2_2.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGrid2_2.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGrid2_2, 0, "SampleSize", "QualityItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGrid2_2, this.uTextPlantCode.Text, "QualityItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                this.uOptionInspectResult2.CheckedIndex = 1;
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("LotNo"))
                    {
                        if (!e.Cell.Value.ToString().Equals(this.uTextLotNo1.Text) || !e.Cell.Value.ToString().Equals(this.uTextLotNo2.Text))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            string strLang = m_resSys.GetString("SYS_LANG");
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , msg.GetMessge_Text("M000073",strLang)
                                                                    , msg.GetMessge_Text("M000073",strLang)
                                                                    , msg.GetMessge_Text("M000961",strLang)
                                                                    + this.uTextLotNo1.Text + " , " + this.uTextLotNo2.Text + msg.GetMessge_Text("M000872",strLang)
                                                                    , Infragistics.Win.HAlign.Center);
                            // 기존값으로 되돌린다
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                    }

                    // 상단 검사결과 업데이트
                    int intCount = 0;
                    for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid2_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }

                    //uGrid2_3
                    int intCount2_3 = 0;
                    for (int i = 0; i < this.uGrid2_3.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid2_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount2_3++;
                        }
                    }

                    // 하나라도 불량일경우 헤더 합부판정 불합격

                    if (intCount > 0)
                    {
                        this.uOptionInspectResult2.CheckedIndex = 1;
                    }
                    else if(intCount==0 && intCount2_3==0)
                    {
                        this.uOptionInspectResult2.CheckedIndex = 0;
                    }

                    // 이벤트 등록
                    this.uGrid2_2.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 3차 품질Item 자동으로 합부판정
        private void uGrid3_2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }
                else if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 이벤트 해제
                    this.uGrid3_2.EventManager.AllEventsEnabled = false;

                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우

                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                        {
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우

                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        // 데이터 유형이 선택인 경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    else if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }
                    //추가 (ryu.2011.11.23)
                    else if (e.Cell.Column.Key.Equals("QualityItemSS"))
                    {
                        if (ReturnIntegerValue(e.Cell.Row.Cells["FaultQty"].Value) < (Convert.ToInt32(ReturnIntegerValue(e.Cell.Value)) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)))
                        {
                            //this.uGrid3_2.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            int intOldSampleSize = ReturnIntegerValue(e.Cell.OriginalValue) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);
                            int intNewSampleSize = ReturnIntegerValue(e.Cell.Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value);

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGrid3_2.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGrid3_2.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            //String[] strLastColKey = { "InspectFaultTypeCode" };
                            String[] strLastColKey = { };
                            CreateColumn(this.uGrid3_2, 0, "SampleSize", "QualityItemSS", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn(uGrid3_2, this.uTextPlantCode.Text, "QualityItemSS");

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (ReturnIntegerValue(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                this.uOptionInspectResult3.CheckedIndex = 1;
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }
                            else if (ReturnIntegerValue(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장

                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < ReturnIntegerValue(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("LotNo"))
                    {
                        if (!e.Cell.Value.ToString().Equals(this.uTextLotNo1.Text) ||
                            !e.Cell.Value.ToString().Equals(this.uTextLotNo2.Text) ||
                            !e.Cell.Value.ToString().Equals(this.uTextLotNo3.Text))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , msg.GetMessge_Text("M000073",m_resSys.GetString("SYS_LANG"))
                                                                    , msg.GetMessge_Text("M000073",m_resSys.GetString("SYS_LANG"))
                                                                    , msg.GetMessge_Text("M000961",m_resSys.GetString("SYS_LANG")) +
                                                                    this.uTextLotNo1.Text + " , " +
                                                                    this.uTextLotNo2.Text + " , " +
                                                                    this.uTextLotNo3.Text + msg.GetMessge_Text("M000872",m_resSys.GetString("SYS_LANG"))
                                                                    , Infragistics.Win.HAlign.Center);
                            // 기존값으로 되돌린다
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                    }

                    // 상단 검사결과 업데이트
                    int intCount = 0;
                    for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid3_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }

                    //uGrid3_3
                    int intCount3_3 = 0;
                    for (int i = 0; i < this.uGrid3_3.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGrid3_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount3_3++;
                        }
                    }

                    // 하나라도 불량일경우 헤더 합부판정 불합격

                    if (intCount > 0)
                    {
                        this.uOptionInspectResult3.CheckedIndex = 1;
                    }
                    else if (intCount==0 && intCount3_3==0)
                    {
                        this.uOptionInspectResult3.CheckedIndex = 0;
                    }

                    // 이벤트 등록
                    this.uGrid3_2.EventManager.AllEventsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계량형 결과값 검사 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementMeasureCount(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value)
                {
                    return;
                }
                else if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Instance 객체 생성
                    QRPSTA.STABAS clsSTABAS = new QRPSTA.STABAS();
                    // 구조체 변수 생성
                    QRPSTA.STABasic structSTA = new QRPSTA.STABasic();

                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;
                    // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                    int intLastIndex = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;

                    // Double형 배열 생성
                    double[] dblXn = new double[intLastIndex - intStart];

                    // Loop돌며 배열에 값 저장

                    for (int i = 0; i < intLastIndex - intStart; i++)
                    {
                        int intIndex = intStart + i;
                        dblXn[i] = Convert.ToDouble(e.Cell.Row.Cells[intIndex].Value);
                    }

                    structSTA = clsSTABAS.mfCalcBasicStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                                                        , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString());

                    // 합부판정
                    if (structSTA.AcceptFlag)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                        this.uOptionInspectResult1.CheckedIndex = 1;
                    }

                    // 계량형인경우 불량수량 자동 입력
                    if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("1"))
                    {
                        e.Cell.Row.Cells["FaultQty"].Value = structSTA.FalutCount;
                    }

                    // 평균
                    e.Cell.Row.Cells["Mean"].Value = structSTA.Mean;
                    // MaxValue
                    e.Cell.Row.Cells["MaxValue"].Value = structSTA.Max;
                    // MinValue
                    e.Cell.Row.Cells["MinValue"].Value = structSTA.Min;
                    // Range
                    e.Cell.Row.Cells["DataRange"].Value = structSTA.Range;
                    // StdDev
                    e.Cell.Row.Cells["StdDev"].Value = structSTA.StdDev;
                    // Cp
                    e.Cell.Row.Cells["Cp"].Value = structSTA.Cp;
                    // Cpk
                    e.Cell.Row.Cells["Cpk"].Value = structSTA.Cpk;

                    // 검사값이 불량일때 폰트색 변경

                    if (Convert.ToDecimal(e.Cell.Value) > Convert.ToDecimal(e.Cell.Row.Cells["UpperSpec"].Value) ||
                        Convert.ToDecimal(e.Cell.Value) < Convert.ToDecimal(e.Cell.Row.Cells["LowerSpec"].Value))
                    {
                        e.Cell.Appearance.ForeColor = Color.Red;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// OK/NG 합/부 판정 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementOkNg(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;
                    // Xn컬럼의 마지막 컬럼 Index저장

                    int intLastIndex = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;

                    int intFaultCount = 0;
                    // Loop 돌며 결과값 검사

                    for (int i = intStart; i < intLastIndex; i++)
                    {
                        if (e.Cell.Row.Cells[i].Value.ToString() == "NG")
                        {
                            intFaultCount += 1;
                        }
                    }

                    e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                    if (intFaultCount > 0)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형 선택 합부판정(Bom인경우 Bom테이블에 자재코드가 존재하면 OK아니면 NG BomCheckFlag가 false이면 기존방식대로 선택
        /// </summary>
        /// <param name="e"></param>
        private void JudgementSelect(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                    int intStart = e.Cell.Row.Cells["1"].Column.Index;
                    // Xn컬럼의 마지막 컬럼 Index저장

                    int intLastIndex = (ReturnIntegerValue(e.Cell.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;

                    // BomCheckFlag 확인
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsType);

                    string strPlantCode = this.uTextPlantCode.Text;
                    string strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    string strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                    DataTable dtBomCheck = clsType.mfReadMASInspectType_BomChecFlag(strPlantCode, strInspectGroupCode, strInspectTypeCode);

                    // BomCheckFlag 가 True 이면
                    if (dtBomCheck.Rows[0]["BomCheckFlag"].ToString().Equals("T"))
                    {
                        bool bolCheck = true;
                        int intFaultCount = 0;
                        // Loop 돌며 결과값 검사

                        for (int i = intStart; i < intLastIndex; i++)
                        {
                            if (!e.Cell.Row.Cells[i].Value.ToString().Equals(string.Empty))
                            {
                                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                                brwChannel.mfCredentials(clsItem);

                                DataTable dtInspectResult = clsItem.mfReadCCSInspectReqItem_DataTypeSelectInspectResult_MASBOM(strPlantCode, this.uTextProductCode.Text
                                                                                                                            , this.uTextNowProcessCode.Text, e.Cell.Row.Cells[i].Value.ToString());
                                if (dtInspectResult.Rows[0]["InspectResult"].ToString().Equals("NG"))
                                {
                                    bolCheck = false;
                                    //break;
                                    intFaultCount += 1;
                                }
                            }
                        }

                        if (bolCheck)
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                            this.uOptionInspectResult1.CheckedIndex = 1;
                        }

                        e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 3.저장 메소드


        /// <summary>
        /// 헤더정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>        
        private DataTable SaveHeaderInfo()
        {
            DataTable dtSaveHeader = new DataTable();
            try
            {
                //BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH(m_strDBConn);
                }

                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                // DataTable Column 설정
                dtSaveHeader = clsHeader.mfSetDataInfo();

                DataRow drRow = dtSaveHeader.NewRow();
                drRow["PlantCode"] = this.uTextPlantCode.Text;
                drRow["ReqNo"] = strReqNo;
                drRow["ReqSeq"] = strReqSeq;
                drRow["ProductCode"] = this.uTextProductCode.Text;
                drRow["WorkProcessCode"] = this.uTextWorkProcessCode.Text;
                drRow["NowProcessCode"] = this.uTextNowProcessCode.Text;
                drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["EtcDesc"] = "";
                drRow["InspectResultFlag"] = "";
                dtSaveHeader.Rows.Add(drRow);

                return dtSaveHeader;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveHeader;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot 정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>                
        private DataTable SaveLotInfo()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            DataTable dtSaveLot = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString());

                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);
                }
                else
                {
                    clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot(m_strDBConn);
                }

                // Column 설정
                dtSaveLot = clsLot.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                drRow = dtSaveLot.NewRow();
                drRow["PlantCode"] = this.uTextPlantCode.Text;
                drRow["ReqNo"] = strReqNo;
                drRow["ReqSeq"] = strReqSeq;
                drRow["ReqLotSeq"] = m_intTabIndex + 1;                             //Convert.ToInt32(this.uTabCCS.SelectedTab.Index.ToString()) + 1;

                // 의뢰 화면에서 저장된것 조회
                //DataTable dtLot = clsLot.mfReadCCSInspectReqLot(this.uTextPlantCode.Text, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                //1차탭 선택된 경우
                if (m_intTabIndex == 0)
                {
                    drRow["LotNo"] = this.uTextLotNo1.Text;
                    drRow["CCSReqTypeCode"] = this.uTextCCSReqTypeCode1.Text;
                    drRow["CauseReason"] = this.uTextCauseReason1.Text;
                    drRow["ReqDate"] = this.uDateReqDate1.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReqTime"] = this.uTextReqTime1.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction1.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;

                    drRow["InspectFaultTypeCode"] = this.uComboFaultType1.Value.ToString();
                    //drRow["CompleteDate"] = this.uDateCompleteDate1.Value.ToString();
                    if (this.uCheckCompleteFlag1.Checked == true)
                    {
                        drRow["CompleteDate"] = this.uDateCompleteDate1.DateTime.Date.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        drRow["CompleteDate"] = string.Empty;
                    }
                    drRow["CompleteTime"] = "";
                    drRow["InspectUserID"] = this.uTextInspectUserID1.Text;
                    drRow["EtcDesc"] = this.uTextEtcDesc1.Text;
                    if (this.uOptionInspectResult1.CheckedIndex != -1)
                        drRow["InspectResultFlag"] = this.uOptionInspectResult1.Value.ToString();
                    drRow["CompleteFlag"] = this.uCheckCompleteFlag1.Checked;
                    drRow["StackSeq"] = this.uTextStackSeq1.Text;

                    drRow["Chase1"] = this.uCheckCHASE1_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE1_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE1_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE1_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE1_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE1_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE1_All.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ReceiptDate"] = this.uDateReceiptDate1.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReceiptTime"] = this.uTextReceiptTime1.Text;
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag1.Text;

                    if (this.uTextFile1.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextFile1.Text);
                        drRow["CCSFile"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-First-CCS-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSFile"] = this.uTextFile1.Text;
                    }

                    if (this.uTextErrorImage1.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextErrorImage1.Text);
                        drRow["CCSErrorImage"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-First-CCS-ErrorImage-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSErrorImage"] = this.uTextErrorImage1.Text;
                    }

                }
                //2차탭 선택된 경우
                else if (m_intTabIndex == 1)
                {
                    drRow["LotNo"] = this.uTextLotNo2.Text;
                    drRow["CCSReqTypeCode"] = this.uTextCCSReqTypeCode2.Text;
                    drRow["CauseReason"] = this.uTextCauseReason2.Text;
                    drRow["ReqDate"] = this.uDateReqDate2.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReqTime"] = this.uTextReqTime2.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction2.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;

                    drRow["InspectFaultTypeCode"] = this.uComboFaultType2.Value;
                    //drRow["CompleteDate"] = this.uDateCompleteDate2.Value.ToString();
                    if (this.uCheckCompleteFlag2.Checked == true)
                    {
                        drRow["CompleteDate"] = this.uDateCompleteDate2.DateTime.Date.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        drRow["CompleteDate"] = string.Empty;
                    }
                    drRow["CompleteTime"] = "";
                    drRow["InspectUserID"] = this.uTextInspectUserID2.Text;
                    drRow["EtcDesc"] = this.uTextEtcDesc2.Text;
                    if (this.uOptionInspectResult2.CheckedIndex != -1)
                        drRow["InspectResultFlag"] = this.uOptionInspectResult2.Value.ToString();
                    drRow["CompleteFlag"] = this.uCheckCompleteFlag2.Checked;
                    drRow["StackSeq"] = this.uTextStackSeq2.Text;

                    drRow["Chase1"] = this.uCheckCHASE2_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE2_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE2_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE2_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE2_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE2_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE2_All.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ReceiptDate"] = this.uDateReceiptDate2.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReceiptTime"] = this.uTextReceiptTime2.Text;
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag2.Text;

                    if (this.uTextFile2.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextFile2.Text);
                        drRow["CCSFile"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-Second-CCS-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSFile"] = this.uTextFile2.Text;
                    }

                    if (this.uTextErrorImage2.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextErrorImage2.Text);
                        drRow["CCSErrorImage"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-Second-CCS-ErrorImage-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSErrorImage"] = this.uTextErrorImage2.Text;
                    }

                }
                //3차탭 선택된 경우
                else if (m_intTabIndex == 2)
                {
                    drRow["LotNo"] = this.uTextLotNo3.Text;
                    drRow["CCSReqTypeCode"] = this.uTextCCSReqTypeCode3.Text;
                    drRow["CauseReason"] = this.uTextCauseReason3.Text;
                    drRow["ReqDate"] = this.uDateReqDate3.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReqTime"] = this.uTextReqTime3.Text;
                    drRow["CorrectAction"] = this.uTextCorrectAction3.Text;
                    drRow["LotMonthCount"] = 0;
                    drRow["LotYearCount"] = 0;

                    drRow["InspectFaultTypeCode"] = this.uComboFaultType3.Value;
                    //drRow["CompleteDate"] = this.uDateCompleteDate3.Value.ToString();
                    if (this.uCheckCompleteFlag3.Checked == true)
                    {
                        drRow["CompleteDate"] = this.uDateCompleteDate3.DateTime.Date.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        drRow["CompleteDate"] = string.Empty;
                    }
                    drRow["CompleteTime"] = "";
                    drRow["InspectUserID"] = this.uTextInspectUserID3.Text;
                    drRow["EtcDesc"] = this.uTextEtcDesc3.Text;
                    if (this.uOptionInspectResult3.CheckedIndex != -1)
                        drRow["InspectResultFlag"] = this.uOptionInspectResult3.Value.ToString();
                    drRow["CompleteFlag"] = this.uCheckCompleteFlag3.Checked;
                    drRow["StackSeq"] = this.uTextStack3.Text;

                    drRow["Chase1"] = this.uCheckCHASE3_1.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase2"] = this.uCheckCHASE3_2.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase3"] = this.uCheckCHASE3_3.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase4"] = this.uCheckCHASE3_4.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase5"] = this.uCheckCHASE3_5.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["Chase6"] = this.uCheckCHASE3_6.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ChaseAll"] = this.uCheckCHASE3_All.Checked.ToString().ToUpper().Substring(0, 1);
                    drRow["ReceiptDate"] = this.uDateReceiptDate3.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow["ReceiptTime"] = this.uTextReceiptTime3.Text;
                    drRow["MESLockFlag"] = this.uTextMESLockingFlag3.Text;

                    if (this.uTextFile3.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextFile3.Text);
                        drRow["CCSFile"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-Third-CCS-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSFile"] = this.uTextFile3.Text;
                    }

                    if (this.uTextErrorImage3.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextErrorImage3.Text);
                        drRow["CCSErrorImage"] = m_resSys.GetString("SYS_PLANTCODE") + "-" + this.uTextReqNo.Text + "-Third-CCS-ErrorImage-" + fileDoc.Name;
                    }
                    else
                    {
                        drRow["CCSErrorImage"] = this.uTextErrorImage3.Text;
                    }

                }
                dtSaveLot.Rows.Add(drRow);

                return dtSaveLot;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveLot;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable SaveItemInfo()
        {
            DataTable dtSaveItem = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                    brwChannel.mfCredentials(clsItem);
                }
                else
                {
                    clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem(m_strDBConn);
                }

                // 사용자가 선택한 탭Index 및 탭에 속한 그리드명 가져오기

                if (m_intTabIndex == 0)
                {
                    m_intRow = this.uGrid1_2.Rows.Count;
                    m_grdCCS = this.uGrid1_2;
                }
                else if (m_intTabIndex == 1)
                {
                    m_intRow = this.uGrid2_2.Rows.Count;
                    m_grdCCS = this.uGrid2_2;
                }
                else if (m_intTabIndex == 2)
                {
                    m_intRow = this.uGrid3_2.Rows.Count;
                    m_grdCCS = this.uGrid3_2;
                }

                dtSaveItem = clsItem.mfSetDataInfo();
                DataRow drRow;
                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_intRow; i++)
                    {
                        drRow = dtSaveItem.NewRow();

                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = m_intTabIndex + 1;
                        drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                        drRow["ReqItemType"] = "Q";
                        drRow["Seq"] = m_grdCCS.Rows[i].Cells["Seq"].Value;
                        drRow["LotNo"] = m_grdCCS.Rows[i].Cells["LotNo"].Value;
                        drRow["FaultQty"] = m_grdCCS.Rows[i].Cells["FaultQty"].Value;
                        drRow["InspectFaultTypeCode"] = m_grdCCS.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString();

                        drRow["InspectIngFlag"] = m_grdCCS.Rows[i].Cells["InspectIngFlag"].Value.ToString().ToUpper().Substring(0, 1);
                        drRow["ProductInspectSS"] = 0;
                        drRow["QualityInspectSS"] = m_grdCCS.Rows[i].Cells["QualityItemSS"].Value;
                        drRow["EtcDesc"] = m_grdCCS.Rows[i].Cells["EtcDesc"].Value.ToString();

                        if (this.m_grdCCS.Rows[i].Cells["Mean"].Value == null || this.m_grdCCS.Rows[i].Cells["Mean"].Value == DBNull.Value)
                            drRow["Mean"] = 0.0;
                        else
                            drRow["Mean"] = this.m_grdCCS.Rows[i].Cells["Mean"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value != null)
                            drRow["InspectResultFlag"] = this.m_grdCCS.Rows[i].Cells["InspectResultFlag"].Value.ToString();

                        if (this.m_grdCCS.Rows[i].Cells["StdDev"].Value == null || this.m_grdCCS.Rows[i].Cells["StdDev"].Value == DBNull.Value)
                            drRow["StdDev"] = 0.0;
                        else
                            drRow["StdDev"] = this.m_grdCCS.Rows[i].Cells["StdDev"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["MaxValue"].Value == null || this.m_grdCCS.Rows[i].Cells["MaxValue"].Value == DBNull.Value)
                            drRow["MaxValue"] = 0.0;
                        else
                            drRow["MaxValue"] = this.m_grdCCS.Rows[i].Cells["MaxValue"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["MinValue"].Value == null || this.m_grdCCS.Rows[i].Cells["MinValue"].Value == DBNull.Value)
                            drRow["MinValue"] = 0.0;
                        else
                            drRow["MinValue"] = this.m_grdCCS.Rows[i].Cells["MinValue"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["DataRange"].Value == null || this.m_grdCCS.Rows[i].Cells["DataRange"].Value == DBNull.Value)
                            drRow["DataRange"] = 0.0;
                        else
                            drRow["DataRange"] = this.m_grdCCS.Rows[i].Cells["DataRange"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["Cp"].Value == null || this.m_grdCCS.Rows[i].Cells["Cp"].Value == DBNull.Value)
                            drRow["Cp"] = 0.0;
                        else
                            drRow["Cp"] = this.m_grdCCS.Rows[i].Cells["Cp"].Value;

                        if (this.m_grdCCS.Rows[i].Cells["Cpk"].Value == null || this.m_grdCCS.Rows[i].Cells["Cpk"].Value == DBNull.Value)
                            drRow["Cpk"] = 0.0;
                        else
                            drRow["Cpk"] = this.m_grdCCS.Rows[i].Cells["Cpk"].Value;

                        if (!this.uTextStdNumber.Text.Equals(string.Empty))
                        {
                            String strStdNumber = this.uTextStdNumber.Text.Substring(0, 9);
                            String strStdSeq = this.uTextStdNumber.Text.Substring(9, 4);
                            drRow["StdNumber"] = strStdNumber;
                            drRow["StdSeq"] = strStdSeq;
                        }
                        dtSaveItem.Rows.Add(drRow);
                    }
                }
                return dtSaveItem;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveItem;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계량형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable SaveResultMeasure(int intStart)
        {
            DataTable dtMeasure = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultMeasure clsMeasure;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultMeasure), "CCSInspectResultMeasure");
                    clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure();
                    brwChannel.mfCredentials(clsMeasure);
                }
                else
                {
                    clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure(m_strDBConn);
                }

                // Column 설정
                dtMeasure = clsMeasure.mfSetDataInfo();
                DataRow drRow;

                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["QualityItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "1" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtMeasure.NewRow();

                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "Q";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value == null || this.m_grdCCS.Rows[i].Cells[j].Value == DBNull.Value)
                                    drRow["InspectValue"] = 0.0;
                                else
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value;
                                dtMeasure.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtMeasure;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMeasure;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계수형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultCount(int intStart)
        {
            DataTable dtCount = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultCount clsCount;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultCount), "CCSInspectResultCount");
                    clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount();
                    brwChannel.mfCredentials(clsCount);
                }
                else
                {
                    clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount(m_strDBConn);
                }
                // Column 설정
                dtCount = clsCount.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["QualityItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "2" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtCount.NewRow();

                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "Q";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value == null || this.m_grdCCS.Rows[i].Cells[j].Value == DBNull.Value)
                                    drRow["InspectValue"] = 0.0;
                                else
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value;
                                dtCount.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtCount;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtCount;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 Ok/Ng인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultOkNg(int intStart)
        {
            DataTable dtOkNg = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultOkNg clsOkNg;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultOkNg), "CCSInspectResultOkNg");
                    clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg();
                    brwChannel.mfCredentials(clsOkNg);
                }
                else
                {
                    clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg(m_strDBConn);
                }

                // Column 설정
                dtOkNg = clsOkNg.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["QualityItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "3" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtOkNg.NewRow();

                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "Q";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtOkNg.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtOkNg;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtOkNg;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 설명인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultDesc(int intStart)
        {
            DataTable dtDesc = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultDesc clsDesc;
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultDesc), "CCSInspectResultDesc");
                    clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc();
                    brwChannel.mfCredentials(clsDesc);
                }
                else
                {
                    clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc(m_strDBConn);
                }
                // Column 설정
                dtDesc = clsDesc.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["QualityItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "4" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtDesc.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "Q";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtDesc.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtDesc;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDesc;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 선택인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable SaveResultSelect(int intStart)
        {
            DataTable dtSelect = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = "";
                String strReqSeq = "";
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                QRPCCS.BL.INSCCS.CCSInspectResultSelect clsSelect;

                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectResultSelect), "CCSInspectResultSelect");
                    clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect();
                    brwChannel.mfCredentials(clsSelect);
                }
                else
                {
                    clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect(m_strDBConn);
                }

                // Column 설정
                dtSelect = clsSelect.mfSetDataInfo();
                DataRow drRow;
                if (this.m_grdCCS.Rows.Count > 0)
                {
                    this.m_grdCCS.ActiveCell = this.m_grdCCS.Rows[0].Cells[0];
                    // 데이터 저장

                    for (int i = 0; i < this.m_grdCCS.Rows.Count; i++)
                    {
                        int intSampleSize = ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["QualityItemSS"].Value) * ReturnIntegerValue(this.m_grdCCS.Rows[i].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (this.m_grdCCS.Rows[i].Cells["DataType"].Value.ToString() == "5" &&
                            this.m_grdCCS.Rows[i].Hidden == false)
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                drRow = dtSelect.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = m_intTabIndex + 1;
                                drRow["ReqItemSeq"] = m_grdCCS.Rows[i].Cells["ReqItemSeq"].Value;
                                drRow["ReqItemType"] = "Q";
                                drRow["ReqResultSeq"] = j - intStart + 1;
                                if (this.m_grdCCS.Rows[i].Cells[j].Value != null)
                                    drRow["InspectValue"] = this.m_grdCCS.Rows[i].Cells[j].Value.ToString();
                                dtSelect.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtSelect;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSelect;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 가동조건 정보를 반환하는 메소드

        /// </summary>
        /// <returns></returns>
        DataTable SaveParaInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                String strFullReqNo = this.uTextReqNo.Text;
                String strReqNo = string.Empty;
                String strReqSeq = string.Empty;
                if (strFullReqNo != "")
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqPara), "CCSInspectReqPara");
                QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                brwChannel.mfCredentials(clsPara);

                dtRtn = clsPara.mfSetDataInfo();
                DataRow drRow;

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                // 사용자가 선택한 탭Index 및 탭에 속한 그리드명 가져오기

                if (m_intTabIndex == 0)
                {
                    uGrid = this.uGrid1_3;
                }
                else if (m_intTabIndex == 1)
                {
                    uGrid = this.uGrid2_3;
                }
                else if (m_intTabIndex == 2)
                {
                    uGrid = this.uGrid3_3;
                }

                if (uGrid.Rows.Count > 0)
                {
                    // ActiveCell 이동
                    uGrid.ActiveCell = uGrid.Rows[0].Cells[0];
                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = m_intTabIndex + 1;
                        drRow["ParaSeq"] = ReturnIntegerValue(uGrid.Rows[i].Cells["ParaSeq"].Value);
                        drRow["CCSParameterCode"] = uGrid.Rows[i].Cells["CCSParameterCode"].Value.ToString();
                        drRow["DATATYPE"] = uGrid.Rows[i].Cells["DATATYPE"].Value.ToString();
                        drRow["VALIDATIONTYPE"] = uGrid.Rows[i].Cells["VALIDATIONTYPE"].Value.ToString();
                        drRow["ParaVALUE"] = uGrid.Rows[i].Cells["ParaVALUE"].Value.ToString();
                        drRow["LOWERLIMIT"] = uGrid.Rows[i].Cells["LOWERLIMIT"].Value.ToString();
                        drRow["UPPERLIMIT"] = uGrid.Rows[i].Cells["UPPERLIMIT"].Value.ToString();
                        drRow["InspectValue"] = uGrid.Rows[i].Cells["InspectValue"].Value.ToString();
                        drRow["QualityValue"] = uGrid.Rows[i].Cells["QualityValue"].Value.ToString();
                        drRow["InspectResultFlag"] = uGrid.Rows[i].Cells["InspectResultFlag"].Value.ToString();
                        drRow["MaterialSpecName"] = uGrid.Rows[i].Cells["MaterialSpecName"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }
        #endregion

        #region 4.일반 조회 Method
        // UserName 조회
        private String GetUserName(String strPlantCode, String strCreateUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCreateUserID, m_resSys.GetString("SYS_LANG"));

                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }

                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }
        #endregion

        private void uGrid1_3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.Equals("QualityValue"))
                {
                    double dbLSL = 0;
                    double dbUSL = 0;
                    double dbValue = 0;
                    if (!e.Cell.Row.Cells["InspectValue"].Value.Equals(string.Empty))
                    {
                        e.Cell.Row.Appearance.BackColor = Color.White;
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (Convert.ToDecimal(e.Cell.Value) >= Convert.ToDecimal(e.Cell.Row.Cells["LOWERLIMIT"].Value)
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                    }
                    else
                    {
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        if (e.Cell.Value == DBNull.Value || e.Cell.Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "";
                            e.Cell.Row.Appearance.BackColor = Color.White;
                        }
                    }
                }

                // 상단 검사결과 업데이트
                int intCount = 0;
                for (int i = 0; i < this.uGrid1_3.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid1_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount++;
                    }
                }

                //uGrid1_2
                int intCount1_2 = 0;
                for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid1_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount1_2++;
                    }
                }

                // 하나라도 불량일경우 헤더 합부판정 불합격

                if (intCount > 0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 1;
                }
                else if(intCount==0 && intCount1_2==0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.Equals("QualityValue"))
                {
                    if (!e.Cell.Row.Cells["InspectValue"].Value.Equals(string.Empty))
                    {
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (Convert.ToDecimal(e.Cell.Value) >= Convert.ToDecimal(e.Cell.Row.Cells["LOWERLIMIT"].Value)
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                    }
                    else
                    {
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        if (e.Cell.Value == DBNull.Value || e.Cell.Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "";
                            e.Cell.Row.Appearance.BackColor = Color.White;
                        }
                    }
                }
                // 상단 검사결과 업데이트
                int intCount = 0;
                for (int i = 0; i < this.uGrid2_3.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid2_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount++;
                    }
                }

                //uGrid2_2
                int intCount2_2 = 0;
                for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid2_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount2_2++;
                    }
                }

                // 하나라도 불량일경우 헤더 합부판정 불합격

                if (intCount > 0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 1;
                }
                else if(intCount==0 && intCount2_2==0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                if (e.Cell.Column.Key.Equals("QualityValue"))
                {
                    if (!e.Cell.Row.Cells["InspectValue"].Value.Equals(string.Empty))
                    {
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (Convert.ToDecimal(e.Cell.Value) >= Convert.ToDecimal(e.Cell.Row.Cells["LOWERLIMIT"].Value)
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m &&
                            ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["InspectValue"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                    }
                    else
                    {
                        if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()).Equals(0m) && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()) > 0m)
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString())
                                && ReturnDecimalValue(e.Cell.Value.ToString()) <= ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                        else if (ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()) > 0m && ReturnDecimalValue(e.Cell.Row.Cells["UPPERLIMIT"].Value.ToString()).Equals(0m))
                        {
                            if (ReturnDecimalValue(e.Cell.Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["LOWERLIMIT"].Value.ToString()))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Appearance.BackColor = Color.White;
                            }
                            else
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Appearance.BackColor = Color.Tomato;
                            }
                        }
                    }
                    if (e.Cell.Value == DBNull.Value || e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "";
                        e.Cell.Row.Appearance.BackColor = Color.White;
                    }
                }
                // 상단 검사결과 업데이트
                int intCount = 0;
                for (int i = 0; i < this.uGrid3_3.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid3_3.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount++;
                    }
                }

                //uGrid3_2
                int intCount3_2 = 0;
                for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                {
                    // 불합격일경우 카운트 증가
                    if (this.uGrid3_2.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        intCount3_2++;
                    }
                }

                // 하나라도 불량일경우 헤더 합부판정 불합격

                if (intCount > 0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 1;
                }
                else if (intCount==0 && intCount3_2==0)
                {
                    this.uOptionInspectResult1.CheckedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmCCSZ0003_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        private void uGrid1_1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["ProductItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_2_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경

                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = ReturnIntegerValue(e.Row.Cells["QualityItemSS"].Value) * ReturnIntegerValue(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private bool Save_MESResult(int intReqLotSeq)
        {
            bool SaveCheck = false;
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                string strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESResult(this.uTextPlantCode.Text, this.uTextReqNo.Text.Substring(0, 8), this.uTextReqNo.Text.Substring(8, 4)
                                                                        , intReqLotSeq, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                // 결과검사

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    SaveCheck = true;

                    //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                               "처리결과", "MES처리결과", "입력한 정보를 저장하고 CCS 결과 정보를 MES 전송하였습니다.", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M000095", "M000955", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                       , msg.GetMessge_Text("M000095",m_resSys.GetString("SYS_LANG"))
                                                       , ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                    }
                }

                return SaveCheck;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return SaveCheck;
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES Hold 코드 전송 메소드

        /// </summary>
        /// <param name="intReqLotSeq">Lot 순번</param>
        /// <returns></returns>
        private bool Save_MESHold(int intReqLotSeq)
        {
            bool SaveCheck = false;
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                DataTable dtLotList = new DataTable();
                dtLotList.Columns.Add("LOTID", typeof(string));
                DataRow drRow;

                string strInspectUserID = string.Empty;

                if (intReqLotSeq.Equals(1))
                {
                    strInspectUserID = this.uTextInspectUserID1.Text;
                    // 불합격인 이고 아직 Hold 코드를 전송 안한경우
                    if (this.uOptionInspectResult1.CheckedIndex.Equals(1) && !this.uTextMESHoldTFlag1.Text.Equals("T") && this.uCheckCompleteFlag1.Checked)
                    {
                        // LotList 작성
                        for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                        {
                            drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uGrid1_2.Rows[i].Cells["LotNo"].Value.ToString();
                            dtLotList.Rows.Add(drRow);
                        }
                        drRow = dtLotList.NewRow();
                        drRow["LOTID"] = this.uTextLotNo1.Text;
                        dtLotList.Rows.Add(drRow);
                    }
                    else if (this.uOptionInspectResult1.CheckedIndex.Equals(0))
                    {
                        SaveCheck = true;
                    }
                    else if (!this.uCheckCompleteFlag1.Checked)
                    {
                        SaveCheck = true;
                    }
                }
                else if (intReqLotSeq.Equals(2))
                {
                    strInspectUserID = this.uTextInspectUserID2.Text;
                    // 불합격인 경우
                    if (this.uOptionInspectResult2.CheckedIndex.Equals(1) && !this.uTextMESHoldTFlag2.Text.Equals("T") && this.uCheckCompleteFlag2.Checked)
                    {
                        // LotList 작성
                        for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                        {
                            drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uGrid2_2.Rows[i].Cells["LotNo"].Value.ToString();
                            dtLotList.Rows.Add(drRow);
                        }
                        drRow = dtLotList.NewRow();
                        drRow["LOTID"] = this.uTextLotNo2.Text;
                        dtLotList.Rows.Add(drRow);
                    }
                    else if (this.uOptionInspectResult2.CheckedIndex.Equals(0))
                    {
                        SaveCheck = true;
                    }
                    else if (!this.uCheckCompleteFlag2.Checked)
                    {
                        SaveCheck = true;
                    }
                }
                else if (intReqLotSeq.Equals(3))
                {
                    strInspectUserID = this.uTextInspectUserID3.Text;
                    // 불합격인 경우
                    if (this.uOptionInspectResult3.CheckedIndex.Equals(1) && !this.uTextMESHoldTFlag3.Text.Equals("T") && this.uCheckCompleteFlag3.Checked)
                    {
                        // LotList 작성
                        for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                        {
                            drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uGrid3_2.Rows[i].Cells["LotNo"].Value.ToString();
                            dtLotList.Rows.Add(drRow);
                        }
                        drRow = dtLotList.NewRow();
                        drRow["LOTID"] = this.uTextLotNo3.Text;
                        dtLotList.Rows.Add(drRow);
                    }
                    else if (this.uOptionInspectResult3.CheckedIndex.Equals(0))
                    {
                        SaveCheck = true;
                    }
                    else if (!this.uCheckCompleteFlag3.Checked)
                    {
                        SaveCheck = true;
                    }
                }

                if (dtLotList.Rows.Count > 0)
                {
                    // 중복제거
                    dtLotList = dtLotList.DefaultView.ToTable(true, "LOTID");

                    // 화일서버 연결정보 가져오기

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S04");     //Live Server
                    //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S07");     //Test Server


                    // MES Lot 정보 요청 매서드 실행
                    QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);

                    for (int i = 0; i < dtLotList.Rows.Count; i++)
                    {
                        DataTable dtLotNo = clsTibrv.LOT_INFO_REQ(dtLotList.Rows[i]["LOTID"].ToString());

                        // 요청하여 정보가 있을 경우 
                        if (dtLotNo.Rows.Count > 0)
                        {
                            // Lot상태가 Hold 이면 
                            if (dtLotNo.Rows[0]["LOTHOLDSTATE"].ToString().Equals("OnHold"))
                            {
                                dtLotList.Rows[i].Delete();
                            }
                        }
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                    DataTable dtStdInfo = clsLot.mfRtnMESDataTable();
                    drRow = dtStdInfo.NewRow();
                    drRow["PlantCode"] = this.uTextPlantCode.Text;
                    drRow["ReqNo"] = this.uTextReqNo.Text.Substring(0, 8);
                    drRow["ReqSeq"] = this.uTextReqNo.Text.Substring(8, 4);
                    drRow["ReqLotSeq"] = intReqLotSeq;
                    dtStdInfo.Rows.Add(drRow);

                    // Hold 코드 가져오는 메소드 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                    brwChannel.mfCredentials(clsReason);

                    DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uTextPlantCode.Text, this.Name);

                    string strHoldCode = string.Empty;
                    if (dtReason.Rows.Count > 0)
                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();
                    else
                        strHoldCode = "HQ03";

                    // Hold코드 전송 메소드 호출
                    string strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESHold(this.Name
                                                                            , ""
                                                                            , ""
                                                                            , strHoldCode
                                                                            , dtLotList
                                                                            , m_resSys.GetString("SYS_USERIP")
                                                                            , dtStdInfo);

                    // 결과검사

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        ////                           "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Hold 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);

                        SaveCheck = true;
                    }
                    else
                    {
                        SaveCheck = false;

                        string strMes = "";

                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000946", m_resSys.GetString("SYS_LANG"));
                        else
                            strMes = ErrRtn.InterfaceResultMessage;

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                   , msg.GetMessge_Text("M000095",m_resSys.GetString("SYS_LANG"))
                                                   , strMes, Infragistics.Win.HAlign.Right);
                    }
                }
                return SaveCheck;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return SaveCheck;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }

        /// <summary>
        /// Int형 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">int로 반환받을 값</param>
        /// <returns></returns>
        private int ReturnIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
                return result;
            else
                return 0;
        }

        private void uComboSearchArea_ValueChanged(object sender, EventArgs e)
        {
            //SetSearchProcessGroupCombo();
        }

        // 검색조건 공정그룹 콤보박스 설정 메소드

        private void SetSearchProcessGroupCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                DataTable dtProcessGroup = new DataTable();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAreaCode = this.uComboSearchArea.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text.Trim();

                this.uComboSearchProcessGroup.Items.Clear();

                if (!strAreaCode.Equals(string.Empty) && !strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    dtProcessGroup = clsEquip.mfReadEquipArea_WithDetailProcessOperationType(strPlantCode, strEquipCode, strAreaCode);
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComboCode", "ComboName", dtProcessGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 품질Item 그리드 Key 이벤트 처리
        //품질Item1 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid1_2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid1_2 == null ? null : uGrid1_2.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGrid1_2.ActiveCell.Row.Index;

                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("QualityItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid1_2.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGrid1_2.ActiveCell = nextCell;
                        this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid1_2.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGrid1_2.ActiveCell = nextCell;
                        this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
                else if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGrid1_2.Rows[intRowIndex].Cells["QualityItemSS"].Value) * ReturnIntegerValue(uGrid1_2.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGrid1_2.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGrid1_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "QualityItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid1_2.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGrid1_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGrid1_2.ActiveCell.Row.Index < this.uGrid1_2.Rows.Count - 1)
                                    nextCell = this.uGrid1_2.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGrid1_2.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGrid1_2.ActiveCell.Row.Index < this.uGrid1_2.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGrid1_2.Rows[intRowIndex + 1].Cells["QualityItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGrid1_2.Rows[0].Cells["QualityItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGrid1_2.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid1_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //품질Item2 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid2_2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid2_2 == null ? null : uGrid2_2.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGrid2_2.ActiveCell.Row.Index;

                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("QualityItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGrid2_2.ActiveCell = nextCell;
                        this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGrid2_2.ActiveCell = nextCell;
                        this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
                else if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGrid2_2.Rows[intRowIndex].Cells["QualityItemSS"].Value) * ReturnIntegerValue(uGrid2_2.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGrid2_2.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGrid2_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "QualityItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGrid2_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGrid2_2.ActiveCell.Row.Index < this.uGrid2_2.Rows.Count - 1)
                                    nextCell = this.uGrid2_2.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGrid2_2.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGrid2_2.ActiveCell.Row.Index < this.uGrid2_2.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGrid2_2.Rows[intRowIndex + 1].Cells["QualityItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGrid2_2.Rows[0].Cells["QualityItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGrid2_2.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //품질Item3 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid3_2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid3_2 == null ? null : uGrid3_2.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGrid3_2.ActiveCell.Row.Index;

                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("QualityItemSS"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGrid2_2.ActiveCell = nextCell;
                        this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGrid2_2.ActiveCell = nextCell;
                        this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
                else if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = ReturnIntegerValue(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (ReturnIntegerValue(uGrid2_2.Rows[intRowIndex].Cells["QualityItemSS"].Value) * ReturnIntegerValue(uGrid2_2.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGrid2_2.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGrid2_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "QualityItemSS";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGrid2_2.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGrid2_2.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGrid2_2.ActiveCell.Row.Index < this.uGrid2_2.Rows.Count - 1)
                                    nextCell = this.uGrid2_2.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGrid2_2.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGrid2_2.ActiveCell.Row.Index < this.uGrid2_2.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGrid2_2.Rows[intRowIndex + 1].Cells["QualityItemSS"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGrid2_2.Rows[0].Cells["QualityItemSS"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGrid2_2.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGrid2_2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 가동조건 그리드 Key 이벤트 처리
        //가동조건1 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid1_3_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid1_3 == null ? null : uGrid1_3.ActiveCell;
                if (activeCell == null) return;

                //가동조건1 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("QualityValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGrid1_3.ActiveCell.Row.Index < this.uGrid1_3.Rows.Count - 1)
                            this.uGrid1_3.ActiveCell = this.uGrid1_3.Rows[this.uGrid1_3.ActiveCell.Row.Index + 1].Cells["QualityValue"];
                        else
                            this.uGrid1_3.ActiveCell = this.uGrid1_3.Rows[0].Cells["QualityValue"];
                        this.uGrid1_3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //가동조건2 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid2_3_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid2_3 == null ? null : uGrid2_3.ActiveCell;
                if (activeCell == null) return;

                //가동조건1 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("QualityValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGrid2_3.ActiveCell.Row.Index < this.uGrid2_3.Rows.Count - 1)
                            this.uGrid2_3.ActiveCell = this.uGrid2_3.Rows[this.uGrid2_3.ActiveCell.Row.Index + 1].Cells["QualityValue"];
                        else
                            this.uGrid2_3.ActiveCell = this.uGrid2_3.Rows[0].Cells["QualityValue"];
                        this.uGrid2_3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //가동조건3 엔터치면 바로 아래로 이동하도록 처리
        private void uGrid3_3_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGrid3_3 == null ? null : uGrid3_3.ActiveCell;
                if (activeCell == null) return;

                //가동조건1 그리드에서 생산적용값을 입력한 경우 엔터를 치는 경우
                if (activeCell.Column.Key == ("QualityValue"))
                {
                    if (e.KeyData == Keys.Enter)
                    {
                        //현재 행이 마지막행이면 첫번째 행으로 이동
                        if (this.uGrid3_3.ActiveCell.Row.Index < this.uGrid3_3.Rows.Count - 1)
                            this.uGrid3_3.ActiveCell = this.uGrid3_3.Rows[this.uGrid3_3.ActiveCell.Row.Index + 1].Cells["QualityValue"];
                        else
                            this.uGrid3_3.ActiveCell = this.uGrid3_3.Rows[0].Cells["QualityValue"];
                        this.uGrid3_3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uGrid1_2_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGrid1_2.ActiveCell == null)
                    return;

                if (this.uGrid1_2.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGrid1_2.ActiveCell.Row.Index;
                    if (this.uGrid1_2.ActiveCell.Value.Equals(0))
                    {
                        this.uGrid1_2.EventManager.AllEventsEnabled = false;
                        this.uGrid1_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGrid1_2.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGrid1_2.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGrid1_2.Rows[intIndex].Cells["QualityItemSS"].Value) *
                                                ReturnIntegerValue(this.uGrid1_2.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGrid1_2.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGrid1_2.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGrid1_2.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGrid1_2.ActiveCell.Column.Key.Equals("QualityItemSS"))
                {
                    int intIndex = this.uGrid1_2.ActiveCell.Row.Index;
                    this.uGrid1_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_2_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGrid2_2.ActiveCell == null)
                    return;

                if (this.uGrid2_2.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGrid2_2.ActiveCell.Row.Index;
                    if (this.uGrid2_2.ActiveCell.Value.Equals(0))
                    {
                        this.uGrid2_2.EventManager.AllEventsEnabled = false;
                        this.uGrid2_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGrid2_2.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGrid2_2.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGrid2_2.Rows[intIndex].Cells["QualityItemSS"].Value) *
                                                ReturnIntegerValue(this.uGrid2_2.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGrid2_2.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGrid2_2.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGrid2_2.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGrid2_2.ActiveCell.Column.Key.Equals("QualityItemSS"))
                {
                    int intIndex = this.uGrid2_2.ActiveCell.Row.Index;
                    this.uGrid2_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_2_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGrid3_2.ActiveCell == null)
                    return;

                if (this.uGrid3_2.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGrid3_2.ActiveCell.Row.Index;
                    if (this.uGrid3_2.ActiveCell.Value.Equals(0))
                    {
                        this.uGrid3_2.EventManager.AllEventsEnabled = false;
                        this.uGrid3_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGrid3_2.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장

                            int intStart = this.uGrid3_2.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장

                            //int intLastIndex = (ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    ReturnIntegerValue(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장

                            int intSampleSize = (ReturnIntegerValue(this.uGrid3_2.Rows[intIndex].Cells["QualityItemSS"].Value) *
                                                ReturnIntegerValue(this.uGrid3_2.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGrid3_2.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGrid3_2.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGrid3_2.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGrid3_2.ActiveCell.Column.Key.Equals("QualityItemSS"))
                {
                    int intIndex = this.uGrid3_2.ActiveCell.Row.Index;
                    this.uGrid3_2.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 1차의뢰 접수버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonReceipt1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;  // ??

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    if (this.uCheckCompleteFlag1.Enabled == true && this.uTextReceiptTime1.Text.Equals(string.Empty))
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = DateTime.Now.ToString("yyyy-MM-dd");
                        string strTime = DateTime.Now.ToString("HH:mm:ss");

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, strReqLotSeq, strDate, strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            this.uDateReceiptDate1.Value = strDate;
                            this.uTextReceiptTime1.Text = strTime;

                            //strDate = msg.GetMessge_Text(this.uDateReceiptDate1.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                            //strTime = msg.GetMessge_Text(this.uTextReceiptTime1.Text, m_resSys.GetString("SYS_LANG"));

                            //msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //    , "M001021", "M001316", msg.GetMessge_Text(this.uDateReceiptDate1.Value.ToString(), m_resSys.GetString("SYS_LANG")) + msg.GetMessge_Text(this.uTextReceiptTime1.Text, m_resSys.GetString("SYS_LANG")) + "M001271", Infragistics.Win.HAlign.Right);
                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001424", m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M001425", m_resSys.GetString("SYS_LANG"))
                                , strDate + " " + strTime + msg.GetMessge_Text("M001426", m_resSys.GetString("SYS_LANG"))
                                , Infragistics.Win.HAlign.Right);

                            this.uButtonReceipt1.Visible = false;
                            this.uButtonCancleReceipt1.Visible = true;

                            // 2012-11-01 CCS의뢰정보가 접수전이면 CCS의뢰취소가능 
                            this.uCheckCCSCancel_1.Visible = false;
                            this.uLabelCCSCancel_1.Visible = false;
                            this.uButtonCCSCancel_1.Visible = false;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001021", "M001316", "M001315", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                    else if (this.uCheckCompleteFlag1.Checked == true && this.uCheckCompleteFlag1.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001313", "M001313", "M001325", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 2차의뢰 접수버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonReceipt2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    if (this.uCheckCompleteFlag2.Enabled == true && this.uTextReceiptTime2.Text.Equals(string.Empty))
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = DateTime.Now.ToString("yyyy-MM-dd");
                        string strTime = DateTime.Now.ToString("HH:mm:ss");

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, strReqLotSeq, strDate, strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            this.uDateReceiptDate2.Value = strDate;
                            this.uTextReceiptTime2.Text = strTime;

                            //msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //    , "M001021", "M001316", "M001271", Infragistics.Win.HAlign.Right);
                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001424", m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M001425", m_resSys.GetString("SYS_LANG"))
                                , strDate + " " + strTime + msg.GetMessge_Text("M001426", m_resSys.GetString("SYS_LANG"))
                                , Infragistics.Win.HAlign.Right);

                            this.uButtonReceipt2.Visible = false;
                            this.uButtonCancleReceipt2.Visible = true;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001021", "M001316", "M001315", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                    else if (this.uCheckCompleteFlag2.Checked == true && this.uCheckCompleteFlag2.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001313", "M001313", "M001325.", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 3차의뢰 접수버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonReceipt3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    if (this.uCheckCompleteFlag3.Enabled == true && this.uTextReceiptTime3.Text.Equals(string.Empty))
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = DateTime.Now.ToString("yyyy-MM-dd");
                        string strTime = DateTime.Now.ToString("HH:mm:ss");

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, strReqLotSeq, strDate, strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            this.uDateReceiptDate3.Value = strDate;
                            this.uTextReceiptTime3.Text = strTime;

                            //msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //    , "M001021", "M001316", "M001271", Infragistics.Win.HAlign.Right);

                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001424", m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M001425", m_resSys.GetString("SYS_LANG"))
                                , strDate + " " + strTime + msg.GetMessge_Text("M001426", m_resSys.GetString("SYS_LANG"))
                                , Infragistics.Win.HAlign.Right);

                            this.uButtonReceipt3.Visible = false;
                            this.uButtonCancleReceipt3.Visible = true;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001021", "M001316", "M001315", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                    else if (this.uCheckCompleteFlag3.Checked == true && this.uCheckCompleteFlag3.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001313", "M001313", "M001325", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 1차 접수 취소 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancleReceipt1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (this.uCheckCompleteFlag1.Checked == true && this.uCheckCompleteFlag1.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001319", "M001310", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = "";
                        string strTime = "";

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, m_intTabIndex.ToString(), strDate
                                , strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001319", "M001311", Infragistics.Win.HAlign.Right);

                            this.uTextReceiptTime1.Text = "";
                            this.uDateReceiptDate1.Value = "";

                            this.uButtonCancleReceipt1.Visible = false;
                            this.uButtonReceipt1.Visible = true;

                            // 2012-11-01 CCS의뢰정보가 접수전이면 CCS의뢰취소가능 
                            this.uCheckCCSCancel_1.Visible = true;
                            this.uLabelCCSCancel_1.Visible = true;
                            this.uButtonCCSCancel_1.Visible = true;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                 , "M001264", "M001319", "M001312", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 2차 접수 취소 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancleReceipt2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (this.uCheckCompleteFlag2.Checked == true && this.uCheckCompleteFlag2.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001319", "M001310", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = "";
                        string strTime = "";

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, m_intTabIndex.ToString(), strDate
                                , strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));


                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001319", "M001311", Infragistics.Win.HAlign.Right);

                            this.uTextReceiptTime2.Text = "";
                            this.uDateReceiptDate2.Value = "";

                            this.uButtonCancleReceipt2.Visible = false;
                            this.uButtonReceipt2.Visible = true;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001319", "M001312", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 3차 접수 취소 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCancleReceipt3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextReqNo.Text.Equals(string.Empty))
                {
                    m_intTabIndex = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (this.uCheckCompleteFlag3.Checked == true && this.uCheckCompleteFlag3.Enabled == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001319", "M001310", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        string strFullReqNo = this.uTextReqNo.Text;
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strReqNo = strFullReqNo.Substring(0, 8);
                        string strReqSeq = strFullReqNo.Substring(8, 4);
                        string strReqLotSeq = m_intTabIndex.ToString();
                        string strDate = "";
                        string strTime = "";

                        string strErrRtn = clsLot.mfSaveINSCCSInspectReqLot_ReceiptTime(strPlantCode, strReqNo, strReqSeq, m_intTabIndex.ToString(), strDate
                                , strTime, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));


                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001319", "M001311", Infragistics.Win.HAlign.Right);

                            this.uTextReceiptTime3.Text = "";
                            this.uDateReceiptDate3.Value = "";

                            this.uButtonCancleReceipt3.Visible = false;
                            this.uButtonReceipt3.Visible = true;

                            return;
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001319", "M001312", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 1차 CCS 의뢰 취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCCSCancel_1_Click(object sender, EventArgs e)
        {
            try
            {

                // CCS 의뢰취소여부가 체크가 안되어 있다면 Return
                // CCS 의뢰취소여부가 비활성화 되어있다면 Return
                if (!this.uCheckCCSCancel_1.Checked 
                    || !this.uCheckCCSCancel_1.Enabled
                    || !this.uCheckCompleteFlag1.Enabled)
                    return;

                // CCS의뢰취소 DB UPDATE -> DB DELETE
                CCSCancel_MES_EQPDown_Delete();

                ////// CCS 의뢰 취소후 CCS의뢰 취소여부 비활성화처리
                ////if(CCSCancel())
                ////    this.uCheckCCSCancel_1.Enabled = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 2차 CCS 의뢰 취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCCSCancel_2_Click(object sender, EventArgs e)
        {
            try
            {

                // CCS 의뢰취소여부가 체크가 안되어 있다면 Return
                // CCS 의뢰취소여부가 비활성화 되어있다면 Return
                if (!this.uCheckCCSCancel_2.Checked
                    || !this.uCheckCCSCancel_2.Enabled
                    || this.uCheckCompleteFlag2.Enabled)
                    return;

                // CCS 의뢰 취소후 CCS의뢰 취소여부 비활성화처리
                if (CCSCancel())
                    this.uCheckCCSCancel_2.Enabled = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 3차CCS 의뢰 취소
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCCSCancel_3_Click(object sender, EventArgs e)
        {
            try
            {

                // CCS 의뢰취소여부가 체크가 안되어 있다면 Return
                // CCS 의뢰취소여부가 비활성화 되어있다면 Return
                if (!this.uCheckCCSCancel_3.Checked
                    || !this.uCheckCCSCancel_3.Enabled
                    || this.uCheckCompleteFlag3.Enabled)
                    return;

                // CCS 의뢰 취소후 CCS의뢰 취소여부 비활성화처리
                if (CCSCancel())
                    this.uCheckCCSCancel_3.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 상세 화면 Item 엑셀 변환 버튼 이벤트

        private void uButtonQualExcel_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid1_2.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid1_2);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uButtonProcExcel1_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid1_1.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid1_1);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }



        private void uButtonProcExcel2_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid2_1.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid2_1);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uButtonQualExcel2_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid2_2.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid2_2);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uButtonProcExcel3_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid3_1.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid3_1);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uButtonQualExcel3_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid3_2.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid3_2);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000375",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion

        /// <summary>
        /// 완료된 항목 조회시 검사항목이 아닌 데이터 숨김
        /// </summary>
        /// <param name="intReqLotSeq"></param>
        public void SetUnvisivleGrid(int intReqLotSeq)
        {
            try
            {
                if (intReqLotSeq == 1)
                {
                    if (this.uCheckCompleteFlag1.Checked == true && this.uCheckCompleteFlag1.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid1_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid1_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid1_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                        {
                            if (this.uGrid1_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid1_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                }
                else if (intReqLotSeq == 2)
                {
                    if (this.uCheckCompleteFlag1.Checked == true && this.uCheckCompleteFlag1.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid1_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid1_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid1_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                        {
                            if (this.uGrid1_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid1_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                    if (this.uCheckCompleteFlag2.Checked == true && this.uCheckCompleteFlag2.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid2_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid2_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid2_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                        {
                            if (this.uGrid2_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid2_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid2_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid2_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid2_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid2_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                }
                else if (intReqLotSeq == 3)
                {
                    if (this.uCheckCompleteFlag1.Checked == true && this.uCheckCompleteFlag1.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid1_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid1_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid1_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid1_2.Rows.Count; i++)
                        {
                            if (this.uGrid1_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid1_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid1_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                    if (this.uCheckCompleteFlag2.Checked == true && this.uCheckCompleteFlag2.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid2_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid2_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid2_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid2_2.Rows.Count; i++)
                        {
                            if (this.uGrid2_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid2_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid2_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid2_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid2_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid2_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                    if (this.uCheckCompleteFlag3.Checked == true && this.uCheckCompleteFlag3.Enabled == false)
                    {
                        //for (int i = 0; i < this.uGrid3_1.Rows.Count; i++)
                        //{
                        //    if (this.uGrid3_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        //    {
                        //        this.uGrid3_1.Rows[i].Hidden = true;
                        //    }
                        //}
                        for (int i = 0; i < this.uGrid3_2.Rows.Count; i++)
                        {
                            if (this.uGrid3_2.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                            {
                                this.uGrid3_2.Rows[i].Hidden = true;
                            }
                        }
                        //this.uGrid3_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                        this.uGrid3_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                    }
                    else
                    {
                        //this.uGrid3_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                        this.uGrid3_2.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = false;
                    }
                }

                ///1차의뢰 생산Item Hidden
                if (this.uGrid1_1.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGrid1_1.Rows.Count; i++)
                    {
                        if (this.uGrid1_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid1_1.Rows[i].Hidden = true;
                        }
                    }
                    this.uGrid1_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
                //2차의뢰 생산 Item Hidden
                if (this.uGrid2_1.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGrid2_1.Rows.Count; i++)
                    {
                        if (this.uGrid2_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid2_1.Rows[i].Hidden = true;
                        }
                    }
                    this.uGrid2_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
                //3차의뢰 생산 Item Hidden
                if (this.uGrid3_1.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGrid3_1.Rows.Count; i++)
                    {
                        if (this.uGrid3_1.Rows[i].Cells["InspectIngFlag"].Value.ToString().Equals("false"))
                        {
                            this.uGrid3_1.Rows[i].Hidden = true;
                        }
                    }
                    this.uGrid3_1.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비 Locking/UnLocking I/F 용 데이터 테이블

        /// </summary>
        /// <returns></returns>
        private DataTable Set_Datatable_EQPDowninfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // 컬럼생성
                dtRtn.Columns.Add("FormName", typeof(string));
                dtRtn.Columns.Add("PlantCode", typeof(string));             // 공장코드
                dtRtn.Columns.Add("InspectUserID", typeof(string));         // 의뢰 : 의뢰자, 의뢰현황 : 검사원
                dtRtn.Columns.Add("LotNo", typeof(string));                 // LotNo
                dtRtn.Columns.Add("EquipCode", typeof(string));             // 설비번호
                dtRtn.Columns.Add("DownCode", typeof(string));              // DOWNCODE
                dtRtn.Columns.Add("LockingFlag", typeof(string));           // LockingFlag (Y/N)
                dtRtn.Columns.Add("Comment", typeof(string));               // 비고
                dtRtn.Columns.Add("InterfaceType", typeof(string));         // InterFace유형(LOCKING/UNLOCKING)

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 설비 Locking 필요한경우 데이터 정보 만들어 반환
        /// </summary>
        /// <returns></returns>
        private DataTable SaveEQPDowninfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // 1차
                if (this.uTabCCS.SelectedTab.Key.Equals("1st"))
                {
                    // 합격이고, 의뢰에서 설비 Locking 한경우 Unlocking I/F
                    if (this.uTextMESLockingFlag1.Text.Equals("T") && this.uOptionInspectResult1.CheckedIndex == 0)
                    {
                        string strLotNo = this.uTextLotNo1.Text.Trim();
                        // 현재 설비상태 체크. 2012-10-15 
                        bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text, strLotNo,"1");
                        // True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 이외의 경우 반환.                        
                        if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                            return dtRtn;

                        dtRtn = Set_Datatable_EQPDowninfo();
                        DataRow _dr = dtRtn.NewRow();
                        _dr["FormName"] = this.Name;
                        _dr["PlantCode"] = this.uTextPlantCode.Text;
                        _dr["InspectUserID"] = this.uTextReqUserID1.Text.ToUpper();
                        _dr["LotNo"] = strLotNo;
                        _dr["EquipCode"] = this.uTextEquipCode.Text;
                        _dr["DownCode"] = "D160";
                        _dr["LockingFlag"] = "N";
                        _dr["Comment"] = this.uTextEtcDesc1.Text;
                        _dr["InterfaceType"] = "UNLOCKING";
                        dtRtn.Rows.Add(_dr);
                    }
                    // 불합격이고 의뢰에서 설비 Locking 하지 않은경우 Locking I/F
                    else if (this.uOptionInspectResult1.CheckedIndex == 1 && this.uTextMESLockingFlag1.Text.Equals("F"))
                    {

                        string strLotNo = this.uTextLotNo1.Text.Trim();
                        // 현재 설비상태 체크. 2012-10-15 
                        bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text, strLotNo, "1");
                        // True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 이외의 경우 반환.                        
                        if (bolChk)        // 설비상태가 "CCS Accept" 상태이면 I/F를 Skip한다.
                            return dtRtn;

                        dtRtn = Set_Datatable_EQPDowninfo();
                        DataRow _dr = dtRtn.NewRow();
                        _dr["FormName"] = this.Name;
                        _dr["PlantCode"] = this.uTextPlantCode.Text;
                        _dr["InspectUserID"] = this.uTextReqUserID1.Text.ToUpper();
                        _dr["LotNo"] = this.uTextLotNo1.Text.Trim();
                        _dr["EquipCode"] = this.uTextEquipCode.Text;
                        _dr["DownCode"] = "D160";
                        _dr["LockingFlag"] = "Y";
                        _dr["Comment"] = this.uTextEtcDesc1.Text;
                        _dr["InterfaceType"] = "LOCKING";
                        dtRtn.Rows.Add(_dr);
                    }
                }
                else if (this.uTabCCS.SelectedTab.Key.Equals("2nd"))
                {
                    //2차 

                    // 2차 합격이고, 1차 의뢰에서 설비 Locking 한경우 Unlocking I/F
                    if (this.uTextMESLockingFlag1.Text.Equals("T") && this.uOptionInspectResult2.CheckedIndex == 0)
                    {
                        string strLotNo = this.uTextLotNo2.Text.Trim();
                        // 현재 설비상태 체크. 2012-10-15 
                        bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text,strLotNo,"2");
                        // True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 경우 반환.                        
                        if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                            return dtRtn;

                        dtRtn = Set_Datatable_EQPDowninfo(); //2012-10-15
                        DataRow _dr = dtRtn.NewRow();
                        _dr["FormName"] = this.Name;
                        _dr["PlantCode"] = this.uTextPlantCode.Text;
                        _dr["InspectUserID"] = this.uTextReqUserID2.Text.ToUpper();
                        _dr["LotNo"] = strLotNo;
                        _dr["EquipCode"] = this.uTextEquipCode.Text;
                        _dr["DownCode"] = "D160";
                        _dr["LockingFlag"] = "N";
                        _dr["Comment"] = this.uTextEtcDesc2.Text;
                        _dr["InterfaceType"] = "UNLOCKING";
                        dtRtn.Rows.Add(_dr);
                    }
                }
                else if (this.uTabCCS.SelectedTab.Key.Equals("3th"))
                {
                    // 3차

                    // 3차 합격이고, 1차 의뢰에서 설비 Locking 한경우 Unlocking I/F
                    if (this.uTextMESLockingFlag1.Text.Equals("T") && this.uOptionInspectResult3.CheckedIndex == 0)
                    {
                        string strLotNo = this.uTextLotNo3.Text.Trim();
                        // 현재 설비상태 체크. 2012-10-15 
                        bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text,strLotNo,"3");
                        // True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 경우 반환.                        
                        if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                            return dtRtn;

                        dtRtn = Set_Datatable_EQPDowninfo(); //2012-10-15
                        DataRow _dr = dtRtn.NewRow();
                        _dr["FormName"] = this.Name;
                        _dr["PlantCode"] = this.uTextPlantCode.Text;
                        _dr["InspectUserID"] = this.uTextReqUserID3.Text.ToUpper();
                        _dr["LotNo"] = strLotNo;
                        _dr["EquipCode"] = this.uTextEquipCode.Text;
                        _dr["DownCode"] = "D160";
                        _dr["LockingFlag"] = "N";
                        _dr["Comment"] = this.uTextEtcDesc3.Text;
                        _dr["InterfaceType"] = "UNLOCKING";
                        dtRtn.Rows.Add(_dr);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                // 2012-12-03 추가
                string strFail = string.Empty;
                if (this.uTextMESLockingFlag1.Text.Equals("T"))     // 1차의뢰에서 Lacking 발생된설비면 FAIL
                    strFail = "CATCH";
                else if (this.uTextMESLockingFlag1.Text.Equals("F")) // W/B Lacking 실패시 쿼리에서 MES 설비상태 확인 후 MESLackFlag 설정
                    strFail = "WBCATCH";
                else
                    strFail = "CATCH";

                CCSCATCH(strFail);

                // 2012-12-03 추가 끝

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS의뢰 완료시 설비상태변경
        /// </summary>
        /// <returns></returns>
        private bool Save_MES_EQPDown()
        {
            try
            {
                DataTable dtEqpDown = SaveEQPDowninfo();
                if (dtEqpDown.Rows.Count > 0)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    TransErrRtn ErrRtn = new TransErrRtn();
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    // MES I/F 메소드 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                    QRPCCS.BL.INSCCS.CCSMESInterface clsMES = new QRPCCS.BL.INSCCS.CCSMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAcce);
                    DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, clsMES.MesCode);

                    DataTable dtMESResult = clsMES.mfSend_EQP_DOWN4QC(dtEqpDown, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtSysAcce);

                    if (dtMESResult.Rows.Count > 0)
                    {
                        string strErrRtn = string.Empty;
                        int intReqLotSeq = m_intTabIndex + 1;

                        string strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                        string strReqSeq = this.uTextReqNo.Text.Substring(8, 4);

                        // 성공시
                        if (dtMESResult.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                            QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                            brwChannel.mfCredentials(clsLot);
                            // MES Locking 성공Flag Update 메소드 호출
                            strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(this.uTextPlantCode.Text
                                                                                    , strReqNo, strReqSeq, intReqLotSeq
                                                                                    , dtEqpDown.Rows[0]["InterfaceType"].ToString()
                                                                                    , "T", m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            clsLot.Dispose();
                            if (ErrRtn.ErrNum == 0)
                            {
                                clsMES.Dispose();
                                clsSysAcce.Dispose();
                                return true;
                            }
                        }
                        else  // 실패시
                        {
                            //if (dtEqpDown.Rows[0]["InterfaceType"].ToString().Equals("LOCKING")) // 주석2012.10.12
                            //{                                                                    // 주석2012.10.12 
                                ////// I/F 실패시 LOKING 요청의 경우 CCS정보 삭제
                                ////brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                                ////QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                                ////brwChannel.mfCredentials(clsHeader);
                                ////strErrRtn = clsHeader.mfDeleteCCSInspectReq_PSTS(this.uTextPlantCode.Text, strReqNo, strReqSeq, intReqLotSeq);

                            // 2012-11-08 주석 권종구 -> 2923 줄 mfSave() Exception 발생시 수정안하기때문
                                ////brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                                ////QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                                ////brwChannel.mfCredentials(clsLot);
                                ////// I/F 실패시 CompleteFlag 'F' 로 업데이트
                                ////strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(this.uTextPlantCode.Text
                                ////                                                    , strReqNo, strReqSeq, intReqLotSeq
                                ////                                                    , "FAIL"
                                ////                                                    , "F", m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                            //} // 주석2012.10.12

                            if (dtMESResult.Rows[0]["returnmessage"].ToString().Trim().Equals(string.Empty)) //
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000955",
                                        Infragistics.Win.HAlign.Right);

                                clsMES.Dispose();
                                clsSysAcce.Dispose();
                                return false; // False  메세지 뿌린후에 return처리
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                        , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                        , dtMESResult.Rows[0]["returnmessage"].ToString()
                                        , Infragistics.Win.HAlign.Right);

                                clsMES.Dispose();
                                clsSysAcce.Dispose();
                                return false; // False
                            }

                            ////if (ErrRtn.ErrNum.Equals(0))
                            ////{
                            ////    if (dtMESResult.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                            ////    {
                            ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                            ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            ////                "M001135", "M001037", "M001474",
                            ////                Infragistics.Win.HAlign.Right);
                            ////        return false;
                            ////    }
                            ////    else
                            ////    {
                            ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                            ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            ////                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                            ////                , dtMESResult.Rows[0]["returnmessage"].ToString()
                            ////                , Infragistics.Win.HAlign.Right);
                            ////        return false;
                            ////    }
                            ////}
                            ////else
                            ////{
                            ////    if (dtMESResult.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                            ////    {
                            ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                            ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            ////                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                            ////                , msg.GetMessge_Text("M001475", m_resSys.GetString("SYS_LANG")) + "<br/> KEY : " + this.uTextReqNo.Text
                            ////                , Infragistics.Win.HAlign.Right);
                            ////        return false;
                            ////    }
                            ////    else
                            ////    {
                            ////        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                            ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            ////                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                            ////                , dtMESResult.Rows[0]["returnmessage"].ToString()
                            ////                , Infragistics.Win.HAlign.Right);
                            ////        return false;
                            ////    }
                            ////}
                        }
                    }
                    else
                    {

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001135", "M000057", "M000096",
                                           Infragistics.Win.HAlign.Right);
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 unLocking 필요한경우 데이터 정보 만들어 반환 2012-11-01
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns></returns>
        private DataTable CancelEQPDowninfo(string strPlantCode, string strEquipCode, string strLotNo)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                //// 1차
                //if (this.uTabCCS.SelectedTab.Key.Equals("1st"))
                //{
                //    // 의뢰에서 설비 Locking 한경우 Unlocking I/F
                //    if (this.uTextMESLockingFlag1.Text.Equals("T"))
                //    {
                        //// 현재 설비상태 체크. 2012-10-15 --> 2012-11-01 CCS취소시 확인함
                        //bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text, strLotNo, "1");
                        //// True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 이외의 경우 반환.                        
                        //if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                        //    return dtRtn;

                        dtRtn = Set_Datatable_EQPDowninfo();
                        DataRow _dr = dtRtn.NewRow();
                        _dr["FormName"] = this.Name;
                        _dr["PlantCode"] = strPlantCode;
                        _dr["InspectUserID"] = this.uTextReqUserID1.Text.ToUpper();
                        _dr["LotNo"] = strLotNo;
                        _dr["EquipCode"] = strEquipCode;
                        _dr["DownCode"] = "D160";
                        _dr["LockingFlag"] = "N";
                        _dr["Comment"] = this.uTextEtcDesc1.Text;
                        _dr["InterfaceType"] = "UNLOCKING";
                        dtRtn.Rows.Add(_dr);
                //    }
                //}
                // 2012-11-01 CCS의뢰취소 1차만(일단).
                //else if (this.uTabCCS.SelectedTab.Key.Equals("2nd"))
                //{
                //    //2차 

                //    // 2차 합격이고, 1차 의뢰에서 설비 Locking 한경우 Unlocking I/F
                //    if (this.uTextMESLockingFlag1.Text.Equals("T"))
                //    {
                //        //// 현재 설비상태 체크. 2012-10-15 
                //        //bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text, strLotNo, "2");
                //        //// True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 경우 반환.                        
                //        //if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                //        //    return dtRtn;

                //        dtRtn = Set_Datatable_EQPDowninfo(); //2012-10-15
                //        DataRow _dr = dtRtn.NewRow();
                //        _dr["FormName"] = this.Name;
                //        _dr["PlantCode"] = this.uTextPlantCode.Text;
                //        _dr["InspectUserID"] = this.uTextReqUserID2.Text.ToUpper();
                //        _dr["LotNo"] = strLotNo;
                //        _dr["EquipCode"] = this.uTextEquipCode.Text;
                //        _dr["DownCode"] = "D160";
                //        _dr["LockingFlag"] = "N";
                //        _dr["Comment"] = this.uTextEtcDesc2.Text;
                //        _dr["InterfaceType"] = "UNLOCKING";
                //        dtRtn.Rows.Add(_dr);
                //    }
                //}
                //else if (this.uTabCCS.SelectedTab.Key.Equals("3th"))
                //{
                //    // 3차

                //    // 3차 합격이고, 1차 의뢰에서 설비 Locking 한경우 Unlocking I/F
                //    if (this.uTextMESLockingFlag1.Text.Equals("T"))
                //    {
                //        //// 현재 설비상태 체크. 2012-10-15 
                //        //bool bolChk = CheckCCS(this.uTextPlantCode.Text, this.uTextEquipCode.Text, strLotNo, "3");
                //        //// True는 "CCS Accept" 상태반환, False는 "CCS Accept" 상태가 아닌 경우 반환.                        
                //        //if (!bolChk)        // 설비상태가 "CCS Accept"가 아닌 상태이면 I/F를 Skip한다.
                //        //    return dtRtn;

                //        dtRtn = Set_Datatable_EQPDowninfo(); //2012-10-15
                //        DataRow _dr = dtRtn.NewRow();
                //        _dr["FormName"] = this.Name;
                //        _dr["PlantCode"] = this.uTextPlantCode.Text;
                //        _dr["InspectUserID"] = this.uTextReqUserID3.Text.ToUpper();
                //        _dr["LotNo"] = strLotNo;
                //        _dr["EquipCode"] = this.uTextEquipCode.Text;
                //        _dr["DownCode"] = "D160";
                //        _dr["LockingFlag"] = "N";
                //        _dr["Comment"] = this.uTextEtcDesc3.Text;
                //        _dr["InterfaceType"] = "UNLOCKING";
                //        dtRtn.Rows.Add(_dr);
                //    }
                //}
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS의뢰취소시 설비상태 변경 2012-11-01
        /// </summary>
        /// <returns></returns>
        private void CCSCancel_MES_EQPDown_Delete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                TransErrRtn ErrRtn = new TransErrRtn();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 로그인사용자가 품질부서가 아닌경우 CCS의뢰 취소 불가
                if (!m_resSys.GetString("SYS_DEPTCODE").Equals("1010"))
                {
                    // CCS의뢰취소는 품질부서만 가능합니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001531", "M001532", Infragistics.Win.HAlign.Right);
                    return;
                }

                string strLotNo = string.Empty;

                // 각 1, 2, 3차 탭 작성완료 여부 확인
                if (this.uTabCCS.SelectedTab.Index == 0)
                {
                    // 작성완료 여부확인
                    if (this.uCheckCompleteFlag1.Checked && !this.uCheckCompleteFlag1.Enabled)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000021", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    strLotNo = this.uTextLotNo1.Text.Trim();
                }
                else if (this.uTabCCS.SelectedTab.Index == 1)
                {
                    if (this.uCheckCompleteFlag2.Checked && !this.uCheckCompleteFlag2.Enabled)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000023", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    strLotNo = this.uTextLotNo2.Text.Trim();
                }
                else if (this.uTabCCS.SelectedTab.Index == 2)
                {
                    if (this.uCheckCompleteFlag3.Checked && !this.uCheckCompleteFlag3.Enabled)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000825", "M000026", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    strLotNo = this.uTextLotNo3.Text.Trim();
                }

                // CCS 의뢰취소 사용자 여부 확인
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000650", "M000665",
                                       Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;

                // 공장, 설비, 의뢰차수 저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uTextEquipCode.Text.Trim();
                int intReqLotSeq = ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsEquip = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsEquip);

                // MES 설비상태관리 검색 매서드 실행
                DataTable dtEquip = clsEquip.mfReadCCSInspectReq_MESDB(strPlantCode, strEquipCode, this.uTextNowProcessCode.Text, strLotNo, intReqLotSeq.ToString(), m_resSys.GetString("SYS_LANG"));
                clsEquip.Dispose();

                // 작성완료 처리 하였는지 확인
                // 작성완료되었다면 메세지 출력 Return
                if (dtEquip.Rows.Count > 0)
                {
                    // 접수일이 공백이아닌경우
                    if (!dtEquip.Rows[0]["ReceiptDate"].ToString().Trim().Equals(string.Empty))
                    {
                        // CCS접수or완료된 정보는 취소할 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M000984", "M000980", "M001528", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    // 작성완료된 정보인경우
                    if (dtEquip.Rows[0]["CompleteFlag"].ToString().ToUpper().Trim().Equals("T"))
                    {
                        // CCS접수or완료된 정보는 취소할 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M000984", "M000980", "M001528", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    // CCS 의뢰상태체크. 2012-10-31
                    // 설비상태가 CCS 의뢰상태가 아닌경우 정보삭제 함.
                    if (!dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                    {
                        if (DeleteCCSInsepctReq(strPlantCode, strLotNo, intReqLotSeq,false))
                            mfSearch();

                        // 삭제완료 후 Return
                        return;
                    }
                }
                else
                {
                    // CCS의뢰취소에 실패하였습니다. <br/> 다시 시도해주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001177", "M001529", Infragistics.Win.HAlign.Right);

                    return;
                }
               

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // CCS Unlacking 정보 저장
                DataTable dtEqpDown = CancelEQPDowninfo(strPlantCode,strEquipCode,strLotNo);
                if (dtEqpDown.Rows.Count > 0)
                {

                    // MES I/F 메소드 호출
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSMESInterface), "CCSMESInterface");
                    QRPCCS.BL.INSCCS.CCSMESInterface clsMES = new QRPCCS.BL.INSCCS.CCSMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    // 2012-11-01
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAcce);
                    DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, clsMES.MesCode);

                    // MES 설비 UnLacking I/F 매서드 실행
                    DataTable dtMESResult = clsMES.mfSend_EQP_DOWN4QC(dtEqpDown, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtSysAcce);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // MES 전송성공시 Code를 통하여 성공여부 판단.
                    if (dtMESResult.Rows.Count > 0)
                    {
                        string strErrRtn = string.Empty;

                        // MES Return Code가 정상적으로 처리를 하였다면 CCS정보 삭제 매서드 실행
                        if (dtMESResult.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                        {
                            bool bolSave = DeleteCCSInsepctReq(strPlantCode, strLotNo, intReqLotSeq,true);

                            // 삭제 성공시 Refresh
                            if (bolSave)
                                mfSearch();

                            return;
                        }
                        else
                        {
                            // MES I/F 실패시 ReturnMessage 출력
                            if (dtMESResult.Rows[0]["returnmessage"].ToString().Trim().Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000955",
                                        Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                        , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                        , dtMESResult.Rows[0]["returnmessage"].ToString()
                                        , Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001135", "M000057", "M000096",
                                           Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                else
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    return;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CCS 의뢰상태여부 검색 (의뢰상태면 Lacking처리 불가) true : CCS 의뢰상태
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strReqLLotSeq">의뢰차수</param>
        /// <returns>true : CCS 의뢰상태</returns>
        private bool CheckCCS(string strPlantCode, string strEquipCode,string strLotNo,string strReqLotSeq)
        {
            bool bolChk = false;

            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsEquip = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsEquip);

                // MES 설비상태관리 검색 매서드 실행
                DataTable dtEquip = clsEquip.mfReadCCSInspectReq_MESDB(strPlantCode, strEquipCode, this.uTextNowProcessCode.Text, strLotNo, strReqLotSeq, m_resSys.GetString("SYS_LANG"));
                clsEquip.Dispose();

                //2012-12-03 추가
                if (dtEquip.Rows.Count == 0)
                {
                    CCSCATCH("FAIL");   //2012-12-03 추가 끝
                }
                else if (dtEquip.Rows.Count > 0 && dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))  // CCS 의뢰상태체크. 2012-10-15
                    bolChk = true;
                
                m_resSys.Close();
                return bolChk;
            }
            catch (Exception ex)
            {
                // 2012-12-03 추가
                string strFail = string.Empty;
                if (this.uTextMESLockingFlag1.Text.Equals("T"))     // 1차의뢰에서 Lacking 발생된설비면 FAIL
                    strFail = "CATCH";
                else if (this.uTextMESLockingFlag1.Text.Equals("F")) // W/B Lacking 실패시 쿼리에서 MES 설비상태 확인 후 MESLackFlag 설정
                    strFail = "WBCATCH";
                else
                    strFail = "CATCH";

                CCSCATCH(strFail);

                // 2012-12-03 추가 끝

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return bolChk;
            }
            finally
            { }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private Int32 ReturnIntegerValue(object value)
        {
            Int32 result = 0;

            string[] arrValue = value.ToString().Split('.');

            string strValue;

            if (arrValue.Length > 1)
                strValue = arrValue[0];
            else
                strValue = value.ToString();

            if (Int32.TryParse(strValue, out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// 3차 검사결과값 합격으로 고정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uOptionInspectResult3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionInspectResult3.CheckedIndex.Equals(1))
                    this.uOptionInspectResult3.CheckedIndex = 0;
                else
                    return;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량 상세등록 POPUP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridQCItem_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //불량 CellButton 아닌경우 Return;
                if (!e.Cell.Column.Key.Equals("InspectFaultTypeCode"))
                    return;
                if(this.uTextReqNo.Text.Equals(string.Empty))
                    return;

                if (e.Cell.Row.GetCellValue("FaultQty") == null     // 불량수량
                    || e.Cell.Row.GetCellValue("FaultQty").ToString().Equals(string.Empty)
                    || Convert.ToDecimal(e.Cell.Row.GetCellValue("FaultQty")) == 0)
                    return;

                //선택한 Cell을 저장
//                Infragistics.Win.UltraWinGrid.UltraGridCell uCell = (Infragistics.Win.UltraWinGrid.UltraGridCell)sender;

                //불량정보등록 POPUP창정보를 입력후 Display
                frmCCSZ0003_POP frmPOP = new frmCCSZ0003_POP();
                frmPOP.strPlantCode = this.uTextPlantCode.Text;     //공장
                frmPOP.strProcCode = this.uTextNowProcessCode.Text;//공정
                frmPOP.strFullReqNo = this.uTextReqNo.Text;         //관리번호
                frmPOP.strReqLotSeq = (this.uTabCCS.SelectedTab.Index + 1).ToString();//Lot순번
                frmPOP.strReqItemSeq = e.Cell.Row.GetCellValue("ReqItemSeq").ToString(); //검사항목순번
                //작성완료여부
                if(this.uTabCCS.SelectedTab.Index + 1 == 1)
                    frmPOP.bolCheck = this.uCheckCompleteFlag1.Enabled;
                if (this.uTabCCS.SelectedTab.Index + 1 == 2)
                    frmPOP.bolCheck = this.uCheckCompleteFlag2.Enabled;
                if (this.uTabCCS.SelectedTab.Index + 1 == 3)
                    frmPOP.bolCheck = this.uCheckCompleteFlag3.Enabled;

                frmPOP.dbFaultQty = Convert.ToDecimal(e.Cell.Row.GetCellValue("FaultQty")); // 불량수량
                
                frmPOP.ShowDialog();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CCS의뢰 취소
        /// </summary>
        private bool CCSCancel()
        {
            try
            {
                bool bolResult = false;

                WinMessageBox msg = new WinMessageBox();

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M001179", "M001524",
                                       Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //CCS 의뢰 차수
                    string strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                    string strReqSeq = this.uTextReqNo.Text.Substring(8, 4);
                    string strReqLotSeq = (ReturnIntegerValue(this.uTabCCS.SelectedTab.Index.ToString()) + 1).ToString();

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "취소중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    QRPCCS.BL.INSCCS.CCSInspectReqLot clsDel = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsDel);

                    string strErrRtn = clsDel.mfSaveINSCCSInspectReqLot_Cancel(this.uTextPlantCode.Text, strReqNo, strReqSeq, strReqLotSeq,"T",
                                                                                m_resSys.GetString("SYS_USERIP"),m_resSys.GetString("SYS_USERID"));

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 결과검사

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    clsDel.Dispose();

                    DialogResult Result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001179", "M001177", "M001523",
                                            Infragistics.Win.HAlign.Right);
                        bolResult = true; // 성공시 true 변경

                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001179", "M001177", "M001522",
                                            Infragistics.Win.HAlign.Right);
                    }

                }

                return bolResult;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료 안된 의뢰건에 대해서 취소(Data삭제) 한다. 2012-10-31 CCS의뢰취소 시 삭제안하고 이력남김 -> 삭제로 변경
        /// </summary>
        private bool DeleteCCSInsepctReq(string strPlantCode, string strLotNo, int intReqLotSeq, bool bol)
        {
            try
            {
                bool bolChk = false;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                string strLang = m_resSys.GetString("SYS_LANG");

                string strEquipCode = this.uTextEquipCode.Text;
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();

                if (bol)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                    QRPCCS.BL.INSCCS.CCSInspectReqH clsEquip = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                    brwChannel.mfCredentials(clsEquip);

                    // MES 설비상태관리 검색 매서드 실행
                    DataTable dtEquip = clsEquip.mfReadCCSInspectReq_MESDB(strPlantCode, strEquipCode, this.uTextNowProcessCode.Text, strLotNo, intReqLotSeq.ToString(), m_resSys.GetString("SYS_LANG"));
                    clsEquip.Dispose();

                    // MES 설비상태 확인
                    if (dtEquip.Rows.Count > 0)
                    {
                        // CCS 의뢰상태체크. 2012-11-19
                        // 설비상태가 CCS 의뢰상태인 경우 정보삭제 안함.
                        if (dtEquip.Rows[0]["DOWNSTATUS"].ToString().ToUpper().Equals("CCSACCEPT"))
                        {
                            // CCS의뢰취소에 실패하였습니다. <br/> 다시 시도해주세요.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001177", "M001529", Infragistics.Win.HAlign.Right);
                            return bolChk;
                        }
                    }
                    else
                    {
                        // CCS의뢰취소에 실패하였습니다. <br/> 다시 시도해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001177", "M001529", Infragistics.Win.HAlign.Right);

                        return bolChk;
                    }
                }

                //CCS 의뢰 차수
                string strReqNo = this.uTextReqNo.Text.Substring(0, 8);
                string strReqSeq = this.uTextReqNo.Text.Substring(8, 4);

                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                QRPCCS.BL.INSCCS.CCSInspectReqH clsDel = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                brwChannel.mfCredentials(clsDel);

                string strErrRtn = clsDel.mfDeleteCCSInspectReq_Cancel(strPlantCode, strReqNo, strReqSeq, intReqLotSeq);

                clsDel.Dispose();

                // 결과검사
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!strErrRtn.Equals(string.Empty) && ErrRtn.ErrNum == 0)
                {

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M000638", "M000666",
                                        Infragistics.Win.HAlign.Right);

                    bolChk = true;
                }
                else
                {
                    //메세지정보를 담을 변수선언
                    string strMes = "";
                    //삭제 에러 메세지를 변수에 담는다.
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000667", strLang);
                    else
                        strMes = ErrRtn.ErrMessage;
                    //삭제 에러 메세지를 출력한다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", strLang)
                                                , msg.GetMessge_Text("M000638", strLang), strMes
                                                , Infragistics.Win.HAlign.Right);
                    
                }

                
               
                m_resSys.Close();

                return bolChk;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }


        /// <summary>
        /// CCS 저장중 문제가 발생했을 경우 OI 설비상태에 따라 completeFlag 변경
        /// </summary>
        /// <param name="strFail"></param>
        /// <returns></returns>
        bool CCSCATCH(string strFail)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strReqNo = this.uTextReqNo.Text.Trim().Substring(0, 8);
                string strReqSeq = this.uTextReqNo.Text.Trim().Substring(8, 4);
                
                int intReqLotSeq = m_intTabIndex + 1;

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                TransErrRtn ErrRtn = new TransErrRtn();

                string strErrRtn = clsLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(this.uTextPlantCode.Text
                                                                    , strReqNo, strReqSeq, intReqLotSeq
                                                                    , strFail
                                                                    , "F", m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                clsLot.Dispose();

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (strErrRtn.Equals(string.Empty) || ErrRtn.ErrNum != 0)
                {
                    m_resSys.Close();
                    return false;
                }
                

                m_resSys.Close();
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// CCS 체크
        /// </summary>
        /// <returns></returns>
        bool CCSStateCheck(bool bolCompleteChk)
        {
            bool bolResult = false;
            try
            {

                // System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strReqNo = this.uTextReqNo.Text.Trim().Substring(0, 8);
                string strReqSeq = this.uTextReqNo.Text.Trim().Substring(8, 4);
                string strGubun = string.Empty;

                // 의뢰차수 저장
                int intReqLotSeq = m_intTabIndex + 1;

                if (bolCompleteChk)  // 작성완료체크 한경우
                {
                    if (this.uTextMESLockingFlag1.Text.Equals("T"))     // 1차의뢰에서 Lacking 발생된설비면 
                        strGubun = "Other";
                    else if (this.uTextMESLockingFlag1.Text.Equals("F")) // W/B 공정인경우
                        strGubun = "WB";
                    else
                        strGubun = "Other";
                }
                else
                {
                    strGubun = "NC";    // 작성완료체크안한경우
                }

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                TransErrRtn ErrRtn = new TransErrRtn();

                // 설비상태확인 매서드 실행
                string strErrRtn = clsLot.mfSaveCCSInspectReqLot_StateCheck(this.uTextPlantCode.Text
                                                                    , strReqNo, strReqSeq, intReqLotSeq
                                                                    , this.uTextEquipCode.Text
                                                                    , strGubun, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                // 리소스해제
                clsLot.Dispose();

                // 결과값 저장
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 결과가 처리가 안되었거나 실패인경우 Return
                if (!strErrRtn.Equals(string.Empty) 
                    && ErrRtn.ErrNum == 0)
                {
                    // OUTPUT 값 저장
                    string strState = ErrRtn.mfGetReturnValue(0);

                    // 처리결과가 성공인경우 True값 저장
                    if (!strState.Trim().ToUpper().Equals("FAIL"))
                        bolResult = true;

                }

                // 리소스해제
                m_resSys.Close();

                return bolResult;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return bolResult;
            }
            finally
            { }
        }

        #region CHASE Checkbox Events... 2012-12-13 천안STS 소스 참고

        /// <summary>
        /// CHASE_All CheckBox Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckCHASE_All_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // 이벤트 걸린 체크박스를 Check
                Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck = (Infragistics.Win.UltraWinEditors.UltraCheckEditor)sender;

                // 체크박스 명을 "_" 기준으로 분리를 한다.
                string[] strCheckName = uCheck.Name.Split('_');

                // 
                if (strCheckName[0].Contains("3"))
                {
                    this.uCheckCHASE3_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE3_6.Checked = uCheck.Checked == true ? true : false;

                }
                else if (strCheckName[0].Contains("2"))
                {
                    this.uCheckCHASE2_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE2_6.Checked = uCheck.Checked == true ? true : false;
                }
                else if (strCheckName[0].Contains("1"))
                {
                    this.uCheckCHASE1_1.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_2.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_3.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_4.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_5.Checked = uCheck.Checked == true ? true : false;
                    this.uCheckCHASE1_6.Checked = uCheck.Checked == true ? true : false;
                }
                else
                    return;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// CHASE CheckBox Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uCheckCHASE_CheckedValueChanged(object sender, EventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck = (Infragistics.Win.UltraWinEditors.UltraCheckEditor)sender;

            // 체크박스 명을 "_" 기준으로 분리를 한다.
            string[] strCheckName = uCheck.Name.Split('_');

            // 체크박스에 따라 Lot의뢰차수를 판단.
            int intLotSeq = 0;

            if (strCheckName[0].Contains("3"))
                intLotSeq = 2;
            else if (strCheckName[0].Contains("2"))
                intLotSeq = 1;
            else if (strCheckName[0].Contains("1"))
                intLotSeq = 0;
            else
                return;

            // 체크박스 상태가 체크된 상태 인경우 파라미터를 추가한다.
            if (uCheck.Checked)
            {
                SetParaData_AddRow(intLotSeq, uCheck);
                SetHiddenRow(intLotSeq + 1);
            }
            else
            {
                SetParaData_DelRow(intLotSeq, uCheck);
                SetUnHiddenRow(intLotSeq + 1);
            }

        }

        private void SetParaData_AddRow(int intReqLotSeq, Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //if (!m_resSys.GetString("SYS_USERID").Equals("testusr"))
                //{
                //    DataTable dtPara = new DataTable();
                //    if (intReqLotSeq.Equals(0))
                //    {
                //        dtPara = (DataTable)this.uGrid1_3.DataSource;
                //    }
                //    else if (intReqLotSeq.Equals(1))
                //    {
                //        dtPara = (DataTable)this.uGrid2_3.DataSource;
                //    }
                //    else if (intReqLotSeq.Equals(2))
                //    {
                //        dtPara = (DataTable)this.uGrid3_3.DataSource;
                //    }

                //    if (dtPara == null || !(dtPara.Rows.Count > 0))
                //        return;

                //    string[] strSeparater = { "_" };
                //    string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);

                //    IEnumerable<DataRow> rows = from Para in dtPara.AsEnumerable()
                //                                where Para["CCSParameterCode"].ToString().Equals("Die Temp")
                //                                select Para;

                //    DataTable dtNewPara = dtPara.Copy();
                //    int intRowCount = dtPara.Rows.Count;

                //    foreach (DataRow drRow in rows)
                //    {
                //        intRowCount++;
                //        DataRow _dr = dtPara.NewRow();
                //        foreach (DataColumn dtCol in _dr.Table.Columns)
                //        {
                //            if (dtCol.ColumnName.ToUpper().Equals("CCSPARAMETERCODE"))
                //                _dr.SetField(dtCol, drRow[dtCol] + "_" + strCtlName[1]);
                //            else if (dtCol.ColumnName.ToUpper().Equals("PARASEQ"))
                //                _dr.SetField(dtCol, intRowCount);
                //            else
                //                _dr.SetField(dtCol, drRow[dtCol]);
                //        }
                //        dtNewPara.ImportRow(_dr);
                //        dtNewPara.Rows.Add(_dr.ItemArray);
                //    }

                //    if (intReqLotSeq.Equals(0))
                //    {
                //        this.uGrid1_3.SetDataBinding(dtNewPara, string.Empty);
                //    }
                //    else if (intReqLotSeq.Equals(1))
                //    {
                //        this.uGrid2_3.SetDataBinding(dtNewPara, string.Empty);
                //    }
                //    else if (intReqLotSeq.Equals(2))
                //    {
                //        this.uGrid3_3.SetDataBinding(dtNewPara, string.Empty);
                //    }
                //}
                //else if (m_resSys.GetString("SYS_USERID").Equals("testusr"))
                //{
                DataTable dtPara = new DataTable();
                if (intReqLotSeq.Equals(0))
                {
                    dtPara = (DataTable)this.uGrid1_3.DataSource;
                }
                else if (intReqLotSeq.Equals(1))
                {
                    dtPara = (DataTable)this.uGrid2_3.DataSource;
                }
                else if (intReqLotSeq.Equals(2))
                {
                    dtPara = (DataTable)this.uGrid3_3.DataSource;
                }

                if (dtPara == null || !(dtPara.Rows.Count > 0))
                    return;

                string[] strSeparater = { "_" };
                string str_ = "_/";
                string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);

                IEnumerable<DataRow> rows = from Para in dtPara.AsEnumerable()
                                            where !Para["CCSParameterCode"].ToString().Contains(str_)
                                            select Para;

                DataTable dtNewPara = dtPara.Copy();
                int intRowCount = dtPara.Rows.Count;

                foreach (DataRow drRow in rows)
                {
                    intRowCount++;
                    DataRow _dr = dtPara.NewRow();


                    if (drRow["CCSPARAMETERCODE"].ToString().Contains("TEMPERATURE"))
                    {
                        for (int i = 1; i < 9; i++)
                        {
                            foreach (DataColumn dtCol in _dr.Table.Columns)
                            {
                                if (dtCol.ColumnName.ToUpper().Equals("CCSPARAMETERCODE"))
                                    _dr.SetField(dtCol, drRow[dtCol] + str_ + strCtlName[1] + "-" + i);
                                else if (dtCol.ColumnName.ToUpper().Equals("PARASEQ"))
                                    _dr.SetField(dtCol, intRowCount);
                                else
                                    _dr.SetField(dtCol, drRow[dtCol]);
                            }

                            dtNewPara.ImportRow(_dr);   // 원래 값의 현재 값 과 모든 속성을 그대로 유지한 상태로 DataRow를 DataTable에복사
                            dtNewPara.Rows.Add(_dr.ItemArray); // 배열의 모든항목을 저장
                            intRowCount++;
                        }
                    }
                    else
                    {
                        foreach (DataColumn dtCol in _dr.Table.Columns)
                        {
                            if (dtCol.ColumnName.ToUpper().Equals("CCSPARAMETERCODE"))
                                _dr.SetField(dtCol, drRow[dtCol] + str_ + strCtlName[1]);
                            else if (dtCol.ColumnName.ToUpper().Equals("PARASEQ"))
                                _dr.SetField(dtCol, intRowCount);
                            else
                                _dr.SetField(dtCol, drRow[dtCol]);
                        }
                        dtNewPara.ImportRow(_dr);
                        dtNewPara.Rows.Add(_dr.ItemArray);
                    }
                }

                if (intReqLotSeq.Equals(0))
                {
                    this.uGrid1_3.SetDataBinding(dtNewPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(1))
                {
                    this.uGrid2_3.SetDataBinding(dtNewPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(2))
                {
                    this.uGrid3_3.SetDataBinding(dtNewPara, string.Empty);
                }

                SetParaSeq(intReqLotSeq + 1);
                //}
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetParaData_DelRow(int intReqLotSeq, Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck)
        {
            try
            {
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //if (!m_resSys.GetString("SYS_USERID").Equals("testusr"))
                //{
                //    DataTable dtPara = new DataTable();
                //    if (intReqLotSeq.Equals(0))
                //    {
                //        dtPara = (DataTable)this.uGrid1_3.DataSource;
                //    }
                //    else if (intReqLotSeq.Equals(1))
                //    {
                //        dtPara = (DataTable)this.uGrid2_3.DataSource;
                //    }
                //    else if (intReqLotSeq.Equals(2))
                //    {
                //        dtPara = (DataTable)this.uGrid3_3.DataSource;
                //    }

                //    if (dtPara == null || !(dtPara.Rows.Count > 0))
                //        return;

                //    string[] strSeparater = { "_" };
                //    string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);

                //    DataRow[] drRows = dtPara.Select("CCSparameterCode = '" + "Die Temp_" + strCtlName[1] + "'");

                //    foreach (DataRow dr in drRows)
                //    {
                //        dr.BeginEdit();
                //        dr.Delete();
                //        dr.EndEdit();
                //    }

                //    dtPara.AcceptChanges();

                //    if (intReqLotSeq.Equals(0))
                //    {
                //        this.uGrid1_3.SetDataBinding(dtPara, string.Empty);
                //    }
                //    else if (intReqLotSeq.Equals(1))
                //    {
                //        this.uGrid2_3.SetDataBinding(dtPara, string.Empty);
                //    }
                //    else if (intReqLotSeq.Equals(2))
                //    {
                //        this.uGrid3_3.SetDataBinding(dtPara, string.Empty);
                //    }
                //}
                //else if (m_resSys.GetString("SYS_USERID").Equals("testusr"))
                //{
                DataTable dtPara = new DataTable();
                if (intReqLotSeq.Equals(0))
                {
                    dtPara = (DataTable)this.uGrid1_3.DataSource;
                }
                else if (intReqLotSeq.Equals(1))
                {
                    dtPara = (DataTable)this.uGrid2_3.DataSource;
                }
                else if (intReqLotSeq.Equals(2))
                {
                    dtPara = (DataTable)this.uGrid3_3.DataSource;
                }

                if (dtPara == null || !(dtPara.Rows.Count > 0))
                    return;

                string[] strSeparater = { "_" };
                string[] strCtlName = uCheck.Name.Split(strSeparater, StringSplitOptions.None);
                string strName = "_/" + strCtlName[1];

                DataRow[] drRows = dtPara.Select("CCSparameterCode Like  '%" + strName + "%'");

                foreach (DataRow dr in drRows)
                {
                    dr.BeginEdit();
                    dr.Delete();
                    dr.EndEdit();
                }

                dtPara.AcceptChanges();

                if (intReqLotSeq.Equals(0))
                {
                    this.uGrid1_3.SetDataBinding(dtPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(1))
                {
                    this.uGrid2_3.SetDataBinding(dtPara, string.Empty);
                }
                else if (intReqLotSeq.Equals(2))
                {
                    this.uGrid3_3.SetDataBinding(dtPara, string.Empty);
                }

                SetParaSeq(intReqLotSeq + 1);
                //}
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetHiddenRow(int intReqLotSeq)
        {
            string strContains = "_/";

            ArrayList dinosaurs = CheckCHASE(intReqLotSeq);

            #region intReqLotSeq == 1

            if (intReqLotSeq == 1)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGrid1_3.Rows.Count; j++)
                    {
                        if (!this.uGrid1_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGrid1_3.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < dinosaurs.Count; i++) // Check1_1, Check1_2
                        {

                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null && m_strBOMCode.Count > 0)
                            {
                                if (this.uGrid1_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp)) // _/1 ? _1
                                {
                                    // TEMPERATURE -> _/1, _/2
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGrid1_3.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGrid1_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        this.uGrid1_3.Rows[j].Hidden = _bol;

                    }

                    SetParaSeq(1);
                }
            }
            #endregion
            #region intReqLotSeq == 2
            else if (intReqLotSeq == 2)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGrid2_3.Rows.Count; j++)
                    {
                        if (!this.uGrid2_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGrid2_3.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < dinosaurs.Count; i++)
                        {
                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();
                            // MaterialCode
                            if (m_strBOMCode != null && m_strBOMCode.Count > 0)
                            {
                                if (this.uGrid2_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                {
                                    // TEMPERATURE -> _/1-1, _/1-2 .... _/1-8
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGrid2_3.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGrid2_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        this.uGrid2_3.Rows[j].Hidden = _bol;
                    }
                    SetParaSeq(2);
                }
            }
            #endregion
            #region intReqLotSeq == 3
            else if (intReqLotSeq == 3)
            {
                //uGrid1_3
                if (dinosaurs.Count > 0)
                {
                    for (int j = 0; j < this.uGrid3_3.Rows.Count; j++)
                    {
                        if (!this.uGrid3_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            this.uGrid3_3.Rows[j].Hidden = true;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;
                        for (int i = 0; i < dinosaurs.Count; i++)
                        {
                            if (!_bol)
                                break;

                            strTemp = dinosaurs[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null && m_strBOMCode.Count > 0)
                            {
                                if (this.uGrid3_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                {
                                    // TEMPERATURE -> _/1-1, _/1-2 .... _/1-8
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (this.uGrid3_3.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (this.uGrid3_3.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }

                        }

                        this.uGrid3_3.Rows[j].Hidden = _bol;
                    }
                    SetParaSeq(3);
                }
            }
            #endregion
        }

        private void SetUnHiddenRow(int intReqLotSeq)
        {
            try
            {
                string str_ = "_/";

                if (intReqLotSeq == 1)
                {
                    //uGrid1_3
                    if (this.uCheckCHASE1_1.Checked || this.uCheckCHASE1_2.Checked || this.uCheckCHASE1_3.Checked || this.uCheckCHASE1_4.Checked || this.uCheckCHASE1_5.Checked || this.uCheckCHASE1_6.Checked || this.uCheckCHASE1_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGrid1_3.Rows.Count; i++)
                    {
                        if (!this.uGrid1_3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(str_))
                        {
                            this.uGrid1_3.Rows[i].Hidden = false;
                        }
                    }
                    SetParaSeq(1);
                }
                else if (intReqLotSeq == 2)
                {
                    //uGrid2_3
                    if (this.uCheckCHASE2_1.Checked || this.uCheckCHASE2_2.Checked || this.uCheckCHASE2_3.Checked || this.uCheckCHASE2_4.Checked || this.uCheckCHASE2_5.Checked || this.uCheckCHASE2_6.Checked || this.uCheckCHASE2_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGrid2_3.Rows.Count; i++)
                    {
                        if (!this.uGrid2_3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(str_))
                        {
                            this.uGrid2_3.Rows[i].Hidden = false;
                        }
                    }
                    SetParaSeq(2);
                }
                else if (intReqLotSeq == 3)
                {
                    //uGrid3_3
                    if (this.uCheckCHASE3_1.Checked || this.uCheckCHASE3_2.Checked || this.uCheckCHASE3_3.Checked || this.uCheckCHASE3_4.Checked || this.uCheckCHASE3_5.Checked || this.uCheckCHASE3_6.Checked || this.uCheckCHASE3_All.Checked)
                    {
                        return;
                    }
                    for (int i = 0; i < this.uGrid3_3.Rows.Count; i++)
                    {
                        if (!this.uGrid3_3.Rows[i].Cells["CCSparameterCode"].Value.ToString().Contains(str_))
                        {
                            this.uGrid3_3.Rows[i].Hidden = false;
                        }
                    }
                    SetParaSeq(3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetParaSeq(int intLotReqSeq)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();

                if (intLotReqSeq == 1)
                    uGrid = this.uGrid1_3;
                else if (intLotReqSeq == 2)
                    uGrid = this.uGrid2_3;
                else if (intLotReqSeq == 3)
                    uGrid = this.uGrid3_3;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Hidden == false)
                    {
                        uGrid.Rows[i].Cells["ParaSeq"].Value = uGrid.Rows[i].RowSelectorNumber;
                    }
                    else
                        uGrid.Rows[i].Cells["ParaSeq"].Value = i + 100;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// CHASE 체크박스확인
        /// </summary>
        /// <param name="intReqLotSeq">Lot의뢰차수</param>
        /// <returns>Check된 CHASE정보</returns>
        private ArrayList CheckCHASE(int intReqLotSeq)
        {
            System.Collections.ArrayList dinosaurs = new System.Collections.ArrayList();
            string strContains = "_/";
            if (intReqLotSeq == 1)
            {
                // Check Box Name
                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE1_1, this.uCheckCHASE1_2, this.uCheckCHASE1_3, 
                                                                             this.uCheckCHASE1_4, this.uCheckCHASE1_5, this.uCheckCHASE1_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    // Check Box Name Add
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }
            }


            else if (intReqLotSeq == 2)
            {
                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE2_1, this.uCheckCHASE2_2, this.uCheckCHASE2_3, 
                                                                             this.uCheckCHASE2_4, this.uCheckCHASE2_5, this.uCheckCHASE2_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }


            }

            else if (intReqLotSeq == 3)
            {

                Infragistics.Win.UltraWinEditors.UltraCheckEditor[] _chk = { this.uCheckCHASE3_1, this.uCheckCHASE3_2, this.uCheckCHASE3_3, 
                                                                             this.uCheckCHASE3_4, this.uCheckCHASE3_5, this.uCheckCHASE3_6 };

                foreach (Infragistics.Win.UltraWinEditors.UltraCheckEditor _Cb in _chk)
                {
                    if (_Cb.Checked)
                        dinosaurs.Add(string.Format("{0}{1}", strContains, _Cb.Name.Substring(_Cb.Name.Length - 1)));
                }


            }

            return dinosaurs;

        }

        /// <summary>
        /// CCS가동조건 정렬
        /// </summary>
        /// <param name="intLotSeq">Lot의뢰차수</param>
        private void ArryCCSPara(int intLotSeq)
        {
            Infragistics.Win.UltraWinGrid.UltraGrid uGridLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
            Infragistics.Win.UltraWinGrid.UltraGrid uGridLotSeq = new Infragistics.Win.UltraWinGrid.UltraGrid();

            // Lot의뢰 차수에 따라서 Gird저장
            if (intLotSeq == 1)
            {
                uGridLot = this.uGrid1_1;
                uGridLotSeq = this.uGrid1_3;
            }
            else if (intLotSeq == 2)
            {
                uGridLot = this.uGrid2_1;
                uGridLotSeq = this.uGrid2_3;
            }
            else if (intLotSeq == 3)
            {
                uGridLot = this.uGrid3_1;
                uGridLotSeq = this.uGrid3_3;
            }
            else
                return;

            ArrayList arrChase = CheckCHASE(intLotSeq);


            // Grid Row Loop
            for (int i = 0; i < uGridLot.Rows.Count; i++)
            {
                // Hidden Row PASS
                if (uGridLot.Rows[i].Hidden)
                    continue;

                // 검사항목 데이터 타입이 "선택" 항목이 아니면 PASS
                if (!uGridLot.Rows[i].GetCellValue("DataType").ToString().Equals("5"))
                    continue;

                // Start Cell Index
                int intStart = uGridLot.Rows[i].Cells["1"].Column.Index;

                // Last Cell Index
                int intSampleSize = ReturnIntegerValue(uGridLot.Rows[i].Cells["ProductItemSS"].Value)
                                    * ReturnIntegerValue(uGridLot.Rows[i].Cells["SampleSize"].Value);

                // Last Cell Index > 99 -> 99
                if (intSampleSize > 99)
                    intSampleSize = 99;

                int intLastIndex = intSampleSize + intStart;

                // Start Index ~ Last Index Loop
                for (int x = intStart; x < intLastIndex; x++)
                {
                    // 선택된 항목이 없는경우 PASS
                    if (uGridLot.Rows[i].Cells[x].Value.ToString().Trim().Equals(string.Empty))
                        continue;

                    // 선택한 BOM 자재코드 저장
                    m_strBOMCode.Add(uGridLot.Rows[i].Cells[x].Value.ToString());

                }
            }

            if (arrChase.Count == 0)
            {
                for (int i = 0; i < uGridLotSeq.Rows.Count; i++)
                {

                    // MaterialSpecName 공백인경우 PASS
                    if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(string.Empty))
                        continue;

                    // BOM MaterialCode 선택되지 않았다면 숨겨있던 Row표시(왠만하면 적용안됌)
                    if (m_strBOMCode.Count == 0)
                    {
                        uGridLotSeq.Rows[i].Hidden = false;
                        continue;
                    }

                    bool _bol = true;

                    for (int r = 0; r < m_strBOMCode.Count; r++)
                    {
                        if (!_bol)
                            break;

                        if (arrChase.Count > 0)
                        {
                            for (int intChese = 0; intChese < arrChase.Count; intChese++)
                            {

                                string strTemp = arrChase[intChese].ToString();

                                if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r])
                                    && uGridLotSeq.Rows[i].GetCellValue("CCSparameterCode").ToString().Contains(strTemp))
                                {
                                    _bol = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (uGridLotSeq.Rows[i].GetCellValue("MaterialSpecName").ToString().Equals(m_strBOMCode[r]))
                            {
                                _bol = false;
                                break;
                            }
                        }

                        uGridLotSeq.Rows[i].Hidden = _bol;
                    }

                }

                SetParaSeq(intLotSeq);

            }
            else
            {

                string strContains = "_/";

                if (arrChase.Count > 0)
                {
                    for (int j = 0; j < uGridLotSeq.Rows.Count; j++)
                    {
                        if (!uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strContains))
                        {
                            uGridLotSeq.Rows[j].Hidden = true;
                            continue;
                        }

                        bool _bol = true;
                        string strTemp = string.Empty;

                        for (int i = 0; i < arrChase.Count; i++) // Check1_1, Check1_2
                        {

                            if (!_bol)
                                break;

                            strTemp = arrChase[i].ToString();

                            // MaterialCode
                            if (m_strBOMCode != null 
                                && m_strBOMCode.Count > 0
                                && !uGridLotSeq.Rows[j].Cells["MaterialSpecName"].Value.ToString().Trim().Equals(string.Empty))
                            {
                                if (uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp)) // _/1 ? _1
                                {
                                    // TEMPERATURE -> _/1, _/2
                                    for (int intTmep = 0; intTmep < m_strBOMCode.Count; intTmep++)
                                    {
                                        if (uGridLotSeq.Rows[j].Cells["MaterialSpecName"].Value.ToString().Equals(m_strBOMCode[intTmep]))
                                        {
                                            _bol = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else // Recipe
                            {
                                if (uGridLotSeq.Rows[j].Cells["CCSparameterCode"].Value.ToString().Contains(strTemp))
                                    _bol = false;
                            }
                        }

                        uGridLotSeq.Rows[j].Hidden = _bol;

                    }

                    SetParaSeq(intLotSeq);
                }
            }

        }

        //private void FileUpload_Header(string ReqNo, int intTabIndex)  
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");  
        //        // 첨부파일 Upload하기
        //        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
        //        ArrayList arrFile = new ArrayList();

        //        if (intTabIndex == 0 && this.uTextFile1.Text.Contains(":\\"))
        //        {
        //            //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
        //            FileInfo fileDoc = new FileInfo(this.uTextFile1.Text);
        //            string strUploadFile = fileDoc.DirectoryName + "\\" +
        //                                   m_strPlantCode + "-" + ReqNo + "-First-CCS-" + fileDoc.Name;
        //            //변경한 화일이 있으면 삭제하기
        //            if (File.Exists(strUploadFile))
        //                File.Delete(strUploadFile);
        //            //변경한 화일이름으로 복사하기
        //            File.Copy(this.uTextFile1.Text, strUploadFile);
        //            arrFile.Add(strUploadFile);
        //        }

        //        if (intTabIndex == 1 && this.uTextFile2.Text.Contains(":\\"))
        //        {
        //            //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
        //            FileInfo fileDoc = new FileInfo(this.uTextFile2.Text);
        //            string strUploadFile = fileDoc.DirectoryName + "\\" +
        //                                   m_strPlantCode + "-" + ReqNo + "-Second-CCS-" + fileDoc.Name;
        //            //변경한 화일이 있으면 삭제하기
        //            if (File.Exists(strUploadFile))
        //                File.Delete(strUploadFile);
        //            //변경한 화일이름으로 복사하기
        //            File.Copy(this.uTextFile2.Text, strUploadFile);
        //            arrFile.Add(strUploadFile);
        //        }

        //        if (intTabIndex == 2 && this.uTextFile3.Text.Contains(":\\"))
        //        {
        //            //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
        //            FileInfo fileDoc = new FileInfo(this.uTextFile3.Text);
        //            string strUploadFile = fileDoc.DirectoryName + "\\" +
        //                                   m_strPlantCode + "-" + ReqNo + "-Third-CCS-" + fileDoc.Name;
        //            //변경한 화일이 있으면 삭제하기
        //            if (File.Exists(strUploadFile))
        //                File.Delete(strUploadFile);
        //            //변경한 화일이름으로 복사하기
        //            File.Copy(this.uTextFile3.Text, strUploadFile);
        //            arrFile.Add(strUploadFile);
        //        }

        //        if (arrFile.Count > 0)
        //        {
        //            // 화일서버 연결정보 가져오기
        //            QRPBrowser brwChannel = new QRPBrowser();
        //            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
        //            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
        //            brwChannel.mfCredentials(clsSysAccess);
        //            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

        //            // 첨부파일 저장경로정보 가져오기
        //            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
        //            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
        //            brwChannel.mfCredentials(clsSysFilePath);
        //            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0041");

        //            //Upload정보 설정
        //            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
        //                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
        //                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
        //                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
        //                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
        //            fileAtt.ShowDialog();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}

        private void FileUpload_Ftp_ErrorImage(string ReqNo, int intTabIndex)
        {
         try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");  
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();
                string strUploadFile="";

                if (intTabIndex == 0 && this.uTextErrorImage1.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextErrorImage1.Text);
                     strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + ReqNo + "-First-CCS-ErrorImage-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextErrorImage1.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                else if (intTabIndex == 1 && this.uTextErrorImage2.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextErrorImage2.Text);
                    strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + ReqNo + "-Second-CCS-ErrorImage-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextErrorImage2.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                else if (intTabIndex == 2 && this.uTextErrorImage3.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextErrorImage3.Text);
                    strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + ReqNo + "-Third-CCS-ErrorImage-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextErrorImage3.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                if (arrFile.Count > 0)
                {
                     string strIp = "";
                        string serverPath = m_resSys.GetString("SYS_SERVERPATH");
                        if (serverPath == "http://10.61.61.71/QRP_PSTS_DEV/")
                        {
                             strIp = "10.61.61.71";
                        }

                        if (serverPath == "http://10.61.61.73/QRP_PSTS_DEV/")
                        {
                            strIp = "10.61.61.73";
                        }

                        string strFtpPath = "ftp://" + strIp + "/" + "UploadFile/CCSErrorImage" + "/";
                        string strUserId = "qrp";
                        string strPwd = "logic12!@";

                        FileUpload_Ftp(strUserId, strPwd, strUploadFile, strFtpPath);

                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void FileUpload_Ftp_FailFile(string ReqNo, int intTabIndex) 
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();
                string strUploadFile = "";

                if (intTabIndex == 0 && this.uTextFile1.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextFile1.Text);
                    strUploadFile = fileDoc.DirectoryName + "\\" +
                                          m_strPlantCode + "-" + ReqNo + "-First-CCS-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextFile1.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                else if (intTabIndex == 1 && this.uTextFile2.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextFile2.Text);
                    strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + ReqNo + "-Second-CCS-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextFile2.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                else if (intTabIndex == 2 && this.uTextFile3.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextFile3.Text);
                    strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + ReqNo + "-Third-CCS-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextFile3.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                if (arrFile.Count > 0)
                {
                    string strIp = "";
                    string serverPath = m_resSys.GetString("SYS_SERVERPATH");
                    if (serverPath == "http://10.61.61.71/QRP_PSTS_DEV/")
                    {
                        strIp = "10.61.61.71";
                    }

                    if (serverPath == "http://10.61.61.73/QRP_PSTS_DEV/")
                    {
                        strIp = "10.61.61.73";
                    }

                    string strFtpPath = "ftp://" + strIp + "/" + "UploadFile/CCSFailFile" + "/";
                    string strUserId = "qrp";
                    string strPwd = "logic12!@";

                    FileUpload_Ftp(strUserId, strPwd, strUploadFile, strFtpPath);

                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }  

        //ftp的方式上传文件（速度比较快）
        public void FileUpload_Ftp(string userId, string pwd, string filename, string ftpPath)
        {
            FileInfo fileInf = new FileInfo(filename);
            FtpWebRequest reqFTP;
            // 根据uri创建FtpWebRequest对象   
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpPath + fileInf.Name));
            // ftp用户名和密码  
            reqFTP.Credentials = new NetworkCredential(userId, pwd);

            reqFTP.UsePassive = false;
            // 默认为true，连接不会被关闭  
            // 在一个命令之后被执行  
            reqFTP.KeepAlive = false;
            // 指定执行什么命令  
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
            // 指定数据传输类型  
            reqFTP.UseBinary = true;
            // 上传文件时通知服务器文件的大小  
            reqFTP.ContentLength = fileInf.Length;
            // 缓冲大小设置为2kb  
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;
            // 打开一个文件流 (System.IO.FileStream) 去读上传的文件  
            FileStream fs = fileInf.OpenRead();
            try
            {
                // 把上传的文件写入流  
                Stream strm = reqFTP.GetRequestStream();
                // 每次读文件流的2kb  
                contentLen = fs.Read(buff, 0, buffLength);
                // 流内容没有结束  
                while (contentLen != 0)
                {
                    // 把内容从file stream 写入 upload stream  
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }
                // 关闭两个流  
                strm.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }

        #endregion


        private void uTextFile1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFile1.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0041");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextFile1.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile1.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextFile2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFile2.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0041");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextFile2.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile2.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextFile3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFile3.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0041");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextFile3.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile3.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextErrorImage1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextErrorImage1.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0043");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextErrorImage1.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextErrorImage1.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextErrorImage2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextErrorImage2.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0043");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextErrorImage2.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextErrorImage2.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextErrorImage3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String m_strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextErrorImage3.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0043");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextErrorImage3.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextErrorImage3.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



    }
}
