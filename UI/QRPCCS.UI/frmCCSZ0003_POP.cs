﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : CCS관리                                               */
/* 프로그램ID   : frmCCSZ0003_POP.cs                                    */
/* 프로그램명   : CCS 의뢰 불량유형POP                                  */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-08-31                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPCCS.UI
{
    public partial class frmCCSZ0003_POP : Form
    {

        #region 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //private DataTable m_dtSaveItem; //불량유형정보

        private String m_strPlantCode; // 공장

        private string m_strProcCode; //공정코드

        private String m_strFullReqNo; //관리번호

        private String m_strReqItemSeq; //검사항목순번

        private String m_strReqLotSeq; // Lot순번 (1차-1, 2차-2, 3차-3)

        private bool m_bolCheck; //작성완료여부

        private decimal m_dbFaultQty;   // 불량수량

        //public DataTable dtSaveItem
        //{
        //    get { return m_dtSaveItem; }
        //    set { m_dtSaveItem = value; }
        //}

        public String strPlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string strProcCode
        {
            get { return m_strProcCode; }
            set { m_strProcCode = value; }
        }

        public String strFullReqNo
        {
            get { return m_strFullReqNo; }
            set { m_strFullReqNo = value; }
        }

        public String strReqLotSeq
        {
            get { return m_strReqLotSeq; }
            set { m_strReqLotSeq = value; }
        }

        public String strReqItemSeq
        {
            get { return m_strReqItemSeq; }
            set { m_strReqItemSeq = value; }
        }

        public bool bolCheck
        {
            get { return m_bolCheck; }
            set { m_bolCheck = value; }
        }

        public decimal dbFaultQty
        {
            get { return m_dbFaultQty; }
            set { m_dbFaultQty = value; }
        }
        #endregion

        public frmCCSZ0003_POP()
        {
            InitializeComponent();
        }

        private void frmCCSZ0001_S_POP_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmCCSZ0001_S_POP_Load(object sender, EventArgs e)
        {
            InitGrid();
            InitButton();
            InitGroupBox();

            //Null값 Error방지
            if (strProcCode == null)
                strProcCode = "";

            ////선택된 검사항목에 대한 불량유형정보가 있을 경우 해당 정보를 보여줌
            //if (m_dtSaveItem != null 
            //    && m_dtSaveItem.Rows.Count > 0
            //    && m_dtSaveItem.Select("ReqLotSeq ='" + strReqLotSeq + "' AND ReqItemSeq = '"+strReqItemSeq+"' ").Count() > 0)
            //{
            //    this.uGridFaulty.DataSource = m_dtSaveItem.Select("ReqLotSeq ='" + strReqLotSeq + "' AND ReqItemSeq = '" + strReqItemSeq + "' ").CopyToDataTable();
            //    this.uGridFaulty.DataBind();
            //}
            //else 
            if (strPlantCode != null 
            && !strPlantCode.Equals(string.Empty)
            && strFullReqNo != null
            && !strFullReqNo.Equals(string.Empty))
            InitData();

            //조회된 정보에 대하여 삭제가능, 수정불가처리
            //ActivationCells();

            //다국어설정
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfSetFormLanguage(this);
        }
         
        #region InitControl
          
        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFaulty, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "ReqSeq", "관리번호순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "ReqItemType", "의뢰유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, true, 1
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "FaultSeq", "불량유형순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 0
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "InspectFaultTypeCode", "불량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 불량유형 콤보설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);
                
                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, strProcCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridFaulty, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtFaultType);

                wGrid.mfAddRowGrid(this.uGridFaulty, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Button초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //해당버튼의 속성을 지정해준다.
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                btn.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonSave, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// GroupBox초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox gbox = new WinGroupBox();

                //GroupBox의 속성을 설정
                gbox.mfSetGroupBox(this.uGroupBoxFaultInfo, GroupBoxType.INFO, "불량정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                                     Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                                      Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                //Font 설정
                this.uGroupBoxFaultInfo.Appearance.FontData.SizeInPoints = 9;
                this.uGroupBoxFaultInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                    
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 초기데이터 조회
        /// </summary>
        private void InitData()
        {
            try
            {
                //BL 호출
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser(); // QRPBrowser Interface 생성
                // 함수호출을 통해 App.Server와 연결
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty), "CCSInspectReqItemFaulty");
                // BL객체 Interface 생성
                QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty clsFaulty = new QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty();
                // 생성한 객체에 권한 설정
                brwChannel.mfCredentials(clsFaulty);

                //받아온 관리번호+개정번호를 정보검색을 위해 분리한다.
                string strReqNo = "";
                string strReqSeq = "";
                if (m_strFullReqNo !=null && !m_strFullReqNo.Equals(string.Empty))
                {
                    strReqNo = m_strFullReqNo.Substring(0, 8);
                    strReqSeq = m_strFullReqNo.Substring(8, 4);
                }
                //불량유형정보 조회 함수호출
                DataTable dtLot = clsFaulty.mfReadInspectReqItemFaulty(strPlantCode, strReqNo, strReqSeq, strReqLotSeq, strReqItemSeq, "Q", m_resSys.GetString("SYS_LANG"));
                
                //조회된정보가 존재할경우 Grid Display
                if (dtLot.Rows.Count > 0)
                {
                    this.uGridFaulty.DataSource = dtLot;
                    this.uGridFaulty.DataBind();
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridFaulty, 0);
                    //ActivationCells();
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Event

        /// <summary>
        /// 닫기버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 저장버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                //작성여부 체크확인
                if (!bolCheck)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000984", "M000986"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }


                this.uGridFaulty.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);

                // 불량유형 콤보설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFT);

                DataTable dtFaultType = clsFT.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, strProcCode, m_resSys.GetString("SYS_LANG"));
                clsFT.Dispose();

                decimal dbSubFaulty = 0;

                //불량정보를 저장하기 위한 필수입력 사항을 체크한다.
                for (int i = 0; i < this.uGridFaulty.Rows.Count; i++)
                {
                    //행삭제된 정보는 Pass
                    if (this.uGridFaulty.Rows[i].Hidden)
                        continue;
                    //편집이미지가 없을 경우 Pass
                    if (this.uGridFaulty.Rows[i].RowSelectorAppearance.Image == null)
                        continue;

                    //불량수량이 입력이 안되어 있을경우 메세지출력
                    if (this.uGridFaulty.Rows[i].GetCellValue("FaultQty").ToString().Equals(string.Empty)
                        || this.uGridFaulty.Rows[i].GetCellValue("FaultQty").ToString().Equals("0"))
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000611", "M001502"
                                                    , Infragistics.Win.HAlign.Right); //불량수량을 입력해주세요
                        return;
                    }

                    // 불량정보가 공백일 경우 메세지 출력
                    // 불량정보가 기준정보에 등록되어 있지않다면 메세지 출력
                    if (this.uGridFaulty.Rows[i].GetCellValue("InspectFaultTypeCode").ToString().Trim().Equals(string.Empty)
                        || dtFaultType.Select("InspectFaultTypeCode = '" + this.uGridFaulty.Rows[i].GetCellValue("InspectFaultTypeCode").ToString() + "'").Count() == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001408"
                                                    , Infragistics.Win.HAlign.Right); //불량유형을 선택해주세요
                        return;
                    }

                    dbSubFaulty = dbSubFaulty + Convert.ToDecimal(this.uGridFaulty.Rows[i].GetCellValue("FaultQty"));
                }

                if (m_dbFaultQty == 0
                    || m_dbFaultQty < dbSubFaulty)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000614", "M000609"
                                                    , Infragistics.Win.HAlign.Right); // 불량수는 Sampling 검사항목의 불량수량 보다 클 수 없습니다
                    return;
                }

                SaveItemInfo();

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 셀업데이트시 편집이미지 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridFault_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //편집이미지 생성
                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                //수정된 행의 정보가 없을 경우 삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridFaulty, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 체크된 Grid의 Row를 Hidden 시킴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridFaulty.Rows.Count; i++)
                {
                    //Hidden Pass
                    if (this.uGridFaulty.Rows[i].Hidden)
                        continue;

                    //선택된 줄을 hidden 시킴
                    if (Convert.ToBoolean(this.uGridFaulty.Rows[i].GetCellValue("Check")))
                        this.uGridFaulty.Rows[i].Hidden = true;

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        #endregion

        #region Method

        /// <summary>
        /// Item 정보 DataTable로 반환하는 Method(행삭제된정보는 기존정보가 있을 경우 삭제)
        /// </summary>
        /// <returns></returns>
        private void SaveItemInfo()
        {
            //m_dtSaveItem = new DataTable();
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            bool bolChk = false;
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                System.Windows.Forms.DialogResult result;
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                this.Cursor = Cursors.WaitCursor;

                //관리번호+순번 정보를 분리
                string strReqNo = "";
                string strReqSeq = "";
                if (m_strFullReqNo != null && !m_strFullReqNo.Equals(string.Empty))
                {
                    strReqNo = m_strFullReqNo.Substring(0, 8); //관리번호
                    strReqSeq = m_strFullReqNo.Substring(8, 4);//순번
                }


                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty), "CCSInspectReqItemFaulty");
                QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty clsFaulty = new QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty();
                brwChannel.mfCredentials(clsFaulty);

                ////기존 불량정보가 없을 경우 컬럼설정을한다.
                //if(m_dtSaveItem == null || m_dtSaveItem.Rows.Count ==0)
                //    m_dtSaveItem = clsFaulty.mfSetDataInfo();

                //저장할 불량유형정보의 컬럼을 설정한다.
                DataTable dtSaveItem = clsFaulty.mfSetDataInfo();

                //삭제할 불량유형정보의 컬럼설정을 한다.
                //DataTable dtDelFaultType = clsFaulty.mfSetDataInfo();

                DataRow drRow;
                int intSeq = 0;

                //그리드의 정보가 있을경우 정보를 담는다.
                if (this.uGridFaulty.Rows.Count > 0)
                {
                    // 현재셀의 위를 맨처음앞으로 이동
                    this.uGridFaulty.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);

                    for (int i = 0; i < uGridFaulty.Rows.Count; i++)
                    {
                        // //편집이미지가 없는경우 Pass
                        //if (this.uGridFaulty.Rows[i].RowSelectorAppearance.Image == null)
                        //    continue;

                        // 기존정보를 확인
                        if (!this.uGridFaulty.Rows[i].GetCellValue("FaultSeq").ToString().Equals("0"))
                            intSeq++;

                        //신규정보를 등록한다.
                        if (this.uGridFaulty.Rows[i].Hidden)
                            continue;

                        //drRow = m_dtSaveItem.NewRow();
                        drRow = dtSaveItem.NewRow();

                        drRow["PlantCode"] = m_strPlantCode == null ? "" : m_strPlantCode; // 공장
                        drRow["ReqNo"] = strReqNo;                                      //관리번호
                        drRow["ReqSeq"] = strReqSeq;                                    //관리번호순번
                        drRow["ReqLotSeq"] = strReqLotSeq;                                  //LotNo 순번
                        drRow["ReqItemSeq"] = strReqItemSeq;                                //검사항목순번
                        drRow["ReqItemType"] = "Q";                                     //"Q" : 품질팀;
                        drRow["FaultSeq"] = this.uGridFaulty.Rows[i].RowSelectorNumber; // 불량유형수번

                        drRow["FaultQty"] = this.uGridFaulty.Rows[i].Cells["FaultQty"].Value; //불량수량
                        drRow["InspectFaultTypeCode"] = this.uGridFaulty.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString(); //불량정보
                        drRow["EtcDesc"] = this.uGridFaulty.Rows[i].Cells["EtcDesc"].Value; //비고

                        //m_dtSaveItem.Rows.Add(drRow);
                        dtSaveItem.Rows.Add(drRow);
                        

                        //신규정보를 행삭제된 경우 Pass
                        //if (this.uGridFaulty.Rows[i].GetCellValue("FaultSeq").ToString().Equals("0"))
                        //    continue;

                        ////행삭제된 정보 (기존정보 : 삭제 , 신규정보 : Pass)
                        //drRow = dtDelFaultType.NewRow();

                        //drRow["PlantCode"] = this.uGridFaulty.Rows[i].GetCellValue("PlantCode"); //공장
                        //drRow["ReqNo"] = this.uGridFaulty.Rows[i].GetCellValue("ReqNo");        //관리번호
                        //drRow["ReqSeq"] = this.uGridFaulty.Rows[i].GetCellValue("ReqSeq");      //순번
                        //drRow["ReqLotSeq"] = this.uGridFaulty.Rows[i].GetCellValue("ReqLotSeq");//Lot순번
                        //drRow["ReqItemSeq"] = this.uGridFaulty.Rows[i].GetCellValue("ReqItemSeq");//검사항목순번
                        //drRow["ReqItemType"] = this.uGridFaulty.Rows[i].GetCellValue("ReqItemType");// P : 의뢰(생산팀) ,Q:의뢰현황(품질팀)
                        //drRow["FaultSeq"] = this.uGridFaulty.Rows[i].GetCellValue("FaultSeq");      //불량순번

                        //dtDelFaultType.Rows.Add(drRow);
                       
                    }
                }

                if (dtSaveItem.Rows.Count == 0 && intSeq == 0)
                {
                    //ProgressPopup Close
                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    return;
                }

                TransErrRtn ErrRtn = new TransErrRtn();
                //불량유형 정보 저장매서드 실행
                string strErrRtn = clsFaulty.mfSaveInspectReqItemFaulty(dtSaveItem, m_strPlantCode == null ? "" : m_strPlantCode,strReqNo,strReqSeq,strReqLotSeq,strReqItemSeq,"Q",
                                                                        m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                //처리결과 DeCoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //ProgressPopup Close
                this.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                bolChk = true;

                if (ErrRtn.ErrNum == 0)
                {
                    //저장처리성공메세지 출력
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);
                    
                }
                else
                {
                    //메세지정보를 담을 변수선언
                    string strMes = "";

                    //삭제 에러 메세지를 변수에 담는다.
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000953", m_resSys.GetString("SYS_LANG"));
                    else
                        strMes = ErrRtn.ErrMessage;


                    //삭제 에러 메세지를 출력한다.
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG")), strMes
                                                , Infragistics.Win.HAlign.Right);
                }

                #region 주석처리 (불량정보를 UI가져갈때 쓰였던것)
                //TransErrRtn ErrRtn = new TransErrRtn();
                ////행삭제정보가 있을 경우 처리
                //if (dtDelFaultType.Rows.Count > 0)
                //{
                //    //불량유형 삭제매서드 실행
                //    string strErrRtn = clsFaulty.mfDeleteInspectReqItemFaulty(dtDelFaultType);
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    //불량 유형 정보삭제 실패시 메세지 출력
                //    if (ErrRtn.ErrNum != 0)
                //    {
                //        //ProgressPopup Close
                //        this.Cursor = Cursors.Default;
                //        m_ProgressPopup.mfCloseProgressPopup(this);
                //        bolChk = true;

                //        //메세지정보를 담을 변수선언
                //        string strMes = "";

                //        //삭제 에러 메세지를 변수에 담는다.
                //        if (ErrRtn.ErrMessage.Equals(string.Empty))
                //            strMes = msg.GetMessge_Text("M000953", m_resSys.GetString("SYS_LANG"));
                //        else
                //            strMes = ErrRtn.ErrMessage;


                //        //삭제 에러 메세지를 출력한다.
                //        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                    , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                //                                    , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG")), strMes
                //                                    , Infragistics.Win.HAlign.Right);

                //    }
                    
                //}
                //if(dtDelFaultType.Rows.Count == 0 || ErrRtn.ErrNum == 0)
                //{

                //    this.Cursor = Cursors.Default;
                //    m_ProgressPopup.mfCloseProgressPopup(this);

                //    bolChk = true;

                //    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                //                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                 "M001135", "M001037", "M000930",
                //                                Infragistics.Win.HAlign.Right);

                //}
                #endregion

                this.Close();
            }
            catch (Exception ex)
            {
                //ProgressPopup창이 열려있을 경우 닫는다.
                if (!bolChk)
                {
                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }

                //ErrorMessage를 보여줌
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드의 해당셀 편집불가처리(기존정보를 수정시 정보가 2개가 됨 : 중복방지)
        /// </summary>
        private void ActivationCells()
        {
            try
            {

                //조회된 정보에 대하여 삭제가능, 수정불가처리
                for (int i = 0; i < this.uGridFaulty.Rows.Count; i++)
                {
                    this.uGridFaulty.Rows[i].Cells["FaultQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly; //불량수량
                    this.uGridFaulty.Rows[i].Cells["InspectFaultTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;//불량정보
                    this.uGridFaulty.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;//비고

                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

    }
}
