﻿namespace QRPCCS.UI
{
    partial class frmPOP0011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmPOP0011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 414);
            this.DeptCode = "";
            this.DeptName = "";
            this.EMail = "";
            this.HpNum = "";
            //this.Name = "frmPOP0011";
            this.PlantCode = "";
            this.PlantName = "";
            this.Position = "";
            this.TelNum = "";
            this.UserID = "";
            this.UserName = "";
            this.ResumeLayout(false);

        }

        #endregion
    }
}