﻿namespace QRPCCS.UI
{
    partial class frmPOP0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmPOP0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 414);
            this.CustomerCode = "";
            this.CustomerName = "";
            this.MaterialGroupCode = "";
            this.MaterialGroupName = "";
            this.MaterialTypeCode = "";
            this.MaterialTypeName = "";
            //this.Name = "frmPOP0002";
            this.PlantCode = "";
            this.PlantName = "";
            this.ProductCode = "";
            //this.Spec = "";
            this.ResumeLayout(false);

        }

        #endregion

    }
}