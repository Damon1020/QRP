﻿namespace QRPDMM.UI
{
    partial class frmDMM0016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0016));
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uText2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uText1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateTime1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraCheckEditor1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTime1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.ultraTextEditor7);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor5);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor11);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor10);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor8);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor4);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor3);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor2);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor1);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.uLabel10);
            this.uGroupBox1.Controls.Add(this.uLabel11);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Controls.Add(this.uComboPlant);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 62);
            this.uGroupBox1.TabIndex = 1;
            // 
            // ultraTextEditor7
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance20;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(740, 100);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor7.TabIndex = 81;
            // 
            // ultraTextEditor6
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Appearance = appearance16;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Location = new System.Drawing.Point(740, 76);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.ReadOnly = true;
            this.ultraTextEditor6.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor6.TabIndex = 81;
            // 
            // ultraTextEditor5
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance4;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(740, 52);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 81;
            // 
            // ultraTextEditor11
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance18;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(128, 100);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 81;
            // 
            // ultraTextEditor10
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Appearance = appearance19;
            this.ultraTextEditor10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Location = new System.Drawing.Point(128, 76);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.ReadOnly = true;
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 81;
            // 
            // ultraTextEditor9
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance10;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(456, 100);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 81;
            // 
            // ultraTextEditor8
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance11;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(456, 76);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor8.TabIndex = 81;
            // 
            // ultraTextEditor4
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance12;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(456, 52);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 81;
            // 
            // ultraTextEditor3
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance13;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(128, 52);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 81;
            // 
            // ultraTextEditor2
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance14;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(740, 28);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 81;
            // 
            // ultraTextEditor1
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor1.Appearance = appearance21;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor1.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor1.Location = new System.Drawing.Point(456, 28);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 81;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(340, 52);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(110, 20);
            this.uLabel5.TabIndex = 64;
            this.uLabel5.Text = "5";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 52);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(110, 20);
            this.uLabel3.TabIndex = 62;
            this.uLabel3.Text = "3";
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(12, 76);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(110, 20);
            this.uLabel4.TabIndex = 63;
            this.uLabel4.Text = "4";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(624, 28);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(110, 20);
            this.uLabel2.TabIndex = 61;
            this.uLabel2.Text = "2";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(624, 52);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(110, 20);
            this.uLabel6.TabIndex = 72;
            this.uLabel6.Text = "6";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(340, 100);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(110, 20);
            this.uLabel8.TabIndex = 80;
            this.uLabel8.Text = "8";
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(624, 100);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(110, 20);
            this.uLabel10.TabIndex = 76;
            this.uLabel10.Text = "10";
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(340, 76);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(110, 20);
            this.uLabel11.TabIndex = 75;
            this.uLabel11.Text = "11";
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(340, 28);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(110, 20);
            this.uLabel1.TabIndex = 60;
            this.uLabel1.Text = "1";
            // 
            // uComboPlant
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance37;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(128, 28);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 39;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(624, 76);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(110, 20);
            this.uLabel9.TabIndex = 74;
            this.uLabel9.Text = "9";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 38;
            this.uLabelPlant.Text = "공장";
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 100);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(110, 20);
            this.uLabel7.TabIndex = 73;
            this.uLabel7.Text = "7";
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(12, 52);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(110, 20);
            this.uLabel14.TabIndex = 79;
            this.uLabel14.Text = "14";
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(340, 28);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(110, 20);
            this.uLabel13.TabIndex = 78;
            this.uLabel13.Text = "13";
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(12, 28);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(110, 20);
            this.uLabel12.TabIndex = 77;
            this.uLabel12.Text = "12";
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(12, 76);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(110, 20);
            this.uLabel16.TabIndex = 82;
            this.uLabel16.Text = "16";
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(340, 104);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(110, 20);
            this.uLabel18.TabIndex = 81;
            this.uLabel18.Text = "18";
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(12, 100);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(110, 20);
            this.uLabel17.TabIndex = 83;
            this.uLabel17.Text = "17";
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(340, 52);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(110, 20);
            this.uLabel15.TabIndex = 84;
            this.uLabel15.Text = "15";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.ultraTextEditor15);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor12);
            this.uGroupBox2.Controls.Add(this.uLabel18);
            this.uGroupBox2.Controls.Add(this.uText2);
            this.uGroupBox2.Controls.Add(this.uLabel17);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor13);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor14);
            this.uGroupBox2.Controls.Add(this.uLabel16);
            this.uGroupBox2.Controls.Add(this.uText1);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor2);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor1);
            this.uGroupBox2.Controls.Add(this.uDateTime1);
            this.uGroupBox2.Controls.Add(this.ultraCheckEditor1);
            this.uGroupBox2.Controls.Add(this.uLabel12);
            this.uGroupBox2.Controls.Add(this.uLabel15);
            this.uGroupBox2.Controls.Add(this.uLabel13);
            this.uGroupBox2.Controls.Add(this.uLabel14);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 176);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1070, 594);
            this.uGroupBox2.TabIndex = 85;
            // 
            // ultraTextEditor15
            // 
            this.ultraTextEditor15.Location = new System.Drawing.Point(456, 104);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.Size = new System.Drawing.Size(380, 21);
            this.ultraTextEditor15.TabIndex = 91;
            // 
            // ultraTextEditor12
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Appearance = appearance15;
            this.ultraTextEditor12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Location = new System.Drawing.Point(232, 76);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.ReadOnly = true;
            this.ultraTextEditor12.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor12.TabIndex = 90;
            // 
            // uText2
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uText2.Appearance = appearance6;
            this.uText2.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance1;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uText2.ButtonsRight.Add(editorButton2);
            this.uText2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uText2.Location = new System.Drawing.Point(128, 76);
            this.uText2.Name = "uText2";
            this.uText2.Size = new System.Drawing.Size(100, 21);
            this.uText2.TabIndex = 89;
            // 
            // ultraTextEditor13
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Appearance = appearance24;
            this.ultraTextEditor13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Location = new System.Drawing.Point(128, 100);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.ReadOnly = true;
            this.ultraTextEditor13.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor13.TabIndex = 88;
            // 
            // ultraTextEditor14
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Appearance = appearance25;
            this.ultraTextEditor14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Location = new System.Drawing.Point(560, 28);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.ReadOnly = true;
            this.ultraTextEditor14.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor14.TabIndex = 88;
            // 
            // uText1
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uText1.Appearance = appearance26;
            this.uText1.BackColor = System.Drawing.Color.PowderBlue;
            appearance27.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance27;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uText1.ButtonsRight.Add(editorButton3);
            this.uText1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uText1.Location = new System.Drawing.Point(456, 28);
            this.uText1.Name = "uText1";
            this.uText1.Size = new System.Drawing.Size(100, 21);
            this.uText1.TabIndex = 87;
            // 
            // ultraDateTimeEditor2
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor2.Appearance = appearance3;
            this.ultraDateTimeEditor2.BackColor = System.Drawing.Color.PowderBlue;
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraDateTimeEditor2.ButtonsRight.Add(spinEditorButton1);
            this.ultraDateTimeEditor2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraDateTimeEditor2.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(456, 52);
            this.ultraDateTimeEditor2.MaskInput = "{LOC}hh:mm:ss tt";
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(120, 21);
            this.ultraDateTimeEditor2.TabIndex = 86;
            // 
            // ultraDateTimeEditor1
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.Appearance = appearance17;
            this.ultraDateTimeEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(128, 52);
            this.ultraDateTimeEditor1.MaskInput = "{LOC}yyyy/mm/dd";
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 86;
            // 
            // uDateTime1
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTime1.Appearance = appearance22;
            this.uDateTime1.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTime1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTime1.Location = new System.Drawing.Point(128, 28);
            this.uDateTime1.Name = "uDateTime1";
            this.uDateTime1.Size = new System.Drawing.Size(100, 21);
            this.uDateTime1.TabIndex = 86;
            // 
            // ultraCheckEditor1
            // 
            this.ultraCheckEditor1.Location = new System.Drawing.Point(580, 52);
            this.ultraCheckEditor1.Name = "ultraCheckEditor1";
            this.ultraCheckEditor1.Size = new System.Drawing.Size(76, 20);
            this.ultraCheckEditor1.TabIndex = 85;
            this.ultraCheckEditor1.Text = "긴급처리";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMM0016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0016";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmDMM0016_Load);
            this.Activated += new System.EventHandler(this.frmDMM0016_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTime1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTime1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;

    }
}