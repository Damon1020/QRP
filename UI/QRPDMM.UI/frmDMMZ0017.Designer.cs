﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0017));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboStockTakeMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStockTakeMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uComboStockTakeYear = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStockTakeYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboDurableInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelDurableInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextStockTakeConfirmDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStockTakeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStockTakeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStockTakeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStockTakeEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStockTakeEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateStockTakeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelStockTakeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStockTakeChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStockTakeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStockTakeMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStockTakeMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStockTakeYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStockTakeYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboDurableInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDurableInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboStockTakeMonth
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeMonth.Appearance = appearance2;
            this.uComboStockTakeMonth.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeMonth.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboStockTakeMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboStockTakeMonth.Location = new System.Drawing.Point(916, 10);
            this.uComboStockTakeMonth.Name = "uComboStockTakeMonth";
            this.uComboStockTakeMonth.Size = new System.Drawing.Size(120, 19);
            this.uComboStockTakeMonth.TabIndex = 15;
            this.uComboStockTakeMonth.Text = "ultraComboEditor2";
            this.uComboStockTakeMonth.AfterCloseUp += new System.EventHandler(this.uComboStockTakeMonth_AfterCloseUp);
            this.uComboStockTakeMonth.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboStockTakeMonth_BeforeDropDown);
            // 
            // uLabelStockTakeMonth
            // 
            this.uLabelStockTakeMonth.Location = new System.Drawing.Point(812, 10);
            this.uLabelStockTakeMonth.Name = "uLabelStockTakeMonth";
            this.uLabelStockTakeMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeMonth.TabIndex = 14;
            this.uLabelStockTakeMonth.Text = "ultraLabel2";
            // 
            // uComboStockTakeYear
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeYear.Appearance = appearance3;
            this.uComboStockTakeYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboStockTakeYear.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboStockTakeYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboStockTakeYear.Location = new System.Drawing.Point(676, 10);
            this.uComboStockTakeYear.Name = "uComboStockTakeYear";
            this.uComboStockTakeYear.Size = new System.Drawing.Size(120, 19);
            this.uComboStockTakeYear.TabIndex = 13;
            this.uComboStockTakeYear.Text = "ultraComboEditor2";
            this.uComboStockTakeYear.AfterCloseUp += new System.EventHandler(this.uComboStockTakeYear_AfterCloseUp);
            // 
            // uLabelStockTakeYear
            // 
            this.uLabelStockTakeYear.Location = new System.Drawing.Point(572, 10);
            this.uLabelStockTakeYear.Name = "uLabelStockTakeYear";
            this.uLabelStockTakeYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeYear.TabIndex = 12;
            this.uLabelStockTakeYear.Text = "ultraLabel2";
            // 
            // uComboDurableInventory
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboDurableInventory.Appearance = appearance4;
            this.uComboDurableInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboDurableInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboDurableInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboDurableInventory.Location = new System.Drawing.Point(404, 10);
            this.uComboDurableInventory.Name = "uComboDurableInventory";
            this.uComboDurableInventory.Size = new System.Drawing.Size(120, 19);
            this.uComboDurableInventory.TabIndex = 11;
            this.uComboDurableInventory.Text = "ultraComboEditor2";
            this.uComboDurableInventory.AfterCloseUp += new System.EventHandler(this.uComboDurableInventory_AfterCloseUp);
            // 
            // uLabelDurableInventory
            // 
            this.uLabelDurableInventory.Location = new System.Drawing.Point(300, 10);
            this.uLabelDurableInventory.Name = "uLabelDurableInventory";
            this.uLabelDurableInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelDurableInventory.TabIndex = 10;
            this.uLabelDurableInventory.Text = "ultraLabel2";
            // 
            // uComboPlant
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance5;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 10);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 9;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.AfterCloseUp += new System.EventHandler(this.uComboPlant_AfterCloseUp);
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 10);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 8;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Controls.Add(this.uTextStockTakeConfirmDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeConfirmDate);
            this.uGroupBox.Controls.Add(this.uTextStockTakeCode);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeCode);
            this.uGroupBox.Controls.Add(this.uLabel5);
            this.uGroupBox.Controls.Add(this.uTextStockTakeChargeID);
            this.uGroupBox.Controls.Add(this.uTextStockTakeChargeName);
            this.uGroupBox.Controls.Add(this.uTextStockTakeEtcDesc);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeEtcDesc);
            this.uGroupBox.Controls.Add(this.uDateStockTakeDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeDate);
            this.uGroupBox.Controls.Add(this.uLabelStockTakeChargeID);
            this.uGroupBox.Controls.Add(this.uGrid1);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox.TabIndex = 2;
            // 
            // uTextStockTakeConfirmDate
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeConfirmDate.Appearance = appearance67;
            this.uTextStockTakeConfirmDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeConfirmDate.Location = new System.Drawing.Point(400, 28);
            this.uTextStockTakeConfirmDate.Name = "uTextStockTakeConfirmDate";
            this.uTextStockTakeConfirmDate.ReadOnly = true;
            this.uTextStockTakeConfirmDate.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeConfirmDate.TabIndex = 116;
            // 
            // uLabelStockTakeConfirmDate
            // 
            this.uLabelStockTakeConfirmDate.Location = new System.Drawing.Point(296, 28);
            this.uLabelStockTakeConfirmDate.Name = "uLabelStockTakeConfirmDate";
            this.uLabelStockTakeConfirmDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeConfirmDate.TabIndex = 115;
            this.uLabelStockTakeConfirmDate.Text = "ultraLabel3";
            // 
            // uTextStockTakeCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeCode.Appearance = appearance20;
            this.uTextStockTakeCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeCode.Location = new System.Drawing.Point(116, 28);
            this.uTextStockTakeCode.Name = "uTextStockTakeCode";
            this.uTextStockTakeCode.ReadOnly = true;
            this.uTextStockTakeCode.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeCode.TabIndex = 114;
            // 
            // uLabelStockTakeCode
            // 
            this.uLabelStockTakeCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelStockTakeCode.Name = "uLabelStockTakeCode";
            this.uLabelStockTakeCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeCode.TabIndex = 113;
            this.uLabelStockTakeCode.Text = "ultraLabel3";
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 104);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(100, 20);
            this.uLabel5.TabIndex = 112;
            this.uLabel5.Text = "ultraLabel6";
            // 
            // uTextStockTakeChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStockTakeChargeID.Appearance = appearance15;
            this.uTextStockTakeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStockTakeChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextStockTakeChargeID.ButtonsRight.Add(editorButton1);
            this.uTextStockTakeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStockTakeChargeID.Location = new System.Drawing.Point(400, 52);
            this.uTextStockTakeChargeID.MaxLength = 20;
            this.uTextStockTakeChargeID.Name = "uTextStockTakeChargeID";
            this.uTextStockTakeChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextStockTakeChargeID.TabIndex = 111;
            this.uTextStockTakeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextStockTakeChargeID_KeyDown);
            this.uTextStockTakeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextStockTakeChargeID_EditorButtonClick);
            // 
            // uTextStockTakeChargeName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeChargeName.Appearance = appearance21;
            this.uTextStockTakeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStockTakeChargeName.Location = new System.Drawing.Point(504, 52);
            this.uTextStockTakeChargeName.Name = "uTextStockTakeChargeName";
            this.uTextStockTakeChargeName.ReadOnly = true;
            this.uTextStockTakeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextStockTakeChargeName.TabIndex = 110;
            // 
            // uTextStockTakeEtcDesc
            // 
            this.uTextStockTakeEtcDesc.Location = new System.Drawing.Point(116, 76);
            this.uTextStockTakeEtcDesc.MaxLength = 1000;
            this.uTextStockTakeEtcDesc.Name = "uTextStockTakeEtcDesc";
            this.uTextStockTakeEtcDesc.Size = new System.Drawing.Size(492, 21);
            this.uTextStockTakeEtcDesc.TabIndex = 109;
            // 
            // uLabelStockTakeEtcDesc
            // 
            this.uLabelStockTakeEtcDesc.Location = new System.Drawing.Point(12, 76);
            this.uLabelStockTakeEtcDesc.Name = "uLabelStockTakeEtcDesc";
            this.uLabelStockTakeEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeEtcDesc.TabIndex = 108;
            this.uLabelStockTakeEtcDesc.Text = "ultraLabel6";
            // 
            // uDateStockTakeDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateStockTakeDate.Appearance = appearance16;
            this.uDateStockTakeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateStockTakeDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateStockTakeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateStockTakeDate.Location = new System.Drawing.Point(116, 52);
            this.uDateStockTakeDate.Name = "uDateStockTakeDate";
            this.uDateStockTakeDate.Size = new System.Drawing.Size(100, 19);
            this.uDateStockTakeDate.TabIndex = 107;
            // 
            // uLabelStockTakeDate
            // 
            this.uLabelStockTakeDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelStockTakeDate.Name = "uLabelStockTakeDate";
            this.uLabelStockTakeDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeDate.TabIndex = 105;
            this.uLabelStockTakeDate.Text = "ultraLabel4";
            // 
            // uLabelStockTakeChargeID
            // 
            this.uLabelStockTakeChargeID.Location = new System.Drawing.Point(296, 52);
            this.uLabelStockTakeChargeID.Name = "uLabelStockTakeChargeID";
            this.uLabelStockTakeChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelStockTakeChargeID.TabIndex = 106;
            this.uLabelStockTakeChargeID.Text = "ultraLabel3";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance22;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 104);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1040, 636);
            this.uGrid1.TabIndex = 15;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // frmDMMZ0017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0017";
            this.Load += new System.EventHandler(this.frmDMMZ0017_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0017_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0017_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStockTakeYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStockTakeEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStockTakeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStockTakeMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeMonth;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStockTakeYear;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDurableInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableInventory;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeConfirmDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeCode;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStockTakeEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStockTakeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStockTakeChargeID;
    }
}