﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0014_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0014_S));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateToGRDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFromGRDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxLot = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uNumQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uComboInputPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextRepresentLot = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepresentLot = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInputPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckSerialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDurableMat = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPONumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPONumber = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelGRSPInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGRConfirmName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRConfirmID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRConfirmID = new Infragistics.Win.Misc.UltraLabel();
            this.uDateGRConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelGRConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridDurableGRD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDurableList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLot)).BeginInit();
            this.uGroupBoxLot.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInputPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepresentLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSerialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateGRConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableGRD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateToGRDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToGRDate.Appearance = appearance4;
            this.uDateToGRDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToGRDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToGRDate.Location = new System.Drawing.Point(500, 12);
            this.uDateToGRDate.Name = "uDateToGRDate";
            this.uDateToGRDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToGRDate.TabIndex = 3;
            // 
            // ultraLabel1
            // 
            appearance31.TextHAlignAsString = "Center";
            appearance31.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance31;
            this.ultraLabel1.Location = new System.Drawing.Point(480, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(20, 16);
            this.ultraLabel1.TabIndex = 4;
            this.ultraLabel1.Text = "~";
            // 
            // uDateFromGRDate
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromGRDate.Appearance = appearance6;
            this.uDateFromGRDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromGRDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromGRDate.Location = new System.Drawing.Point(380, 12);
            this.uDateFromGRDate.Name = "uDateFromGRDate";
            this.uDateFromGRDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromGRDate.TabIndex = 2;
            // 
            // uLabelGRDate
            // 
            this.uLabelGRDate.Location = new System.Drawing.Point(276, 12);
            this.uLabelGRDate.Name = "uLabelGRDate";
            this.uLabelGRDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRDate.TabIndex = 2;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 1;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // uGroupBoxLot
            // 
            this.uGroupBoxLot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxLot.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxLot.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxLot.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxLot.Name = "uGroupBoxLot";
            this.uGroupBoxLot.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxLot.TabIndex = 104;
            this.uGroupBoxLot.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxLot_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumQty);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDown);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInputPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRepresentLot);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRepresentLot);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelQty);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInputPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckSerialFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDurableMatName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDurableMatCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDurableMat);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPONumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPONumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInventory);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRSPInventory);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRConfirmName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRConfirmID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRConfirmID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateGRConfirmDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRConfirmDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonOK);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridDurableGRD);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uNumQty
            // 
            appearance42.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumQty.Appearance = appearance42;
            this.uNumQty.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumQty.Location = new System.Drawing.Point(740, 56);
            this.uNumQty.MaxValue = 999;
            this.uNumQty.MinValue = 0;
            this.uNumQty.Name = "uNumQty";
            this.uNumQty.PromptChar = ' ';
            this.uNumQty.Size = new System.Drawing.Size(100, 21);
            this.uNumQty.TabIndex = 133;
            this.uNumQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextQty_KeyPress);
            // 
            // uButtonDown
            // 
            appearance20.FontData.BoldAsString = "True";
            this.uButtonDown.Appearance = appearance20;
            this.uButtonDown.Location = new System.Drawing.Point(108, 60);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(96, 28);
            this.uButtonDown.TabIndex = 132;
            this.uButtonDown.Text = "다운로드";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uComboInputPlant
            // 
            this.uComboInputPlant.Location = new System.Drawing.Point(740, 12);
            this.uComboInputPlant.MaxLength = 50;
            this.uComboInputPlant.Name = "uComboInputPlant";
            this.uComboInputPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboInputPlant.TabIndex = 6;
            this.uComboInputPlant.AfterCloseUp += new System.EventHandler(this.uComboInputPlant_AfterCloseUp);
            this.uComboInputPlant.ValueChanged += new System.EventHandler(this.uComboInputPlant_ValueChanged);
            // 
            // uTextRepresentLot
            // 
            appearance40.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepresentLot.Appearance = appearance40;
            this.uTextRepresentLot.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepresentLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextRepresentLot.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepresentLot.Location = new System.Drawing.Point(740, 34);
            this.uTextRepresentLot.MaxLength = 1000;
            this.uTextRepresentLot.Name = "uTextRepresentLot";
            this.uTextRepresentLot.Size = new System.Drawing.Size(100, 21);
            this.uTextRepresentLot.TabIndex = 8;
            this.uTextRepresentLot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRepresentLot_KeyDown);
            // 
            // uLabelRepresentLot
            // 
            this.uLabelRepresentLot.Location = new System.Drawing.Point(620, 36);
            this.uLabelRepresentLot.Name = "uLabelRepresentLot";
            this.uLabelRepresentLot.Size = new System.Drawing.Size(116, 20);
            this.uLabelRepresentLot.TabIndex = 129;
            // 
            // uLabelQty
            // 
            this.uLabelQty.Location = new System.Drawing.Point(620, 58);
            this.uLabelQty.Name = "uLabelQty";
            this.uLabelQty.Size = new System.Drawing.Size(116, 20);
            this.uLabelQty.TabIndex = 129;
            // 
            // uTextPlantCode
            // 
            this.uTextPlantCode.Location = new System.Drawing.Point(956, 68);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 128;
            // 
            // uTextGRCode
            // 
            this.uTextGRCode.Location = new System.Drawing.Point(920, 16);
            this.uTextGRCode.Name = "uTextGRCode";
            this.uTextGRCode.ReadOnly = true;
            this.uTextGRCode.Size = new System.Drawing.Size(100, 21);
            this.uTextGRCode.TabIndex = 127;
            this.uTextGRCode.Visible = false;
            // 
            // uTextPlantName
            // 
            this.uTextPlantName.Location = new System.Drawing.Point(956, 44);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantName.TabIndex = 127;
            // 
            // uLabelInputPlant
            // 
            this.uLabelInputPlant.Location = new System.Drawing.Point(620, 12);
            this.uLabelInputPlant.Name = "uLabelInputPlant";
            this.uLabelInputPlant.Size = new System.Drawing.Size(116, 20);
            this.uLabelInputPlant.TabIndex = 126;
            // 
            // uCheckSerialFlag
            // 
            this.uCheckSerialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSerialFlag.Location = new System.Drawing.Point(464, 60);
            this.uCheckSerialFlag.Name = "uCheckSerialFlag";
            this.uCheckSerialFlag.Size = new System.Drawing.Size(144, 20);
            this.uCheckSerialFlag.TabIndex = 125;
            this.uCheckSerialFlag.Text = "LotNo적용여부";
            this.uCheckSerialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uTextDurableMatName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance36;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(464, 36);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 124;
            // 
            // uTextDurableMatCode
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDurableMatCode.Appearance = appearance8;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance7.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance7.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance7;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDurableMatCode.ButtonsRight.Add(editorButton1);
            this.uTextDurableMatCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(360, 36);
            this.uTextDurableMatCode.MaxLength = 20;
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatCode.TabIndex = 7;
            this.uTextDurableMatCode.ValueChanged += new System.EventHandler(this.uTextDurableMatCode_ValueChanged);
            this.uTextDurableMatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDurableMatCode_KeyDown);
            this.uTextDurableMatCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDurableMatCode_EditorButtonClick);
            // 
            // uLabelDurableMat
            // 
            this.uLabelDurableMat.Location = new System.Drawing.Point(240, 36);
            this.uLabelDurableMat.Name = "uLabelDurableMat";
            this.uLabelDurableMat.Size = new System.Drawing.Size(116, 20);
            this.uLabelDurableMat.TabIndex = 122;
            // 
            // uTextPONumber
            // 
            this.uTextPONumber.Location = new System.Drawing.Point(128, 34);
            this.uTextPONumber.MaxLength = 40;
            this.uTextPONumber.Name = "uTextPONumber";
            this.uTextPONumber.Size = new System.Drawing.Size(100, 21);
            this.uTextPONumber.TabIndex = 10;
            // 
            // uLabelPONumber
            // 
            this.uLabelPONumber.Location = new System.Drawing.Point(12, 36);
            this.uLabelPONumber.Name = "uLabelPONumber";
            this.uLabelPONumber.Size = new System.Drawing.Size(112, 20);
            this.uLabelPONumber.TabIndex = 120;
            // 
            // uComboInventory
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.Appearance = appearance2;
            this.uComboInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboInventory.Location = new System.Drawing.Point(360, 60);
            this.uComboInventory.MaxLength = 50;
            this.uComboInventory.Name = "uComboInventory";
            this.uComboInventory.Size = new System.Drawing.Size(100, 19);
            this.uComboInventory.TabIndex = 9;
            // 
            // uLabelGRSPInventory
            // 
            this.uLabelGRSPInventory.Location = new System.Drawing.Point(240, 60);
            this.uLabelGRSPInventory.Name = "uLabelGRSPInventory";
            this.uLabelGRSPInventory.Size = new System.Drawing.Size(116, 20);
            this.uLabelGRSPInventory.TabIndex = 118;
            // 
            // uTextGRConfirmName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRConfirmName.Appearance = appearance37;
            this.uTextGRConfirmName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRConfirmName.Location = new System.Drawing.Point(464, 12);
            this.uTextGRConfirmName.Name = "uTextGRConfirmName";
            this.uTextGRConfirmName.ReadOnly = true;
            this.uTextGRConfirmName.Size = new System.Drawing.Size(100, 21);
            this.uTextGRConfirmName.TabIndex = 117;
            // 
            // uTextGRConfirmID
            // 
            appearance38.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextGRConfirmID.Appearance = appearance38;
            this.uTextGRConfirmID.BackColor = System.Drawing.Color.PowderBlue;
            appearance39.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance39.TextHAlignAsString = "Center";
            editorButton2.Appearance = appearance39;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextGRConfirmID.ButtonsRight.Add(editorButton2);
            this.uTextGRConfirmID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextGRConfirmID.Location = new System.Drawing.Point(360, 12);
            this.uTextGRConfirmID.MaxLength = 20;
            this.uTextGRConfirmID.Name = "uTextGRConfirmID";
            this.uTextGRConfirmID.Size = new System.Drawing.Size(100, 21);
            this.uTextGRConfirmID.TabIndex = 5;
            this.uTextGRConfirmID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextGRConfirmID_KeyDown);
            this.uTextGRConfirmID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextGRConfirmID_EditorButtonClick);
            // 
            // uLabelGRConfirmID
            // 
            this.uLabelGRConfirmID.Location = new System.Drawing.Point(240, 12);
            this.uLabelGRConfirmID.Name = "uLabelGRConfirmID";
            this.uLabelGRConfirmID.Size = new System.Drawing.Size(116, 20);
            this.uLabelGRConfirmID.TabIndex = 115;
            // 
            // uDateGRConfirmDate
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateGRConfirmDate.Appearance = appearance5;
            this.uDateGRConfirmDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateGRConfirmDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateGRConfirmDate.Location = new System.Drawing.Point(128, 10);
            this.uDateGRConfirmDate.Name = "uDateGRConfirmDate";
            this.uDateGRConfirmDate.Size = new System.Drawing.Size(100, 21);
            this.uDateGRConfirmDate.TabIndex = 4;
            // 
            // uLabelGRConfirmDate
            // 
            this.uLabelGRConfirmDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelGRConfirmDate.Name = "uLabelGRConfirmDate";
            this.uLabelGRConfirmDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelGRConfirmDate.TabIndex = 113;
            // 
            // uButtonOK
            // 
            appearance3.FontData.BoldAsString = "True";
            this.uButtonOK.Appearance = appearance3;
            this.uButtonOK.Location = new System.Drawing.Point(1024, 8);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(28, 20);
            this.uButtonOK.TabIndex = 112;
            this.uButtonOK.Text = "확인";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonDeleteRow
            // 
            appearance34.FontData.BoldAsString = "True";
            this.uButtonDeleteRow.Appearance = appearance34;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(8, 60);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(96, 28);
            this.uButtonDeleteRow.TabIndex = 111;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridDurableGRD
            // 
            this.uGridDurableGRD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableGRD.DisplayLayout.Appearance = appearance21;
            this.uGridDurableGRD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableGRD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.uGridDurableGRD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableGRD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableGRD.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableGRD.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridDurableGRD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableGRD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableGRD.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridDurableGRD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableGRD.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridDurableGRD.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridDurableGRD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableGRD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableGRD.DisplayLayout.Override.RowAppearance = appearance33;
            this.uGridDurableGRD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableGRD.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridDurableGRD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableGRD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableGRD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableGRD.Location = new System.Drawing.Point(8, 92);
            this.uGridDurableGRD.Name = "uGridDurableGRD";
            this.uGridDurableGRD.Size = new System.Drawing.Size(1046, 595);
            this.uGridDurableGRD.TabIndex = 16;
            this.uGridDurableGRD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableGRD_AfterCellUpdate);
            this.uGridDurableGRD.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableGRD_ClickCellButton);
            this.uGridDurableGRD.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableGRD_DoubleClickCell);
            // 
            // uGridDurableList
            // 
            this.uGridDurableList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableList.DisplayLayout.Appearance = appearance9;
            this.uGridDurableList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.GroupByBox.Appearance = appearance10;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance11;
            this.uGridDurableList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableList.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.uGridDurableList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableList.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGridDurableList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableList.DisplayLayout.Override.CellAppearance = appearance16;
            this.uGridDurableList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableList.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.uGridDurableList.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridDurableList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableList.DisplayLayout.Override.RowAppearance = appearance19;
            this.uGridDurableList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableList.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridDurableList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableList.Location = new System.Drawing.Point(0, 80);
            this.uGridDurableList.Name = "uGridDurableList";
            this.uGridDurableList.Size = new System.Drawing.Size(1070, 760);
            this.uGridDurableList.TabIndex = 103;
            this.uGridDurableList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // frmDMMZ0014_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxLot);
            this.Controls.Add(this.uGridDurableList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0014_S";
            this.Load += new System.EventHandler(this.frmDMMZ0014_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0014_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0014_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMMZ0014_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLot)).EndInit();
            this.uGroupBoxLot.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInputPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepresentLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSerialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRConfirmID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateGRConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableGRD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToGRDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromGRDate;
        private Infragistics.Win.Misc.UltraLabel uLabelGRDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxLot;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableGRD;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPONumber;
        private Infragistics.Win.Misc.UltraLabel uLabelPONumber;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelGRSPInventory;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRConfirmName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRConfirmID;
        private Infragistics.Win.Misc.UltraLabel uLabelGRConfirmID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateGRConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelGRConfirmDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMat;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSerialFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInputPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelQty;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInputPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepresentLot;
        private Infragistics.Win.Misc.UltraLabel uLabelRepresentLot;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumQty;
    }
}