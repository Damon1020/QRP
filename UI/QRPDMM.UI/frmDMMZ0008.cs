﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmDMMZ0008.cs                                        */
/* 프로그램명   : 금형치공구폐기등록                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-23 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using System.Collections;
using System.IO;


namespace QRPDMM.UI
{
    public partial class frmDMMZ0008 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        // 수정여부 전역변수
        string strEdit = "";

        public frmDMMZ0008()
        {
            InitializeComponent();
        }
        private void frmDMMZ0008_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, false, true, true, true, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0008_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitButton();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //타이틀설정
                titleArea.mfSetLabelText("금형치공구폐기등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                btn.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // -- 로그인 사용자 저장 --
                string strUserID = m_resSys.GetString("SYS_USERID");
                string strUserName = m_resSys.GetString("SYS_USERNAME");

                // 사용자 검색 BL 호출 //
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                // 사용자 정보검색 매서드 호출
                DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), strUserID, m_resSys.GetString("SYS_LANG"));


                //기본값설정(로그인유저)
                uTextDiscardChargeID.Text = strUserID;
                uTextDiscardChargeName.Text = strUserName;

                if (dtUser.Rows.Count > 0)
                {
                    uTextDiscardChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDiscardDate, "폐기일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardChargeUser, "폐기담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardReason, "폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFile, "첨부문서", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //첨부문서
                //기본설정
                grd.mfInitGeneralGrid(this.uGridDiscardFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼
                grd.mfSetGridColumn(this.uGridDiscardFile, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, 
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDiscardFile, 0, "Subject", "문서제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, true, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDiscardFile, 0, "FileName", "파일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, true, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridDiscardFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 400, false, false, 1000,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                
                //빈줄추가
                grd.mfAddRowGrid(this.uGridDiscardFile,0);
            
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();


                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "금형치공구정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "금형치공구폐기등록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바관련

        public void mfSearch()
        {
           
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항 확인
                //--------------------- 필 수 입 력 사 항 확 인 ---------------------//

                // -- 공장 미선택 시 메세지 박스를 띄움 --
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;

                }
                //- 금형치공구 미입력시 메세지 박스를 띄운다 //
                else if (this.uTextDurableMatCode.Text.Trim() == "" || this.uTextDurableMatName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "금형치공구코드를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    this.uTextDurableMatCode.Focus();
                    return;
                }
                // 폐기일 미 선택시 메세지 박스를 띄운다 
                else if (this.uDateDiscardDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "페기일을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uDateDiscardDate.DropDown();
                    return;
                }
                else if (this.uTextDiscardChargeID.Text.Trim() == "" || this.uTextDiscardChargeName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "페기담당자를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    this.uTextDiscardChargeID.Focus();
                    return;
                }

                #endregion

                #region 정보저장

                // ---- 금형 치공구 폐기등록 BL 호출 ----//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DurableMatDiscardFile), "DurableMatDiscardFile");
                QRPEQU.BL.EQUCCS.DurableMatDiscardFile clsDurableMatDiscardFile = new QRPEQU.BL.EQUCCS.DurableMatDiscardFile();
                brwChannel.mfCredentials(clsDurableMatDiscardFile);

                // --- 헤더 정보 컬럼 매서드 호출 ---//
                DataTable dtDurableMatDiscard = clsDurableMatDiscardFile.mfDataSetInfoH();

                //-- 첨부파일 정보 컬럼 매서드 호출
                DataTable dtDurableMatDiscardFile = clsDurableMatDiscardFile.mfDatasetInfoD();

                // 공장, 금형치공구 코드 저장 //
                string strPlantcode = this.uComboPlant.Value.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                #region 헤더 저장

                // 금형치공구 정보에 업데이트할 정보 저장 //
                DataRow drDurableMatDiscard;
                drDurableMatDiscard = dtDurableMatDiscard.NewRow();
                drDurableMatDiscard["PlantCode"] = this.uComboPlant.Value.ToString();
                drDurableMatDiscard["DurableMatCode"] = this.uTextDurableMatCode.Text;
                drDurableMatDiscard["DiscardDate"] = this.uDateDiscardDate.Value.ToString();
                drDurableMatDiscard["DiscardChargeID"] = this.uTextDiscardChargeID.Text;
                drDurableMatDiscard["DiscardReason"] = this.uTextDiscardReason.Text;
                dtDurableMatDiscard.Rows.Add(drDurableMatDiscard);

                #endregion

                #region 첨부파일 저장

                // 첨부파일 경로 저장할 변수 //
                ArrayList arrFile = new ArrayList();

                //파일 편집여부 판단.
                string strUplodChk = "";

                // 편집여부 판단.
                string strChk = "";

                // 첨부파일이 1개 이상 등록 되어있으면 저장한다. //
                if (this.uGridDiscardFile.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridDiscardFile.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridDiscardFile.ActiveCell = this.uGridDiscardFile.Rows[0].Cells[0];

                        // 숨겨져 있는 줄이 아닐 경우 저장한다. //
                        if (this.uGridDiscardFile.Rows[i].Hidden == false)
                        {
                            // 줄의 Selector에 이미지가 있으면 수정판단.
                            if (this.uGridDiscardFile.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                strChk = "Image";
                            }

                            #region 필수입력사항
                            string strRowNum = this.uGridDiscardFile.Rows[i].RowSelectorNumber.ToString();
                            // ---------------- 그리드안 필수입력사항 확인 -------------------//
                            if (this.uGridDiscardFile.Rows[i].Cells["Subject"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "확인창", "필수입력사항확인", strRowNum + "번째 열의 문서제목을 입력해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus
                                this.uGridDiscardFile.ActiveCell = this.uGridDiscardFile.Rows[i].Cells["Subject"];
                                this.uGridDiscardFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else if (this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "확인창", "필수입력사항확인", strRowNum + "번째 열의 파일경로를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                this.uGridDiscardFile.ActiveCell = this.uGridDiscardFile.Rows[i].Cells["FileName"];
                                this.uGridDiscardFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            #endregion

                            #region 첨부파일 담기
                            // 첨부파일이 수정되거 나신규일 경우 arryList에 정보를 저장시킨다.//
                            if (this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo FileInfo = new FileInfo(this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString());

                                strUplodChk = FileInfo.Name;

                                //파일 이름을변경하여 복사하한다 //
                                string strFileName = FileInfo.DirectoryName + "\\" + strDurableMatCode + "-" + 
                                                    this.uGridDiscardFile.Rows[i].RowSelectorNumber + "-" + FileInfo.Name;

                                //변경된 파일이 있을경우 삭제한다.
                                if (File.Exists(strFileName))
                                    File.Delete(strFileName);

                                //변경된파일이름으로 복사하기
                                File.Copy(this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString(), strFileName);
                                arrFile.Add(strFileName);

                            #endregion

                                // 첨부파일 정보를 저장 한다 //
                                DataRow drFile;
                                drFile = dtDurableMatDiscardFile.NewRow();
                                drFile["Seq"] = this.uGridDiscardFile.Rows[i].RowSelectorNumber;
                                drFile["Subject"] = this.uGridDiscardFile.Rows[i].Cells["Subject"].Value.ToString();
                                drFile["FileName"] = strDurableMatCode + "-" +this.uGridDiscardFile.Rows[i].RowSelectorNumber+ "-" + FileInfo.Name;
                                drFile["EtcDesc"] = this.uGridDiscardFile.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtDurableMatDiscardFile.Rows.Add(drFile);
                            }
                            else
                            {
                                // 첨부파일이 변동사항이 없으면 
                                DataRow drNotEdit;
                                drNotEdit = dtDurableMatDiscardFile.NewRow();
                                drNotEdit["Seq"] = this.uGridDiscardFile.Rows[i].RowSelectorNumber;
                                drNotEdit["Subject"] = this.uGridDiscardFile.Rows[i].Cells["Subject"].Value.ToString();
                                drNotEdit["FileName"] = this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString();
                                drNotEdit["EtcDesc"] = this.uGridDiscardFile.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtDurableMatDiscardFile.Rows.Add(drNotEdit);
                            }

                        }
                    }
                }

                #endregion

                #endregion

                // ------------- 저장 여부 메세지 박스를 띄운다 ------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 팝업창을 띄운다 //
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                    // 마우스 커서를 바뀐다
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 금형치공구 폐기등록 매서드 호출 //

                    string strErrRtn = clsDurableMatDiscardFile.mfSaveDurableMatDiscardFile(dtDurableMatDiscard, dtDurableMatDiscardFile,
                                                                                            m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    #region  파일 업로드 정보

                    // 파일 연결 정보를 가져온다 //
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSystemAccessInfo);

                    DataTable dtSysAcce = clsSystemAccessInfo.mfReadSystemAccessInfoDetail(strPlantcode, "S02");

                    // 금형치공구 파일 저장경로를 가져온다 //
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);

                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantcode, "D0005");

                    #endregion

                    // 커서 를 기본값으로 바꾼다. //
                    this.MdiParent.Cursor = Cursors.Default;

                    // 팝업창을 닫는다.
                    m_ProgressPopup.mfCloseProgressPopup(this);


                    //처리결과에 따라서 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        // 첨부파일이 신규거나 수정이 되었으면 파일을 업로드시킨다.
                        if (strUplodChk != "")
                        {
                            //Upload정보 설정
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAcce.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAcce.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();


                        }


                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);

                        //자동조회 //
                        DisplayLoad(strPlantcode, strDurableMatCode);
                        // 전역변수에 수정이되었다는 표시초기화
                        if (strEdit != "")
                        { strEdit = ""; }

                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //------------------- 필수 입력 확인 --------------------//
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //공장콤보DropDown
                    this.uComboPlant.DropDown();
                    return;

                }
                else if (this.uTextDurableMatCode.Text.Trim() == "" || this.uTextDurableMatName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "금형치공구를 선택해주세요.", Infragistics.Win.HAlign.Right);

                    // Focus.
                    this.uTextDurableMatCode.Focus();
                    return;
                }

                // --- 삭제 정보저장 ---//
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                // -- 페기 취소 여부 메세지 박스를 띄운다 =-/
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "삭제확인", "선택한 정보를 폐기취소 하시겠습니까?",
                                    Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //   팝업창을 띄운다 
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                    // WaitCursor로 커서를 변경한다 .
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 금형치공구 폐기등록 BL을 호출한다 //
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DurableMatDiscardFile), "DurableMatDiscardFile");
                    QRPEQU.BL.EQUCCS.DurableMatDiscardFile clsDurableMatDiscardFile = new QRPEQU.BL.EQUCCS.DurableMatDiscardFile();
                    brwChannel.mfCredentials(clsDurableMatDiscardFile);

                    // 금형치공구 폐기등록 취소 매서드를 호출한다. 
                    string strErrRtn = clsDurableMatDiscardFile.mfDeleteDurableMatDiscardFile(strPlantCode, strDurableMatCode, 
                                                                                                m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    
                    // 커서를 기본값으로 변경한다 //
                    this.MdiParent.Cursor = Cursors.Default;
                    // 팝업창을 닫는다.
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    // 처리결과에 따라서 메세지박스를 띄운다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "취소처리결과", "선택한 정보를 성공적으로 페기취소 했습니다.",
                                   Infragistics.Win.HAlign.Right);

                        mfCreate();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "처리결과", "취소처리결과", "선택한 정보를 폐기취소하지 못했습니다.",
                                    Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                this.uTextDurableMatCode.Text = "";
                this.uTextDurableMatName.Text = "";
                this.uTextSpec.Text = "";
                this.uTextVendor.Text = "";
                this.uDateDiscardDate.Value = DateTime.Now;
                this.uTextDiscardReason.Text = "";

                InitText();

                if (this.uGridDiscardFile.Rows.Count > 0)
                {
                    //-- 전체 행을 선택 하여 삭제한다 --//
                    this.uGridDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDiscardFile.Rows.All);
                    this.uGridDiscardFile.DeleteSelectedRows(false);
                }
                // 수정여부 초기화
                if (strEdit != "")
                { strEdit = ""; }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfExcel()
        {
            
        }
        public void mfPrint()
        {
           
        }

        #endregion

        #region 이벤트

        #region 수정여부
        //폐기일이 수정되었다
        private void uDateDiscardDate_ValueChanged(object sender, EventArgs e)
        {
            // 전역변수에 수정이되었다는 표시를 한다
            if (strEdit == "")
            { strEdit = "E"; }
        }

        //페기사유가 편집되었다.
        private void uTextDiscardReason_KeyDown(object sender, KeyEventArgs e)
        {
            // 전역변수에 수정이되었다는 표시를 한다
            if (strEdit == "")
            { strEdit = "E"; }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            if (strEdit == "")
            {
                mfCreate();
            }
        }

        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDiscardFile, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);


                // 전역변수에 수정이되었다는 표시를 한다
                if (strEdit == "")
                { strEdit = "E"; }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDiscardFile_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

           
        }

        // 첨부파일을 등록할수있도록 파일경로 윈도우창을 띄운다.//
        private void uGridDiscardFile_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    e.Cell.Value = strImageFile;

                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    //e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        // 체크박스에 체크된 파일이 다운이된다 //
        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridDiscardFile.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridDiscardFile.Rows[i].Hidden == false && this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridDiscardFile.Rows[i].Cells["Check"].Value) == true)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비폐기 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0005");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridDiscardFile.Rows.Count; i++)
                    {
                        if (this.uGridDiscardFile.Rows[i].Hidden == false && this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridDiscardFile.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridDiscardFile.Rows[i].Cells["FileName"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //금형치공구 코드 텍스트박스의 에디터 버튼을 클릭시 금형치공구정보창이뜬다 //
        private void uTextDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();


                // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //공장 콤보DropDown
                    this.uComboPlant.DropDown();
                    return;
                }

                // 공장 저장
                string strPlantCode = this.uComboPlant.Value.ToString();

                frmPOP0007 frmDurableMat = new frmPOP0007();
                frmDurableMat.PlantCode = strPlantCode;
                frmDurableMat.ShowDialog();

                if (frmDurableMat.PlantCode != "" && strPlantCode != frmDurableMat.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "선택사항확인", "등록하시려는 공장정보와 선택하신 공장정보가 다릅니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);

                    return;
                }
                // 금형치공구코드 저장
                string strDurableMatCode = frmDurableMat.DurableMatCode;

                DisplayLoad(strPlantCode, strDurableMatCode);

                // 팝업창에서 선택한 금형치공구의 정보를 각 해당 컨트롤에 삽입한다.
                this.uTextDurableMatCode.Text = frmDurableMat.DurableMatCode;
                this.uTextDurableMatName.Text = frmDurableMat.DurableMatName;
                this.uTextSpec.Text = frmDurableMat.Spec;
                this.uTextVendor.Text = "";

                // 전역변수에 수정 표시를 초기화한다
                if (strEdit != "")
                { strEdit = ""; }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //폐기담당자 텍스트박스의 에디터 버튼을 클릭하면 유저정보창을 띄운다 ..
        private void uTextDiscardChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //-- 공장 이 공백일 경우 메세지박스를 띄운다. --//
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                    //공장 콤보DropDown
                    this.uComboPlant.DropDown();
                    return;
                }
                //-- 공장정보 저장 --//
                string strPlantCode = this.uComboPlant.Value.ToString();

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                //-- 등록하려는 공장과 선택한 정보의 공장이 틀릴경우 메세지박스를띄운다 --
                if (frmUser.PlantCode != "" && strPlantCode != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "등록하시려는 공장정보와 선택하신 정보의 공장정보가 틀립니다 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }

                //--각 해당컨트롤에 아이디 명 을 삽입시킨다.
                this.uTextDiscardChargeID.Text = frmUser.UserID;
                this.uTextDiscardChargeName.Text = frmUser.UserName;

                // 전역변수에 수정이되었다는 표시를 한다
                if (strEdit == "")
                { strEdit = "E"; }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 폐기담당자 텍스트박스에 엔터키를 누르면 담당자명이 자동조회된다.
        private void uTextDiscardChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextDiscardChargeName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //승인자자 공장코드 저장
                    string strDiscardUserID = this.uTextDiscardChargeID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //----- 아이디 공백 확인
                    if (strDiscardUserID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextDiscardChargeID.Focus();
                        return;
                    }
                    //-- 공장 확인
                    else if (strPlantCode == "" && this.uTextDiscardChargeName.Text.Trim() != "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //-- 유저정보 가져오기 
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strDiscardUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        // 이름 텍스트에 유저 이름을 삽입한다.
                        this.uTextDiscardChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "조회결과 확인", "입력하신 ID가 존재하지 않습니다.", Infragistics.Win.HAlign.Right);

                        this.uTextDiscardChargeName.Clear();
                        return;
                    }
                    // 전역변수에 수정이되었다는 표시를 한다
                    if (strEdit == "")
                    { strEdit = "E"; }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 이벤트 ..
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDiscardFile.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDiscardFile.Rows[i].Cells["Check"].Value) == true)
                        this.uGridDiscardFile.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //금형치공구코드를 입력 후 엔터를 누르면 자동조회
        private void uTextDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Delete || e.KeyData == Keys.Back)
                {
                    this.uTextDurableMatName.Text = "";
                    this.uTextSpec.Text = "";
                    this.uTextVendor.Text = "";
                }

                // 엔터키 입력시 자동조회
                if (e.KeyData == Keys.Enter)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //금형치공구코드 저장
                    string strDurableCode = this.uTextDurableMatCode.Text;

                    if (!strDurableCode.Equals(string.Empty))
                    {
                        string strPlantCode = this.uComboPlant.Value.ToString();

                        //공장정보 체크
                        if (strPlantCode.Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "공장정보 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);
                            
                            this.uComboPlant.DropDown();
                            return;
                            
                        }

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                        QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                        brwChannel.mfCredentials(clsDurableMat);

                        DataTable dtDurableMat = clsDurableMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                        if (dtDurableMat.Rows.Count > 0)
                        {
                            this.uTextDurableMatName.Text = dtDurableMat.Rows[0]["DurableMatName"].ToString();
                            this.uTextSpec.Text = dtDurableMat.Rows[0]["Spec"].ToString();

                            DisplayLoad(strPlantCode, strDurableCode);
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "조회결과 확인", "입력하신 금형치공구를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);

                            this.uTextDurableMatName.Clear();
                            this.uTextSpec.Clear();
                            return;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        /// <summary>
        /// 금형치공구폐기정보 자동조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        private void DisplayLoad(string strPlantCode , string strDurableMatCode)
        {
            try
            {
                
                // 금형치공구 폐기 정보 BL호출 //
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.DurableMatDiscardFile), "DurableMatDiscardFile");
                QRPEQU.BL.EQUCCS.DurableMatDiscardFile clsDurableMatDiscardFile = new QRPEQU.BL.EQUCCS.DurableMatDiscardFile();
                brwChannel.mfCredentials(clsDurableMatDiscardFile);

                //금형치공구폐기 조회 매서드 호출 //
                DataTable dtDurableMatDiscard = clsDurableMatDiscardFile.mfReadDurableMatDiscardFile(strPlantCode, strDurableMatCode);

                // 정보가 있을경우 각 컨트롤에 삽입한다.
                if (dtDurableMatDiscard.Rows.Count > 0)
                {
                    // 폐기일 페기담당자 폐기사유 정보를 컨트롤에 넣는다 //
                    this.uDateDiscardDate.Value = dtDurableMatDiscard.Rows[0]["DiscardDate"].ToString();
                    this.uTextDiscardChargeID.Text = dtDurableMatDiscard.Rows[0]["DiscardChargeID"].ToString();
                    this.uTextDiscardChargeName.Text = dtDurableMatDiscard.Rows[0]["DiscardChargeName"].ToString();
                    this.uTextDiscardReason.Text = dtDurableMatDiscard.Rows[0]["DiscardReason"].ToString();

                    // 그리드에 첨부파일 정보 바인드 //
                    this.uGridDiscardFile.DataSource = dtDurableMatDiscard;
                    this.uGridDiscardFile.DataBind();

                    //if(strEdit != "")
                    //{ strEdit = ""; }
                }
                else
                {
                    this.uDateDiscardDate.Value = DateTime.Now;
                    InitText();
                    this.uTextDiscardReason.Text = "";

                    if (this.uGridDiscardFile.Rows.Count > 0)
                    {
                        //-- 전체 행을 선택 하여 삭제한다 --//
                        this.uGridDiscardFile.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDiscardFile.Rows.All);
                        this.uGridDiscardFile.DeleteSelectedRows(false);

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //공장선택전 수정된 내용이 있을 시 메세지 박스
        private void uComboPlant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (strEdit != "")
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    DialogResult DRmsg = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "수정된 정보가 있습니다. 저장하시겠습니까?", Infragistics.Win.HAlign.Right);

                    if (DRmsg == DialogResult.Yes)
                    {
                        mfSave();

                    }
                    else if (DRmsg == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                        
                    }
                    else
                    {
                        mfCreate();
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void frmDMMZ0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }



    }
}
