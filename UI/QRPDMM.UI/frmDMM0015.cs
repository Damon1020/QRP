﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmDMM0015.cs                                         */
/* 프로그램명   : 정비점검결과등록                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-08                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;

namespace QRPDMM.UI
{
    public partial class frmDMM0015 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal Sysres = new QRPGlobal();

        public frmDMM0015()
        {
            InitializeComponent();
        }

        private string strFormName;
        public frmDMM0015(string strPlantCode, string strRepairCode)
        {
            InitializeComponent();
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();

            Display_Detail(strPlantCode, strRepairCode);
            

        }

        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }

        private void frmDMM0015_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0015_Load(object sender, EventArgs e)
        {
            if (strFormName == null)
            {
                //컨트롤초기화
                SetToolAuth();

                InitText();
                InitLabel();
                InitGrid();
                InitGroupBox();
                InitComboBox();

                uGroupBoxContentsArea.Expanded = false;
            }
            else
            {
                Point po = new Point(0, 0);
                this.Location = po;
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("정비점검결과등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값설정(로그인유저)
                uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                //btn.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uTextDurableMatCode.Text = "";
                this.uTextDurableMatName.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextRepairCode.Text = "";
                this.uTextSpec.Text = "";
                this.uTextUsageDocCode.Text = "";
                this.uTextCumUsage.Text = "";
                this.uTextCurUsage.Text = "";


                this.uTextPlantCode.Visible = false;
                this.uTextUsageDocCode.Visible = false;
                this.uTextCumUsage.Visible = false;
                this.uTextCurUsage.Visible = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //lbl.mfSetLabel(this.uLabel1, "정비완료일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //lbl.mfSetLabel(this.uLabel2, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel3, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel4, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel5, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //lbl.mfSetLabel(this.uLabel6, "정비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel7, "정비구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel8, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel9, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel10, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel11, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel12, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel13, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabel14, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel16, "사용일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel17, "사용담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel18, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUserID, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairCode, "정비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlantName, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //기본설정
                //--1
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //컬럼설정
                //--1
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairCode", "정비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunCode", "정비구분코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunName", "정비구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CurUsage", "현재Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CumUsage", "현재Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageJudgeDate", "Shot판정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StandbyDate", "정비대기일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0,
                     Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairDate", "정비착수일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "FinishDate", "정비완료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //--사용SparePart
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //--사용SparePart
                grd.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurSparePartCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurSparePartName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurInputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurSpec", "구성품Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurMaker", "구성품Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgSPInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100,true , false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgSparePartCode", "사용구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgSparePartName", "사용구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgInvQty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgInputQty", "사용량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgSpec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgMaker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CancelFlag", "취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid2, 0, "DataFlag", "데이터여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");     // 이 필드는 데이터가 원래 


                this.uGrid2.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--정비점검 상세정보
                grd.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--3
                //grd.mfSetGridColumn(this.uGrid3, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairPlace", "수정장소", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairPart", "수정부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairName", "정비내용", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMGubunCode", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMGubunName", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMTypeCode", "정비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMTypeName", "정비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid3, 0, "작업조", "작업조", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairChargeID", "정비사ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairChargeName", "정비사명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairTime", "정비시간(분)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 3, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn,nnn,nnn", "0");
            

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGrid2, 0);

                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGrid2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();


                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "정비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.Appearance.FontData.SizeInPoints = 9;
                uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "SparePart사용정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.Appearance.FontData.SizeInPoints = 9;
                uGroupBox2.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "정비상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromUsageJudgeDate = this.uDateFromUsageJudgeDate.Value.ToString();
                string strToUsageJudgeDate = this.uDateToUsageJudgeDate.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;
                string strEquipName = this.uTextSearchEquipName.Text;
                string strDurableMatCode = this.uTextSearchDurableMatCode.Text;
                string strDurableMatName = this.uTextSearchDurableMatName.Text;
                string strUserID = this.uTextUserID.Text;
                string strUserName = this.uTextUserName.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // Call Method
                DataTable dtRepairH = clsRepairH.mfReadRepairH(strPlantCode
                    , strFromUsageJudgeDate
                    , strToUsageJudgeDate
                    , strEquipCode
                    , strEquipName
                    , strDurableMatCode
                    , strDurableMatName
                    , strUserID
                    , strUserName
                    , m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtRepairH;
                uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtRepairH.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intChgInvQty = 0;
                int intChgInputQty = 0;
                int intCurInputQty = 0;

                int intRowCheck = 0;
                string strStockFlag = "F";

                #region 필수입력사항

                // 필수입력사항 확인
                if (this.uTextPlantCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextRepairCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }

                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.uTextEquipCode.Text;

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //  해당설비의 구성품
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtSPBOM = new DataTable();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                DataTable dtStock = new DataTable();

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit
                            && (this.uGrid2.Rows[i].Cells["CancelFlag"].Value).Equals(false) && this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            this.uGrid2.Rows[i].RowSelectorAppearance.Image = null;
                        }

                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            intRowCheck = intRowCheck + 1;

                            int intRow = this.uGrid2.Rows[i].RowSelectorNumber;

                            if (this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000528", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000507", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgSparePartCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString() == "0" ||
                                this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString() == "" )
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000508", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgInputQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000499", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["UnitCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #region 수량체크

                            //교체시 수량체크
                            if (!Convert.ToBoolean(this.uGrid2.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                            {


                                dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString()
                                                                        , this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000310", intRow + "M000457", Infragistics.Win.HAlign.Right);

                                    SearchSPStock(strPlantCode, this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString(), i);
                                    this.uGrid2.Rows[i].Cells["ChgInputQty"].Value = 0;
                                    this.uGrid2.Rows[i].Cells["ChgInvQty"].Value = 0;
                                    this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value = string.Empty;
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgSparePartName"];
                                    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (!this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty))
                                {
                                    dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString()
                                                    , m_resSys.GetString("SYS_LANG"));

                                    if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001264", "M000310", intRow + "M000471", Infragistics.Win.HAlign.Right);

                                        SearchSPBOM(strPlantCode, strEquipCode);
                                        this.uGrid2.Rows[i].Cells["CurInputQty"].Value = 0;
                                        this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value = string.Empty;
                                        this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CurSparePartName"];
                                        this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                }
                            }
                            // 수리 취소 일시 수량을 체크한다.
                            else
                            {
                                if (!this.uGrid2.Rows[i].GetCellValue("CurSparePartCode").ToString().Equals(string.Empty))
                                {
                                    dtStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString()
                                                                            , this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    if (dtStock.Rows.Count == 0 || Convert.ToInt32(dtStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001264", "M000746", intRow + "M000450", Infragistics.Win.HAlign.Right);
                                        this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CancelFlag"];
                                        return;
                                    }
                                }
                                dtSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, strEquipCode, this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString()
                                                                               , m_resSys.GetString("SYS_LANG"));

                                if (dtSPBOM.Rows.Count == 0 || Convert.ToInt32(dtSPBOM.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000746", intRow + "M000450", Infragistics.Win.HAlign.Right);
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CancelFlag"];
                                    return;
                                }
                            }

                            #endregion
                            ////intChgInputQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString());
                            ////intChgInvQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInvQty"].Value.ToString());
                            ////intCurInputQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["CurInputQty"].Value.ToString());

                            ////if (intChgInvQty < intChgInputQty)
                            ////{
                            ////    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                    , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 사용량이 재고량보다 클 수 없습니다. 확인 후 다시 입력해 주세요", Infragistics.Win.HAlign.Center);
                            ////    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgInputQty"];
                            ////    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            ////    return;
                            ////}

                            ////if (intCurInputQty < intChgInputQty)
                            ////{
                            ////    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                    , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 사용량이 구성품가용수량 보다 클 수 없습니다. 확인 후 다시 입력해 주세요", Infragistics.Win.HAlign.Center);
                            ////    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgInputQty"];
                            ////    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            ////    return;
                            ////}
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001237", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                #endregion

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }


                #region 정보 저장
                //BL 데이터셋 호출

                #region 1. DMMRepairUseSP 에 저장 : CancelFlag 상관없이 데이터테이블에 저장
                //-------- 1. DMMRepairUseSP 에 저장 : CancelFlag 상관없이 데이터테이블에 저장 ----------------------------//;
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.DMMRepairUseSP), "DMMRepairUseSP");
                QRPDMM.BL.DMMMGM.DMMRepairUseSP clsDMMRepairUseSP = new QRPDMM.BL.DMMMGM.DMMRepairUseSP();
                brwChannel.mfCredentials(clsDMMRepairUseSP);

                DataTable dtDMMRepairUseSP = clsDMMRepairUseSP.mfSetDatainfo();
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            row = dtDMMRepairUseSP.NewRow();

                            row["PlantCode"] = strPlantCode;
                            row["RepairCode"] = this.uTextRepairCode.Text;
                            row["Seq"] = i + 1;
                            row["CurSparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                            row["CurInputQty"] = this.uGrid2.Rows[i].Cells["CurInputQty"].Value.ToString();
                            row["ChgSPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                            row["ChgSparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                            row["ChgInputQty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                            row["ChgDesc"] = this.uGrid2.Rows[i].Cells["ChgDesc"].Value.ToString();

                            if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")))
                                row["CancelFlag"] = "T";
                            else
                                row["CancelFlag"] = "F";

                            dtDMMRepairUseSP.Rows.Add(row);
                        }
                    }
                }

                #endregion

                #region 2.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 창고재고에서 투입한 SparePart에 대한 정보
                //-------- 2.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 창고재고에서 투입한 SparePart에 대한 정보---------//;
                // 일반 저장시 창고에서 (-) 처리, 취소시 창고에서(+) 처리함.

                DataTable dtSPStock_Minus = clsSPStock.mfSetDatainfo();
                DataTable dtSPStock_Plus = clsSPStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("DataFlag")) == false) // 기존 저장된 데이터가 아닌것
                            {
                                row = dtSPStock_Minus.NewRow();

                                row["PlantCode"] = this.uTextPlantCode.Text;
                                row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                row["SparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                row["Qty"] = "-" + this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                dtSPStock_Minus.Rows.Add(row);
                            }
                            else
                            {
                                if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")) == true) // 취소여부 체크한 것만
                                {
                                    row = dtSPStock_Plus.NewRow();

                                    row["PlantCode"] = this.uTextPlantCode.Text;
                                    row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    row["SparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    row["Qty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStock_Plus.Rows.Add(row);
                                }
                            }
                        }
                    }
                }

                #endregion

                #region 3.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 창고재고에서 투입한 SparePart에 대한 정보
                //-------- 3.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 창고재고에서 투입한 SparePart에 대한 정보-------------//;
                // 일반 저장시 창고에서 (-) 처리, 취소시 창고에서(+) 처리함.
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStockMoveHist), "SPStockMoveHist");
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                brwChannel.mfCredentials(clsSPStockMoveHist);

                DataTable dtSPStockMoveHist_Minus = clsSPStockMoveHist.mfSetDatainfo();
                DataTable dtSPStockMoveHist_Plus = clsSPStockMoveHist.mfSetDatainfo();
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("DataFlag")) == false) // 기존 저장된 데이터가 아닌것
                            {

                                row = dtSPStockMoveHist_Minus.NewRow();

                                row["MoveGubunCode"] = "M03";       //자재출고일 경우는 "M03"
                                row["DocCode"] = this.uTextRepairCode.Text;
                                row["MoveDate"] = this.uDateWriteDate.Value.ToString();
                                row["MoveChargeID"] = this.uTextWriteID.Text;
                                row["PlantCode"] = this.uTextPlantCode.Text;
                                row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                row["EquipCode"] = strEquipCode;
                                row["SparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                row["MoveQty"] = "-" + this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                dtSPStockMoveHist_Minus.Rows.Add(row);
                            }
                            else
                            {
                                if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")) == true) // 취소여부 체크한 것만
                                {
                                    row = dtSPStockMoveHist_Plus.NewRow();

                                    row["MoveGubunCode"] = "M04";       //자재출고일 경우는 "M03"
                                    row["DocCode"] = this.uTextRepairCode.Text;
                                    row["MoveDate"] = this.uDateWriteDate.Value.ToString();
                                    row["MoveChargeID"] = this.uTextWriteID.Text;
                                    row["PlantCode"] = this.uTextPlantCode.Text;
                                    row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    row["EquipCode"] = this.uTextEquipCode.Text;
                                    row["SparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();
                                    row["MoveQty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStockMoveHist_Plus.Rows.Add(row);
                                }
                            }
                        }
                    }
                }

                #endregion

                #region 4.  MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외
                //-------- 4.  MASEquipSPBOM 테이블 : 교체처리 : 취소정보는 제외 ----------------------------------------------//;
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPTransferD), "SPTransferD");
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                brwChannel.mfCredentials(clsSPTransferD);
                DataTable dtEquipSPBOM = clsSPTransferD.mfSetDatainfo();
                DataTable dtEquipSPBOM_Cancel = clsSPTransferD.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("DataFlag")) == false) // 기존 저장된 데이터가 아닌것
                            {
                                row = dtEquipSPBOM.NewRow();
                                row["PlantCode"] = this.uTextPlantCode.Text;
                                row["TransferSPCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString();        //새로운 SP
                                row["TransferQty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                row["ChgEquipCode"] = this.uTextEquipCode.Text;
                                row["ChgSparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();
                                dtEquipSPBOM.Rows.Add(row);
                            }
                            else
                            {
                                if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")) == true) // 취소여부 체크한 것만
                                {
                                    row = dtEquipSPBOM.NewRow();
                                    row["PlantCode"] = this.uTextPlantCode.Text;
                                    row["TransferSPCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();        //새로운 SP
                                    row["TransferQty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();

                                    row["ChgEquipCode"] = this.uTextEquipCode.Text;
                                    row["ChgSparePartCode"] = this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value.ToString(); // 기존에 있는 SP
                                    row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();
                                    dtEquipSPBOM.Rows.Add(row);
                                }
                            }
                        }
                    }
                }

                #endregion

                #region 5.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 설비에 투입되어있는(SPBOM) SparePart에 대한 정보
                //-------- 5.  EQUSPStock(현재 SparePart 재고정보 테이블) 에 저장 : 설비에 투입되어있는(SPBOM) SparePart에 대한 정보-------//
                // 일반 저장시 창고에서 (+) 처리, 취소시 창고에서(-) 처리함.
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString() != "")
                            {
                                if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("DataFlag")) == false) // 기존 저장된 데이터가 아닌것
                                {

                                    row = dtSPStock_Plus.NewRow();

                                    row["PlantCode"] = this.uTextPlantCode.Text;
                                    row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    row["SparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                    row["Qty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();
                                    dtSPStock_Plus.Rows.Add(row);
                                }
                                else
                                    if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")) == true) // 취소여부 체크한 것만
                                    {
                                        row = dtSPStock_Minus.NewRow();

                                        row["PlantCode"] = this.uTextPlantCode.Text;
                                        row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        row["SparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        row["Qty"] = "-" + this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();
                                        dtSPStock_Minus.Rows.Add(row);
                                    }
                            }
                        }
                    }
                }

                #endregion

                #region  6.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 설비에 투입되어있는(SPBOM) SparePart에 대한 정보
                //-------- 6.  EQUSPStockMoveHist(재고이력 테이블) 에 저장 : 설비에 투입되어있는(SPBOM) SparePart에 대한 정보-------//
                // 일반 저장시 창고에서 (+) 처리, 취소시 창고에서(-) 처리함.
                
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString() != "")
                            {
                                if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("DataFlag")) == false) // 기존 저장된 데이터가 아닌것
                                {
                                    row = dtSPStockMoveHist_Plus.NewRow();

                                    row["MoveGubunCode"] = "M04";       //자재반납일 경우는 "M04"
                                    row["DocCode"] = this.uTextRepairCode.Text;
                                    row["MoveDate"] = this.uDateWriteDate.Value.ToString();
                                    row["MoveChargeID"] = this.uTextWriteID.Text;
                                    row["PlantCode"] = this.uTextPlantCode.Text;
                                    row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                    row["EquipCode"] = this.uTextEquipCode.Text;
                                    row["SparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                    row["MoveQty"] = this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtSPStockMoveHist_Plus.Rows.Add(row);
                                }
                                else
                                {
                                    if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")) == true) // 취소여부 체크한 것만
                                    {
                                        row = dtSPStockMoveHist_Minus.NewRow();

                                        row["MoveGubunCode"] = "M03";       //자재반납일 경우는 "M04"
                                        row["DocCode"] = this.uTextRepairCode.Text;
                                        row["MoveDate"] = this.uDateWriteDate.Value.ToString();
                                        row["MoveChargeID"] = this.uTextWriteID.Text;
                                        row["PlantCode"] = this.uTextPlantCode.Text;
                                        row["SPInventoryCode"] = this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value.ToString();
                                        row["EquipCode"] = this.uTextEquipCode.Text;
                                        row["SparePartCode"] = this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString();
                                        row["MoveQty"] = "-" + this.uGrid2.Rows[i].Cells["ChgInputQty"].Value.ToString();
                                        row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                                        dtSPStockMoveHist_Minus.Rows.Add(row);
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion

                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.SAVEMGM), "SAVEMGM");
                QRPDMM.BL.DMMMGM.SAVEMGM clsSAVEMGM = new QRPDMM.BL.DMMMGM.SAVEMGM();
                brwChannel.mfCredentials(clsSAVEMGM);
                string rtMSG = clsSAVEMGM.mfSaveRepairUseSP
                    (dtDMMRepairUseSP
                    , dtSPStock_Minus
                    , dtSPStockMoveHist_Minus
                    , dtEquipSPBOM
                    , dtSPStock_Plus
                    , dtSPStockMoveHist_Plus
                    , m_resSys.GetString("SYS_USERIP")
                    , m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
                InitText();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0 && (this.uGrid2.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)) && (this.uGrid3.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(true)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                if(this.uGrid1.Rows.Count >0)
                    grd.mfDownLoadGridToExcel(this.uGrid1);

                if (this.uGrid2.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid2);

                if (this.uGrid3.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid3);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        #region 이벤트
        //접히거나 닫힐때 일어나는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 175);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 720;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["DataFlag"].Value) == false)
                            
                        {
                            this.uGrid2.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmDMM0015_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 텍스트 이벤트

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0005 frm = new frmPOP0005();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchEquipCode.Text = frm.EquipCode;
                uTextSearchEquipName.Text = frm.EquipName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0007 frm = new frmPOP0007();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchDurableMatCode.Text = frm.DurableMatCode;
                uTextSearchDurableMatName.Text = frm.DurableMatName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextUserID.Text = frm.UserID;
                uTextUserName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uTextPlantCode.Text == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    //this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uTextPlantCode.Text;
                frm.ShowDialog();

                uTextWriteID.Text = frm.UserID;
                uTextWriteName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                String strPlantCode = this.uTextPlantCode.Text;

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextWriteID;
                uTextName = this.uTextWriteName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        //this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strEquipCode = this.uTextSearchEquipCode.Text;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquip.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000902",
                                            Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSearchEquipCode.Text = "";
                    this.uTextSearchEquipName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 치공구 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsMat);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strDurableCode = this.uTextSearchDurableMatCode.Text;

                    DataTable dtDurable = clsMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                    if (dtDurable.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000797",
                                                                    Infragistics.Win.HAlign.Right);
                    }

                    else
                    {
                        this.uTextSearchDurableMatName.Text = dtDurable.Rows[0]["DurableMatName"].ToString();
                    }

                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchDurableMatCode.Text = "";
                    this.uTextSearchDurableMatName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 점검자 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uComboPlant.Value.ToString().Equals(""))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000266",
                                                                    Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsuser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsuser);

                        String strPlantCode = this.uComboPlant.Value.ToString();
                        String strUserID = this.uTextUserID.Text;

                        DataTable dtName = clsuser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                        if (dtName.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000615",
                                                                    Infragistics.Win.HAlign.Right);
                            this.uTextUserID.Focus();
                            return;
                        }
                        else
                        {
                            this.uTextUserName.Text = dtName.Rows[0]["UserName"].ToString();
                        }
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextUserID.Text = "";
                    this.uTextUserName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion

        #region 그리드 이벤트

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Display_Detail(string strPlantCode, string strRepairCode)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }

                QRPBrowser brwChannel = new QRPBrowser();

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                // Call Method
                DataTable dtRepairH = clsRepairH.mfReadRepairH_Detail(strPlantCode, strRepairCode, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {
                    this.uTextPlantCode.Text = dtRepairH.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtRepairH.Rows[i]["PlantName"].ToString();

                    this.uTextDurableMatCode.Text = dtRepairH.Rows[i]["DurableMatCode"].ToString();
                    this.uTextDurableMatName.Text = dtRepairH.Rows[i]["DurableMatName"].ToString();
                    this.uTextEquipCode.Text = dtRepairH.Rows[i]["EquipCode"].ToString();
                    this.uTextEquipName.Text = dtRepairH.Rows[i]["EquipName"].ToString();
                    this.uTextEtcDesc.Text = dtRepairH.Rows[i]["EtcDesc"].ToString();
                    this.uTextPlantCode.Text = dtRepairH.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtRepairH.Rows[i]["PlantName"].ToString();
                    this.uTextRepairCode.Text = dtRepairH.Rows[i]["RepairCode"].ToString();
                    this.uTextSpec.Text = dtRepairH.Rows[i]["Spec"].ToString();
                    this.uTextUsageDocCode.Text = dtRepairH.Rows[i]["UsageDocCode"].ToString();

                    this.uTextCumUsage.Text = dtRepairH.Rows[i]["CumUsage"].ToString();
                    this.uTextCurUsage.Text = dtRepairH.Rows[i]["CurUsage"].ToString();
                }

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                brwChannel.mfCredentials(clsEquipWorker);
                DataTable dtEquipWorker = clsEquipWorker.mfReadEquipWorker_Combo(this.uTextPlantCode.Text, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGrid2, 0, "RepairChargeID", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtEquipWorker);


                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairD), "RepairD");
                QRPDMM.BL.DMMMGM.RepairD clsRepairD = new QRPDMM.BL.DMMMGM.RepairD();
                brwChannel.mfCredentials(clsRepairD);

                // Call Method
                DataTable dtRepairD = clsRepairD.mfReadRepairD(strPlantCode, strRepairCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid3.DataSource = dtRepairD;
                uGrid3.DataBind();

                if (dtRepairD.Rows.Count > 0)
                {
                    grd.mfSetAutoResizeColWidth(this.uGrid3, 0);
                }

                SearchSPBOM(strPlantCode, this.uTextEquipCode.Text);

                // SP 창고정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);
                DataTable dtSPInventory = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGrid2, 0, "ChgSPInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtSPInventory);


                //미리 저장된 UseSP정보
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.DMMRepairUseSP), "DMMRepairUseSP");
                QRPDMM.BL.DMMMGM.DMMRepairUseSP clsDMMRepairUseSP = new QRPDMM.BL.DMMMGM.DMMRepairUseSP();
                brwChannel.mfCredentials(clsDMMRepairUseSP);
                DataTable dtDMMRepairUseSP = clsDMMRepairUseSP.mfReadDMMRepairUseSP(strPlantCode, strRepairCode, m_resSys.GetString("SYS_LANG"));
                //테이터바인드

                
                uGrid2.DataSource = dtDMMRepairUseSP;
                uGrid2.DataBind();
                uGrid2_AfterDataBinding();

                if (dtDMMRepairUseSP.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterDataBinding()
        {
            try
            {
                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")))
                    {
                        uGrid2.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;
                        uGrid2.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    else
                    {
                        for (int j = 0; j < uGrid2.Rows[i].Cells.Count; j++)
                        {

                            if (uGrid2.Rows[i].Cells[j].Column.Key.ToString() == "CancelFlag" )
                            {
                                uGrid2.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                
                            }
                            else
                            {
                                uGrid2.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }


                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strRepairCode = e.Cell.Row.Cells["RepairCode"].Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strRepairCode == "")
                    return;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                Display_Detail(strPlantCode, strRepairCode);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uTextPlantCode.Text;
                string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strSPInventoryCode == "")
                    return;


                if (Convert.ToBoolean(e.Cell.Row.Cells["DataFlag"].Value.ToString()))
                    return;

                frmPOP0008 frm = new frmPOP0008();
                frm.PlantCode = strPlantCode;
                frm.ShowDialog();

                e.Cell.Row.Cells["ChgSparePartCode"].Value = frm.SparePartCode;

                //if (e.Cell.Row.Cells["ChgSparePartCode"].Value != "")
                //{
                //    e.Cell.Row.Cells["ChgSparePartName"].Value = frm.SparePartName;
                //}
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellUpdate_1(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                KeyEventArgs KeyEvent = new KeyEventArgs(Keys.Enter);
                QRPBrowser brwChannel = new QRPBrowser();

                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;

                string strPlantCode = this.uTextPlantCode.Text;
                string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();
                string strChgSparePartCode = e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString();
                string strCurSparePartCode = e.Cell.Row.Cells["CurSparePartCode"].Value.ToString();

                #region 사용량
                if (strColumn == "ChgInputQty" && !e.Cell.Value.ToString().Equals("0"))
                {
                    if (e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Value = 0;
                        return;
                    }

                    if (strCurSparePartCode == "")
                    {
                        int intChgInputQty = Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value.ToString()); //입력수량
                        int intChgInvQty = Convert.ToInt32(e.Cell.Row.Cells["ChgInvQty"].Value.ToString());     //재고수량

                        if (intChgInputQty > intChgInvQty)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000302", "M000875", Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["ChgInputQty"].Value = 0;

                            this.uGrid2.ActiveCell = this.uGrid2.Rows[e.Cell.Row.Index].Cells["ChgInputQty"];
                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        }
                    }
                    else
                    {
                        int intChgInputQty = Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value.ToString()); //입력수량
                        int intChgInvQty = Convert.ToInt32(e.Cell.Row.Cells["ChgInvQty"].Value.ToString());     //재고수량
                        int intCurInputQty = Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Value.ToString()); //구성품수량

                        if (!e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString().Equals(string.Empty))
                        {
                            if (intChgInputQty > intCurInputQty)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000302", "M000875", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;

                                this.uGrid2.ActiveCell = this.uGrid2.Rows[e.Cell.Row.Index].Cells["ChgInputQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }

                            if (intChgInputQty > intChgInvQty)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000302", "M000875", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["ChgInputQty"].Value = 0;

                                this.uGrid2.ActiveCell = this.uGrid2.Rows[e.Cell.Row.Index].Cells["ChgInputQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                    }


                    //현재 입력한교체수량
                    int intChgQty = 0;
                    int intQty = 0;


                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null
                            && this.uGrid2.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            && this.uGrid2.Rows[i].Hidden.Equals(false))
                        {
                            //if (!i.Equals(e.Cell.Row.Index)
                            //    && e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSparePartCode"].Value)
                            //    && e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGridUseSP.Rows[i].Cells["ChgSPInventoryCode"].Value))
                            //{
                            if (e.Cell.Row.Cells["ChgSparePartCode"].Value.Equals(this.uGrid2.Rows[i].Cells["ChgSparePartCode"].Value) &&
                                e.Cell.Row.Cells["ChgSPInventoryCode"].Value.Equals(this.uGrid2.Rows[i].Cells["ChgSPInventoryCode"].Value))
                            {
                                if (intQty.Equals(0))
                                {
                                    intQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInvQty"].Value);
                                }
                                intChgQty = intChgQty + Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInputQty"].Value);


                            }
                        }
                    }

                    if (intChgQty > intQty)
                    {

                        string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M000617", "M000892" + strQty + " M000010", Infragistics.Win.HAlign.Right);

                        //e.Cell.Row.Cells["ChgSparePartCode"].Value = string.Empty;
                        //e.Cell.Row.Cells["ChgSparePartName"].Value = string.Empty;
                        //e.Cell.Row.Cells["Qty"].Value = 0;
                        //e.Cell.Row.Cells["Qty"].Tag = 0;
                        e.Cell.Value = 0;
                        //e.Cell.Row.Cells["UnitCode"].Value = string.Empty;

                        return;
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                KeyEventArgs KeyEvent = new KeyEventArgs(Keys.Enter);

                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;

                string strPlantCode = this.uTextPlantCode.Text;
                //
                //string strChgSparePartCode = e.Cell.Row.Cells["ChgSparePartCode"].Value.ToString();
                //string strCurSparePartCode = e.Cell.Row.Cells["CurSparePartCode"].Value.ToString();



                QRPBrowser brwChannel = new QRPBrowser();

                #region 구성품코드

                if (strColumn == "CurSparePartName")
                {
                    string strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    if (strValue == "")
                        return;

                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);

                    DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMDetail(strPlantCode, uTextEquipCode.Text, strValue, m_resSys.GetString("SYS_LANG"));
                    if (dtEquipSPBOM.Rows.Count > 0)
                    {
                        e.Cell.Row.Cells["CurSparePartCode"].Value = strValue;
                        e.Cell.Row.Cells["CurSparePartCode"].Tag = dtEquipSPBOM.Rows[0]["UnitCode"].ToString();
                        int intInputQty = Convert.ToInt32(dtEquipSPBOM.Rows[0]["InputQty"].ToString());
                        int intChgStandbyQty = Convert.ToInt32(dtEquipSPBOM.Rows[0]["ChgStandbyQty"].ToString());

                        e.Cell.Row.Cells["CurInputQty"].Tag = intInputQty - intChgStandbyQty;
                        e.Cell.Row.Cells["CurInputQty"].Value = intInputQty - intChgStandbyQty;

                        e.Cell.Row.Cells["CurSpec"].Value = dtEquipSPBOM.Rows[0]["Spec"];
                        e.Cell.Row.Cells["CurMaker"].Value = dtEquipSPBOM.Rows[0]["Maker"];

                        //선택한 정보가 그리드내에 있으면 다시입력
                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGrid2.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGrid2.Rows[i].Hidden.Equals(false))
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    this.uGrid2.Rows[i].Cells["CurSparePartCode"].Value.ToString().Equals(strValue))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000311", "이미" + this.uGrid2.Rows[i].RowSelectorNumber + "M000459", Infragistics.Win.HAlign.Right);

                                    //단위저장 
                                    e.Cell.Row.Cells["CurSparePartCode"].Tag = string.Empty;

                                    e.Cell.Row.Cells["CurSparePartCode"].Value = string.Empty;
                                    e.Cell.Row.Cells["CurSparePartName"].Value = string.Empty;

                                    e.Cell.Row.Cells["CurInputQty"].Tag = 0;
                                    e.Cell.Row.Cells["CurInputQty"].Value = 0;

                                    e.Cell.Row.Cells["CurSpec"].Value = string.Empty;
                                    e.Cell.Row.Cells["CurMaker"].Value = string.Empty;

                                    return;
                                }
                            }
                        }
                        if (Convert.ToInt32(e.Cell.Row.Cells["ChgInputQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["CurInputQty"].Value))
                        {
                            e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                        }
                    }

                }
                #endregion

                #region 창고
                if (strColumn == "ChgSPInventoryCode")
                {
                    string strValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    if (strValue == "")
                        return;

                    e.Cell.Row.Cells["ChgSparePartName"].Value = "";
                    e.Cell.Row.Cells["ChgSparePartCode"].Value = "";
                    e.Cell.Row.Cells["ChgInvQty"].Value = 0;
                    //e.Cell.Row.Cells["UnitCode"].Value = "EA";

                    SearchSPStock(strPlantCode, strValue, e.Cell.Row.Index);

                }
                #endregion

                #region 사용구성품

                if (strColumn == "ChgSparePartName")
                {
                    string strSPInventoryCode = e.Cell.Row.Cells["ChgSPInventoryCode"].Value.ToString();
                    string strSparePartCode = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();

                    if (strSparePartCode == "")
                        return;

                    if (strSPInventoryCode == "")
                        return;

                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                    QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                    brwChannel.mfCredentials(clsSPStock);

                    DataTable dtSPStock = clsSPStock.mfReadSPStock_Detail(strPlantCode, strSPInventoryCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));
                    if (dtSPStock.Rows.Count > 0)
                    {
                        if (!e.Cell.Row.Cells["CurSparePartCode"].Value.ToString().Equals(string.Empty)
                            && e.Cell.Row.Cells["CurSparePartCode"].Tag != null
                            && !e.Cell.Row.Cells["CurSparePartCode"].Tag.Equals(dtSPStock.Rows[0]["UnitCode"]))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001135", "선택하신 SparePart단위와 기존SparePart단위가 다릅니다.", Infragistics.Win.HAlign.Right);
                            return;
                        }

                        e.Cell.Row.Cells["ChgSparePartCode"].Value = dtSPStock.Rows[0]["SparePartCode"];
                        e.Cell.Row.Cells["ChgInvQty"].Value = dtSPStock.Rows[0]["Qty"];
                        e.Cell.Row.Cells["ChgInvQty"].Tag = dtSPStock.Rows[0]["Qty"];

                        e.Cell.Row.Cells["UnitCode"].Value = dtSPStock.Rows[0]["UnitCode"];

                        e.Cell.Row.Cells["ChgSpec"].Value = dtSPStock.Rows[0]["Spec"];
                        e.Cell.Row.Cells["ChgMaker"].Value = dtSPStock.Rows[0]["Maker"];

                        if (!e.Cell.Row.Cells["ChgInputQty"].Value.Equals(0))
                        {
                            e.Cell.Row.Cells["ChgInputQty"].Value = 0;
                        }

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 콤보그리드
        /// <summary>
        /// 그리드 SPBOM콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        private void SearchSPBOM(string strPlantCode,string strEquipCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();
                // 설비 구성품 정보 가지고옴
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                brwChannel.mfCredentials(clsEquipSPBOM);
                DataTable dtEquipSPBOM = clsEquipSPBOM.mfReadEquipSTBOMCombo(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                this.uGrid2.DisplayLayout.Bands[0].Columns["CurSparePartName"].Layout.ValueLists.Clear();

                string strValue = "SparePartCode,SparePartName,InputQty,UnitName,Spec,Maker";
                string strText = "구성품코드,구성품명,Qty,단위,Spec,Maker";

                grd.mfSetGridColumnValueGridList(this.uGrid2, 0, "CurSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                , "SparePartCode", "SparePartName", dtEquipSPBOM);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 SPStock콤보그리드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInventoryCode">창고코드</param>
        /// <param name="intIndex">줄번호</param>
        private void SearchSPStock(string strPlantCode, string strInventoryCode,int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //SP현재고 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //공장, 창고코드 선택시 해당되는 SP정보 가져옴
                DataTable dtChgSP = clsSPStock.mfReadSPStock_SPCombo(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                if (dtChgSP.Rows.Count > 0)
                {
                    string strDropDownKey = "SparePartCode,SparePartName,Qty,UnitName,Spec,Maker";
                    string strDropDownName = "사용구성품코드,사용구성품명,재고량,단위,Spec,Maker";

                    WinGrid wGrid = new WinGrid();

                    //GridList 그리드에 삽입
                    wGrid.mfSetGridCellValueGridList(this.uGrid2, 0, intIndex, "ChgSparePartName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownKey, strDropDownName
                                                        , "SparePartCode", "SparePartName", dtChgSP);
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000121", "M001252", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion



        #endregion

    }
}
