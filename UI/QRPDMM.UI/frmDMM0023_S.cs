﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMM0023_S.cs                                         */
/* 프로그램명   : 치공구재고현황                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using System.Collections;

namespace QRPDMM.UI
{
    public partial class frmDMM0023_S : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmDMM0023_S()
        {
            InitializeComponent();
        }

        private void frmDMM0023_Activated(object sender, EventArgs e)
        {
            //활성화된 툴바 사용여부
            QRPBrowser ToolBar = new QRPBrowser();
            ToolBar.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true);
        }

        private void frmDMM0023_Load(object sender, EventArgs e)
        {
            InitTitle();
            InitLabel();
            InitCombo();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }


        #region 컨트롤초기화
        /// <summary>
        /// 타이틀초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("치공구재고현황", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchInventory, "창고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMat, "치공구", m_resSys.GetString("SYS_FONTNAME"), true, false);



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE")
                    , "", "선택", "PlantCode", "PlantName", dtPlant);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();


                //치공구재고현황 기본설정
                grd.mfInitGeneralGrid(this.uGridDurableMatInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                                        false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                                         Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                                          Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //치공구재고현황 컬럼설정
                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "DurableInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "StandName", "대기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "DurableMatName", "치공구 명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridDurableMatInfo, 0, "SafeQty", "안전재고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                // 그리드 편집불가상태로
                this.uGridDurableMatInfo.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridDurableMatInfo.DisplayLayout.Bands[0].Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보 확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);
                    
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                //설비코드,치공구 코드 입력확인
                if (!this.uTextEquipCode.Text.Equals(string.Empty) && this.uTextEquipName.Text.Equals(string.Empty))
                {
                    this.uTextEquipCode.Clear();
                }
                if (!this.uTextSearchDurableMat.Text.Equals(string.Empty) && this.uTextDurableMatName.Text.Equals(string.Empty))
                {
                    this.uTextSearchDurableMat.Clear();
                }

                //검색정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDurableInventory = this.uComboSearchInventory.Value.ToString();
                
                string strEquipCode = "";
                string strDurableMatCode = "";
                
                //설비코드,치공구 코드 입력확인
                if (!this.uTextSearchDurableMat.Text.Equals(string.Empty) && !this.uTextDurableMatName.Text.Equals(string.Empty))
                {
                    strDurableMatCode = this.uTextSearchDurableMat.Text;
                }

                if (!this.uTextEquipCode.Text.Equals(string.Empty) && !this.uTextEquipName.Text.Equals(string.Empty))
                {
                    strEquipCode = this.uTextEquipCode.Text;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //치공구현황정보 매서드실행
                DataTable dtDurableStockState = clsDurableStock.mfReadDurableStockState_PSTS_frmDMM0023_S(strPlantCode, strDurableInventory, strEquipCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                this.uGridDurableMatInfo.DataSource = dtDurableStockState;
                this.uGridDurableMatInfo.DataBind();

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtDurableStockState.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    foreach (Infragistics.Win.UltraWinGrid.UltraGridRow _uRow in this.uGridDurableMatInfo.Rows)
                    {
                        if (!(_uRow.Cells["DurableInventoryName"].Value.ToString().Equals(string.Empty)))
                        {
                            if (Convert.ToInt32(_uRow.Cells["Qty"].Value) <= (Convert.ToInt32(_uRow.Cells["SafeQty"].Value)))
                            {
                                _uRow.Appearance.BackColor = Color.Salmon;
                            }
                        }
                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableMatInfo, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }
        
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDurableMatInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridDurableMatInfo);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트
        //공장선택에 따라 창고가 달라짐
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlant = this.uComboSearchPlant.Value.ToString();

                if (!strPlant.Equals(string.Empty))
                {

                    this.uComboSearchInventory.Items.Clear();
                    
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor com = new WinComboEditor();

                    QRPBrowser brwChannel = new QRPBrowser();
                    //금형치공구 창고정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);

                    //금형치공구 창고정보조회매서드실행
                    DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlant, m_resSys.GetString("SYS_LANG"));

                    //콤보박스에 적용
                    com.mfSetComboEditor(this.uComboSearchInventory, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_LANG"), true
                        , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "DurableInventoryCode", "DurableInventoryName", dtDurableInven);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //금형치공구정보 키다운 설정
        private void uTextSearchDurableMat_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //코드를 지우면 명이지워진다.
                if (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    if(!this.uTextDurableMatName.Text.Equals(string.Empty))
                        this.uTextDurableMatName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (!this.uTextSearchDurableMat.Text.Equals(string.Empty))
                    {
                        //치공구코드저장
                        string strDurable = this.uTextSearchDurableMat.Text;

                        //공장이 널이면
                        if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                            return;
                        }
                        //공장 치공구 코드 저장
                        string strPlantCode = this.uComboSearchPlant.Value.ToString();
                        string strDurableCode = this.uTextSearchDurableMat.Text;

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                        QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                        brwChannel.mfCredentials(clsDurableMat);

                        DataTable dtDurableMat = clsDurableMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                        if (dtDurableMat.Rows.Count > 0)
                        {
                            this.uTextDurableMatName.Text = dtDurableMat.Rows[0]["DurableMatName"].ToString();

                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001101", "M000894", Infragistics.Win.HAlign.Right);

                            this.uTextDurableMatName.Clear();
                            return;
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //금형치공구정보 에디트버튼
        private void uTextSearchDurableMat_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();


                // 공장콤보 값이 공백일 경우 메세지 박스를 띄운다.
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);

                    //공장 콤보DropDown
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                // 공장 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                frmPOP0007 frmDurableMat = new frmPOP0007();
                frmDurableMat.PlantCode = strPlantCode;
                frmDurableMat.ShowDialog();

                // 금형치공구코드 저장
                string strDurableMatCode = frmDurableMat.DurableMatCode;


                // 팝업창에서 선택한 금형치공구의 정보를 각 해당 컨트롤에 삽입한다.
                this.uTextSearchDurableMat.Text = frmDurableMat.DurableMatCode;
                this.uTextDurableMatName.Text = frmDurableMat.DurableMatName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비코드 자동조회
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (e.KeyData == Keys.Delete || e.KeyData == Keys.Back)
                {
                    if (!this.uTextEquipName.Text.Equals(string.Empty))
                    {
                        this.uTextEquipName.Clear();
                    }
                }

                //엔터키를 누를시
                if (e.KeyData.Equals(Keys.Enter))
                {
                    if (!this.uTextEquipCode.Text.Equals(string.Empty))
                    {
                        //공장정보 체크
                        if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                            //DropDown
                            this.uComboSearchPlant.DropDown();
                            return;
                        }

                        //공장,설비 정보 저장
                        string strPlant = this.uComboSearchPlant.Value.ToString();
                        string strEquip = this.uTextEquipCode.Text;

                        QRPBrowser brwChannel = new QRPBrowser();
                        //설비정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);
                        //설비정보 조회 매서드호출
                        DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlant, strEquip, m_resSys.GetString("SYS_LANG"));

                        //정보가 있는경우
                        if (dtEquip.Rows.Count > 0)
                        {
                            this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();             //설비코드
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001116", "M000902", Infragistics.Win.HAlign.Right);

                            if (!this.uTextEquipName.Text.Equals(string.Empty))
                            {
                                this.uTextEquipName.Text = "";      //설비명
                            }

                            return;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비코드 조회POPUP창
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();



                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                frmPOP0005 frmEquip = new frmPOP0005();
                //공장정보를 보낸다.
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                // 선택한 정보 각 컨트롤에 삽입 //
                this.uTextEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmDMM0023_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
