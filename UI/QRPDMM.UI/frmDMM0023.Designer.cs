﻿namespace QRPDMM.UI
{
    partial class frmDMM0023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0023));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchDurableMat = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDurableMat = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableMatInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMat);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMat);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextDurableMatName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance16;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(844, 36);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 2;
            // 
            // uTextSearchDurableMat
            // 
            appearance17.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance17;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDurableMat.ButtonsRight.Add(editorButton2);
            this.uTextSearchDurableMat.Location = new System.Drawing.Point(740, 36);
            this.uTextSearchDurableMat.MaxLength = 20;
            this.uTextSearchDurableMat.Name = "uTextSearchDurableMat";
            this.uTextSearchDurableMat.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMat.TabIndex = 2;
            this.uTextSearchDurableMat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchDurableMat_KeyDown);
            this.uTextSearchDurableMat.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDurableMat_EditorButtonClick);
            // 
            // uLabelSearchDurableMat
            // 
            this.uLabelSearchDurableMat.Location = new System.Drawing.Point(636, 36);
            this.uLabelSearchDurableMat.Name = "uLabelSearchDurableMat";
            this.uLabelSearchDurableMat.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDurableMat.TabIndex = 0;
            this.uLabelSearchDurableMat.Text = "ultraLabel1";
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(284, 36);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 0;
            this.uLabelSearchEquip.Text = "ultraLabel1";
            // 
            // uComboSearchInventory
            // 
            this.uComboSearchInventory.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchInventory.MaxLength = 50;
            this.uComboSearchInventory.Name = "uComboSearchInventory";
            this.uComboSearchInventory.Size = new System.Drawing.Size(116, 21);
            this.uComboSearchInventory.TabIndex = 1;
            // 
            // uLabelSearchInventory
            // 
            this.uLabelSearchInventory.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchInventory.Name = "uLabelSearchInventory";
            this.uLabelSearchInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInventory.TabIndex = 0;
            this.uLabelSearchInventory.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(116, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridDurableMatInfo
            // 
            this.uGridDurableMatInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMatInfo.DisplayLayout.Appearance = appearance5;
            this.uGridDurableMatInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMatInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatInfo.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridDurableMatInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMatInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridDurableMatInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMatInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMatInfo.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMatInfo.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridDurableMatInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMatInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatInfo.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMatInfo.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridDurableMatInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMatInfo.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMatInfo.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridDurableMatInfo.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridDurableMatInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMatInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMatInfo.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridDurableMatInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMatInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridDurableMatInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMatInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMatInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMatInfo.Location = new System.Drawing.Point(0, 100);
            this.uGridDurableMatInfo.Name = "uGridDurableMatInfo";
            this.uGridDurableMatInfo.Size = new System.Drawing.Size(1070, 740);
            this.uGridDurableMatInfo.TabIndex = 2;
            // 
            // uTextEquipCode
            // 
            appearance14.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance14;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.Location = new System.Drawing.Point(388, 36);
            this.uTextEquipCode.MaxLength = 20;
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 2;
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uTextEquipName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance15;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(492, 36);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 2;
            // 
            // frmDMM0023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridDurableMatInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0023";
            this.Load += new System.EventHandler(this.frmDMM0023_Load);
            this.Activated += new System.EventHandler(this.frmDMM0023_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMM0023_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMatInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMat;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMat;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInventory;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMatInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
    }
}