﻿namespace QRPDMM.UI
{
    partial class frmDMM0020
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0020));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextChgChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextChgChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChgChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uDateChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelChgDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateChgDate)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextEquipName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance7;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(480, 12);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 6;
            // 
            // uTextEquipCode
            // 
            appearance6.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance6;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipCode.Location = new System.Drawing.Point(380, 12);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 5;
            this.uTextEquipCode.ValueChanged += new System.EventHandler(this.uTextEquipCode_ValueChanged);
            this.uTextEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEquipCode_KeyDown);
            this.uTextEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(276, 12);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquip.TabIndex = 4;
            this.uLabelEquip.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 3;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGridDurableTransferD);
            this.uGroupBox1.Controls.Add(this.uTextChgChargeName);
            this.uGroupBox1.Controls.Add(this.uTextChgChargeID);
            this.uGroupBox1.Controls.Add(this.uLabelChgChargeID);
            this.uGroupBox1.Controls.Add(this.uDateChgDate);
            this.uGroupBox1.Controls.Add(this.uLabelChgDate);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox1.TabIndex = 2;
            // 
            // uGridDurableTransferD
            // 
            this.uGridDurableTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableTransferD.DisplayLayout.Appearance = appearance11;
            this.uGridDurableTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGridDurableTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableTransferD.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridDurableTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance16;
            appearance18.TextHAlignAsString = "Left";
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableTransferD.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridDurableTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridDurableTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableTransferD.Location = new System.Drawing.Point(12, 32);
            this.uGridDurableTransferD.Name = "uGridDurableTransferD";
            this.uGridDurableTransferD.Size = new System.Drawing.Size(1044, 712);
            this.uGridDurableTransferD.TabIndex = 10;
            this.uGridDurableTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uTextChgChargeName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgChargeName.Appearance = appearance3;
            this.uTextChgChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgChargeName.Location = new System.Drawing.Point(444, 28);
            this.uTextChgChargeName.Name = "uTextChgChargeName";
            this.uTextChgChargeName.ReadOnly = true;
            this.uTextChgChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextChgChargeName.TabIndex = 9;
            // 
            // uTextChgChargeID
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChgChargeID.Appearance = appearance5;
            this.uTextChgChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance2;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextChgChargeID.ButtonsRight.Add(editorButton2);
            this.uTextChgChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextChgChargeID.Location = new System.Drawing.Point(340, 28);
            this.uTextChgChargeID.Name = "uTextChgChargeID";
            this.uTextChgChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextChgChargeID.TabIndex = 8;
            this.uTextChgChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextChgChargeID_KeyDown);
            this.uTextChgChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextChgChargeID_EditorButtonClick);
            // 
            // uLabelChgChargeID
            // 
            this.uLabelChgChargeID.Location = new System.Drawing.Point(236, 28);
            this.uLabelChgChargeID.Name = "uLabelChgChargeID";
            this.uLabelChgChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelChgChargeID.TabIndex = 7;
            this.uLabelChgChargeID.Text = "ultraLabel1";
            // 
            // uDateChgDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateChgDate.Appearance = appearance4;
            this.uDateChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateChgDate.Location = new System.Drawing.Point(116, 28);
            this.uDateChgDate.Name = "uDateChgDate";
            this.uDateChgDate.Size = new System.Drawing.Size(100, 21);
            this.uDateChgDate.TabIndex = 6;
            // 
            // uLabelChgDate
            // 
            this.uLabelChgDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelChgDate.Name = "uLabelChgDate";
            this.uLabelChgDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelChgDate.TabIndex = 5;
            this.uLabelChgDate.Text = "ultraLabel1";
            // 
            // frmDMM0020
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0020";
            this.Load += new System.EventHandler(this.frmDMM0020_Load);
            this.Activated += new System.EventHandler(this.frmDMM0020_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMM0020_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateChgDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelChgChargeID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateChgDate;
        private Infragistics.Win.Misc.UltraLabel uLabelChgDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableTransferD;
    }
}