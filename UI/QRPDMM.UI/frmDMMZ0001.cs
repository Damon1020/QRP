﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmDMMZ0001.cs                                        */
/* 프로그램명   : 금형입고등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;


namespace QRPDMM.UI
{
    public partial class frmDMMZ0001 : Form,IToolbar
    {
        //다국어지원
            QRPGlobal Sysres = new QRPGlobal();
        public frmDMMZ0001()
        {
            InitializeComponent();
        }

        private void frmDMMZ0001_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0001_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();
            InitComboBox();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("금형입고등록",m_resSys.GetString("SYS_FONTNAME"),12);

                //기본값설정(로그인유저)
                ultraTextEditor19.Text = m_resSys.GetString("SYS_USERID");
                ultraTextEditor21.Text = m_resSys.GetString("SYS_USERNAME");

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.ultraButton1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel1, "금형코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel2, "금형명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel3, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel4, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                lbl.mfSetLabel(this.ultraLabel5, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel6, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel7, "Super설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel8, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel9, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel10, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel11, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel12, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel13, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel14, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel15, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel16, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel17, "STS입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel18, "제작년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel19, "설비등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.ultraLabel20, "설비인증구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel21, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel22, "Setup문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel23, "목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel24, "작업조", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel25, "Setup담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.ultraLabel26, "Setup완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel27, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel28, "Setup진행사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();
                
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONNAME"));
                
                //컬럼설정
                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "일자", "일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "진행현황", "진행현황", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 770, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGrid1, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();
                
                grp.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "금형정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                //폰트설정
                uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "설비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.DETAIL, "설비인증정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;            
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
#endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        public void mfExcel()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
