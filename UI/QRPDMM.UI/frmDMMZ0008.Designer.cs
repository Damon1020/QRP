﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0008));
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uGridDiscardFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextDiscardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateDiscardDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextDiscardChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDiscardChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDiscardChargeUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDiscardReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFile = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDiscardFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uButtonDown);
            this.uGroupBox2.Controls.Add(this.uGridDiscardFile);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox2.Controls.Add(this.uTextDiscardReason);
            this.uGroupBox2.Controls.Add(this.uDateDiscardDate);
            this.uGroupBox2.Controls.Add(this.uTextDiscardChargeName);
            this.uGroupBox2.Controls.Add(this.uTextDiscardChargeID);
            this.uGroupBox2.Controls.Add(this.uLabelDiscardDate);
            this.uGroupBox2.Controls.Add(this.uLabelDiscardChargeUser);
            this.uGroupBox2.Controls.Add(this.uLabelDiscardReason);
            this.uGroupBox2.Controls.Add(this.uLabelFile);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 124);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1060, 732);
            this.uGroupBox2.TabIndex = 14;
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(224, 88);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonDown.TabIndex = 45;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uGridDiscardFile
            // 
            this.uGridDiscardFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDiscardFile.DisplayLayout.Appearance = appearance9;
            this.uGridDiscardFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDiscardFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDiscardFile.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDiscardFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridDiscardFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDiscardFile.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridDiscardFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDiscardFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDiscardFile.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDiscardFile.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridDiscardFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDiscardFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDiscardFile.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDiscardFile.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGridDiscardFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDiscardFile.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDiscardFile.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance32.TextHAlignAsString = "Left";
            this.uGridDiscardFile.DisplayLayout.Override.HeaderAppearance = appearance32;
            this.uGridDiscardFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDiscardFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.uGridDiscardFile.DisplayLayout.Override.RowAppearance = appearance31;
            this.uGridDiscardFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDiscardFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.uGridDiscardFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDiscardFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDiscardFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDiscardFile.Location = new System.Drawing.Point(12, 96);
            this.uGridDiscardFile.Name = "uGridDiscardFile";
            this.uGridDiscardFile.Size = new System.Drawing.Size(1040, 620);
            this.uGridDiscardFile.TabIndex = 44;
            this.uGridDiscardFile.Text = "ultraGrid1";
            this.uGridDiscardFile.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGridDiscardFile.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDiscardFile_ClickCellButton);
            this.uGridDiscardFile.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDiscardFile_CellChange);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(132, 88);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 43;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextDiscardReason
            // 
            this.uTextDiscardReason.Location = new System.Drawing.Point(132, 52);
            this.uTextDiscardReason.MaxLength = 1000;
            this.uTextDiscardReason.Name = "uTextDiscardReason";
            this.uTextDiscardReason.Size = new System.Drawing.Size(480, 21);
            this.uTextDiscardReason.TabIndex = 42;
            this.uTextDiscardReason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDiscardReason_KeyDown);
            // 
            // uDateDiscardDate
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardDate.Appearance = appearance5;
            this.uDateDiscardDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateDiscardDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDiscardDate.Location = new System.Drawing.Point(132, 28);
            this.uDateDiscardDate.Name = "uDateDiscardDate";
            this.uDateDiscardDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDiscardDate.TabIndex = 41;
            this.uDateDiscardDate.ValueChanged += new System.EventHandler(this.uDateDiscardDate_ValueChanged);
            // 
            // uTextDiscardChargeName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardChargeName.Appearance = appearance15;
            this.uTextDiscardChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDiscardChargeName.Location = new System.Drawing.Point(512, 28);
            this.uTextDiscardChargeName.Name = "uTextDiscardChargeName";
            this.uTextDiscardChargeName.ReadOnly = true;
            this.uTextDiscardChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextDiscardChargeName.TabIndex = 37;
            // 
            // uTextDiscardChargeID
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDiscardChargeID.Appearance = appearance6;
            this.uTextDiscardChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance1;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDiscardChargeID.ButtonsRight.Add(editorButton1);
            this.uTextDiscardChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDiscardChargeID.Location = new System.Drawing.Point(408, 28);
            this.uTextDiscardChargeID.MaxLength = 20;
            this.uTextDiscardChargeID.Name = "uTextDiscardChargeID";
            this.uTextDiscardChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextDiscardChargeID.TabIndex = 36;
            this.uTextDiscardChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDiscardChargeID_KeyDown);
            this.uTextDiscardChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDiscardChargeID_EditorButtonClick);
            // 
            // uLabelDiscardDate
            // 
            this.uLabelDiscardDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelDiscardDate.Name = "uLabelDiscardDate";
            this.uLabelDiscardDate.Size = new System.Drawing.Size(116, 20);
            this.uLabelDiscardDate.TabIndex = 13;
            this.uLabelDiscardDate.Text = "5";
            // 
            // uLabelDiscardChargeUser
            // 
            this.uLabelDiscardChargeUser.Location = new System.Drawing.Point(288, 28);
            this.uLabelDiscardChargeUser.Name = "uLabelDiscardChargeUser";
            this.uLabelDiscardChargeUser.Size = new System.Drawing.Size(116, 20);
            this.uLabelDiscardChargeUser.TabIndex = 15;
            this.uLabelDiscardChargeUser.Text = "6";
            // 
            // uLabelDiscardReason
            // 
            this.uLabelDiscardReason.Location = new System.Drawing.Point(12, 52);
            this.uLabelDiscardReason.Name = "uLabelDiscardReason";
            this.uLabelDiscardReason.Size = new System.Drawing.Size(116, 20);
            this.uLabelDiscardReason.TabIndex = 17;
            this.uLabelDiscardReason.Text = "7";
            // 
            // uLabelFile
            // 
            this.uLabelFile.Location = new System.Drawing.Point(12, 92);
            this.uLabelFile.Name = "uLabelFile";
            this.uLabelFile.Size = new System.Drawing.Size(116, 20);
            this.uLabelFile.TabIndex = 19;
            this.uLabelFile.Text = "8";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uLabelVendor);
            this.uGroupBox1.Controls.Add(this.uComboPlant);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Controls.Add(this.uTextVendor);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uTextSpec);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatName);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatName);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1060, 80);
            this.uGroupBox1.TabIndex = 12;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(288, 52);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(116, 20);
            this.uLabelVendor.TabIndex = 11;
            this.uLabelVendor.Text = "4";
            this.uLabelVendor.Visible = false;
            // 
            // uComboPlant
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance2;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(132, 28);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 36;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            this.uComboPlant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboPlant_BeforeDropDown);
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 52);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(116, 20);
            this.uLabelSpec.TabIndex = 9;
            this.uLabelSpec.Text = "3";
            // 
            // uTextVendor
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance18;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(408, 52);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(100, 21);
            this.uTextVendor.TabIndex = 12;
            this.uTextVendor.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(116, 20);
            this.uLabelPlant.TabIndex = 35;
            this.uLabelPlant.Text = "Plant";
            // 
            // uTextSpec
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance19;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(132, 52);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextSpec.TabIndex = 10;
            // 
            // uTextDurableMatName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance20;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(664, 28);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 8;
            // 
            // uLabelDurableMatName
            // 
            this.uLabelDurableMatName.Location = new System.Drawing.Point(544, 28);
            this.uLabelDurableMatName.Name = "uLabelDurableMatName";
            this.uLabelDurableMatName.Size = new System.Drawing.Size(116, 20);
            this.uLabelDurableMatName.TabIndex = 7;
            this.uLabelDurableMatName.Text = "2";
            // 
            // uTextDurableMatCode
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDurableMatCode.Appearance = appearance27;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance28;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDurableMatCode.ButtonsRight.Add(editorButton2);
            this.uTextDurableMatCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(408, 28);
            this.uTextDurableMatCode.MaxLength = 20;
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatCode.TabIndex = 6;
            this.uTextDurableMatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDurableMatCode_KeyDown);
            this.uTextDurableMatCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDurableMatCode_EditorButtonClick);
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(288, 28);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(116, 20);
            this.uLabelDurableMatCode.TabIndex = 5;
            this.uLabelDurableMatCode.Text = "1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 13;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMMZ0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0008";
            this.Load += new System.EventHandler(this.frmDMMZ0008_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0008_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0008_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDiscardFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDiscardChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDiscardFile;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardReason;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDiscardChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardChargeUser;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardReason;
        private Infragistics.Win.Misc.UltraLabel uLabelFile;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
    }
}