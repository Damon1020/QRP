﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 변경점관리                                            */
/* 프로그램ID   : frmDMMZ0010.cs                                        */
/* 프로그램명   : 점검출고등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-08                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using Microsoft.VisualBasic;
namespace QRPDMM.UI
{
    public partial class frmDMMZ0010 : Form, IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal Sysres = new QRPGlobal();
        private string strFormName;
        private string m_strBindCheck = "";

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmDMMZ0010(string strPlantCode, string strRepairCode)
        {
            InitializeComponent();
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();

            Display_Detail(strPlantCode, strRepairCode);
            

        }

        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }

        public frmDMMZ0010()
        {
            InitializeComponent();
        }

        private void frmDMMZ0010_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            
            ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0010_Load(object sender, EventArgs e)
        {
            if (strFormName == null)
            {
                //컨트롤초기화
                SetToolAuth();
                InitTitle();
                InitText();
                InitLabel();
                InitGrid();
                InitGroupBox();
                InitComboBox();
                uGroupBoxContentsArea.Expanded = false;
            }
            else
            {
                Point po = new Point(0, 0);
                this.Location = po;
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        
        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {//System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("점검출고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                //기본값설정(로그인유저)
                uTextRepairGIReqID.Text = m_resSys.GetString("SYS_USERID");
                uTextRepairGIReqName.Text = m_resSys.GetString("SYS_USERNAME");


                this.uTextCumUsage.Text = "";
                this.uTextCurUsage.Text = "";
                this.uTextDurableMatCode.Text = "";
                this.uTextDurableMatName.Text = "";
                this.utextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextRepairReqCode.Text = "";
                this.uTextRepairGICode.Text = "";
                
                this.uTextRepairGIReqName.Text = "";
                this.uTextRepairGubunName.Text = "";
                this.uTextSpec.Text = "";
                this.uTextUsageDocCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextLotNo.Text = "";

                this.uTextPlantCode.Visible = false;

                this.uTextUsageDocCode.Visible = false;
                this.uTextCumUsage.Visible = false;
                this.uTextCurUsage.Visible = false;
                this.uLabelVendor.Visible = false;
                this.uTextVendorName.Visible = false;
                this.uButtonDeleteRow.Visible = false;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelFinishDate, "정비완료일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchUser, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelRepairGICode, "점검출고코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairCode, "정비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairGubun, "정비구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlantName, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairGIReqDate, "수리출고요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairGIReqID, "수리출고요청자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEtcDesc, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGrid grd = new WinGrid();

                #region 정비정보
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,false,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정비정보
                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGICode", "점검출고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairReqCode", "정비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunCode", "정비구분코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunName", "정비구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid1, 0, "CurUsage", "Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "FinishDate", "정비완료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGIReqDate", "수리출고요청일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 수리출고구성품
                //--
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--수리출고구성품
                grd.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairGICode", "수리출고번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurLotNo", "LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");




                grd.mfSetGridColumn(this.uGrid2, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgLotNo", "LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgInvQty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CancelFlag", "취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                this.uGrid2.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGrid2.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #endregion

                #region 정비상세정보
                //--
                grd.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--정비상세정보
                grd.mfSetGridColumn(this.uGrid3, 0, "RepairPlace", "수정장소", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairPart", "수정부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairName", "정비내용", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMGubunCode", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMGubunName", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMTypeCode", "정비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "DurablePMTypeName", "정비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid3, 0, "작업조", "작업조", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairChargeID", "정비사ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairChargeName", "정비사명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "RepairTime", "정비시간(분)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 3, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                #endregion

                #region Font설정
                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Grid DropDown Binding

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommon);

                DataTable dtCommon = clsCommon.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));
                // 수리출고구분
                grd.mfSetGridColumnValueList(this.uGrid2, 0, "RepairGIGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCommon);

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGrid2, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();


                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "정비정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "수리출고구성품", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "정비상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromFinishDate = this.uDateSearchFromFinishDate.Value.ToString();
                string strToFinishDate = this.uDateSearchToFinishDate.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;
                string strEquipName = this.uTextSearchEquipName.Text;
                string strDurableMatCode = this.uTextSearchDurableMatCode.Text;
                string strDurableMatName = this.uTextSearchDurableMatName.Text;
                string strUserID = this.uTextSearchUserID.Text;
                string strUserName = this.uTextSearchUserName.Text;


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // Call Method
                DataTable dtRepairH = clsDurableMatRepairH.mfReadEQUDurableMatRepairGIH_DMMRepairH(strPlantCode
                    , strFromFinishDate
                    , strToFinishDate
                    , strEquipCode
                    , strEquipName
                    , strDurableMatCode
                    , strDurableMatName
                    , strUserID
                    , strUserName
                    , m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtRepairH;
                uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtRepairH.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intChgInvQty = 0;
                int intChgQty = 0;

                int intRowCheck = 0;
                string strStockFlag = "F";

                #region 필수입력
                // 필수입력사항 확인
                if (this.uTextPlantCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextRepairGICode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }

                
                #endregion

                //공장 ,설비 정보 저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strEquipCode = this.utextEquipCode.Text;

                

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH DurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(DurableMatRepairH);

                #region Header

                DataTable dtRepairH = DurableMatRepairH.mfDataSetInfo();

                DataRow drRepairH = dtRepairH.NewRow();

                drRepairH["PlantCode"] = strPlantCode; 
                drRepairH["RepairGICode"] = this.uTextRepairGICode.Text;
                drRepairH["GITypeCode"] = "LF";
                drRepairH["EquipCode"] = strEquipCode;
                drRepairH["RepairReqCode"] = this.uTextRepairReqCode.Text;
                drRepairH["RepairGIReqDate"] = Convert.ToDateTime(uDateRepairGIReqDate.Value).ToString("yyyy-MM-dd");
                drRepairH["RepairGIReqID"] = this.uTextRepairGIReqID.Text;
                drRepairH["EtcDesc"] = this.uTextEtcDesc.Text;

                dtRepairH.Rows.Add(drRepairH);
                #endregion

                #region Detail

                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);
                

                QRPEQU.BL.EQUMGM.DurableMatRepairD DurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                DataTable dtMatRepairD = DurableMatRepairD.mfsetDataInfo();

                this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {
                    if (!uGrid2.Rows[i].Hidden)
                    {
                        if (this.uGrid2.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit
                            && (this.uGrid2.Rows[i].Cells["CancelFlag"].Value).Equals(false) && this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            this.uGrid2.Rows[i].RowSelectorAppearance.Image = null;
                        }

                        if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            string strRowNum = this.uGrid2.Rows[i].RowSelectorNumber.ToString();

                            #region 필수 입력사항 확인
                            intRowCheck = intRowCheck + 1;

                            if (this.uGrid2.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000743", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["RepairGIGubunCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["CurDurableMatCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000485", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CurDurableMatName"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["InputQty"].Value.ToString() == "0" ||
                                this.uGrid2.Rows[i].Cells["InputQty"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000510", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["InputQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M001132", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000485", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgDurableMatName"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid2.Rows[i].Cells["ChgQty"].Value.ToString() == "0" ||
                                this.uGrid2.Rows[i].Cells["ChgQty"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "M000525", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }


                            intChgQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgQty"].Value.ToString());
                            intChgInvQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgInvQty"].Value.ToString());

                            //if (intChgInvQty < intChgQty)
                            //{
                            //    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                    , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 사용량이 재고량보다 클 수 없습니다. 확인 후 다시 입력해 주세요", Infragistics.Win.HAlign.Center);
                            //    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgQty"];
                            //    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            //    return;
                            //}

                            #endregion

                            #region 수량체크

                            //구성품의 교체 일시 수량을 확인한다.
                            if (!Convert.ToBoolean(this.uGrid2.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                            {
                                DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGrid2.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                            this.uGrid2.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                

                                if (dtDurableMat.Rows.Count == 0 || (Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["InputQty"].Value)))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000297", strRowNum + "M000487", Infragistics.Win.HAlign.Right);

                                    //SearchDurableBOM(strPlantCode,strEquipCode);
                                    //this.uGrid2.Rows[i].Cells["InputQty"].Value = 0;
                                    //this.uGrid2.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;
                                    //this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CurDurableMatName"];
                                    //this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                        this.uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                        this.uGrid2.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000297", strRowNum + "M000487", Infragistics.Win.HAlign.Right);

                                    SearchChgDurableMat(this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                    if (!this.uGrid2.Rows[i].Cells["ChgQty"].Value.Equals(1))
                                    {
                                        this.uGrid2.Rows[i].Cells["ChgQty"].Value = 0;
                                        this.uGrid2.Rows[i].Cells["ChgInvQty"].Value = 0;
                                        this.uGrid2.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                    }

                                    this.uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgDurableMatName"];
                                    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                            }
                            //구성품의 취소일 시 수량을 확인한다.
                            else
                            {

                                DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                            this.uGrid2.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                
                                if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000746", strRowNum + "M000487", Infragistics.Win.HAlign.Right);
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CancelFlag"];
                                    return;
                                }

                                DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                        this.uGrid2.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                        this.uGrid2.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGrid2.Rows[i].Cells["InputQty"].Value))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000746", strRowNum + "M000487", Infragistics.Win.HAlign.Right);
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["CancelFlag"];
                                    return;
                                }


                            }

                            #endregion


                            DataRow drRepairD = dtMatRepairD.NewRow();
                            drRepairD["PlantCode"] = this.uTextPlantCode.Text;
                            drRepairD["RepairGICode"] = this.uTextRepairGICode.Text;

                            if (uGrid2.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                            {                                
                                drRepairD["Seq"] = uGrid2.Rows[i].Cells["Seq"].Value;
                                drRepairD["CancelFlag"] = Convert.ToBoolean(uGrid2.Rows[i].Cells["CancelFlag"].Value) == false ? "F" : "T";
                            }
                            else
                            {                                
                                drRepairD["Seq"] = 0;
                                drRepairD["CancelFlag"] = "F";
                            }

                            drRepairD["RepairGIGubunCode"] = uGrid2.Rows[i].Cells["RepairGIGubunCode"].Value;
                            drRepairD["CurDurableMatCode"] = uGrid2.Rows[i].Cells["CurDurableMatCode"].Value;
                            drRepairD["CurLotNo"] = uGrid2.Rows[i].Cells["CurLotNo"].Value;
                            drRepairD["CurQty"] = uGrid2.Rows[i].Cells["InputQty"].Value;
                            drRepairD["ChgDurableInventoryCode"] = uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value;
                            drRepairD["ChgDurableMatCode"] = uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value;
                            drRepairD["ChgLotNo"] = uGrid2.Rows[i].Cells["ChgLotNo"].Value;
                            drRepairD["UnitCode"] = "EA";
                            drRepairD["ChgQty"] = uGrid2.Rows[i].Cells["ChgQty"].Value;
                            drRepairD["EtcDesc"] = uGrid2.Rows[i].Cells["EtcDesc"].Value;

                            dtMatRepairD.Rows.Add(drRepairD);
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001237", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }


                #endregion

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                string rtMSG = DurableMatRepairH.mfSaveDurableMatRepairH(dtRepairH, dtMatRepairD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
                InitText();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0 && ((this.uGrid2.Rows.Count == 0 && this.uGrid3.Rows.Count == 0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                if(this.uGrid1.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGrid1);

                if(this.uGrid2.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid2);
                if (this.uGrid3.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid3);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        #region 이벤트
        //접히거나 닫힐때 일어나는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 175);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 700;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
  
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        #endregion

        #region 텍스트 이벤트
        private void ultraTextEditor4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0005 frm = new frmPOP0005();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchEquipCode.Text = frm.EquipCode;
                uTextSearchEquipName.Text = frm.EquipName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0007 frm = new frmPOP0007();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchDurableMatCode.Text = frm.DurableMatCode;
                uTextSearchDurableMatName.Text = frm.DurableMatName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchUserID.Text = frm.UserID;
                uTextSearchUserName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextRepairGIReqID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uTextPlantCode.Text == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    //this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uTextPlantCode.Text;
                frm.ShowDialog();

                uTextRepairGIReqID.Text = frm.UserID;
                uTextRepairGIReqName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextRepairGIReqID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                String strPlantCode = this.uTextPlantCode.Text;

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextRepairGIReqID;
                uTextName = this.uTextRepairGIReqName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        //this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 그리드 이벤트
        private void uGrid2_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            

            try
            {
                //e.Row.Cells["EditFlag"].Value = false;
                e.Row.Cells["CancelFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try 
            {
                

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strRepairGICode = e.Cell.Row.Cells["RepairGICode"].Value.ToString();
                string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strRepairGICode == "")
                    return;
                
                mfCreate();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    
                }
                e.Cell.Row.Fixed = true;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                Display_Detail(strPlantCode, strRepairGICode);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Display_Detail(string strPlantCode, string strRepairGICode)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinGrid wGrid = new WinGrid();
                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }


                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);

                // Call Method
                DataTable dtDurableMatRepairH = clsDurableMatRepairH.mfReadEQUDurableMatRepairGIH_DMMRepairH_Detail(strPlantCode, strRepairGICode, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtDurableMatRepairH.Rows.Count; i++)
                {
                    this.uTextPlantCode.Text = dtDurableMatRepairH.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtDurableMatRepairH.Rows[i]["PlantName"].ToString();

                    this.uTextDurableMatCode.Text = dtDurableMatRepairH.Rows[i]["DurableMatCode"].ToString();
                    this.uTextDurableMatName.Text = dtDurableMatRepairH.Rows[i]["DurableMatName"].ToString();
                    this.uTextLotNo.Text = dtDurableMatRepairH.Rows[i]["LotNo"].ToString();
                    this.utextEquipCode.Text = dtDurableMatRepairH.Rows[i]["EquipCode"].ToString();
                    this.utextEquipCode.Tag = dtDurableMatRepairH.Rows[i]["ModelName"].ToString();
                    this.uTextEquipName.Text = dtDurableMatRepairH.Rows[i]["EquipName"].ToString();
                    this.uTextEtcDesc.Text = dtDurableMatRepairH.Rows[i]["EtcDesc"].ToString();
                    this.uTextSpec.Text = dtDurableMatRepairH.Rows[i]["Spec"].ToString();
                    this.uTextUsageDocCode.Text = dtDurableMatRepairH.Rows[i]["UsageDocCode"].ToString();
                    this.uTextCumUsage.Text = dtDurableMatRepairH.Rows[i]["CumUsage"].ToString();
                    this.uTextCurUsage.Text = dtDurableMatRepairH.Rows[i]["CurUsage"].ToString();
                    this.uTextRepairGICode.Text = dtDurableMatRepairH.Rows[i]["RepairGICode"].ToString();

                    //this.uTextRepairGIReqID.Text = dtDurableMatRepairH.Rows[i]["RepairGIReqID"].ToString();
                    //this.uTextRepairGIReqName.Text = dtDurableMatRepairH.Rows[i]["RepairGIReqName"].ToString();

                    this.uTextRepairReqCode.Text =  dtDurableMatRepairH.Rows[i]["RepairReqCode"].ToString();

                    this.uTextRepairGubunName.Text = dtDurableMatRepairH.Rows[i]["RepairGubunName"].ToString();
                    this.uTextSpec.Text = dtDurableMatRepairH.Rows[i]["Spec"].ToString();
                    this.uTextUsageDocCode.Text = "";
                    this.uTextVendorName.Text = "";
                    
                }
                

                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                //QRPMAS.BL.MASDMM.DurableInventory clsInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                //brwChannel.mfCredentials(clsInventory);
                //DataTable dtInventory = clsInventory.mfReadDurableInventoryCombo(this.uTextPlantCode.Text, m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(uGrid2, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

                //brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                //QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                //brwChannel.mfCredentials(clsDurableMat);
                //DataTable dtclsDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_OnlyLot(this.uTextPlantCode.Text, this.utextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                //wGrid.mfSetGridColumnValueGridList(uGrid2, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
                //    "DurableMatCode,DurableMatName,LotNo,InputQty", "치공구코드,치공구,LOTNO,수량", "DurableMatCode", "DurableMatName", dtclsDurableMat);

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsEquipD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsEquipD);

                DataTable dtDetail = clsEquipD.mfReadDurableMatRepairD(this.uTextPlantCode.Text, this.utextEquipCode.Text, this.uTextRepairGICode.Text, m_resSys.GetString("SYS_LANG"));

                

                if (dtDetail.Rows.Count > 0)
                {
                    //정보가 있는 경우 맨마지막의 줄의 수리취소여부에따라 출추가여부가 결정됨
                    if (dtDetail.Rows[dtDetail.Rows.Count - 1]["CancelFlag"].ToString().Equals("True"))
                    {
                        DataRow drDetail_bin = dtDetail.NewRow();
                        drDetail_bin["Seq"] = 0;
                        drDetail_bin["PlantCode"] = "";
                        drDetail_bin["RepairGICode"] = this.uTextRepairGICode.Text;
                        drDetail_bin["RepairGIGubunCode"] = "INN";
                        drDetail_bin["CurDurableMatCode"] = this.uTextDurableMatCode.Text;
                        drDetail_bin["CurDurableMatName"] = this.uTextDurableMatName.Text;
                        drDetail_bin["CurLotNo"] = this.uTextLotNo.Text;
                        drDetail_bin["InputQty"] = "1";

                        drDetail_bin["ChgDurableInventoryCode"] = "";
                        drDetail_bin["ChgDurableMatCode"] = "";
                        drDetail_bin["ChgDurableMatName"] = "";
                        drDetail_bin["ChgLotNo"] = "";
                        drDetail_bin["ChgInvQty"] = "0";
                        drDetail_bin["ChgQty"] = "0";
                        drDetail_bin["UnitCode"] = "EA";
                        drDetail_bin["EtcDesc"] = "";
                        drDetail_bin["CancelFlag"] = false;
                        dtDetail.Rows.Add(drDetail_bin);
                    }

                    m_strBindCheck = "T";
                    uGrid2.Refresh();
                    SearChDurableInven(strPlantCode);
                    //SearchDurableBOM(strPlantCode, this.utextEquipCode.Text);
                    uGrid2.DataSource = dtDetail;
                    uGrid2.DataBind();
                    uGrid2_AfterDataBinding();
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }
                else
                {
                    #region 바인드할 테이블설정
                    DataTable dtDetail_bin = new DataTable();
                    dtDetail_bin.Columns.Add("Check", typeof(Boolean));
                    dtDetail_bin.Columns.Add("Seq", typeof(String));
                    dtDetail_bin.Columns.Add("PlantCode", typeof(String));
                    dtDetail_bin.Columns.Add("RepairGICode", typeof(String));
                    dtDetail_bin.Columns.Add("RepairGIGubunCode", typeof(String));
                    dtDetail_bin.Columns.Add("CurDurableMatCode", typeof(String));
                    dtDetail_bin.Columns.Add("CurDurableMatName", typeof(String));
                    dtDetail_bin.Columns.Add("CurLotNo", typeof(String));
                    dtDetail_bin.Columns.Add("InputQty", typeof(String));
                    dtDetail_bin.Columns.Add("UnitName", typeof(String));
                    dtDetail_bin.Columns.Add("ChgDurableInventoryCode", typeof(String));
                    dtDetail_bin.Columns.Add("ChgDurableMatCode", typeof(String));
                    dtDetail_bin.Columns.Add("ChgDurableMatName", typeof(String));
                    dtDetail_bin.Columns.Add("ChgLotNo", typeof(String));
                    dtDetail_bin.Columns.Add("ChgInvQty", typeof(String));
                    dtDetail_bin.Columns.Add("ChgQty", typeof(String));
                    dtDetail_bin.Columns.Add("UnitCode", typeof(String));
                    dtDetail_bin.Columns.Add("EtcDesc", typeof(String));
                    dtDetail_bin.Columns.Add("CancelFlag", typeof(Boolean));

                    DataRow drDetail_bin = dtDetail_bin.NewRow();
                    drDetail_bin["Check"] = false;
                    drDetail_bin["Seq"] = 0;
                    drDetail_bin["PlantCode"] = "";
                    drDetail_bin["RepairGICode"] = this.uTextRepairGICode.Text;
                    drDetail_bin["RepairGIGubunCode"] = "INN";
                    drDetail_bin["CurDurableMatCode"] = this.uTextDurableMatCode.Text;
                    drDetail_bin["CurDurableMatName"] = this.uTextDurableMatName.Text;
                    drDetail_bin["CurLotNo"] = this.uTextLotNo.Text;
                    drDetail_bin["InputQty"] = "1";
                    drDetail_bin["UnitName"] = "EA";

                    drDetail_bin["ChgDurableInventoryCode"] = "";
                    drDetail_bin["ChgDurableMatCode"] = "";
                    drDetail_bin["ChgDurableMatName"] = "";
                    drDetail_bin["ChgLotNo"] = "";
                    drDetail_bin["ChgInvQty"] = "0";
                    drDetail_bin["ChgQty"] = "0";
                    drDetail_bin["UnitCode"] = "EA";                    
                    drDetail_bin["EtcDesc"] =   "";
                    drDetail_bin["CancelFlag"] = false;
                    dtDetail_bin.Rows.Add(drDetail_bin);
                    #endregion

                    uGrid2.Refresh();
                    SearChDurableInven(strPlantCode);
                    uGrid2.DataSource = dtDetail_bin;
                    uGrid2.DataBind();
                    uGrid2_AfterDataBinding();

                    m_strBindCheck = "F";
                }

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairD), "RepairD");
                QRPDMM.BL.DMMMGM.RepairD clsRepairD = new QRPDMM.BL.DMMMGM.RepairD();
                brwChannel.mfCredentials(clsRepairD);

                // Call Method
                DataTable dtRepairD = clsRepairD.mfReadRepairD(strPlantCode, this.uTextRepairReqCode.Text, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid3.DataSource = dtRepairD;
                uGrid3.DataBind();

            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterDataBinding()
        {
            try
            {
                if (m_strBindCheck != "T")
                    return;

                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid2.Rows[i].GetCellValue("CancelFlag")))
                    {
                        uGrid2.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;
                        uGrid2.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    else
                    {
                        for (int j = 0; j < uGrid2.Rows[i].Cells.Count; j++)
                        {
                            if (uGrid2.Rows[i].Cells["PlantCode"].Value.ToString() != string.Empty)
                            {
                                if (uGrid2.Rows[i].Cells[j].Column.Key.ToString() == "CancelFlag" ||
                                    uGrid2.Rows[i].Cells[j].Column.Key.ToString() == "RepairGIGubunName")
                                {
                                    uGrid2.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }
                                else
                                {
                                    uGrid2.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                }
                            }
                            if (this.uGrid2.Rows[i].Cells["InputQty"].Value.ToString().Equals("1"))
                            {
                                this.uGrid2.Rows[i].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }

                        }
                    }

                    uGrid2.Rows[i].Cells["InputQty"].Tag = uGrid2.Rows[i].Cells["InputQty"].Value;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ////try
            ////{
            ////    QRPGlobal grdImg = new QRPGlobal();
            ////    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            ////    QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            ////    if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
            ////        e.Cell.Row.Delete(false);

            ////    int intSelectItem = 0;


            ////    titleArea.Focus();
            ////    WinGrid wGrid = new WinGrid();
            ////    #region CurDurableMatCode
            ////    if (e.Cell.Column.Key.ToString() == "CurDurableMatName")
            ////    {
            ////        ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
            ////        QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
            ////        brwChannel.mfCredentials(clsDurableMat);

            ////        string strDurableMatCode = uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value.ToString();
            ////        string strLang = m_resSys.GetString("SYS_LANG");
            ////        DataTable dtDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_Lot(this.uTextPlantCode.Text, this.utextEquipCode.Text, strLang);

            ////        if (dtDurableMat.Rows.Count != 0)
            ////        {
            ////            //DataRow[] drDurable = dtDurableMat.Select("DurableMatCode = '" + strDurableMatCode + "'");

            ////            DataRow[] drDurable = dtDurableMat.Select("DurableMatCode = '" + strDurableMatCode + "'");

            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = drDurable[0]["InputQty"];
            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = drDurable[0]["InputQty"];

            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = drDurable[0]["LotNo"];
            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = drDurable[0]["DurableMatCode"];

            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["UnitCode"].Value = drDurable[0]["UnitCode"];
            ////            uGrid2.Rows[e.Cell.Row.Index].Cells["UnitName"].Value = drDurable[0]["UnitName"];

            ////            ////intSelectItem = Convert.ToInt32(e.Cell.ValueList.SelectedItemIndex.ToString());

            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();
            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();

            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = dtDurableMat.Rows[intSelectItem]["LotNo"].ToString();
            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = drDurable[0]["DurableMatCode"];

            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["UnitCode"].Value = drDurable[0]["UnitCode"];
            ////            ////uGrid2.Rows[e.Cell.Row.Index].Cells["UnitName"].Value = drDurable[0]["UnitName"];



            ////            e.Cell.Row.Cells["ChgDurableInventoryName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////            if (!drDurable[0]["LotNo"].ToString().Equals(string.Empty))
            ////            {
            ////                uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////            }
            ////            else
            ////            {
            ////                uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////            }

            ////            for (int i = 0; i < uGrid2.Rows.Count; i++)
            ////            {
            ////                if (!i.Equals(e.Cell.Row.Index) &&
            ////                    uGrid2.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(drDurable[0]["LotNo"].ToString()) &&
            ////                    !drDurable[0]["LotNo"].ToString().Equals(string.Empty))
            ////                {
            ////                    WinMessageBox msg = new WinMessageBox();
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "M001264", "M001264", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = 0;

            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////                    return;
            ////                }
            ////            }
            ////        }
            ////    }
            ////    #endregion

            ////    #region ChgDurableInventory
            ////    if (e.Cell.Column.Key.ToString().Equals("ChgDurableInventoryCode"))
            ////    {
            ////        //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
            ////        string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString();

            ////        ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ////        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
            ////        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
            ////        brwChannel.mfCredentials(clsDurableStock);

            ////        DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(this.uTextPlantCode.Text, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

            ////        string strCode = "";
            ////        if (e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
            ////        {
            ////            strCode = "LotNo = ''";
            ////        }
            ////        else
            ////        {
            ////            strCode = "LotNo <> ''";
            ////        }

            ////        DataRow[] dr = dtStock.Select(strCode);

            ////        DataTable dtBind = dtStock.Copy();
            ////        dtBind.Clear();

            ////        for (int i = 0; i < dr.Length; i++)
            ////        {
            ////            DataRow _dr = dtBind.NewRow();
            ////            for (int j = 0; j < _dr.ItemArray.Length; j++)
            ////            {
            ////                _dr[j] = dr[i][j];
            ////            }
            ////            dtBind.Rows.Add(_dr);
            ////        }

            ////        //WinGrid wGrid = new WinGrid();
            ////        wGrid.mfSetGridCellValueGridList(uGrid2, 0, e.Cell.Row.Index, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
            ////            "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName", "치공구코드,치공구,LOTNO,수량,단위코드,단위", "DurableMatCode", "DurableMatName", dtBind);

            ////    }
            ////    #endregion

            ////    #region ChgDurableMatCode
            ////    if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatName"))
            ////    {
            ////        //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
            ////        string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString();
            ////        string _strDurableMatCode = e.Cell.Value.ToString();

            ////        ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ////        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
            ////        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
            ////        brwChannel.mfCredentials(clsDurableStock);

            ////        DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(this.uTextPlantCode.Text, _strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

            ////        if (!dtStock.Rows.Count.Equals(0))
            ////        {

            ////            DataRow[] dr = dtStock.Select("DurableMatCode = '" + _strDurableMatCode + "'");

            ////            e.Cell.Row.Cells["ChgLotNo"].Value = dr[0]["LotNo"].ToString();
            ////            //e.Cell.Row.Cells["CurQty"].Tag = dr[0]["Qty"].ToString();

            ////            e.Cell.Row.Cells["ChgInvQty"].Tag = dr[0]["Qty"].ToString();
            ////            e.Cell.Row.Cells["ChgInvQty"].Value = dr[0]["Qty"].ToString();

            ////            e.Cell.Row.Cells["ChgDurableMatCode"].Value = dr[0]["DurableMatCode"];

            ////            if (!dr[0]["LotNo"].ToString().Equals(string.Empty))
            ////            {
            ////                e.Cell.Row.Cells["ChgQty"].Tag = dr[0]["Qty"].ToString();
            ////                e.Cell.Row.Cells["ChgQty"].Value = dr[0]["Qty"].ToString();

            ////                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ////            }

            ////            for (int i = 0; i < uGrid2.Rows.Count; i++)
            ////            {
            ////                if (!i.Equals(e.Cell.Row.Index) &&
            ////                    uGrid2.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dr[0]["LotNo"].ToString()) &&
            ////                    !dr[0]["LotNo"].ToString().Equals(string.Empty))
            ////                {
            ////                    WinMessageBox msg = new WinMessageBox();
            ////                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "M001264", "M001264", "이미 입력한 LOTID 입니다.", Infragistics.Win.HAlign.Right);

            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = 0;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Value = 0;

            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;
            ////                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
            ////                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ////                    return;
            ////                }
            ////            }
            ////        }

            ////    }
            ////    #endregion

            ////}
            ////catch (Exception ex)
            ////{
            ////}

            ////finally
            ////{
            ////}
        }

        private void uGrid2_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.Key.ToString().Equals("ChgDurableInventoryCode"))
                {
                    if (e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001264", "M000318", Infragistics.Win.HAlign.Right);
                        e.Cell.Row.Cells["ChgDurableInventoryCode"].Value = "";
                        this.uGrid2.ActiveCell = e.Cell.Row.Cells["CurDurableMatName"];
                        this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {       
            try
            {
                int intSelectItem = 0;

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);


                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                

                // Cell 수정시 RowSelector 이미지 변경

                // 빈줄이면 자동삭제

                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                #region 수량

                if (e.Cell.Column.Key.Equals("InputQty"))
                {
                    if (e.Cell.Tag != null && !e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        if (!Information.IsNumeric(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M001135", "M000758",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = e.Cell.Tag;
                            return;
                        }

                        if (Convert.ToInt32(e.Cell.Tag) < Convert.ToInt32(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M001135", "M000157",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = e.Cell.Tag;
                            return;
                        }

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;
                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            if (this.uGrid2.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGrid2.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGrid2.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                            {
                                if (
                                    e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGrid2.Rows[i].Cells["CurDurableMatCode"].Value))
                                {

                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGrid2.Rows[i].Cells["InputQty"].Value);

                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Value;

                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000617", "M000659 " + strQty + "M000010", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }
                    }
                }

                #endregion

                #region 교체수량

                if (e.Cell.Column.Key.Equals("ChgQty"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        if (!Information.IsNumeric(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M001135", "M000758",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = 0;
                            return;
                        }

                        if (Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value) < Convert.ToInt32(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M001135", "M000157",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = 0;
                            return;
                        }

                        if (Convert.ToInt32(e.Cell.Row.Cells["ChgInvQty"].Value) < Convert.ToInt32(e.Cell.Value))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001264", "M001135", "재고가용수량보다 더 클수 없습니다.",
                                                         Infragistics.Win.HAlign.Right);
                            e.Cell.Value = 0;
                            return;
                        }

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            if (
                                this.uGrid2.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGrid2.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGrid2.Rows[i].Hidden.Equals(false)
                                && this.uGrid2.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGrid2.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGrid2.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGrid2.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000302", "M000891" + strQty + "M000010", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }

                    }
                }


                if (e.Cell.Column.Key.Equals("ChgDurableMatName"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        brwChannel.mfCredentials(clsEquipDurableBOM);

                        string strChgDurableMatCode = e.Cell.Value.ToString();
                        string strChgLotNo = e.Cell.Row.Cells["ChgLotNo"].Value.ToString();

                        if (strChgDurableMatCode == "")
                            return;

                        DataTable dtEquipDurableBOM = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(this.uTextPlantCode.Text, this.utextEquipCode.Text,m_resSys.GetString("SYS_LANG"));
                        DataRow[] dr = dtEquipDurableBOM.Select("DurableMatCode = '" + strChgDurableMatCode + "' AND LotNo = '" + strChgLotNo + "'");
                        //if (strChgLotNo.Equals(string.Empty))
                        //{
                        //    DataRow[] dr = dtEquipDurableBOM.Select("DurableMatCode = '" + strChgDurableMatCode + "'");
                        //}
                        //else
                        //{
                        //    DataRow[] dr = dtEquipDurableBOM.Select("DurableMatCode = '" + strChgDurableMatCode + "' AND LotNo = '" + strChgLotNo + "'");
                        //}

                        if (!dr.Length.Equals(0))
                        {
                            if (!dr[0]["LotNo"].ToString().Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001264", "M001135", "설비구성품정보에 이미 해당 치공구에 대한 Lot정보가 있습니다.",
                                                             Infragistics.Win.HAlign.Right);
                                e.Cell.Value = "";
                                e.Cell.Row.Cells["ChgLotNo"].Value = "";
                                e.Cell.Row.Cells["ChgQty"].Value = "0";
                                e.Cell.Row.Cells["ChgInvQty"].Value = "0";

                                e.Cell.Row.Cells["ChgDurableMatCode"].Value = "";
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = "";
                                e.Cell.Row.Cells["ChgLotNo"].Value = "";


                                return;
                            }
                            else
                            {
                                if (!strChgLotNo.Equals(string.Empty))
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001264", "M001135", "설비구성품정보에 이미 해당 치공구에 대한 정보가 있습니다.",
                                                             Infragistics.Win.HAlign.Right);
                                    e.Cell.Value = "";
                                    e.Cell.Row.Cells["ChgLotNo"].Value = "";
                                    e.Cell.Row.Cells["ChgQty"].Value = "0";
                                    e.Cell.Row.Cells["ChgInvQty"].Value = "0";

                                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = "";
                                    e.Cell.Row.Cells["ChgDurableMatName"].Value = "";
                                    e.Cell.Row.Cells["ChgLotNo"].Value = "";
                                    return;
                                }

                            }
                        }

                    }
                }


                #endregion



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

               
                
            
        }

        private void uGrid2_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {

            try
            {
                //int intSelectItem = Convert.ToInt32(e.Cell.ValueList.SelectedItemIndex.ToString());

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString()); 

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                
                titleArea.Focus();
                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                #region CurDurableMatCode
                if (e.Cell.Column.Key.ToString() == "CurDurableMatName")
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChannel.mfCredentials(clsDurableMat);

                    string strDurableMatCode = uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value.ToString();
        
                    string strLang = m_resSys.GetString("SYS_LANG");
                    DataTable dtDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_OnlyLot(this.uTextPlantCode.Text, this.utextEquipCode.Text, strLang);

                    if (dtDurableMat.Rows.Count != 0)
                    {
                        //DataRow[] drDurable = dtDurableMat.Select("DurableMatCode = '" + strDurableMatCode + "'");

                        //uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = drDurable[0]["InputQty"];
                        //uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Value = drDurable[0]["InputQty"];
                        //uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = drDurable[0]["LotNo"];
                        //uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = drDurable[0]["DurableMatCode"];
                        //uGrid2.Rows[e.Cell.Row.Index].Cells["UnitCode"].Value = drDurable[0]["UnitCode"];
                        //uGrid2.Rows[e.Cell.Row.Index].Cells["UnitName"].Value = drDurable[0]["UnitName"];

                        //uGrid2.Rows[e.Cell.Row.Index].Cells["UnitName"].Value = dtDurableMat.Rows[intSelectItem]["UnitName"].ToString();
                        uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = dtDurableMat.Rows[intSelectItem]["LotNo"].ToString();
                        uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = dtDurableMat.Rows[intSelectItem]["DurableMatCode"].ToString();

                        uGrid2.Rows[e.Cell.Row.Index].Cells["UnitCode"].Value = dtDurableMat.Rows[intSelectItem]["UnitCode"].ToString();


                        uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();
                        uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = dtDurableMat.Rows[intSelectItem]["InputQty"].ToString();


                        e.Cell.Row.Cells["ChgDurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        if (!dtDurableMat.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                        else
                        {
                            uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        }

                        for (int i = 0; i < uGrid2.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(uGrid2.Rows[i].Cells["CancelFlag"].Value) == false)
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGrid2.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMat.Rows[intSelectItem]["LotNo"].ToString()) &&
                                    !dtDurableMat.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty) &&
                                    uGrid2.Rows[i].Cells["Seq"].Value.ToString() == "0")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                                    //uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;

                                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurLotNo"].Value = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatCode"].Value = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["CurDurableMatName"].Value = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    //e.Cell.Row.Cells["ChgDurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                    return;
                                }
                            }
                        }

                        if (!e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(string.Empty))
                        {
                            SearchChgDurableMat(e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), e.Cell.Row.Index);

                            if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgInvQty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                        }
                        
                    }
                }
                #endregion

                #region ChgDurableInventory
                if (e.Cell.Column.Key.ToString().Equals("ChgDurableInventoryCode"))
                {
                    //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString();
                    string _strLotNo = e.Cell.Row.Cells["ChgLotNo"].Value.ToString();

                    SearchChgDurableMat(_strDurableInventoryCode, e.Cell.Row.Index);

                }
                #endregion

                #region ChgDurableMatCode
                if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatName"))
                {
                    //string _strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string _strDurableInventoryCode = e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString();
                    string _strDurableMatCode = e.Cell.Value.ToString();

                    if (_strDurableMatCode == "")
                        return;
                    string strPlantCode = this.uTextPlantCode.Text;
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    brwChannel.mfCredentials(clsRepairReq);

                    DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.utextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                        return;


                    //금형치공구정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구정보 Detail매서드 호출
                    DataTable dtStock = new DataTable();


                    //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                    string strLotChk = "";

                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                        strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                    dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                            dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                            strLotChk, m_resSys.GetString("SYS_LANG"));
                    

                    if (!dtStock.Rows.Count.Equals(0))
                    {
                        
                        DataRow[] dr = dtStock.Select("DurableMatCode = '" + _strDurableMatCode + "'");

                        ////e.Cell.Row.Cells["ChgLotNo"].Value = dr[0]["LotNo"].ToString();
                        //////e.Cell.Row.Cells["InputQty"].Tag = dr[0]["Qty"].ToString();

                        ////e.Cell.Row.Cells["ChgInvQty"].Tag = dr[0]["Qty"].ToString();
                        ////e.Cell.Row.Cells["ChgInvQty"].Value = dr[0]["Qty"].ToString();

                        ////e.Cell.Row.Cells["ChgDurableMatCode"].Value = dr[0]["DurableMatCode"];
                        if (!e.Cell.Row.Cells["UnitCode"].Value.Equals(dtStock.Rows[intSelectItem]["UnitCode"]))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001135", "선택하신 치공구와 기존BOM에 선택하신 치공구의 단위가 다릅니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectItem]["LotNo"].ToString();
                        e.Cell.Row.Cells["ChgInvQty"].Tag = dtStock.Rows[intSelectItem]["Qty"].ToString();
                        e.Cell.Row.Cells["ChgInvQty"].Value = dtStock.Rows[intSelectItem]["Qty"].ToString();

                        e.Cell.Row.Cells["ChgDurableMatCode"].Value = dtStock.Rows[intSelectItem]["DurableMatCode"].ToString();

                        if (!dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["ChgQty"].Tag = dtStock.Rows[intSelectItem]["Qty"].ToString();
                            e.Cell.Row.Cells["ChgQty"].Value = dtStock.Rows[intSelectItem]["Qty"].ToString();

                            e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                        



                        for (int i = 0; i < uGrid2.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(uGrid2.Rows[i].Cells["CancelFlag"].Value) == false)
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGrid2.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectItem]["LotNo"].ToString()) &&
                                    !dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty) &&
                                    uGrid2.Rows[i].Cells["Seq"].Value.ToString() == "0")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                                    //uGrid2.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                                    //uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = 0;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgQty"].Value = 0;

                                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
                                    uGrid2.Rows[e.Cell.Row.Index].Cells["ChgDurableMatName"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    return;
                                }
                            }
                        }
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        #endregion

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {                        
                            this.uGrid2.Rows[i].Hidden = true;                        
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmDMMZ0010_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 그리드콤보

        /// <summary>
        /// 금형치공구창고 그리드콤보
        /// </summary>
        /// <param name="strPlantCode"></param>
        private void SearChDurableInven(string strPlantCode)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsInventory);
                DataTable dtInventory = clsInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uGrid2.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Layout.ValueLists.Clear();

                wGrid.mfSetGridColumnValueList(uGrid2, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비구성품정보
        /// </summary>
        private void SearchDurableBOM(string strPlantCode,string strEquipCode)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsInventory);
                DataTable dtInventory = clsInventory.mfReadDurableInventoryCombo(this.uTextPlantCode.Text, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(uGrid2, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInventory);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsDurableMat = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtclsDurableMat = clsDurableMat.mfReadEquipDurableBOMCombo_OnlyLot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                this.uGrid2.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                wGrid.mfSetGridColumnValueGridList(uGrid2, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
                    "DurableMatCode,DurableMatName,LotNo,InputQty", "치공구코드,치공구,LOTNO,수량", "DurableMatCode", "DurableMatName", dtclsDurableMat);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        /// <summary>
        /// 재고구성품정보
        /// </summary>
        /// <param name="strInventoryCode"></param>
        /// <param name="intIndex"></param>
        private void SearchChgDurableMat(string strInventoryCode,int intIndex)
        {
            try
            {

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(this.uTextPlantCode.Text, this.utextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                    return;

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                string strLotChk = "";

                if (!this.uGrid2.Rows[intIndex].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    strLotChk = this.uGrid2.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(this.uTextPlantCode.Text, strInventoryCode,
                                                                                dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));


                ////brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                ////QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                ////brwChannel.mfCredentials(clsDurableStock);

                ////DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_OnlyLot(this.uTextPlantCode.Text, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                ////string strCode = "";
                ////if (this.uGrid2.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                ////{
                ////    strCode = "LotNo = ''";
                ////}
                ////else
                ////{
                ////    strCode = "LotNo <> ''";
                ////}

                ////DataRow[] dr = dtStock.Select(strCode);

                ////DataTable dtBind = dtStock.Copy();
                ////dtBind.Clear();

                ////for (int i = 0; i < dr.Length; i++)
                ////{
                ////    DataRow _dr = dtBind.NewRow();
                ////    for (int j = 0; j < _dr.ItemArray.Length; j++)
                ////    {
                ////        _dr[j] = dr[i][j];
                ////    }
                ////    dtBind.Rows.Add(_dr);
                ////}

                //WinGrid wGrid = new WinGrid();
                wGrid.mfSetGridCellValueGridList(uGrid2, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DataValue,
                    "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName", "치공구코드,치공구,LOTNO,수량,단위코드,단위", "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001264", "M001135", "M000299", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region KeyDown

        /// <summary>
        /// 설비 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strEquipCode = this.uTextSearchEquipCode.Text;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquip.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000902",
                                            Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSearchEquipCode.Text = "";
                    this.uTextSearchEquipName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 치공구 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsMat);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strDurableCode = this.uTextSearchDurableMatCode.Text;

                    DataTable dtDurable = clsMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                    if (dtDurable.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000797",
                                                                    Infragistics.Win.HAlign.Right);
                    }

                    else
                    {
                        this.uTextSearchDurableMatName.Text = dtDurable.Rows[0]["DurableMatName"].ToString();
                    }

                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchDurableMatCode.Text = "";
                    this.uTextSearchDurableMatName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 점검자 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uComboPlant.Value.ToString().Equals(""))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000266",
                                                                    Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsuser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsuser);

                        String strPlantCode = this.uComboPlant.Value.ToString();
                        String strUserID = this.uTextSearchUserID.Text;

                        DataTable dtName = clsuser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                        if (dtName.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000615",
                                                                    Infragistics.Win.HAlign.Right);
                            this.uTextSearchUserID.Focus();
                            return;
                        }
                        else
                        {
                            this.uTextSearchUserName.Text = dtName.Rows[0]["UserName"].ToString();
                        }
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchUserID.Text = "";
                    this.uTextSearchUserName.Text = "";
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

    }
}
