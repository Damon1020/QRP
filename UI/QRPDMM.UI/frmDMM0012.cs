﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmDMM0012.cs                                        */
/* 프로그램명   : 정기점검관리                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
namespace QRPDMM.UI
{
    public partial class frmDMM0012 : Form,IToolbar
    {

        //다국어지원
        QRPGlobal Sysres = new QRPGlobal();

        public frmDMM0012()
        {
            InitializeComponent();
        }

        private void frmDMM0012_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0012_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGroupBox();
            InitComboBox();
            InitGrid();
            //InitTree();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMM0012_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd =  new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("정기점검관리", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값설정(로그인유저)
                this.uTextJudgeChargeID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextJudgeChargeName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextChangeUsage.Text = "";
                this.uTextCurUsage.Text = "";
                this.uTextDurableMatCode.Text = "";
                this.uTextDurableMatName.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextEtcDesc.Text = "";
                
                this.uTextPlantName.Text = "";
                this.uTextSearchDurableMatCode.Text = "";
                this.uTextSpec.Text = "";
                this.uTextUsageLimit.Text = "";
                this.uTextUsageLimitLower.Text = "";
                this.uTextUsageLimitUpper.Text = "";
                this.uTextPlantCode.Text = "";
                this.uTextPlantCode.Visible = false;
                this.uTextCumUsage.Text = "";
                this.uTextLotNo.Text = "";
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDurableMat, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelChangeUsage, "교체Shot수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimitLower, "Limit-1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimit, "Limit0", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimitUpper, "Limit+1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCurUsage, "현재Shot수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCumUsage, "누적Shot수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageJudgeCode, "Shot판정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelJudgeChargeID, "Shot판정담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                this.uComboPlant.Items.Clear();
                this.uComboUsageJudgeCode.Items.Clear();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);
                
                DataTable dtCommon = clsCommonCode.mfReadCommonCode("C0041", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboUsageJudgeCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCommon);


                
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid1, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ChangeUsage", "교체Shot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimit", "Limit 0", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CurUsage", "현재Shot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CumUsage", "누적Shot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Shot%", "Shot%", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LastInspectDate", "최종점검일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LimitArrivalDate", "경과일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(uGrid1, 0);
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();
                uGroupBoxContentsArea.Expanded = false;

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.Appearance.FontData.SizeInPoints = 9;
                uGroupBox1.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "점검처리", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.Appearance.FontData.SizeInPoints = 9;
                uGroupBox2.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                WinGrid wGrid = new WinGrid();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableMatCode = this.uTextSearchDurableMatCode.Text;

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new QRPDMM.BL.DMMMGM.InspectHist();
                brwChannel.mfCredentials(clsInspectHist);
                DataTable dtInspectHist = clsInspectHist.mfReadDurableMatInfo(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                this.uGrid1.DataSource = dtInspectHist;
                this.uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtInspectHist.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";

                // 필수입력사항 확인
                if (this.uTextPlantName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001050", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextDurableMatCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001050", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                // 필수입력사항 확인
                if (this.uComboUsageJudgeCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "Shot판정을 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboUsageJudgeCode.DropDown();
                    return;
                }
 

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }



                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);


                //--------- 1. MASDurableLot 테이블에 저장 ----------------------------------------------//
                // 실제 업데이트 되는 값은 LastInspectDate 뿐임. 나머지는 BL에서 에러막기위해 처리
                DataTable dtDurableLot = clsDurableLot.mfSetDatainfo();
                row = dtDurableLot.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["GRDate"] = "";

                row["DiscardFlag"] = "F";
                row["DiscardDate"] = "";
                row["DiscardChargeID"] = "";
                row["DiscardReason"] = "";
                row["ChangeUsage"] = "0";
                row["CurUsage"] = this.uTextCurUsage.Text;
                row["CumUsage"] = "0";
                row["LastInspectDate"] = this.uDateUsageJudgeDate.Value.ToString();
                row["LimitArrivalDate"] = "";
                dtDurableLot.Rows.Add(row);


                //-------- 2.  DMMInspectHist 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new QRPDMM.BL.DMMMGM.InspectHist();
                brwChannel.mfCredentials(clsInspectHist);

                DataTable dtInspectHist = clsInspectHist.mfSetDatainfo();


                row = dtInspectHist.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["UsageDocCode"] = "";
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["UsageJudgeCode"] = this.uComboUsageJudgeCode.Value.ToString();
                row["UsageJudgeDate"] = this.uDateUsageJudgeDate.Value.ToString();
                row["JudgeChargeID"] = this.uTextJudgeChargeID.Text;
                row["CurUsage"] = this.uTextCurUsage.Text;
                row["CumUsage"] = this.uTextCumUsage.Text;
                row["EtcDesc"] = this.uTextEtcDesc.Text;

                dtInspectHist.Rows.Add(row);


                //-------- 3.  DMMRepairH 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                DataTable dtRepairH = clsRepairH.mfSetDatainfo();

                row = dtRepairH.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["RepairCode"] = "";
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["RepairGubunCode"] = "";
                row["UsageDocCode"] = "";
                row["StandbyFlag"] = "F";
                row["StandbyDate"] = "";
                row["RepairFlag"] = "F";
                row["RepairDate"] = "";
                row["FinishFlag"] = "F";
                row["FinishDate"] = "";
                row["RepairResultCode"] = "";
                row["ChangeFlag"] = "F";
                row["EtcDesc"] = "";
                row["UsageClearFlag"] = "F";
                row["UsageClearDate"] = "";
                row["ClearCurUsage"] = "0";
                row["ClearCumUsage"] = "0";

                dtRepairH.Rows.Add(row);

                

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.SAVEMGM), "SAVEMGM");
                QRPDMM.BL.DMMMGM.SAVEMGM clsSAVEMGM = new QRPDMM.BL.DMMMGM.SAVEMGM();
                brwChannel.mfCredentials(clsSAVEMGM);
                string rtMSG = clsSAVEMGM.mfSaveInspectHist(dtDurableLot, dtInspectHist, dtRepairH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                string strRepairCode = ErrRtn.mfGetReturnValue(0);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    

                    if (this.uComboUsageJudgeCode.Value.ToString() == "R")
                    {

                        QRPDMM.UI.frmDMM0014 frmRepairGI = new QRPDMM.UI.frmDMM0014(this.uTextPlantCode.Text, strRepairCode);
                        frmRepairGI.FormName = this.Name;

                        CommonControl cControl = new CommonControl();
                        Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //해당화면의 탭이 있는경우 해당 탭 닫기
                        if (uTabMenu.Tabs.Exists("QRPDMM" + "," + "frmDMM0014"))
                        {
                            uTabMenu.Tabs["QRPDMM" + "," + "frmDMM0014"].Close();
                        }
                        //탭에 추가
                        uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0014", "점검일지등록");
                        uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0014"];

                        //호출시 화면 속성 설정
                        frmRepairGI.AutoScroll = true;
                        frmRepairGI.MdiParent = this.MdiParent;
                        frmRepairGI.ControlBox = false;
                        frmRepairGI.Dock = DockStyle.Fill;
                        frmRepairGI.FormBorderStyle = FormBorderStyle.None;
                        frmRepairGI.WindowState = FormWindowState.Normal;
                        frmRepairGI.Text = "점검일지등록";
                        frmRepairGI.Show();

                        //QRPDMM.UI.frmDMM0014 frmCarryOut = new QRPDMM.UI.frmDMM0014(this.uTextPlantCode.Text, strRepairCode);
                        //frmCarryOut.FormName = this.Name;

                        //CommonControl cControl = new CommonControl();
                        //Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        //Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0014", "점검일지등록");
                        //uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0014"];
                        //frmCarryOut.MdiParent = this.ParentForm;
                        //frmCarryOut.Show();

                    }

                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                
                    this.uGroupBoxContentsArea.Expanded = false;
                 
                InitText();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ultraTextEditor11_ValueChanged(object sender, EventArgs e)
        {

        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();
                string strLotNo = e.Cell.Row.Cells["LotNo"].Value.ToString();

                ////brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                ////QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new QRPDMM.BL.DMMMGM.InspectHist();
                ////brwChannel.mfCredentials(clsInspectHist);
                ////DataTable dtInspectHist = clsInspectHist.mfReadDurableMatInfo(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));
                
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);
                DataTable dtDurableLot = clsDurableLot.mfReadMASDurableMatLot_Detail(strPlantCode, strDurableMatCode, strLotNo, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtDurableLot.Rows.Count; i++)
                {
                    this.uTextPlantCode.Text = dtDurableLot.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtDurableLot.Rows[i]["PlantName"].ToString();
                    this.uTextDurableMatCode.Text = dtDurableLot.Rows[i]["DurableMatCode"].ToString();
                    this.uTextDurableMatName.Text = dtDurableLot.Rows[i]["DurableMatName"].ToString();
                    this.uTextLotNo.Text = dtDurableLot.Rows[i]["LotNo"].ToString();
                    this.uTextSpec.Text = dtDurableLot.Rows[i]["Spec"].ToString();
                    this.uTextEquipCode.Text = dtDurableLot.Rows[i]["EquipCode"].ToString();
                    this.uTextEquipName.Text = dtDurableLot.Rows[i]["EquipName"].ToString();
                    this.uTextChangeUsage.Text = dtDurableLot.Rows[i]["ChangeUsage"].ToString();
                    this.uTextCurUsage.Text = dtDurableLot.Rows[i]["CurUsage"].ToString();
                    this.uTextCumUsage.Text = dtDurableLot.Rows[i]["CumUsage"].ToString();
                    this.uTextUsageLimitLower.Text = dtDurableLot.Rows[i]["UsageLimitLower"].ToString();
                    this.uTextUsageLimit.Text = dtDurableLot.Rows[i]["UsageLimit"].ToString();
                    this.uTextUsageLimitUpper.Text = dtDurableLot.Rows[i]["UsageLimitUpper"].ToString();
                   
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString()=="")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }


                frmPOP0007 frm = new frmPOP0007();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchDurableMatCode.Text = frm.DurableMatCode;
                uTextSearchDurableMatName.Text = frm.DurableMatName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextJudgeChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uTextPlantCode.Text == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    //this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uTextPlantCode.Text;
                frm.ShowDialog();

                uTextJudgeChargeID.Text = frm.UserID;
                uTextJudgeChargeName.Text = frm.UserName;

            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmDMM0012_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextJudgeChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                String strPlantCode = this.uTextPlantCode.Text;

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextJudgeChargeID;
                uTextName = this.uTextJudgeChargeName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        //this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsMat);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strDurableCode = this.uTextSearchDurableMatCode.Text;

                    DataTable dtDurable = clsMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                    if (dtDurable.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000797",
                                                                    Infragistics.Win.HAlign.Right);
                    }

                    else
                    {
                        this.uTextSearchDurableMatName.Text = dtDurable.Rows[0]["DurableMatName"].ToString();
                    }

                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchDurableMatCode.Text = "";
                    this.uTextSearchDurableMatName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
    }
}
