﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMMZ0014.cs                                        */
/* 프로그램명   : 입고확인등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Infragistics.Win.UltraWinGrid;
using Microsoft.VisualBasic;
//파일 업 / 다운로드
using System.IO;
using System.Collections;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0014 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        string m_strSaveCheck = "";

        public frmDMMZ0014()
        {
            InitializeComponent();
        }

        private void frmDMMZ0014_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0014_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정

            titleArea.mfSetLabelText("입고확인등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();
            InitButton();

            //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMMZ0014_FormClosing(object sender, FormClosingEventArgs e)
        {
            //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                //titleArea.mfSetLabelText("입고확인등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                uTextGRConfirmID.Text = m_resSys.GetString("SYS_USERID");
                uTextGRConfirmName.Text = m_resSys.GetString("SYS_USERNAME");
                uTextPONumber.Text = "";
                uTextDurableMatCode.Text = "";
                uTextDurableMatName.Text = "";
                uTextPlantCode.Text = "";
                uTextPlantName.Text = "";
                uTextQty.Text = "";
                this.uTextGRCode.Clear();
                uCheckSerialFlag.CheckedValue = false;
                uCheckSerialFlag.Enabled = false;
                uTextPlantCode.Visible = false;
                uTextPlantName.Visible = false;
                uButtonOK.Visible = false;
                m_strSaveCheck = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboInputPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                SetPlantCode();

                this.uComboInventory.Text = "선택";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void SetPlantCode()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboInputPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelGRConfirmDate, "현장입고일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelGRConfirmID, "입고담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelGRSPInventory, "입고창고", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelPONumber, "PO번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDurableMat, "금형치공구", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInputPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelQty, "수량", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                //SearchPlant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboInputPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

              
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region Header
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, true, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRCode", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRSeq", "입고순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "GRQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "SerialFlag", "Serial", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "F");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region Lot
                //기본설정
                grd.mfInitGeneralGrid(this.uGridDurableGRD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "GRCode", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "GRSeq", "입고순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); 

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "LotNo", "LOT NO", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "RevNo", "RevNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "BtnRevNo", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 20, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "InspectResult", "외관검사(SCOPE 확인)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "BtnSCOPE", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 20, false, false, 1, Infragistics.Win.HAlign.Center,
                   Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "F");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "SizeResult", "치수확인", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "BtnValue", "", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 20, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "FileName", "검사 성적서 첨부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 2000, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableGRD, 0, "DeleteFlag", "DeleteFlag", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");
                #endregion
                // 체크박스 Type 설정
                //this.uGrid1.DisplayLayout.Bands[0].Columns["Check"].DataType = typeof(Boolean);
                //Grid초기화 후 Font크기를 아래와 같이 적용

                #region DropDown Binding
                DataTable dtUseFlag = new DataTable();

                dtUseFlag.Columns.Add("Key", typeof(String));
                dtUseFlag.Columns.Add("Value", typeof(String));

                DataRow row1 = dtUseFlag.NewRow();
                row1["Key"] = "T";
                row1["Value"] = "사용함";
                dtUseFlag.Rows.Add(row1);

                DataRow row2 = dtUseFlag.NewRow();
                row2["Key"] = "F";
                row2["Value"] = "사용안함";
                dtUseFlag.Rows.Add(row2);

                grd.mfSetGridColumnValueList(this.uGrid1, 0, "SerialFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                DataTable dtDropDown = new DataTable();
                dtDropDown.Columns.Add("Key", typeof(string));
                dtDropDown.Columns.Add("Value", typeof(string));

                DataRow drK;

                for (int i = 0; i < 11; i++)
                {
                    drK = dtDropDown.NewRow();
                    if (i < 10)
                    {
                        drK["Key"] = "0" + i.ToString();
                        drK["Value"] = "0" + i.ToString();
                    }
                    else
                    {
                        drK["Key"] = i;
                        drK["Value"] = i;
                    }
                    dtDropDown.Rows.Add(drK);
                }

                grd.mfSetGridColumnValueList(this.uGridDurableGRD, 0, "RevNo", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDropDown);

                dtDropDown.Clear();

                drK = dtDropDown.NewRow();
                drK["Key"] = "OK";
                drK["Value"] = "OK";
                dtDropDown.Rows.Add(drK);

                drK = dtDropDown.NewRow();
                drK["Key"] = "NG";
                drK["Value"] = "NG";
                dtDropDown.Rows.Add(drK);

                grd.mfSetGridColumnValueList(this.uGridDurableGRD, 0, "InspectResult", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDropDown);
                grd.mfSetGridColumnValueList(this.uGridDurableGRD, 0, "SizeResult", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDropDown);

                #endregion

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGroupBoxLot.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                //grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "입고리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                 //   , Infragistics.Win.Misc.GroupBoxBorderStyle.Default, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트지정
                //uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                //uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                uGroupBoxLot.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitValue()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 입고일 From : 시스템의 현재날의 월초
                //DateTime monthFirst = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                this.uDateFromGRDate.Value = DateTime.Now.ToString("yyyy-MM-01");
                // 입고일 To :시스템 현재날자
                this.uDateToGRDate.Value = DateTime.Now;

                // 현장입고일 : 시스템의 현재날자
                this.uDateGRConfirmDate.Value = DateTime.Now;

                // 입고담당자 ID
                this.uTextGRConfirmID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextGRConfirmName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
                wButton.mfSetButton(uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }            
        }
        /// <summary>
        /// 수정 가능(작성시 파일 다운로드는 불가능 하므로 다운로드 버튼 Hidden)
        /// </summary>
        private void SetWritable()
        {
            try
            {
                this.uDateGRConfirmDate.ReadOnly = false;
                this.uDateGRConfirmDate.Appearance.BackColor = Color.PowderBlue;
                this.uTextGRConfirmID.ReadOnly = false;
                this.uTextGRConfirmID.Appearance.BackColor = Color.PowderBlue;
                this.uComboInputPlant.ReadOnly = false;
                this.uComboInputPlant.Appearance.BackColor = Color.PowderBlue;
                this.uTextDurableMatCode.ReadOnly = false;
                this.uTextDurableMatCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextQty.ReadOnly = false;
                this.uTextQty.Appearance.BackColor = Color.PowderBlue;
                this.uComboInventory.ReadOnly = false;
                this.uComboInventory.Appearance.BackColor = Color.PowderBlue;
                this.uTextPONumber.ReadOnly = false;
                this.uTextPONumber.Appearance.BackColor = Color.White;
                this.uButtonDeleteRow.Visible = true;
                
                for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                {
                    this.uGridDurableGRD.Rows[i].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridDurableGRD.Rows[i].Cells["RevNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        /// <summary>
        /// 수정불가(조회시, 그리드 삭제불가, 행삭제버튼 Hidden)
        /// </summary>
        private void SetDisWritable()
        {
            try
            {
                this.uDateGRConfirmDate.ReadOnly = true;
                this.uDateGRConfirmDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextGRConfirmID.ReadOnly = true;
                this.uTextGRConfirmID.Appearance.BackColor = Color.Gainsboro;
                this.uComboInputPlant.ReadOnly = true;
                this.uComboInputPlant.Appearance.BackColor = Color.Gainsboro;
                this.uTextDurableMatCode.ReadOnly = true;
                this.uTextDurableMatCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextQty.ReadOnly = true;
                this.uTextQty.Appearance.BackColor = Color.Gainsboro;
                this.uComboInventory.ReadOnly = true;
                this.uComboInventory.Appearance.BackColor = Color.Gainsboro;
                this.uTextPONumber.ReadOnly = true;
                this.uTextPONumber.Appearance.BackColor = Color.Gainsboro;
                this.uButtonDeleteRow.Visible = false;
                this.uButtonDown.Visible = true;
                for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                {
                    this.uGridDurableGRD.Rows[i].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridDurableGRD.Rows[i].Cells["RevNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

       
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                InitText();
                InitGrid();
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                if (this.uDateFromGRDate.Value == null || this.uDateFromGRDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000215", "M000209", Infragistics.Win.HAlign.Right);

                    this.uDateFromGRDate.DropDown();
                    return;
                }
                if (this.uDateToGRDate.Value == null || this.uDateToGRDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000215", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateToGRDate.DropDown();
                    return;
                }

                if (Convert.ToDateTime(this.uDateFromGRDate.Value).Date > Convert.ToDateTime(this.uDateToGRDate.Value).Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000215", "M000210", Infragistics.Win.HAlign.Right);

                    this.uDateFromGRDate.DropDown();
                    return;
                }


                DialogResult Result = new DialogResult();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //this.uComboInventory.Items.Clear();
                //this.uComboInventory.SelectedItem.DataValue = "";

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromGRDate = this.uDateFromGRDate.Value.ToString();
                string strToDGRDate = this.uDateToGRDate.Value.ToString();



                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableGR), "DurableGR");
                QRPDMM.BL.DMMICP.DurableGR clsDurableGR = new QRPDMM.BL.DMMICP.DurableGR();
                brwChannel.mfCredentials(clsDurableGR);

                // Call Method
                DataTable dtDurableGR = clsDurableGR.mfReadDMMDurableGR(strPlantCode, strFromGRDate, strToDGRDate, "", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtDurableGR;
                uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtDurableGR.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtDurableStock = new DataTable();
                DataTable dtDurableStockMoveHist = new DataTable();
                DataTable dtDurableGR = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();
                int intRowCheck = 0;

                #region 필수입력사항 확인
                if (m_strSaveCheck == "T")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000851", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }
                if (this.uDateGRConfirmDate.Value == null || this.uDateGRConfirmDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M001230", "M001253", Infragistics.Win.HAlign.Right);

                    this.uDateGRConfirmDate.DropDown();
                    return;
                }

                if (this.uTextDurableMatCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000337", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextDurableMatCode.Focus();
                    return;
                }

                ////if (this.uTextDurableMatName.Text == "")
                ////{
                ////    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                    , "M001264", "M001230", "M000337", Infragistics.Win.HAlign.Center);

                ////    // Focus
                ////    this.uTextDurableMatCode.Focus();
                ////    return;
                ////}

                if (this.uTextQty.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000729", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextQty.Focus();
                    return;
                }

                if (!Information.IsNumeric(this.uTextQty.Text))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                           "M001264", "M001230", "M000728", Infragistics.Win.HAlign.Right);
                    this.uTextQty.Text = "";
                    this.uTextQty.Focus();
                    return;
                }

                if (this.uComboInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000870", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInventory.DropDown();
                    return;
                }
                

                // 필수 입력사항 확인

                if (this.uCheckSerialFlag.CheckedValue.Equals(true))
                {
                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {
                        this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];

                        if (this.uGridDurableGRD.Rows[i].Hidden == false)
                        {

                            intRowCheck = intRowCheck + 1;

                            string strRow = this.uGridDurableGRD.Rows[i].RowSelectorNumber.ToString();

                            if (this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", strRow + "M000467", Infragistics.Win.HAlign.Center);
                                this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[i].Cells["LotNo"];
                                this.uGridDurableGRD.PerformAction(UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridDurableGRD.Rows[i].Cells["RevNo"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", strRow + "M000468", Infragistics.Win.HAlign.Center);
                                this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[i].Cells["RevNo"];
                                this.uGridDurableGRD.PerformAction(UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            if (this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", strRow + "M000515", Infragistics.Win.HAlign.Center);
                                this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[i].Cells["InspectResult"];
                                this.uGridDurableGRD.PerformAction(UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            if (this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", strRow + "M000534", Infragistics.Win.HAlign.Center);
                                this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[i].Cells["SizeResult"];
                                this.uGridDurableGRD.PerformAction(UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                        }
                    }

                    if (intRowCheck == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M001239", Infragistics.Win.HAlign.Center);

                        // Focus
                        return;
                    }

                    // 필수 입력사항 확인
                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {
                        this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];

                        if (this.uGridDurableGRD.Rows[i].Hidden == false)
                        {
                            for (int k = i + 1; k < this.uGridDurableGRD.Rows.Count; k++)
                            {
                                if (this.uGridDurableGRD.Rows[k].Hidden == false)
                                {
                                    if (this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString() == this.uGridDurableGRD.Rows[k].Cells["LotNo"].Value.ToString())
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M001119", Infragistics.Win.HAlign.Center);

                                        // Focus
                                        return;
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion

                DialogResult dir = new DialogResult();
                string strAdmit = ""; //저장시 승인여부
                dir = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000944"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.Cancel)
                {
                    return;
                }
                else if (dir == DialogResult.Yes)
                {
                    strAdmit = "Admit";
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //////-------- 1. DMMDurableStock(현재 치공구 재고정보 테이블) 에 저장 -------//
                ////brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                ////QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                ////brwChannel.mfCredentials(clsDurableStock);

                ////dtDurableStock = clsDurableStock.mfSetDatainfo();

                ////for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                ////{
                ////    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                ////    if (this.uGrid1.Rows[i].Hidden == false)
                ////    {
                ////        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                ////        {
                ////            if (uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("F"))
                ////            {
                ////                row = dtDurableStock.NewRow();

                ////                row["PlantCode"] = this.uComboPlant.Value.ToString();
                ////                row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                ////                row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                ////                row["Qty"] = this.uGrid1.Rows[i].Cells["GRQty"].Value.ToString();
                ////                row["LotNo"] = string.Empty;
                ////                row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                ////                dtDurableStock.Rows.Add(row);
                ////            }
                ////            else
                ////            {
                ////                for (int j = 0; j < uGridDurableGRD.Rows.Count; j++)
                ////                {
                ////                    if (uGridDurableGRD.Rows[j].Cells["PlantCode"].Value.ToString().Equals(uComboPlant.Value.ToString()) &&
                ////                        uGridDurableGRD.Rows[j].Cells["DurableMatCode"].Value.ToString().Equals(this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString()) &&
                ////                        uGridDurableGRD.Rows[j].Cells["GRCode"].Value.ToString().Equals(this.uGrid1.Rows[i].Cells["GRCode"].Value.ToString()) &&
                ////                        uGridDurableGRD.Rows[j].Cells["GRSeq"].Value.ToString().Equals(this.uGrid1.Rows[i].Cells["GRSeq"].Value.ToString()) &&
                ////                        uGridDurableGRD.Rows[j].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                ////                    {
                ////                        row = dtDurableStock.NewRow();

                ////                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                ////                        row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                ////                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                ////                        row["Qty"] = "1";
                ////                        row["LotNo"] = uGridDurableGRD.Rows[j].Cells["LotNo"].Value.ToString();
                ////                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                ////                        dtDurableStock.Rows.Add(row);
                ////                    }
                ////                }
                ////            }
                ////        }
                ////    }
                ////}


                #region Columns설정
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                dtDurableStock = clsDurableStock.mfSetDatainfo();

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);
                DataTable dtDurableLot = clsDurableLot.mfSetDatainfo();

                
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableGR), "DurableGR");
                QRPDMM.BL.DMMICP.DurableGR clsDurableGR = new QRPDMM.BL.DMMICP.DurableGR();
                brwChannel.mfCredentials(clsDurableGR);

                dtDurableGR = clsDurableGR.mfSetDatainfo();
                DataTable dtDurableGRD = clsDurableGR.mfSetDataInfo_D();
                #endregion

                //-------- 1.  DMMDurableGR(치공구 입고확인 테이블) 에 저장 -------//
                row = dtDurableGR.NewRow();
                row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                row["GRCode"] = this.uTextGRCode.Text;
                //row["GRSeq"] = this.uGrid1.Rows[i].Cells["GRSeq"].Value.ToString();
                row["GRSeq"] = "1";     //GRSeq는 1로 고정하기로 결정함
                row["GRDate"] = this.uDateGRConfirmDate.Value.ToString();
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;

                if (this.uCheckSerialFlag.CheckedValue.Equals(true))
                    row["GRQty"] = intRowCheck; // 만약 모든 수정을 마치고 this.uTextQty.Text의 숫자를 바꾸고 저장버튼을 누르면 안되기 때문에..
                else
                    row["GRQty"] = this.uTextQty.Text;

                row["UnitCode"] = "EA";             //인위적으로 고정 추후 수정할시
                row["GRConfirmFlag"] = "F";
                row["GRConfirmDate"] = this.uDateGRConfirmDate.Value.ToString();
                row["GRConfirmID"] = this.uTextGRConfirmID.Text;
                row["GRDurableInventoryCode"] = this.uComboInventory.Value.ToString();
                row["PONumber"] = this.uTextPONumber.Text;
                dtDurableGR.Rows.Add(row);


                //-------- 2.  DMMDurableGRD(치공구 입고확인 아이템테이블) 에 저장 -------//
                for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                {
                    this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];
                    if (this.uGridDurableGRD.Rows[i].Hidden == false)
                    {
                        row = dtDurableGRD.NewRow();

                        row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                        row["GRCode"] = this.uGridDurableGRD.Rows[i].Cells["GRCode"].Value.ToString();
                        //row["GRSeq"] = uGridDurableGRD.Rows[i].Cells["GRSeq"].Value.ToString();
                        row["GRSeq"] = "1";     //GRSeq는 1로 고정하기로 결정함
                        row["LotNo"] = this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString();
                        row["RevNo"] = this.uGridDurableGRD.Rows[i].Cells["RevNo"].Value.ToString();
                        row["InspectResult"] = this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Value.ToString();
                        row["SizeResult"] = this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Value.ToString();
                        //row["FileName"] = uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString();

                        //if (!this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.Equals(null) && !this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString().Equals(""))
                        //{
                        if (this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString());
                            row["FileName"] = fileDoc.Name;
                        }
                        else
                        {
                            row["FileName"] = this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString();
                        }
                        //}

                        dtDurableGRD.Rows.Add(row);
                    }
                }


                if (!strAdmit.Equals(string.Empty)) //저장시 승인할 경우 정보저장
                {
                    //-------- 3. DMMDurableStock(현재 치공구 재고정보 테이블) 에 저장 -------//
                    if (this.uCheckSerialFlag.CheckedValue.Equals(true))
                    {
                        for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                        {
                            if (this.uGridDurableGRD.Rows[i].Hidden == false)
                            {
                                row = dtDurableStock.NewRow();

                                row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                                row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                                row["Qty"] = "1";
                                row["UnitCode"] = "EA";     //인위적으로 EA로 고정.. 추후 교체시 수정해야함.
                                row["LotNo"] = this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString();

                                dtDurableStock.Rows.Add(row);
                            }
                        }
                    }
                    else
                    {

                        row = dtDurableStock.NewRow();

                        row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                        row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                        row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        row["Qty"] = this.uTextQty.Text;
                        //row["Qty"] = intRowCheck;  
                        row["UnitCode"] = "EA";     //인위적으로 EA로 고정.. 추후 교체시 수정해야함.
                        row["LotNo"] = "";
                        dtDurableStock.Rows.Add(row);
                    }



                    //-------- 4.  DMMDurableStockMoveHist(현재 치공구 재고이력정보 ) 에 저장 -------//

                    for (int i = 0; i < dtDurableStock.Rows.Count; i++)
                    {
                        row = dtDurableStockMoveHist.NewRow();

                        row["MoveGubunCode"] = "M02";       //입고확인일 경우는 "M02"
                        row["DocCode"] = "";
                        row["MoveDate"] = this.uDateGRConfirmDate.Value.ToString();
                        row["MoveChargeID"] = this.uTextGRConfirmID.Text;
                        row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                        row["DurableInventoryCode"] = dtDurableStock.Rows[i]["DurableInventoryCode"].ToString();
                        row["EquipCode"] = "";
                        row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        row["MoveQty"] = dtDurableStock.Rows[i]["Qty"].ToString();
                        row["UnitCode"] = dtDurableStock.Rows[i]["UnitCode"].ToString();
                        row["LotNo"] = dtDurableStock.Rows[i]["LotNo"].ToString();

                        dtDurableStockMoveHist.Rows.Add(row);

                    }

                    //-------- 5.  MASDurableLot(치공구마스터Lot테이블) 에 저장 -------//
                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        if (this.uGridDurableGRD.Rows[i].Hidden == false)
                        {
                            row = dtDurableLot.NewRow();

                            row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                            row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                            row["LotNo"] = uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString();
                            row["GRDate"] = this.uDateGRConfirmDate.Value.ToString();
                            row["DiscardFlag"] = "F";
                            row["DiscardDate"] = "";
                            row["DiscardChargeID"] = "";
                            row["DiscardReason"] = "";
                            row["ChangeUsage"] = "0";
                            row["CurUsage"] = "0";

                            row["CumUsage"] = "0";
                            row["LastInspectDate"] = "";
                            row["LimitArrivalDate"] = "";
                            dtDurableLot.Rows.Add(row);
                        }
                    }

                }


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 저장함수 호출
                //-------- BL 호출 : 입고정보, 현재고갱신, 재고이동 이력 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);

                //FileServer 연결
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboInputPlant.Value.ToString(), "S02");

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboInputPlant.Value.ToString(), "D0021");

                //치공구입고정보저장매서드 실행
                string rtMSG = clsSAVEDurableStock.mfSaveDurableGR(dtDurableGR, dtDurableGRD, dtDurableStock, dtDurableStockMoveHist, dtDurableLot, strAdmit,m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                //파일명을 만들기 위한 변수
                String strPlantCode = this.uComboInputPlant.Value.ToString();
                String strGRNo = ErrRtn.mfGetReturnValue(0);
                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {
                        if (this.uGridDurableGRD.Rows[i].Hidden == false)
                        {
                            if (this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString());
                                string strUploadFile = fileDoc.DirectoryName + "\\" + strPlantCode + "-" + strGRNo
                                                        + "-" + this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString() + "-" + fileDoc.Name;

                                if (File.Exists(strUploadFile))
                                    File.Delete(strUploadFile);

                                File.Copy(this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile);
                                arrFile.Add(strUploadFile);
                            }
                        }
                    }

                   
                    if (arrFile.Count != 0)
                    {
                        //File UpLoad
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                 dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                 dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                 dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                 dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                    }

                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                else
                {
                    string strMeg = "";
                    if(ErrRtn.ErrMessage.Equals(string.Empty))
                        strMeg = "M000953";
                    else
                        strMeg = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", strMeg,
                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (uGroupBoxLot.Expanded == false)
                    uGroupBoxLot.Expanded = true;

                while (this.uGridDurableGRD.Rows.Count > 0)
                {
                    this.uGridDurableGRD.Rows[0].Delete(false);
                }
                InitText();
                this.uComboInputPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboInventory.Value = "";
                this.uButtonDown.Visible = false;
                SetWritable();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0 && (this.uGridDurableGRD.Rows.Count == 0 || this.uGroupBoxLot.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                if (this.uGrid1.Rows.Count > 0)
                {
                    grd.mfDownLoadGridToExcel(this.uGrid1);
                }

                if (this.uGridDurableGRD.Rows.Count > 0 && this.uGroupBoxLot.Expanded.Equals(true))
                {
                    grd.mfDownLoadGridToExcel(this.uGridDurableGRD);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Event

        private void frmDMMZ0014_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxLot.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxLot.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uGroupBoxLot_ExpandedStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uGroupBoxLot.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    uGroupBoxLot.Location = point;
                    this.uGrid1.Height = 60;

                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        uGridDurableGRD.Rows[i].Hidden = false;
                    }
                }
                else
                {
                    Point point = new Point(0, 825);
                    uGroupBoxLot.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Fixed = false;
                    }
                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        uGridDurableGRD.Rows[i].Hidden = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGroupBoxLot_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxLot.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxLot.Location = point;
                    this.uGroupBoxLot.Height = 50;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxLot.Location = point;
                    this.uGroupBoxLot.Height = 680;

                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Button
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                uGroupBoxLot.Expanded = false;
                //for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                //{
                //    uGridDurableGRD.Rows[i].Hidden = false;
                //}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                int intCount = 0;

                for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                {
                    
                    if (Convert.ToBoolean(this.uGridDurableGRD.Rows[i].Cells["Check"].Value) == true)
                    {
                        uGridDurableGRD.Rows[i].Hidden = true;
                        uGridDurableGRD.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }

                for (int k = 0; k < uGridDurableGRD.Rows.Count; k++)
                {
                    this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];

                    if (this.uGridDurableGRD.Rows[k].Hidden == false)
                    {
                        intCount = intCount + 1;
                    }
                }

                this.uTextQty.Text = Convert.ToString( intCount);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Text

        #region 치공구 텍스트

        /// <summary>
        /// 치공구 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && this.uTextDurableMatCode.ReadOnly.Equals(false))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsMat);

                    String strPlantCode = this.uComboInputPlant.Value.ToString();
                    String strDurableCode = this.uTextDurableMatCode.Text;

                    DataTable dtDurable = clsMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                    if (dtDurable.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000797",
                                                                    Infragistics.Win.HAlign.Right);
                        this.uTextDurableMatName.Clear();

                    }

                    else
                    {
                        this.uTextDurableMatName.Text = dtDurable.Rows[0]["DurableMatName"].ToString();
                    }

                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDurableMatCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextDurableMatName.Text.Equals(string.Empty))
                {
                    this.uTextDurableMatName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDurableMatName_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextDurableMatName.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 채널연결
                    QRPBrowser brwChannel = new QRPBrowser();

                    ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                    // BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsDurableMat);


                    // Call Method
                    DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(this.uComboInputPlant.Value.ToString(), this.uTextDurableMatCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtDurableMat.Rows.Count > 0)
                    {
                        if (dtDurableMat.Rows[0]["SerialFlag"].Equals("T"))
                            this.uCheckSerialFlag.CheckedValue = true;
                        else
                            this.uCheckSerialFlag.CheckedValue = false;

                        if (this.uGridDurableGRD.Rows.Count > 0)
                        {
                            this.uGridDurableGRD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableGRD.Rows.All);
                            this.uGridDurableGRD.DeleteSelectedRows(false);
                        }
                            
                    }



                    ////////WinComboEditor wCombo = new WinComboEditor();



                    ////////string strPlantCode = this.uTextPlantCode.Text;

                    ////////if (strPlantCode == "")
                    ////////    return;

                    ////////////////----------------- SparePart 창고 콤보 불러오기------------/////////////
                    ////////// BL호출
                    ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    ////////QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    ////////brwChannel.mfCredentials(clsDurableInventory);

                    ////////// Call Method
                    ////////DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));



                    ////////wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    ////////    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    ////////    , true, 100, Infragistics.Win.HAlign.Center, "", "", "", "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);

                }
                else
                {
                    if (this.uCheckSerialFlag.Checked)
                        this.uCheckSerialFlag.Checked = false;

                    if (this.uGridDurableGRD.Rows.Count > 0)
                    {
                        this.uGridDurableGRD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableGRD.Rows.All);
                        this.uGridDurableGRD.DeleteSelectedRows(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uButtonDeleteRow.Visible == true && this.uTextDurableMatCode.ReadOnly.Equals(false))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    DialogResult dir = new DialogResult();

                    if (this.uComboInputPlant.Value.ToString() == "")
                    {
                        dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                    , Infragistics.Win.HAlign.Right);
                        this.uComboInputPlant.DropDown();
                        return;

                    }

                    //if (uTextDurableMatCode.Text != "")
                    //{
                    //    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    //                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 변경확인", "입력하시던 정보가 초기화 됩니다. 계속하시겠습니까? "
                    //                                , Infragistics.Win.HAlign.Right);

                    //    if (dir == DialogResult.No)
                    //        return;
                    //}

                    //mfCreate();
                    frmPOP0007 frm = new frmPOP0007();
                    frm.PlantCode = this.uComboInputPlant.Value.ToString();
                    frm.ShowDialog();

                    uTextPlantCode.Text = frm.PlantCode;
                    uTextPlantName.Text = frm.PlantName;
                    uTextDurableMatCode.Text = frm.DurableMatCode;
                    uTextDurableMatName.Text = frm.DurableMatName;
                }
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uTextGRConfirmID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uButtonDeleteRow.Visible == true && this.uTextGRConfirmID.ReadOnly.Equals(false))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                    Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                    uTextID = this.uTextGRConfirmID;
                    uTextName = this.uTextGRConfirmName;

                    String strUserID = uTextID.Text;
                    uTextName.Text = "";

                    if (e.KeyCode == Keys.Enter)
                    {


                        // 공장콤보박스 미선택\시 종료
                        if (strPlantCode == "" && strUserID != "")
                        {

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning,500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000882", "M000266",
                                                Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else if (strPlantCode != "" && strUserID != "")
                        {
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                            QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                            brwChannel.mfCredentials(clsUser);

                            DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                            if (dtUser.Rows.Count > 0)
                            {
                                uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000615",
                                                Infragistics.Win.HAlign.Right);

                                uTextName.Text = "";
                                uTextID.Text = "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextGRConfirmID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uButtonDeleteRow.Visible == true && this.uTextGRConfirmID.ReadOnly.Equals(false))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    DialogResult dir = new DialogResult();

                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                    , Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;

                    }

                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                    frmPOP.ShowDialog();

                    this.uTextGRConfirmID.Text = frmPOP.UserID;
                    this.uTextGRConfirmName.Text = frmPOP.UserName;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 입고물품 총갯수 선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();
            DataRow row;
            try
            {
                //Enter키 처리
                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                {
                    if (!this.uCheckSerialFlag.Checked)
                        return;

                    ////if (!this.uCheckSerialFlag.Checked)
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                    ////            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////           "M001264", "M000882", "M000336", Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}

                    if (this.uTextQty.Text == "")
                        return;

                    if (this.uCheckSerialFlag.CheckedValue.Equals(false))
                        return;

                    int intQty = Convert.ToInt32(this.uTextQty.Text);

                    string strGRCode = "";
                    string strGRSeq = "";

                    DataTable dtLot = new DataTable();
                    dtLot.Columns.Add("PlantCode");
                    dtLot.Columns.Add("GRCode");
                    dtLot.Columns.Add("GRSeq");
                    dtLot.Columns.Add("DurableMatCode");
                    dtLot.Columns.Add("DurableMatName");
                    dtLot.Columns.Add("LotNo");
                    dtLot.Columns.Add("RevNo");
                    dtLot.Columns.Add("InspectResult");
                    dtLot.Columns.Add("SizeResult");
                    dtLot.Columns.Add("FileName");
                    dtLot.Columns.Add("DeleteFlag");
                    dtLot.Columns.Add("BtnRevNo");
                    dtLot.Columns.Add("BtnSCOPE");
                    dtLot.Columns.Add("BtnValue");
                    //기존치공구입고정보가 있는경우 
                    if (this.uGridDurableGRD.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                        {
                            if (strGRCode.Equals(string.Empty))
                            {
                                strGRCode = this.uGridDurableGRD.Rows[i].Cells["GRCode"].Value.ToString();
                                strGRSeq = this.uGridDurableGRD.Rows[i].Cells["GRSeq"].Value.ToString();
                            }
                            //LotNo가 공백이아니고 행삭제 경우가 아닌 정보만 담는다.
                            if (this.uGridDurableGRD.Rows[i].Hidden.Equals(false)
                                && !this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                            {
                                row = dtLot.NewRow();
                                row["PlantCode"] = this.uGridDurableGRD.Rows[i].Cells["PlantCode"].Value;
                                row["GRCode"] = this.uGridDurableGRD.Rows[i].Cells["GRCode"].Value;
                                row["GRSeq"] = this.uGridDurableGRD.Rows[i].Cells["GRSeq"].Value;
                                row["DurableMatCode"] = this.uGridDurableGRD.Rows[i].Cells["DurableMatCode"].Value;
                                row["DurableMatName"] = this.uGridDurableGRD.Rows[i].Cells["DurableMatName"].Value;
                                row["LotNo"] = this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value;
                                row["RevNo"] = this.uGridDurableGRD.Rows[i].Cells["RevNo"].Value;
                                row["InspectResult"] = this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Value;
                                row["SizeResult"] = this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Value;
                                row["FileName"] = this.uGridDurableGRD.Rows[i].Cells["FileName"].Value;
                                row["DeleteFlag"] = "F";


                                dtLot.Rows.Add(row);
                            }
                        }
                    }

                    for (int i = 0; i < intQty; i++)
                    {
                        if (dtLot.Rows.Count.Equals(intQty))
                            break;

                        row = dtLot.NewRow();
                        row["PlantCode"] = this.uComboInputPlant.Value.ToString();
                        row["GRCode"] = strGRCode;
                        row["GRSeq"] = strGRSeq;
                        row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        row["DurableMatName"] = this.uTextDurableMatName.Text;
                        row["LotNo"] = "";
                        row["RevNo"] = "";
                        row["InspectResult"] = "";
                        row["SizeResult"] = "";
                        row["FileName"] = "";
                        row["DeleteFlag"] = "F";


                        dtLot.Rows.Add(row);
                    }
                    //테이터바인드
                    uGridDurableGRD.DataSource = dtLot;
                    uGridDurableGRD.DataBind();
                }
                //숫자만 입력
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        ///// <summary>
        ///// 입고물품 총갯수 선택
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void uTextQty_KeyUp(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
        //        WinGrid wGrid = new WinGrid();
        //        DataRow row;

        //        if (this.uTextDurableMatName.Text != ""
        //            && this.uTextQty.Text != ""
        //            && this.uCheckSerialFlag.CheckedValue.Equals(true)
        //            && Information.IsNumeric(this.uTextQty.Text))
        //         {
        //             int intQty = Convert.ToInt32(this.uTextQty.Text);

        //             if (this.uGridDurableGRD.Rows.Count == 0
        //                 || !this.uGridDurableGRD.Rows.Count.Equals(intQty))
        //             {
        //                 DataTable dtLot = new DataTable();
        //                 dtLot.Columns.Add("PlantCode");
        //                 dtLot.Columns.Add("GRCode");
        //                 dtLot.Columns.Add("GRSeq");
        //                 dtLot.Columns.Add("DurableMatCode");
        //                 dtLot.Columns.Add("DurableMatName");
        //                 dtLot.Columns.Add("LotNo");
        //                 dtLot.Columns.Add("RevNo");
        //                 dtLot.Columns.Add("InspectResult");
        //                 dtLot.Columns.Add("SizeResult");
        //                 dtLot.Columns.Add("FileName");
        //                 dtLot.Columns.Add("DeleteFlag");

        //                 //기존치공구입고정보가 있는경우 
        //                 if (this.uGridDurableGRD.Rows.Count > 0)
        //                 {
        //                     for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
        //                     {
        //                         row = dtLot.NewRow();
        //                         row["PlantCode"] = this.uGridDurableGRD.Rows[i].Cells["PlantCode"].Value;
        //                         row["GRCode"] = this.uGridDurableGRD.Rows[i].Cells["GRCode"].Value;
        //                         row["GRSeq"] = this.uGridDurableGRD.Rows[i].Cells["GRSeq"].Value;
        //                         row["DurableMatCode"] = this.uGridDurableGRD.Rows[i].Cells["DurableMatCode"].Value;
        //                         row["DurableMatName"] = this.uGridDurableGRD.Rows[i].Cells["DurableMatName"].Value;
        //                         row["LotNo"] = this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value;
        //                         row["RevNo"] = this.uGridDurableGRD.Rows[i].Cells["RevNo"].Value;
        //                         row["InspectResult"] = this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Value;
        //                         row["SizeResult"] = this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Value;
        //                         row["FileName"] = this.uGridDurableGRD.Rows[i].Cells["FileName"].Value;
        //                         row["DeleteFlag"] = this.uGridDurableGRD.Rows[i].Cells["DeleteFlag"].Value;


        //                         dtLot.Rows.Add(row);
        //                     }
        //                 }

        //                 for (int i = 0; i < intQty; i++)
        //                 {
        //                     row = dtLot.NewRow();
        //                     row["PlantCode"] = this.uComboInputPlant.Value.ToString();
        //                     row["GRCode"] = "";
        //                     row["GRSeq"] = 1;
        //                     row["DurableMatCode"] = this.uTextDurableMatCode.Text;
        //                     row["DurableMatName"] = this.uTextDurableMatName.Text;
        //                     row["LotNo"] = "";
        //                     row["RevNo"] = "";
        //                     row["InspectResult"] = "";
        //                     row["SizeResult"] = "";
        //                     row["FileName"] = "";
        //                     row["DeleteFlag"] = "F";


        //                     dtLot.Rows.Add(row);
        //                 }
        //                 //테이터바인드
        //                 uGridDurableGRD.DataSource = dtLot;
        //                 uGridDurableGRD.DataBind();
        //             }
        //        }
        //        if (this.uTextQty.Text.Equals(string.Empty) && this.uGridDurableGRD.Rows.Count > 0)
        //        {
        //            this.uGridDurableGRD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableGRD.Rows.All);
        //            this.uGridDurableGRD.DeleteSelectedRows(false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}
        #endregion

        #region Grid
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            try
            {
               

                //string strPlantCode = uGrid1.Rows[e.Cell.Row.Index].Cells["PlantCode"].Value.ToString();
                
                //string strGRCode = uGrid1.Rows[e.Cell.Row.Index].Cells["GRCode"].Value.ToString();
                //string strGRSeq = uGrid1.Rows[e.Cell.Row.Index].Cells["GRSeq"].Value.ToString();

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                string strGRCode = e.Cell.Row.Cells["GRCode"].Value.ToString();
                string strGRSeq = e.Cell.Row.Cells["GRSeq"].Value.ToString();
                
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableGR), "DurableGR");
                QRPDMM.BL.DMMICP.DurableGR clsDurableGR = new QRPDMM.BL.DMMICP.DurableGR();
                brwChannel.mfCredentials(clsDurableGR);

                // Call Method
                DataTable dtDurableGR = clsDurableGR.mfReadDMMDurableGR_Detail(strPlantCode, strGRCode, strGRSeq, m_resSys.GetString("SYS_LANG"));

                if (dtDurableGR.Rows.Count > 0)
                {
                    e.Cell.Row.Fixed = true;

                    // ExtandableGroupBox 설정
                    if (this.uGroupBoxLot.Expanded == false)
                    {
                        this.uGroupBoxLot.Expanded = true;
                    }

                    this.uTextGRCode.Text = strGRCode;

                    for (int i = 0; i < dtDurableGR.Rows.Count; i++)
                    {
                        this.uTextPlantCode.Text = dtDurableGR.Rows[i]["PlantCode"].ToString();
                        this.uTextPlantName.Text = dtDurableGR.Rows[i]["PlantName"].ToString();
                        this.uTextDurableMatCode.Text = dtDurableGR.Rows[i]["DurableMatCode"].ToString();
                        this.uTextDurableMatName.Text = dtDurableGR.Rows[i]["DurableMatName"].ToString();

                        this.uComboInputPlant.Value = dtDurableGR.Rows[i]["PlantCode"].ToString();
                        this.uDateGRConfirmDate.Value = dtDurableGR.Rows[i]["GRConfirmDate"].ToString();
                        this.uTextGRConfirmID.Text = dtDurableGR.Rows[i]["GRConfirmID"].ToString();
                        this.uTextGRConfirmName.Text = dtDurableGR.Rows[i]["GRConfirmName"].ToString();
                        this.uComboInventory.Value = dtDurableGR.Rows[i]["GRDurableInventoryCode"].ToString();
                        this.uTextQty.Text = dtDurableGR.Rows[i]["GRQty"].ToString();
                        this.uTextPONumber.Text = dtDurableGR.Rows[i]["PONumber"].ToString();

                        if (dtDurableGR.Rows[i]["SerialFlag"].ToString() == "T")
                            this.uCheckSerialFlag.Checked = true;
                        else
                            this.uCheckSerialFlag.Checked = false;
                    }


                    DataTable dtDurableGRD = clsDurableGR.mfReadDMMDurableGRD(strPlantCode, strGRCode, strGRSeq, m_resSys.GetString("SYS_LANG"));

                    //테이터바인드
                    uGridDurableGRD.DataSource = dtDurableGRD;
                    uGridDurableGRD.DataBind();

                    //if (dtDurableGRD.Rows.Count > 0)
                    //{
                    //    WinGrid grd = new WinGrid();
                    //    grd.mfSetAutoResizeColWidth(this.uGridDurableGRD, 0);
                    //}
                    
                    if (dtDurableGR.Rows[0]["GRConfirmFlag"].ToString().Equals("T"))
                    {
                        SetDisWritable();
                        m_strSaveCheck = "T";   //저장불가처리
                    }
                    else
                    {
                        SetWritable();


                        this.uDateGRConfirmDate.ReadOnly = true;
                        this.uDateGRConfirmDate.Appearance.BackColor = Color.Gainsboro;
                        this.uComboInputPlant.ReadOnly = true;
                        this.uComboInputPlant.Appearance.BackColor = Color.Gainsboro;
                        this.uTextDurableMatCode.ReadOnly = true;
                        this.uTextDurableMatCode.Appearance.BackColor = Color.Gainsboro;
                        this.uButtonDown.Visible = true;
                        m_strSaveCheck = "";
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableGRD_AfterCellUpdate(object sender, CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableGRD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                if (e.Cell.Column.Key.ToString().Equals("LotNo"))
                {
                    string strLotNo = e.Cell.Value.ToString();

                    if (strLotNo == "")
                        return;

                    // LotNo정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                    QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                    brwChannel.mfCredentials(clsDurableLot);

                    // Call Method
                    DataTable dtDurableLot = clsDurableLot.mfReadDurableLot_Detail(this.uComboInputPlant.Value.ToString(), uTextDurableMatCode.Text, strLotNo, m_resSys.GetString("SYS_LANG"));

                    //기준정보 LotNo 정보유무확인
                    if (dtDurableLot.Rows.Count > 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000079", "M001258", Infragistics.Win.HAlign.Center);

                        // Focus
                        e.Cell.Value = "";
                        return;

                    }

                    //치공구입고정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableGR), "DurableGR");
                    QRPDMM.BL.DMMICP.DurableGR clsDurableGR = new QRPDMM.BL.DMMICP.DurableGR();
                    brwChannel.mfCredentials(clsDurableGR);

                    //치공구입고상세 LotNo정보조회매서드 실행
                    DataTable dtDurableGRLotNo = clsDurableGR.mfReadDMMDurableGRD_LotNo(this.uComboInputPlant.Value.ToString(),e.Cell.Row.Cells["GRCode"].Value.ToString(),e.Cell.Row.Cells["GRSeq"].Value.ToString(),strLotNo);

                    //치공구입고상세 LotNo정보유무 확인
                    if (dtDurableGRLotNo.Rows.Count > 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000079", "M001259", Infragistics.Win.HAlign.Center);

                        // Focus
                        e.Cell.Value = "";
                        return;

                    }


                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        if (!i.Equals(e.Cell.Row.Index) 
                            && this.uGridDurableGRD.Rows[i].Hidden.Equals(false)
                            && this.uGridDurableGRD.Rows[i].Cells["LotNo"].Value.ToString().Equals(e.Cell.Value))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = "";
                            return;
                        }

                    }
                }

            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
        }

        #endregion

        #region Combo

        private void uComboInputPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();



                string strPlantCode = this.uComboInputPlant.Value.ToString();

                if (strPlantCode == "")
                    return;

                this.uComboInventory.Items.Clear();

                ////////----------------- SparePart 창고 콤보 불러오기------------/////////////
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                // Call Method
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));



                wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
        }

        private void uComboInputPlant_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();



                //dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 변경확인", "입력하시던 정보가 초기화 됩니다. 계속하시겠습니까? "
                //                            , Infragistics.Win.HAlign.Right);

                //if (dir == DialogResult.No)
                //{
                //    uComboInputPlant.Value = "";
                //    uTextPlantCode.Focus(); 
                //    return;

                //}


                if (uGroupBoxLot.Expanded == false)
                    uGroupBoxLot.Expanded = true;

                while (this.uGridDurableGRD.Rows.Count > 0)
                {
                    this.uGridDurableGRD.Rows[0].Delete(false);
                }
                InitText();

                //frmPOP0007 frm = new frmPOP0007();
                //frm.PlantCode = this.uComboPlant.Value.ToString();
                //frm.ShowDialog();

                //uTextPlantCode.Text = frm.PlantCode;
                //uTextPlantName.Text = frm.PlantName;
                //uTextDurableMatCode.Text = frm.DurableMatCode;
                //uTextDurableMatName.Text = frm.DurableMatName;

            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 파일관련
        /// <summary>
        /// 셀버튼 클릭시 파일 이름 가져오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridDurableGRD_ClickCellButton(object sender, CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FileName"))
                {
                    if (this.uButtonDeleteRow.Visible == true)
                    {
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        openFile.Filter = "All files (*.*)|*.*";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string strImageFile = openFile.FileName;
                            e.Cell.Value = strImageFile;

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        }
                    }
                }

                #region 복사버튼
                if (e.Cell.Column.Key.Equals("BtnSCOPE") && e.Cell.Row.Cells["InspectResult"].Activation == Activation.AllowEdit)
                {
                    string strSCOPE = e.Cell.Row.Cells["InspectResult"].Value.ToString();

                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {

                        if (!i.Equals(e.Cell.Row.Index) 
                            && i > e.Cell.Row.Index)
                        {
                            this.uGridDurableGRD.Rows[i].Cells["InspectResult"].Value = strSCOPE;
                        }
                    }
                }


                if (e.Cell.Column.Key.Equals("BtnValue") && e.Cell.Row.Cells["SizeResult"].Activation == Activation.AllowEdit)
                {
                    string strValue = e.Cell.Row.Cells["SizeResult"].Value.ToString();

                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {

                        if (!i.Equals(e.Cell.Row.Index)
                            && i > e.Cell.Row.Index)
                        {
                            this.uGridDurableGRD.Rows[i].Cells["SizeResult"].Value = strValue;
                        }
                    }
                }

                if (e.Cell.Column.Key.Equals("BtnRevNo") && e.Cell.Row.Cells["RevNo"].Activation == Activation.AllowEdit)
                {
                    string strRevNo = e.Cell.Row.Cells["RevNo"].Value.ToString();

                    for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                    {

                        if (!i.Equals(e.Cell.Row.Index)
                            && i > e.Cell.Row.Index)
                        {
                            this.uGridDurableGRD.Rows[i].Cells["RevNo"].Value = strRevNo;
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 파일 셀 더블클릭시 파일 다운로드 && 보여주기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridDurableGRD_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.Key.Equals("FileName"))
                {
                    if (e.Cell.Value.ToString() == "" || e.Cell.Value.ToString().Contains(":\\") == true)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000357",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0021");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    if (e.Cell.Value.ToString().Contains(":\\") == false)
                    {
                        arrFile.Add(e.Cell.Value.ToString());
                    }

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();



                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        /// <summary>
        /// 다운로드 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDurableGRD.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboInputPlant.Value.ToString(), "S02");

                        //저장된 폴더 경로 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboInputPlant.Value.ToString(), "D0021");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGridDurableGRD.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridDurableGRD.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGridDurableGRD.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        #endregion

        #endregion

    }
}
