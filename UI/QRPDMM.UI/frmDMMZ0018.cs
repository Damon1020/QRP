﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMMZ0018.cs                                        */
/* 프로그램명   : 실사비교확정                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0018 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();
        string m_strPlantCode = "";
        string m_strStockTakeCode = "";

        public frmDMMZ0018()
        {
            InitializeComponent();
        }

        private void frmDMMZ0018_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0018_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
            uGroupBoxContentsArea.Expanded = false;
        }
        private void frmDMMZ0018_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method

        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("실사비교확정", m_resSys.GetString("SYS_FONTNAME"), 12);

                //버튼설정
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                this.uButtonDeleteRow.Visible = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextStockTakeConfirmID.Text = m_resSys.GetString("SYS_USERID");
                uTextStockTakeConfirmName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uButtonDeleteRow.Visible = false;

                m_strPlantCode = "";
                m_strStockTakeCode = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //Resource SystemInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //함수호출
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDurableInventory, "창고", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelStockTakeYear, "실사년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStockTakeMonth, "실사월", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelStockTakeConfirmDate, "실사확정일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeConfirmID, "실사확정담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel3, "실사치공구리스트", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                //자재정보
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //컬럼설정
                //자재정보
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeCode", "실사문서번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryCode", "실사창고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryName", "실사창고명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeYear", "실사년도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeMonth", "실사월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeDate", "실사등록일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 15, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeChargeID", "실사담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeChargeName", "실사담당자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeEtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGrid1, 0);


                //실사자재리스트
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //실사자재리스트
                grd.mfSetGridColumn(this.uGrid2, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "StockTakeCode", "실사문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableInventoryCode", "실사창고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableInventoryName", "실사창고명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableMatCode", "DurableMat코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableMatName", "DurableMat명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "LotNo", "LotNo명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "CurStockQty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid2, 0, "StockTakeQty", "실사량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                   Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "StockConfirmQty", "실사확정량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //grd.mfAddRowGrid(this.uGrid2, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataRow row;
                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboDurableInventory.Items.Clear();
                //this.uComboDurableInventory.Text = "";



                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("Year", typeof(String));
                dtYear.Columns.Add("YearText", typeof(String));

                DataTable dtMonth = new DataTable();
                dtMonth.Columns.Add("Month", typeof(String));
                dtMonth.Columns.Add("MonthText", typeof(String));

                for (int i = 2011; i < 2022; i++)
                {
                    row = dtYear.NewRow();
                    row["Year"] = i;
                    row["YearText"] = i;
                    dtYear.Rows.Add(row);
                }

                for (int i = 1; i < 13; i++)
                {
                    row = dtMonth.NewRow();
                    if (i < 10)
                    {
                        row["Month"] = "0" + i;
                        row["MonthText"] = "0" + i;

                    }
                    else
                    {
                        row["Month"] = i;
                        row["MonthText"] = i;
                    }
                    dtMonth.Rows.Add(row);
                }

                wCombo.mfSetComboEditor(this.uComboStockTakeYear, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "Year", "YearText", dtYear);

                wCombo.mfSetComboEditor(this.uComboStockTakeMonth, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "Month", "MonthText", dtMonth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                string strPlantCode = "";
                string strDurableInventoryCode = "";
                string strStockTakeYear = "";
                string strStockTakeMonth = "";

                strPlantCode = this.uComboPlant.Value.ToString();

                if (this.uComboDurableInventory.Value != null)
                    strDurableInventoryCode = this.uComboDurableInventory.Value.ToString();
                else
                    strDurableInventoryCode = "";

                strStockTakeYear = this.uComboStockTakeYear.Value.ToString();
                strStockTakeMonth = this.uComboStockTakeMonth.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeH), "DurableStockTakeH");
                //QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new QRPDMM.BL.DMMICP.DurableStockTakeH();
                //brwChannel.mfCredentials(clsDurableStockTakeH);


                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeH), "DurableStockTakeH");
                QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new QRPDMM.BL.DMMICP.DurableStockTakeH();
                brwChannel.mfCredentials(clsDurableStockTakeH);

                DataTable dtDurableStockTakeH = clsDurableStockTakeH.mfReadDMMDurableStockTakeH(strPlantCode, strDurableInventoryCode, strStockTakeYear, strStockTakeMonth, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtDurableStockTakeH;
                uGrid1.DataBind();


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtDurableStockTakeH.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                int intStockConfirmQty = 0;
                int intCurStockQty = 0;
                int intCheckQty = 0;
                // 필수입력사항 확인
                if (m_strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001043", Infragistics.Win.HAlign.Center);
                    return;
                }

                if (m_strStockTakeCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001043", Infragistics.Win.HAlign.Center);
                    return;
                }

                if (this.uDateStockTakeConfirmDate.Value == null || this.uDateStockTakeConfirmDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000792", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateStockTakeConfirmDate.Focus();
                    return;
                }

                if (this.uTextStockTakeConfirmID.Text == "" || this.uTextStockTakeConfirmName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000791", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextStockTakeConfirmID.Focus();
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");

                if(this.uGrid2.Rows.Count > 0)
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGrid2.Rows[i].Cells["StockConfirmQty"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M00120",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000514",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid2.ActiveCell = this.uGrid1.Rows[i].Cells["StockConfirmQty"];
                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString() != "")
                        {
                            intCheckQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["StockConfirmQty"].Value.ToString());

                            if (intCheckQty > 1)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M00120", strLang)
                                            , this.uGrid2.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000465",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["StockConfirmQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001019", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeH), "DurableStockTakeH");
                QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new QRPDMM.BL.DMMICP.DurableStockTakeH();
                brwChannel.mfCredentials(clsDurableStockTakeH);

                //--------- 1. DMMDurableStockTakeH 테이블에 저장 ----------------------------------------------//
                DataTable dtDurableStockTakeH = clsDurableStockTakeH.mfSetDatainfo();

                row = dtDurableStockTakeH.NewRow();
                row["PlantCode"] = m_strPlantCode;
                row["StockTakeCode"] = m_strStockTakeCode;
                row["StockTakeConfirmDate"] = this.uDateStockTakeConfirmDate.Value.ToString();
                row["StockTakeConfirmID"] = this.uTextStockTakeConfirmID.Text;
                row["ConfirmEtcDesc"] = "";

                dtDurableStockTakeH.Rows.Add(row);



                //-------- 2. DMMDurableStockTakeD 테이블에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeD), "DurableStockTakeD");
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new QRPDMM.BL.DMMICP.DurableStockTakeD();
                brwChannel.mfCredentials(clsDurableStockTakeD);

                DataTable dtDurableStockTakeD = clsDurableStockTakeD.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {

                        row = dtDurableStockTakeD.NewRow();

                        row["PlantCode"] = this.uGrid2.Rows[i].Cells["PlantCode"].Value.ToString();
                        row["StockTakeCode"] = this.uGrid2.Rows[i].Cells["StockTakeCode"].Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid2.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                        row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();

                        row["CurStockQty"] = this.uGrid2.Rows[i].Cells["CurStockQty"].Value.ToString();
                        row["StockTakeQty"] = this.uGrid2.Rows[i].Cells["StockTakeQty"].Value.ToString();
                        row["StockConfirmQty"] = this.uGrid2.Rows[i].Cells["StockConfirmQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStockTakeD.Rows.Add(row);

                    }
                }

                //-------- 3.  DMMDurableStock(현재 DurableMat 재고정보 테이블) 에 저장 : 확정량 - 갱신량 만큼 처리 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                DataTable dtDurableStock = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        row = dtDurableStock.NewRow();
                        intStockConfirmQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["StockConfirmQty"].Value.ToString());
                        intCurStockQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["CurStockQty"].Value.ToString());

                        row["PlantCode"] = this.uGrid2.Rows[i].Cells["PlantCode"].Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid2.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                        row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();
                        row["Qty"] = intStockConfirmQty - intCurStockQty;
                        row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStock.Rows.Add(row);
                    }
                }


                //-------- 4.  DMMDurableStockMoveHist(재고이력 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                DataTable dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        row = dtDurableStockMoveHist.NewRow();

                        intStockConfirmQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["StockConfirmQty"].Value.ToString());
                        intCurStockQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["CurStockQty"].Value.ToString());

                        row["MoveGubunCode"] = "M07";       //자재재고실사일 경우는 "M07"
                        row["DocCode"] = this.uGrid2.Rows[i].Cells["StockTakeCode"].Value.ToString();
                        row["MoveDate"] = this.uDateStockTakeConfirmDate.Value.ToString();
                        row["MoveChargeID"] = this.uTextStockTakeConfirmID.Text;
                        row["PlantCode"] = this.uGrid2.Rows[i].Cells["PlantCode"].Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid2.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                        row["EquipCode"] = "";
                        row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();
                        row["MoveQty"] = intStockConfirmQty - intCurStockQty;
                        row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStockMoveHist.Rows.Add(row);
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : DurableMat 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveStockTake_Confirm(dtDurableStockTakeH, dtDurableStockTakeD, dtDurableStock, dtDurableStockMoveHist, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitText();
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //Systesm ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (uGrid1.Rows.Count == 0 && (this.uGrid2.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500,500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001173", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }
                WinGrid grd = new WinGrid();
                if(this.uGrid1.Rows.Count > 0)
                {
                    grd.mfDownLoadGridToExcel(this.uGrid1);
                }
                

                if(this.uGrid2.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    grd.mfDownLoadGridToExcel(this.uGrid2);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        // Contents GroupBox 펼침상태 변화 이벤트

        

        private void uComboPlant_AfterCloseUp(object sender, EventArgs e)
        {
            
        }

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정이 일어나면 RowSelector Image 설정하는 구문

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정이 일어나면 RowSelector Image 설정하는 구문

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }


        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 135);
                    uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 40;
                }
                else
                {
                    Point point = new Point(0, 825);
                    uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                if (uGroupBoxContentsArea.Expanded == false)
                {
                    uGroupBoxContentsArea.Expanded = true;
                }
                e.Cell.Row.Fixed = true;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strStockTakeCode = e.Cell.Row.Cells["StockTakeCode"].Value.ToString();

                m_strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                m_strStockTakeCode = e.Cell.Row.Cells["StockTakeCode"].Value.ToString();

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeD), "DurableStockTakeD");
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new QRPDMM.BL.DMMICP.DurableStockTakeD();
                brwChannel.mfCredentials(clsDurableStockTakeD);

                DataTable dtDurableStockTakeD = clsDurableStockTakeD.mfReadDMMDurableStockTakeD_Confirm(strPlantCode, strStockTakeCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid2.DataSource = dtDurableStockTakeD;
                uGrid2.DataBind();




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitText();
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                this.uComboDurableInventory.Items.Clear();

                if (strPlantCode == "")
                    return;


                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                ////wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "DurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);


                wCombo.mfSetComboEditor(this.uComboDurableInventory, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
    }
}
