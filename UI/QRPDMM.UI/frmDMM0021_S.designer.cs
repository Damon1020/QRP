﻿namespace QRPDMM.UI
{
    partial class frmDMM0021_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0021));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateToTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFromTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchTransferDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableTransferH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxExpand = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextTransferCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTransferCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextTransferChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTransferChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateTransferDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelTransferChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTransferDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxExpand)).BeginInit();
            this.uGroupBoxExpand.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTransferDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTransferDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uDateToTransferDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToTransferDate.Appearance = appearance4;
            this.uDateToTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToTransferDate.Location = new System.Drawing.Point(500, 12);
            this.uDateToTransferDate.Name = "uDateToTransferDate";
            this.uDateToTransferDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToTransferDate.TabIndex = 5;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(480, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(20, 16);
            this.ultraLabel1.TabIndex = 4;
            this.ultraLabel1.Text = "~";
            // 
            // uDateFromTransferDate
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromTransferDate.Appearance = appearance17;
            this.uDateFromTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromTransferDate.Location = new System.Drawing.Point(380, 12);
            this.uDateFromTransferDate.Name = "uDateFromTransferDate";
            this.uDateFromTransferDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromTransferDate.TabIndex = 3;
            // 
            // uLabelSearchTransferDate
            // 
            this.uLabelSearchTransferDate.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchTransferDate.Name = "uLabelSearchTransferDate";
            this.uLabelSearchTransferDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTransferDate.TabIndex = 2;
            this.uLabelSearchTransferDate.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGridDurableTransferH
            // 
            this.uGridDurableTransferH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableTransferH.DisplayLayout.Appearance = appearance5;
            this.uGridDurableTransferH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableTransferH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferH.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGridDurableTransferH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferH.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridDurableTransferH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableTransferH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableTransferH.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableTransferH.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridDurableTransferH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableTransferH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferH.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableTransferH.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridDurableTransferH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableTransferH.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferH.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridDurableTransferH.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridDurableTransferH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableTransferH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableTransferH.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridDurableTransferH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableTransferH.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.uGridDurableTransferH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableTransferH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableTransferH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableTransferH.Location = new System.Drawing.Point(0, 80);
            this.uGridDurableTransferH.Name = "uGridDurableTransferH";
            this.uGridDurableTransferH.Size = new System.Drawing.Size(1070, 690);
            this.uGridDurableTransferH.TabIndex = 3;
            this.uGridDurableTransferH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableTransferH_DoubleClickCell);
            // 
            // uGroupBoxExpand
            // 
            this.uGroupBoxExpand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxExpand.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxExpand.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxExpand.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxExpand.Name = "uGroupBoxExpand";
            this.uGroupBoxExpand.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxExpand.TabIndex = 4;
            this.uGroupBoxExpand.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel6);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTransferChargeName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateTransferDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferChargeID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTransferDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridDurableTransferD);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextTransferCode
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferCode.Appearance = appearance67;
            this.uTextTransferCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferCode.Location = new System.Drawing.Point(356, 12);
            this.uTextTransferCode.Name = "uTextTransferCode";
            this.uTextTransferCode.ReadOnly = true;
            this.uTextTransferCode.Size = new System.Drawing.Size(100, 21);
            this.uTextTransferCode.TabIndex = 114;
            // 
            // uLabelTransferCode
            // 
            this.uLabelTransferCode.Location = new System.Drawing.Point(252, 12);
            this.uLabelTransferCode.Name = "uLabelTransferCode";
            this.uLabelTransferCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferCode.TabIndex = 113;
            this.uLabelTransferCode.Text = "ultraLabel3";
            // 
            // uComboPlant
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance31;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(120, 19);
            this.uComboPlant.TabIndex = 112;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uButtonDeleteRow
            // 
            appearance3.FontData.BoldAsString = "True";
            this.uButtonDeleteRow.Appearance = appearance3;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(116, 88);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 111;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextTransferChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTransferChargeID.Appearance = appearance15;
            this.uTextTransferChargeID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTransferChargeID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance35;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTransferChargeID.ButtonsRight.Add(editorButton1);
            this.uTextTransferChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTransferChargeID.Location = new System.Drawing.Point(356, 36);
            this.uTextTransferChargeID.MaxLength = 20;
            this.uTextTransferChargeID.Name = "uTextTransferChargeID";
            this.uTextTransferChargeID.Size = new System.Drawing.Size(100, 19);
            this.uTextTransferChargeID.TabIndex = 110;
            this.uTextTransferChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTransferChargeID_KeyDown);
            this.uTextTransferChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTransferChargeID_EditorButtonClick);
            // 
            // uLabelPlant1
            // 
            this.uLabelPlant1.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant1.Name = "uLabelPlant1";
            this.uLabelPlant1.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant1.TabIndex = 109;
            this.uLabelPlant1.Text = "ultraLabel9";
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 92);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 108;
            this.uLabel7.Text = "ultraLabel7";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextEtcDesc.MaxLength = 1000;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(444, 21);
            this.uTextEtcDesc.TabIndex = 107;
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(12, 60);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(100, 20);
            this.uLabel6.TabIndex = 106;
            this.uLabel6.Text = "ultraLabel6";
            // 
            // uTextTransferChargeName
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferChargeName.Appearance = appearance34;
            this.uTextTransferChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTransferChargeName.Location = new System.Drawing.Point(460, 36);
            this.uTextTransferChargeName.Name = "uTextTransferChargeName";
            this.uTextTransferChargeName.ReadOnly = true;
            this.uTextTransferChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextTransferChargeName.TabIndex = 105;
            // 
            // uDateTransferDate
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTransferDate.Appearance = appearance16;
            this.uDateTransferDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTransferDate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateTransferDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTransferDate.Location = new System.Drawing.Point(116, 36);
            this.uDateTransferDate.Name = "uDateTransferDate";
            this.uDateTransferDate.Size = new System.Drawing.Size(100, 19);
            this.uDateTransferDate.TabIndex = 104;
            // 
            // uLabelTransferChargeID
            // 
            this.uLabelTransferChargeID.Location = new System.Drawing.Point(252, 36);
            this.uLabelTransferChargeID.Name = "uLabelTransferChargeID";
            this.uLabelTransferChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferChargeID.TabIndex = 103;
            this.uLabelTransferChargeID.Text = "ultraLabel3";
            // 
            // uLabelTransferDate
            // 
            this.uLabelTransferDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelTransferDate.Name = "uLabelTransferDate";
            this.uLabelTransferDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelTransferDate.TabIndex = 102;
            this.uLabelTransferDate.Text = "ultraLabel4";
            // 
            // uGridDurableTransferD
            // 
            this.uGridDurableTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableTransferD.DisplayLayout.Appearance = appearance21;
            this.uGridDurableTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.uGridDurableTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableTransferD.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridDurableTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableTransferD.DisplayLayout.Override.RowAppearance = appearance33;
            this.uGridDurableTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridDurableTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableTransferD.Location = new System.Drawing.Point(12, 120);
            this.uGridDurableTransferD.Name = "uGridDurableTransferD";
            this.uGridDurableTransferD.Size = new System.Drawing.Size(1044, 548);
            this.uGridDurableTransferD.TabIndex = 16;
            this.uGridDurableTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableTransferD_AfterCellUpdate);
            this.uGridDurableTransferD.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableTransferD_CellListSelect);
            this.uGridDurableTransferD.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableTransferD_AfterCellListCloseUp);
            this.uGridDurableTransferD.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableTransferD_ClickCellButton);
            this.uGridDurableTransferD.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridDurableTransferD_BeforeCellListDropDown);
            // 
            // frmDMM0021_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxExpand);
            this.Controls.Add(this.uGridDurableTransferH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0021_S";
            this.Load += new System.EventHandler(this.frmDMM0021_Load);
            this.Activated += new System.EventHandler(this.frmDMM0021_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMM0021_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxExpand)).EndInit();
            this.uGroupBoxExpand.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTransferChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTransferDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToTransferDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromTransferDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTransferDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableTransferH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxExpand;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableTransferD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferCode;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant1;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTransferChargeName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTransferDate;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelTransferDate;
    }
}