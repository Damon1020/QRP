﻿namespace QRPDMM.UI
{
    partial class frmDMM0025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0025));
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor21 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor19 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor17 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.ultraButton1);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor1);
            this.uGroupBox2.Controls.Add(this.uGrid1);
            this.uGroupBox2.Controls.Add(this.uLabel17);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor21);
            this.uGroupBox2.Controls.Add(this.uLabel18);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor19);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 220);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1060, 558);
            this.uGroupBox2.TabIndex = 12;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(12, 60);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(88, 28);
            this.ultraButton1.TabIndex = 41;
            this.ultraButton1.Text = "ultraButton1";
            // 
            // ultraDateTimeEditor1
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.Appearance = appearance5;
            this.ultraDateTimeEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(116, 28);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 40;
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance24;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance31;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance32;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance33.BackColor2 = System.Drawing.SystemColors.Control;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance33;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance34;
            appearance35.BackColor = System.Drawing.SystemColors.Highlight;
            appearance35.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance35;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance36;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            appearance37.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance37;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance38.BackColor = System.Drawing.SystemColors.Control;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance38;
            appearance39.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance39;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance40;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance41;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 68);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1040, 474);
            this.uGrid1.TabIndex = 39;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(12, 28);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(100, 20);
            this.uLabel17.TabIndex = 36;
            this.uLabel17.Text = "17";
            // 
            // ultraTextEditor21
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor21.Appearance = appearance15;
            this.ultraTextEditor21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor21.Location = new System.Drawing.Point(480, 28);
            this.ultraTextEditor21.Name = "ultraTextEditor21";
            this.ultraTextEditor21.ReadOnly = true;
            this.ultraTextEditor21.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor21.TabIndex = 35;
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(272, 28);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(100, 20);
            this.uLabel18.TabIndex = 14;
            this.uLabel18.Text = "18";
            // 
            // ultraTextEditor19
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor19.Appearance = appearance6;
            this.ultraTextEditor19.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance1;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor19.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor19.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor19.Location = new System.Drawing.Point(376, 28);
            this.ultraTextEditor19.Name = "ultraTextEditor19";
            this.ultraTextEditor19.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor19.TabIndex = 8;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uComboPlant);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor17);
            this.uGroupBox1.Controls.Add(this.uLabel15);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor16);
            this.uGroupBox1.Controls.Add(this.uLabel14);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor15);
            this.uGroupBox1.Controls.Add(this.uLabel13);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor14);
            this.uGroupBox1.Controls.Add(this.uLabel12);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor13);
            this.uGroupBox1.Controls.Add(this.uLabel11);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor12);
            this.uGroupBox1.Controls.Add(this.uLabel10);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor11);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor10);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor9);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor8);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor7);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor6);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor5);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor4);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor3);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1060, 106);
            this.uGroupBox1.TabIndex = 11;
            // 
            // uComboPlant
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance2;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 36;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 35;
            this.uLabelPlant.Text = "Plant";
            // 
            // ultraTextEditor17
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Appearance = appearance4;
            this.ultraTextEditor17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Location = new System.Drawing.Point(116, 148);
            this.ultraTextEditor17.Name = "ultraTextEditor17";
            this.ultraTextEditor17.ReadOnly = true;
            this.ultraTextEditor17.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor17.TabIndex = 34;
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(12, 148);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(100, 20);
            this.uLabel15.TabIndex = 33;
            this.uLabel15.Text = "15";
            // 
            // ultraTextEditor16
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Appearance = appearance21;
            this.ultraTextEditor16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Location = new System.Drawing.Point(376, 124);
            this.ultraTextEditor16.Name = "ultraTextEditor16";
            this.ultraTextEditor16.ReadOnly = true;
            this.ultraTextEditor16.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor16.TabIndex = 32;
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(272, 124);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(100, 20);
            this.uLabel14.TabIndex = 31;
            this.uLabel14.Text = "14";
            // 
            // ultraTextEditor15
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Appearance = appearance22;
            this.ultraTextEditor15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Location = new System.Drawing.Point(116, 124);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.ReadOnly = true;
            this.ultraTextEditor15.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor15.TabIndex = 30;
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(12, 124);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(100, 20);
            this.uLabel13.TabIndex = 29;
            this.uLabel13.Text = "13";
            // 
            // ultraTextEditor14
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Appearance = appearance10;
            this.ultraTextEditor14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Location = new System.Drawing.Point(116, 100);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.ReadOnly = true;
            this.ultraTextEditor14.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor14.TabIndex = 28;
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(12, 100);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(100, 20);
            this.uLabel12.TabIndex = 27;
            this.uLabel12.Text = "12";
            // 
            // ultraTextEditor13
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Appearance = appearance11;
            this.ultraTextEditor13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Location = new System.Drawing.Point(376, 100);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.ReadOnly = true;
            this.ultraTextEditor13.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor13.TabIndex = 26;
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(272, 100);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(100, 20);
            this.uLabel11.TabIndex = 25;
            this.uLabel11.Text = "11";
            // 
            // ultraTextEditor12
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Appearance = appearance12;
            this.ultraTextEditor12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Location = new System.Drawing.Point(376, 76);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.ReadOnly = true;
            this.ultraTextEditor12.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor12.TabIndex = 24;
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(272, 76);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(100, 20);
            this.uLabel10.TabIndex = 23;
            this.uLabel10.Text = "10";
            // 
            // ultraTextEditor11
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance13;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(632, 124);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 22;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(528, 124);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(100, 20);
            this.uLabel9.TabIndex = 21;
            this.uLabel9.Text = "9";
            // 
            // ultraTextEditor10
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Appearance = appearance14;
            this.ultraTextEditor10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Location = new System.Drawing.Point(116, 76);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.ReadOnly = true;
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 20;
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 76);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(100, 20);
            this.uLabel8.TabIndex = 19;
            this.uLabel8.Text = "8";
            // 
            // ultraTextEditor9
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance26;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(376, 52);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 18;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(272, 52);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 17;
            this.uLabel7.Text = "7";
            // 
            // ultraTextEditor8
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance16;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(632, 100);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor8.TabIndex = 16;
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(528, 100);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(100, 20);
            this.uLabel6.TabIndex = 15;
            this.uLabel6.Text = "6";
            // 
            // ultraTextEditor7
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance17;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(632, 76);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor7.TabIndex = 14;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(528, 76);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(100, 20);
            this.uLabel5.TabIndex = 13;
            this.uLabel5.Text = "5";
            // 
            // ultraTextEditor6
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Appearance = appearance18;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Location = new System.Drawing.Point(116, 52);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.ReadOnly = true;
            this.ultraTextEditor6.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor6.TabIndex = 12;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(12, 52);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 11;
            this.uLabel4.Text = "4";
            // 
            // ultraTextEditor5
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance19;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(632, 52);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 10;
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(528, 52);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(100, 20);
            this.uLabel3.TabIndex = 9;
            this.uLabel3.Text = "3";
            // 
            // ultraTextEditor4
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance20;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(632, 28);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 8;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(528, 28);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(100, 20);
            this.uLabel2.TabIndex = 7;
            this.uLabel2.Text = "2";
            // 
            // ultraTextEditor3
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor3.Appearance = appearance27;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.PowderBlue;
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance28;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor3.ButtonsRight.Add(editorButton2);
            this.ultraTextEditor3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor3.Location = new System.Drawing.Point(376, 28);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 6;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(272, 28);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(100, 20);
            this.uLabel1.TabIndex = 5;
            this.uLabel1.Text = "1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 10;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMM0025
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0025";
            this.Load += new System.EventHandler(this.frmDMM0025_Load);
            this.Activated += new System.EventHandler(this.frmDMM0025_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor21;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor19;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor17;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor16;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private QRPUserControl.TitleArea titleArea;
    }
}