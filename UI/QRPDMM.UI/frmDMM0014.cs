﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmDMM0014.cs                                        */
/* 프로그램명   : 점검일지등록                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;

namespace QRPDMM.UI
{
    public partial class frmDMM0014 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal Sysres = new QRPGlobal();
        string m_strFinishFlag = "";

        private string strFormName;
        public frmDMM0014(string strPlantCode, string strRepairCode)
        {
            InitializeComponent();
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();

            Display_Detail(strPlantCode, strRepairCode);
            

        }

        public string FormName
        {
            get { return strFormName; }
            set { strFormName = value; }
        }

        public frmDMM0014()
        {
            InitializeComponent();
        }

        private void frmDMM0014_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0014_Load(object sender, EventArgs e)
        {
            if (strFormName == null)
            {
                //컨트롤초기화
                SetToolAuth();
                InitText();
                InitLabel();
                InitGrid();
                InitGroupBox();
                InitComboBox();

                this.uGroupBoxContentsArea.Expanded = false;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfLoadGridColumnProperty(this);
            }
            else
            {
                Point po = new Point(0, 0);
                this.Location = po;
            }
        }

        private void frmDMM0014_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                //타이틀설정
                titleArea.mfSetLabelText("점검일지등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uTextDurableMatCode.Text = "";
                this.uTextDurableMatName.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextJudgeChargeID.Text = "";
                this.uTextJudgeChargeName.Text = "";
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextRepairCode.Text = "";
                this.uTextSpec.Text = "";
                this.uTextUsageJudgeDate.Text = "";
                this.uTextUsageDocCode.Text = "";
                this.uTextCumUsage.Text = "";
                this.uTextCurUsage.Text = "";
                this.uTextLotNo.Text = "";
                m_strFinishFlag = "";
                
                this.uCheckFinishFlag.CheckedValue = false;
                this.uCheckChangeFlag.CheckedValue = false;
                this.uCheckRepairFlag.CheckedValue = false;
                this.uCheckStandbyFlag.CheckedValue = false;
                this.uCheckUsageClearFlag.CheckedValue = false;
                this.uCheckChangeFlag.Enabled = false;

                this.uTextPlantCode.Visible = false;
                this.uTextUsageDocCode.Visible = false;
                this.uTextCumUsage.Visible = false;
                this.uTextCurUsage.Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUserID, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairCode, "정비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairGubunCode, "정비구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPlantName, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInputUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelJudgeChargeID, "Shot판정담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStandbyDate, "정비대기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairDate, "정비착수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFinishDate, "정비완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairResultCode, "점검결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                // Call Method
                DataTable dtPMResultType = clsPMResultType.mfReadPMResultTypeCombo(this.uTextPlantCode.Text,m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboRepairResultCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PMResultCode", "PMResultName", dtPMResultType);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCommonCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCommonCode);

                // Call Method
                DataTable dtUserCommonCode = clsUserCommonCode.mfReadUserCommonCode("EQU", "U0008", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboRepairGubunCode,true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtUserCommonCode);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //기본설정
                //--1
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                

                //컬럼설정
                //--1
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairCode", "정비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunCode", "정비구분코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairGubunName", "정비구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CurUsage", "현재Shot수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CumUsage", "누적Shot수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageJudgeDate", "Shot판정일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StandbyDate", "정비대기일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0,
                     Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "RepairDate", "정비착수일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "FinishDate", "정비완료일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "yyyy-mm-dd", "");




                //--2
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--2
                grd.mfSetGridColumn(this.uGrid2, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairPlace", "수정장소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairPart", "수정부위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairName", "정비내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurablePMGubunCode", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                grd.mfSetGridColumn(this.uGrid2, 0, "DurablePMTypeCode", "정비유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "작업조", "작업조", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairChargeID", "정비사ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairChargeName", "정비사명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RepairTime", "정비시간(분)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 3, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "#,###", "nnnnnnnnn", "0");
            
                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGrid2, 0);


                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMGubun), "DurablePMGubun");
                QRPMAS.BL.MASDMM.DurablePMGubun clsDurablePMGubun = new QRPMAS.BL.MASDMM.DurablePMGubun();
                brwChannel.mfCredentials(clsDurablePMGubun);

                DataTable dtDurablePMGubun = clsDurablePMGubun.mfReadDurablePMGubun_Combo(uTextPlantCode.Text, m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGrid2, 0, "DurablePMGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtDurablePMGubun);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();


                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "점검진행정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "정비상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                   Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                   Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromUsageJudgeDate = this.uDateFromUsageJudgeDate.Value.ToString();
                string strToUsageJudgeDate = this.uDateToUsageJudgeDate.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;
                string strEquipName = this.uTextSearchEquipName.Text;
                string strDurableMatCode = this.uTextSearchDurableMatCode.Text;
                string strDurableMatName = this.uTextSearchDurableMatName.Text;
                string strUserID = this.uTextUserID.Text;
                string strUserName = this.uTextUserName.Text;


                // Call Method
                DataTable dtRepairH = clsRepairH.mfReadRepairH(strPlantCode
                    , strFromUsageJudgeDate
                    , strToUsageJudgeDate
                    , strEquipCode
                    , strEquipName
                    , strDurableMatCode
                    , strDurableMatName
                    , strUserID
                    , strUserName
                    , m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtRepairH;
                uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRepairH.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";

                
                if (m_strFinishFlag == "T")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "정비완료가 된 데이터는 수정을 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();

                    return;
                }
                
                // 필수입력사항 확인
                if (this.uTextPlantCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextRepairCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000392", Infragistics.Win.HAlign.Center);

                    // Focus
                    //this.uComboPlant.DropDown();
                    return;
                }
                // 필수입력사항 확인
                if (this.uComboRepairGubunCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "정비구분을 선택하셔야 합니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboRepairGubunCode.DropDown();
                    return;
                }

                if (this.uCheckStandbyFlag.CheckedValue.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "정비대기가 선택되지 않았습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uCheckStandbyFlag.Focus();
                    return;
                }
                

                // 필수입력사항 확인
                if (this.uCheckFinishFlag.CheckedValue.Equals(true))
                {
                    if (this.uComboRepairResultCode.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "정비완료일 경우, 반드시 점검결과를 선택하셔야 합니다", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboRepairResultCode.DropDown();
                        return;
                    }
                }
                else
                {
                    if (this.uComboRepairResultCode.Value.ToString() != "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "정비 완료일 경우에만 점검결과를 선택해 주시기 바랍니다.", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboRepairResultCode.Value = "";
                        this.uCheckChangeFlag.CheckedValue = false;
                        this.uCheckUsageClearFlag.CheckedValue = false;
                        return;
                    }

                    if (this.uCheckUsageClearFlag.CheckedValue.Equals(true))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "초기화 여부는 정비완료일 경우에만 선택 할 수 있습니다.", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uCheckUsageClearFlag.CheckedValue = false;
                        this.uCheckFinishFlag.Focus();
                        return;
                    }
                }


                if (this.uCheckStandbyFlag.CheckedValue.Equals(true) && this.uCheckRepairFlag.CheckedValue.Equals(true) && this.uCheckFinishFlag.CheckedValue.Equals(true))
                {
                    if (Convert.ToDateTime(this.uDateStandbyDate.Value) > Convert.ToDateTime(this.uDateRepairDate.Value))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "정비착수일을 정비대기일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uDateRepairDate.Focus();
                        return;
                    }

                    if (Convert.ToDateTime(this.uDateRepairDate.Value) > Convert.ToDateTime(this.uDateFinishDate.Value))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "정비완료일을 정비착수일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uDateFinishDate.Focus();
                        return;
                    }

                }

                if (this.uCheckStandbyFlag.CheckedValue.Equals(true) && this.uCheckRepairFlag.CheckedValue.Equals(true) )
                {
                    if (Convert.ToDateTime(this.uDateStandbyDate.Value) > Convert.ToDateTime(this.uDateRepairDate.Value))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "정비착수일을 정비대기일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uDateRepairDate.Focus();
                        return;
                    }

                }
                

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGrid2.Rows[i].Cells["DurablePMGubunCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 정비분류를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["DurablePMGubunCode"];
                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid2.Rows[i].Cells["DurablePMTypeCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 정비유형을 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["DurablePMTypeCode"];
                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid2.Rows[i].Cells["RepairChargeID"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", this.uGrid2.Rows[i].RowSelectorNumber + "번째 열의 정비사를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["RepairChargeID"];
                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }                  
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001236", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);


                //--------- 1. DMMRepairH 테이블에 저장 ----------------------------------------------//
                DataTable dtRepairH = clsRepairH.mfSetDatainfo();

            
                row = dtRepairH.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["RepairCode"] = this.uTextRepairCode.Text;
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["RepairGubunCode"] = this.uComboRepairGubunCode.Value.ToString();
                row["UsageDocCode"] = this.uTextUsageDocCode.Text;

                if (this.uCheckStandbyFlag.Checked == true)
                {
                    row["StandbyFlag"] = "T";
                    row["StandbyDate"] = this.uDateStandbyDate.Value.ToString();
                }
                else
                {
                    row["StandbyFlag"] = "F";
                    row["StandbyDate"] = "";
                }

                if (this.uCheckRepairFlag.Checked == true)
                {
                    row["RepairFlag"] = "T";
                    row["RepairDate"] = this.uDateRepairDate.Value.ToString();
                }
                else
                {
                    row["RepairFlag"] = "F";
                    row["RepairDate"] = "";
                }

                if (this.uCheckFinishFlag.Checked == true)
                {
                    row["FinishFlag"] = "T";
                    row["FinishDate"] = this.uDateFinishDate.Value.ToString();
                }
                else
                {
                    row["FinishFlag"] = "F";
                    row["FinishDate"] = "";
                }

                row["RepairResultCode"] = this.uComboRepairResultCode.Value.ToString();

                if (this.uCheckChangeFlag.Checked == true)
                    row["ChangeFlag"] = "T";
                else
                    row["ChangeFlag"] = "F";

                row["EtcDesc"] = this.uTextEtcDesc.Text;

                if (this.uCheckUsageClearFlag.Checked == true)
                    row["UsageClearFlag"] = "T";
                else
                    row["UsageClearFlag"] = "F";

                row["UsageClearDate"] = "";
                row["ClearCurUsage"] = this.uTextCurUsage.Text;
                row["ClearCumUsage"] = this.uTextCumUsage.Text;

                dtRepairH.Rows.Add(row);
           

                //-------- 2.  DMMRepairD 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairD), "RepairD");
                QRPDMM.BL.DMMMGM.RepairD clsRepairD = new QRPDMM.BL.DMMMGM.RepairD();
                brwChannel.mfCredentials(clsRepairD);
                DataTable dtRepairD = clsRepairD.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        row = dtRepairD.NewRow();
                        row["PlantCode"] = this.uTextPlantCode.Text;
                        row["RepairCode"] = this.uTextRepairCode.Text;
                        row["Seq"] = i + 1;
                        row["RepairPlace"] = this.uGrid2.Rows[i].Cells["RepairPlace"].Value.ToString();
                        row["RepairPart"] = this.uGrid2.Rows[i].Cells["RepairPart"].Value.ToString();
                        row["RepairName"] = this.uGrid2.Rows[i].Cells["RepairName"].Value.ToString();
                        row["DurablePMGubunCode"] = this.uGrid2.Rows[i].Cells["DurablePMGubunCode"].Value.ToString();
                        row["DurablePMTypeCode"] = this.uGrid2.Rows[i].Cells["DurablePMTypeCode"].Value.ToString();
                        row["RepairChargeID"] = this.uGrid2.Rows[i].Cells["RepairChargeID"].Value.ToString();
                        row["RepairTime"] = this.uGrid2.Rows[i].Cells["RepairTime"].Value.ToString();
                        row["EtcDesc"] = this.uGrid2.Rows[i].Cells["EtcDesc"].Value.ToString();
                        
                        dtRepairD.Rows.Add(row);
                    }
                }

                //---------3. EQUDurableMatRepairGIH 테이블에 저장 ---------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                DataTable dtDurableMatRepairH = clsDurableMatRepairH.mfDataSetInfo();
                
                row = dtDurableMatRepairH.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["RepairGICode"] = "";
                row["GITypeCode"] = "LF";
                row["PlanYear"] = "";
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["PMPlanDate"] = "";
                row["RepairReqCode"] = this.uTextRepairCode.Text;
                row["RepairGIReqDate"] = "";
                row["RepairGIReqID"] = "";
                row["EtcDesc"] = "";
                dtDurableMatRepairH.Rows.Add(row);
               


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.SAVEMGM), "SAVEMGM");
                QRPDMM.BL.DMMMGM.SAVEMGM clsSAVEMGM = new QRPDMM.BL.DMMMGM.SAVEMGM();
                brwChannel.mfCredentials(clsSAVEMGM);
                string rtMSG = clsSAVEMGM.mfSaveRepair(dtRepairH, dtRepairD, dtDurableMatRepairH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
                InitText();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if(this.uGrid1.Rows.Count == 0 && (this.uGrid2.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                WinGrid grd = new WinGrid();
                if(this.uGrid1.Rows.Count >0)
                    grd.mfDownLoadGridToExcel(this.uGrid1);

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid2);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 720;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                
                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                KeyEventArgs KeyEvent = new KeyEventArgs(Keys.Enter);
                
                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                string strPlantCode = uTextPlantCode.Text;
                string strDurablePMGubunCode = uGrid.ActiveCell.Row.Cells["DurablePMGubunCode"].Value.ToString();
                string strRepairChargeID = uGrid.ActiveCell.Row.Cells["RepairChargeID"].Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                if (strColumn == "DurablePMGubunCode")
                {                    
                    if (strPlantCode == "")
                        return;
                    if (strDurablePMGubunCode == "")
                        return;

                    
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMType), "DurablePMType");
                    QRPMAS.BL.MASDMM.DurablePMType clsDurablePMType = new QRPMAS.BL.MASDMM.DurablePMType();
                    brwChannel.mfCredentials(clsDurablePMType);

                    DataTable dtDurablePMType = clsDurablePMType.mfReadDurablePMType_Combo(strPlantCode, strDurablePMGubunCode, m_resSys.GetString("SYS_LANG"));
                    grd.mfSetGridCellValueList(this.uGrid2, intIndex, "DurablePMTypeCode", "", "", dtDurablePMType);
                }

                //////////////////////정비사명 히든처리
                ////if (strColumn == "RepairChargeID")
                ////{
                ////    // 필수입력사항 확인
                ////    if (strRepairChargeID == "")
                ////        return;



                ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                ////    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                ////    brwChannel.mfCredentials(clsUser);

                ////    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strRepairChargeID, m_resSys.GetString("SYS_LANG"));

                ////    if (dtUser.Rows.Count > 0)
                ////    {
                ////        uGrid.ActiveCell.Row.Cells["RepairChargeName"].Value = dtUser.Rows[0]["UserName"].ToString();
                ////    }                     

                ////}
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboRepairResultCode_AfterCloseUp(object sender, EventArgs e)
        {
            
        }

        private void Display_Detail(string strPlantCode, string strRepairCode)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }

                QRPBrowser brwChannel = new QRPBrowser();

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                // Call Method
                DataTable dtRepairH = clsRepairH.mfReadRepairH_Detail(strPlantCode, strRepairCode, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {
                    this.uTextPlantCode.Text = dtRepairH.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtRepairH.Rows[i]["PlantName"].ToString();

                    this.uTextDurableMatCode.Text = dtRepairH.Rows[i]["DurableMatCode"].ToString();
                    this.uTextDurableMatName.Text = dtRepairH.Rows[i]["DurableMatName"].ToString();
                    this.uTextLotNo.Text = dtRepairH.Rows[i]["LotNo"].ToString();
                       
                    this.uTextEquipCode.Text = dtRepairH.Rows[i]["EquipCode"].ToString();
                    this.uTextEquipName.Text = dtRepairH.Rows[i]["EquipName"].ToString();
                    this.uTextEtcDesc.Text = dtRepairH.Rows[i]["EtcDesc"].ToString();
                    this.uTextJudgeChargeID.Text = dtRepairH.Rows[i]["JudgeChargeID"].ToString();
                    this.uTextJudgeChargeName.Text = dtRepairH.Rows[i]["JudgeChargeName"].ToString();
                    this.uTextPlantCode.Text = dtRepairH.Rows[i]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtRepairH.Rows[i]["PlantName"].ToString();
                    this.uTextRepairCode.Text = dtRepairH.Rows[i]["RepairCode"].ToString();
                    this.uTextSpec.Text = dtRepairH.Rows[i]["Spec"].ToString();
                    this.uTextUsageJudgeDate.Text = dtRepairH.Rows[i]["UsageJudgeDate"].ToString();
                    this.uTextUsageDocCode.Text = dtRepairH.Rows[i]["UsageDocCode"].ToString();

                    this.uTextCumUsage.Text = dtRepairH.Rows[i]["CumUsage"].ToString();
                    this.uTextCurUsage.Text = dtRepairH.Rows[i]["CurUsage"].ToString();

                    this.uComboRepairGubunCode.Value = dtRepairH.Rows[i]["RepairGubunCode"].ToString();
                    this.uComboRepairResultCode.Value = dtRepairH.Rows[i]["RepairResultCode"].ToString();

                    if (dtRepairH.Rows[i]["ChangeFlag"].ToString() == "T")
                        this.uCheckChangeFlag.Checked = true;
                    else
                        this.uCheckChangeFlag.Checked = false;

                    if (dtRepairH.Rows[i]["StandbyFlag"].ToString() == "T")
                        this.uCheckStandbyFlag.Checked = true;
                    else
                        this.uCheckStandbyFlag.Checked = false;

                    if (dtRepairH.Rows[i]["RepairFlag"].ToString() == "T")
                        this.uCheckRepairFlag.Checked = true;
                    else
                        this.uCheckRepairFlag.Checked = false;

                    if (dtRepairH.Rows[i]["FinishFlag"].ToString() == "T")
                        this.uCheckFinishFlag.Checked = true;
                    else
                        this.uCheckFinishFlag.Checked = false;

                    if (dtRepairH.Rows[i]["UsageClearFlag"].ToString() == "T")
                        this.uCheckUsageClearFlag.Checked = true;
                    else
                        this.uCheckUsageClearFlag.Checked = false;

                    if (dtRepairH.Rows[i]["FinishDate"].ToString() != "")
                        this.uDateFinishDate.Value = dtRepairH.Rows[i]["FinishDate"].ToString();

                    if (dtRepairH.Rows[i]["RepairDate"].ToString() != "")
                        this.uDateRepairDate.Value = dtRepairH.Rows[i]["RepairDate"].ToString();

                    if (dtRepairH.Rows[i]["StandbyDate"].ToString() != "")
                        this.uDateStandbyDate.Value = dtRepairH.Rows[i]["StandbyDate"].ToString();

                    m_strFinishFlag = dtRepairH.Rows[i]["FinishFlag"].ToString();

                }

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                brwChannel.mfCredentials(clsEquipWorker);

                DataTable dtEquipWorker = clsEquipWorker.mfReadEquipWorker_Combo(this.uTextPlantCode.Text, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGrid2, 0, "RepairChargeID", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtEquipWorker);


                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairD), "RepairD");
                QRPDMM.BL.DMMMGM.RepairD clsRepairD = new QRPDMM.BL.DMMMGM.RepairD();
                brwChannel.mfCredentials(clsRepairD);

                // Call Method
                DataTable dtRepairD = clsRepairD.mfReadRepairD(strPlantCode, strRepairCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid2.DataSource = dtRepairD;
                uGrid2.DataBind();

                if (dtRepairD.Rows.Count > 0)
                {
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strRepairCode = e.Cell.Row.Cells["RepairCode"].Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strRepairCode == "")
                    return;

                Display_Detail(strPlantCode, strRepairCode);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboRepairResultCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();



                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                string strPMResultCode = this.uComboRepairResultCode.Value.ToString();
                string strPlantCode = this.uTextPlantCode.Text;

                if (strPlantCode == "")
                    return;

                if (strPMResultCode == "")
                    return;


                // Call Method
                DataTable dtPMResultType = clsPMResultType.mfReadPMResultType_Detail(this.uTextPlantCode.Text, strPMResultCode, m_resSys.GetString("SYS_LANG"));

                if (dtPMResultType.Rows[0]["ChangeFlag"].ToString() == "T")
                {
                    uCheckChangeFlag.CheckedValue = true;
                }
                else
                {
                    uCheckChangeFlag.CheckedValue = false;
                }




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0005 frm = new frmPOP0005();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchEquipCode.Text = frm.EquipCode;
                uTextSearchEquipName.Text = frm.EquipName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextUserID.Text = frm.UserID;
                uTextUserName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString()=="")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000597", "M000266"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0007 frm = new frmPOP0007();
                frm.PlantCode = this.uComboPlant.Value.ToString();
                frm.ShowDialog();

                uTextSearchDurableMatCode.Text = frm.DurableMatCode;
                uTextSearchDurableMatName.Text = frm.DurableMatName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckRepairFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                if (this.uCheckStandbyFlag.CheckedValue.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "정비 착수가 선택되려면 정비대기가 먼저 선택되어져야 합니다.", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uCheckStandbyFlag.Focus();
                    this.uCheckRepairFlag.CheckedValue = false;
                    return;
                }


            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckFinishFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                if (this.uCheckRepairFlag.CheckedValue.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "정비완료를 선택하려면 정비착수가 먼저 선택되어져야 합니다.", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uCheckRepairFlag.Focus();
                    this.uCheckFinishFlag.CheckedValue = false;
                    return;
                }


            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmDMM0014_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextUserID;
                uTextName = this.uTextUserName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextUserID.Text = "";
                    this.uTextUserName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비코드 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strEquipCode = this.uTextSearchEquipCode.Text;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquip.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000901",
                                            Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSearchEquipCode.Text = "";
                    this.uTextSearchEquipName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextSearchDurableMatCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(Sysres.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsMat);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strDurableCode = this.uTextSearchDurableMatCode.Text;

                    DataTable dtDurable = clsMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                    if (dtDurable.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M000962", "M000797",
                                                                    Infragistics.Win.HAlign.Right);
                    }

                    else
                    {
                        this.uTextSearchDurableMatName.Text = dtDurable.Rows[0]["DurableMatName"].ToString();
                    }

                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchDurableMatCode.Text = "";
                    this.uTextSearchDurableMatName.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
    }
}
