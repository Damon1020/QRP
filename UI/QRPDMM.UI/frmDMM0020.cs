﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMM0020.cs                                         */
/* 프로그램명   : 치공구교체등록                                        */
/* 작성자       : 이종호 , 코딩 : 남현식                                         */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using QRPMAS.BL.MASPRC;

namespace QRPDMM.UI
{
    public partial class frmDMM0020 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMM0020()
        {
            InitializeComponent();
        }

        private void frmDMM0020_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0020_Load(object sender, EventArgs e)
        {

            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMM0020_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 초기화 Method

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText() 
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("치공구교체등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                uTextChgChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextChgChargeName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uDateChgDate.Value = DateTime.Now;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelChgDate, "교체일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelChgChargeID, "교체담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그룹박스초기화
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "치공구교체리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트지정
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGridDurableTransferD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferCode", "출고문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferSeq", "출고순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDate", "출고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferChargeID", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferChargeName", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "자재출고유형", "자재출고유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableInventoryCode", "출고창고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableInventoryName", "출고창고명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgEquipCode", "교체설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgEquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgDurableMatCode", "교체치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgDurableMatName", "교체치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgLotNo", "교체LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferD, 0, "InputQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");


                //grd.mfSetGridColumn(this.uGrid1, 0, "교체처리방법", "교체처리방법", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "선택");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridDurableTransferD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDurableTransferD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferD), "DurableTransferD");
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new QRPDMM.BL.DMMICP.DurableTransferD();
                brwChannel.mfCredentials(clsDurableTransferD);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                // Call Method
                DataTable dtDurableTransferD = clsDurableTransferD.mfReadDMMDurableTransferD_Chg(strPlantCode, strEquipCode, "", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridDurableTransferD.DataSource = dtDurableTransferD;
                uGridDurableTransferD.DataBind();

                //if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                //{

                //}

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtDurableTransferD.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                QRPBrowser brwChannel = new QRPBrowser();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;
                int intRowCheck = 0;


                // 필수입력사항 확인

                if (this.uDateChgDate.Value == null || this.uDateChgDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M000304", Infragistics.Win.HAlign.Center);

                    this.uDateChgDate.DropDown();
                    return;
                }

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsBOM);
                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
                {
                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];

                    if (this.uGridDurableTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;
                            string strPlantCode = this.uGridDurableTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            string strEquipCode = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            string strDurableMatCode = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
                            string strLotNo = this.uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();

                            DataTable dtBOM = new DataTable();

                            if (strLotNo != "")
                            {
                                dtBOM = clsBOM.mfReadEquipDurableBOM_Detail(strPlantCode, strEquipCode, strDurableMatCode, strLotNo, m_resSys.GetString("SYS_LANG"));
                            }
                            else
                            {
                                dtBOM = clsBOM.mfReadEquipDurableBOM_DurableMatInputQty(strPlantCode, strEquipCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));
                            }

                            if (dtBOM.Rows.Count > 0)
                            {
                                int intBOMInputQty = Convert.ToInt32(dtBOM.Rows[0]["InputQty"].ToString());
                                int intInputQty = Convert.ToInt32(this.uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString());

                                if (intInputQty > intBOMInputQty)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGridDurableTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000461", strLang), Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["Check"];
                                    this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                            }
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                            , this.uGridDurableTransferD.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000462",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["Check"];
                                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001236", Infragistics.Win.HAlign.Center);
                    return;
                }


                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                

                //-------- 1.  EQUDurableTransferD 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferD), "DurableTransferD");
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new QRPDMM.BL.DMMICP.DurableTransferD();
                brwChannel.mfCredentials(clsDurableTransferD);

                DataTable dtDurableTransferD = clsDurableTransferD.mfSetDatainfo();

                for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
                {
                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
                    if (this.uGridDurableTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableTransferD.NewRow();
                            row["PlantCode"] = this.uGridDurableTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["TransferCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString();
                            row["TransferSeq"] = this.uGridDurableTransferD.Rows[i].Cells["TransferSeq"].Value.ToString();
                            row["TransferDurableInventoryCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString();
                            row["TransferDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
                            row["TransferLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
                            row["TransferQty"] = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["ChgEquipCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            row["ChgDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
                            row["ChgLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();
                            row["InputQty"] = this.uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString();
                            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            row["TransferEtcDesc"] = this.uGridDurableTransferD.Rows[i].Cells["TransferEtcDesc"].Value.ToString();
                            row["ChgFlag"] = "T";
                            row["ChgDate"] = this.uDateChgDate.Value.ToString();
                            row["ChgChargeID"] = this.uTextChgChargeID.Text;
                            row["ChgEtcDesc"] = "";
                            row["ReturnFlag"] = "";
                            row["ReturnDate"] = "";
                            row["ReturnChargeID"] = "";
                            row["ReturnDurableInventoryCode"] = "";
                            row["ReturnEtcDesc"] = "";
                            dtDurableTransferD.Rows.Add(row);
                        }
                    }
                }

                //-------- 2.  EQUDurableChgStandby 테이블에 저장 (수량 - 처리) : 설비에 투입될 SparePart : TransferDurableMatCode ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableChgStandby), "DurableChgStandby");
                QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new QRPDMM.BL.DMMICP.DurableChgStandby();
                brwChannel.mfCredentials(clsDurableChgStandby);
                DataTable dtDurableChgStandbyInput = clsDurableChgStandby.mfSetDatainfo();

                for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
                {
                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
                    if (this.uGridDurableTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableChgStandbyInput.NewRow();
                            row["PlantCode"] = this.uGridDurableTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
                            row["ChgQty"] = "-" + this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtDurableChgStandbyInput.Rows.Add(row);
                        }
                    }
                }


                //-------- 3.  EQUDurableChgStandby 테이블에 저장 (수량 + 처리) : 설비에서 교체된 SparePart : ChgDurableMatCode ----------------------------------------------//;
                DataTable dtDurableChgStandbyOut = clsDurableChgStandby.mfSetDatainfo();

                for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
                {
                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
                    if (this.uGridDurableTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableChgStandbyOut.NewRow();
                            row["PlantCode"] = this.uGridDurableTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();
                            row["ChgQty"] = this.uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString();
                            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtDurableChgStandbyOut.Rows.Add(row);
                        }
                    }
                }

                //-------- 4.  MASEquipDurableBOM 테이블 : 교체처리 ----------------------------------------------//;
                DataTable dtEquipDurableBOM = clsDurableTransferD.mfSetDatainfo();
                for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
                {
                    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
                    if (this.uGridDurableTransferD.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtEquipDurableBOM.NewRow();
                            row["PlantCode"] = this.uGridDurableTransferD.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["TransferDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
                            row["TransferLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
                            row["TransferQty"] = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();

                            row["ChgEquipCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                            row["ChgDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
                            row["ChgLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();
                            row["InputQty"] = this.uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString();

                            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                            dtEquipDurableBOM.Rows.Add(row);
                        }
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveDurableChange(dtDurableTransferD, dtDurableChgStandbyInput, dtDurableChgStandbyOut, dtEquipDurableBOM, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning,  500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceIno
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGridDurableTransferD.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridDurableTransferD);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Event
        // 셀수정시 이벤트

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableTransferD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = this.uComboPlant.Value.ToString();
                frmEquip.ShowDialog();

                this.uTextEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipName.Text = frmEquip.EquipName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 사용자 검색 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextChgChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                this.uTextChgChargeID.Text = frmUser.UserID;
                this.uTextChgChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 설비 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEqu);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strEquipCode = this.uTextEquipCode.Text;

                    DataTable dtEquip = clsEqu.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    }
                    else
                    {
                        this.uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 변경자 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextChgChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextChgChargeID.Text;

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        this.uTextChgChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                }
                //if (e.KeyCode.Equals(Keys.Delete) || e.KeyCode.Equals(e.KeyCode.Equals(Keys.Back)))
                if(e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextChgChargeID.Text = "";
                    this.uTextChgChargeName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextEquipName.Text.Equals(string.Empty))
                this.uTextEquipName.Clear();
        }

        #endregion
    }
}
