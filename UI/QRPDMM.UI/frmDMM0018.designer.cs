﻿namespace QRPDMM.UI
{
    partial class frmDMM0018
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0018));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uText1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraDateTimeEditor4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraComboEditor2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraDateTimeEditor3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraCheckEditor3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraCheckEditor2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraCheckEditor1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraDateTimeEditor5 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor5)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor14);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uText1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraDateTimeEditor5);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraDateTimeEditor4);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel4);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(544, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 102;
            this.ultraLabel1.Text = "~";
            // 
            // ultraTextEditor1
            // 
            appearance46.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor1.Appearance = appearance46;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.PowderBlue;
            appearance47.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance47.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance47;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor1.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor1.Location = new System.Drawing.Point(128, 36);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 100;
            // 
            // ultraTextEditor2
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance18;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(444, 36);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 99;
            // 
            // ultraTextEditor14
            // 
            appearance45.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Appearance = appearance45;
            this.ultraTextEditor14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Location = new System.Drawing.Point(844, 35);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.ReadOnly = true;
            this.ultraTextEditor14.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor14.TabIndex = 99;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(328, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(110, 20);
            this.uLabel1.TabIndex = 87;
            this.uLabel1.Text = "1";
            // 
            // uText1
            // 
            appearance15.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance15;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uText1.ButtonsRight.Add(editorButton2);
            this.uText1.Location = new System.Drawing.Point(740, 35);
            this.uText1.Name = "uText1";
            this.uText1.Size = new System.Drawing.Size(100, 21);
            this.uText1.TabIndex = 98;
            // 
            // ultraDateTimeEditor4
            // 
            this.ultraDateTimeEditor4.Location = new System.Drawing.Point(560, 12);
            this.ultraDateTimeEditor4.Name = "ultraDateTimeEditor4";
            this.ultraDateTimeEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor4.TabIndex = 88;
            // 
            // uComboPlant
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance19;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 41;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 40;
            this.uLabelPlant.Text = "공장";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(12, 35);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(110, 20);
            this.uLabel2.TabIndex = 81;
            this.uLabel2.Text = "2";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(328, 36);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(110, 20);
            this.uLabel3.TabIndex = 82;
            this.uLabel3.Text = "3";
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(623, 36);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(110, 20);
            this.uLabel4.TabIndex = 83;
            this.uLabel4.Text = "4";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance5;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 720);
            this.uGrid1.TabIndex = 2;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox3.Controls.Add(this.uGrid2);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 252);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 400);
            this.uGroupBox3.TabIndex = 100;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 1;
            this.uButtonDeleteRow.Text = "ultraButton1";
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance33;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance42;
            appearance36.BackColor = System.Drawing.SystemColors.Highlight;
            appearance36.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance36;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            appearance34.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance34;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance41.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance40;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 40);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1016, 348);
            this.uGrid2.TabIndex = 0;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.ultraTextEditor15);
            this.uGroupBox2.Controls.Add(this.ultraComboEditor2);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor3);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor2);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor1);
            this.uGroupBox2.Controls.Add(this.ultraCheckEditor3);
            this.uGroupBox2.Controls.Add(this.ultraCheckEditor2);
            this.uGroupBox2.Controls.Add(this.ultraCheckEditor1);
            this.uGroupBox2.Controls.Add(this.uLabel15);
            this.uGroupBox2.Controls.Add(this.uLabel16);
            this.uGroupBox2.Controls.Add(this.uLabel20);
            this.uGroupBox2.Controls.Add(this.uLabel21);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor16);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor13);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor12);
            this.uGroupBox2.Controls.Add(this.uLabel19);
            this.uGroupBox2.Controls.Add(this.uLabel18);
            this.uGroupBox2.Controls.Add(this.uLabel17);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 144);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBox2.TabIndex = 99;
            // 
            // ultraTextEditor15
            // 
            this.ultraTextEditor15.Location = new System.Drawing.Point(392, 76);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.Size = new System.Drawing.Size(420, 21);
            this.ultraTextEditor15.TabIndex = 104;
            // 
            // ultraComboEditor2
            // 
            this.ultraComboEditor2.Location = new System.Drawing.Point(128, 76);
            this.ultraComboEditor2.Name = "ultraComboEditor2";
            this.ultraComboEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraComboEditor2.TabIndex = 103;
            this.ultraComboEditor2.Text = "ultraComboEditor2";
            // 
            // ultraDateTimeEditor3
            // 
            this.ultraDateTimeEditor3.Location = new System.Drawing.Point(924, 52);
            this.ultraDateTimeEditor3.Name = "ultraDateTimeEditor3";
            this.ultraDateTimeEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor3.TabIndex = 102;
            // 
            // ultraDateTimeEditor2
            // 
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(572, 52);
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor2.TabIndex = 102;
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(192, 52);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 102;
            // 
            // ultraCheckEditor3
            // 
            this.ultraCheckEditor3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ultraCheckEditor3.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.ultraCheckEditor3.Location = new System.Drawing.Point(732, 52);
            this.ultraCheckEditor3.Name = "ultraCheckEditor3";
            this.ultraCheckEditor3.Size = new System.Drawing.Size(72, 20);
            this.ultraCheckEditor3.TabIndex = 101;
            this.ultraCheckEditor3.Text = "수리완료";
            this.ultraCheckEditor3.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // ultraCheckEditor2
            // 
            this.ultraCheckEditor2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ultraCheckEditor2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.ultraCheckEditor2.Location = new System.Drawing.Point(380, 52);
            this.ultraCheckEditor2.Name = "ultraCheckEditor2";
            this.ultraCheckEditor2.Size = new System.Drawing.Size(72, 20);
            this.ultraCheckEditor2.TabIndex = 101;
            this.ultraCheckEditor2.Text = "수리착수";
            this.ultraCheckEditor2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // ultraCheckEditor1
            // 
            this.ultraCheckEditor1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ultraCheckEditor1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.ultraCheckEditor1.Location = new System.Drawing.Point(12, 52);
            this.ultraCheckEditor1.Name = "ultraCheckEditor1";
            this.ultraCheckEditor1.Size = new System.Drawing.Size(72, 20);
            this.ultraCheckEditor1.TabIndex = 101;
            this.ultraCheckEditor1.Text = "수리대기";
            this.ultraCheckEditor1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(12, 28);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(110, 20);
            this.uLabel15.TabIndex = 97;
            this.uLabel15.Text = "15";
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(276, 28);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(110, 20);
            this.uLabel16.TabIndex = 95;
            this.uLabel16.Text = "16";
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(12, 76);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(110, 20);
            this.uLabel20.TabIndex = 94;
            this.uLabel20.Text = "20";
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(276, 76);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(110, 20);
            this.uLabel21.TabIndex = 94;
            this.uLabel21.Text = "21";
            // 
            // ultraTextEditor16
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Appearance = appearance16;
            this.ultraTextEditor16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Location = new System.Drawing.Point(392, 28);
            this.ultraTextEditor16.Name = "ultraTextEditor16";
            this.ultraTextEditor16.ReadOnly = true;
            this.ultraTextEditor16.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor16.TabIndex = 100;
            // 
            // ultraTextEditor13
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Appearance = appearance48;
            this.ultraTextEditor13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Location = new System.Drawing.Point(496, 28);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.ReadOnly = true;
            this.ultraTextEditor13.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor13.TabIndex = 100;
            // 
            // ultraTextEditor12
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Appearance = appearance43;
            this.ultraTextEditor12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Location = new System.Drawing.Point(128, 28);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.ReadOnly = true;
            this.ultraTextEditor12.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor12.TabIndex = 100;
            // 
            // uLabel19
            // 
            this.uLabel19.Location = new System.Drawing.Point(808, 52);
            this.uLabel19.Name = "uLabel19";
            this.uLabel19.Size = new System.Drawing.Size(110, 20);
            this.uLabel19.TabIndex = 94;
            this.uLabel19.Text = "19";
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(456, 52);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(110, 20);
            this.uLabel18.TabIndex = 94;
            this.uLabel18.Text = "18";
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(88, 52);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(100, 20);
            this.uLabel17.TabIndex = 96;
            this.uLabel17.Text = "17";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.ultraComboEditor1);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor10);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor11);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor8);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor4);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor7);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor5);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor3);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uLabel12);
            this.uGroupBox1.Controls.Add(this.uLabel14);
            this.uGroupBox1.Controls.Add(this.uLabel13);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Controls.Add(this.uLabel10);
            this.uGroupBox1.Controls.Add(this.uLabel11);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 128);
            this.uGroupBox1.TabIndex = 98;
            // 
            // ultraComboEditor1
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor1.Appearance = appearance37;
            this.ultraComboEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraComboEditor1.Location = new System.Drawing.Point(672, 28);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(140, 21);
            this.ultraComboEditor1.TabIndex = 101;
            this.ultraComboEditor1.Text = "ultraComboEditor1";
            // 
            // ultraTextEditor10
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Appearance = appearance25;
            this.ultraTextEditor10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Location = new System.Drawing.Point(392, 100);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.ReadOnly = true;
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 100;
            // 
            // ultraTextEditor9
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance20;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(392, 76);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 100;
            // 
            // ultraTextEditor11
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance17;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(672, 52);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 100;
            // 
            // ultraTextEditor8
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance21;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(392, 52);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor8.TabIndex = 100;
            // 
            // ultraTextEditor4
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance23;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(392, 28);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 100;
            // 
            // ultraTextEditor7
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance44;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(128, 100);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor7.TabIndex = 100;
            // 
            // ultraTextEditor6
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Appearance = appearance24;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Location = new System.Drawing.Point(128, 76);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.ReadOnly = true;
            this.ultraTextEditor6.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor6.TabIndex = 100;
            // 
            // ultraTextEditor5
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance28;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(128, 52);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 100;
            // 
            // ultraTextEditor3
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance29;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(128, 28);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 100;
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 52);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(110, 20);
            this.uLabel8.TabIndex = 90;
            this.uLabel8.Text = "8";
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(276, 52);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(110, 20);
            this.uLabel9.TabIndex = 87;
            this.uLabel9.Text = "9";
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 28);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(110, 20);
            this.uLabel5.TabIndex = 84;
            this.uLabel5.Text = "5";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(276, 28);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(110, 20);
            this.uLabel6.TabIndex = 85;
            this.uLabel6.Text = "6";
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(276, 76);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(110, 20);
            this.uLabel12.TabIndex = 91;
            this.uLabel12.Text = "12";
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(276, 100);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(110, 20);
            this.uLabel14.TabIndex = 93;
            this.uLabel14.Text = "14";
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(12, 100);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(110, 20);
            this.uLabel13.TabIndex = 92;
            this.uLabel13.Text = "13";
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(556, 28);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(110, 20);
            this.uLabel7.TabIndex = 86;
            this.uLabel7.Text = "7";
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(556, 52);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(110, 20);
            this.uLabel10.TabIndex = 89;
            this.uLabel10.Text = "10";
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(12, 76);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(110, 20);
            this.uLabel11.TabIndex = 88;
            this.uLabel11.Text = "11";
            // 
            // ultraDateTimeEditor5
            // 
            this.ultraDateTimeEditor5.Location = new System.Drawing.Point(444, 12);
            this.ultraDateTimeEditor5.Name = "ultraDateTimeEditor5";
            this.ultraDateTimeEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor5.TabIndex = 88;
            // 
            // frmDMM0018
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0018";
            this.Load += new System.EventHandler(this.frmDMM0018_Load);
            this.Activated += new System.EventHandler(this.frmDMM0018_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCheckEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.Misc.UltraLabel uLabel19;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor ultraCheckEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor16;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor5;
    }
}