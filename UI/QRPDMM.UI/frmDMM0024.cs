﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
namespace QRPDMM.UI
{
    public partial class frmDMM0024 : Form, IToolbar
    {
        //다국어지원

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMM0024()
        {
            InitializeComponent();
        }

        private void frmDMM0024_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0024_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth(); 
            InitTitle();
            InitLabel();
            InitGrid();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMM0024_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("치공구이력조회", m_resSys.GetString("SYS_FONTNAME"), 12);

               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                WinLabel lbl = new WinLabel();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDurableMat, "치공구", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정

               

                //grd.mfSetGridColumn(this.uGrid1, 0, "test", "test", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 0, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 0, 0, 1, 2, group0);

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",0, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "MoveGubunCode", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "MoveGubunName", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                grd.mfSetGridColumn(this.uGrid1, 0, "MoveDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                string strLang = m_resSys.GetString("SYS_LANG");
                string strIn = "";
                string strOut = "";
                string strIOQty = "";
                if (strLang.Equals("KOR"))
                {
                    strIn = "입고";
                    strOut = "출고";
                    strIOQty = "입출고수량";
                }
                else if (strLang.Equals("CHN"))
                {
                    strIn = "入库";
                    strOut = "出库";
                    strIOQty = "入出库";
                }
                else if (strLang.Equals("ENG"))
                {
                    strIn = "입고";
                    strOut = "출고";
                    strIOQty = "입출고수량";
                }

                this.uGrid1.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                //Infragistics.Win.UltraWinGrid.UltraGridGroup group0 = grd.mfSetGridGroup(this.uGrid1, 0, "te", "test", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = grd.mfSetGridGroup(this.uGrid1, 0, "In", strIn, 7, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = grd.mfSetGridGroup(this.uGrid1, 0, "Out", strOut, 10, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = grd.mfSetGridGroup(this.uGrid1, 0, "IOQty", strIOQty, 13, 0, 2, 2, false);

                ////grd.mfSetGridColumn(this.uGrid1, 0, "test", "", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 1, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 2, group0);

                grd.mfSetGridColumn(this.uGrid1, 0, "GRInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, group1);

                grd.mfSetGridColumn(this.uGrid1, 0, "GREquipCode", "교체설비번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, group1);

                grd.mfSetGridColumn(this.uGrid1, 0, "GREquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, group1);

                grd.mfSetGridColumn(this.uGrid1, 0, "GPInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, group2);

                grd.mfSetGridColumn(this.uGrid1, 0, "GPEquipCode", "교체설비번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, group2);

                grd.mfSetGridColumn(this.uGrid1, 0, "GPEquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, group2);

                grd.mfSetGridColumn(this.uGrid1, 0, "GRQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "", 0, 0, 1, 2, group3);

                grd.mfSetGridColumn(this.uGrid1, 0, "GPQty", "출고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "", 1, 0, 1, 2, group3);


                this.uGrid1.DisplayLayout.Bands[0].Columns["GRInventoryName"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GREquipCode"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GREquipName"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GPInventoryName"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GPEquipCode"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GPEquipName"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GRQty"].CellAppearance.ForeColor = Color.Black;
                this.uGrid1.DisplayLayout.Bands[0].Columns["GPQty"].CellAppearance.ForeColor = Color.Black;


                //grd.mfSetGridColumn(this.uGrid1, 0, "GRInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 9, 0, 1, 2, group1);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GREquipCode", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 10, 0, 1, 2, group1);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GREquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 11, 0, 1, 2, group1);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GPInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 12, 0, 1, 2, group2);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GPEquipCode", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //     , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 13, 0, 1, 2, group2);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GPEquipName", "교체설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //     , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 14, 0, 1, 2, group2);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GRQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //     , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 15, 0, 1, 2, group3);

                //grd.mfSetGridColumn(this.uGrid1, 0, "GPQty", "출고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 16, 0, 1, 2, group3);

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄추가
                grd.mfAddRowGrid(this.uGrid1, 0);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region 툴바관련

        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                WinGrid wGrid = new WinGrid();

                

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

               

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                if (this.uTextDurableMatCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M001181"
                                                , Infragistics.Win.HAlign.Right);
                    this.uTextDurableMatCode.Focus();
                    return;

                }

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;
                string strLotNo = this.uComboLotNo.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);
                DataTable dtDurableStockMoveHist = clsDurableStockMoveHist.mfReadDMMDurableStockMoveHist_Display(strPlantCode, strDurableMatCode,strLotNo, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtDurableStockMoveHist;
                uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtDurableStockMoveHist.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

         
        }
        public void mfSave()
        {
            try
            {
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {

            }
            catch
            { }
            finally
            { }
        }
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        private void uTextDurableMatName_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uTextDurableMatName.Text.Equals(string.Empty))
                {
                    this.uComboLotNo.Items.Clear();
                    return;
                }

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinGrid wGrid = new WinGrid();
                WinComboEditor wCombo = new WinComboEditor();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableStockMoveHist");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                if (dtDurableMat.Rows.Count > 0)
                {
                    if (dtDurableMat.Rows[0]["SerialFlag"].ToString() == "T")
                    {
                        this.uComboLotNo.Items.Clear();

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                        QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                        brwChannel.mfCredentials(clsDurableLot);

                        DataTable dtDurableLot = clsDurableLot.mfReadMASDurableLot_Combo(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                        wCombo.mfSetComboEditor(this.uComboLotNo, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                            , "LotNo", "LotNo1", dtDurableLot);

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDurableMatCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                    DialogResult dir = new DialogResult();

                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                    , Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;

                    }


                    //mfCreate();
                    frmPOP0007 frm = new frmPOP0007();
                    frm.PlantCode = this.uComboPlant.Value.ToString();
                    frm.ShowDialog();

                    //uTextPlantCode.Text = frm.PlantCode;
                    //uTextPlantName.Text = frm.PlantName;
                    uTextDurableMatCode.Text = frm.DurableMatCode;
                    uTextDurableMatName.Text = frm.DurableMatName;
               
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //금형치공구정보 키다운 설정
        private void uTextSearchDurableMat_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //코드를 지우면 명이지워진다.
                if (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    if (!this.uTextDurableMatName.Text.Equals(string.Empty))
                        this.uTextDurableMatName.Clear();
                }

                if (e.KeyData.Equals(Keys.Enter))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (!this.uTextDurableMatCode.Text.Equals(string.Empty))
                    {
                        //치공구코드저장
                        string strDurable = this.uTextDurableMatCode.Text;

                        //공장이 널이면
                        if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                            return;
                        }
                        //공장 치공구 코드 저장
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string strDurableCode = this.uTextDurableMatCode.Text;

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                        QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                        brwChannel.mfCredentials(clsDurableMat);

                        DataTable dtDurableMat = clsDurableMat.mfReadDurableMatName(strPlantCode, strDurableCode, m_resSys.GetString("SYS_LANG"));

                        if (dtDurableMat.Rows.Count > 0)
                        {
                            this.uTextDurableMatName.Text = dtDurableMat.Rows[0]["DurableMatName"].ToString();

                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001116", "M000894", Infragistics.Win.HAlign.Right);

                            this.uTextDurableMatName.Clear();
                            return;
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDurableMatCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextDurableMatName.Text.Equals(string.Empty))
                this.uTextDurableMatName.Clear();
        }
    }
}
