﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0013_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0013_S));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupBoxLot = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableGRD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextMoveChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMoveChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoveChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uDateMoveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLot)).BeginInit();
            this.uGroupBoxLot.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableGRD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoveChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoveChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMoveDate)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGroupBoxLot);
            this.uGroupBox1.Controls.Add(this.uComboInventory);
            this.uGroupBox1.Controls.Add(this.uLabelInventory);
            this.uGroupBox1.Controls.Add(this.uGridDurableList);
            this.uGroupBox1.Controls.Add(this.uTextMoveChargeName);
            this.uGroupBox1.Controls.Add(this.uTextMoveChargeID);
            this.uGroupBox1.Controls.Add(this.uLabelMoveChargeID);
            this.uGroupBox1.Controls.Add(this.uDateMoveDate);
            this.uGroupBox1.Controls.Add(this.uLabelRegisterDate);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox1.TabIndex = 2;
            // 
            // uGroupBoxLot
            // 
            this.uGroupBoxLot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxLot.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxLot.ExpandedSize = new System.Drawing.Size(1040, 636);
            this.uGroupBoxLot.Location = new System.Drawing.Point(12, 112);
            this.uGroupBoxLot.Name = "uGroupBoxLot";
            this.uGroupBoxLot.Size = new System.Drawing.Size(1040, 636);
            this.uGroupBoxLot.TabIndex = 6;
            this.uGroupBoxLot.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxLot_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonOK);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridDurableGRD);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1034, 616);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uButtonOK
            // 
            appearance3.FontData.BoldAsString = "True";
            this.uButtonOK.Appearance = appearance3;
            this.uButtonOK.Location = new System.Drawing.Point(204, 8);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 112;
            this.uButtonOK.Text = "확인";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonDeleteRow
            // 
            appearance20.FontData.BoldAsString = "True";
            this.uButtonDeleteRow.Appearance = appearance20;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(112, 8);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 111;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 12);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 108;
            this.uLabel7.Text = "ultraLabel7";
            this.uLabel7.Visible = false;
            // 
            // uGridDurableGRD
            // 
            this.uGridDurableGRD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableGRD.DisplayLayout.Appearance = appearance21;
            this.uGridDurableGRD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableGRD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableGRD.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.uGridDurableGRD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableGRD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableGRD.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableGRD.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridDurableGRD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableGRD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableGRD.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridDurableGRD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableGRD.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableGRD.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridDurableGRD.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridDurableGRD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableGRD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableGRD.DisplayLayout.Override.RowAppearance = appearance33;
            this.uGridDurableGRD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableGRD.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridDurableGRD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableGRD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableGRD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableGRD.Location = new System.Drawing.Point(8, 36);
            this.uGridDurableGRD.Name = "uGridDurableGRD";
            this.uGridDurableGRD.Size = new System.Drawing.Size(1014, 572);
            this.uGridDurableGRD.TabIndex = 16;
            this.uGridDurableGRD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableGRD_AfterCellUpdate);
            // 
            // uComboInventory
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.Appearance = appearance17;
            this.uComboInventory.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboInventory.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboInventory.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboInventory.Location = new System.Drawing.Point(672, 12);
            this.uComboInventory.MaxLength = 50;
            this.uComboInventory.Name = "uComboInventory";
            this.uComboInventory.Size = new System.Drawing.Size(116, 19);
            this.uComboInventory.TabIndex = 98;
            this.uComboInventory.Text = "ultraComboEditor1";
            // 
            // uLabelInventory
            // 
            this.uLabelInventory.Location = new System.Drawing.Point(568, 12);
            this.uLabelInventory.Name = "uLabelInventory";
            this.uLabelInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelInventory.TabIndex = 97;
            this.uLabelInventory.Text = "ultraLabel1";
            // 
            // uGridDurableList
            // 
            this.uGridDurableList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableList.DisplayLayout.Appearance = appearance6;
            this.uGridDurableList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridDurableList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableList.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridDurableList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableList.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableList.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridDurableList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableList.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridDurableList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableList.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableList.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridDurableList.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridDurableList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableList.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridDurableList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableList.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGridDurableList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableList.Location = new System.Drawing.Point(12, 40);
            this.uGridDurableList.Name = "uGridDurableList";
            this.uGridDurableList.Size = new System.Drawing.Size(1040, 690);
            this.uGridDurableList.TabIndex = 5;
            this.uGridDurableList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGridDurableList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_ClickCellButton);
            this.uGridDurableList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uTextMoveChargeName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoveChargeName.Appearance = appearance5;
            this.uTextMoveChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoveChargeName.Location = new System.Drawing.Point(444, 12);
            this.uTextMoveChargeName.Name = "uTextMoveChargeName";
            this.uTextMoveChargeName.ReadOnly = true;
            this.uTextMoveChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextMoveChargeName.TabIndex = 4;
            // 
            // uTextMoveChargeID
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMoveChargeID.Appearance = appearance19;
            this.uTextMoveChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance4.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMoveChargeID.ButtonsRight.Add(editorButton1);
            this.uTextMoveChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMoveChargeID.Location = new System.Drawing.Point(340, 12);
            this.uTextMoveChargeID.MaxLength = 20;
            this.uTextMoveChargeID.Name = "uTextMoveChargeID";
            this.uTextMoveChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextMoveChargeID.TabIndex = 3;
            this.uTextMoveChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextMoveChargeID_KeyDown);
            this.uTextMoveChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMoveChargeID_EditorButtonClick);
            // 
            // uLabelMoveChargeID
            // 
            this.uLabelMoveChargeID.Location = new System.Drawing.Point(236, 12);
            this.uLabelMoveChargeID.Name = "uLabelMoveChargeID";
            this.uLabelMoveChargeID.Size = new System.Drawing.Size(100, 20);
            this.uLabelMoveChargeID.TabIndex = 2;
            this.uLabelMoveChargeID.Text = "ultraLabel1";
            // 
            // uDateMoveDate
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateMoveDate.Appearance = appearance2;
            this.uDateMoveDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateMoveDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateMoveDate.Location = new System.Drawing.Point(116, 12);
            this.uDateMoveDate.Name = "uDateMoveDate";
            this.uDateMoveDate.Size = new System.Drawing.Size(100, 21);
            this.uDateMoveDate.TabIndex = 1;
            // 
            // uLabelRegisterDate
            // 
            this.uLabelRegisterDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelRegisterDate.Name = "uLabelRegisterDate";
            this.uLabelRegisterDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRegisterDate.TabIndex = 0;
            this.uLabelRegisterDate.Text = "ultraLabel1";
            // 
            // frmDMMZ0013_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0013_S";
            this.Load += new System.EventHandler(this.frmDMMZ0013_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0013_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0013_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMMZ0013_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLot)).EndInit();
            this.uGroupBoxLot.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableGRD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoveChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoveChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMoveDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelRegisterDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoveChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoveChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelMoveChargeID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateMoveDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelInventory;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxLot;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableGRD;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
    }
}