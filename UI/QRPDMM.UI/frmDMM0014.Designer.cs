﻿namespace QRPDMM.UI
{
    partial class frmDMM0014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0014));
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckUsageClearFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboRepairResultCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelFinishDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateStandbyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextJudgeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudgeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckFinishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckRepairFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckStandbyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelInputUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudgeChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairResultCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandbyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCurUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCumUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboRepairGubunCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextRepairCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairGubunCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlantName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairGubunCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 14;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox3.Controls.Add(this.uGrid2);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 228);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 416);
            this.uGroupBox3.TabIndex = 66;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 1;
            this.uButtonDeleteRow.Text = "ultraButton1";
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance26;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance35.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 40);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1016, 368);
            this.uGrid2.TabIndex = 0;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uCheckUsageClearFlag);
            this.uGroupBox2.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBox2.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox2.Controls.Add(this.uComboRepairResultCode);
            this.uGroupBox2.Controls.Add(this.uDateFinishDate);
            this.uGroupBox2.Controls.Add(this.uDateRepairDate);
            this.uGroupBox2.Controls.Add(this.uLabelFinishDate);
            this.uGroupBox2.Controls.Add(this.uDateStandbyDate);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeName);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uTextUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uCheckFinishFlag);
            this.uGroupBox2.Controls.Add(this.uCheckRepairFlag);
            this.uGroupBox2.Controls.Add(this.uCheckStandbyFlag);
            this.uGroupBox2.Controls.Add(this.uLabelInputUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uLabelJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox2.Controls.Add(this.uLabelRepairResultCode);
            this.uGroupBox2.Controls.Add(this.uLabelRepairDate);
            this.uGroupBox2.Controls.Add(this.uLabelStandbyDate);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 120);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBox2.TabIndex = 65;
            // 
            // uCheckUsageClearFlag
            // 
            this.uCheckUsageClearFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckUsageClearFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUsageClearFlag.Location = new System.Drawing.Point(868, 52);
            this.uCheckUsageClearFlag.Name = "uCheckUsageClearFlag";
            this.uCheckUsageClearFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckUsageClearFlag.TabIndex = 76;
            this.uCheckUsageClearFlag.Text = "초기화";
            this.uCheckUsageClearFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(868, 28);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckChangeFlag.TabIndex = 75;
            this.uCheckChangeFlag.Text = "교체여부";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(128, 76);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(316, 21);
            this.uTextEtcDesc.TabIndex = 74;
            // 
            // uComboRepairResultCode
            // 
            this.uComboRepairResultCode.Location = new System.Drawing.Point(868, 76);
            this.uComboRepairResultCode.Name = "uComboRepairResultCode";
            this.uComboRepairResultCode.Size = new System.Drawing.Size(100, 21);
            this.uComboRepairResultCode.TabIndex = 73;
            this.uComboRepairResultCode.AfterCloseUp += new System.EventHandler(this.uComboRepairResultCode_AfterCloseUp);
            this.uComboRepairResultCode.ValueChanged += new System.EventHandler(this.uComboRepairResultCode_ValueChanged);
            // 
            // uDateFinishDate
            // 
            this.uDateFinishDate.Location = new System.Drawing.Point(644, 76);
            this.uDateFinishDate.Name = "uDateFinishDate";
            this.uDateFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFinishDate.TabIndex = 72;
            // 
            // uDateRepairDate
            // 
            this.uDateRepairDate.Location = new System.Drawing.Point(644, 52);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairDate.TabIndex = 72;
            // 
            // uLabelFinishDate
            // 
            this.uLabelFinishDate.Location = new System.Drawing.Point(528, 76);
            this.uLabelFinishDate.Name = "uLabelFinishDate";
            this.uLabelFinishDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelFinishDate.TabIndex = 56;
            this.uLabelFinishDate.Text = "19";
            // 
            // uDateStandbyDate
            // 
            this.uDateStandbyDate.Location = new System.Drawing.Point(644, 28);
            this.uDateStandbyDate.Name = "uDateStandbyDate";
            this.uDateStandbyDate.Size = new System.Drawing.Size(100, 21);
            this.uDateStandbyDate.TabIndex = 72;
            // 
            // uTextJudgeChargeName
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Appearance = appearance8;
            this.uTextJudgeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Location = new System.Drawing.Point(232, 52);
            this.uTextJudgeChargeName.Name = "uTextJudgeChargeName";
            this.uTextJudgeChargeName.ReadOnly = true;
            this.uTextJudgeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeName.TabIndex = 70;
            // 
            // uTextJudgeChargeID
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeID.Appearance = appearance15;
            this.uTextJudgeChargeID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeID.Location = new System.Drawing.Point(128, 52);
            this.uTextJudgeChargeID.Name = "uTextJudgeChargeID";
            this.uTextJudgeChargeID.ReadOnly = true;
            this.uTextJudgeChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeID.TabIndex = 70;
            // 
            // uTextUsageJudgeDate
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageJudgeDate.Appearance = appearance16;
            this.uTextUsageJudgeDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageJudgeDate.Location = new System.Drawing.Point(128, 28);
            this.uTextUsageJudgeDate.Name = "uTextUsageJudgeDate";
            this.uTextUsageJudgeDate.ReadOnly = true;
            this.uTextUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageJudgeDate.TabIndex = 70;
            // 
            // uCheckFinishFlag
            // 
            this.uCheckFinishFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckFinishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFinishFlag.Location = new System.Drawing.Point(452, 76);
            this.uCheckFinishFlag.Name = "uCheckFinishFlag";
            this.uCheckFinishFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckFinishFlag.TabIndex = 71;
            this.uCheckFinishFlag.Text = "정비완료";
            this.uCheckFinishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckFinishFlag.CheckedChanged += new System.EventHandler(this.uCheckFinishFlag_CheckedChanged);
            // 
            // uCheckRepairFlag
            // 
            this.uCheckRepairFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckRepairFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRepairFlag.Location = new System.Drawing.Point(452, 52);
            this.uCheckRepairFlag.Name = "uCheckRepairFlag";
            this.uCheckRepairFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckRepairFlag.TabIndex = 71;
            this.uCheckRepairFlag.Text = "정비착수";
            this.uCheckRepairFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckRepairFlag.CheckedChanged += new System.EventHandler(this.uCheckRepairFlag_CheckedChanged);
            // 
            // uCheckStandbyFlag
            // 
            this.uCheckStandbyFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckStandbyFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckStandbyFlag.Location = new System.Drawing.Point(452, 28);
            this.uCheckStandbyFlag.Name = "uCheckStandbyFlag";
            this.uCheckStandbyFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckStandbyFlag.TabIndex = 71;
            this.uCheckStandbyFlag.Text = "정비대기";
            this.uCheckStandbyFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uLabelInputUsageJudgeDate
            // 
            this.uLabelInputUsageJudgeDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelInputUsageJudgeDate.Name = "uLabelInputUsageJudgeDate";
            this.uLabelInputUsageJudgeDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelInputUsageJudgeDate.TabIndex = 53;
            this.uLabelInputUsageJudgeDate.Text = "15";
            // 
            // uLabelJudgeChargeID
            // 
            this.uLabelJudgeChargeID.Location = new System.Drawing.Point(12, 52);
            this.uLabelJudgeChargeID.Name = "uLabelJudgeChargeID";
            this.uLabelJudgeChargeID.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudgeChargeID.TabIndex = 54;
            this.uLabelJudgeChargeID.Text = "16";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 76);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 56;
            this.uLabelEtcDesc.Text = "21";
            // 
            // uLabelRepairResultCode
            // 
            this.uLabelRepairResultCode.Location = new System.Drawing.Point(752, 76);
            this.uLabelRepairResultCode.Name = "uLabelRepairResultCode";
            this.uLabelRepairResultCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairResultCode.TabIndex = 56;
            this.uLabelRepairResultCode.Text = "20";
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(528, 52);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairDate.TabIndex = 53;
            this.uLabelRepairDate.Text = "18";
            // 
            // uLabelStandbyDate
            // 
            this.uLabelStandbyDate.Location = new System.Drawing.Point(528, 28);
            this.uLabelStandbyDate.Name = "uLabelStandbyDate";
            this.uLabelStandbyDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelStandbyDate.TabIndex = 55;
            this.uLabelStandbyDate.Text = "17";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextLotNo);
            this.uGroupBox1.Controls.Add(this.uLabelLotNo);
            this.uGroupBox1.Controls.Add(this.uTextCurUsage);
            this.uGroupBox1.Controls.Add(this.uTextCumUsage);
            this.uGroupBox1.Controls.Add(this.uTextUsageDocCode);
            this.uGroupBox1.Controls.Add(this.uTextPlantCode);
            this.uGroupBox1.Controls.Add(this.uComboRepairGubunCode);
            this.uGroupBox1.Controls.Add(this.uTextRepairCode);
            this.uGroupBox1.Controls.Add(this.uLabelRepairCode);
            this.uGroupBox1.Controls.Add(this.uLabelRepairGubunCode);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uTextSpec);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatName);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uTextPlantName);
            this.uGroupBox1.Controls.Add(this.uLabelPlantName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBox1.TabIndex = 64;
            // 
            // uTextLotNo
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance41;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Location = new System.Drawing.Point(868, 52);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextLotNo.TabIndex = 80;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(752, 52);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelLotNo.TabIndex = 79;
            this.uLabelLotNo.Text = "11";
            // 
            // uTextCurUsage
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Appearance = appearance6;
            this.uTextCurUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Location = new System.Drawing.Point(796, 16);
            this.uTextCurUsage.Name = "uTextCurUsage";
            this.uTextCurUsage.ReadOnly = true;
            this.uTextCurUsage.Size = new System.Drawing.Size(40, 21);
            this.uTextCurUsage.TabIndex = 78;
            // 
            // uTextCumUsage
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Appearance = appearance43;
            this.uTextCumUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Location = new System.Drawing.Point(752, 16);
            this.uTextCumUsage.Name = "uTextCumUsage";
            this.uTextCumUsage.ReadOnly = true;
            this.uTextCumUsage.Size = new System.Drawing.Size(40, 21);
            this.uTextCumUsage.TabIndex = 77;
            // 
            // uTextUsageDocCode
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageDocCode.Appearance = appearance44;
            this.uTextUsageDocCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageDocCode.Location = new System.Drawing.Point(648, 16);
            this.uTextUsageDocCode.Name = "uTextUsageDocCode";
            this.uTextUsageDocCode.ReadOnly = true;
            this.uTextUsageDocCode.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageDocCode.TabIndex = 76;
            // 
            // uTextPlantCode
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance40;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(548, 16);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 75;
            // 
            // uComboRepairGubunCode
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboRepairGubunCode.Appearance = appearance10;
            this.uComboRepairGubunCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboRepairGubunCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboRepairGubunCode.Location = new System.Drawing.Point(368, 28);
            this.uComboRepairGubunCode.Name = "uComboRepairGubunCode";
            this.uComboRepairGubunCode.Size = new System.Drawing.Size(120, 21);
            this.uComboRepairGubunCode.TabIndex = 73;
            this.uComboRepairGubunCode.Text = "ultraComboEditor1";
            // 
            // uTextRepairCode
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairCode.Appearance = appearance39;
            this.uTextRepairCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairCode.Location = new System.Drawing.Point(128, 28);
            this.uTextRepairCode.Name = "uTextRepairCode";
            this.uTextRepairCode.ReadOnly = true;
            this.uTextRepairCode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairCode.TabIndex = 74;
            // 
            // uLabelRepairCode
            // 
            this.uLabelRepairCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairCode.Name = "uLabelRepairCode";
            this.uLabelRepairCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairCode.TabIndex = 71;
            this.uLabelRepairCode.Text = "6";
            // 
            // uLabelRepairGubunCode
            // 
            this.uLabelRepairGubunCode.Location = new System.Drawing.Point(252, 28);
            this.uLabelRepairGubunCode.Name = "uLabelRepairGubunCode";
            this.uLabelRepairGubunCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairGubunCode.TabIndex = 72;
            this.uLabelRepairGubunCode.Text = "7";
            // 
            // uTextEquipName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance17;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(368, 76);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(120, 21);
            this.uTextEquipName.TabIndex = 70;
            // 
            // uTextEquipCode
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance18;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(128, 76);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 70;
            // 
            // uTextSpec
            // 
            appearance45.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance45;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(628, 76);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextSpec.TabIndex = 70;
            // 
            // uTextDurableMatName
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance42;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(628, 52);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 70;
            // 
            // uTextDurableMatCode
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance4;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(368, 52);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(120, 21);
            this.uTextDurableMatCode.TabIndex = 69;
            // 
            // uTextPlantName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance27;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(128, 52);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantName.TabIndex = 68;
            // 
            // uLabelPlantName
            // 
            this.uLabelPlantName.Location = new System.Drawing.Point(12, 52);
            this.uLabelPlantName.Name = "uLabelPlantName";
            this.uLabelPlantName.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlantName.TabIndex = 62;
            this.uLabelPlantName.Text = "8";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(252, 76);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipName.TabIndex = 53;
            this.uLabelEquipName.Text = "14";
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(252, 52);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatCode.TabIndex = 49;
            this.uLabelDurableMatCode.Text = "9";
            // 
            // uLabelDurableMatName
            // 
            this.uLabelDurableMatName.Location = new System.Drawing.Point(512, 52);
            this.uLabelDurableMatName.Name = "uLabelDurableMatName";
            this.uLabelDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatName.TabIndex = 50;
            this.uLabelDurableMatName.Text = "10";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(12, 76);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipCode.TabIndex = 52;
            this.uLabelEquipCode.Text = "13";
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(512, 76);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 50;
            this.uLabelSpec.Text = "11";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance20;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance38.BackColor = System.Drawing.SystemColors.Highlight;
            appearance38.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance38;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance22;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 720);
            this.uGrid1.TabIndex = 13;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance7;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToUsageJudgeDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromUsageJudgeDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelUsageJudgeDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 12;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(536, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 64;
            this.ultraLabel1.Text = "~";
            // 
            // uTextSearchDurableMatName
            // 
            this.uTextSearchDurableMatName.Location = new System.Drawing.Point(436, 36);
            this.uTextSearchDurableMatName.Name = "uTextSearchDurableMatName";
            this.uTextSearchDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMatName.TabIndex = 63;
            // 
            // uTextSearchEquipName
            // 
            this.uTextSearchEquipName.Location = new System.Drawing.Point(896, 12);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipName.TabIndex = 62;
            // 
            // uTextSearchEquipCode
            // 
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance1;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(792, 12);
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 61;
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uTextUserName
            // 
            this.uTextUserName.Location = new System.Drawing.Point(896, 36);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 60;
            // 
            // uLabelUserID
            // 
            this.uLabelUserID.Location = new System.Drawing.Point(675, 35);
            this.uLabelUserID.Name = "uLabelUserID";
            this.uLabelUserID.Size = new System.Drawing.Size(110, 20);
            this.uLabelUserID.TabIndex = 59;
            this.uLabelUserID.Text = "5";
            // 
            // uTextUserID
            // 
            appearance19.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton2);
            this.uTextUserID.Location = new System.Drawing.Point(792, 36);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 59;
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(319, 35);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMatName.TabIndex = 57;
            this.uLabelSearchDurableMatName.Text = "3";
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(676, 12);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchEquip.TabIndex = 58;
            this.uLabelSearchEquip.Text = "4";
            // 
            // uDateToUsageJudgeDate
            // 
            this.uDateToUsageJudgeDate.Location = new System.Drawing.Point(552, 12);
            this.uDateToUsageJudgeDate.Name = "uDateToUsageJudgeDate";
            this.uDateToUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToUsageJudgeDate.TabIndex = 38;
            // 
            // uDateFromUsageJudgeDate
            // 
            this.uDateFromUsageJudgeDate.Location = new System.Drawing.Point(436, 12);
            this.uDateFromUsageJudgeDate.Name = "uDateFromUsageJudgeDate";
            this.uDateFromUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromUsageJudgeDate.TabIndex = 38;
            // 
            // uComboPlant
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance37;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 37;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchDurableMatCode
            // 
            this.uLabelSearchDurableMatCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchDurableMatCode.Name = "uLabelSearchDurableMatCode";
            this.uLabelSearchDurableMatCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMatCode.TabIndex = 11;
            this.uLabelSearchDurableMatCode.Text = "2";
            // 
            // uTextSearchDurableMatCode
            // 
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance28;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDurableMatCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchDurableMatCode.Location = new System.Drawing.Point(128, 36);
            this.uTextSearchDurableMatCode.Name = "uTextSearchDurableMatCode";
            this.uTextSearchDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMatCode.TabIndex = 10;
            this.uTextSearchDurableMatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchDurableMatCode_KeyDown);
            this.uTextSearchDurableMatCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDurableMatCode_EditorButtonClick);
            // 
            // uLabelUsageJudgeDate
            // 
            this.uLabelUsageJudgeDate.Location = new System.Drawing.Point(320, 12);
            this.uLabelUsageJudgeDate.Name = "uLabelUsageJudgeDate";
            this.uLabelUsageJudgeDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageJudgeDate.TabIndex = 9;
            this.uLabelUsageJudgeDate.Text = "1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 11;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMM0014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0014";
            this.Load += new System.EventHandler(this.frmDMM0014_Load);
            this.Activated += new System.EventHandler(this.frmDMM0014_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMM0014_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMM0014_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairGubunCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResultCode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.Misc.UltraLabel uLabelFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFinishFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRepairFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStandbyFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInputUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelJudgeChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStandbyDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairGubunCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairGubunCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUsageClearFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageDocCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCurUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCumUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
    }
}