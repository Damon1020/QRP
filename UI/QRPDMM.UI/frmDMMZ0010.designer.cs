﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0010));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFinishDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCurUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCumUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairGIReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairGIReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateRepairGIReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairGubunName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairReqCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.utextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairGICode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairGICode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairGIReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairGIReqID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlantName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairCode = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairGIReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGubunName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.utextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance7;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToFinishDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromFinishDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelFinishDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 10;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(228, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 65;
            this.ultraLabel1.Text = "~";
            // 
            // uTextSearchDurableMatName
            // 
            this.uTextSearchDurableMatName.Location = new System.Drawing.Point(468, 37);
            this.uTextSearchDurableMatName.MaxLength = 50;
            this.uTextSearchDurableMatName.Name = "uTextSearchDurableMatName";
            this.uTextSearchDurableMatName.Size = new System.Drawing.Size(140, 21);
            this.uTextSearchDurableMatName.TabIndex = 63;
            this.uTextSearchDurableMatName.ValueChanged += new System.EventHandler(this.ultraTextEditor4_ValueChanged);
            // 
            // uTextSearchEquipName
            // 
            this.uTextSearchEquipName.Location = new System.Drawing.Point(852, 12);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipName.TabIndex = 62;
            // 
            // uTextSearchEquipCode
            // 
            appearance30.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance30.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance30;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(748, 12);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 61;
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uTextSearchUserName
            // 
            this.uTextSearchUserName.Location = new System.Drawing.Point(852, 36);
            this.uTextSearchUserName.Name = "uTextSearchUserName";
            this.uTextSearchUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchUserName.TabIndex = 60;
            // 
            // uLabelSearchUser
            // 
            this.uLabelSearchUser.Location = new System.Drawing.Point(632, 36);
            this.uLabelSearchUser.Name = "uLabelSearchUser";
            this.uLabelSearchUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchUser.TabIndex = 59;
            this.uLabelSearchUser.Text = "5";
            // 
            // uTextSearchUserID
            // 
            appearance19.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchUserID.ButtonsRight.Add(editorButton2);
            this.uTextSearchUserID.Location = new System.Drawing.Point(748, 36);
            this.uTextSearchUserID.MaxLength = 20;
            this.uTextSearchUserID.Name = "uTextSearchUserID";
            this.uTextSearchUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchUserID.TabIndex = 59;
            this.uTextSearchUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchUserID_KeyDown);
            this.uTextSearchUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchUserID_EditorButtonClick);
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(352, 36);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMatName.TabIndex = 57;
            this.uLabelSearchDurableMatName.Text = "3";
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(632, 12);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchEquip.TabIndex = 58;
            this.uLabelSearchEquip.Text = "4";
            // 
            // uDateSearchToFinishDate
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToFinishDate.Appearance = appearance2;
            this.uDateSearchToFinishDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToFinishDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToFinishDate.Location = new System.Drawing.Point(240, 12);
            this.uDateSearchToFinishDate.Name = "uDateSearchToFinishDate";
            this.uDateSearchToFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToFinishDate.TabIndex = 38;
            // 
            // uDateSearchFromFinishDate
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromFinishDate.Appearance = appearance3;
            this.uDateSearchFromFinishDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromFinishDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromFinishDate.Location = new System.Drawing.Point(128, 12);
            this.uDateSearchFromFinishDate.Name = "uDateSearchFromFinishDate";
            this.uDateSearchFromFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromFinishDate.TabIndex = 38;
            // 
            // uComboPlant
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance37;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(468, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 37;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchDurableMatCode
            // 
            this.uLabelSearchDurableMatCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchDurableMatCode.Name = "uLabelSearchDurableMatCode";
            this.uLabelSearchDurableMatCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMatCode.TabIndex = 11;
            this.uLabelSearchDurableMatCode.Text = "2";
            // 
            // uTextSearchDurableMatCode
            // 
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance28;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDurableMatCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchDurableMatCode.Location = new System.Drawing.Point(128, 36);
            this.uTextSearchDurableMatCode.MaxLength = 20;
            this.uTextSearchDurableMatCode.Name = "uTextSearchDurableMatCode";
            this.uTextSearchDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMatCode.TabIndex = 10;
            this.uTextSearchDurableMatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchDurableMatCode_KeyDown);
            this.uTextSearchDurableMatCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDurableMatCode_EditorButtonClick);
            // 
            // uLabelFinishDate
            // 
            this.uLabelFinishDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelFinishDate.Name = "uLabelFinishDate";
            this.uLabelFinishDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelFinishDate.TabIndex = 9;
            this.uLabelFinishDate.Text = "1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(352, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance17;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance32.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance32;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 715);
            this.uGrid1.TabIndex = 11;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 12;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uGrid3);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 420);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 224);
            this.uGroupBox3.TabIndex = 73;
            // 
            // uGrid3
            // 
            this.uGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance33;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance34;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance35;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance36.BackColor2 = System.Drawing.SystemColors.Control;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance36;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance58;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            appearance41.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance41;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance59.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance59;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            appearance60.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance60;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(12, 6);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(1020, 210);
            this.uGrid3.TabIndex = 1;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 192);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 224);
            this.uGroupBox2.TabIndex = 73;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance46;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance47;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance48;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance49.BackColor2 = System.Drawing.SystemColors.Control;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance49;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance50;
            appearance51.BackColor = System.Drawing.SystemColors.Highlight;
            appearance51.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance51;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance52;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            appearance53.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance53;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance54.BackColor = System.Drawing.SystemColors.Control;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance54;
            appearance55.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance55;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance56;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance57.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance57;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 40);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1020, 176);
            this.uGrid2.TabIndex = 1;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellListSelect);
            this.uGrid2.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellListCloseUp);
            this.uGrid2.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGrid2_BeforeCellListDropDown);
            this.uGrid2.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGrid2_AfterRowInsert);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 0;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextLotNo);
            this.uGroupBox1.Controls.Add(this.uLabelLotNo);
            this.uGroupBox1.Controls.Add(this.uTextCurUsage);
            this.uGroupBox1.Controls.Add(this.uTextCumUsage);
            this.uGroupBox1.Controls.Add(this.uTextUsageDocCode);
            this.uGroupBox1.Controls.Add(this.uTextPlantCode);
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uTextRepairGIReqName);
            this.uGroupBox1.Controls.Add(this.uTextRepairGIReqID);
            this.uGroupBox1.Controls.Add(this.uDateRepairGIReqDate);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatName);
            this.uGroupBox1.Controls.Add(this.uTextRepairGubunName);
            this.uGroupBox1.Controls.Add(this.uTextRepairReqCode);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uTextVendorName);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.utextEquipCode);
            this.uGroupBox1.Controls.Add(this.uTextSpec);
            this.uGroupBox1.Controls.Add(this.uTextPlantName);
            this.uGroupBox1.Controls.Add(this.uTextRepairGICode);
            this.uGroupBox1.Controls.Add(this.uLabelRepairGICode);
            this.uGroupBox1.Controls.Add(this.uLabelRepairGIReqDate);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelRepairGIReqID);
            this.uGroupBox1.Controls.Add(this.uLabelRepairGubun);
            this.uGroupBox1.Controls.Add(this.uLabelVendor);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Controls.Add(this.uLabelPlantName);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatName);
            this.uGroupBox1.Controls.Add(this.uLabelRepairCode);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 176);
            this.uGroupBox1.TabIndex = 72;
            // 
            // uTextLotNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance8;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Location = new System.Drawing.Point(748, 76);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(136, 21);
            this.uTextLotNo.TabIndex = 90;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(632, 76);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelLotNo.TabIndex = 89;
            this.uLabelLotNo.Text = "11";
            // 
            // uTextCurUsage
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Appearance = appearance6;
            this.uTextCurUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Location = new System.Drawing.Point(968, 124);
            this.uTextCurUsage.Name = "uTextCurUsage";
            this.uTextCurUsage.ReadOnly = true;
            this.uTextCurUsage.Size = new System.Drawing.Size(40, 21);
            this.uTextCurUsage.TabIndex = 88;
            // 
            // uTextCumUsage
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Appearance = appearance43;
            this.uTextCumUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Location = new System.Drawing.Point(924, 124);
            this.uTextCumUsage.Name = "uTextCumUsage";
            this.uTextCumUsage.ReadOnly = true;
            this.uTextCumUsage.Size = new System.Drawing.Size(40, 21);
            this.uTextCumUsage.TabIndex = 87;
            // 
            // uTextUsageDocCode
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageDocCode.Appearance = appearance44;
            this.uTextUsageDocCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageDocCode.Location = new System.Drawing.Point(820, 124);
            this.uTextUsageDocCode.Name = "uTextUsageDocCode";
            this.uTextUsageDocCode.ReadOnly = true;
            this.uTextUsageDocCode.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageDocCode.TabIndex = 86;
            // 
            // uTextPlantCode
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance40;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(720, 124);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 85;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(128, 148);
            this.uTextEtcDesc.MaxLength = 100;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(496, 21);
            this.uTextEtcDesc.TabIndex = 84;
            // 
            // uTextRepairGIReqName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGIReqName.Appearance = appearance15;
            this.uTextRepairGIReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGIReqName.Location = new System.Drawing.Point(500, 124);
            this.uTextRepairGIReqName.Name = "uTextRepairGIReqName";
            this.uTextRepairGIReqName.ReadOnly = true;
            this.uTextRepairGIReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGIReqName.TabIndex = 83;
            // 
            // uTextRepairGIReqID
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairGIReqID.Appearance = appearance27;
            this.uTextRepairGIReqID.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance1;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairGIReqID.ButtonsRight.Add(editorButton4);
            this.uTextRepairGIReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairGIReqID.Location = new System.Drawing.Point(396, 124);
            this.uTextRepairGIReqID.MaxLength = 20;
            this.uTextRepairGIReqID.Name = "uTextRepairGIReqID";
            this.uTextRepairGIReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGIReqID.TabIndex = 82;
            this.uTextRepairGIReqID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRepairGIReqID_KeyDown);
            this.uTextRepairGIReqID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRepairGIReqID_EditorButtonClick);
            // 
            // uDateRepairGIReqDate
            // 
            this.uDateRepairGIReqDate.Location = new System.Drawing.Point(128, 124);
            this.uDateRepairGIReqDate.Name = "uDateRepairGIReqDate";
            this.uDateRepairGIReqDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairGIReqDate.TabIndex = 81;
            // 
            // uTextDurableMatName
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance61;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(748, 52);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 80;
            // 
            // uTextRepairGubunName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGubunName.Appearance = appearance18;
            this.uTextRepairGubunName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGubunName.Location = new System.Drawing.Point(748, 28);
            this.uTextRepairGubunName.Name = "uTextRepairGubunName";
            this.uTextRepairGubunName.ReadOnly = true;
            this.uTextRepairGubunName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGubunName.TabIndex = 80;
            // 
            // uTextRepairReqCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqCode.Appearance = appearance20;
            this.uTextRepairReqCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairReqCode.Location = new System.Drawing.Point(396, 28);
            this.uTextRepairReqCode.Name = "uTextRepairReqCode";
            this.uTextRepairReqCode.ReadOnly = true;
            this.uTextRepairReqCode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairReqCode.TabIndex = 80;
            // 
            // uTextDurableMatCode
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance21;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(396, 52);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(120, 21);
            this.uTextDurableMatCode.TabIndex = 80;
            // 
            // uTextVendorName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance22;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(396, 76);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(100, 21);
            this.uTextVendorName.TabIndex = 80;
            // 
            // uTextEquipName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance23;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(396, 100);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(204, 21);
            this.uTextEquipName.TabIndex = 80;
            // 
            // utextEquipCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.utextEquipCode.Appearance = appearance24;
            this.utextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.utextEquipCode.Location = new System.Drawing.Point(128, 100);
            this.utextEquipCode.Name = "utextEquipCode";
            this.utextEquipCode.ReadOnly = true;
            this.utextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.utextEquipCode.TabIndex = 80;
            // 
            // uTextSpec
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance25;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(128, 76);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextSpec.TabIndex = 80;
            // 
            // uTextPlantName
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance26;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(128, 52);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(128, 21);
            this.uTextPlantName.TabIndex = 80;
            // 
            // uTextRepairGICode
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Appearance = appearance29;
            this.uTextRepairGICode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Location = new System.Drawing.Point(128, 28);
            this.uTextRepairGICode.Name = "uTextRepairGICode";
            this.uTextRepairGICode.ReadOnly = true;
            this.uTextRepairGICode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGICode.TabIndex = 80;
            // 
            // uLabelRepairGICode
            // 
            this.uLabelRepairGICode.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairGICode.Name = "uLabelRepairGICode";
            this.uLabelRepairGICode.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairGICode.TabIndex = 63;
            this.uLabelRepairGICode.Text = "6";
            // 
            // uLabelRepairGIReqDate
            // 
            this.uLabelRepairGIReqDate.Location = new System.Drawing.Point(12, 124);
            this.uLabelRepairGIReqDate.Name = "uLabelRepairGIReqDate";
            this.uLabelRepairGIReqDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairGIReqDate.TabIndex = 75;
            this.uLabelRepairGIReqDate.Text = "16";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(280, 100);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipName.TabIndex = 73;
            this.uLabelEquipName.Text = "15";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 148);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 74;
            this.uLabelEtcDesc.Text = "18";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(12, 100);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipCode.TabIndex = 70;
            this.uLabelEquipCode.Text = "14";
            // 
            // uLabelRepairGIReqID
            // 
            this.uLabelRepairGIReqID.Location = new System.Drawing.Point(280, 124);
            this.uLabelRepairGIReqID.Name = "uLabelRepairGIReqID";
            this.uLabelRepairGIReqID.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairGIReqID.TabIndex = 76;
            this.uLabelRepairGIReqID.Text = "17";
            // 
            // uLabelRepairGubun
            // 
            this.uLabelRepairGubun.Location = new System.Drawing.Point(632, 28);
            this.uLabelRepairGubun.Name = "uLabelRepairGubun";
            this.uLabelRepairGubun.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairGubun.TabIndex = 71;
            this.uLabelRepairGubun.Text = "8";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(280, 76);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelVendor.TabIndex = 69;
            this.uLabelVendor.Text = "13";
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(280, 52);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatCode.TabIndex = 67;
            this.uLabelDurableMatCode.Text = "10";
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 76);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 68;
            this.uLabelSpec.Text = "12";
            // 
            // uLabelPlantName
            // 
            this.uLabelPlantName.Location = new System.Drawing.Point(12, 52);
            this.uLabelPlantName.Name = "uLabelPlantName";
            this.uLabelPlantName.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlantName.TabIndex = 65;
            this.uLabelPlantName.Text = "9";
            // 
            // uLabelDurableMatName
            // 
            this.uLabelDurableMatName.Location = new System.Drawing.Point(632, 52);
            this.uLabelDurableMatName.Name = "uLabelDurableMatName";
            this.uLabelDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatName.TabIndex = 66;
            this.uLabelDurableMatName.Text = "11";
            // 
            // uLabelRepairCode
            // 
            this.uLabelRepairCode.Location = new System.Drawing.Point(280, 28);
            this.uLabelRepairCode.Name = "uLabelRepairCode";
            this.uLabelRepairCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairCode.TabIndex = 64;
            this.uLabelRepairCode.Text = "7";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 9;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMMZ0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0010";
            this.Load += new System.EventHandler(this.frmDMMZ0010_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0010_Activated);
            this.Resize += new System.EventHandler(this.frmDMMZ0010_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGIReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairGIReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGubunName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairReqCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.utextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelFinishDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairGICode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairCode;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairGIReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairGIReqID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGubunName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairReqCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor utextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGICode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairGIReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGIReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGIReqID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCurUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCumUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageDocCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
    }
}