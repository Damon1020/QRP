﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0022));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboInstall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInstall = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxDurableLot = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableLotList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSpec = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateTimeUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextJudeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpecName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckStandbyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateStandbyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextSpecCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckFinishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextDurableMatLot = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandbyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatLot = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudeCharge = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckRepairFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelShot = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboShot = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGroupBoxChange = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridChangeDurable = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckUsageClearFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboRepairResultCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRepairResultCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFinishDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridSpec = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInstall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableLot)).BeginInit();
            this.uGroupBoxDurableLot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLotList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSpec)).BeginInit();
            this.uGroupBoxSpec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboShot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChange)).BeginInit();
            this.uGroupBoxChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChangeDurable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).BeginInit();
            this.uGroupBoxResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpec)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboInstall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelInstall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboInstall
            // 
            this.uComboInstall.Location = new System.Drawing.Point(716, 12);
            this.uComboInstall.Name = "uComboInstall";
            this.uComboInstall.Size = new System.Drawing.Size(144, 21);
            this.uComboInstall.TabIndex = 3;
            this.uComboInstall.Visible = false;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(424, 10);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackage.TabIndex = 2;
            this.uComboSearchPackage.Visible = false;
            // 
            // uLabelInstall
            // 
            this.uLabelInstall.Location = new System.Drawing.Point(588, 12);
            this.uLabelInstall.Name = "uLabelInstall";
            this.uLabelInstall.Size = new System.Drawing.Size(124, 20);
            this.uLabelInstall.TabIndex = 14;
            this.uLabelInstall.Visible = false;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(296, 10);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchPackage.TabIndex = 14;
            this.uLabelSearchPackage.Visible = false;
            // 
            // uComboSearchDurableMatName
            // 
            this.uComboSearchDurableMatName.Location = new System.Drawing.Point(140, 10);
            this.uComboSearchDurableMatName.MaxLength = 50;
            this.uComboSearchDurableMatName.Name = "uComboSearchDurableMatName";
            this.uComboSearchDurableMatName.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchDurableMatName.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(924, 10);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(36, 21);
            this.uComboSearchPlant.TabIndex = 12;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchDurableMatName.TabIndex = 10;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(904, 10);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchPlant.TabIndex = 11;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGroupBoxDurableLot
            // 
            this.uGroupBoxDurableLot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxDurableLot.Controls.Add(this.uGridDurableLotList);
            this.uGroupBoxDurableLot.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxDurableLot.Name = "uGroupBoxDurableLot";
            this.uGroupBoxDurableLot.Size = new System.Drawing.Size(1068, 252);
            this.uGroupBoxDurableLot.TabIndex = 2;
            // 
            // uGridDurableLotList
            // 
            this.uGridDurableLotList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableLotList.DisplayLayout.Appearance = appearance14;
            this.uGridDurableLotList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableLotList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridDurableLotList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableLotList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableLotList.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableLotList.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridDurableLotList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableLotList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableLotList.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridDurableLotList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableLotList.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridDurableLotList.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridDurableLotList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableLotList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableLotList.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridDurableLotList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableLotList.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridDurableLotList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableLotList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableLotList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableLotList.Location = new System.Drawing.Point(12, 12);
            this.uGridDurableLotList.Name = "uGridDurableLotList";
            this.uGridDurableLotList.Size = new System.Drawing.Size(1043, 228);
            this.uGridDurableLotList.TabIndex = 4;
            this.uGridDurableLotList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableLotList_DoubleClickCell);
            // 
            // uGroupBoxSpec
            // 
            this.uGroupBoxSpec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxSpec.Controls.Add(this.uDateTimeUsageJudgeDate);
            this.uGroupBoxSpec.Controls.Add(this.uTextJudeChargeID);
            this.uGroupBoxSpec.Controls.Add(this.uTextJudeChargeName);
            this.uGroupBoxSpec.Controls.Add(this.uTextSpecName);
            this.uGroupBoxSpec.Controls.Add(this.uCheckStandbyFlag);
            this.uGroupBoxSpec.Controls.Add(this.uDateStandbyDate);
            this.uGroupBoxSpec.Controls.Add(this.uTextSpecCode);
            this.uGroupBoxSpec.Controls.Add(this.uCheckFinishFlag);
            this.uGroupBoxSpec.Controls.Add(this.uTextDurableMatLot);
            this.uGroupBoxSpec.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBoxSpec.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelSpec);
            this.uGroupBoxSpec.Controls.Add(this.uLabelStandbyDate);
            this.uGroupBoxSpec.Controls.Add(this.uLabelDurableMatLot);
            this.uGroupBoxSpec.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelEquipCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelUsageJudgeDate);
            this.uGroupBoxSpec.Controls.Add(this.uLabelJudeCharge);
            this.uGroupBoxSpec.Controls.Add(this.uCheckRepairFlag);
            this.uGroupBoxSpec.Controls.Add(this.uLabelShot);
            this.uGroupBoxSpec.Controls.Add(this.uLabelRepairDate);
            this.uGroupBoxSpec.Controls.Add(this.uComboShot);
            this.uGroupBoxSpec.Controls.Add(this.uDateRepairDate);
            this.uGroupBoxSpec.Controls.Add(this.uGroupBoxChange);
            this.uGroupBoxSpec.Controls.Add(this.uGroupBoxResult);
            this.uGroupBoxSpec.Controls.Add(this.uGridSpec);
            this.uGroupBoxSpec.Location = new System.Drawing.Point(0, 332);
            this.uGroupBoxSpec.Name = "uGroupBoxSpec";
            this.uGroupBoxSpec.Size = new System.Drawing.Size(1068, 500);
            this.uGroupBoxSpec.TabIndex = 2;
            // 
            // uDateTimeUsageJudgeDate
            // 
            appearance42.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeUsageJudgeDate.Appearance = appearance42;
            this.uDateTimeUsageJudgeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeUsageJudgeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTimeUsageJudgeDate.Location = new System.Drawing.Point(836, 128);
            this.uDateTimeUsageJudgeDate.Name = "uDateTimeUsageJudgeDate";
            this.uDateTimeUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateTimeUsageJudgeDate.TabIndex = 8;
            // 
            // uTextJudeChargeID
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextJudeChargeID.Appearance = appearance39;
            this.uTextJudeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance40.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance40.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance40;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextJudeChargeID.ButtonsRight.Add(editorButton1);
            this.uTextJudeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextJudeChargeID.Location = new System.Drawing.Point(836, 152);
            this.uTextJudeChargeID.Name = "uTextJudeChargeID";
            this.uTextJudeChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextJudeChargeID.TabIndex = 9;
            this.uTextJudeChargeID.ValueChanged += new System.EventHandler(this.uTextJudeChargeID_ValueChanged);
            this.uTextJudeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextJudeChargeID_KeyDown);
            this.uTextJudeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextJudeChargeID_EditorButtonClick);
            // 
            // uTextJudeChargeName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudeChargeName.Appearance = appearance38;
            this.uTextJudeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudeChargeName.Location = new System.Drawing.Point(940, 152);
            this.uTextJudeChargeName.Name = "uTextJudeChargeName";
            this.uTextJudeChargeName.ReadOnly = true;
            this.uTextJudeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextJudeChargeName.TabIndex = 4;
            // 
            // uTextSpecName
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecName.Appearance = appearance41;
            this.uTextSpecName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecName.Location = new System.Drawing.Point(940, 104);
            this.uTextSpecName.Name = "uTextSpecName";
            this.uTextSpecName.ReadOnly = true;
            this.uTextSpecName.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecName.TabIndex = 4;
            // 
            // uCheckStandbyFlag
            // 
            this.uCheckStandbyFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckStandbyFlag.Location = new System.Drawing.Point(996, 136);
            this.uCheckStandbyFlag.Name = "uCheckStandbyFlag";
            this.uCheckStandbyFlag.Size = new System.Drawing.Size(8, 20);
            this.uCheckStandbyFlag.TabIndex = 83;
            this.uCheckStandbyFlag.Text = "정비대기";
            this.uCheckStandbyFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckStandbyFlag.Visible = false;
            // 
            // uDateStandbyDate
            // 
            this.uDateStandbyDate.Location = new System.Drawing.Point(1032, 160);
            this.uDateStandbyDate.Name = "uDateStandbyDate";
            this.uDateStandbyDate.Size = new System.Drawing.Size(20, 21);
            this.uDateStandbyDate.TabIndex = 87;
            this.uDateStandbyDate.Visible = false;
            // 
            // uTextSpecCode
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecCode.Appearance = appearance43;
            this.uTextSpecCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecCode.Location = new System.Drawing.Point(836, 104);
            this.uTextSpecCode.Name = "uTextSpecCode";
            this.uTextSpecCode.ReadOnly = true;
            this.uTextSpecCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecCode.TabIndex = 7;
            // 
            // uCheckFinishFlag
            // 
            this.uCheckFinishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFinishFlag.Location = new System.Drawing.Point(996, 184);
            this.uCheckFinishFlag.Name = "uCheckFinishFlag";
            this.uCheckFinishFlag.Size = new System.Drawing.Size(8, 20);
            this.uCheckFinishFlag.TabIndex = 82;
            this.uCheckFinishFlag.Text = "정비완료";
            this.uCheckFinishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckFinishFlag.Visible = false;
            // 
            // uTextDurableMatLot
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatLot.Appearance = appearance44;
            this.uTextDurableMatLot.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatLot.Location = new System.Drawing.Point(836, 80);
            this.uTextDurableMatLot.Name = "uTextDurableMatLot";
            this.uTextDurableMatLot.ReadOnly = true;
            this.uTextDurableMatLot.Size = new System.Drawing.Size(120, 21);
            this.uTextDurableMatLot.TabIndex = 6;
            // 
            // uTextDurableMatCode
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance46;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(836, 56);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(120, 21);
            this.uTextDurableMatCode.TabIndex = 6;
            // 
            // uTextEquipCode
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance47;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(836, 32);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(120, 21);
            this.uTextEquipCode.TabIndex = 6;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(724, 104);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(108, 20);
            this.uLabelSpec.TabIndex = 3;
            // 
            // uLabelStandbyDate
            // 
            this.uLabelStandbyDate.Location = new System.Drawing.Point(1012, 160);
            this.uLabelStandbyDate.Name = "uLabelStandbyDate";
            this.uLabelStandbyDate.Size = new System.Drawing.Size(20, 20);
            this.uLabelStandbyDate.TabIndex = 78;
            this.uLabelStandbyDate.Text = "17";
            this.uLabelStandbyDate.Visible = false;
            // 
            // uLabelDurableMatLot
            // 
            this.uLabelDurableMatLot.Location = new System.Drawing.Point(724, 80);
            this.uLabelDurableMatLot.Name = "uLabelDurableMatLot";
            this.uLabelDurableMatLot.Size = new System.Drawing.Size(108, 20);
            this.uLabelDurableMatLot.TabIndex = 3;
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(724, 56);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(108, 20);
            this.uLabelDurableMatCode.TabIndex = 3;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(724, 32);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(108, 20);
            this.uLabelEquipCode.TabIndex = 3;
            // 
            // uLabelUsageJudgeDate
            // 
            this.uLabelUsageJudgeDate.Location = new System.Drawing.Point(724, 128);
            this.uLabelUsageJudgeDate.Name = "uLabelUsageJudgeDate";
            this.uLabelUsageJudgeDate.Size = new System.Drawing.Size(108, 20);
            this.uLabelUsageJudgeDate.TabIndex = 3;
            // 
            // uLabelJudeCharge
            // 
            this.uLabelJudeCharge.Location = new System.Drawing.Point(724, 152);
            this.uLabelJudeCharge.Name = "uLabelJudeCharge";
            this.uLabelJudeCharge.Size = new System.Drawing.Size(108, 20);
            this.uLabelJudeCharge.TabIndex = 3;
            // 
            // uCheckRepairFlag
            // 
            this.uCheckRepairFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRepairFlag.Location = new System.Drawing.Point(996, 160);
            this.uCheckRepairFlag.Name = "uCheckRepairFlag";
            this.uCheckRepairFlag.Size = new System.Drawing.Size(8, 20);
            this.uCheckRepairFlag.TabIndex = 84;
            this.uCheckRepairFlag.Text = "정비착수";
            this.uCheckRepairFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckRepairFlag.Visible = false;
            // 
            // uLabelShot
            // 
            this.uLabelShot.Location = new System.Drawing.Point(724, 176);
            this.uLabelShot.Name = "uLabelShot";
            this.uLabelShot.Size = new System.Drawing.Size(108, 20);
            this.uLabelShot.TabIndex = 3;
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(1012, 184);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(20, 20);
            this.uLabelRepairDate.TabIndex = 77;
            this.uLabelRepairDate.Text = "18";
            this.uLabelRepairDate.Visible = false;
            // 
            // uComboShot
            // 
            this.uComboShot.Location = new System.Drawing.Point(836, 176);
            this.uComboShot.Name = "uComboShot";
            this.uComboShot.Size = new System.Drawing.Size(144, 21);
            this.uComboShot.TabIndex = 10;
            this.uComboShot.ValueChanged += new System.EventHandler(this.uComboShot_ValueChanged);
            // 
            // uDateRepairDate
            // 
            this.uDateRepairDate.Location = new System.Drawing.Point(1032, 184);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(20, 21);
            this.uDateRepairDate.TabIndex = 86;
            this.uDateRepairDate.Visible = false;
            // 
            // uGroupBoxChange
            // 
            this.uGroupBoxChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxChange.Controls.Add(this.uGridChangeDurable);
            this.uGroupBoxChange.Location = new System.Drawing.Point(12, 312);
            this.uGroupBoxChange.Name = "uGroupBoxChange";
            this.uGroupBoxChange.Size = new System.Drawing.Size(1048, 176);
            this.uGroupBoxChange.TabIndex = 1;
            // 
            // uGridChangeDurable
            // 
            this.uGridChangeDurable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridChangeDurable.DisplayLayout.Appearance = appearance5;
            this.uGridChangeDurable.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridChangeDurable.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridChangeDurable.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridChangeDurable.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridChangeDurable.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridChangeDurable.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridChangeDurable.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridChangeDurable.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridChangeDurable.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridChangeDurable.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridChangeDurable.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridChangeDurable.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridChangeDurable.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridChangeDurable.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridChangeDurable.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridChangeDurable.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridChangeDurable.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridChangeDurable.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridChangeDurable.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridChangeDurable.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridChangeDurable.Location = new System.Drawing.Point(12, 12);
            this.uGridChangeDurable.Name = "uGridChangeDurable";
            this.uGridChangeDurable.Size = new System.Drawing.Size(1024, 160);
            this.uGridChangeDurable.TabIndex = 15;
            this.uGridChangeDurable.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridChangeDurable_AfterCellUpdate);
            this.uGridChangeDurable.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridChangeDurable_CellListSelect);
            // 
            // uGroupBoxResult
            // 
            this.uGroupBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxResult.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxResult.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBoxResult.Controls.Add(this.uCheckUsageClearFlag);
            this.uGroupBoxResult.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBoxResult.Controls.Add(this.uComboRepairResultCode);
            this.uGroupBoxResult.Controls.Add(this.uLabelRepairResultCode);
            this.uGroupBoxResult.Controls.Add(this.uLabelFinishDate);
            this.uGroupBoxResult.Controls.Add(this.uDateFinishDate);
            this.uGroupBoxResult.Location = new System.Drawing.Point(12, 212);
            this.uGroupBoxResult.Name = "uGroupBoxResult";
            this.uGroupBoxResult.Size = new System.Drawing.Size(1048, 96);
            this.uGroupBoxResult.TabIndex = 1;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(372, 52);
            this.uTextEtcDesc.Multiline = true;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(476, 36);
            this.uTextEtcDesc.TabIndex = 14;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(372, 28);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 92;
            this.uLabelEtcDesc.Text = "21";
            // 
            // uCheckUsageClearFlag
            // 
            this.uCheckUsageClearFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUsageClearFlag.Location = new System.Drawing.Point(220, 52);
            this.uCheckUsageClearFlag.Name = "uCheckUsageClearFlag";
            this.uCheckUsageClearFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckUsageClearFlag.TabIndex = 13;
            this.uCheckUsageClearFlag.Text = "초기화";
            this.uCheckUsageClearFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.Enabled = false;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(292, 52);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckChangeFlag.TabIndex = 90;
            this.uCheckChangeFlag.Text = "교체여부";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckChangeFlag.CheckedChanged += new System.EventHandler(this.uCheckChangeFlag_CheckedChanged);
            // 
            // uComboRepairResultCode
            // 
            this.uComboRepairResultCode.Location = new System.Drawing.Point(116, 52);
            this.uComboRepairResultCode.Name = "uComboRepairResultCode";
            this.uComboRepairResultCode.Size = new System.Drawing.Size(100, 21);
            this.uComboRepairResultCode.TabIndex = 12;
            this.uComboRepairResultCode.ValueChanged += new System.EventHandler(this.uComboRepairResultCode_ValueChanged);
            // 
            // uLabelRepairResultCode
            // 
            this.uLabelRepairResultCode.Location = new System.Drawing.Point(12, 52);
            this.uLabelRepairResultCode.Name = "uLabelRepairResultCode";
            this.uLabelRepairResultCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairResultCode.TabIndex = 81;
            this.uLabelRepairResultCode.Text = "20";
            // 
            // uLabelFinishDate
            // 
            this.uLabelFinishDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelFinishDate.Name = "uLabelFinishDate";
            this.uLabelFinishDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelFinishDate.TabIndex = 79;
            this.uLabelFinishDate.Text = "19";
            // 
            // uDateFinishDate
            // 
            appearance45.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFinishDate.Appearance = appearance45;
            this.uDateFinishDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFinishDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFinishDate.Location = new System.Drawing.Point(116, 28);
            this.uDateFinishDate.Name = "uDateFinishDate";
            this.uDateFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFinishDate.TabIndex = 11;
            // 
            // uGridSpec
            // 
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSpec.DisplayLayout.Appearance = appearance26;
            this.uGridSpec.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSpec.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpec.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridSpec.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpec.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridSpec.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSpec.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSpec.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSpec.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGridSpec.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSpec.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            appearance33.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSpec.DisplayLayout.Override.CellAppearance = appearance33;
            this.uGridSpec.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSpec.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance35.TextHAlignAsString = "Left";
            this.uGridSpec.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.uGridSpec.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSpec.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridSpec.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridSpec.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSpec.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.uGridSpec.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSpec.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSpec.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSpec.Location = new System.Drawing.Point(12, 28);
            this.uGridSpec.Name = "uGridSpec";
            this.uGridSpec.Size = new System.Drawing.Size(700, 200);
            this.uGridSpec.TabIndex = 5;
            this.uGridSpec.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridSpec_DoubleClickRow);
            // 
            // frmDMMZ0022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSpec);
            this.Controls.Add(this.uGroupBoxDurableLot);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0022";
            this.Load += new System.EventHandler(this.frmDMMZ0022_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0022_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0022_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInstall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableLot)).EndInit();
            this.uGroupBoxDurableLot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLotList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSpec)).EndInit();
            this.uGroupBoxSpec.ResumeLayout(false);
            this.uGroupBoxSpec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboShot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChange)).EndInit();
            this.uGroupBoxChange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridChangeDurable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).EndInit();
            this.uGroupBoxResult.ResumeLayout(false);
            this.uGroupBoxResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpec)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDurableLot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableLotList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSpec;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelShot;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboShot;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxChange;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxResult;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUsageClearFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFinishDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChangeDurable;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInstall;
        private Infragistics.Win.Misc.UltraLabel uLabelInstall;
        private Infragistics.Win.Misc.UltraLabel uLabelJudeCharge;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudeChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudeChargeName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStandbyFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFinishFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRepairFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatLot;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatLot;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
    }
}