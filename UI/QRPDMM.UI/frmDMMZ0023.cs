﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmDMMZ0022.cs                                        */
/* 프로그램명   : 금형치공구정비이력리스트                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-03-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;


namespace QRPDMM.UI
{
    public partial class frmDMMZ0023 : Form,IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();


        public frmDMMZ0023()
        {
            InitializeComponent();
        }

        private void frmDMMZ0023_Activated(object sender, EventArgs e)
        {
            QRPBrowser brwChannel = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            brwChannel.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0023_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmDMMZ0023_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitCombo();
            InitGrid();
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.mfSetLabelText("금형치공구 수명관리 이력조회", m_resSys.GetString("SYS_FONTNAME"), 12);

                this.uGridRepairList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.DownLoadCell);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchDate, "정비일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDurableMatName, uLabelSearchDurableMatName.Text, m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelDurable, "치공구", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스설정
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grb = new WinGroupBox();

                grb.mfSetGroupBox(this.uGroupBoxRepairList, GroupBoxType.LIST, "금형치공구정비이력", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                                 Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                                  Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxRepairList.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxRepairList.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor com = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                com.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista,
                                        m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default,
                                        true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtDurableMatName = clsDurableMat.mfReadDurableMat_TypeCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                if (this.uComboSearchDurableMatName.Items.Count > 0)
                    this.uComboSearchDurableMatName.Items.Clear();

                com.mfSetComboEditor(this.uComboSearchDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                   , "", "", "전체", "DurableMatTypeCode", "DurableMatTypeName", dtDurableMatName);



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                grd.mfInitGeneralGrid(this.uGridRepairList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridRepairList, 0, "PlantCode", "", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "DurableMatCode", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "EMC", "EMC", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "UsageJudgeDate", "정비일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "UsageJudgeTime", "정비시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "JudgeChargeID", "정비담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "JudgeChargeName", "정비담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "UsageLimit", "Limit", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "CurUsage", "정비 Shot", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                      , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                      , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridRepairList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                       , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Toolbar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                #region 필수입력사항
                if (this.uDateTimeFrom.Value == null || this.uDateTimeFrom.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Right);
                    this.uDateTimeFrom.DropDown();
                    return;

                }
                if (this.uDateTimeTo.Value == null || this.uDateTimeTo.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateTimeTo.DropDown();
                    return;

                }

                if (this.uDateTimeFrom.DateTime.Date > this.uDateTimeTo.DateTime.Date)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M000211", Infragistics.Win.HAlign.Right);
                    this.uDateTimeFrom.DropDown();
                    return;

                }
                #endregion

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromDate = this.uDateTimeFrom.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateTimeTo.DateTime.Date.ToString("yyyy-MM-dd");
                string strDurable = this.uTextDurableMat.Text;
                string strLotNo = this.uTextLotNo.Text;
                // 2013-01-29 추가
                string strDurableMatName = this.uComboSearchDurableMatName.Value == null ? string.Empty : this.uComboSearchDurableMatName.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new QRPDMM.BL.DMMMGM.InspectHist();
                brwChannel.mfCredentials(clsInspectHist);

                DataTable dtRepairList = clsInspectHist.mfReadInspectHist_List(strPlantCode, strFromDate, strToDate, strDurableMatName, strLotNo, m_resSys.GetString("SYS_LANG"));

                this.uGridRepairList.DataSource = dtRepairList;
                this.uGridRepairList.DataBind();

                if (dtRepairList.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridRepairList, 0);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();

            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridRepairList.Rows.Count <= 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);

                    return;
                }
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridRepairList);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfPrint()
        {
        }

        public void mfCreate()
        {
        }
        #endregion


        private void DownLoadCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (!e.Cell.Column.Key.Equals("FilePath"))
                    return;

                if (e.Cell.Value == null || e.Cell.Value.ToString().Equals(string.Empty))
                    return;

                string strFilePath = e.Cell.Value.ToString();

                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 파일서버에서 불러올수 있는 파일인지 체크
                if (strFilePath.Contains(":\\"))
                    return;

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0039");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                    arrFile.Add(strFilePath);

                    if (arrFile.Count > 0)
                    {
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strSaveFolder + strFilePath);
                    }
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
