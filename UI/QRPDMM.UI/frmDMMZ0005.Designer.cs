﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0005));
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor21 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor22 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor20 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor18 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor4 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor19 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor17 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor23 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uText1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.ultraButton1);
            this.uGroupBox2.Controls.Add(this.ultraDateTimeEditor1);
            this.uGroupBox2.Controls.Add(this.uGrid1);
            this.uGroupBox2.Controls.Add(this.ultraLabel28);
            this.uGroupBox2.Controls.Add(this.ultraLabel27);
            this.uGroupBox2.Controls.Add(this.ultraLabel26);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor21);
            this.uGroupBox2.Controls.Add(this.ultraLabel25);
            this.uGroupBox2.Controls.Add(this.ultraLabel24);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor22);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor20);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor18);
            this.uGroupBox2.Controls.Add(this.ultraLabel23);
            this.uGroupBox2.Controls.Add(this.ultraLabel22);
            this.uGroupBox2.Controls.Add(this.ultraComboEditor4);
            this.uGroupBox2.Controls.Add(this.ultraComboEditor3);
            this.uGroupBox2.Controls.Add(this.ultraComboEditor2);
            this.uGroupBox2.Controls.Add(this.ultraLabel21);
            this.uGroupBox2.Controls.Add(this.ultraTextEditor19);
            this.uGroupBox2.Controls.Add(this.ultraLabel20);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 277);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1060, 502);
            this.uGroupBox2.TabIndex = 11;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(116, 156);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(88, 28);
            this.ultraButton1.TabIndex = 41;
            this.ultraButton1.Text = "ultraButton1";
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(116, 76);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 40;
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance24;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance31;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance32;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance33.BackColor2 = System.Drawing.SystemColors.Control;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance33;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance34;
            appearance35.BackColor = System.Drawing.SystemColors.Highlight;
            appearance35.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance35;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance36;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            appearance37.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance37;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance38.BackColor = System.Drawing.SystemColors.Control;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance38;
            appearance39.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance39;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance40;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance41;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 164);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1036, 322);
            this.uGrid1.TabIndex = 39;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // ultraLabel28
            // 
            this.ultraLabel28.Location = new System.Drawing.Point(12, 160);
            this.ultraLabel28.Name = "ultraLabel28";
            this.ultraLabel28.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel28.TabIndex = 38;
            this.ultraLabel28.Text = "ultraLabel28";
            // 
            // ultraLabel27
            // 
            this.ultraLabel27.Location = new System.Drawing.Point(12, 124);
            this.ultraLabel27.Name = "ultraLabel27";
            this.ultraLabel27.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel27.TabIndex = 37;
            this.ultraLabel27.Text = "ultraLabel27";
            // 
            // ultraLabel26
            // 
            this.ultraLabel26.Location = new System.Drawing.Point(12, 76);
            this.ultraLabel26.Name = "ultraLabel26";
            this.ultraLabel26.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel26.TabIndex = 36;
            this.ultraLabel26.Text = "ultraLabel26";
            // 
            // ultraTextEditor21
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor21.Appearance = appearance15;
            this.ultraTextEditor21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor21.Location = new System.Drawing.Point(736, 52);
            this.ultraTextEditor21.Name = "ultraTextEditor21";
            this.ultraTextEditor21.ReadOnly = true;
            this.ultraTextEditor21.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor21.TabIndex = 35;
            // 
            // ultraLabel25
            // 
            this.ultraLabel25.Location = new System.Drawing.Point(528, 52);
            this.ultraLabel25.Name = "ultraLabel25";
            this.ultraLabel25.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel25.TabIndex = 14;
            this.ultraLabel25.Text = "ultraLabel25";
            // 
            // ultraLabel24
            // 
            this.ultraLabel24.Location = new System.Drawing.Point(272, 52);
            this.ultraLabel24.Name = "ultraLabel24";
            this.ultraLabel24.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel24.TabIndex = 13;
            this.ultraLabel24.Text = "ultraLabel24";
            // 
            // ultraTextEditor22
            // 
            this.ultraTextEditor22.Location = new System.Drawing.Point(116, 124);
            this.ultraTextEditor22.Name = "ultraTextEditor22";
            this.ultraTextEditor22.Size = new System.Drawing.Size(464, 21);
            this.ultraTextEditor22.TabIndex = 12;
            // 
            // ultraTextEditor20
            // 
            this.ultraTextEditor20.Location = new System.Drawing.Point(116, 100);
            this.ultraTextEditor20.Name = "ultraTextEditor20";
            this.ultraTextEditor20.Size = new System.Drawing.Size(464, 21);
            this.ultraTextEditor20.TabIndex = 12;
            // 
            // ultraTextEditor18
            // 
            this.ultraTextEditor18.Location = new System.Drawing.Point(116, 52);
            this.ultraTextEditor18.Name = "ultraTextEditor18";
            this.ultraTextEditor18.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor18.TabIndex = 12;
            // 
            // ultraLabel23
            // 
            this.ultraLabel23.Location = new System.Drawing.Point(12, 100);
            this.ultraLabel23.Name = "ultraLabel23";
            this.ultraLabel23.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel23.TabIndex = 11;
            this.ultraLabel23.Text = "ultraLabel23";
            // 
            // ultraLabel22
            // 
            this.ultraLabel22.Location = new System.Drawing.Point(12, 52);
            this.ultraLabel22.Name = "ultraLabel22";
            this.ultraLabel22.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel22.TabIndex = 10;
            this.ultraLabel22.Text = "ultraLabel22";
            // 
            // ultraComboEditor4
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor4.Appearance = appearance3;
            this.ultraComboEditor4.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraComboEditor4.Location = new System.Drawing.Point(376, 52);
            this.ultraComboEditor4.Name = "ultraComboEditor4";
            this.ultraComboEditor4.Size = new System.Drawing.Size(105, 21);
            this.ultraComboEditor4.TabIndex = 9;
            this.ultraComboEditor4.Text = "ultraComboEditor3";
            // 
            // ultraComboEditor3
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor3.Appearance = appearance29;
            this.ultraComboEditor3.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraComboEditor3.Location = new System.Drawing.Point(376, 28);
            this.ultraComboEditor3.Name = "ultraComboEditor3";
            this.ultraComboEditor3.Size = new System.Drawing.Size(105, 21);
            this.ultraComboEditor3.TabIndex = 9;
            this.ultraComboEditor3.Text = "ultraComboEditor3";
            // 
            // ultraComboEditor2
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor2.Appearance = appearance30;
            this.ultraComboEditor2.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraComboEditor2.Location = new System.Drawing.Point(116, 28);
            this.ultraComboEditor2.Name = "ultraComboEditor2";
            this.ultraComboEditor2.Size = new System.Drawing.Size(105, 21);
            this.ultraComboEditor2.TabIndex = 8;
            this.ultraComboEditor2.Text = "ultraComboEditor2";
            // 
            // ultraLabel21
            // 
            this.ultraLabel21.Location = new System.Drawing.Point(272, 28);
            this.ultraLabel21.Name = "ultraLabel21";
            this.ultraLabel21.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel21.TabIndex = 7;
            this.ultraLabel21.Text = "ultraLabel21";
            // 
            // ultraTextEditor19
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor19.Appearance = appearance6;
            this.ultraTextEditor19.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance1;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor19.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor19.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor19.Location = new System.Drawing.Point(632, 52);
            this.ultraTextEditor19.Name = "ultraTextEditor19";
            this.ultraTextEditor19.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor19.TabIndex = 8;
            // 
            // ultraLabel20
            // 
            this.ultraLabel20.Location = new System.Drawing.Point(12, 28);
            this.ultraLabel20.Name = "ultraLabel20";
            this.ultraLabel20.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel20.TabIndex = 5;
            this.ultraLabel20.Text = "ultraLabel20";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.ultraTextEditor17);
            this.uGroupBox1.Controls.Add(this.ultraLabel19);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor16);
            this.uGroupBox1.Controls.Add(this.ultraLabel18);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor15);
            this.uGroupBox1.Controls.Add(this.ultraLabel17);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor14);
            this.uGroupBox1.Controls.Add(this.ultraLabel16);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor13);
            this.uGroupBox1.Controls.Add(this.ultraLabel15);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor12);
            this.uGroupBox1.Controls.Add(this.ultraLabel14);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor11);
            this.uGroupBox1.Controls.Add(this.ultraLabel13);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor10);
            this.uGroupBox1.Controls.Add(this.ultraLabel12);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor9);
            this.uGroupBox1.Controls.Add(this.ultraLabel11);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor8);
            this.uGroupBox1.Controls.Add(this.ultraLabel10);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor7);
            this.uGroupBox1.Controls.Add(this.ultraLabel9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor6);
            this.uGroupBox1.Controls.Add(this.ultraLabel8);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor5);
            this.uGroupBox1.Controls.Add(this.ultraLabel7);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor4);
            this.uGroupBox1.Controls.Add(this.ultraLabel6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor3);
            this.uGroupBox1.Controls.Add(this.ultraLabel5);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 125);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1060, 82);
            this.uGroupBox1.TabIndex = 10;
            // 
            // ultraTextEditor17
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Appearance = appearance4;
            this.ultraTextEditor17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Location = new System.Drawing.Point(632, 124);
            this.ultraTextEditor17.Name = "ultraTextEditor17";
            this.ultraTextEditor17.ReadOnly = true;
            this.ultraTextEditor17.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor17.TabIndex = 34;
            // 
            // ultraLabel19
            // 
            this.ultraLabel19.Location = new System.Drawing.Point(528, 124);
            this.ultraLabel19.Name = "ultraLabel19";
            this.ultraLabel19.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel19.TabIndex = 33;
            this.ultraLabel19.Text = "ultraLabel19";
            // 
            // ultraTextEditor16
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Appearance = appearance21;
            this.ultraTextEditor16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Location = new System.Drawing.Point(376, 124);
            this.ultraTextEditor16.Name = "ultraTextEditor16";
            this.ultraTextEditor16.ReadOnly = true;
            this.ultraTextEditor16.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor16.TabIndex = 32;
            // 
            // ultraLabel18
            // 
            this.ultraLabel18.Location = new System.Drawing.Point(272, 124);
            this.ultraLabel18.Name = "ultraLabel18";
            this.ultraLabel18.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel18.TabIndex = 31;
            this.ultraLabel18.Text = "ultraLabel18";
            // 
            // ultraTextEditor15
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Appearance = appearance22;
            this.ultraTextEditor15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Location = new System.Drawing.Point(116, 124);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.ReadOnly = true;
            this.ultraTextEditor15.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor15.TabIndex = 30;
            // 
            // ultraLabel17
            // 
            this.ultraLabel17.Location = new System.Drawing.Point(12, 124);
            this.ultraLabel17.Name = "ultraLabel17";
            this.ultraLabel17.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel17.TabIndex = 29;
            this.ultraLabel17.Text = "ultraLabel17";
            // 
            // ultraTextEditor14
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Appearance = appearance10;
            this.ultraTextEditor14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Location = new System.Drawing.Point(116, 100);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.ReadOnly = true;
            this.ultraTextEditor14.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor14.TabIndex = 28;
            // 
            // ultraLabel16
            // 
            this.ultraLabel16.Location = new System.Drawing.Point(12, 100);
            this.ultraLabel16.Name = "ultraLabel16";
            this.ultraLabel16.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel16.TabIndex = 27;
            this.ultraLabel16.Text = "ultraLabel16";
            // 
            // ultraTextEditor13
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Appearance = appearance11;
            this.ultraTextEditor13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Location = new System.Drawing.Point(376, 100);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.ReadOnly = true;
            this.ultraTextEditor13.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor13.TabIndex = 26;
            // 
            // ultraLabel15
            // 
            this.ultraLabel15.Location = new System.Drawing.Point(272, 100);
            this.ultraLabel15.Name = "ultraLabel15";
            this.ultraLabel15.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel15.TabIndex = 25;
            this.ultraLabel15.Text = "ultraLabel15";
            // 
            // ultraTextEditor12
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Appearance = appearance12;
            this.ultraTextEditor12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Location = new System.Drawing.Point(376, 76);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.ReadOnly = true;
            this.ultraTextEditor12.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor12.TabIndex = 24;
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Location = new System.Drawing.Point(272, 76);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel14.TabIndex = 23;
            this.ultraLabel14.Text = "ultraLabel14";
            // 
            // ultraTextEditor11
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance13;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(632, 100);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 22;
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Location = new System.Drawing.Point(528, 100);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel13.TabIndex = 21;
            this.ultraLabel13.Text = "ultraLabel13";
            // 
            // ultraTextEditor10
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Appearance = appearance14;
            this.ultraTextEditor10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Location = new System.Drawing.Point(116, 76);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.ReadOnly = true;
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 20;
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Location = new System.Drawing.Point(12, 76);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel12.TabIndex = 19;
            this.ultraLabel12.Text = "ultraLabel12";
            // 
            // ultraTextEditor9
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance26;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(376, 52);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 18;
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Location = new System.Drawing.Point(272, 52);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel11.TabIndex = 17;
            this.ultraLabel11.Text = "ultraLabel11";
            // 
            // ultraTextEditor8
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance16;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(632, 76);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor8.TabIndex = 16;
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Location = new System.Drawing.Point(528, 76);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel10.TabIndex = 15;
            this.ultraLabel10.Text = "ultraLabel10";
            // 
            // ultraTextEditor7
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance17;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(632, 52);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor7.TabIndex = 14;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(528, 52);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel9.TabIndex = 13;
            this.ultraLabel9.Text = "ultraLabel9";
            // 
            // ultraTextEditor6
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Appearance = appearance18;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Location = new System.Drawing.Point(116, 52);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.ReadOnly = true;
            this.ultraTextEditor6.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor6.TabIndex = 12;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(12, 52);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel8.TabIndex = 11;
            this.ultraLabel8.Text = "ultraLabel8";
            // 
            // ultraTextEditor5
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance19;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(632, 28);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 10;
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(528, 28);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel7.TabIndex = 9;
            this.ultraLabel7.Text = "ultraLabel7";
            // 
            // ultraTextEditor4
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance20;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(376, 28);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 8;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(272, 28);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel6.TabIndex = 7;
            this.ultraLabel6.Text = "ultraLabel6";
            // 
            // ultraTextEditor3
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor3.Appearance = appearance27;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.PowderBlue;
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance28;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor3.ButtonsRight.Add(editorButton2);
            this.ultraTextEditor3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor3.Location = new System.Drawing.Point(116, 28);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 6;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(12, 28);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel5.TabIndex = 5;
            this.ultraLabel5.Text = "ultraLabel5";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.ultraTextEditor23);
            this.uGroupBox.Controls.Add(this.ultraLabel3);
            this.uGroupBox.Controls.Add(this.ultraTextEditor2);
            this.uGroupBox.Controls.Add(this.ultraTextEditor1);
            this.uGroupBox.Controls.Add(this.ultraLabel2);
            this.uGroupBox.Controls.Add(this.uText1);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.ultraLabel1);
            this.uGroupBox.Controls.Add(this.ultraLabel4);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1060, 14);
            this.uGroupBox.TabIndex = 9;
            // 
            // ultraTextEditor23
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor23.Appearance = appearance5;
            this.ultraTextEditor23.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor23.Location = new System.Drawing.Point(632, 28);
            this.ultraTextEditor23.Name = "ultraTextEditor23";
            this.ultraTextEditor23.ReadOnly = true;
            this.ultraTextEditor23.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor23.TabIndex = 8;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(272, 52);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel3.TabIndex = 7;
            this.ultraLabel3.Text = "ultraLabel3";
            // 
            // ultraTextEditor2
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance23;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(376, 52);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 6;
            // 
            // ultraTextEditor1
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Appearance = appearance7;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Location = new System.Drawing.Point(116, 52);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 6;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(12, 52);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel2.TabIndex = 5;
            this.ultraLabel2.Text = "ultraLabel2";
            // 
            // uText1
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uText1.Appearance = appearance8;
            this.uText1.BackColor = System.Drawing.Color.PowderBlue;
            appearance9.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance9;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uText1.ButtonsRight.Add(editorButton3);
            this.uText1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uText1.Location = new System.Drawing.Point(376, 28);
            this.uText1.Name = "uText1";
            this.uText1.Size = new System.Drawing.Size(100, 21);
            this.uText1.TabIndex = 4;
            // 
            // uComboPlant
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance2;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 28);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 2;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(272, 28);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel1.TabIndex = 1;
            this.ultraLabel1.Text = "ultraLabel1";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(528, 28);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel4.TabIndex = 0;
            this.ultraLabel4.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "Plant";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 8;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmDMMZ0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0005";
            this.Load += new System.EventHandler(this.frmDMMZ0005_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0005_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel28;
        private Infragistics.Win.Misc.UltraLabel ultraLabel27;
        private Infragistics.Win.Misc.UltraLabel ultraLabel26;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor21;
        private Infragistics.Win.Misc.UltraLabel ultraLabel25;
        private Infragistics.Win.Misc.UltraLabel ultraLabel24;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor18;
        private Infragistics.Win.Misc.UltraLabel ultraLabel23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel22;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel21;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor19;
        private Infragistics.Win.Misc.UltraLabel ultraLabel20;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor17;
        private Infragistics.Win.Misc.UltraLabel ultraLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor16;
        private Infragistics.Win.Misc.UltraLabel ultraLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;
        private Infragistics.Win.Misc.UltraLabel ultraLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.Misc.UltraLabel ultraLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.Misc.UltraLabel ultraLabel15;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor23;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
    }
}