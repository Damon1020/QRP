﻿namespace QRPDMM.UI
{
    partial class frmDMM0019
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0019));
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMoldName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMoldName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMoldCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMold = new Infragistics.Win.Misc.UltraLabel();
            this.uDateShotJudgToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateShotJudgFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelShotJudg = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uLabelUseSparePart = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUnusual = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUseUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUseUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUseUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateUseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelUseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRepairUnusual = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCCSRequest = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCCSRequest = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipStopType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRepairDiv = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairDiv = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairRequestDiv = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairRequestDiv = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairRequestNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairRequestNum = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMoldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMoldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShotJudgToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShotJudgFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnusual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUnusual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMoldName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMoldName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMoldCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMold);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateShotJudgToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateShotJudgFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelShotJudg);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSearchEquipName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance7;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(848, 36);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipName.TabIndex = 13;
            // 
            // uTextSearchEquipCode
            // 
            appearance6.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance6;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(748, 36);
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 12;
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(632, 36);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchEquip.TabIndex = 11;
            this.uLabelSearchEquip.Text = "ultraLabel1";
            // 
            // uTextSearchMoldName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMoldName.Appearance = appearance5;
            this.uTextSearchMoldName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMoldName.Location = new System.Drawing.Point(404, 36);
            this.uTextSearchMoldName.Name = "uTextSearchMoldName";
            this.uTextSearchMoldName.ReadOnly = true;
            this.uTextSearchMoldName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMoldName.TabIndex = 10;
            // 
            // uLabelSearchMoldName
            // 
            this.uLabelSearchMoldName.Location = new System.Drawing.Point(288, 36);
            this.uLabelSearchMoldName.Name = "uLabelSearchMoldName";
            this.uLabelSearchMoldName.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchMoldName.TabIndex = 9;
            this.uLabelSearchMoldName.Text = "ultraLabel1";
            // 
            // uTextSearchMoldCode
            // 
            appearance35.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchMoldCode.Appearance = appearance35;
            this.uTextSearchMoldCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance36.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance36.TextHAlignAsString = "Center";
            editorButton2.Appearance = appearance36;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMoldCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMoldCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMoldCode.Location = new System.Drawing.Point(128, 36);
            this.uTextSearchMoldCode.Name = "uTextSearchMoldCode";
            this.uTextSearchMoldCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMoldCode.TabIndex = 8;
            // 
            // uLabelSearchMold
            // 
            this.uLabelSearchMold.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchMold.Name = "uLabelSearchMold";
            this.uLabelSearchMold.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchMold.TabIndex = 7;
            this.uLabelSearchMold.Text = "ultraLabel1";
            // 
            // uDateShotJudgToDate
            // 
            this.uDateShotJudgToDate.Location = new System.Drawing.Point(524, 12);
            this.uDateShotJudgToDate.Name = "uDateShotJudgToDate";
            this.uDateShotJudgToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateShotJudgToDate.TabIndex = 6;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(504, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(20, 16);
            this.ultraLabel1.TabIndex = 5;
            this.ultraLabel1.Text = "~";
            // 
            // uDateShotJudgFromDate
            // 
            this.uDateShotJudgFromDate.Location = new System.Drawing.Point(404, 12);
            this.uDateShotJudgFromDate.Name = "uDateShotJudgFromDate";
            this.uDateShotJudgFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateShotJudgFromDate.TabIndex = 3;
            // 
            // uLabelShotJudg
            // 
            this.uLabelShotJudg.Location = new System.Drawing.Point(288, 12);
            this.uLabelShotJudg.Name = "uLabelShotJudg";
            this.uLabelShotJudg.Size = new System.Drawing.Size(110, 20);
            this.uLabelShotJudg.TabIndex = 2;
            this.uLabelShotJudg.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance11;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance16;
            appearance18.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 720);
            this.uGrid1.TabIndex = 2;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGrid2);
            this.ultraGroupBox1.Controls.Add(this.uButtonDelete);
            this.ultraGroupBox1.Controls.Add(this.uLabelUseSparePart);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 284);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1048, 368);
            this.ultraGroupBox1.TabIndex = 18;
            // 
            // uGrid2
            // 
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance41;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance42;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance43;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance44.BackColor2 = System.Drawing.SystemColors.Control;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance44;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance45;
            appearance46.BackColor = System.Drawing.SystemColors.Highlight;
            appearance46.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance46;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance47;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            appearance48.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance48;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance49.BackColor = System.Drawing.SystemColors.Control;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance49;
            appearance50.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance50;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance51;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance52;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 36);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1028, 324);
            this.uGrid2.TabIndex = 16;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(128, 4);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 15;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uLabelUseSparePart
            // 
            this.uLabelUseSparePart.Location = new System.Drawing.Point(12, 12);
            this.uLabelUseSparePart.Name = "uLabelUseSparePart";
            this.uLabelUseSparePart.Size = new System.Drawing.Size(110, 20);
            this.uLabelUseSparePart.TabIndex = 14;
            this.uLabelUseSparePart.Text = "ultraLabel1";
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uTextUnusual);
            this.uGroupBox4.Controls.Add(this.uLabelUnusual);
            this.uGroupBox4.Controls.Add(this.uTextUseUserName);
            this.uGroupBox4.Controls.Add(this.uTextUseUserID);
            this.uGroupBox4.Controls.Add(this.uLabelUseUser);
            this.uGroupBox4.Controls.Add(this.uDateUseDate);
            this.uGroupBox4.Controls.Add(this.uLabelUseDate);
            this.uGroupBox4.Location = new System.Drawing.Point(12, 240);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(1048, 40);
            this.uGroupBox4.TabIndex = 17;
            // 
            // uTextUnusual
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnusual.Appearance = appearance21;
            this.uTextUnusual.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnusual.Location = new System.Drawing.Point(732, 12);
            this.uTextUnusual.Name = "uTextUnusual";
            this.uTextUnusual.ReadOnly = true;
            this.uTextUnusual.Size = new System.Drawing.Size(308, 21);
            this.uTextUnusual.TabIndex = 33;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(616, 12);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(110, 20);
            this.uLabelUnusual.TabIndex = 32;
            this.uLabelUnusual.Text = "ultraLabel1";
            // 
            // uTextUseUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseUserName.Appearance = appearance29;
            this.uTextUseUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseUserName.Location = new System.Drawing.Point(505, 12);
            this.uTextUseUserName.Name = "uTextUseUserName";
            this.uTextUseUserName.ReadOnly = true;
            this.uTextUseUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUseUserName.TabIndex = 17;
            // 
            // uTextUseUserID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUseUserID.Appearance = appearance3;
            this.uTextUseUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance4.TextHAlignAsString = "Center";
            editorButton3.Appearance = appearance4;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUseUserID.ButtonsRight.Add(editorButton3);
            this.uTextUseUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextUseUserID.Location = new System.Drawing.Point(404, 12);
            this.uTextUseUserID.Name = "uTextUseUserID";
            this.uTextUseUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUseUserID.TabIndex = 16;
            // 
            // uLabelUseUser
            // 
            this.uLabelUseUser.Location = new System.Drawing.Point(288, 12);
            this.uLabelUseUser.Name = "uLabelUseUser";
            this.uLabelUseUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelUseUser.TabIndex = 15;
            this.uLabelUseUser.Text = "ultraLabel1";
            // 
            // uDateUseDate
            // 
            this.uDateUseDate.Location = new System.Drawing.Point(128, 12);
            this.uDateUseDate.Name = "uDateUseDate";
            this.uDateUseDate.Size = new System.Drawing.Size(100, 21);
            this.uDateUseDate.TabIndex = 14;
            // 
            // uLabelUseDate
            // 
            this.uLabelUseDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelUseDate.Name = "uLabelUseDate";
            this.uLabelUseDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelUseDate.TabIndex = 13;
            this.uLabelUseDate.Text = "ultraLabel1";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uTextRepairUnusual);
            this.uGroupBox3.Controls.Add(this.uLabelRepairUnusual);
            this.uGroupBox3.Controls.Add(this.uTextCCSRequest);
            this.uGroupBox3.Controls.Add(this.uLabelCCSRequest);
            this.uGroupBox3.Controls.Add(this.uTextEquipStopType);
            this.uGroupBox3.Controls.Add(this.uLabelEquipStopType);
            this.uGroupBox3.Controls.Add(this.uTextRepairResult);
            this.uGroupBox3.Controls.Add(this.uLabelRepairResult);
            this.uGroupBox3.Controls.Add(this.uTextRepairUserName);
            this.uGroupBox3.Controls.Add(this.uTextRepairUserID);
            this.uGroupBox3.Controls.Add(this.uLabelRepairUser);
            this.uGroupBox3.Controls.Add(this.uTextRepairDate);
            this.uGroupBox3.Controls.Add(this.uLabelRepairDate);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 156);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1048, 80);
            this.uGroupBox3.TabIndex = 16;
            // 
            // uTextRepairUnusual
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUnusual.Appearance = appearance39;
            this.uTextRepairUnusual.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUnusual.Location = new System.Drawing.Point(732, 52);
            this.uTextRepairUnusual.Name = "uTextRepairUnusual";
            this.uTextRepairUnusual.ReadOnly = true;
            this.uTextRepairUnusual.Size = new System.Drawing.Size(308, 21);
            this.uTextRepairUnusual.TabIndex = 31;
            // 
            // uLabelRepairUnusual
            // 
            this.uLabelRepairUnusual.Location = new System.Drawing.Point(616, 52);
            this.uLabelRepairUnusual.Name = "uLabelRepairUnusual";
            this.uLabelRepairUnusual.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairUnusual.TabIndex = 30;
            this.uLabelRepairUnusual.Text = "ultraLabel1";
            // 
            // uTextCCSRequest
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSRequest.Appearance = appearance37;
            this.uTextCCSRequest.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCCSRequest.Location = new System.Drawing.Point(404, 52);
            this.uTextCCSRequest.Name = "uTextCCSRequest";
            this.uTextCCSRequest.ReadOnly = true;
            this.uTextCCSRequest.Size = new System.Drawing.Size(150, 21);
            this.uTextCCSRequest.TabIndex = 29;
            // 
            // uLabelCCSRequest
            // 
            this.uLabelCCSRequest.Location = new System.Drawing.Point(288, 52);
            this.uLabelCCSRequest.Name = "uLabelCCSRequest";
            this.uLabelCCSRequest.Size = new System.Drawing.Size(110, 20);
            this.uLabelCCSRequest.TabIndex = 28;
            this.uLabelCCSRequest.Text = "ultraLabel1";
            // 
            // uTextEquipStopType
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopType.Appearance = appearance38;
            this.uTextEquipStopType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipStopType.Location = new System.Drawing.Point(128, 52);
            this.uTextEquipStopType.Name = "uTextEquipStopType";
            this.uTextEquipStopType.ReadOnly = true;
            this.uTextEquipStopType.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipStopType.TabIndex = 27;
            // 
            // uLabelEquipStopType
            // 
            this.uLabelEquipStopType.Location = new System.Drawing.Point(12, 52);
            this.uLabelEquipStopType.Name = "uLabelEquipStopType";
            this.uLabelEquipStopType.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipStopType.TabIndex = 26;
            this.uLabelEquipStopType.Text = "ultraLabel1";
            // 
            // uTextRepairResult
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResult.Appearance = appearance32;
            this.uTextRepairResult.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairResult.Location = new System.Drawing.Point(732, 28);
            this.uTextRepairResult.Name = "uTextRepairResult";
            this.uTextRepairResult.ReadOnly = true;
            this.uTextRepairResult.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairResult.TabIndex = 25;
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(616, 28);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairResult.TabIndex = 24;
            this.uLabelRepairResult.Text = "ultraLabel1";
            // 
            // uTextRepairUserName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Appearance = appearance40;
            this.uTextRepairUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Location = new System.Drawing.Point(505, 28);
            this.uTextRepairUserName.Name = "uTextRepairUserName";
            this.uTextRepairUserName.ReadOnly = true;
            this.uTextRepairUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserName.TabIndex = 16;
            // 
            // uTextRepairUserID
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserID.Appearance = appearance31;
            this.uTextRepairUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserID.Location = new System.Drawing.Point(404, 28);
            this.uTextRepairUserID.Name = "uTextRepairUserID";
            this.uTextRepairUserID.ReadOnly = true;
            this.uTextRepairUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserID.TabIndex = 15;
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(288, 28);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairUser.TabIndex = 14;
            this.uLabelRepairUser.Text = "ultraLabel1";
            // 
            // uTextRepairDate
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDate.Appearance = appearance33;
            this.uTextRepairDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDate.Location = new System.Drawing.Point(128, 28);
            this.uTextRepairDate.Name = "uTextRepairDate";
            this.uTextRepairDate.ReadOnly = true;
            this.uTextRepairDate.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairDate.TabIndex = 13;
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairDate.TabIndex = 12;
            this.uLabelRepairDate.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextRepairDiv);
            this.uGroupBox1.Controls.Add(this.uLabelRepairDiv);
            this.uGroupBox1.Controls.Add(this.uTextRepairRequestDiv);
            this.uGroupBox1.Controls.Add(this.uLabelRepairRequestDiv);
            this.uGroupBox1.Controls.Add(this.uTextRepairRequestNum);
            this.uGroupBox1.Controls.Add(this.uLabelRepairRequestNum);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1048, 40);
            this.uGroupBox1.TabIndex = 15;
            // 
            // uTextRepairDiv
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDiv.Appearance = appearance20;
            this.uTextRepairDiv.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDiv.Location = new System.Drawing.Point(680, 12);
            this.uTextRepairDiv.Name = "uTextRepairDiv";
            this.uTextRepairDiv.ReadOnly = true;
            this.uTextRepairDiv.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairDiv.TabIndex = 19;
            // 
            // uLabelRepairDiv
            // 
            this.uLabelRepairDiv.Location = new System.Drawing.Point(564, 12);
            this.uLabelRepairDiv.Name = "uLabelRepairDiv";
            this.uLabelRepairDiv.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairDiv.TabIndex = 18;
            this.uLabelRepairDiv.Text = "ultraLabel1";
            // 
            // uTextRepairRequestDiv
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestDiv.Appearance = appearance22;
            this.uTextRepairRequestDiv.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestDiv.Location = new System.Drawing.Point(404, 12);
            this.uTextRepairRequestDiv.Name = "uTextRepairRequestDiv";
            this.uTextRepairRequestDiv.ReadOnly = true;
            this.uTextRepairRequestDiv.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairRequestDiv.TabIndex = 17;
            // 
            // uLabelRepairRequestDiv
            // 
            this.uLabelRepairRequestDiv.Location = new System.Drawing.Point(288, 12);
            this.uLabelRepairRequestDiv.Name = "uLabelRepairRequestDiv";
            this.uLabelRepairRequestDiv.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairRequestDiv.TabIndex = 16;
            this.uLabelRepairRequestDiv.Text = "ultraLabel1";
            // 
            // uTextRepairRequestNum
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestNum.Appearance = appearance23;
            this.uTextRepairRequestNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestNum.Location = new System.Drawing.Point(128, 12);
            this.uTextRepairRequestNum.Name = "uTextRepairRequestNum";
            this.uTextRepairRequestNum.ReadOnly = true;
            this.uTextRepairRequestNum.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairRequestNum.TabIndex = 15;
            // 
            // uLabelRepairRequestNum
            // 
            this.uLabelRepairRequestNum.Location = new System.Drawing.Point(12, 12);
            this.uLabelRepairRequestNum.Name = "uLabelRepairRequestNum";
            this.uLabelRepairRequestNum.Size = new System.Drawing.Size(110, 20);
            this.uLabelRepairRequestNum.TabIndex = 14;
            this.uLabelRepairRequestNum.Text = "ultraLabel1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uTextEquipName);
            this.uGroupBox2.Controls.Add(this.uLabelEquipName);
            this.uGroupBox2.Controls.Add(this.uTextEquipCode);
            this.uGroupBox2.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox2.Controls.Add(this.uTextVendor);
            this.uGroupBox2.Controls.Add(this.uLabelVendor);
            this.uGroupBox2.Controls.Add(this.uTextSpec);
            this.uGroupBox2.Controls.Add(this.uLabelSpec);
            this.uGroupBox2.Controls.Add(this.uTextMoldName);
            this.uGroupBox2.Controls.Add(this.uLabelMoldName);
            this.uGroupBox2.Controls.Add(this.uTextMoldCode);
            this.uGroupBox2.Controls.Add(this.uLabelMoldCode);
            this.uGroupBox2.Controls.Add(this.uTextPlant);
            this.uGroupBox2.Controls.Add(this.uLabelPlant);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 56);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1048, 96);
            this.uGroupBox2.TabIndex = 14;
            // 
            // uTextEquipName
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance34;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(680, 60);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipName.TabIndex = 23;
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(564, 60);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipName.TabIndex = 22;
            this.uLabelEquipName.Text = "ultraLabel1";
            // 
            // uTextEquipCode
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance26;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(680, 36);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipCode.TabIndex = 21;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(564, 36);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipCode.TabIndex = 20;
            this.uLabelEquipCode.Text = "ultraLabel1";
            // 
            // uTextVendor
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance25;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(128, 60);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(426, 21);
            this.uTextVendor.TabIndex = 19;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 60);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelVendor.TabIndex = 18;
            this.uLabelVendor.Text = "ultraLabel1";
            // 
            // uTextSpec
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance27;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(128, 36);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(426, 21);
            this.uTextSpec.TabIndex = 17;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 36);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 16;
            this.uLabelSpec.Text = "ultraLabel1";
            // 
            // uTextMoldName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldName.Appearance = appearance28;
            this.uTextMoldName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldName.Location = new System.Drawing.Point(680, 12);
            this.uTextMoldName.Name = "uTextMoldName";
            this.uTextMoldName.ReadOnly = true;
            this.uTextMoldName.Size = new System.Drawing.Size(150, 21);
            this.uTextMoldName.TabIndex = 15;
            // 
            // uLabelMoldName
            // 
            this.uLabelMoldName.Location = new System.Drawing.Point(564, 12);
            this.uLabelMoldName.Name = "uLabelMoldName";
            this.uLabelMoldName.Size = new System.Drawing.Size(110, 20);
            this.uLabelMoldName.TabIndex = 14;
            this.uLabelMoldName.Text = "ultraLabel1";
            // 
            // uTextMoldCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldCode.Appearance = appearance24;
            this.uTextMoldCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldCode.Location = new System.Drawing.Point(404, 12);
            this.uTextMoldCode.Name = "uTextMoldCode";
            this.uTextMoldCode.ReadOnly = true;
            this.uTextMoldCode.Size = new System.Drawing.Size(150, 21);
            this.uTextMoldCode.TabIndex = 13;
            // 
            // uLabelMoldCode
            // 
            this.uLabelMoldCode.Location = new System.Drawing.Point(288, 12);
            this.uLabelMoldCode.Name = "uLabelMoldCode";
            this.uLabelMoldCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelMoldCode.TabIndex = 12;
            this.uLabelMoldCode.Text = "ultraLabel1";
            // 
            // uTextPlant
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance30;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(128, 12);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 11;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 10;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // frmDMM0019
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0019";
            this.Load += new System.EventHandler(this.frmDMM0019_Load);
            this.Activated += new System.EventHandler(this.frmDMM0019_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMoldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMoldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShotJudgToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShotJudgFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            this.uGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnusual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUnusual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCCSRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateShotJudgFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelShotJudg;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMold;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateShotJudgToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMoldCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMoldName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMoldName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDiv;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDiv;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairRequestDiv;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairRequestDiv;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairRequestNum;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairRequestNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldName;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipStopType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipStopType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairResult;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelUseUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateUseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelUseDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCCSRequest;
        private Infragistics.Win.Misc.UltraLabel uLabelCCSRequest;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraLabel uLabelUseSparePart;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseUserName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
    }
}