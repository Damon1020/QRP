﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMMZ0017.cs                                        */
/* 프로그램명   : 치공구실사등록                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0017 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMMZ0017()
        {
            InitializeComponent();
        }

        private void frmDMMZ0017_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitText();
            InitLabel();
            InitGrid();
            InitCombo();
            InitGroupBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        private void frmDMMZ0017_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
        private void frmDMMZ0017_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 초기화 Method

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("치공구실사등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextStockTakeChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextStockTakeChargeName.Text = m_resSys.GetString("SYS_USERNAME");
                
                uTextStockTakeEtcDesc.Text = "";
                uTextStockTakeCode.Text = "";
                uTextStockTakeConfirmDate.Text = "";

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //레이블 이름,font,image,Mandentory설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDurableInventory, "창고", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeChargeID, "실사담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeDate, "실사일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeEtcDesc, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel5, "자재리스트", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStockTakeYear, "실사년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeMonth, "실사월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStockTakeCode, "실사번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStockTakeConfirmDate, "실사확정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeCode", "실사번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryCode", "실사창고코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryName", "실사창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockTakeQty", "실사량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid1, 0, "StockConfirmQty", "실사확정량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataRow row;
                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboDurableInventory.Items.Clear();
                //this.uComboDurableInventory.Text = "";



                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("Year", typeof(String));
                dtYear.Columns.Add("YearText", typeof(String));

                DataTable dtMonth = new DataTable();
                dtMonth.Columns.Add("Month", typeof(String));
                dtMonth.Columns.Add("MonthText", typeof(String));

                for (int i = 2011; i < 2022; i++)
                {
                    row = dtYear.NewRow();
                    row["Year"] = i;
                    row["YearText"] = i + "년도";
                    dtYear.Rows.Add(row);
                }

                for (int i = 1; i < 13; i++)
                {
                    row = dtMonth.NewRow();
                    if (i < 10)
                    {
                        row["Month"] = "0" + i;
                        row["MonthText"] = i+"월";

                    }
                    else
                    {
                        row["Month"] = i;
                        row["MonthText"] = i + "월";
                    }
                    dtMonth.Rows.Add(row);
                }

                wCombo.mfSetComboEditor(this.uComboStockTakeYear, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Year", "YearText", dtYear);

                wCombo.mfSetComboEditor(this.uComboStockTakeMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Month", "MonthText", dtMonth);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox, GroupBoxType.LIST, "치공구실사리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }



        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uComboPlant.Value.ToString();
                //string strDurableInventoryCode = this.uComboDurableInventory.Value.ToString();
                string strStockTakeYear = this.uComboStockTakeYear.Value.ToString();
                string strStockTakeMonth = this.uComboStockTakeMonth.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000265", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uComboDurableInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000789", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboDurableInventory.DropDown();
                    return;
                }

                if (strStockTakeYear == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000783", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboStockTakeYear.DropDown();
                    return;
                }

                if(strStockTakeMonth.Equals(string.Empty))
                {
                     msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000786", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboStockTakeMonth.DropDown();
                    return;
                }
                InitText();
                Display_Grid();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";
                int intStockTakeQty = 0;

                // 필수입력사항 확인
                if (this.uTextStockTakeConfirmDate.Text != "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001265", Infragistics.Win.HAlign.Center);
                    return;
                }

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uComboDurableInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000790", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboDurableInventory.DropDown();
                    return;
                }

                if (this.uComboStockTakeYear.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000782", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboStockTakeYear.DropDown();
                    return;
                }

                if (this.uComboStockTakeMonth.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000787", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboStockTakeMonth.DropDown();
                    return;
                }


                if (this.uDateStockTakeDate.Value == null || this.uDateStockTakeDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000788", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateStockTakeDate.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextStockTakeChargeID.Text == "" || this.uTextStockTakeChargeName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000785", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextStockTakeChargeID.Focus();
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");

                if(this.uGrid1.Rows.Count > 0)
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGrid1.Rows[i].Cells["StockTakeQty"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000513", strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["StockTakeQty"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString() != "")
                        {
                            intStockTakeQty = Convert.ToInt32(this.uGrid1.Rows[i].Cells["StockTakeQty"].Value.ToString());

                            if (intStockTakeQty > 1)
                            {
                                //번째 열의 LotNo가 있는 수량은 1 이상일 수가 없습니다
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000465", strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["StockTakeQty"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001019", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeH), "DurableStockTakeH");
                QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new QRPDMM.BL.DMMICP.DurableStockTakeH();
                brwChannel.mfCredentials(clsDurableStockTakeH);

                //--------- 1. EQUDurableStockTakeH 테이블에 저장 ----------------------------------------------//
                DataTable dtDurableStockTakeH = clsDurableStockTakeH.mfSetDatainfo();

                row = dtDurableStockTakeH.NewRow();
                row["PlantCode"] = this.uComboPlant.Value.ToString();
                row["StockTakeCode"] = this.uTextStockTakeCode.Text;
                row["DurableInventoryCode"] = this.uComboDurableInventory.Value.ToString();
                row["StockTakeYear"] = this.uComboStockTakeYear.Value.ToString();
                row["StockTakeMonth"] = this.uComboStockTakeMonth.Value.ToString();
                row["StockTakeDate"] = this.uDateStockTakeDate.Value.ToString();
                row["StockTakeChargeID"] = this.uTextStockTakeChargeID.Text;
                row["StockTakeEtcDesc"] = this.uTextStockTakeEtcDesc.Text;
                row["StockTakeConfirmDate"] = "";
                row["StockTakeConfirmID"] = "";
                row["ConfirmEtcDesc"] = "";

                dtDurableStockTakeH.Rows.Add(row);



                //-------- 2. EQUDurableStockTakeD 테이블에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeD), "DurableStockTakeD");
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new QRPDMM.BL.DMMICP.DurableStockTakeD();
                brwChannel.mfCredentials(clsDurableStockTakeD);

                DataTable dtDurableStockTakeD = clsDurableStockTakeD.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {

                        row = dtDurableStockTakeD.NewRow();

                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["StockTakeCode"] = "";
                        row["DurableInventoryCode"] = this.uComboDurableInventory.Value.ToString();
                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                        row["CurStockQty"] = this.uGrid1.Rows[i].Cells["Qty"].Value.ToString();
                        row["StockTakeQty"] = this.uGrid1.Rows[i].Cells["StockTakeQty"].Value.ToString();
                        row["StockConfirmQty"] = "0";
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStockTakeD.Rows.Add(row);

                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : DurableMat 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveStockTake(dtDurableStockTakeH, dtDurableStockTakeD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitText();
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uComboPlant_AfterCloseUp(object sender, EventArgs e)
        {
            
        }

        private void uComboDurableInventory_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                InitText();
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboStockTakeYear_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                InitText();
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }
                this.uComboStockTakeMonth.Value = "";
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uComboStockTakeMonth_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                InitText();
                Display_Grid();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboStockTakeMonth_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uComboPlant.Value.ToString();
                //string strDurableInventoryCode = this.uComboDurableInventory.Value.ToString();
                string strStockTakeYear = this.uComboStockTakeYear.Value.ToString();
                string strStockTakeMonth = this.uComboStockTakeMonth.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uComboDurableInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000789", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboDurableInventory.DropDown();
                    return;
                }

                if (strStockTakeYear == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000783", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboStockTakeYear.DropDown();
                    return;
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void Display_Grid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableInventoryCode = this.uComboDurableInventory.Value.ToString();
                string strStockTakeYear = this.uComboStockTakeYear.Value.ToString();
                string strStockTakeMonth = this.uComboStockTakeMonth.Value.ToString();

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeH), "DurableStockTakeH");
                //QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new QRPDMM.BL.DMMICP.DurableStockTakeH();
                //brwChannel.mfCredentials(clsDurableStockTakeH);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockTakeD), "DurableStockTakeD");
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new QRPDMM.BL.DMMICP.DurableStockTakeD();
                brwChannel.mfCredentials(clsDurableStockTakeD);

                DataTable dtDurableStockTakeD = clsDurableStockTakeD.mfReadDMMDurableStockTakeD(strPlantCode, strDurableInventoryCode, strStockTakeYear, strStockTakeMonth, m_resSys.GetString("SYS_LANG"));
                if (dtDurableStockTakeD.Rows.Count > 0)
                {
                    //테이터바인드
                    uGrid1.DataSource = dtDurableStockTakeD;
                    uGrid1.DataBind();

                    this.uTextStockTakeCode.Text = dtDurableStockTakeD.Rows[0]["StockTakeCode"].ToString();
                    this.uTextStockTakeConfirmDate.Text = dtDurableStockTakeD.Rows[0]["StockTakeConfirmDate"].ToString();
                }
                else
                {
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    // Call Method
                    DataTable dtSPTransferH = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                    //테이터바인드
                    uGrid1.DataSource = dtSPTransferH;
                    uGrid1.DataBind();
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitText();
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                this.uComboDurableInventory.Items.Clear();

                if (strPlantCode == "")
                    return;


                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                ////wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "DurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);


                wCombo.mfSetComboEditor(this.uComboDurableInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 사용자 검색
        /// <summary>
        /// 사용자 검색 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextStockTakeChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                this.uTextStockTakeChargeID.Text = frmUser.UserID;
                this.uTextStockTakeChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 사용자 검색 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextStockTakeChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    WinMessageBox msg = new WinMessageBox();

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUsrID = this.uTextStockTakeChargeID.Text;

                    DataTable dtUsr = clsUser.mfReadSYSUser(strPlantCode, strUsrID, m_resSys.GetString("SYS_LANG"));

                    if (dtUsr.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextStockTakeChargeID.Focus();
                        return;
                    }
                    else
                    {
                        this.uTextStockTakeChargeName.Text = dtUsr.Rows[0]["UserName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextStockTakeChargeName.Text = "";
                    this.uTextStockTakeChargeID.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
    }
}
