﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmDMMZ0022.cs                                        */
/* 프로그램명   : 금형치공구수명관리                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-02-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0022 : Form,IToolbar
    {
        #region 전역변수
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        QRPBrowser brwChannel = new QRPBrowser();

        private string strPackage;

        private string strModel;

        private string strCurUsage;

        private string strCumUsage;

        #endregion

        public frmDMMZ0022()
        {
            InitializeComponent();
        }

        private void frmDMMZ0022_Activated(object sender, EventArgs e)
        {
            ResourceSet m_Sysres= new ResourceSet(SysRes.SystemInfoRes);
            
            brwChannel.mfActiveToolBar(this.MdiParent, true, true, false, false, false, false, m_Sysres.GetString("SYS_USERID"), this.Name);
            m_Sysres.Close();
        }

        private void frmDMMZ0022_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmDMMZ0022_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitGroupBox();
            InitCombo();
            InitGrid();
            mfVisible(this.uGroupBoxResult, false);
            mfVisible(this.uGroupBoxChange, false);

            WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
              
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("금형치공구수명관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchPackage, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
               
                lbl.mfSetLabel(this.uLabelStandbyDate, "정비대기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairDate, "정비착수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFinishDate, "정비완료일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRepairResultCode, "점검결과", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInstall, "설비장착여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelJudeCharge, "Shot판정담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelShot, "Shot판정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatCode, "치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableMatLot, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSpec, "Spec", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grb = new WinGroupBox();

                grb.mfSetGroupBox(this.uGroupBoxDurableLot, GroupBoxType.LIST, "금형치공구리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                     Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                     Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupBoxSpec, GroupBoxType.DETAIL, "금형치공구상세", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                     Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                     Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupBoxResult, GroupBoxType.INFO, "정비결과정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                     Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                     Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grb.mfSetGroupBox(this.uGroupBoxChange, GroupBoxType.INFO, "치공구교체등록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                     Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                     Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                this.uGroupBoxDurableLot.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxDurableLot.Appearance.FontData.SizeInPoints = 9;

                this.uGroupBoxSpec.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxSpec.Appearance.FontData.SizeInPoints = 9;

                this.uGroupBoxResult.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxResult.Appearance.FontData.SizeInPoints = 9;

                this.uGroupBoxChange.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxChange.Appearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor com = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                 
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                com.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista,
                                        m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default,
                                        true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                // Call Method
                DataTable dtUserCommonCode = clsCommonCode.mfReadCommonCode("C0041", m_resSys.GetString("SYS_LANG"));
                com.mfSetComboEditor(this.uComboShot, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtUserCommonCode);

                dtUserCommonCode = clsCommonCode.mfReadCommonCode("C0065", m_resSys.GetString("SYS_LANG"));

                com.mfSetComboEditor(this.uComboInstall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtUserCommonCode);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                #region 금형치공구리스트

                grd.mfInitGeneralGrid(this.uGridDurableLotList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
               
                //루프를 돌며 컬럼15개 생성
                for (int i = 0; i < 15; i++)
                {
                    string strKey = "";
                    string strKeyName = "";
                    string strValue = "";
                    if (i < 9)
                    {
                        strKey = "L0" + (i + 1).ToString();
                        strValue = "D0" + (i + 1).ToString();
                    }
                    else
                    {
                        strKey = "L" + (i + 1).ToString();
                        strValue = "L" + (i + 1).ToString();
                    }

                    grd.mfSetGridColumn(this.uGridDurableLotList, 0, strKey, strKeyName, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    grd.mfSetGridColumn(this.uGridDurableLotList, 0, strValue, strKeyName, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                }

                this.uGridDurableLotList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableLotList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //해당그리드의 선택한 줄이나 셀의 속성을 비활성화한다.
                this.uGridDurableLotList.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridDurableLotList.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                #endregion

                #region SpecGrid

                grd.mfInitGeneralGrid(this.uGridSpec, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns,
                  false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                  Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                  Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridSpec, 0, "PlantCode", "", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpec, 0, "DurableMatCode", "", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpec, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 65, false, true, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpec, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpec, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpec, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridSpec, 0, "UsageLimit", "Limit", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridSpec, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridSpec, 0, "CurUsage", "Shot", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridSpec, 0, "CumUsage", "Shot", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");


                this.uGridSpec.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridSpec.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                this.uGridSpec.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSpec.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //컬럼 고정
                this.uGridSpec.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;

                this.uGridSpec.DisplayLayout.Override.CellAppearance.BackColor = Color.White;

                #endregion

                #region 수리출고 구성품
                // 수리출고구성품 Grid
                // 일반설정
                grd.mfInitGeneralGrid(this.uGridChangeDurable, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default
                    , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Set Column
                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "RepairGICode", "수리출고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "RepairGIGubunCode", "수리출고구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "INN");


                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "CurDurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "CurDurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "CurLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "ChgDurableInventoryCode", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "ChgDurableMatCode", "교체구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "ChgDurableMatName", "교체구성품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "ChgLotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "Qty", "가용수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "ChgQty", "교체수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGridChangeDurable, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridChangeDurable, 0, "CancelFlag", "수리취소", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                #region DropDown

                //--헤더에있는 체크박스를 사용안함 --//
                //this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["CancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--수리출고구분--//
                 
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtRepairGICode = clsCommonCode.mfReadCommonCode("C0016", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridChangeDurable, 0, "RepairGIGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtRepairGICode);


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGridChangeDurable, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                #endregion

                this.uGridChangeDurable.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridChangeDurable.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGridChangeDurable, 0);

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion

        #region Toolbar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                #region 필수입력사항
                if (this.uComboSearchDurableMatName.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M000340", Infragistics.Win.HAlign.Right);
                    this.uComboSearchDurableMatName.DropDown();
                    return;

                }

                #endregion

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                 
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotNoSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotNoSpecInfo);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDurableMatName = this.uComboSearchDurableMatName.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value == null ? "" : this.uComboSearchPackage.Value.ToString();
                string strEquipBOMChk = this.uComboInstall.Value.ToString();

                // Call Method
                DataTable dtLotNo = clsLotNoSpecInfo.mfReadLotNoSpecInfo_LotNo(strPlantCode, strDurableMatName, strPackage, strEquipBOMChk, m_resSys.GetString("SYS_LANG"));


                //테이터바인드
                this.uGridDurableLotList.DataSource = dtLotNo;
                this.uGridDurableLotList.DataBind();

                

                // 조회결과 없을시 MessageBox 로 알림
                if (dtLotNo.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableLotList, 0);

                    #region Limit초과한Lot조회
                    DataTable dtchk = clsLotNoSpecInfo.mfReadLotNoSpecInfo_LimitChk(strPlantCode, strDurableMatName, strPackage, strEquipBOMChk, m_resSys.GetString("SYS_LANG"));

                    int chk = 0;

                    if (dtchk.Rows.Count > 0)
                    {
                        for (int d = 0; d < dtchk.Rows.Count; d++)
                        {
                            //줄수
                            for (int i = 0; i < this.uGridDurableLotList.Rows.Count; i++)
                            {
                                //컬럼수
                                for (int j = 0; j < this.uGridDurableLotList.DisplayLayout.Bands[0].Columns.Count; j++)
                                {
                                    //컬럼명중이 D가 들어간 컬럼일경우 건너뛴다.(D 치공구코드)
                                    if (!this.uGridDurableLotList.Rows[i].Cells[j].Column.Key.Contains("D"))
                                    {
                                        //LotNo가 똑같은 셀정보를 찾는다.
                                        if (this.uGridDurableLotList.Rows[i].Cells[j].Value.ToString().Equals(dtchk.Rows[d]["LotNo"].ToString()))
                                        {
                                            //LotNo가 똑같다 해도 치공구코드가 틀릴 수 있다 (해당컬럼명이 L01이면 D01이 해당 LotNo의 치공구코드)
                                            string strKey = "D" + this.uGridDurableLotList.Rows[i].Cells[j].Column.Key.Replace("L", "");

                                            //치공구코드정보도 같으면 해당셀의 색을 변경한다.
                                            if (this.uGridDurableLotList.Rows[i].Cells[strKey].Value.ToString().Equals(dtchk.Rows[d]["DurableMatCode"].ToString()))
                                            {
                                                this.uGridDurableLotList.Rows[i].Cells[j].Appearance.BackColor = Color.Salmon;
                                                chk = 1; //리미트값 찾음
                                                break;
                                            }
                                        }
                                        
                                    }
                                }
                                if (!chk.Equals(0))
                                    break; //리미트값 찾음
                            }
                            chk = 0; // 다음리미트값을 위해 초기화
                        }
                    }

                    #endregion

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
       
        
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항 체크

                if (this.uGridDurableLotList.Rows.Count == 0 
                    || (this.uTextSpecCode.Text.Equals(string.Empty) || this.uTextSpecName.Equals(string.Empty)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001230", "M001047", Infragistics.Win.HAlign.Right);

                    // Focus
                    return;
                }
                
                if (this.uDateTimeUsageJudgeDate.Value == null || this.uDateTimeUsageJudgeDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001230", "M001457", Infragistics.Win.HAlign.Right);

                    this.uDateTimeUsageJudgeDate.DropDown();
                    return;
                }
                if (this.uTextJudeChargeID.Text.Equals(string.Empty) || this.uTextJudeChargeName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001230", "M001458", Infragistics.Win.HAlign.Right);

                    this.uTextJudeChargeID.Focus();
                    return;


                }
                if (this.uComboShot.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001230", "M001459", Infragistics.Win.HAlign.Right);

                    this.uComboShot.DropDown();
                    return;
                }


                #endregion


                //-------------------정보 저장-----------------//
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strEquipCode = this.uTextEquipCode.Text;
                string strShotDate = this.uDateTimeUsageJudgeDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strShotChargeID = this.uTextJudeChargeID.Text;

                DataRow drRow;

                #region 정기정검관리 정보

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                QRPDMM.BL.DMMMGM.InspectHist clsInspect = new QRPDMM.BL.DMMMGM.InspectHist();
                brwChannel.mfCredentials(clsInspect);

                DataTable dtInspect = clsInspect.mfSetDatainfo_Spec();

                drRow= dtInspect.NewRow();
                drRow["PlantCode"] = strPlantCode;
                drRow["UsageDocCode"] = "";
                drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                drRow["LotNo"] = this.uTextDurableMatLot.Text;
                drRow["SpecCode"]=this.uTextSpecCode.Text;
                drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["UsageJudgeCode"] = this.uComboShot.Value.ToString(); //판정
                drRow["UsageJudgeDate"] = this.uDateTimeUsageJudgeDate.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["JudgeChargeID"] = this.uTextJudeChargeID.Text;
                drRow["CurUsage"] = strCurUsage == "" ? "0":strCurUsage;
                drRow["CumUsage"] = strCumUsage == "" ? "0" : strCumUsage;
                drRow["EtcDesc"] = "";
  
                dtInspect.Rows.Add(drRow);

                #endregion


                #region 정비결과정보

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                DataTable 
                    dtRseaprH = clsRepairH.mfSetDatainfo_Spec();


                if (this.uComboShot.Value.ToString().Equals("R"))
                {
                    if (this.uDateFinishDate.Value == null || this.uDateFinishDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001230", "M000826", Infragistics.Win.HAlign.Right);

                        this.uDateFinishDate.DropDown();
                        return;
                    }
                    if (this.uComboRepairResultCode.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001230인", "M001064", Infragistics.Win.HAlign.Right);

                        this.uComboRepairResultCode.DropDown();
                        return;
                    }


                    string strFinishDate = this.uDateFinishDate.DateTime.Date.ToString("yyyy-MM-dd");
                    drRow = dtRseaprH.NewRow();

                    drRow["PlantCode"] = m_resSys.GetString("SYS_PLANTCODE");
                    drRow["RepairCode"] = "";
                    drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                    drRow["LotNo"] = this.uTextDurableMatLot.Text;
                    drRow["SpecCode"] = this.uTextSpecCode.Text;
                    drRow["EquipCode"] = this.uTextEquipCode.Text;
                    drRow["UsageDocCode"] = "";
                    drRow["StandbyFlag"] = "T";

                    drRow["StandbyDate"] = strFinishDate;
                    drRow["RepairFlag"] = "T";
                    drRow["RepairDate"] = strFinishDate;
                    drRow["FinishFlag"] = "T";
                    drRow["FinishDate"] = strFinishDate;
                    drRow["RepairResultCode"] = this.uComboRepairResultCode.Value.ToString();
                    drRow["ChangeFlag"] = this.uCheckChangeFlag.Checked == true ? "T" : "F";
                    drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                    drRow["UsageClearFlag"] = this.uCheckUsageClearFlag.Checked == true ? "T":"F";
                    drRow["UsageClearDate"] = this.uCheckUsageClearFlag.Checked == true ? strFinishDate : "";
                    drRow["ClearCurUsage"] = strCurUsage == "" ? "0" : strCurUsage;
                    drRow["ClearCumUsage"] = strCumUsage == "" ? "0" : strCumUsage;

                    dtRseaprH.Rows.Add(drRow);

                }

                #endregion

                #region 치공구

                //치공구교체정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                //---헤더값을 넣을 데이터 테이블---//
                DataTable dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                brwChannel.mfCredentials(clsDurableMatRepairD);

                //--상세정보를 넣을 데이터 테이블--//
                DataTable dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();
                //--행삭제된 정보를 넣을 데이터테이블--//
                //DataTable dtDurableRepairDel = clsDurableMatRepairD.mfsetDataInfo();
                //dtDurableRepairD.Columns.Add("UnitCode", typeof(string));
                dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                dtDurableRepairD.Columns.Add("Qty", typeof(string));
                dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));


                //설비구성품정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);


                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);



                if (this.uGridChangeDurable.Rows.Count > 0 && this.uCheckChangeFlag.Checked)
                {

                    #region 상세그리드 저장

                    string strLang = m_resSys.GetString("SYS_LANG");

                    if (this.uGridChangeDurable.Rows.Count > 0)
                    {
                        this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridChangeDurable.Rows.Count; i++)
                        {
                            //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.

                            ////조회된 정보에서 수리취소가 false인데 편집이미지가 있으면 없앤다(수리출고구분 수정가능일 시 다른방법을..)
                            //if (this.uGridRepair.Rows[i].Cells["CancelFlag"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                            //    && Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value) == false && this.uGridRepair.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                            //    this.uGridRepair.Rows[i].RowSelectorAppearance.Image = null;
                            //}

                            if (this.uGridChangeDurable.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                if (this.uGridChangeDurable.Rows[i].Hidden == false)
                                {
                                    if (this.uGridChangeDurable.Rows[i].Cells["RepairGIGubunCode"].Value.ToString() != "")
                                    {
                                        #region 상세 필수입력사항 확인
                                        string strRowNum = this.uGridChangeDurable.Rows[i].RowSelectorNumber.ToString();

                                        if (!this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty) && this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value.ToString().Equals("0"))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000519",strLang)
                                                , Infragistics.Win.HAlign.Right);

                                            //PerFormAction
                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["InputQty"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        if (this.uGridChangeDurable.Rows[i].Cells["InputQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridChangeDurable.Rows[i].Cells["InputQty"].Tag != null 
                                            && Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value) > Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["InputQty"].Tag))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M000730",strLang)
                                                , strRowNum + msg.GetMessge_Text("M000539",strLang)
                                                , Infragistics.Win.HAlign.Right);
                                            //PerFormAction
                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["InputQty"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001230",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000552",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;

                                        }
                                        if (this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatName"].Value.ToString() == "" 
                                            || this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001230",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000452",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }
                                        if (this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001230",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000453",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["ChgQty"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                            && Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["Qty"].Value) < Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , strRowNum + msg.GetMessge_Text("M000454",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["ChgQty"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        #region 구성품 수량체크

                                        //구성품 교체 시
                                        //if (!Convert.ToBoolean(this.uGridRepair.Rows[i].Cells["CancelFlag"].Value).Equals(true))
                                        //{
                                        //BOM정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        if (!this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                                        {
                                            DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                                                                                        this.uGridChangeDurable.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));


                                            //BOM 정보와 비교하여 정보가 없거나 수량이 부족할 경우 콤보를 다시 뿌려줌
                                            if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    msg.GetMessge_Text("M001264",strLang)
                                                                    , msg.GetMessge_Text("M000310",strLang)
                                                                    , strRowNum + msg.GetMessge_Text("M000497",strLang)
                                                                    , Infragistics.Win.HAlign.Right);

                                                #region DropDown

                                                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                                                //--해당설비의 BOM 이없을 경우 리턴 --
                                                if (dtDurable.Rows.Count == 0)
                                                {
                                                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M000715", "M001251", Infragistics.Win.HAlign.Right);

                                                    return;
                                                }
                                                //콤보 그리드 리스트 클러어
                                                this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();

                                                //콤보그리드 컬럼설정
                                                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                                                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                                                //--그리드에 추가 --
                                                WinGrid wGrid = new WinGrid();
                                                wGrid.mfSetGridColumnValueGridList(this.uGridChangeDurable, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                                                    , "DurableMatCode", "DurableMatName", dtDurable);
                                                #endregion

                                                this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value = 0;
                                                this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value = string.Empty;

                                                this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["CurDurableMatName"];
                                                this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                        //Stock의 재고정보를 불러와 수량 혹은 정보 유무를 판별한다.
                                        DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                                this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                                                                                this.uGridChangeDurable.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //재고정보와 비교하여 정보가 없거나 수량이 부족 할 경우 콤보를 다시뿌려줌
                                        if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                msg.GetMessge_Text("M001264",strLang)
                                                                , msg.GetMessge_Text("M000310",strLang)
                                                                , strRowNum + msg.GetMessge_Text("M000487",strLang)
                                                                , Infragistics.Win.HAlign.Right);

                                            ChgDurbleMat(strPlantCode, this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(), i);
                                            this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatCode"].Value = string.Empty;
                                            this.uGridChangeDurable.Rows[i].Cells["ChgLotNo"].Value = string.Empty;
                                            this.uGridChangeDurable.Rows[i].Cells["Qty"].Value = 0;
                                            this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value = 0;
                                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatName"];
                                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                            return;
                                        }

                                        //}
                                        //// 수리취소 할 경우
                                        //else
                                        //{
                                        //    DataTable dtDurableMat = clsEquipDurableBOM.mfReadDurableBOM_TotalQty(strPlantCode, strEquipCode, this.uGridRepair.Rows[i].Cells["ChgDurableMatCode"].Value.ToString(),
                                        //                                                                this.uGridRepair.Rows[i].Cells["ChgLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_DetailLot(strPlantCode, this.uGridRepair.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurDurableMatCode"].Value.ToString(),
                                        //                                                            this.uGridRepair.Rows[i].Cells["CurLotNo"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                        //    //BOM정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableMat.Rows.Count == 0 || Convert.ToInt32(dtDurableMat.Rows[0]["TotalQty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["ChgQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "M001264", "수리취소 정보확인", strRowNum + "번째 열의 기존구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //    //재고정보와 비교하여 정보가 없거나 수량이 부족하면 수리취소 불가
                                        //    if (dtDurableStock.Rows.Count == 0 || Convert.ToInt32(dtDurableStock.Rows[0]["Qty"]) < Convert.ToInt32(this.uGridRepair.Rows[i].Cells["InputQty"].Value))
                                        //    {
                                        //        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        //                            "M001264", "수리취소 정보확인", strRowNum + "번째 열의 교체구성품이 교체되거나 수량이 부족하여 취소 할 수 없습니다.", Infragistics.Win.HAlign.Right);
                                        //        this.uGridRepair.ActiveCell = this.uGridRepair.Rows[i].Cells["CancelFlag"];
                                        //        return;
                                        //    }

                                        //}

                                        #endregion

                                        #endregion


                                        DataRow drRepairD;
                                        drRepairD = dtDurableRepairD.NewRow();
                                        drRepairD["PlantCode"] = strPlantCode;                                                                  //공장코드
                                        drRepairD["Seq"] = this.uGridChangeDurable.Rows[i].Cells["Seq"].Value.ToString();                           //순번
                                        drRepairD["ItemGubunCode"] = "TL";
                                        drRepairD["RepairGIGubunCode"] = this.uGridChangeDurable.Rows[i].Cells["RepairGIGubunCode"].Value.ToString();//수리출고구분
                                        drRepairD["CurDurableMatCode"] = this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value.ToString(); //구성품
                                        drRepairD["CurQty"] = this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value.ToString();                  //기존수량
                                        drRepairD["CurLotNo"] = this.uGridChangeDurable.Rows[i].Cells["CurLotNo"].Value.ToString();                //기존 LotNo
                                        drRepairD["ChgDurableInventoryCode"] = this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"].Value.ToString();//교체창고
                                        drRepairD["ChgDurableMatCode"] = this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();//교체구성품
                                        drRepairD["ChgDurableMatName"] = this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatName"].Text;
                                        drRepairD["ChgQty"] = this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value.ToString();                //교체수량
                                        drRepairD["Qty"] = this.uGridChangeDurable.Rows[i].Cells["Qty"].Value.ToString();                      //가용수량
                                        drRepairD["ChgLotNo"] = this.uGridChangeDurable.Rows[i].Cells["ChgLotNo"].Value.ToString();            //변경된LotNo
                                        drRepairD["UnitCode"] = this.uGridChangeDurable.Rows[i].Cells["UnitCode"].Value.ToString();            //단위
                                        //drRepairD["UnitName"] = this.uGridRepair.Rows[i].Cells["UnitName"].Value.ToString();
                                        drRepairD["EtcDesc"] = this.uGridChangeDurable.Rows[i].Cells["EtcDesc"].Value.ToString();              //특이사항
                                        drRepairD["CancelFlag"] = "False";//this.uGridRepair.Rows[i].Cells["CancelFlag"].Value.ToString().Substring(0, 1);
                                        dtDurableRepairD.Rows.Add(drRepairD);

                                    }

                                }
                            }
                        }
                    }
                    #endregion

                    if (dtDurableRepairD.Rows.Count > 0)
                    {
                        #region 치공구교체헤더저장

                        DataRow drRepairH;
                        drRepairH = dtDurableRepairH.NewRow();

                        drRepairH["PlantCode"] = strPlantCode;
                        drRepairH["EquipCode"] = strEquipCode;
                        drRepairH["RepairGICode"] = "";
                        drRepairH["GITypeCode"] = "LF";
                        drRepairH["RepairGIReqDate"] = strShotDate;
                        drRepairH["RepairGIReqID"] = strShotChargeID;
                        drRepairH["EtcDesc"] = this.uTextEtcDesc.Text;


                        dtDurableRepairH.Rows.Add(drRepairH);

                        #endregion

                    }

                }

                #endregion


                //-----------------저장여부 묻기-------------------//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--------POPUP창 열기--------//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    string strErrRtn = clsInspect.mfSaveInspectHist_Spec(dtInspect,
                                                                        dtRseaprH,
                                                                        dtDurableRepairH,
                                                                        dtDurableRepairD,
                                                                        m_resSys.GetString("SYS_USERIP"),
                                                                        m_resSys.GetString("SYS_USERID"));


                    ///---Decoding---//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--커서변경--//
                    this.Cursor = Cursors.Default;

                    //-------POPUP창 닫기-------//
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //----처리결과에 따른 메세지박스---//
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        if(this.uGroupBoxResult.Visible)
                            mfVisible(this.uGroupBoxResult, false);

                        if (this.uGroupBoxChange.Visible)
                            mfVisible(this.uGroupBoxChange, false);
                        Clear();
                        mfSearch();
                    }
                    else
                    {
                        string strErr = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strErr = "M000953";
                        else
                            strErr = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", strErr,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        { }

        public void mfCreate()
        { }

        public void mfExcel()
        { }

        public void mfPrint()
        { }

        #endregion


        #region Event

        #region Combo
        /// <summary>
        /// 공장콤보의 공장이 변경시 패키지 금형치공구명콤보가 변경이 된다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //System ResourceInfo

                WinComboEditor wCombo = new WinComboEditor();
                string strPlantCode = uComboSearchPlant.Value.ToString();

                 

                //제품정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                //패키지콤보매서드 호출
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                if (this.uComboSearchPackage.Items.Count > 0)
                    this.uComboSearchPackage.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "Package", "ComboName", dtPackage);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtDurableMatName = clsDurableMat.mfReadDurableMatNameCombo(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                if (this.uComboSearchDurableMatName.Items.Count > 0)
                    this.uComboSearchDurableMatName.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboSearchDurableMatName, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "선택", "DurableMat", "DurableMatName", dtDurableMatName);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Shot판정의 콤보정보에 따라 정비결과정보를 등록할수있다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboShot_ValueChanged(object sender, EventArgs e)
        {

            try
            {
                //Spec정보를 클릭시 이벤트발생함
                if (this.uTextSpecCode.Text.Equals(string.Empty))
                    return;

                string strCode = this.uComboShot.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboShot.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboShot.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboShot.Items[i].DataValue.ToString()) && strText.Equals(this.uComboShot.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty) && strCode.Equals("R"))
                    {
                        mfVisible(this.uGroupBoxResult, true);
                    }
                    else
                    {
                        mfVisible(this.uGroupBoxResult, false);
                    }
                }
                else
                {
                    mfVisible(this.uGroupBoxResult, false);
                    
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 점검결과선택에 따른 교체여부체크
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboRepairResultCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboRepairResultCode.Items.Count <= 0)
                    return;

                string strPlantCode = this.uTextEquipCode.Tag.ToString();
                string strResultCode = this.uComboRepairResultCode.Value.ToString();


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //점검결과등록정보BL 호출
                 
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                //점검결과등록정보 상세정보 조회매서드 실행
                DataTable dtResultType = clsPMResultType.mfReadPMResultType_Detail(strPlantCode, strResultCode, m_resSys.GetString("SYS_LANG"));

                //정보가 있으면 교체여부를 확인한다. (교체여부가 T 인 경우 교체여부를 체크해준다.)
                if (dtResultType.Rows.Count > 0)
                {
                    if (dtResultType.Rows[0]["ChangeFlag"].ToString().Equals("T"))
                        this.uCheckChangeFlag.Checked = true;
                    else
                        this.uCheckChangeFlag.Checked = false;
                }
                else
                    this.uCheckChangeFlag.Checked = false;



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Grid

        /// <summary>
        /// 금형치공구리스트의 셀을 더블클릭할 경우 해당 셀의 LotNo의 상세내용이 조회된다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridDurableLotList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //셀의정보가 공백인경우 리턴
                if (e.Cell.Value.ToString().Equals(string.Empty))
                    return;
                //리피트된 LotNo가 아니면 리턴
                //if(!e.Cell.Appearance.BackColor.Equals(Color.Salmon))
                //    return;

                //필요정보 생성
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strDurableMatCode = e.Cell.Row.Cells["D" + e.Cell.Column.Key.Replace("L","")].Value.ToString();
                string strLotNo = e.Cell.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                 
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotNoSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotNoSpecInfo);

                DataTable dtSpec = clsLotNoSpecInfo.mfReadLotNoSpecInfo_Detail(strPlantCode, strDurableMatCode, strLotNo, "All", "All");

                this.uGridSpec.DataSource = dtSpec;
                this.uGridSpec.DataBind();


                if (dtSpec.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridSpec.Rows.Count; i++)
                    {
                        //색변경 (상한치,리미트,하한치 초과시)
                        if (Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("CurUsage")) > Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("UsageLimitUpper")))
                        {
                            this.uGridSpec.Rows[i].Appearance.BackColor = Color.MistyRose;
                            this.uGridSpec.Rows[i].Cells["CurUsage"].Appearance.BackColor = Color.White;
                        }
                        else if (Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("CurUsage")) > Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("UsageLimit")))
                        {
                            this.uGridSpec.Rows[i].Appearance.BackColor = Color.Plum;
                            this.uGridSpec.Rows[i].Cells["UsageLimitUpper"].Appearance.BackColor = Color.White;
                            this.uGridSpec.Rows[i].Cells["CurUsage"].Appearance.BackColor = Color.White;
                        }
                        else if (Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("CurUsage")) > Convert.ToInt32(this.uGridSpec.Rows[i].GetCellValue("UsageLimitLower")))
                        {
                            this.uGridSpec.Rows[i].Appearance.BackColor = Color.PaleGreen;

                            this.uGridSpec.Rows[i].Cells["UsageLimitUpper"].Appearance.BackColor = Color.White;
                            this.uGridSpec.Rows[i].Cells["UsageLimit"].Appearance.BackColor = Color.White;
                            this.uGridSpec.Rows[i].Cells["CurUsage"].Appearance.BackColor = Color.White;
                            
                        }
                           
                    }
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 금형치공구상세Spec정보 더블클릭시 해당 Spec의 정보를 등록할 수 있다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridSpec_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {

                if (e.Row.Appearance.BackColor.Equals(Color.Empty))
                    return;

                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strDurableMatCode = e.Row.Cells["DurableMatCode"].Value.ToString();
                string strLotNo = e.Row.Cells["LotNo"].Value.ToString();
                string strSpecCode = e.Row.Cells["SpecCode"].Value.ToString();

                this.uTextDurableMatCode.Text = strDurableMatCode;
                this.uTextDurableMatLot.Text = strLotNo;
                strCurUsage = e.Row.Cells["CurUsage"].Value.ToString();
                strCumUsage = e.Row.Cells["CumUsage"].Value.ToString();

                this.uTextEquipCode.Tag = strPlantCode;
                this.uTextSpecCode.Text = strSpecCode;
                this.uTextSpecName.Text = e.Row.Cells["SpecName"].Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //해당 치공구의 BOM정보확인

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                DataTable dtEquip = clsEquipDurableBOM.mfReadDurableBOM_Equip(strPlantCode, strDurableMatCode, strLotNo, m_resSys.GetString("SYS_LANG"));

                if (dtEquip.Rows.Count > 0)
                {
                    if (!this.uTextEquipCode.Text.Equals(dtEquip.Rows[0]["EquipCode"].ToString()))
                    {
                        this.uTextEquipCode.Text = dtEquip.Rows[0]["EquipCode"].ToString();

                        //BL호출
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                        QRPEQU.BL.EQUREP.RepairReq clsEquip = new QRPEQU.BL.EQUREP.RepairReq();
                        brwChannel.mfCredentials(clsEquip);

                        DataTable dtEquipInfo = clsEquip.mfReadEquipRepair_MESDB_Detail(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                        strPackage = dtEquipInfo.Rows[0]["PackageA"].ToString();
                        strModel = dtEquipInfo.Rows[0]["ModelName"].ToString();
                    }
                }
                else
                {
                    strPackage = string.Empty;
                    strModel = string.Empty;
                }

                if (!this.uComboShot.SelectedIndex.Equals(0))
                    this.uComboShot.SelectedIndex = 0;

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                //점검결과 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResult = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResult);


                DataTable dtResult = clsPMResult.mfReadPMResultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboRepairResultCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista,
                                        m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default,
                                        true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "PMResultCode", "PMResultName", dtResult);


                WinGrid wGrid = new WinGrid();
                //-----금형치공구창고정보-----//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInven = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridChangeDurable, 0, "ChgDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtDurableInven);


                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                wGrid.mfSetGridColumnValueList(this.uGridChangeDurable, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 셀에 업데이트가 발생하면 편집이미지를 나타나게 한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridChangeDurable_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;

                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridChangeDurable, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);

                }

                //--체크박스가 수정불가 일시 false로 되돌린다 
                if (strColumn == "Check")
                {
                    if (e.Cell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    {
                        e.Cell.Value = false;
                        return;
                    }
                }
                ////-- 수리취소 체크 박스를 체크 해제하면 편집이미지 X --//
                //if (strColumn == "CancelFlag")
                //{
                //    if (e.Cell.Value.ToString() == "False")
                //    {
                //        e.Cell.Row.RowSelectorAppearance.Image = null;
                //        return;
                //    }
                //}

                #region 수리출고 구분 셀업데이트시 창고 교체량 교체구성품코드 셀의 헤더 색이 필수로 변한다
                if (strColumn == "RepairGIGubunCode")
                {
                    if (e.Cell.Value.ToString() != "")
                    {
                        //-- 헤더 색변화 --//
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Red;
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Red;
                    }
                    else
                    {
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgDurableInventoryCode"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgDurableMatName"].Header.Appearance.ForeColor = Color.Black;
                        this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["ChgQty"].Header.Appearance.ForeColor = Color.Black;
                    }
                }
                #endregion

                #region 수량 비교
                //수량을 입력한 후 해당설비구성품의 수량과 비교함 (입력한 수량보다 설비구성품의수량이 크면 안됨)
                if (strColumn.Equals("InputQty")
                    && !e.Cell.Value.ToString().Equals(string.Empty)
                    && !e.Cell.Value.ToString().Equals("0"))
                {
                    if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;
                        for (int i = 0; i < this.uGridChangeDurable.Rows.Count; i++)
                        {
                            if (this.uGridChangeDurable.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridChangeDurable.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridChangeDurable.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(string.Empty))
                            {
                                if (
                                    e.Cell.Row.Cells["CurDurableMatCode"].Value.Equals(this.uGridChangeDurable.Rows[i].Cells["CurDurableMatCode"].Value))
                                {

                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["InputQty"].Value);

                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["CurLotNo"].Tag;

                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000617", "M000659" + strQty + "M000010", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }

                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Tag))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000617", "M000903", Infragistics.Win.HAlign.Right);
                            //수량되돌림
                            e.Cell.Value = e.Cell.Tag;
                            //PerFormAction
                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[e.Cell.Row.Index].Cells["InputQty"];
                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                    else
                        e.Cell.Value = 0;
                }
                #endregion

                #region 교체수량비교

                if (strColumn.Equals("ChgQty"))
                {
                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        //입력한 숫자가 해당구성품의 갯수 보다 클 경우 메세지
                        if ((!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty)
                                && Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag))
                            || (Convert.ToInt32(e.Cell.Value) > Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value)))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000730", "M000890", Infragistics.Win.HAlign.Right);

                            //교체하는 금형치공구 수량적용
                            if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag))
                            {
                                e.Cell.Value = 0;//e.Cell.Row.Cells["InputQty"].Tag;
                            }
                            else
                            {
                                e.Cell.Value = 0;// e.Cell.Row.Cells["Qty"].Value;
                            }

                            //PerFormAction
                            this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[e.Cell.Row.Index].Cells["ChgQty"];
                            this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        #region 중복구성품재고수량체크

                        //현재 입력한교체수량
                        int intChgQty = 0;
                        int intQty = 0;

                        //선택한 정보가 그리드내에 있으면 재고량 - 교체수량 하여 0이면 초기화 0이아니면 가용수량에 값을 넣는다.
                        for (int i = 0; i < this.uGridChangeDurable.Rows.Count; i++)
                        {
                            if (this.uGridChangeDurable.Rows[i].RowSelectorAppearance.Image != null
                                && this.uGridChangeDurable.Rows[i].Cells["Check"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                && this.uGridChangeDurable.Rows[i].Hidden.Equals(false)
                                && this.uGridChangeDurable.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(string.Empty)
                                )
                            {
                                if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.Equals(this.uGridChangeDurable.Rows[i].Cells["ChgDurableMatCode"].Value) &&
                                    e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.Equals(this.uGridChangeDurable.Rows[i].Cells["ChgDurableInventoryCode"].Value))
                                {
                                    if (intQty.Equals(0))
                                    {
                                        intQty = Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["Qty"].Value);
                                    }
                                    intChgQty = intChgQty + Convert.ToInt32(this.uGridChangeDurable.Rows[i].Cells["ChgQty"].Value);


                                }
                            }
                        }

                        if (intChgQty > intQty)
                        {

                            string strQty = Convert.ToString(intChgQty - intQty) + " " + e.Cell.Row.Cells["UnitCode"].Value;
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M000302", "M000891" + strQty + " M000010", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = 0;

                            return;
                        }


                        #endregion
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수리출고구성품그리드에서 창고 콤보를 선택하기 전 수리출고구분 체크
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridChangeDurable_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //---창고 컬럼일때 발생--//
                if (e.Cell.Column.Key == "ChgDurableInventoryCode")
                {
                    
                    //--- 수리출고구분이 "" 일경우 메세지 박스 ---//
                    if (e.Cell.Row.Cells["RepairGIGubunCode"].Value.ToString() == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000962", "M000743", Infragistics.Win.HAlign.Right);

                        this.uGridChangeDurable.ActiveCell = this.uGridChangeDurable.Rows[e.Cell.Row.Index].Cells["RepairGIGubunCode"];
                        this.uGridChangeDurable.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 그리드 컬럼중 리스트 컬럼의 아이템을 선택하였을 때 발생함
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridChangeDurable_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strColumn = e.Cell.Column.Key;
                int intSelectRow;
                //공장 정보저장
                string strPlantCode = this.uTextEquipCode.Tag.ToString();
                string strEquipCode = this.uTextEquipCode.Text;

                //선택한 리스트의 Key값과 Value값 저장(컬럼그리드콤보가 없으면 셀그리드 콤보 정보로
                string strKey = "";
                string strValue = "";

                if (e.Cell.Column.ValueList != null)
                {
                    strKey = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                }
                else
                {
                    strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);
                }


                #region 구성품명 의 리스트 선택시 선택한 이름과 코드를 각 셀에 넣는다

                if (strColumn == "CurDurableMatName")
                {

                    //공백일 경우
                    if (strKey.Equals(string.Empty))
                    {
                        //수량,구성품코드 공백처리
                        e.Cell.Row.Cells["InputQty"].Value = 0;
                        e.Cell.Row.Cells["CurDurableMatCode"].Value = "";

                    }
                    else
                    {
                        intSelectRow = e.Cell.Column.ValueList.SelectedItemIndex;

                        //설비구성품정보 BL 호출

                        //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        //QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        //brwChannel.mfCredentials(clsEquipDurableBOM);

                        //DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOM_DurableMatInputQty(strPlantCode, this.uTextEquipCode.Text, strKey, m_resSys.GetString("SYS_LANG"));

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                        QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                        brwChannel.mfCredentials(clsEquipDurableBOM);

                        DataTable dtDurableMatInfo = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                        e.Cell.Row.Cells["CurDurableMatCode"].Value = strKey;

                        //LotNo가 없으면 수량을 직접입력할수있고(100개중 50개만 교체대상이 될수도있음) LotNo가 있으면 할수없다.(LotNo가있는 구성품은 무조건1개라서)
                        if (dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            this.uGridChangeDurable.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = "";
                        }
                        else
                        {
                            //e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridChangeDurable.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            e.Cell.Row.Cells["InputQty"].Tag = 1;
                            e.Cell.Row.Cells["InputQty"].Value = dtDurableMatInfo.Rows[intSelectRow]["InputQty"];
                            e.Cell.Row.Cells["CurLotNo"].Value = dtDurableMatInfo.Rows[intSelectRow]["LotNo"];
                        }

                        //단위저장
                        e.Cell.Row.Cells["CurLotNo"].Tag = dtDurableMatInfo.Rows[intSelectRow]["UnitCode"];

                        //Lot정보가 있는 구성품을 선택하였을 경우 리스트내에 똑같은 구성품이 올수없다.
                        for (int i = 0; i < this.uGridChangeDurable.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                this.uGridChangeDurable.Rows[i].Cells["CurLotNo"].Value.ToString().Equals(dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString()) &&
                                !dtDurableMatInfo.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                !this.uGridChangeDurable.Rows[i].Appearance.BackColor.Equals(Color.Yellow) &&
                                this.uGridChangeDurable.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["InputQty"].Value = 0;
                                e.Cell.Row.Cells["InputQty"].Tag = 0;
                                e.Cell.Row.Cells["CurLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["CurLotNo"].Tag = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatCode"].Value = string.Empty;
                                e.Cell.Row.Cells["CurDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }


                        }

                        //창고 콤보가 선택되어있다면,
                        if (!e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString().Equals(string.Empty))
                        {
                            if (!e.Cell.Row.Cells["CurLotNo"].Value.Equals(e.Cell.Row.Cells["ChgLotNo"].Value))
                            {
                                ChgDurbleMat(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(), e.Cell.Row.Index);
                                e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                e.Cell.Row.Cells["Qty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                            else
                            {
                                if (e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) || Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value) > Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Value))
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;
                            }
                        }

                    }

                }
                #endregion

                #region 교체구성품창고의 구성품코드 리스트를 가져온다
                //선택한 교체구성품창고의 구성품코드리스트를 가져온다.
                if (strColumn.Equals("ChgDurableInventoryCode"))
                {
                    if (!strKey.Equals(string.Empty))
                    {
                        ChgDurbleMat(strPlantCode, strKey, e.Cell.Row.Index);
                    }

                }
                #endregion

                #region 교체구성품코드 선택시 가용량 Lot정보 삽입

                if (strColumn.Equals("ChgDurableMatName"))
                {
                    if (!strKey.Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                        QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                        brwChannel.mfCredentials(clsRepairReq);

                        DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                        if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                            return;

                        //금형치공구정보 BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                        brwChannel.mfCredentials(clsDurableStock);

                        //금형치공구정보 Detail매서드 호출
                        DataTable dtStock = new DataTable();


                        //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                        string strLotChk = "";

                        if (!e.Cell.Row.Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                            strLotChk = e.Cell.Row.Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                        dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, e.Cell.Row.Cells["ChgDurableInventoryCode"].Value.ToString(),
                                                                                dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));

                        if (dtStock.Rows.Count > 0)
                        {
                            intSelectRow = e.Cell.ValueList.SelectedItemIndex;

                            ////구성품끼리의 단위가 다를경우 경고
                            //if (!e.Cell.Row.Cells["CurLotNo"].Tag.Equals(dtStock.Rows[intSelectRow]["UnitCode"]))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                       , "M001264", "구성품 단위 확인", "기존구성품의 단위와 교체구성품의 단위가 다릅니다. 다시 선택해주세요.", Infragistics.Win.HAlign.Right);
                            //    return;
                            //}
                            //그리드콤보에 선택한 정보를 삽입
                            e.Cell.Row.Cells["ChgLotNo"].Value = dtStock.Rows[intSelectRow]["LotNo"];
                            e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectRow]["Qty"];
                            e.Cell.Row.Cells["ChgDurableMatCode"].Value = strKey;
                            //e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectRow]["UnitCode"];

                            //Lot정보가 있을경우 교체수량1개 수정불가로 변경
                            if (!dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty))
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 1;

                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            }
                            else
                            {
                                e.Cell.Row.Cells["ChgQty"].Value = 0;
                                e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }

                            //선택한 정보가 그리드내에 있으면 다시입력
                            for (int i = 0; i < this.uGridChangeDurable.Rows.Count; i++)
                            {
                                if (!i.Equals(e.Cell.Row.Index) &&
                                    uGridChangeDurable.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtStock.Rows[intSelectRow]["LotNo"].ToString()) &&
                                    !dtStock.Rows[intSelectRow]["LotNo"].ToString().Equals(string.Empty) &&
                                    this.uGridChangeDurable.Rows[i].Cells["Check"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit &&
                                   !this.uGridChangeDurable.Rows[i].Appearance.BackColor.Equals(Color.Yellow))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000068", "M000853", Infragistics.Win.HAlign.Right);

                                    e.Cell.Row.Cells["Qty"].Value = 0;
                                    e.Cell.Row.Cells["ChgQty"].Value = 0;

                                    e.Cell.Row.Cells["ChgLotNo"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgDurableMatName"].Value = string.Empty;
                                    e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                    return;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Text

        /// <summary>
        /// Shot판정담당자
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextJudeChargeID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextJudeChargeName.Text.Equals(string.Empty))
                this.uTextJudeChargeName.Clear();
        }

        /// <summary>
        /// Shot판정담당자 직접입력조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextJudeChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                    this.uTextJudeChargeName.Clear();
                

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strUserID = this.uTextJudeChargeID.Text;
                    string strPlantCode = this.uTextEquipCode.Tag == null ? "" : this.uTextEquipCode.Tag.ToString();

                    WinMessageBox msg = new WinMessageBox();
                    DialogResult result;
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strUserID == "")
                    {

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextJudeChargeID.Focus();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                        this.uTextJudeChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                        this.uTextJudeChargeName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Shot판정담당자Editor버튼조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextJudeChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uTextEquipCode.Tag == null ? "" : this.uTextEquipCode.Tag.ToString();
                if (strPlant.Equals(string.Empty))
                    return;

                frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 유저팝업창에 보낸다
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                //해당 텍스트에 삽입
                this.uTextJudeChargeID.Text = frmUser.UserID;
                this.uTextJudeChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion


        /// <summary>
        /// 교체여부체크박스에 체크가 되면  치공구교체등록을 할수있다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckChangeFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //점검결과콤보값이 공백인경우 리턴
                if (this.uComboRepairResultCode.Value.ToString().Equals(string.Empty))
                    return;

                //설비코드 없는 경우 리턴
                if (this.uTextEquipCode.Text.Equals(string.Empty) && this.uCheckChangeFlag.Checked)
                {
                    //WinMessageBox msg = new WinMessageBox();
                    //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    //DialogResult Result;
                    //Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //    , "M001264", "M001135", "M001460", Infragistics.Win.HAlign.Right);

                    //this.uComboRepairResultCode.SelectedIndex = 0;
                    this.uCheckChangeFlag.Checked = false;
                    return;

                }
                if(this.uTextEquipCode.Text.Equals(string.Empty))
                    return;

                //체크여부에따라 치공구교체 활성화 비활성화처리
                if (this.uCheckChangeFlag.Checked)
                {
                    mfVisible(this.uGroupBoxChange, true);
                    SearchDurableBOM(this.uTextEquipCode.Tag.ToString(), this.uTextEquipCode.Text);
                }
                else
                {
                    mfVisible(this.uGroupBoxChange, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion

        #region Method

        private void Clear()
        {
            try
            {
                strPackage = string.Empty;
                strModel = string.Empty;
                
                strCurUsage = string.Empty;
                strCumUsage = string.Empty;

                this.uTextEquipCode.Tag = string.Empty;
                this.uTextDurableMatLot.Clear();
                this.uTextDurableMatCode.Clear();
                this.uTextEquipCode.Clear();
                this.uTextSpecCode.Clear();
                this.uTextSpecName.Clear();
                this.uComboShot.SelectedIndex = 0;
                this.uDateTimeUsageJudgeDate.Value = DateTime.Now.Date;
                this.uTextJudeChargeName.Clear();
                this.uTextJudeChargeID.Clear();

                if (this.uGridSpec.Rows.Count > 0)
                {
                    this.uGridSpec.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridSpec.Rows.All);
                    this.uGridSpec.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 정비결과정보,치공구교체등록그룹박스 활성화,비활성화
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="bol"></param>
        private void mfVisible(Control ctrl, bool bol)
        {
            try
            {

                foreach (Control c in ctrl.Controls)
                {
                    c.Visible = bol;

                    if (!bol)
                    {
                        if (c.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraTextEditor")
                            ((Infragistics.Win.UltraWinEditors.UltraTextEditor)c).Clear();
                        else if (c.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraComboEditor")
                            ((Infragistics.Win.UltraWinEditors.UltraComboEditor)c).Value = "";
                        else if (c.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor")
                            ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)c).Value = DateTime.Now;
                        else if (c.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraCheckEditor")
                            ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Checked = false;
                        else if (c.GetType().ToString() == "Infragistics.Win.UltraWinGrid.UltraGrid")
                        {
                            while (((Infragistics.Win.UltraWinGrid.UltraGrid)c).Rows.Count > 0)
                            {
                                ((Infragistics.Win.UltraWinGrid.UltraGrid)c).Rows[0].Delete(false);
                            }
                        }
                    }

                }

                ctrl.Visible = bol;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #region 치공구 구성품

        /// <summary>
        /// 해당 설비의 치공구BOM정보
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        private string SearchDurableBOM(string strPlantCode, string strEquipCode)
        {
            string strRtn = "";
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                DataTable dtDurable = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));



                if (dtDurable.Rows.Count == 0)
                {
                    this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                }
                else
                {
                    this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["CurDurableMatName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["InputQty"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                }

                //콤보 그리드 리스트 클러어
                this.uGridChangeDurable.DisplayLayout.Bands[0].Columns["CurDurableMatName"].Layout.ValueLists.Clear();
                //콤보그리드 컬럼설정
                string strValue = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                string strText = "구성품코드,구성품명,LotNo,수량,단위";

                //--그리드에 추가 --
                WinGrid wGrid = new WinGrid();
                wGrid.mfSetGridColumnValueGridList(this.uGridChangeDurable, 0, "CurDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText, strValue, strText
                    , "DurableMatCode", "DurableMatName", dtDurable);


                return strRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtn;
            }
            finally
            { }

        }

        /// <summary>
        /// 교체구성품그리드콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInven">창고정보</param>
        /// <param name="e">Infragistics.Win.UltraWinGrid.CellEventArgs</param>
        private void ChgDurbleMat(string strPlantCode, string strInven, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult result;
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                brwChannel.mfCredentials(clsRepairReq);

                DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }

                //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                string strLotChk = "";

                if (!this.uGridChangeDurable.Rows[intIndex].Cells["CurDurableMatCode"].Value.ToString().Equals(string.Empty))
                    strLotChk = this.uGridChangeDurable.Rows[intIndex].Cells["CurLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                //금형치공구 현재고 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                //금형치공구창고에 해당하는 금형치공구 재고 조회
                DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(strPlantCode, strInven,
                                                                                dtEquipPackModel.Rows[0]["PackageB"].ToString(), dtEquipPackModel.Rows[0]["ModelName"].ToString(),
                                                                                strLotChk, m_resSys.GetString("SYS_LANG"));

                //금형치공구정보에 검색조건 적용
                WinGrid wGrid = new WinGrid();

                //                this.uGridRepair.Rows[intIndex].Cells["ChgDurableMatName"].Column.Layout.ValueLists.Clear();
                string strDropDownValue = "DurableMatCode,DurableMatName,LotNo,Qty,UnitCode,UnitName";
                string strDropDownText = "치공구코드,치공구,LOTNO,수량,단위코드,단위";

                //콤보그리드 초기화.
                //e.Cell.Column.Band.Layout.ValueLists.Clear();

                wGrid.mfSetGridCellValueGridList(this.uGridChangeDurable, 0, intIndex, "ChgDurableMatName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                   , strDropDownValue, strDropDownText, "DurableMatCode", "DurableMatName", dtStock);

                if (dtStock.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion



        #endregion

    }
}
