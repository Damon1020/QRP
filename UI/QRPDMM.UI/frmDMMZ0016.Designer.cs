﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0016));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateToChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFromChgDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboReturnDurableInventoryCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelReturnDurableInventoryCode = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableTransferD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextReturnChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReturnChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReturnDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromChgDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnDurableInventoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToChgDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromChgDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uDateToChgDate
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToChgDate.Appearance = appearance4;
            this.uDateToChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateToChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateToChgDate.Location = new System.Drawing.Point(500, 12);
            this.uDateToChgDate.Name = "uDateToChgDate";
            this.uDateToChgDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToChgDate.TabIndex = 5;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(480, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(20, 16);
            this.ultraLabel1.TabIndex = 4;
            this.ultraLabel1.Text = "~";
            // 
            // uDateFromChgDate
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromChgDate.Appearance = appearance21;
            this.uDateFromChgDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFromChgDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFromChgDate.Location = new System.Drawing.Point(380, 12);
            this.uDateFromChgDate.Name = "uDateFromChgDate";
            this.uDateFromChgDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromChgDate.TabIndex = 3;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(276, 12);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(100, 20);
            this.uLabel2.TabIndex = 2;
            this.uLabel2.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uComboReturnDurableInventoryCode);
            this.uGroupBox1.Controls.Add(this.uLabelReturnDurableInventoryCode);
            this.uGroupBox1.Controls.Add(this.uGridDurableTransferD);
            this.uGroupBox1.Controls.Add(this.uTextReturnChargeName);
            this.uGroupBox1.Controls.Add(this.uTextReturnChargeID);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uDateReturnDate);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBox1.TabIndex = 3;
            // 
            // uComboReturnDurableInventoryCode
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboReturnDurableInventoryCode.Appearance = appearance5;
            this.uComboReturnDurableInventoryCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboReturnDurableInventoryCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboReturnDurableInventoryCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboReturnDurableInventoryCode.Location = new System.Drawing.Point(660, 36);
            this.uComboReturnDurableInventoryCode.Name = "uComboReturnDurableInventoryCode";
            this.uComboReturnDurableInventoryCode.Size = new System.Drawing.Size(120, 19);
            this.uComboReturnDurableInventoryCode.TabIndex = 42;
            this.uComboReturnDurableInventoryCode.Text = "ultraComboEditor1";
            // 
            // uLabelReturnDurableInventoryCode
            // 
            this.uLabelReturnDurableInventoryCode.Location = new System.Drawing.Point(556, 36);
            this.uLabelReturnDurableInventoryCode.Name = "uLabelReturnDurableInventoryCode";
            this.uLabelReturnDurableInventoryCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelReturnDurableInventoryCode.TabIndex = 41;
            this.uLabelReturnDurableInventoryCode.Text = "ultraLabel1";
            // 
            // uGridDurableTransferD
            // 
            this.uGridDurableTransferD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableTransferD.DisplayLayout.Appearance = appearance9;
            this.uGridDurableTransferD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableTransferD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.Appearance = appearance10;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance11;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance12.BackColor2 = System.Drawing.SystemColors.Control;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableTransferD.DisplayLayout.GroupByBox.PromptAppearance = appearance12;
            this.uGridDurableTransferD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableTransferD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableTransferD.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableTransferD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableTransferD.DisplayLayout.Override.CellAppearance = appearance16;
            this.uGridDurableTransferD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableTransferD.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableTransferD.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableTransferD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableTransferD.DisplayLayout.Override.RowAppearance = appearance19;
            this.uGridDurableTransferD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableTransferD.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.uGridDurableTransferD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableTransferD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableTransferD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableTransferD.Location = new System.Drawing.Point(12, 40);
            this.uGridDurableTransferD.Name = "uGridDurableTransferD";
            this.uGridDurableTransferD.Size = new System.Drawing.Size(1050, 712);
            this.uGridDurableTransferD.TabIndex = 8;
            this.uGridDurableTransferD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableTransferD_AfterCellUpdate);
            // 
            // uTextReturnChargeName
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnChargeName.Appearance = appearance6;
            this.uTextReturnChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnChargeName.Location = new System.Drawing.Point(428, 36);
            this.uTextReturnChargeName.Name = "uTextReturnChargeName";
            this.uTextReturnChargeName.ReadOnly = true;
            this.uTextReturnChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextReturnChargeName.TabIndex = 7;
            // 
            // uTextReturnChargeID
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReturnChargeID.Appearance = appearance8;
            this.uTextReturnChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance7.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance7.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance7;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReturnChargeID.ButtonsRight.Add(editorButton1);
            this.uTextReturnChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReturnChargeID.Location = new System.Drawing.Point(328, 36);
            this.uTextReturnChargeID.Name = "uTextReturnChargeID";
            this.uTextReturnChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextReturnChargeID.TabIndex = 6;
            this.uTextReturnChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReturnChargeID_KeyDown);
            this.uTextReturnChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReturnChargeID_EditorButtonClick);
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(224, 36);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 5;
            this.uLabel4.Text = "ultraLabel2";
            // 
            // uDateReturnDate
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.Appearance = appearance3;
            this.uDateReturnDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReturnDate.Location = new System.Drawing.Point(116, 36);
            this.uDateReturnDate.Name = "uDateReturnDate";
            this.uDateReturnDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReturnDate.TabIndex = 4;
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 36);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(100, 20);
            this.uLabel3.TabIndex = 3;
            this.uLabel3.Text = "ultraLabel1";
            // 
            // frmDMMZ0016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0016";
            this.Load += new System.EventHandler(this.frmDMMZ0016_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0016_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0016_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromChgDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnDurableInventoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableTransferD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToChgDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromChgDate;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReturnDate;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableTransferD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboReturnDurableInventoryCode;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnDurableInventoryCode;
    }
}