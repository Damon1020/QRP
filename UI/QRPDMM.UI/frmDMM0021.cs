﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMM0021.cs                                         */
/* 프로그램명   : 치공구출고등록                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using Microsoft.VisualBasic;

namespace QRPDMM.UI
{
    public partial class frmDMM0021 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMM0021()
        {
            InitializeComponent();
        }

        private void frmDMM0021_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0021_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitCombo();
            InitButton();

            this.uGroupBoxExpand.Expanded = false;
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMM0021_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        } 

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("치공구출고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                this.uTextTransferChargeID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextTransferChargeName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextEtcDesc.Text = "";
                this.uTextTransferCode.Text = "";

                // 출고일 : 시스템의 현재날자
                this.uDateTransferDate.Value = DateTime.Now;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchTransferDate, "출고일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                lbl.mfSetLabel(this.uLabelPlant1, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferCode, "출고번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferDate, "출고일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelTransferChargeID, "출고담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel6, "특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "출고내역", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region Header
                //기본설정
                //1
                grd.mfInitGeneralGrid(this.uGridDurableTransferH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //1
                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "TransferCode", "출고문서번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "TransferDate", "출고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "TransferChargeID", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "TransferChargeName", "출고담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDurableTransferH, 0, "StockFlag", "재고반영여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridDurableTransferH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDurableTransferH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGridDurableTransferH, 0);
                #endregion

                #region Detail
                //2
                WinGrid grd2 = new WinGrid();
                grd2.mfInitGeneralGrid(this.uGridDurableTransferD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FOTNNAME")); 
                //2
                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "PlantCode", "PlantCode", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferCode", "TransferCode", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, true, 20, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferSeq", "TransferSeq", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, true, 0, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");
                
                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgEquipCode", "교체설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgEquipName", "교체설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 50, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, true, false, 20, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 50, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgDurableMatCode", "교체 치공구", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgLotNo", "교체 LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 40, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "InputQty", "구성품수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableInventoryCode", "출고창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferDurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferLotNo", "LOTNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 40, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnn", "0");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferQty", "출고량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "TransferEtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgFlag", "교체여부플래그", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ChgDate", "교체일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 15, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd2.mfSetGridColumn(this.uGridDurableTransferD, 0, "ModelName", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, true, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGridDurableTransferD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDurableTransferD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDurableTransferD.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //한줄생성
                grd2.mfAddRowGrid(this.uGridDurableTransferD, 0);
                #endregion

                //채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                grd.mfSetGridColumnValueList(this.uGridDurableTransferD, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
           
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();


                this.uGroupBoxExpand.Expanded = false;

                if (this.uDateFromTransferDate.Value == null || this.uDateFromTransferDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000215", "M000209", Infragistics.Win.HAlign.Right);
                    this.uDateFromTransferDate.DropDown();
                    return;
                }
                if (this.uDateToTransferDate.Value == null || this.uDateToTransferDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000215", "M000218", Infragistics.Win.HAlign.Right);

                    this.uDateToTransferDate.DropDown();
                    return;
                }

                if (this.uDateFromTransferDate.DateTime.Date > this.uDateToTransferDate.DateTime.Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000215", "M000211", Infragistics.Win.HAlign.Right);

                    this.uDateFromTransferDate.DropDown();
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferH), "DurableTransferH");
                QRPDMM.BL.DMMICP.DurableTransferH clsDurableTransferH = new QRPDMM.BL.DMMICP.DurableTransferH();
                brwChannel.mfCredentials(clsDurableTransferH);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromTransferDate = this.uDateFromTransferDate.Value.ToString();
                string strToTransferDate = this.uDateToTransferDate.Value.ToString();

                // Call Method
                DataTable dtDurableTransferH = clsDurableTransferH.mfReadDMMDurableTransferH(strPlantCode, strFromTransferDate, strToTransferDate, "", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridDurableTransferH.DataSource = dtDurableTransferH;
                uGridDurableTransferH.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtDurableTransferH.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            
            try
            {
                string strStockFlag = "F";
                int intInputQty = 0;
                int intTransferQty = 0;
                int intQty = 0;
                int intRowCheck = 0;

                
                #region 필수 확인
                if (uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Right);
                    uComboPlant.Focus();
                    return;
                }
                if (uDateFromTransferDate.Value == null || uDateFromTransferDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001171", Infragistics.Win.HAlign.Right);
                    uDateFromTransferDate.Focus();
                    return;
                }
                //uTextTransferChargeID
                if (uTextTransferChargeID.Text.ToString().Equals(string.Empty) ||
                    uTextTransferChargeName.Text.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001165", Infragistics.Win.HAlign.Right);
                    uTextTransferChargeID.Focus();
                    return;
                }

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                {
                    if (uGridDurableTransferD.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        intRowCheck = intRowCheck + 1;
                        if (uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000301", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["ProductCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001087", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["ProductCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000298", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000316", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["InputQty"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001163", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001172", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString().Equals(string.Empty) ||
                            uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString().Equals("0"))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001166", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000362", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        intInputQty = Convert.ToInt32(uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString());
                        intTransferQty = Convert.ToInt32(uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString());
                        intQty = Convert.ToInt32(uGridDurableTransferD.Rows[i].Cells["Qty"].Value);

                        if (intTransferQty > intQty && this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error,500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001167", Infragistics.Win.HAlign.Right);
                            // Focus Cell
                            this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"];
                            this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;

                        }


                        //if (intTransferQty > intInputQty)
                        //{
                        //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                , "확인창", "필수입력사항 확인", "출고량이 구성품수량보다 클수는 없습니다. 확인 후 다시 입력하세요", Infragistics.Win.HAlign.Right);
                        //    // Focus Cell
                        //    this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"];
                        //    this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        //    return;

                        //}

                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001240", Infragistics.Win.HAlign.Right);
                    // Focus Cell
                    
                    return;

                }
                
                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M001053", "M000936",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

      

                #region Create Data
                
                DataRow drData;

                //////    //--------- 1. EQUDurableTransferH 테이블에 저장 ----------------------------------------------//
                #region Header
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferH), "DurableTransferH");
                QRPDMM.BL.DMMICP.DurableTransferH clsDurableTransferH = new QRPDMM.BL.DMMICP.DurableTransferH();
                brwChannel.mfCredentials(clsDurableTransferH);

                DataTable dtTransferH = clsDurableTransferH.mfSetDatainfo();
                drData = dtTransferH.NewRow();

                drData["PlantCode"] = uComboPlant.Value.ToString();
                drData["TransferCode"] = uTextTransferCode.Text;
                drData["TransferDate"] = Convert.ToDateTime(uDateFromTransferDate.Value.ToString()).ToString("yyyy-MM-dd");
                drData["TransferChargeID"] = uTextTransferChargeID.Text;
                drData["EtcDesc"] = uTextEtcDesc.Text;

                
                dtTransferH.Rows.Add(drData);
                #endregion

                //////    //-------- 2.  EQUDurableTransferD 테이블에 저장 ----------------------------------------------//
                #region Detail
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferD), "DurableTransferD");
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new QRPDMM.BL.DMMICP.DurableTransferD();
                brwChannel.mfCredentials(clsDurableTransferD);

                DataTable dtTransferD = clsDurableTransferD.mfSetDatainfo();

                for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                {
                    if (uGridDurableTransferD.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        drData = dtTransferD.NewRow();
                        drData["PlantCode"] = uComboPlant.Value.ToString();
                        drData["TransferCode"] = uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString() == string.Empty ? uTextTransferCode.Text : uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString();
                        drData["TransferSeq"] = uGridDurableTransferD.Rows[i].Cells["TransferSeq"].Value.ToString() == string.Empty ? "0" : uGridDurableTransferD.Rows[i].Cells["TransferSeq"].Value;
                        drData["TransferDurableInventoryCode"] = uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString();
                        drData["ProductCode"] = uGridDurableTransferD.Rows[i].Cells["ProductCode"].Value.ToString();
                        drData["TransferDurableMatCode"] = uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
                        drData["TransferLotNo"] = uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
                        drData["TransferQty"] = uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
                        drData["ChgEquipCode"] = uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
                        drData["ChgDurableMatCode"] = uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
                        drData["ChgLotNo"] = uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();
                        drData["InputQty"] = uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString();
                        drData["UnitCode"] = uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
                        drData["TransferEtcDesc"] = uGridDurableTransferD.Rows[i].Cells["TransferEtcDesc"].Value.ToString();
                        drData["ChgFlag"] = "";
                        drData["ChgDate"] = "";
                        drData["ChgChargeID"] = "";
                        drData["ChgEtcDesc"] = "";
                        drData["ReturnFlag"] = "";
                        drData["ReturnDate"] = "";
                        drData["ReturnChargeID"] = "";
                        drData["ReturnDurableInventoryCode"] = "";
                        drData["ReturnEtcDesc"] = "";
                        if (!uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString().Equals(string.Empty) &&
                            uGridDurableTransferD.Rows[i].Hidden == true)
                        {
                            drData["DeleteFlag"] = "T";
                        }

                        if (uGridDurableTransferD.Rows[i].Hidden == false ||
                            (uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString().Equals(string.Empty) && uGridDurableTransferD.Rows[i].Hidden == false) ||
                            (uGridDurableTransferD.Rows[i].Hidden == true && !uGridDurableTransferD.Rows[i].Cells["TransferCode"].Value.ToString().Equals(string.Empty)))
                        {
                            dtTransferD.Rows.Add(drData);
                        }
                    }
                }
                #endregion

                #region StandBy
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableChgStandby), "DurableChgStandby");
                QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new QRPDMM.BL.DMMICP.DurableChgStandby();
                brwChannel.mfCredentials(clsDurableChgStandby);

                DataTable dtStandBy = clsDurableChgStandby.mfSetDatainfo();

                for(int i=0;i<dtTransferD.Rows.Count;i++)
                {
                    drData = dtStandBy.NewRow();
                    drData["PlantCode"] = dtTransferD.Rows[i]["PlantCode"].ToString();
                    drData["DurableMatCode"] = dtTransferD.Rows[i]["TransferDurableMatCode"].ToString();
                    drData["LotNo"] = dtTransferD.Rows[i]["TransferLotNo"].ToString();
                    drData["UnitCode"] = dtTransferD.Rows[i]["UnitCode"].ToString();

                    if(dtTransferD.Rows[i]["DeleteFlag"].ToString().Equals("T"))
                    {
                        drData["ChgQty"] = (Convert.ToInt32(dtTransferD.Rows[i]["TransferQty"]) * -1).ToString();
                    }
                    else
                    {
                        drData["ChgQty"] = dtTransferD.Rows[i]["TransferQty"].ToString();
                    }
                    dtStandBy.Rows.Add(drData);
                }
                #endregion

                #region History
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                DataTable dtHist = clsDurableStockMoveHist.mfSetDatainfo();

                for(int i=0;i<dtTransferD.Rows.Count;i++)
                {
                    drData = dtHist.NewRow();
                    
                    drData["DocCode"] = "";
                    drData["MoveDate"] = dtTransferH.Rows[0]["TransferDate"].ToString();
                    drData["MoveChargeID"] = uTextTransferChargeID.Text;
                    drData["DurableInventoryCode"] = dtTransferD.Rows[i]["TransferDurableInventoryCode"].ToString();
                    drData["PlantCode"] = dtTransferD.Rows[i]["PlantCode"].ToString();
                    //drData["ProductCode"] = dtTransferD.Rows[i]["ProductCode"].ToString();
                    drData["ProductCode"] = "";
                    drData["EquipCode"] = dtTransferD.Rows[i]["ChgEquipCode"].ToString();
                    drData["DurableMatCode"] = dtTransferD.Rows[i]["TransferDurableMatCode"].ToString();
                    drData["LotNo"] = dtTransferD.Rows[i]["TransferLotNo"].ToString();
                    drData["UnitCode"] = dtTransferD.Rows[i]["UnitCode"].ToString();

                    if(dtTransferD.Rows[i]["DeleteFlag"].ToString().Equals("T"))
                    {
                        drData["MoveGubunCode"] = "M04";
                        drData["MoveQty"] = dtTransferD.Rows[i]["TransferQty"].ToString();
                    }
                    else
                    {
                        drData["MoveGubunCode"] = "M03";
                        drData["MoveQty"] = (Convert.ToInt32(dtTransferD.Rows[i]["TransferQty"]) * -1).ToString();
                    }
                    dtHist.Rows.Add(drData);
                }
                #endregion

                #region Stock
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                DataTable dtStock = clsDurableStock.mfSetDatainfo();

                for(int i=0;i<dtTransferD.Rows.Count;i++)
                {
                    drData = dtStock.NewRow();
                    drData["PlantCode"] = dtTransferD.Rows[i]["PlantCode"].ToString();
                    drData["DurableInventoryCode"] = dtTransferD.Rows[i]["TransferDurableInventoryCode"].ToString();
                    drData["DurableMatCode"] = dtTransferD.Rows[i]["TransferDurableMatCode"].ToString();
                    drData["LotNo"] = dtTransferD.Rows[i]["TransferLotNo"].ToString();
                    drData["UnitCode"] = dtTransferD.Rows[i]["UnitCode"].ToString();

                    if(dtTransferD.Rows[i]["DeleteFlag"].ToString().Equals("T"))
                    {
                        drData["Qty"] = dtTransferD.Rows[i]["TransferQty"].ToString();
                    }
                    else
                    {
                        drData["Qty"] = (Convert.ToInt32(dtTransferD.Rows[i]["TransferQty"]) * -1).ToString();
                    }
                    dtStock.Rows.Add(drData);
                }
                #endregion

                #endregion

                #region BL
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strRtn = clsSAVEDurableStock.mfSaveTransfer(dtTransferH, dtTransferD, dtStandBy, dtStock,
                                                    dtHist, "T", m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                if (uGroupBoxExpand.Expanded == false)
                {

                    while (this.uGridDurableTransferD.Rows.Count > 0)
                    {
                        this.uGridDurableTransferD.Rows[0].Delete(false);
                    }
                    uGroupBoxExpand.Expanded = true;
                }
                else
                {
                    WinGrid grd = new WinGrid();

                    while (this.uGridDurableTransferD.Rows.Count > 0)
                    {
                        this.uGridDurableTransferD.Rows[0].Delete(false);
                    }
                    //while (this.uGridDurableTransferH.Rows.Count > 0)
                    //{
                    //    //this.uGridDurableTransferH.Rows[0].Delete(false);
                    //}

                }

                InitText();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
                
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinGrid grd = new WinGrid();

                if (this.uGridDurableTransferH.Rows.Count == 0 && (this.uGridDurableTransferD.Rows.Count == 0 || this.uGroupBoxExpand.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uGridDurableTransferH.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridDurableTransferH);
                
                if (this.uGridDurableTransferD.Rows.Count > 0 && this.uGroupBoxExpand.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridDurableTransferD);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                mfCreate();
                string strPlantCode = uComboPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    GridComboList(strPlantCode, 0);
                    GridCombo(strPlantCode);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Grid Cell Update Event
       
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxExpand.Expanded == false)
                {
                    Point point = new Point(0, 125);
                    uGroupBoxExpand.Location = point;
                    this.uGridDurableTransferH.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    uGroupBoxExpand.Location = point;
                    this.uGridDurableTransferH.Height = 760;
                    for (int i = 0; i < this.uGridDurableTransferH.Rows.Count; i++)
                    {
                        this.uGridDurableTransferH.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region GridEvent
        private void uGridDurableTransferD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableTransferD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                if (e.Cell.Column.Key.Equals("ProductCode"))
                {
                    if (e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Row.Cells["ProductName"].Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["ProductName"].Value = string.Empty;
                        e.Cell.Row.Cells["Package"].Value = string.Empty;
                    }
                }

                if (e.Cell.Column.Key.ToString().Equals("InputQty"))
                {
                    if (!Information.IsNumeric(e.Cell.Value))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001264", "M000882", "M000758", Infragistics.Win.HAlign.Right);
                        for (int i = 0; i < e.Cell.Value.ToString().Length; i++)
                        {
                            if (!Information.IsNumeric(e.Cell.Value.ToString().Substring(i, 1)))
                            {
                                e.Cell.Value = e.Cell.Value.ToString().Remove(i, 1);
                            }
                        }
                        uGridDurableTransferD.ActiveCell = e.Cell;
                        //uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;                        
                    }

                    if (Convert.ToInt32(e.Cell.Value) < 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001264", "M000882", "M000823", Infragistics.Win.HAlign.Right);
                        //uGridDurableTransferD.ActiveCell = e.Cell;
                        uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;   
                    }

                    if (Convert.ToInt32(e.Cell.Tag) < Convert.ToInt32(e.Cell.Value))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M000962", "M000157",
                                                     Infragistics.Win.HAlign.Right);
                        e.Cell.Value = e.Cell.Tag;
                        return;
                    }
                }

                if (e.Cell.Column.Key.ToString().Equals("TransferQty"))
                {
                    if (!Information.IsNumeric(e.Cell.Value))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001264", "M000882", "M000758", Infragistics.Win.HAlign.Right);
                        for (int i = 0; i < e.Cell.Value.ToString().Length; i++)
                        {
                            if (!Information.IsNumeric(e.Cell.Value.ToString().Substring(i, 1)))
                            {
                                e.Cell.Value = e.Cell.Value.ToString().Remove(i, 1);
                            }
                        }
                        uGridDurableTransferD.ActiveCell = e.Cell;
                        //uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }

                    if (Convert.ToInt32(e.Cell.Value) < 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001264", "M000882", "M000823", Infragistics.Win.HAlign.Right);
                        //uGridDurableTransferD.ActiveCell = e.Cell;
                        uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }

                    if (Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value) < Convert.ToInt32(e.Cell.Value))
                    {
                         msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M000962", "M001005",
                                                     Infragistics.Win.HAlign.Right);
                        e.Cell.Value = 0;
                        return;
                    }

                    if (Convert.ToInt32(e.Cell.Row.Cells["InputQty"].Tag) < Convert.ToInt32(e.Cell.Value))
                    {
                         msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M000962", "M000157",
                                                     Infragistics.Win.HAlign.Right);
                        e.Cell.Value = 0;
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableTransferD_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //this.titleArea.Focus();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                
                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                string strPlantCode = uComboPlant.Value.ToString();

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                #region 교체할 설비 선택
                if (e.Cell.Column.Key.ToString().Equals("ChgEquipCode"))
                {
                    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;

                    if (uGrid.ActiveCell.Row.Cells["ChgEquipCode"].Value == null)
                        return;

                    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChnnel.mfCredentials(clsEquip);

                    
                    // 교체설비명 가져오기//
                    string strChgEquipCode = uGrid.ActiveCell.Row.Cells["ChgEquipCode"].Text;

                    e.Cell.Row.Cells["ChgEquipName"].Value = "";
                    e.Cell.Row.Cells["ProductCode"].Value = "";
                    e.Cell.Row.Cells["ProductName"].Value = "";
                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = "";
                    e.Cell.Row.Cells["ChgLotNo"].Value = "";
                    e.Cell.Row.Cells["InputQty"].Value = 0;
                    e.Cell.Row.Cells["TransferDurableInventoryCode"].Value = "";
                    e.Cell.Row.Cells["TransferDurableMatCode"].Value = "";
                    e.Cell.Row.Cells["TransferDurableMatName"].Value = "";

                    e.Cell.Row.Cells["TransferLotNO"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = 0;
                    e.Cell.Row.Cells["TransferQty"].Value = 0;
                    e.Cell.Row.Cells["UnitCode"].Value = "EA";
                    e.Cell.Row.Cells["TransferEtcDesc"].Value = "";
                    e.Cell.Row.Cells["ChgFlag"].Value = "F";
                    e.Cell.Row.Cells["ChgDate"].Value = "";
                    e.Cell.Row.Cells["ModelName"].Value = "";
                    e.Cell.Row.Cells["Package"].Value = string.Empty;

                    if (strChgEquipCode == "")
                        return;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquip.Rows.Count > 0)
                    {
                        uGrid.ActiveCell.Row.Cells["ChgEquipName"].Value = dtEquip.Rows[0]["EquipName"].ToString();
                        e.Cell.Row.Cells["ModelName"].Value = dtEquip.Rows[0]["ModelName"].ToString();
                    }
                    // 설비에 해당하는 DurableMatCode 코드 가져오기
                    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChnnel.mfCredentials(clsEquipDurableBOM);
                    DataTable dtEquipDurableBOM = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));

                    string strDropDownGridHKey = "DurableMatCode,DurableMatName,LotNo,InputQty,UnitName";
                    string strDropDownGridHName = "치공구코드,치공구명,LOTNO,교체수량,단위";
                    grd.mfSetGridCellValueGridList(uGridDurableTransferD, 0, e.Cell.Row.Index, "ChgDurableMatCode", Infragistics.Win.ValueListDisplayStyle.DataValue,
                        strDropDownGridHKey, strDropDownGridHName, "DurableMatCode", "DurableMatName", dtEquipDurableBOM);



                }
                //if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatCode"))
                //{
                //    //this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];
                    
                //    string strChgEquipCode = e.Cell.Row.Cells["ChgEquipCode"].Value.ToString();
                //    string strChgDurableMatCode = e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString(); //(string)e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex);

                //    if (strChgDurableMatCode == "")
                //        return;

                //    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                //    QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                //    brwChnnel.mfCredentials(clsEquipDurableBOM);
                //    DataTable dtEquipDurableBOM = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));                    

                //    DataRow[] arDr = dtEquipDurableBOM.Select("DurableMatCode = '" + strChgDurableMatCode + "'");

                //    uGridDurableTransferD.ActiveCell.Row.Cells["InputQty"].Value = arDr[0]["InputQty"].ToString();
                //    uGridDurableTransferD.ActiveCell.Row.Cells["ChgLotNo"].Value = arDr[0]["LotNo"].ToString();

                //    if (!uGridDurableTransferD.ActiveCell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty))
                //    {
                //        e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                //    }
                //}
                #endregion

                //#region TransferInventory
                //if (e.Cell.Column.Key.ToString().Equals("TransferDurableInventoryCode"))
                //{
                //    string strInventoryCode = e.Cell.Row.Cells["TransferDurableInventoryCode"].Value.ToString();

                //    brwChnnel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                //    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                //    brwChnnel.mfCredentials(clsDurableStock);

                //    DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                //    if (e.Cell.Row.Cells["ChgLotNo"].Value.ToString() == "")
                //        dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));
                //    else
                //        dtStock = clsDurableStock.mfReadDurableStock_SPCombo_OnlyLot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));


                //    string strDropDownGridHKey = "DurableMatCode,DurableMatName,LotNo,Qty"; //,UnitCode,UnitName";
                //    string strDropDownGridHName = "치공구코드,치공구명,LOTNO,교체수량";
                //    grd.mfSetGridCellValueGridList(uGridDurableTransferD, 0, e.Cell.Row.Index, "TransferDurableMatCode", Infragistics.Win.ValueListDisplayStyle.DataValue,
                //        strDropDownGridHKey, strDropDownGridHName, "DurableMatName", "DurableMatCode", dtStock);
                //}

                //if (e.Cell.Column.Key.ToString().Equals("TransferDurableMatCode"))
                //{
                //    //this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];

                //    string strInventoryCode = e.Cell.Row.Cells["TransferDurableInventoryCode"].Value.ToString();
                //    string strDurableMatCode = e.Cell.Row.Cells["TransferDurableMatCode"].Text.ToString(); //(string)e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex);

                //    if (strDurableMatCode == "")
                //        return;

                //    brwChnnel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                //    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                //    brwChnnel.mfCredentials(clsDurableStock);

                //    DataTable dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                //    DataRow[] arDr = dtStock.Select("DurableMatCode = '" + strDurableMatCode + "'");

                //    uGridDurableTransferD.ActiveCell.Row.Cells["TransferDurableMatName"].Value = arDr[0]["DurableMatName"].ToString();
                //    uGridDurableTransferD.ActiveCell.Row.Cells["Qty"].Value = arDr[0]["Qty"].ToString();
                //    uGridDurableTransferD.ActiveCell.Row.Cells["TransferLotNO"].Value = arDr[0]["LotNo"].ToString();
                //    uGridDurableTransferD.ActiveCell.Row.Cells["UnitCode"].Value = arDr[0]["UnitCode"].ToString();

                //    if (!uGridDurableTransferD.ActiveCell.Row.Cells["TransferLotNO"].Value.ToString().Equals(string.Empty))
                //    {
                //        e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                //        e.Cell.Row.Cells["TransferQty"].Value = arDr[0]["Qty"].ToString();
                //    }
                //}
                //#endregion
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableTransferH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                // ExtandableGroupBox 설정
                if (this.uGroupBoxExpand.Expanded == false)
                {
                    this.uGroupBoxExpand.Expanded = true;
                }

                e.Cell.Row.Fixed = true;

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strTransferCode = e.Cell.Row.Cells["TransferCode"].Value.ToString();


                QRPBrowser brwChannel = new QRPBrowser();

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferH), "DurableTransferH");
                QRPDMM.BL.DMMICP.DurableTransferH clsDurableTransferH = new QRPDMM.BL.DMMICP.DurableTransferH();
                brwChannel.mfCredentials(clsDurableTransferH);

                // Call Method
                DataTable dtDurableTransferH = clsDurableTransferH.mfReadDMMDurableTransferH_Detail(strPlantCode, strTransferCode, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtDurableTransferH.Rows.Count; i++)
                {
                    this.uComboPlant.Value = dtDurableTransferH.Rows[i]["PlantCode"].ToString();
                    this.uTextTransferCode.Text = dtDurableTransferH.Rows[i]["TransferCode"].ToString();
                    this.uDateTransferDate.Value = dtDurableTransferH.Rows[i]["TransferDate"].ToString();
                    this.uTextTransferChargeID.Text = dtDurableTransferH.Rows[i]["TransferChargeID"].ToString();
                    this.uTextTransferChargeName.Text = dtDurableTransferH.Rows[i]["TransferChargeName"].ToString();
                    this.uTextEtcDesc.Text = dtDurableTransferH.Rows[i]["EtcDesc"].ToString();
                }


                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferD), "DurableTransferD");
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new QRPDMM.BL.DMMICP.DurableTransferD();
                brwChannel.mfCredentials(clsDurableTransferD);

                // Call Method
                DataTable dtDurableTransferD = clsDurableTransferD.mfReadDMMDurableTransferD(strPlantCode, strTransferCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridDurableTransferD.DataSource = dtDurableTransferD;
                uGridDurableTransferD.DataBind();

                uGridDurableTransferD_AfterDataBinding();
                for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                {
                    if (uGridDurableTransferD.Rows[i].Cells["ChgFlag"].Value.ToString().Equals("T"))
                    {
                        uGridDurableTransferD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableTransferD.Rows[i].Appearance.BackColor = Color.Yellow;
                    }
                //    else if (uGridDurableTransferD.Rows[i].Cells["ChgFlag"].Value.ToString().Equals("F"))
                //    {
                //        uGridDurableTransferD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                //        uGridDurableTransferD.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                //    }
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableTransferD_AfterDataBinding()
        {
            try
            {
                for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                {
                    //if (Convert.ToBoolean(uGridDurableTransferD.Rows[i].GetCellValue("CancelFlag")))
                    //{
                    //    uGridDurableTransferD.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;
                    //    uGridDurableTransferD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //}
                    //else
                    //{
                        for (int j = 0; j < uGridDurableTransferD.Rows[i].Cells.Count; j++)
                        {
                            
                            if (uGridDurableTransferD.Rows[i].Cells[j].Column.Key.ToString() == "Check" )
                            {
                                uGridDurableTransferD.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }
                            else
                            {
                                uGridDurableTransferD.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        

                        }
                    //}

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableTransferD_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (e.Cell.Row.Cells["TransferCode"].Value.ToString() != "")
                    return;


                if (e.Cell.Column.Key.ToString().Equals("ProductCode"))
                {

                    QRPCOM.UI.frmCOM0002 frmProduct = new QRPCOM.UI.frmCOM0002();
                    frmProduct.PlantCode = this.uComboPlant.Value.ToString();
                    frmProduct.ShowDialog();

                    if (!frmProduct.PlantCode.ToString().Equals(string.Empty))
                    {
                        if (frmProduct.PlantCode.ToString().Equals(uComboPlant.Value.ToString()))
                        {
                            e.Cell.Value = frmProduct.ProductCode;
                            e.Cell.Row.Cells["ProductName"].Value = frmProduct.ProductName;
                            e.Cell.Row.Cells["Package"].Value = frmProduct.Package;
                        }
                        else
                        {

                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uGridDurableTransferD_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //int intSelectItem = Convert.ToInt32(e.Cell.ValueList.SelectedItemIndex.ToString());

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString());

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                titleArea.Focus();
                WinGrid wGrid = new WinGrid();

                string _strPlantCode = this.uComboPlant.Value.ToString();

                if (_strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                #region ChgDurableMatCode
                if (e.Cell.Column.Key.ToString().Equals("ChgDurableMatCode"))
                {
                    //this.uGridSPTransferD.ActiveCell = this.uGridSPTransferD.Rows[0].Cells[0];


                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = "";
                    e.Cell.Row.Cells["ChgLotNo"].Value = "";
                    e.Cell.Row.Cells["InputQty"].Value = 0;
                    e.Cell.Row.Cells["TransferDurableInventoryCode"].Value = "";
                    e.Cell.Row.Cells["TransferDurableMatCode"].Value = "";
                    e.Cell.Row.Cells["TransferDurableMatName"].Value = "";

                    e.Cell.Row.Cells["TransferLotNO"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = 0;
                    e.Cell.Row.Cells["TransferQty"].Value = 0;
                    e.Cell.Row.Cells["UnitCode"].Tag = string.Empty;
                    e.Cell.Row.Cells["UnitCode"].Value = "EA";
                    e.Cell.Row.Cells["TransferEtcDesc"].Value = "";
                    e.Cell.Row.Cells["ChgFlag"].Value = "F";
                    e.Cell.Row.Cells["ChgDate"].Value = "";


                    string strChgEquipCode = e.Cell.Row.Cells["ChgEquipCode"].Value.ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChannel.mfCredentials(clsEquipDurableBOM);
                    DataTable dtEquipDurableBOM = clsEquipDurableBOM.mfReadEquipDurableBOMCombo_Lot(_strPlantCode, strChgEquipCode, m_resSys.GetString("SYS_LANG"));


                    e.Cell.Row.Cells["ChgDurableMatCode"].Value = dtEquipDurableBOM.Rows[intSelectItem]["DurableMatCode"].ToString();
                    
                    e.Cell.Row.Cells["InputQty"].Tag = dtEquipDurableBOM.Rows[intSelectItem]["InputQty"];
                    e.Cell.Row.Cells["InputQty"].Value = dtEquipDurableBOM.Rows[intSelectItem]["InputQty"];
                    e.Cell.Row.Cells["ChgLotNo"].Value = dtEquipDurableBOM.Rows[intSelectItem]["LotNo"].ToString();
                    e.Cell.Row.Cells["UnitCode"].Tag = dtEquipDurableBOM.Rows[intSelectItem]["UnitCode"].ToString();

                    if (!e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty))
                    {
                        e.Cell.Row.Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }

                    for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                    {
                        if (!i.Equals(e.Cell.Row.Index) &&
                            uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString().Equals(dtEquipDurableBOM.Rows[intSelectItem]["LotNo"].ToString()) &&
                            !dtEquipDurableBOM.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            //WinMessageBox msg = new WinMessageBox();
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;


                            
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                            //uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Tag = 0;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Value = 0;

                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferLotNO"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatCode"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatName"].Value = string.Empty;
                            e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            return;
                        }

                        if (!i.Equals(e.Cell.Row.Index) &&
                            uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString().Equals(dtEquipDurableBOM.Rows[intSelectItem]["DurableMatCode"].ToString()) &&
                            uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString().Equals(dtEquipDurableBOM.Rows[intSelectItem]["EquipCode"].ToString()) &&
                            !dtEquipDurableBOM.Rows[intSelectItem]["DurableMatCode"].ToString().Equals(string.Empty) &&
                            dtEquipDurableBOM.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            //WinMessageBox msg = new WinMessageBox();
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001264", "M000854", Infragistics.Win.HAlign.Right);

                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgDurableMatCode"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgLotNo"].Value = string.Empty;



                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                            //uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Tag = 0;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Value = 0;

                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferLotNO"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatCode"].Value = string.Empty;
                            uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatName"].Value = string.Empty;
                            e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            return;
                        }
                    }
                }
                #endregion

                #region TransferInventory
                if (e.Cell.Column.Key.ToString().Equals("TransferDurableInventoryCode")) 
                {

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);
                    DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(_strPlantCode, m_resSys.GetString("SYS_LANG"));

                    string strInventoryCode = dtDurableInventory.Rows[intSelectItem]["DurableInventoryCode"].ToString();

                    //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    //QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    //brwChannel.mfCredentials(clsRepairReq);

                    //DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(_strPlantCode, e.Cell.Row.Cells["ChgEquipCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    //if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                    //{
                    //    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                      , "M001264", "M000319", "M000299", Infragistics.Win.HAlign.Right);
                    //    return;
                    //}

                    //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                    string strLotChk = "";

                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                        strLotChk = e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                    //금형치공구 현재고 정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구창고에 해당하는 금형치공구 재고 조회
                    DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(_strPlantCode, strInventoryCode, e.Cell.Row.Cells["Package"].Value.ToString(), e.Cell.Row.Cells["ModelName"].Value.ToString(),
                                                                                    strLotChk, m_resSys.GetString("SYS_LANG"));

                    string strDropDownGridHKey = "DurableMatCode,DurableMatName,LotNo,Qty,UnitName"; //,UnitCode,UnitName";
                    string strDropDownGridHName = "치공구코드,치공구명,LOTNO,교체수량,단위";
                    grd.mfSetGridCellValueGridList(uGridDurableTransferD, 0, e.Cell.Row.Index, "TransferDurableMatCode", Infragistics.Win.ValueListDisplayStyle.DataValue,
                        strDropDownGridHKey, strDropDownGridHName, "DurableMatName", "DurableMatCode", dtStock);
                }
                #endregion

                #region TransferDurableMatCode
                if (e.Cell.Column.Key.ToString().Equals("TransferDurableMatCode"))
                {

                    string _strDurableInventoryCode = e.Cell.Row.Cells["TransferDurableInventoryCode"].Value.ToString();

                    //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairReq), "RepairReq");
                    //QRPEQU.BL.EQUREP.RepairReq clsRepairReq = new QRPEQU.BL.EQUREP.RepairReq();
                    //brwChannel.mfCredentials(clsRepairReq);

                    //DataTable dtEquipPackModel = clsRepairReq.mfReadEquipRepair_MESDBInfo(_strPlantCode, e.Cell.Row.Cells["ChgEquipCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    //if (dtEquipPackModel.Rows.Count <= 0) // 해당설비에 대한 Pakcage 와 설비모델이 없으면 재고 없음처리
                    //    return;


                    //금형치공구정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구정보 Detail매서드 호출
                    DataTable dtStock = new DataTable();


                    //LotNo가 있으면 교체가 LotNo가 있는 금형치공구로 교체가가능하다. LotNo가 없으면 없는 것끼리 교체가 가능하다.
                    string strLotChk = "";

                    if (!e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                        strLotChk = e.Cell.Row.Cells["ChgLotNo"].Value.ToString().Equals(string.Empty) ? "F" : "T";

                    dtStock = clsDurableStock.mfReadDMMDurableStock_Combo(_strPlantCode, _strDurableInventoryCode,
                                                                            e.Cell.Row.Cells["Package"].Value.ToString(), e.Cell.Row.Cells["ModelName"].Value.ToString(),
                                                                            strLotChk, m_resSys.GetString("SYS_LANG"));


                    if (!dtStock.Rows.Count.Equals(0))
                    {
                        if (!e.Cell.Row.Cells["UnitCode"].Tag.Equals(dtStock.Rows[intSelectItem]["UnitCode"]))
                        {
                            //단위가 틀릴경우 메세지
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000360", "M000306", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = string.Empty;
                            e.Cell.Row.Cells["UnitCode"].Value = e.Cell.Row.Cells["UnitCode"].Tag;
                            e.Cell.Row.Cells["TransferDurableMatName"].Value = string.Empty;
                            e.Cell.Row.Cells["Qty"].Tag = 0;
                            e.Cell.Row.Cells["Qty"].Value = 0;
                            e.Cell.Row.Cells["TransferQty"].Tag = 0;
                            e.Cell.Row.Cells["TransferQty"].Value = 0;
                            return;

                        }

                        e.Cell.Row.Cells["TransferLotNO"].Value = dtStock.Rows[intSelectItem]["LotNo"].ToString();
                        e.Cell.Row.Cells["Qty"].Tag = dtStock.Rows[intSelectItem]["Qty"];
                        e.Cell.Row.Cells["Qty"].Value = dtStock.Rows[intSelectItem]["Qty"];
                        e.Cell.Row.Cells["UnitCode"].Value = dtStock.Rows[intSelectItem]["UnitCode"].ToString();
                        e.Cell.Row.Cells["TransferDurableMatCode"].Value = dtStock.Rows[intSelectItem]["DurableMatCode"].ToString();
                        e.Cell.Row.Cells["TransferDurableMatName"].Value = dtStock.Rows[intSelectItem]["DurableMatName"].ToString();

                        if (!dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["TransferQty"].Tag = dtStock.Rows[intSelectItem]["Qty"];
                            e.Cell.Row.Cells["TransferQty"].Value = dtStock.Rows[intSelectItem]["Qty"];

                            e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }




                        for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index) &&
                                uGridDurableTransferD.Rows[i].Cells["TransferLotNO"].Value.ToString().Equals(dtStock.Rows[intSelectItem]["LotNo"].ToString()) &&
                                !dtStock.Rows[intSelectItem]["LotNo"].ToString().Equals(string.Empty))
                            {
                                //WinMessageBox msg = new WinMessageBox();
                                msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000853", Infragistics.Win.HAlign.Right);

                                //uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["CurQty"].Tag = string.Empty;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Tag = 0;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["InputQty"].Value = 0;
                                //uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["ChgQty"].Tag = string.Empty;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Tag = 0;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferQty"].Value = 0;

                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferLotNO"].Value = string.Empty;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatCode"].Value = string.Empty;
                                uGridDurableTransferD.Rows[e.Cell.Row.Index].Cells["TransferDurableMatName"].Value = string.Empty;
                                e.Cell.Row.Cells["TransferQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                return;
                            }
                        }
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        private void uGridDurableTransferD_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //출고창고 선택전 교체 치공구 입력여부 확인
                if (e.Cell.Column.Key.Equals("TransferDurableInventoryCode"))
                {
                    if (e.Cell.Row.Cells["ChgDurableMatCode"].Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M001180", "M000305", Infragistics.Win.HAlign.Right);
                        
                        e.Cancel = true;
                        this.uGridDurableTransferD.ActiveCell = e.Cell.Row.Cells["ChgDurableMatCode"];
                        this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Text

        private void uTextTransferChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextTransferChargeID.Text;
                    WinMessageBox msg = new WinMessageBox();

                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            this.uTextTransferChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            this.uTextTransferChargeName.Text = "";
                            this.uTextTransferChargeID.Text = "";
                        }
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextTransferChargeID.Text = "";
                    this.uTextTransferChargeName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTransferChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning,  500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                

                QRPCOM.UI.frmCOM0011 frmUser = new QRPCOM.UI.frmCOM0011();
                frmUser.PlantCode = uComboPlant.Value.ToString().Trim();
                frmUser.ShowDialog();

                if (!frmUser.UserID.ToString().Equals(string.Empty) &&
                    !frmUser.UserName.ToString().Equals(string.Empty))
                {
                    uTextTransferChargeID.Text = frmUser.UserID;
                    uTextTransferChargeName.Text = frmUser.UserName;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        #endregion

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < uGridDurableTransferD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDurableTransferD.Rows[i].Cells["Check"].Value))
                    {
                        uGridDurableTransferD.Rows[i].Hidden = true;
                        //uGridDurableTransferD.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void GridComboList(string strPlantCode, int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChnnel.mfCredentials(clsEquip);

                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                //매서드호출
                DataTable dtEquipList = clsEquip.mfReadEquipListCombo(strPlantCode, "", "", "", "", m_resSys.GetString("SYS_LANG"));



                if (dtEquipList.Rows.Count != 0)
                {

                    WinGrid grd = new WinGrid();

                    string strDropDownGridHKey = "EquipCode,EquipName,SuperEquipCode,ModelName,SerialNo,EquipTypeName,EquipGroupName,VendorName";
                    string strDropDownGridHName = "설비코드,설비명,Super설비,모델,SerialNo,설비유형,설비그룹명,Vendor";
                    grd.mfSetGridColumnValueGridList(this.uGridDurableTransferD, intIndex, "ChgEquipCode", Infragistics.Win.ValueListDisplayStyle.DataValue
                        , strDropDownGridHKey, strDropDownGridHName, "EquipCode", "EquipCode", dtEquipList);

                }
                else
                {
                    ///* 검색결과 Record수 = 0이면 메시지 띄움 */
                    //System.Windows.Forms.DialogResult result;
                    //result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                            "처리결과", "처리결과", "검색처리결과가 없습니다.", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void GridCombo(string strPlantCode)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurableTransferD, 0, "TransferDurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        #region Before mfSave()
        //////try
            //////{
            //////    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
            //////    // SystemInfo 리소스
            //////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //////    DialogResult DResult = new DialogResult();
            //////    DataRow row;

            //////    int intRowCheck = 0;
            //////    string strStockFlag = "F";

            //////    // 필수입력사항 확인
            //////    if (this.uComboPlant.Value.ToString() == "")
            //////    {
            //////        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                        , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

            //////        // Focus
            //////        this.uComboPlant.DropDown();
            //////        return;
            //////    }

            //////    // 필수입력사항 확인
            //////    if (this.uTextTransferChargeID.Text == "")
            //////    {
            //////        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                        , "확인창", "필수입력사항 확인", "출고담당자를  입력해주세요", Infragistics.Win.HAlign.Center);

            //////        // Focus
            //////        this.uComboPlant.DropDown();
            //////        return;
            //////    }

            //////    // 필수 입력사항 확인
            //////    for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
            //////    {
            //////        this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];

            //////        if (this.uGridDurableTransferD.Rows[i].Hidden == false)
            //////        {
            //////            intRowCheck = intRowCheck + 1;

            //////            if (this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString() == "")
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", this.uGridDurableTransferD.Rows[i].RowSelectorNumber + "번째 열의 교체설비코드를 입력해주세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }

            //////            if (this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString() == "")
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", this.uGridDurableTransferD.Rows[i].RowSelectorNumber + "번째 열의 교체SparePart를 입력해주세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }

            //////            if (this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString() == "")
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", this.uGridDurableTransferD.Rows[i].RowSelectorNumber + "번째 열의 출고창고를 입력해주세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }

            //////            if (this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString() == "")
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", this.uGridDurableTransferD.Rows[i].RowSelectorNumber + "번째 열의 SparePart코드를 입력해주세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }

            //////            if (!Information.IsNumeric(uGridDurableTransferD.Rows[i].Cells["InputQty"]))
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", "올바른 숫자를 입력하세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["InputQty"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }
            //////            if (!Information.IsNumeric(uGridDurableTransferD.Rows[i].Cells["TransferQty"]))
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", "올바른 숫자를 입력하세요", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }

            //////            int intQty = Convert.ToInt32(this.uGridDurableTransferD.Rows[i].Cells["Qty"].Value.ToString());
            //////            int intTransferQty = Convert.ToInt32(this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString());

            //////            if (intTransferQty > intQty)
            //////            {
            //////                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                                , "확인창", "필수입력사항 확인", this.uGridDurableTransferD.Rows[i].RowSelectorNumber + "번째 열의 출고량이 재고량보다 클수 없습니다", Infragistics.Win.HAlign.Center);

            //////                // Focus Cell
            //////                this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"];
            //////                this.uGridDurableTransferD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            //////                return;
            //////            }
            //////        }
            //////    }

            //////    if (intRowCheck == 0)
            //////    {
            //////        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //////                        , "확인창", "필수입력사항 확인", "하나 이상의 행을 입력하셔야 합니다", Infragistics.Win.HAlign.Center);

            //////        // Focus
            //////        return;
            //////    }

            //////    DialogResult dir = new DialogResult();
            //////    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            //////                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "입력한 정보를 저장하시겠습니까? "
            //////                                , Infragistics.Win.HAlign.Right);

            //////    if (dir == DialogResult.No)
            //////    {
            //////        return;
            //////    }

            //////    DialogResult dirStockFlag = new DialogResult();
            //////    dirStockFlag = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            //////                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "저장 하려는 정보를 재고에 반영 하시겠습니까? "
            //////                                , Infragistics.Win.HAlign.Right);



            //////    // 채널연결
            //////    QRPBrowser brwChannel = new QRPBrowser();

            //////    //BL 데이터셋 호출
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferH), "DurableTransferH");
            //////    QRPDMM.BL.DMMICP.DurableTransferH clsDurableTransferH = new QRPDMM.BL.DMMICP.DurableTransferH();
            //////    brwChannel.mfCredentials(clsDurableTransferH);


            //////    //--------- 1. EQUDurableTransferH 테이블에 저장 ----------------------------------------------//
            //////    DataTable dtDurableTransferH = clsDurableTransferH.mfSetDatainfo();
            //////    row = dtDurableTransferH.NewRow();
            //////    row["PlantCode"] = this.uComboPlant.Value.ToString();
            //////    row["TransferCode"] = this.uTextTransferCode.Text;
            //////    row["TransferDate"] = this.uDateTransferDate.Value.ToString();
            //////    row["TransferChargeID"] = this.uTextTransferChargeID.Text;
            //////    row["EtcDesc"] = this.uTextEtcDesc.Text;

            //////    if (dirStockFlag == DialogResult.No)
            //////    {
            //////        row["StockFlag"] = "F";
            //////        strStockFlag = "F";
            //////    }
            //////    else
            //////    {
            //////        row["StockFlag"] = "T";
            //////        strStockFlag = "T";
            //////    }
            //////    dtDurableTransferH.Rows.Add(row);


            //////    //-------- 2.  EQUDurableTransferD 테이블에 저장 ----------------------------------------------//
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableTransferD), "DurableTransferD");
            //////    QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new QRPDMM.BL.DMMICP.DurableTransferD();
            //////    brwChannel.mfCredentials(clsDurableTransferD);

            //////    DataTable dtDurableTransferD = clsDurableTransferD.mfSetDatainfo();

            //////    for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
            //////    {
            //////        this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
            //////        if (this.uGridDurableTransferD.Rows[i].Hidden == false)
            //////        {
            //////            row = dtDurableTransferD.NewRow();
            //////            row["PlantCode"] = this.uComboPlant.Value.ToString();
            //////            row["TransferCode"] = this.uTextTransferCode.Text;
            //////            row["TransferSeq"] = i + 1;
            //////            row["TransferDurableInventoryCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString();
            //////            row["TransferDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
            //////            row["TransferLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
            //////            row["TransferQty"] = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
            //////            row["ChgEquipCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
            //////            row["ChgDurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgDurableMatCode"].Value.ToString();
            //////            row["ChgLotNo"] = this.uGridDurableTransferD.Rows[i].Cells["ChgLotNo"].Value.ToString();
            //////            row["InputQty"] = this.uGridDurableTransferD.Rows[i].Cells["InputQty"].Value.ToString();
            //////            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
            //////            row["TransferEtcDesc"] = this.uGridDurableTransferD.Rows[i].Cells["TransferEtcDesc"].Value.ToString();
            //////            row["ChgFlag"] = "";
            //////            row["ChgDate"] = "";
            //////            row["ChgChargeID"] = "";
            //////            row["ChgEtcDesc"] = "";
            //////            row["ReturnFlag"] = "";
            //////            row["ReturnDate"] = "";
            //////            row["ReturnChargeID"] = "";
            //////            row["ReturnDurableInventoryCode"] = "";
            //////            row["ReturnEtcDesc"] = "";
            //////            dtDurableTransferD.Rows.Add(row);
            //////        }
            //////    }


            //////    //-------- 3.  EQUDurableChgStandby 테이블에 저장 (수량 + 처리)----------------------------------------------//
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableChgStandby), "DurableChgStandby");
            //////    QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new QRPDMM.BL.DMMICP.DurableChgStandby();
            //////    brwChannel.mfCredentials(clsDurableChgStandby);

            //////    DataTable dtDurableChgStandby = clsDurableChgStandby.mfSetDatainfo();

            //////    for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
            //////    {
            //////        this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];
            //////        if (this.uGridDurableTransferD.Rows[i].Hidden == false)
            //////        {
            //////            row = dtDurableChgStandby.NewRow();
            //////            row["PlantCode"] = this.uComboPlant.Value.ToString();
            //////            row["DurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
            //////            row["LotNo"] = uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
            //////            row["ChgQty"] = this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
            //////            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();
            //////            dtDurableChgStandby.Rows.Add(row);
            //////        }
            //////    }


            //////    //-------- 4.  EQUDurableStock(현재 SparePart 재고정보 테이블) 에 저장 : 수량 (-) 처리 -------//
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
            //////    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
            //////    brwChannel.mfCredentials(clsDurableStock);

            //////    DataTable dtDurableStock = clsDurableStock.mfSetDatainfo();

            //////    for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
            //////    {
            //////        this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];

            //////        if (this.uGridDurableTransferD.Rows[i].Hidden == false)
            //////        {
            //////            row = dtDurableStock.NewRow();

            //////            row["PlantCode"] = this.uComboPlant.Value.ToString();
            //////            row["DurableInventoryCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString();
            //////            row["DurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
            //////            row["LotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
            //////            row["Qty"] = "-" + this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
            //////            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();

            //////            dtDurableStock.Rows.Add(row);
            //////        }
            //////    }


            //////    //-------- 5.  EQUDurableStockMoveHist(재고이력 테이블) 에 저장 -------//
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
            //////    QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
            //////    brwChannel.mfCredentials(clsDurableStockMoveHist);

            //////    DataTable dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

            //////    for (int i = 0; i < this.uGridDurableTransferD.Rows.Count; i++)
            //////    {
            //////        this.uGridDurableTransferD.ActiveCell = this.uGridDurableTransferD.Rows[0].Cells[0];

            //////        if (this.uGridDurableTransferD.Rows[i].Hidden == false)
            //////        {
            //////            row = dtDurableStockMoveHist.NewRow();

            //////            row["MoveGubunCode"] = "M03";       //자재출고일 경우는 "M03"
            //////            row["DocCode"] = "";
            //////            row["MoveDate"] = this.uDateTransferDate.Value.ToString();
            //////            row["MoveChargeID"] = this.uTextTransferChargeID.Text;
            //////            row["PlantCode"] = this.uComboPlant.Value.ToString();
            //////            row["DurableInventoryCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableInventoryCode"].Value.ToString();
            //////            row["EquipCode"] = this.uGridDurableTransferD.Rows[i].Cells["ChgEquipCode"].Value.ToString();
            //////            row["DurableMatCode"] = this.uGridDurableTransferD.Rows[i].Cells["TransferDurableMatCode"].Value.ToString();
            //////            row["LotNo"] = this.uGridDurableTransferD.Rows[i].Cells["TransferLotNo"].Value.ToString();
            //////            row["MoveQty"] = "-" + this.uGridDurableTransferD.Rows[i].Cells["TransferQty"].Value.ToString();
            //////            row["UnitCode"] = this.uGridDurableTransferD.Rows[i].Cells["UnitCode"].Value.ToString();

            //////            dtDurableStockMoveHist.Rows.Add(row);
            //////        }
            //////    }

            //////    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            //////    Thread t1 = m_ProgressPopup.mfStartThread();
            //////    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
            //////    this.MdiParent.Cursor = Cursors.WaitCursor;

            //////    //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
            //////    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
            //////    QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
            //////    brwChannel.mfCredentials(clsSAVEDurableStock);
            //////    string rtMSG = clsSAVEDurableStock.mfSaveTransfer(dtDurableTransferH, dtDurableTransferD, dtDurableChgStandby, dtDurableStock, dtDurableStockMoveHist, strStockFlag, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

            //////    // Decoding //
            //////    TransErrRtn ErrRtn = new TransErrRtn();
            //////    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
            //////    // 처리로직 끝//

            //////    this.MdiParent.Cursor = Cursors.Default;
            //////    m_ProgressPopup.mfCloseProgressPopup(this);

            //////    // 처리결과에 따른 메세지 박스
            //////    System.Windows.Forms.DialogResult result;
            //////    if (ErrRtn.ErrNum == 0)
            //////    {
            //////        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //////                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
            //////                            Infragistics.Win.HAlign.Right);
            //////        mfCreate();

            //////    }
            //////    else
            //////    {
            //////        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //////                            "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
            //////                            Infragistics.Win.HAlign.Right);
            //////    }


            //////}
            //////catch (Exception ex)
            //////{ }
            //////finally
        //////{ }
        #endregion



        #region 주석
        ////private void uGridDurableTransferD_KeyDown(object sender, KeyEventArgs e)
        ////{
        ////try
        ////{
        ////    // SystemInfo ResourceSet
        ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        ////    WinMessageBox msg = new WinMessageBox();
        ////    QRPBrowser brwChannel = new QRPBrowser();

        ////    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
        ////    Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs uCell = sender as Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs;

        ////    if (e.KeyCode == Keys.Enter)
        ////    {
        ////        // SparePartCode 열에서 엔터키를 칠 경우 SparePartName 및 현 재고 수량을 가지고 오는 이벤트 
        ////        if (uGrid.ActiveCell.Column.Key == "TransferDurableMatCode")
        ////        {
        ////            string strSparePartCode = uGrid.ActiveCell.Row.Cells["TransferDurableMatCode"].Text;
        ////            string strSPInventoryCode = uGrid.ActiveCell.Row.Cells["TransferDurableInventoryCode"].Value.ToString();
        ////            string strPlantCode = this.uComboPlant.Value.ToString();

        ////            // 필수입력사항 확인
        ////            if (strPlantCode == "")
        ////            {
        ////                DialogResult dir = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                                , "확인창", "필수입력사항 확인", "공장을 선택하세요", Infragistics.Win.HAlign.Center);

        ////                // Focus
        ////                return;
        ////            }

        ////            // 필수입력사항 확인
        ////            if (strSPInventoryCode == "")
        ////            {
        ////                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                                , "확인창", "필수입력사항 확인", "출고창고를 선택하세요", Infragistics.Win.HAlign.Center);

        ////                // Focus
        ////                return;
        ////            }

        ////            // 필수입력사항 확인
        ////            if (strSparePartCode == "")
        ////            {
        ////                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                                , "확인창", "필수입력사항 확인", "SparePartCode 를 입력하세요", Infragistics.Win.HAlign.Center);

        ////                // Focus
        ////                return;
        ////            }


        ////            brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
        ////            QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
        ////            brwChannel.mfCredentials(clsDurableStock);

        ////            DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_Detail(strPlantCode, strSPInventoryCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));

        ////            if (dtDurableStock.Rows.Count > 0)
        ////            {
        ////                uGrid.ActiveCell.Row.Cells["TransferDurableMatName"].Value = dtDurableStock.Rows[0]["DurableMatName"].ToString();
        ////                uGrid.ActiveCell.Row.Cells["Qty"].Value = Convert.ToInt32(dtDurableStock.Rows[0]["Qty"].ToString());
        ////            }
        ////            else
        ////            {
        ////                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
        ////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////                            "확인창", "입력확인", "올바른 코드를 입력하세요",
        ////                            Infragistics.Win.HAlign.Right);
        ////                //uGrid.ActiveCell.Row.Cells["Qty"].Value = "0";
        ////                uGrid.ActiveCell.Row.Cells["TransferDurableMatName"].Value = "";
        ////                uGrid.ActiveCell.Row.Cells["TransferDurableMatCode"].Value = "";
        ////            }



        ////        }

        ////    }
        ////}
        ////catch (Exception ex)
        ////{
        ////}
        ////finally
        ////{
        ////}
        ////}
        #endregion
    }
}
