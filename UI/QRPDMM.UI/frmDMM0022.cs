﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMM0022.cs                                         */
/* 프로그램명   : 치공구폐기등록                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPDMM.UI
{
    public partial class frmDMM0022 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMM0022()
        {
            InitializeComponent();
        }

        private void frmDMM0022_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMM0022_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
            uButtonDeleteRow.Visible = false;
        }
        private void frmDMM0022_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("치공구폐기등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //버튼
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextDiscardChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextDiscardChargeName.Text = m_resSys.GetString("SYS_USERNAME");

               

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //Label의 text,font,lmage,mandentory 설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDiscardDate, "폐기일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardDate, "폐기일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardChargeID, "폐기담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDiscardReason, "폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDurableInventoryCode, "폐기창고", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboDurableInventory.Items.Clear();
                //this.uComboDurableInventory.Text = "";
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox grpbox = new WinGroupBox();
                grpbox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "폐기자재리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                //자재정보
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //자재정보
                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardCode", "폐기문서번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardDate", "폐기일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardChargeID", "폐기담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardChargeName", "폐기담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardReason", "폐기사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryCode", "폐기창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryName", "폐기창고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DiscardQty", "폐기량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Button, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;



                //폐기자재리스트
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FOTNNAME"));

                //폐기자재리스트
                grd.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid2, 0, "Qty", "재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "#,###", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "DiscardQty", "폐기량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Right
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "#,###", "nnnnnnnnn", "");

                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄생성
                grd.mfAddRowGrid(this.uGrid2, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                    if (!this.uComboDurableInventory.Value.ToString().Equals(string.Empty))
                        this.uComboDurableInventory.SelectedIndex = 0;

                    if (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGrid2.Rows.All);
                        this.uGrid2.DeleteSelectedRows(false);
                    }

                }
                 if (this.uDateFromDiscardDate.Value == null || this.uDateFromDiscardDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000209", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateFromDiscardDate.DropDown();
                    return;
                }

                 if (this.uDateToDiscardDate.Value == null || this.uDateToDiscardDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000218", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateToDiscardDate.DropDown();
                    return;
                }
                 if (this.uDateFromDiscardDate.DateTime.Date > this.uDateToDiscardDate.DateTime.Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000211", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateFromDiscardDate.DropDown();
                    return;
                }


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableDiscard), "DurableDiscard");
                QRPDMM.BL.DMMICP.DurableDiscard clsDurableDiscard = new QRPDMM.BL.DMMICP.DurableDiscard();
                brwChannel.mfCredentials(clsDurableDiscard);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strFromDiscardDate = this.uDateFromDiscardDate.Value.ToString();
                string strToDiscardDate = this.uDateToDiscardDate.Value.ToString();

                // Call Method
                DataTable dtSPTransferH = clsDurableDiscard.mfReadDMMDurableDiscard(strPlantCode, strFromDiscardDate, strToDiscardDate, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtSPTransferH;
                uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSPTransferH.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";

                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uDateDiscardDate.Value == null || this.uDateDiscardDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001195", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateDiscardDate.DropDown();
                    return;
                }
                // 필수입력사항 확인
                if (this.uTextDiscardChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001191", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextDiscardChargeID.Focus();
                    return;
                }
                if (this.uComboDurableInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001197", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboDurableInventory.DropDown();
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;

                            if (this.uGrid2.Rows[i].Cells["DiscardQty"].Value.ToString() == "0")
                            {

                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang)
                                                , this.uGrid2.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000536", strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["ChgEquipCode"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }


                            int intQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["Qty"].Value.ToString());
                            int intDiscardQty = Convert.ToInt32(this.uGrid2.Rows[i].Cells["DiscardQty"].Value.ToString());

                            if (intDiscardQty > intQty)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                , this.uGrid2.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000537", strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["DiscardQty"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001238", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableDiscard), "DurableDiscard");
                QRPDMM.BL.DMMICP.DurableDiscard clsDurableDiscard = new QRPDMM.BL.DMMICP.DurableDiscard();
                brwChannel.mfCredentials(clsDurableDiscard);


                //--------- 1. EQUDurableDiscard 테이블에 저장 ----------------------------------------------//
                DataTable dtDurableDiscard = clsDurableDiscard.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableDiscard.NewRow();
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DiscardCode"] = "";
                            row["DiscardDate"] = this.uDateDiscardDate.Value.ToString();
                            row["DiscardChargeID"] = this.uTextDiscardChargeID.Text;
                            row["DiscardReason"] = this.uTextDiscardReason.Text;
                            row["DurableInventoryCode"] = this.uComboDurableInventory.Value.ToString();
                            row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();
                            row["DiscardQty"] = this.uGrid2.Rows[i].Cells["DiscardQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtDurableDiscard.Rows.Add(row);
                        }
                    }
                }

                //-------- 2.  EQUDurableStock(현재 SparePart 재고정보 테이블) 에 저장 : 수량 (-) 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                DataTable dtDurableStock = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableStock.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DurableInventoryCode"] = this.uComboDurableInventory.Value.ToString();
                            row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();
                            row["Qty"] = "-" + this.uGrid2.Rows[i].Cells["DiscardQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtDurableStock.Rows.Add(row);
                        }
                    }
                }


                //-------- 3.  EQUDurableStockMoveHist(재고이력 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                DataTable dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableStockMoveHist.NewRow();

                            row["MoveGubunCode"] = "M06";       //자재폐기일 경우는 "M06"
                            row["DocCode"] = "";
                            row["MoveDate"] = this.uDateDiscardDate.Value.ToString();
                            row["MoveChargeID"] = this.uTextDiscardChargeID.Text;
                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DurableInventoryCode"] = this.uComboDurableInventory.Value.ToString();
                            row["EquipCode"] = "";
                            row["DurableMatCode"] = this.uGrid2.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGrid2.Rows[i].Cells["LotNo"].Value.ToString();
                            row["MoveQty"] = "-" + this.uGrid2.Rows[i].Cells["DiscardQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid2.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtDurableStockMoveHist.Rows.Add(row);
                        }
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveDiscard(dtDurableDiscard, dtDurableStock, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;



                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001238", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo,500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableDiscard), "DurableDiscard");
                QRPDMM.BL.DMMICP.DurableDiscard clsDurableDiscard = new QRPDMM.BL.DMMICP.DurableDiscard();
                brwChannel.mfCredentials(clsDurableDiscard);


                //--------- 1. EQUDurableDiscard 테이블에 삭제 : 재고이력은 본 프로시져에서 처리 ----------------------------------------------//
                DataTable dtDurableDiscard = clsDurableDiscard.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];
                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableDiscard.NewRow();
                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DiscardCode"] = this.uGrid1.Rows[i].Cells["DiscardCode"].Value.ToString();
                            row["DiscardDate"] = this.uGrid1.Rows[i].Cells["DiscardDate"].Value.ToString();
                            row["DiscardChargeID"] = this.uGrid1.Rows[i].Cells["DiscardChargeID"].Value.ToString();
                            row["DiscardReason"] = this.uGrid1.Rows[i].Cells["DiscardReason"].Value.ToString();
                            row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                            row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                            row["DiscardQty"] = this.uGrid1.Rows[i].Cells["DiscardQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtDurableDiscard.Rows.Add(row);
                        }
                    }
                }

                //-------- 2.  EQUDurableStock(현재 SparePart 재고정보 테이블) 에 저장 : 수량 (+) 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                DataTable dtDurableStock = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableStock.NewRow();

                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                            row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                            row["Qty"] = this.uGrid1.Rows[i].Cells["DiscardQty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                            dtDurableStock.Rows.Add(row);
                        }
                    }
                }


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfDeleteDiscard(dtDurableDiscard, dtDurableStock, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M000638", "M000677",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M000638", "M000676",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {
                //InitCombo();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                InitText();

                if (uGroupBoxContentsArea.Expanded == false)
                {

                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                    uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    WinGrid grd = new WinGrid();

                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGrid1.Rows.Count == 0 && (this.uGrid2.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                if (this.uGrid1.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGrid1);
                if (this.uGrid2.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGrid2);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...
        // Grid Cell Update Event
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀수정시 RowSelector 이미지 처리
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 빈행일시 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Contents GroupBox 펼침상태 변화 이벤트

        

        // 행삭제 버튼 이벤트

        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid2.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitText();
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                this.uComboDurableInventory.Items.Clear();

                if (strPlantCode == "")
                    return;


                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                ////wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "DurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);


                wCombo.mfSetComboEditor(this.uComboDurableInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboDurableInventory_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strDurableInventoryCode = this.uComboDurableInventory.Value.ToString();


                // Call Method
                DataTable dtSPTransferH = clsDurableStock.mfReadDurableStock_SPCombo_Lot(strPlantCode, strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid2.DataSource = dtSPTransferH;
                uGrid2.DataBind();



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region
        /// <summary>
        /// 에디트 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextDiscardChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                this.uTextDiscardChargeID.Text = frmUser.UserID;
                this.uTextDiscardChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 사용자 검색 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextDiscardChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextDiscardChargeID.Text;

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                        this.uTextDiscardChargeID.Focus();
                        return;
                    }
                    else
                    {
                        this.uTextDiscardChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDiscardChargeID.Text = "";
                    this.uTextDiscardChargeName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

    }
}
