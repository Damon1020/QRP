﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmDMMZ0019.cs                                        */
/* 프로그램명   : Shot수등록/정비등록                                   */
/* 작성자       : 권종구 , 코딩 : 남현식                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0019 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmDMMZ0019()
        {
            InitializeComponent();
        }

        private void frmDMMZ0019_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부처리
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0019_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitButton();
            InitText();
            InitLabel();
            InitGird();
            InitGroupBox();
            InitComboBox();

            this.uGroupBoxContentsArea.Expanded = false;
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 초기화Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀등록
                titleArea.mfSetLabelText("Shot수등록/정비등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값설정
                //--등록자
                uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");

                //--Shot판정담당자
                uTextJudgeChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextJudgeChargeName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextChangeUsage.Text = "";
                this.uTextCumUsage.Text = "";
                this.uTextCurUsage.Text = "";
                this.uTextDurableMatCode.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextEtcDesc2.Text = "";

                this.uTextPlantCode.Text = "";
                this.uTextUsageLimit.Text = "";
                this.uTextUsageLimitLower.Text = "";
                this.uTextUsageLimitUpper.Text = "";
                this.uTextInputCurUsage.Text = "";
                this.uTextRepairCode.Text = "";
                this.uTextRepairGICode.Text = "";
                this.uTextLotNo.Text = "";
                this.uTextDurableMatCode.Visible = false;
                this.uTextCumUsage.Visible = false;
                this.uTextEquipCode.Visible = false;
                this.uTextPlantCode.Visible = false;
                //this.uText.Visible = false;
                //this.uText.Visible = false;

                this.uCheckFinishFlag.CheckedValue = false;
                this.uCheckChangeFlag.CheckedValue = false;
                this.uCheckRepairFlag.CheckedValue = false;
                this.uCheckStandbyFlag.CheckedValue = false;
                
                this.uCheckUsageClearFlag.CheckedValue = false;
                this.uCheckChangeFlag.Enabled = false;

                this.uComboUsageJudgeCode.Value = "";


                this.uTextRepairCode.Visible = false;
                this.uTextRepairGICode.Visible = false;
                //this.uTextRepairCode.Visible = true;
                //this.uTextRepairGICode.Visible = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.ultraButton1, "금형치공구교체", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.ultraButton2, "SP사용등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.ultraButton3, "정비상세등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchEquip, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //lbl.mfSetLabel(this.uLabelEquipmentName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelEquipmentProcess, "설비공정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelEquipmentType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //lbl.mfSetLabel(this.uLabelEquipmentGroup, "설비그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelRegisterDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRegister, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInfo, "구성품정보", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelChangeUsage, "교체Shot수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimitLower, "Limit-1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimit, "Limit 0", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageLimitUpper, "Limit+1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCurUsage, "현재Shot수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUsageJudgeCode, "Shot판정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelUsageJudgeDate, "Shot판정일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelJudgeChargeID, "Shot판정담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelStandbyDate, "정비대기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairDate, "정비착수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFinishDate, "정비완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepairResultCode, "점검결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNote1, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGird()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid1, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGrid1, 0, "ChangeUsage", "교체Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimit", "Limit 0", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CurUsage", "현재Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CumUsage", "누적Shot수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Shot%", "Shot%", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LastInspectDate", "최종점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LimitArrivalDate", "경과일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(uGrid1, 0);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant,true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtCommon = clsCommonCode.mfReadCommonCode("C0041", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboUsageJudgeCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCommon);

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그룹박스 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.DETAIL, "Shot등록정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "점검처리", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "정비일지", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(true))
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;

                // 필수입력사항 확인
                if (strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (strEquipCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000699", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextSearchEquipCode.Focus();
                    return;
                }
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);
                DataTable dtEquipSPBOM = clsEquipDurableBOM.mfReadEquipDurableBOM_LimitOver(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGrid1.DataSource = dtEquipSPBOM;
                uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtEquipSPBOM.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;

                int intRowCheck = 0;
                string strStockFlag = "F";

       
                #region 필수입력사항
                if (this.uTextPlantCode.Text == "" || this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "저장할 정보를 리스트에서 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextDurableMatCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "저장할 정보를 리스트에서 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                // 필수입력사항 확인
                if (this.uComboUsageJudgeCode.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "Shot판정을 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboUsageJudgeCode.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uTextInputCurUsage.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "현재 Shot을 입력해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextInputCurUsage.Focus();
                    return;
                }

                if (this.uComboUsageJudgeCode.Value.ToString() != "R")
                {
                    double numCurUsage = Convert.ToDouble(this.uTextInputCurUsage.Text);
                    double numUsageLimit = Convert.ToDouble(this.uTextUsageLimit.Text);

                    if (numCurUsage >= numUsageLimit)
                    {
                        DialogResult dirStand = new DialogResult();
                        dirStand = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "Limit보다 입력한 현재Shot가 더 큽니다. 정비일지 정보를 입력하시겠습니까? "
                                                , Infragistics.Win.HAlign.Right);
                        if (dirStand == DialogResult.Yes)
                        {
                            this.uComboUsageJudgeCode.Value = "R";
                        }
                    }
                }


                if (this.uComboUsageJudgeCode.Value.ToString() == "R")
                {
                    if (this.uCheckFinishFlag.Checked == false)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비완료정보를 선택해 주십시오", Infragistics.Win.HAlign.Center);

                        // Focus
                        return;
                    }

                    if (this.uComboRepairResultCode.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "확인창", "필수입력사항 확인", "점검결과를 선택해 주십시오", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboRepairResultCode.DropDown();
                        return;

                    }

                    if (this.uCheckUsageClearFlag.Checked == true)
                    {
                        DialogResult dirClear = new DialogResult();
                        dirClear = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "초기화를 하면 현재Shot정보가 0 이 됩니다. 계속하시겠습니까? "
                                                , Infragistics.Win.HAlign.Right);

                        if (dirClear == DialogResult.No)
                        {
                            return;
                        }
                    }


                    // 필수입력사항 확인
                    if (this.uCheckFinishFlag.CheckedValue.Equals(true))
                    {
                        if (this.uComboRepairResultCode.Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "정비완료일 경우, 반드시 점검결과를 선택하셔야 합니다", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uComboRepairResultCode.DropDown();
                            return;
                        }
                    }
                    else
                    {
                        if (this.uComboRepairResultCode.Value.ToString() != "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "정비 완료일 경우에만 점검결과를 선택해 주시기 바랍니다.", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uComboRepairResultCode.Value = "";
                            this.uCheckChangeFlag.CheckedValue = false;
                            this.uCheckUsageClearFlag.CheckedValue = false;
                            return;
                        }

                        if (this.uCheckUsageClearFlag.CheckedValue.Equals(true))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "초기화 여부는 정비완료일 경우에만 선택 할 수 있습니다.", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uCheckUsageClearFlag.CheckedValue = false;
                            this.uCheckFinishFlag.Focus();
                            return;
                        }
                    }


                    if (this.uCheckStandbyFlag.CheckedValue.Equals(true) && this.uCheckRepairFlag.CheckedValue.Equals(true) && this.uCheckFinishFlag.CheckedValue.Equals(true))
                    {
                        if (Convert.ToDateTime(this.uDateStandbyDate.Value) > Convert.ToDateTime(this.uDateRepairDate.Value))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "정비착수일을 정비대기일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uDateRepairDate.Focus();
                            return;
                        }

                        if (Convert.ToDateTime(this.uDateRepairDate.Value) > Convert.ToDateTime(this.uDateFinishDate.Value))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "정비완료일을 정비착수일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uDateFinishDate.Focus();
                            return;
                        }

                    }

                    if (this.uCheckStandbyFlag.CheckedValue.Equals(true) && this.uCheckRepairFlag.CheckedValue.Equals(true))
                    {
                        if (Convert.ToDateTime(this.uDateStandbyDate.Value) > Convert.ToDateTime(this.uDateRepairDate.Value))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "정비착수일을 정비대기일보다 이전의 날짜로 선택할 수는 없습니다.", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uDateRepairDate.Focus();
                            return;
                        }

                    }
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                #endregion

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);
                if (dir == DialogResult.No)
                {
                    return;
                }

                

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //////BL 데이터셋 호출
                ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                ////QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                ////brwChannel.mfCredentials(clsDurableMat);


                //////--------- 1. MASDurableMat 테이블에 저장 ----------------------------------------------//
                ////// 실제 업데이트 되는 값은 LastInspectDate 뿐임. 나머지는 BL에서 에러막기위해 처리
                ////DataTable dtDurableMat = clsDurableMat.mfSetDatainfo();
                ////row = dtDurableMat.NewRow();
                ////row["PlantCode"] = this.uTextPlantCode.Text;
                ////row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                ////row["DurableMatName"] = "";
                ////row["DurableMatNameCh"] = "";
                ////row["DurableMatNameEn"] = "";
                ////row["DurableMatTypeCode"] = "";
                ////row["Spec"] = "";
                ////row["UsageLimitLower"] = "0";
                ////row["UsageLimit"] = "0";
                ////row["UsageLimitUpper"] = "0";

                ////row["UsageLimitUnitName"] = "";
                ////row["UseFlag"] = "";
                ////row["ChangeUsage"] = "0";
                ////row["CurUsage"] = this.uTextInputCurUsage.Text;
                ////row["CumUsage"] = "0";
                ////row["LastInspectDate"] = this.uDateUsageJudgeDate.Value.ToString();
                ////row["LimitArrivalDate"] = "";

                ////dtDurableMat.Rows.Add(row);

                //BL 데이터셋 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);

                int intCurUsage = Convert.ToInt32(this.uTextInputCurUsage.Text);
                int intCumUsage = Convert.ToInt32(this.uTextInputCurUsage.Text);

                //--------- 1. MASDurableLot 테이블에 저장 ----------------------------------------------//
                // 실제 업데이트 되는 값은 LastInspectDate 뿐임. 나머지는 BL에서 에러막기위해 처리
                DataTable dtDurableLot = clsDurableLot.mfSetDatainfo();
                row = dtDurableLot.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["GRDate"] = "";

                row["DiscardFlag"] = "F";
                row["DiscardDate"] = "";
                row["DiscardChargeID"] = "";
                row["DiscardReason"] = "";
                row["ChangeUsage"] = "0";
                row["CurUsage"] = intCurUsage;
                row["CumUsage"] = intCumUsage;
                row["LastInspectDate"] = this.uDateUsageJudgeDate.Value.ToString();
                row["LimitArrivalDate"] = "";
                dtDurableLot.Rows.Add(row);



                //-------- 2.  DMMInspectHist 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.InspectHist), "InspectHist");
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new QRPDMM.BL.DMMMGM.InspectHist();
                brwChannel.mfCredentials(clsInspectHist);

                DataTable dtInspectHist = clsInspectHist.mfSetDatainfo();


                row = dtInspectHist.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["UsageDocCode"] = "";
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["UsageJudgeCode"] = this.uComboUsageJudgeCode.Value.ToString();
                row["UsageJudgeDate"] = this.uDateUsageJudgeDate.Value.ToString();
                row["JudgeChargeID"] = this.uTextJudgeChargeID.Text;
                row["CurUsage"] = this.uTextInputCurUsage.Text;
                row["CumUsage"] = this.uTextCumUsage.Text;
                row["EtcDesc"] = this.uTextEtcDesc.Text;

                dtInspectHist.Rows.Add(row);


                //-------- 3.  DMMRepairH 테이블에 저장 ----------------------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.RepairH), "RepairH");
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new QRPDMM.BL.DMMMGM.RepairH();
                brwChannel.mfCredentials(clsRepairH);

                DataTable dtRepairH = clsRepairH.mfSetDatainfo();

                row = dtRepairH.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["RepairCode"] = "";
                row["DurableMatCode"] = this.uTextDurableMatCode.Text;
                row["LotNo"] = this.uTextLotNo.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["RepairGubunCode"] = "";
                row["UsageDocCode"] = "";
                if (this.uCheckStandbyFlag.Checked == true)
                {
                    row["StandbyFlag"] = "T";
                    row["StandbyDate"] = this.uDateStandbyDate.Value.ToString();
                }
                else
                {
                    row["StandbyFlag"] = "F";
                    row["StandbyDate"] = "";
                }

                if (this.uCheckRepairFlag.Checked == true)
                {
                    row["RepairFlag"] = "T";
                    row["RepairDate"] = this.uDateRepairDate.Value.ToString();
                }
                else
                {
                    row["RepairFlag"] = "F";
                    row["RepairDate"] = "";
                }

                if (this.uCheckFinishFlag.Checked == true)
                {
                    row["FinishFlag"] = "T";
                    row["FinishDate"] = this.uDateFinishDate.Value.ToString();
                }
                else
                {
                    row["FinishFlag"] = "F";
                    row["FinishDate"] = "";
                }

                row["RepairResultCode"] = this.uComboRepairResultCode.Value.ToString();

                if (this.uCheckChangeFlag.Checked == true)
                    row["ChangeFlag"] = "T";
                else
                    row["ChangeFlag"] = "F";

                row["EtcDesc"] = this.uTextEtcDesc2.Text;
                if (this.uCheckUsageClearFlag.Checked == true)
                    row["UsageClearFlag"] = "T";
                else
                    row["UsageClearFlag"] = "F";

                row["UsageClearDate"] = "";
                row["ClearCurUsage"] = this.uTextInputCurUsage.Text;
                row["ClearCumUsage"] = this.uTextCumUsage.Text;
                dtRepairH.Rows.Add(row);


                //---------4. EQUDurableMatRepairGIH 테이블에 저장 ---------------------------------//
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                brwChannel.mfCredentials(clsDurableMatRepairH);
                DataTable dtDurableMatRepairH = clsDurableMatRepairH.mfDataSetInfo();
                row = dtDurableMatRepairH.NewRow();
                row["PlantCode"] = this.uTextPlantCode.Text;
                row["RepairGICode"] = "";
                row["GITypeCode"] = "LF";
                row["PlanYear"] = "";
                row["EquipCode"] = this.uTextEquipCode.Text;
                row["PMPlanDate"] = "";
                row["RepairReqCode"] = "";
                row["RepairGIReqDate"] = "";
                row["RepairGIReqID"] = "";
                row["EtcDesc"] = "";
                dtDurableMatRepairH.Rows.Add(row);



                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : SparePart 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMMGM.SAVEMGM), "SAVEMGM");
                QRPDMM.BL.DMMMGM.SAVEMGM clsSAVEMGM = new QRPDMM.BL.DMMMGM.SAVEMGM();
                brwChannel.mfCredentials(clsSAVEMGM);
                string rtMSG = clsSAVEMGM.mfSaveShotInput(dtDurableLot, dtInspectHist, dtRepairH, dtDurableMatRepairH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                if (this.uComboUsageJudgeCode.Value.ToString() == "R")
                {
                    // 교체여부가 체크되어있으면 EQUDurableMatRepairGIH를 저장하는 프로시져에서 리턴값을 두개가지고옴
                    // Return(0) = RepairGICode      ,,,       Return(1) = RepairCode
                    if (this.uCheckChangeFlag.Checked == true)
                    {
                        this.uTextRepairGICode.Text = ErrRtn.mfGetReturnValue(0); //화면상에 보이지 않는 텍스트 박스에 리턴값 받아놓음.
                        this.uTextRepairCode.Text = ErrRtn.mfGetReturnValue(1); //화면상에 보이지 않는 텍스트 박스에 리턴값 받아놓음.
                    }

                    // 교체여부가 체크되어있지않으면 DMMRepairH를 저장하는 프로시져에서 리턴값을 한개가지고옴
                    // Return(0) = RepairCode
                    else
                    {
                        this.uTextRepairGICode.Text = "";
                        this.uTextRepairCode.Text = ErrRtn.mfGetReturnValue(0); //화면상에 보이지 않는 텍스트 박스에 리턴값 받아놓음.
                    }
                }
                else
                {
                    this.uTextRepairGICode.Text = "";
                    this.uTextRepairCode.Text = "";

                }
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);


                    //if (this.uComboUsageJudgeCode.Value.ToString() == "R")
                    //{
                    //    QRPDMM.UI.frmDMM0014 frmCarryOut = new QRPDMM.UI.frmDMM0014(this.uTextPlantCode.Text, strRepairCode);
                    //    frmCarryOut.FormName = this.Name;

                    //    CommonControl cControl = new CommonControl();
                    //    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                    //    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                    //    uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0014", "점검일지등록");
                    //    uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0014"];
                    //    frmCarryOut.MdiParent = this.ParentForm;
                    //    frmCarryOut.Show();
                    //}

                    //mfCreate();
                    //mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                InitText();
                
            }
            catch
            { }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        #region Event

        #region Combo
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextSearchEquipCode.Text.Equals(string.Empty) || !this.uTextSearchEquipName.Text.Equals(string.Empty))
                {
                    this.uTextSearchEquipCode.Text = string.Empty;
                    this.uTextSearchEquipName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboRepairResultCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();



                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                string strPMResultCode = this.uComboRepairResultCode.Value.ToString();
                string strPlantCode = this.uTextPlantCode.Text;

                if (strPlantCode == "")
                    return;

                if (strPMResultCode == "")
                    return;


                // Call Method
                DataTable dtPMResultType = clsPMResultType.mfReadPMResultType_Detail(this.uTextPlantCode.Text, strPMResultCode, m_resSys.GetString("SYS_LANG"));

                if (dtPMResultType.Rows[0]["ChangeFlag"].ToString() == "T")
                {
                    uCheckChangeFlag.CheckedValue = true;
                }
                else
                {
                    uCheckChangeFlag.CheckedValue = false;
                }




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region POP업창
        private void ultraButton3_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();


                if (this.uComboUsageJudgeCode.Value.ToString() != "R")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "Shot판정을 [정비]로 선택하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }


                if (this.uTextRepairCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비정보를 저장하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                ////QRPDMM.UI.frmDMM0014 frmCarryOut = new QRPDMM.UI.frmDMM0014(this.uTextPlantCode.Text, this.uTextRepairCode.Text);
                ////frmCarryOut.FormName = this.Name;

                ////CommonControl cControl = new CommonControl();
                ////Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                ////Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                ////uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0014", "점검일지등록");
                ////uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0014"];
                ////frmCarryOut.MdiParent = this.ParentForm;
                ////frmCarryOut.Show();

                QRPDMM.UI.frmDMM0014 frmCarryOut = new QRPDMM.UI.frmDMM0014(this.uTextPlantCode.Text, this.uTextRepairCode.Text);
                frmCarryOut.FormName = this.Name;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;

                //해당화면의 탭이 있는경우 해당 탭 닫기
                if (uTabMenu.Tabs.Exists("QRPDMM" + "," + "frmDMM0015"))
                {
                    uTabMenu.Tabs["QRPDMM" + "," + "frmDMM0015"].Close();
                }
                //탭에 추가
                uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0014", "점검일지등록");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0014"];


                frmCarryOut.AutoScroll = true;
                frmCarryOut.MdiParent = this.MdiParent;
                frmCarryOut.ControlBox = false;
                frmCarryOut.Dock = DockStyle.Fill;
                frmCarryOut.FormBorderStyle = FormBorderStyle.None;
                frmCarryOut.WindowState = FormWindowState.Normal;
                frmCarryOut.Text = "점검일지등록";
                frmCarryOut.Show();
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            

        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                if (this.uComboUsageJudgeCode.Value.ToString() != "R")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "Shot판정을 [정비]로 선택하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                if (this.uCheckChangeFlag.Checked.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "교체여부가 없으므로 해당 화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                if (this.uTextRepairCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비정보를 저장하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                QRPDMM.UI.frmDMMZ0010 frmCarryOut = new QRPDMM.UI.frmDMMZ0010(this.uTextPlantCode.Text, this.uTextRepairGICode.Text);
                frmCarryOut.FormName = this.Name;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //해당화면의 탭이 있는경우 해당 탭 닫기

                if (uTabMenu.Tabs.Exists("QRPDMM" + "," + "frmDMMZ0010"))
                {
                    uTabMenu.Tabs["QRPDMM" + "," + "frmDMMZ0010"].Close();
                }

                //탭에 추가
                uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMMZ0010", "점검출고등록");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMMZ0010"];


                frmCarryOut.AutoScroll = true;
                frmCarryOut.MdiParent = this.MdiParent;
                frmCarryOut.ControlBox = false;
                frmCarryOut.Dock = DockStyle.Fill;
                frmCarryOut.FormBorderStyle = FormBorderStyle.None;
                frmCarryOut.WindowState = FormWindowState.Normal;
                frmCarryOut.Text = "점검출고등록";
                frmCarryOut.Show();





            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                if (this.uComboUsageJudgeCode.Value.ToString() != "R")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "Shot판정을 [정비]로 선택하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                if (this.uTextRepairCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비정보를 저장하지 않았으므로 해당화면으로 이동 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                ////QRPDMM.UI.frmDMM0015 frmCarryOut = new QRPDMM.UI.frmDMM0015(this.uTextPlantCode.Text, this.uTextRepairCode.Text);
                ////frmCarryOut.FormName = this.Name;

                ////CommonControl cControl = new CommonControl();
                ////Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                ////Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //////해당화면의 탭이 있는경우 해당 탭 닫기

                ////if (uTabMenu.Tabs.Exists("QRPDMM" + "," + "frmDMM0015"))
                ////{
                ////    uTabMenu.Tabs["QRPDMM" + "," + "frmDMM0015"].Close();
                ////}

                //////탭에 추가
                ////uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0015", "정비점검결과등록");
                ////uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0015"];


                ////frmCarryOut.MdiParent = this.ParentForm;
                ////frmCarryOut.Show();

                QRPDMM.UI.frmDMM0015 frmCarryOut = new QRPDMM.UI.frmDMM0015(this.uTextPlantCode.Text, this.uTextRepairCode.Text);
                frmCarryOut.FormName = this.Name;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //해당화면의 탭이 있는경우 해당 탭 닫기

                if (uTabMenu.Tabs.Exists("QRPDMM" + "," + "frmDMM0015"))
                {
                    uTabMenu.Tabs["QRPDMM" + "," + "frmDMM0015"].Close();
                }

                //탭에 추가
                uTabMenu.Tabs.Add("QRPDMM" + "," + "frmDMM0015", "정비점검결과등록");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPDMM,frmDMM0015"];


                frmCarryOut.AutoScroll = true;
                frmCarryOut.MdiParent = this.MdiParent;
                frmCarryOut.ControlBox = false;
                frmCarryOut.Dock = DockStyle.Fill;
                frmCarryOut.FormBorderStyle = FormBorderStyle.None;
                frmCarryOut.WindowState = FormWindowState.Normal;
                frmCarryOut.Text = "정비점검결과등록";
                frmCarryOut.Show();

 



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Check
        private void uCheckRepairFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                if (this.uCheckStandbyFlag.CheckedValue.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비 착수가 선택되려면 정비대기가 먼저 선택되어져야 합니다.", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uCheckStandbyFlag.Focus();
                    this.uCheckRepairFlag.CheckedValue = false;
                    return;
                }
                if (this.uCheckRepairFlag.Checked.Equals(false))
                {
                    this.uCheckFinishFlag.Checked = false;
                }


            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckFinishFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                if (this.uCheckRepairFlag.CheckedValue.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "정비완료를 선택하려면 정비착수가 먼저 선택되어져야 합니다.", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uCheckRepairFlag.Focus();
                    this.uCheckFinishFlag.CheckedValue = false;
                    return;
                }


            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 체크박스 해제시 하위 체크박스도 같이 선택 해제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckStandbyFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckStandbyFlag.Checked.Equals(false))
                {
                    this.uCheckRepairFlag.Checked = false;
                    this.uCheckFinishFlag.Checked = false;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }        

        #endregion

        #region Text

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "입력확인", "공장을 선택해주세요.",
                                    Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frm2 = new QRPCOM.UI.frmCOM0005();
                frm2.PlantCode = uComboPlant.Value.ToString();
                frm2.ShowDialog();
                uTextSearchEquipCode.Text = frm2.EquipCode;
                uTextSearchEquipName.Text = frm2.EquipName;

                
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextJudgeChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = uTextPlantCode.Text.ToString().Trim();
                frm.ShowDialog();

                uTextJudgeChargeID.Text = frm.UserID;
                uTextJudgeChargeName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frm = new frmPOP0011();
                frm.PlantCode = uTextPlantCode.Text.ToString().Trim();
                frm.ShowDialog();

                uTextWriteID.Text = frm.UserID;
                uTextWriteName.Text = frm.UserName;
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = uTextPlantCode.Text.ToString().Trim();
                //String strPlantCode = uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextWriteID;
                uTextName = this.uTextWriteName;

                String strUserID = uTextID.Text;
                
                //uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "사번을 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
                //백스페이스나 Delete키 누를때 텍스트박스 삭제
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextWriteID.Text = "";
                    this.uTextWriteName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextJudgeChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = uTextPlantCode.Text.ToString().Trim();
                //String strPlantCode = uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextJudgeChargeID;
                uTextName = this.uTextJudgeChargeName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "사번을 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextJudgeChargeID.Text = "";
                    this.uTextJudgeChargeName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strEquipCode = this.uTextSearchEquipCode.Text;

                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
                    if (dtEquip.Rows.Count == 0)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "없는 설비코드 입니다.",
                                            Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                   
                    this.uTextSearchEquipName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextSearchEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                this.uTextSearchEquipName.Clear();
        }

        /// <summary>
        /// 수량 입력시 Decimal만 입력 가능
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextInputCurUsage_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) || e.KeyChar == Convert.ToChar(46)))
                {
                    e.Handled = true;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        #endregion

        #region Grid
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 200);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 680;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();


                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    
                }

                e.Cell.Row.Fixed = true;

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();
                string strLotNo = e.Cell.Row.Cells["LotNo"].Value.ToString();

                if (strPlantCode == "")
                    return;

                if (strDurableMatCode == "")
                    return;

                if (strLotNo == "")
                    return;


                //this.uTextEquipCode.Text = e.Cell.Row.Cells["EquipCode"].Value.ToString();

                Display_Detail(strPlantCode, strDurableMatCode, strLotNo, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Display_Detail(string strPlantCode, string strDurableMatCode, string strLotNo, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinComboEditor wCombo = new WinComboEditor();

                
                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                 
                QRPBrowser brwChannel = new QRPBrowser();

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);

                // Call Method
                DataTable dtDurableLot = clsDurableLot.mfReadMASDurableMatLot_Detail(strPlantCode, strDurableMatCode,strLotNo, m_resSys.GetString("SYS_LANG"));


                for (int i = 0; i < dtDurableLot.Rows.Count; i++)
                {

                    this.uTextChangeUsage.Text = dtDurableLot.Rows[i]["ChangeUsage"].ToString();
                    this.uTextCumUsage.Text = dtDurableLot.Rows[i]["CumUsage"].ToString();
                    this.uTextCurUsage.Text = dtDurableLot.Rows[i]["CurUsage"].ToString();
                    this.uTextDurableMatCode.Text = dtDurableLot.Rows[i]["DurableMatCode"].ToString();
                    this.uTextEquipCode.Text =  e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    this.uTextPlantCode.Text = dtDurableLot.Rows[i]["PlantCode"].ToString();
                    this.uTextUsageLimit.Text = dtDurableLot.Rows[i]["UsageLimit"].ToString();
                    this.uTextUsageLimitLower.Text = dtDurableLot.Rows[i]["UsageLimitLower"].ToString();
                    this.uTextUsageLimitUpper.Text = dtDurableLot.Rows[i]["UsageLimitUpper"].ToString();
                    this.uTextLotNo.Text = dtDurableLot.Rows[i]["LotNo"].ToString();

                }


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                // Call Method
                DataTable dtPMResultType = clsPMResultType.mfReadPMResultTypeCombo(this.uTextPlantCode.Text, m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboRepairResultCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PMResultCode", "PMResultName", dtPMResultType);



            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmDMMZ0019_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmDMMZ0019_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


    }
}
