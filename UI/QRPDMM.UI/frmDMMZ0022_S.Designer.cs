﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0022_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0022_S));
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.uDateTimeUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextJudeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpecName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckStandbyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateStandbyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextSpecCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckFinishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextDurableMatLot = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridChangeDurable = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandbyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatLot = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudeCharge = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckRepairFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelShot = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboShot = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckUsageClearFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboRepairResultCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRepairResultCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFinishDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBoxChange = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInstall = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSpec = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboInstall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxDurableLot = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableLotList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSpec = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEMC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonDelRow = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxFileList = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridFileList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelFilePath = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFilePath = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUse = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchUse = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDurableInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCurUsage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCurUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChangeDurable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboShot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).BeginInit();
            this.uGroupBoxResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChange)).BeginInit();
            this.uGroupBoxChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInstall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableLot)).BeginInit();
            this.uGroupBoxDurableLot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLotList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSpec)).BeginInit();
            this.uGroupBoxSpec.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFileList)).BeginInit();
            this.uGroupBoxFileList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFileList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            this.SuspendLayout();
            // 
            // uDateTimeUsageJudgeDate
            // 
            appearance13.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeUsageJudgeDate.Appearance = appearance13;
            this.uDateTimeUsageJudgeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeUsageJudgeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTimeUsageJudgeDate.Location = new System.Drawing.Point(717, 128);
            this.uDateTimeUsageJudgeDate.Name = "uDateTimeUsageJudgeDate";
            this.uDateTimeUsageJudgeDate.Size = new System.Drawing.Size(86, 21);
            this.uDateTimeUsageJudgeDate.TabIndex = 8;
            // 
            // uTextJudeChargeID
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextJudeChargeID.Appearance = appearance14;
            this.uTextJudeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance15.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextJudeChargeID.ButtonsRight.Add(editorButton1);
            this.uTextJudeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextJudeChargeID.Location = new System.Drawing.Point(717, 152);
            this.uTextJudeChargeID.Name = "uTextJudeChargeID";
            this.uTextJudeChargeID.Size = new System.Drawing.Size(86, 21);
            this.uTextJudeChargeID.TabIndex = 9;
            this.uTextJudeChargeID.ValueChanged += new System.EventHandler(this.uTextJudeChargeID_ValueChanged);
            this.uTextJudeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextJudeChargeID_KeyDown);
            this.uTextJudeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextJudeChargeID_EditorButtonClick);
            // 
            // uTextJudeChargeName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudeChargeName.Appearance = appearance16;
            this.uTextJudeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudeChargeName.Location = new System.Drawing.Point(806, 152);
            this.uTextJudeChargeName.Name = "uTextJudeChargeName";
            this.uTextJudeChargeName.ReadOnly = true;
            this.uTextJudeChargeName.Size = new System.Drawing.Size(86, 21);
            this.uTextJudeChargeName.TabIndex = 4;
            // 
            // uTextSpecName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecName.Appearance = appearance17;
            this.uTextSpecName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecName.Location = new System.Drawing.Point(806, 104);
            this.uTextSpecName.Name = "uTextSpecName";
            this.uTextSpecName.ReadOnly = true;
            this.uTextSpecName.Size = new System.Drawing.Size(86, 21);
            this.uTextSpecName.TabIndex = 4;
            // 
            // uCheckStandbyFlag
            // 
            this.uCheckStandbyFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckStandbyFlag.Location = new System.Drawing.Point(854, 136);
            this.uCheckStandbyFlag.Name = "uCheckStandbyFlag";
            this.uCheckStandbyFlag.Size = new System.Drawing.Size(7, 20);
            this.uCheckStandbyFlag.TabIndex = 83;
            this.uCheckStandbyFlag.Text = "정비대기";
            this.uCheckStandbyFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckStandbyFlag.Visible = false;
            // 
            // uDateStandbyDate
            // 
            this.uDateStandbyDate.DateTime = new System.DateTime(2012, 10, 16, 0, 0, 0, 0);
            this.uDateStandbyDate.Location = new System.Drawing.Point(885, 160);
            this.uDateStandbyDate.Name = "uDateStandbyDate";
            this.uDateStandbyDate.Size = new System.Drawing.Size(17, 21);
            this.uDateStandbyDate.TabIndex = 87;
            this.uDateStandbyDate.Value = new System.DateTime(2012, 10, 16, 0, 0, 0, 0);
            this.uDateStandbyDate.Visible = false;
            // 
            // uTextSpecCode
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecCode.Appearance = appearance18;
            this.uTextSpecCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecCode.Location = new System.Drawing.Point(717, 104);
            this.uTextSpecCode.Name = "uTextSpecCode";
            this.uTextSpecCode.ReadOnly = true;
            this.uTextSpecCode.Size = new System.Drawing.Size(86, 21);
            this.uTextSpecCode.TabIndex = 7;
            // 
            // uCheckFinishFlag
            // 
            this.uCheckFinishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFinishFlag.Location = new System.Drawing.Point(854, 184);
            this.uCheckFinishFlag.Name = "uCheckFinishFlag";
            this.uCheckFinishFlag.Size = new System.Drawing.Size(7, 20);
            this.uCheckFinishFlag.TabIndex = 82;
            this.uCheckFinishFlag.Text = "정비완료";
            this.uCheckFinishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckFinishFlag.Visible = false;
            // 
            // uTextDurableMatLot
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatLot.Appearance = appearance19;
            this.uTextDurableMatLot.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatLot.Location = new System.Drawing.Point(717, 80);
            this.uTextDurableMatLot.Name = "uTextDurableMatLot";
            this.uTextDurableMatLot.ReadOnly = true;
            this.uTextDurableMatLot.Size = new System.Drawing.Size(103, 21);
            this.uTextDurableMatLot.TabIndex = 6;
            // 
            // uTextDurableMatCode
            // 
            appearance76.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance76;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(717, 56);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(103, 21);
            this.uTextDurableMatCode.TabIndex = 6;
            // 
            // uTextEquipCode
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance21;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(717, 32);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(103, 21);
            this.uTextEquipCode.TabIndex = 6;
            // 
            // uGridChangeDurable
            // 
            this.uGridChangeDurable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridChangeDurable.DisplayLayout.Appearance = appearance22;
            this.uGridChangeDurable.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridChangeDurable.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridChangeDurable.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGridChangeDurable.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridChangeDurable.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridChangeDurable.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridChangeDurable.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGridChangeDurable.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridChangeDurable.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridChangeDurable.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridChangeDurable.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridChangeDurable.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridChangeDurable.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridChangeDurable.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridChangeDurable.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridChangeDurable.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridChangeDurable.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridChangeDurable.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridChangeDurable.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridChangeDurable.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridChangeDurable.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridChangeDurable.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridChangeDurable.Location = new System.Drawing.Point(10, 12);
            this.uGridChangeDurable.Name = "uGridChangeDurable";
            this.uGridChangeDurable.Size = new System.Drawing.Size(878, 40);
            this.uGridChangeDurable.TabIndex = 15;
            this.uGridChangeDurable.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridChangeDurable_AfterCellUpdate);
            this.uGridChangeDurable.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridChangeDurable_CellListSelect);
            this.uGridChangeDurable.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridChangeDurable_BeforeCellListDropDown);
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(600, 104);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(113, 20);
            this.uLabelSpec.TabIndex = 3;
            // 
            // uLabelStandbyDate
            // 
            this.uLabelStandbyDate.Location = new System.Drawing.Point(867, 160);
            this.uLabelStandbyDate.Name = "uLabelStandbyDate";
            this.uLabelStandbyDate.Size = new System.Drawing.Size(17, 20);
            this.uLabelStandbyDate.TabIndex = 78;
            this.uLabelStandbyDate.Text = "17";
            this.uLabelStandbyDate.Visible = false;
            // 
            // uLabelDurableMatLot
            // 
            this.uLabelDurableMatLot.Location = new System.Drawing.Point(600, 80);
            this.uLabelDurableMatLot.Name = "uLabelDurableMatLot";
            this.uLabelDurableMatLot.Size = new System.Drawing.Size(113, 20);
            this.uLabelDurableMatLot.TabIndex = 3;
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(600, 56);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(113, 20);
            this.uLabelDurableMatCode.TabIndex = 3;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(600, 32);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(113, 20);
            this.uLabelEquipCode.TabIndex = 3;
            // 
            // uLabelUsageJudgeDate
            // 
            this.uLabelUsageJudgeDate.Location = new System.Drawing.Point(600, 128);
            this.uLabelUsageJudgeDate.Name = "uLabelUsageJudgeDate";
            this.uLabelUsageJudgeDate.Size = new System.Drawing.Size(113, 20);
            this.uLabelUsageJudgeDate.TabIndex = 3;
            // 
            // uLabelJudeCharge
            // 
            this.uLabelJudeCharge.Location = new System.Drawing.Point(600, 152);
            this.uLabelJudeCharge.Name = "uLabelJudeCharge";
            this.uLabelJudeCharge.Size = new System.Drawing.Size(113, 20);
            this.uLabelJudeCharge.TabIndex = 3;
            // 
            // uCheckRepairFlag
            // 
            this.uCheckRepairFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRepairFlag.Location = new System.Drawing.Point(854, 160);
            this.uCheckRepairFlag.Name = "uCheckRepairFlag";
            this.uCheckRepairFlag.Size = new System.Drawing.Size(7, 20);
            this.uCheckRepairFlag.TabIndex = 84;
            this.uCheckRepairFlag.Text = "정비착수";
            this.uCheckRepairFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckRepairFlag.Visible = false;
            // 
            // uLabelShot
            // 
            this.uLabelShot.Location = new System.Drawing.Point(600, 176);
            this.uLabelShot.Name = "uLabelShot";
            this.uLabelShot.Size = new System.Drawing.Size(113, 20);
            this.uLabelShot.TabIndex = 3;
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(867, 184);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(17, 20);
            this.uLabelRepairDate.TabIndex = 77;
            this.uLabelRepairDate.Text = "18";
            this.uLabelRepairDate.Visible = false;
            // 
            // uComboShot
            // 
            this.uComboShot.Location = new System.Drawing.Point(717, 176);
            this.uComboShot.Name = "uComboShot";
            this.uComboShot.Size = new System.Drawing.Size(123, 21);
            this.uComboShot.TabIndex = 10;
            this.uComboShot.ValueChanged += new System.EventHandler(this.uComboShot_ValueChanged);
            // 
            // uDateRepairDate
            // 
            this.uDateRepairDate.DateTime = new System.DateTime(2012, 10, 16, 0, 0, 0, 0);
            this.uDateRepairDate.Location = new System.Drawing.Point(885, 184);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(17, 21);
            this.uDateRepairDate.TabIndex = 86;
            this.uDateRepairDate.Value = new System.DateTime(2012, 10, 16, 0, 0, 0, 0);
            this.uDateRepairDate.Visible = false;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(377, 28);
            this.uTextEtcDesc.Multiline = true;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(199, 44);
            this.uTextEtcDesc.TabIndex = 14;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(278, 28);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(94, 20);
            this.uLabelEtcDesc.TabIndex = 92;
            this.uLabelEtcDesc.Text = "21";
            // 
            // uGroupBoxResult
            // 
            this.uGroupBoxResult.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxResult.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBoxResult.Controls.Add(this.uCheckUsageClearFlag);
            this.uGroupBoxResult.Controls.Add(this.uComboRepairResultCode);
            this.uGroupBoxResult.Controls.Add(this.uLabelRepairResultCode);
            this.uGroupBoxResult.Controls.Add(this.uLabelFinishDate);
            this.uGroupBoxResult.Controls.Add(this.uDateFinishDate);
            this.uGroupBoxResult.Location = new System.Drawing.Point(10, 212);
            this.uGroupBoxResult.Name = "uGroupBoxResult";
            this.uGroupBoxResult.Size = new System.Drawing.Size(583, 96);
            this.uGroupBoxResult.TabIndex = 1;
            // 
            // uCheckUsageClearFlag
            // 
            this.uCheckUsageClearFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUsageClearFlag.Location = new System.Drawing.Point(195, 28);
            this.uCheckUsageClearFlag.Name = "uCheckUsageClearFlag";
            this.uCheckUsageClearFlag.Size = new System.Drawing.Size(75, 20);
            this.uCheckUsageClearFlag.TabIndex = 13;
            this.uCheckUsageClearFlag.Text = "초기화";
            this.uCheckUsageClearFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uComboRepairResultCode
            // 
            this.uComboRepairResultCode.Location = new System.Drawing.Point(106, 52);
            this.uComboRepairResultCode.Name = "uComboRepairResultCode";
            this.uComboRepairResultCode.Size = new System.Drawing.Size(86, 21);
            this.uComboRepairResultCode.TabIndex = 12;
            this.uComboRepairResultCode.ValueChanged += new System.EventHandler(this.uComboRepairResultCode_ValueChanged);
            // 
            // uLabelRepairResultCode
            // 
            this.uLabelRepairResultCode.Location = new System.Drawing.Point(10, 52);
            this.uLabelRepairResultCode.Name = "uLabelRepairResultCode";
            this.uLabelRepairResultCode.Size = new System.Drawing.Size(93, 20);
            this.uLabelRepairResultCode.TabIndex = 81;
            this.uLabelRepairResultCode.Text = "20";
            // 
            // uLabelFinishDate
            // 
            this.uLabelFinishDate.Location = new System.Drawing.Point(10, 28);
            this.uLabelFinishDate.Name = "uLabelFinishDate";
            this.uLabelFinishDate.Size = new System.Drawing.Size(93, 20);
            this.uLabelFinishDate.TabIndex = 79;
            this.uLabelFinishDate.Text = "19";
            // 
            // uDateFinishDate
            // 
            appearance34.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFinishDate.Appearance = appearance34;
            this.uDateFinishDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateFinishDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateFinishDate.Location = new System.Drawing.Point(106, 28);
            this.uDateFinishDate.Name = "uDateFinishDate";
            this.uDateFinishDate.Size = new System.Drawing.Size(86, 21);
            this.uDateFinishDate.TabIndex = 11;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.Enabled = false;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(813, 8);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(75, 20);
            this.uCheckChangeFlag.TabIndex = 90;
            this.uCheckChangeFlag.Text = "교체여부";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.uCheckChangeFlag.Visible = false;
            this.uCheckChangeFlag.CheckedChanged += new System.EventHandler(this.uCheckChangeFlag_CheckedChanged);
            // 
            // uGroupBoxChange
            // 
            this.uGroupBoxChange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxChange.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBoxChange.Controls.Add(this.uGridChangeDurable);
            this.uGroupBoxChange.Location = new System.Drawing.Point(10, 312);
            this.uGroupBoxChange.Name = "uGroupBoxChange";
            this.uGroupBoxChange.Size = new System.Drawing.Size(898, 56);
            this.uGroupBoxChange.TabIndex = 1;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(854, 0);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(27, 21);
            this.uComboSearchPackage.TabIndex = 2;
            this.uComboSearchPackage.Visible = false;
            // 
            // uLabelInstall
            // 
            this.uLabelInstall.Location = new System.Drawing.Point(840, 12);
            this.uLabelInstall.Name = "uLabelInstall";
            this.uLabelInstall.Size = new System.Drawing.Size(17, 20);
            this.uLabelInstall.TabIndex = 14;
            this.uLabelInstall.Visible = false;
            // 
            // uGridSpec
            // 
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSpec.DisplayLayout.Appearance = appearance35;
            this.uGridSpec.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSpec.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.GroupByBox.Appearance = appearance36;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpec.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
            this.uGridSpec.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance38.BackColor2 = System.Drawing.SystemColors.Control;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpec.DisplayLayout.GroupByBox.PromptAppearance = appearance38;
            this.uGridSpec.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSpec.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSpec.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.Highlight;
            appearance40.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSpec.DisplayLayout.Override.ActiveRowAppearance = appearance40;
            this.uGridSpec.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSpec.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.Override.CardAreaAppearance = appearance41;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSpec.DisplayLayout.Override.CellAppearance = appearance42;
            this.uGridSpec.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSpec.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpec.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.uGridSpec.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.uGridSpec.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSpec.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            this.uGridSpec.DisplayLayout.Override.RowAppearance = appearance45;
            this.uGridSpec.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSpec.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.uGridSpec.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSpec.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSpec.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSpec.Location = new System.Drawing.Point(192, 28);
            this.uGridSpec.Name = "uGridSpec";
            this.uGridSpec.Size = new System.Drawing.Size(401, 200);
            this.uGridSpec.TabIndex = 5;
            this.uGridSpec.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridSpec_DoubleClickRow);
            // 
            // uComboInstall
            // 
            this.uComboInstall.Location = new System.Drawing.Point(883, 0);
            this.uComboInstall.Name = "uComboInstall";
            this.uComboInstall.Size = new System.Drawing.Size(34, 21);
            this.uComboInstall.TabIndex = 3;
            this.uComboInstall.Visible = false;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(727, 10);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchPackage.TabIndex = 14;
            this.uLabelSearchPackage.Visible = false;
            // 
            // uComboSearchDurableMatName
            // 
            this.uComboSearchDurableMatName.Location = new System.Drawing.Point(127, 10);
            this.uComboSearchDurableMatName.MaxLength = 50;
            this.uComboSearchDurableMatName.Name = "uComboSearchDurableMatName";
            this.uComboSearchDurableMatName.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchDurableMatName.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(886, 27);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(31, 21);
            this.uComboSearchPlant.TabIndex = 12;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uGroupBoxDurableLot
            // 
            this.uGroupBoxDurableLot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxDurableLot.Controls.Add(this.uGridDurableLotList);
            this.uGroupBoxDurableLot.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxDurableLot.Name = "uGroupBoxDurableLot";
            this.uGroupBoxDurableLot.Size = new System.Drawing.Size(915, 322);
            this.uGroupBoxDurableLot.TabIndex = 5;
            // 
            // uGridDurableLotList
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableLotList.DisplayLayout.Appearance = appearance1;
            this.uGridDurableLotList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableLotList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLotList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridDurableLotList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableLotList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableLotList.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableLotList.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridDurableLotList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableLotList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableLotList.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridDurableLotList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableLotList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLotList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridDurableLotList.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridDurableLotList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableLotList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableLotList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridDurableLotList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableLotList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridDurableLotList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableLotList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableLotList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableLotList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDurableLotList.Location = new System.Drawing.Point(3, 0);
            this.uGridDurableLotList.Name = "uGridDurableLotList";
            this.uGridDurableLotList.Size = new System.Drawing.Size(909, 319);
            this.uGridDurableLotList.TabIndex = 4;
            this.uGridDurableLotList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableLotList_DoubleClickCell);
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(113, 20);
            this.uLabelSearchDurableMatName.TabIndex = 10;
            // 
            // uGroupBoxSpec
            // 
            this.uGroupBoxSpec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxSpec.Controls.Add(this.uTextPackage);
            this.uGroupBoxSpec.Controls.Add(this.uTextEMC);
            this.uGroupBoxSpec.Controls.Add(this.uButtonDelRow);
            this.uGroupBoxSpec.Controls.Add(this.uGroupBoxFileList);
            this.uGroupBoxSpec.Controls.Add(this.uGroupBoxResult);
            this.uGroupBoxSpec.Controls.Add(this.uGridEquipList);
            this.uGroupBoxSpec.Controls.Add(this.uDateTimeUsageJudgeDate);
            this.uGroupBoxSpec.Controls.Add(this.uTextJudeChargeID);
            this.uGroupBoxSpec.Controls.Add(this.uTextJudeChargeName);
            this.uGroupBoxSpec.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxSpec.Controls.Add(this.uTextSpecName);
            this.uGroupBoxSpec.Controls.Add(this.uCheckStandbyFlag);
            this.uGroupBoxSpec.Controls.Add(this.uDateStandbyDate);
            this.uGroupBoxSpec.Controls.Add(this.uTextSpecCode);
            this.uGroupBoxSpec.Controls.Add(this.uCheckFinishFlag);
            this.uGroupBoxSpec.Controls.Add(this.uTextDurableMatLot);
            this.uGroupBoxSpec.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBoxSpec.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelSpec);
            this.uGroupBoxSpec.Controls.Add(this.uLabelStandbyDate);
            this.uGroupBoxSpec.Controls.Add(this.uLabelDurableMatLot);
            this.uGroupBoxSpec.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelEquipCode);
            this.uGroupBoxSpec.Controls.Add(this.uLabelUsageJudgeDate);
            this.uGroupBoxSpec.Controls.Add(this.uLabelJudeCharge);
            this.uGroupBoxSpec.Controls.Add(this.uCheckRepairFlag);
            this.uGroupBoxSpec.Controls.Add(this.uLabelFilePath);
            this.uGroupBoxSpec.Controls.Add(this.uLabelShot);
            this.uGroupBoxSpec.Controls.Add(this.uLabelRepairDate);
            this.uGroupBoxSpec.Controls.Add(this.uComboShot);
            this.uGroupBoxSpec.Controls.Add(this.uDateRepairDate);
            this.uGroupBoxSpec.Controls.Add(this.uGroupBoxChange);
            this.uGroupBoxSpec.Controls.Add(this.uGridSpec);
            this.uGroupBoxSpec.Controls.Add(this.uTextFilePath);
            this.uGroupBoxSpec.Location = new System.Drawing.Point(0, 405);
            this.uGroupBoxSpec.Name = "uGroupBoxSpec";
            this.uGroupBoxSpec.Size = new System.Drawing.Size(915, 439);
            this.uGroupBoxSpec.TabIndex = 6;
            // 
            // uTextEMC
            // 
            appearance77.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEMC.Appearance = appearance77;
            this.uTextEMC.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEMC.Location = new System.Drawing.Point(823, 56);
            this.uTextEMC.Name = "uTextEMC";
            this.uTextEMC.ReadOnly = true;
            this.uTextEMC.Size = new System.Drawing.Size(89, 21);
            this.uTextEMC.TabIndex = 94;
            // 
            // uButtonDelRow
            // 
            this.uButtonDelRow.Location = new System.Drawing.Point(717, 200);
            this.uButtonDelRow.Name = "uButtonDelRow";
            this.uButtonDelRow.Size = new System.Drawing.Size(75, 28);
            this.uButtonDelRow.TabIndex = 93;
            this.uButtonDelRow.Text = "행 삭제";
            // 
            // uGroupBoxFileList
            // 
            this.uGroupBoxFileList.Controls.Add(this.uGridFileList);
            this.uGroupBoxFileList.Location = new System.Drawing.Point(593, 228);
            this.uGroupBoxFileList.Name = "uGroupBoxFileList";
            this.uGroupBoxFileList.Size = new System.Drawing.Size(315, 80);
            this.uGroupBoxFileList.TabIndex = 90;
            // 
            // uGridFileList
            // 
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFileList.DisplayLayout.Appearance = appearance47;
            this.uGridFileList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFileList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.GroupByBox.Appearance = appearance48;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFileList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance49;
            this.uGridFileList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance50.BackColor2 = System.Drawing.SystemColors.Control;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFileList.DisplayLayout.GroupByBox.PromptAppearance = appearance50;
            this.uGridFileList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFileList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFileList.DisplayLayout.Override.ActiveCellAppearance = appearance51;
            appearance52.BackColor = System.Drawing.SystemColors.Highlight;
            appearance52.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFileList.DisplayLayout.Override.ActiveRowAppearance = appearance52;
            this.uGridFileList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFileList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.Override.CardAreaAppearance = appearance53;
            appearance54.BorderColor = System.Drawing.Color.Silver;
            appearance54.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFileList.DisplayLayout.Override.CellAppearance = appearance54;
            this.uGridFileList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFileList.DisplayLayout.Override.CellPadding = 0;
            appearance55.BackColor = System.Drawing.SystemColors.Control;
            appearance55.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance55.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance55.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.Override.GroupByRowAppearance = appearance55;
            appearance56.TextHAlignAsString = "Left";
            this.uGridFileList.DisplayLayout.Override.HeaderAppearance = appearance56;
            this.uGridFileList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFileList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            appearance57.BorderColor = System.Drawing.Color.Silver;
            this.uGridFileList.DisplayLayout.Override.RowAppearance = appearance57;
            this.uGridFileList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFileList.DisplayLayout.Override.TemplateAddRowAppearance = appearance58;
            this.uGridFileList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFileList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFileList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFileList.Location = new System.Drawing.Point(3, 0);
            this.uGridFileList.Name = "uGridFileList";
            this.uGridFileList.Size = new System.Drawing.Size(309, 77);
            this.uGridFileList.TabIndex = 0;
            this.uGridFileList.Text = "ultraGrid1";
            // 
            // uGridEquipList
            // 
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance59;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance60;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance61;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance62.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance62.BackColor2 = System.Drawing.SystemColors.Control;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance62;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance63;
            appearance64.BackColor = System.Drawing.SystemColors.Highlight;
            appearance64.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance64;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance65;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            appearance66.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance66;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance67.BackColor = System.Drawing.SystemColors.Control;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance67;
            appearance68.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance68;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance69;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance70.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance70;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(10, 28);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(178, 200);
            this.uGridEquipList.TabIndex = 88;
            // 
            // uLabelFilePath
            // 
            this.uLabelFilePath.Location = new System.Drawing.Point(600, 200);
            this.uLabelFilePath.Name = "uLabelFilePath";
            this.uLabelFilePath.Size = new System.Drawing.Size(113, 20);
            this.uLabelFilePath.TabIndex = 3;
            // 
            // uTextFilePath
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFilePath.Appearance = appearance72;
            this.uTextFilePath.BackColor = System.Drawing.Color.Gainsboro;
            appearance73.Image = global::QRPDMM.UI.Properties.Resources.btn_Fileupload;
            appearance73.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance73;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "UP";
            appearance74.Image = global::QRPDMM.UI.Properties.Resources.btn_Filedownload;
            appearance74.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance74;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Key = "DOWN";
            editorButton3.Visible = false;
            this.uTextFilePath.ButtonsRight.Add(editorButton2);
            this.uTextFilePath.ButtonsRight.Add(editorButton3);
            this.uTextFilePath.Location = new System.Drawing.Point(717, 200);
            this.uTextFilePath.Name = "uTextFilePath";
            this.uTextFilePath.ReadOnly = true;
            this.uTextFilePath.Size = new System.Drawing.Size(175, 21);
            this.uTextFilePath.TabIndex = 89;
            this.uTextFilePath.Visible = false;
            this.uTextFilePath.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextFilePath_KeyDown);
            this.uTextFilePath.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFilePath_EditorButtonClick);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(775, 10);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(14, 20);
            this.uLabelSearchPlant.TabIndex = 11;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance75;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUse);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUse);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDurableInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableInventory);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboInstall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCurUsage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelInstall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCurUsage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 40);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uTextUse
            // 
            this.uTextUse.Location = new System.Drawing.Point(783, 10);
            this.uTextUse.Name = "uTextUse";
            this.uTextUse.Size = new System.Drawing.Size(101, 21);
            this.uTextUse.TabIndex = 122;
            // 
            // uLabelSearchUse
            // 
            this.uLabelSearchUse.Location = new System.Drawing.Point(683, 11);
            this.uLabelSearchUse.Name = "uLabelSearchUse";
            this.uLabelSearchUse.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchUse.TabIndex = 121;
            // 
            // uComboSearchDurableInventory
            // 
            this.uComboSearchDurableInventory.Location = new System.Drawing.Point(377, 10);
            this.uComboSearchDurableInventory.MaxLength = 50;
            this.uComboSearchDurableInventory.Name = "uComboSearchDurableInventory";
            this.uComboSearchDurableInventory.Size = new System.Drawing.Size(110, 21);
            this.uComboSearchDurableInventory.TabIndex = 120;
            // 
            // uLabelSearchDurableInventory
            // 
            this.uLabelSearchDurableInventory.Location = new System.Drawing.Point(261, 12);
            this.uLabelSearchDurableInventory.Name = "uLabelSearchDurableInventory";
            this.uLabelSearchDurableInventory.Size = new System.Drawing.Size(113, 20);
            this.uLabelSearchDurableInventory.TabIndex = 119;
            // 
            // uComboSearchCurUsage
            // 
            this.uComboSearchCurUsage.Location = new System.Drawing.Point(586, 10);
            this.uComboSearchCurUsage.Name = "uComboSearchCurUsage";
            this.uComboSearchCurUsage.Size = new System.Drawing.Size(86, 21);
            this.uComboSearchCurUsage.TabIndex = 2;
            // 
            // uLabelSearchCurUsage
            // 
            this.uLabelSearchCurUsage.Location = new System.Drawing.Point(497, 12);
            this.uLabelSearchCurUsage.Name = "uLabelSearchCurUsage";
            this.uLabelSearchCurUsage.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCurUsage.TabIndex = 14;
            // 
            // uTextPlantCode
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance71;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(882, 28);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(10, 21);
            this.uTextPlantCode.TabIndex = 4;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextPackage
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance20;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(823, 79);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(89, 21);
            this.uTextPackage.TabIndex = 95;
            // 
            // frmDMMZ0022_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxDurableLot);
            this.Controls.Add(this.uGroupBoxSpec);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0022_S";
            this.Text = "frmDMMZ0022_S";
            this.Load += new System.EventHandler(this.frmDMMZ0022_S_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0022_S_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0022_S_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMMZ0022_S_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridChangeDurable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboShot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).EndInit();
            this.uGroupBoxResult.ResumeLayout(false);
            this.uGroupBoxResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChange)).EndInit();
            this.uGroupBoxChange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInstall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableLot)).EndInit();
            this.uGroupBoxDurableLot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLotList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSpec)).EndInit();
            this.uGroupBoxSpec.ResumeLayout(false);
            this.uGroupBoxSpec.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFileList)).EndInit();
            this.uGroupBoxFileList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFileList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudeChargeID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudeChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecName;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStandbyFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFinishFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatLot;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridChangeDurable;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelStandbyDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatLot;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelJudeCharge;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRepairFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelShot;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboShot;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxResult;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUsageClearFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFinishDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxChange;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelInstall;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSpec;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInstall;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDurableLot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableLotList;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDurableInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableInventory;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFilePath;
        private Infragistics.Win.Misc.UltraLabel uLabelFilePath;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCurUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCurUsage;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxFileList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFileList;
        private Infragistics.Win.Misc.UltraButton uButtonDelRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUse;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUse;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEMC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
    }
}