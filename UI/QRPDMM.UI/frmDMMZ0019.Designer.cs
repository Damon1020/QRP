﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0019
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0019));
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInfo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRegister = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckUsageClearFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboRepairResultCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRepairResultCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.uTextEtcDesc2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateFinishDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRepairDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateStandbyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckFinishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelNote1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFinishDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckRepairFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckStandbyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelStandbyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRepairGICode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInputCurUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCumUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelJudgeChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uTextJudgeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageJudgeCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCurUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimitUpper = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimit = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimitLower = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCurUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageLimitUpper = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudgeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageLimit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUsageLimitLower = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextChangeUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboUsageJudgeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInputCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitLower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUsageJudgeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBox.Appearance = appearance21;
            this.uGroupBox.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBox.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBox.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBox.Controls.Add(this.uComboPlant);
            this.uGroupBox.Controls.Add(this.uLabelSearchEquip);
            this.uGroupBox.Controls.Add(this.uLabelPlant);
            this.uGroupBox.Location = new System.Drawing.Point(0, 40);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBox.TabIndex = 1;
            // 
            // uTextSearchEquipName
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance26;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(556, 12);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchEquipName.TabIndex = 10;
            // 
            // uTextSearchEquipCode
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchEquipCode.Appearance = appearance1;
            this.uTextSearchEquipCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(432, 12);
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchEquipCode.TabIndex = 2;
            this.uTextSearchEquipCode.ValueChanged += new System.EventHandler(this.uTextSearchEquipCode_ValueChanged);
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance30;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelSearchEquip
            // 
            this.uLabelSearchEquip.Location = new System.Drawing.Point(328, 12);
            this.uLabelSearchEquip.Name = "uLabelSearchEquip";
            this.uLabelSearchEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquip.TabIndex = 3;
            this.uLabelSearchEquip.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 4;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uDateWriteDate);
            this.uGroupBox1.Controls.Add(this.uTextWriteName);
            this.uGroupBox1.Controls.Add(this.uLabelInfo);
            this.uGroupBox1.Controls.Add(this.uLabelRegister);
            this.uGroupBox1.Controls.Add(this.uLabelRegisterDate);
            this.uGroupBox1.Controls.Add(this.uTextWriteID);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1070, 76);
            this.uGroupBox1.TabIndex = 2;
            // 
            // uDateWriteDate
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance29;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(116, 24);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 3;
            // 
            // uTextWriteName
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance31;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(535, 28);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 5;
            // 
            // uLabelInfo
            // 
            this.uLabelInfo.Location = new System.Drawing.Point(12, 52);
            this.uLabelInfo.Name = "uLabelInfo";
            this.uLabelInfo.Size = new System.Drawing.Size(100, 20);
            this.uLabelInfo.TabIndex = 9;
            this.uLabelInfo.Text = "ultraLabel2";
            // 
            // uLabelRegister
            // 
            this.uLabelRegister.Location = new System.Drawing.Point(328, 28);
            this.uLabelRegister.Name = "uLabelRegister";
            this.uLabelRegister.Size = new System.Drawing.Size(100, 20);
            this.uLabelRegister.TabIndex = 9;
            this.uLabelRegister.Text = "ultraLabel2";
            // 
            // uLabelRegisterDate
            // 
            this.uLabelRegisterDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelRegisterDate.Name = "uLabelRegisterDate";
            this.uLabelRegisterDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRegisterDate.TabIndex = 9;
            this.uLabelRegisterDate.Text = "ultraLabel2";
            // 
            // uTextWriteID
            // 
            appearance16.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance16.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance16;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton2);
            this.uTextWriteID.Location = new System.Drawing.Point(431, 28);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 4;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 610);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 230);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 610);
            this.uGroupBoxContentsArea.TabIndex = 12;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 590);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uCheckUsageClearFlag);
            this.uGroupBox3.Controls.Add(this.uCheckChangeFlag);
            this.uGroupBox3.Controls.Add(this.uComboRepairResultCode);
            this.uGroupBox3.Controls.Add(this.uLabelRepairResultCode);
            this.uGroupBox3.Controls.Add(this.ultraButton3);
            this.uGroupBox3.Controls.Add(this.ultraButton2);
            this.uGroupBox3.Controls.Add(this.ultraButton1);
            this.uGroupBox3.Controls.Add(this.uTextEtcDesc2);
            this.uGroupBox3.Controls.Add(this.uDateFinishDate);
            this.uGroupBox3.Controls.Add(this.uDateRepairDate);
            this.uGroupBox3.Controls.Add(this.uDateStandbyDate);
            this.uGroupBox3.Controls.Add(this.uCheckFinishFlag);
            this.uGroupBox3.Controls.Add(this.uLabelNote1);
            this.uGroupBox3.Controls.Add(this.uLabelFinishDate);
            this.uGroupBox3.Controls.Add(this.uCheckRepairFlag);
            this.uGroupBox3.Controls.Add(this.uLabelRepairDate);
            this.uGroupBox3.Controls.Add(this.uCheckStandbyFlag);
            this.uGroupBox3.Controls.Add(this.uLabelStandbyDate);
            this.uGroupBox3.Location = new System.Drawing.Point(536, 12);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(520, 568);
            this.uGroupBox3.TabIndex = 1;
            // 
            // uCheckUsageClearFlag
            // 
            this.uCheckUsageClearFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckUsageClearFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckUsageClearFlag.Location = new System.Drawing.Point(412, 52);
            this.uCheckUsageClearFlag.Name = "uCheckUsageClearFlag";
            this.uCheckUsageClearFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckUsageClearFlag.TabIndex = 16;
            this.uCheckUsageClearFlag.Text = "초기화";
            this.uCheckUsageClearFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uCheckChangeFlag
            // 
            this.uCheckChangeFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckChangeFlag.Location = new System.Drawing.Point(412, 28);
            this.uCheckChangeFlag.Name = "uCheckChangeFlag";
            this.uCheckChangeFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckChangeFlag.TabIndex = 79;
            this.uCheckChangeFlag.Text = "교체여부";
            this.uCheckChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            // 
            // uComboRepairResultCode
            // 
            this.uComboRepairResultCode.Location = new System.Drawing.Point(412, 76);
            this.uComboRepairResultCode.Name = "uComboRepairResultCode";
            this.uComboRepairResultCode.Size = new System.Drawing.Size(100, 21);
            this.uComboRepairResultCode.TabIndex = 17;
            this.uComboRepairResultCode.ValueChanged += new System.EventHandler(this.uComboRepairResultCode_ValueChanged);
            // 
            // uLabelRepairResultCode
            // 
            this.uLabelRepairResultCode.Location = new System.Drawing.Point(316, 76);
            this.uLabelRepairResultCode.Name = "uLabelRepairResultCode";
            this.uLabelRepairResultCode.Size = new System.Drawing.Size(92, 20);
            this.uLabelRepairResultCode.TabIndex = 77;
            this.uLabelRepairResultCode.Text = "20";
            // 
            // ultraButton3
            // 
            this.ultraButton3.Location = new System.Drawing.Point(324, 316);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(120, 28);
            this.ultraButton3.TabIndex = 20;
            this.ultraButton3.Text = "ultraButton1";
            this.ultraButton3.Click += new System.EventHandler(this.ultraButton3_Click);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(200, 316);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(120, 28);
            this.ultraButton2.TabIndex = 19;
            this.ultraButton2.Text = "ultraButton1";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(76, 316);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(120, 28);
            this.ultraButton1.TabIndex = 18;
            this.ultraButton1.Text = "ultraButton1";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // uTextEtcDesc2
            // 
            this.uTextEtcDesc2.Location = new System.Drawing.Point(12, 124);
            this.uTextEtcDesc2.Multiline = true;
            this.uTextEtcDesc2.Name = "uTextEtcDesc2";
            this.uTextEtcDesc2.Size = new System.Drawing.Size(496, 180);
            this.uTextEtcDesc2.TabIndex = 12;
            // 
            // uDateFinishDate
            // 
            this.uDateFinishDate.Location = new System.Drawing.Point(204, 76);
            this.uDateFinishDate.Name = "uDateFinishDate";
            this.uDateFinishDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFinishDate.TabIndex = 15;
            // 
            // uDateRepairDate
            // 
            this.uDateRepairDate.Location = new System.Drawing.Point(204, 52);
            this.uDateRepairDate.Name = "uDateRepairDate";
            this.uDateRepairDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairDate.TabIndex = 13;
            // 
            // uDateStandbyDate
            // 
            this.uDateStandbyDate.Location = new System.Drawing.Point(204, 28);
            this.uDateStandbyDate.Name = "uDateStandbyDate";
            this.uDateStandbyDate.Size = new System.Drawing.Size(100, 21);
            this.uDateStandbyDate.TabIndex = 11;
            // 
            // uCheckFinishFlag
            // 
            this.uCheckFinishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFinishFlag.Location = new System.Drawing.Point(12, 76);
            this.uCheckFinishFlag.Name = "uCheckFinishFlag";
            this.uCheckFinishFlag.Size = new System.Drawing.Size(84, 20);
            this.uCheckFinishFlag.TabIndex = 14;
            this.uCheckFinishFlag.Text = "정비완료";
            this.uCheckFinishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckFinishFlag.CheckedChanged += new System.EventHandler(this.uCheckFinishFlag_CheckedChanged);
            // 
            // uLabelNote1
            // 
            this.uLabelNote1.Location = new System.Drawing.Point(12, 100);
            this.uLabelNote1.Name = "uLabelNote1";
            this.uLabelNote1.Size = new System.Drawing.Size(84, 20);
            this.uLabelNote1.TabIndex = 9;
            this.uLabelNote1.Text = "ultraLabel4";
            // 
            // uLabelFinishDate
            // 
            this.uLabelFinishDate.Location = new System.Drawing.Point(100, 76);
            this.uLabelFinishDate.Name = "uLabelFinishDate";
            this.uLabelFinishDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelFinishDate.TabIndex = 9;
            this.uLabelFinishDate.Text = "ultraLabel4";
            // 
            // uCheckRepairFlag
            // 
            this.uCheckRepairFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRepairFlag.Location = new System.Drawing.Point(12, 52);
            this.uCheckRepairFlag.Name = "uCheckRepairFlag";
            this.uCheckRepairFlag.Size = new System.Drawing.Size(84, 20);
            this.uCheckRepairFlag.TabIndex = 12;
            this.uCheckRepairFlag.Text = "정비착수";
            this.uCheckRepairFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckRepairFlag.CheckedChanged += new System.EventHandler(this.uCheckRepairFlag_CheckedChanged);
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(100, 52);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRepairDate.TabIndex = 9;
            this.uLabelRepairDate.Text = "ultraLabel4";
            // 
            // uCheckStandbyFlag
            // 
            this.uCheckStandbyFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckStandbyFlag.Location = new System.Drawing.Point(12, 28);
            this.uCheckStandbyFlag.Name = "uCheckStandbyFlag";
            this.uCheckStandbyFlag.Size = new System.Drawing.Size(84, 20);
            this.uCheckStandbyFlag.TabIndex = 10;
            this.uCheckStandbyFlag.Text = "정비대기";
            this.uCheckStandbyFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckStandbyFlag.CheckedChanged += new System.EventHandler(this.uCheckStandbyFlag_CheckedChanged);
            // 
            // uLabelStandbyDate
            // 
            this.uLabelStandbyDate.Location = new System.Drawing.Point(100, 28);
            this.uLabelStandbyDate.Name = "uLabelStandbyDate";
            this.uLabelStandbyDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStandbyDate.TabIndex = 9;
            this.uLabelStandbyDate.Text = "ultraLabel4";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uTextRepairGICode);
            this.uGroupBox2.Controls.Add(this.uLabelLotNo);
            this.uGroupBox2.Controls.Add(this.uTextLotNo);
            this.uGroupBox2.Controls.Add(this.uTextRepairCode);
            this.uGroupBox2.Controls.Add(this.uTextInputCurUsage);
            this.uGroupBox2.Controls.Add(this.ultraLabel1);
            this.uGroupBox2.Controls.Add(this.uTextEquipCode);
            this.uGroupBox2.Controls.Add(this.uTextCumUsage);
            this.uGroupBox2.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBox2.Controls.Add(this.uTextPlantCode);
            this.uGroupBox2.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox2.Controls.Add(this.uDateUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uLabelJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeName);
            this.uGroupBox2.Controls.Add(this.uLabelUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox2.Controls.Add(this.uLabelUsageJudgeCode);
            this.uGroupBox2.Controls.Add(this.uLabelCurUsage);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimitUpper);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimit);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimitLower);
            this.uGroupBox2.Controls.Add(this.uTextCurUsage);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimitUpper);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimit);
            this.uGroupBox2.Controls.Add(this.uLabelChangeUsage);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimitLower);
            this.uGroupBox2.Controls.Add(this.uTextChangeUsage);
            this.uGroupBox2.Controls.Add(this.uComboUsageJudgeCode);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(520, 568);
            this.uGroupBox2.TabIndex = 0;
            // 
            // uTextRepairGICode
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Appearance = appearance47;
            this.uTextRepairGICode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Location = new System.Drawing.Point(292, 384);
            this.uTextRepairGICode.Name = "uTextRepairGICode";
            this.uTextRepairGICode.ReadOnly = true;
            this.uTextRepairGICode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairGICode.TabIndex = 110;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(260, 76);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelLotNo.TabIndex = 108;
            this.uLabelLotNo.Text = "ultraLabel3";
            // 
            // uTextLotNo
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance37;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Location = new System.Drawing.Point(376, 76);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(110, 21);
            this.uTextLotNo.TabIndex = 26;
            // 
            // uTextRepairCode
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairCode.Appearance = appearance22;
            this.uTextRepairCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairCode.Location = new System.Drawing.Point(404, 384);
            this.uTextRepairCode.Name = "uTextRepairCode";
            this.uTextRepairCode.ReadOnly = true;
            this.uTextRepairCode.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairCode.TabIndex = 107;
            // 
            // uTextInputCurUsage
            // 
            this.uTextInputCurUsage.Location = new System.Drawing.Point(376, 28);
            this.uTextInputCurUsage.Name = "uTextInputCurUsage";
            this.uTextInputCurUsage.Size = new System.Drawing.Size(56, 21);
            this.uTextInputCurUsage.TabIndex = 6;
            this.uTextInputCurUsage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextInputCurUsage_KeyPress);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(436, 32);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 105;
            this.ultraLabel1.Text = "/";
            // 
            // uTextEquipCode
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance50;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(352, 360);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(40, 21);
            this.uTextEquipCode.TabIndex = 104;
            // 
            // uTextCumUsage
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Appearance = appearance51;
            this.uTextCumUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Location = new System.Drawing.Point(308, 360);
            this.uTextCumUsage.Name = "uTextCumUsage";
            this.uTextCumUsage.ReadOnly = true;
            this.uTextCumUsage.Size = new System.Drawing.Size(40, 21);
            this.uTextCumUsage.TabIndex = 103;
            // 
            // uTextDurableMatCode
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance46;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(204, 360);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatCode.TabIndex = 102;
            // 
            // uTextPlantCode
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance3;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(404, 360);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 99;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(12, 172);
            this.uTextEtcDesc.Multiline = true;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(496, 180);
            this.uTextEtcDesc.TabIndex = 12;
            // 
            // uDateUsageJudgeDate
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUsageJudgeDate.Appearance = appearance2;
            this.uDateUsageJudgeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUsageJudgeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateUsageJudgeDate.Location = new System.Drawing.Point(376, 100);
            this.uDateUsageJudgeDate.Name = "uDateUsageJudgeDate";
            this.uDateUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateUsageJudgeDate.TabIndex = 8;
            // 
            // uLabelJudgeChargeID
            // 
            this.uLabelJudgeChargeID.Location = new System.Drawing.Point(12, 124);
            this.uLabelJudgeChargeID.Name = "uLabelJudgeChargeID";
            this.uLabelJudgeChargeID.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudgeChargeID.TabIndex = 9;
            this.uLabelJudgeChargeID.Text = "ultraLabel3";
            // 
            // uTextJudgeChargeName
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Appearance = appearance34;
            this.uTextJudgeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Location = new System.Drawing.Point(232, 123);
            this.uTextJudgeChargeName.Name = "uTextJudgeChargeName";
            this.uTextJudgeChargeName.ReadOnly = true;
            this.uTextJudgeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeName.TabIndex = 28;
            // 
            // uLabelUsageJudgeDate
            // 
            this.uLabelUsageJudgeDate.Location = new System.Drawing.Point(260, 100);
            this.uLabelUsageJudgeDate.Name = "uLabelUsageJudgeDate";
            this.uLabelUsageJudgeDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageJudgeDate.TabIndex = 9;
            this.uLabelUsageJudgeDate.Text = "ultraLabel3";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 148);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 9;
            this.uLabelEtcDesc.Text = "ultraLabel3";
            // 
            // uLabelUsageJudgeCode
            // 
            this.uLabelUsageJudgeCode.Location = new System.Drawing.Point(12, 100);
            this.uLabelUsageJudgeCode.Name = "uLabelUsageJudgeCode";
            this.uLabelUsageJudgeCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageJudgeCode.TabIndex = 9;
            this.uLabelUsageJudgeCode.Text = "ultraLabel3";
            // 
            // uLabelCurUsage
            // 
            this.uLabelCurUsage.Location = new System.Drawing.Point(260, 28);
            this.uLabelCurUsage.Name = "uLabelCurUsage";
            this.uLabelCurUsage.Size = new System.Drawing.Size(110, 20);
            this.uLabelCurUsage.TabIndex = 9;
            this.uLabelCurUsage.Text = "ultraLabel3";
            // 
            // uLabelUsageLimitUpper
            // 
            this.uLabelUsageLimitUpper.Location = new System.Drawing.Point(12, 76);
            this.uLabelUsageLimitUpper.Name = "uLabelUsageLimitUpper";
            this.uLabelUsageLimitUpper.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimitUpper.TabIndex = 9;
            this.uLabelUsageLimitUpper.Text = "ultraLabel3";
            // 
            // uLabelUsageLimit
            // 
            this.uLabelUsageLimit.Location = new System.Drawing.Point(260, 52);
            this.uLabelUsageLimit.Name = "uLabelUsageLimit";
            this.uLabelUsageLimit.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimit.TabIndex = 9;
            this.uLabelUsageLimit.Text = "ultraLabel3";
            // 
            // uLabelUsageLimitLower
            // 
            this.uLabelUsageLimitLower.Location = new System.Drawing.Point(12, 52);
            this.uLabelUsageLimitLower.Name = "uLabelUsageLimitLower";
            this.uLabelUsageLimitLower.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimitLower.TabIndex = 9;
            this.uLabelUsageLimitLower.Text = "ultraLabel3";
            // 
            // uTextCurUsage
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Appearance = appearance5;
            this.uTextCurUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Location = new System.Drawing.Point(452, 28);
            this.uTextCurUsage.Name = "uTextCurUsage";
            this.uTextCurUsage.ReadOnly = true;
            this.uTextCurUsage.Size = new System.Drawing.Size(56, 21);
            this.uTextCurUsage.TabIndex = 22;
            // 
            // uTextUsageLimitUpper
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitUpper.Appearance = appearance32;
            this.uTextUsageLimitUpper.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitUpper.Location = new System.Drawing.Point(128, 76);
            this.uTextUsageLimitUpper.Name = "uTextUsageLimitUpper";
            this.uTextUsageLimitUpper.ReadOnly = true;
            this.uTextUsageLimitUpper.Size = new System.Drawing.Size(110, 21);
            this.uTextUsageLimitUpper.TabIndex = 25;
            // 
            // uTextJudgeChargeID
            // 
            appearance35.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextJudgeChargeID.Appearance = appearance35;
            this.uTextJudgeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance36.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance36;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextJudgeChargeID.ButtonsRight.Add(editorButton3);
            this.uTextJudgeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextJudgeChargeID.Location = new System.Drawing.Point(128, 123);
            this.uTextJudgeChargeID.Name = "uTextJudgeChargeID";
            this.uTextJudgeChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeID.TabIndex = 9;
            this.uTextJudgeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextJudgeChargeID_KeyDown);
            this.uTextJudgeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uTextUsageLimit
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimit.Appearance = appearance6;
            this.uTextUsageLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimit.Location = new System.Drawing.Point(376, 52);
            this.uTextUsageLimit.Name = "uTextUsageLimit";
            this.uTextUsageLimit.ReadOnly = true;
            this.uTextUsageLimit.Size = new System.Drawing.Size(110, 21);
            this.uTextUsageLimit.TabIndex = 24;
            // 
            // uLabelChangeUsage
            // 
            this.uLabelChangeUsage.Location = new System.Drawing.Point(12, 28);
            this.uLabelChangeUsage.Name = "uLabelChangeUsage";
            this.uLabelChangeUsage.Size = new System.Drawing.Size(110, 20);
            this.uLabelChangeUsage.TabIndex = 9;
            this.uLabelChangeUsage.Text = "ultraLabel3";
            // 
            // uTextUsageLimitLower
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitLower.Appearance = appearance38;
            this.uTextUsageLimitLower.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitLower.Location = new System.Drawing.Point(128, 52);
            this.uTextUsageLimitLower.Name = "uTextUsageLimitLower";
            this.uTextUsageLimitLower.ReadOnly = true;
            this.uTextUsageLimitLower.Size = new System.Drawing.Size(110, 21);
            this.uTextUsageLimitLower.TabIndex = 23;
            // 
            // uTextChangeUsage
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeUsage.Appearance = appearance33;
            this.uTextChangeUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeUsage.Location = new System.Drawing.Point(128, 27);
            this.uTextChangeUsage.Name = "uTextChangeUsage";
            this.uTextChangeUsage.ReadOnly = true;
            this.uTextChangeUsage.Size = new System.Drawing.Size(110, 21);
            this.uTextChangeUsage.TabIndex = 21;
            // 
            // uComboUsageJudgeCode
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboUsageJudgeCode.Appearance = appearance20;
            this.uComboUsageJudgeCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboUsageJudgeCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboUsageJudgeCode.Location = new System.Drawing.Point(128, 99);
            this.uComboUsageJudgeCode.Name = "uComboUsageJudgeCode";
            this.uComboUsageJudgeCode.Size = new System.Drawing.Size(112, 21);
            this.uComboUsageJudgeCode.TabIndex = 7;
            this.uComboUsageJudgeCode.Text = "ultraComboEditor1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance7;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance11;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            appearance14.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance15.BackColor = System.Drawing.SystemColors.Control;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance15;
            appearance17.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance18;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance19;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 156);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 692);
            this.uGrid1.TabIndex = 11;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // frmDMMZ0019
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0019";
            this.Load += new System.EventHandler(this.frmDMMZ0019_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0019_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0019_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMMZ0019_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUsageClearFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepairResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinishDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateStandbyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRepairFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStandbyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInputCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitLower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUsageJudgeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeName;
        private Infragistics.Win.Misc.UltraLabel uLabelRegister;
        private Infragistics.Win.Misc.UltraLabel uLabelRegisterDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeID;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraLabel uLabelInfo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelJudgeChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCurUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimitLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCurUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimitLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeUsage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUsageJudgeCode;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFinishFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelNote1;
        private Infragistics.Win.Misc.UltraLabel uLabelFinishDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRepairFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStandbyFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelStandbyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimitUpper;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimitUpper;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimit;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUsageClearFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckChangeFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepairResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResultCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCumUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInputCurUsage;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairCode;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGICode;
    }
}