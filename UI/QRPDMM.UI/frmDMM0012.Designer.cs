﻿namespace QRPDMM.UI
{
    partial class frmDMM0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMM0012));
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDurableMat = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelCumUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCumUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimitUpper = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimit = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageLimitLower = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCurUsage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboUsageJudgeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextChangeUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateUsageJudgeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextUsageLimitUpper = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageLimit = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUsageLimitLower = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudgeChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCurUsage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextJudgeChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelJudgeChargeID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageJudgeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUsageJudgeCode = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUsageJudgeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsageJudgeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitLower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance7;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDurableMatCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMat);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 10;
            // 
            // uComboPlant
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance10;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 37;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uTextSearchDurableMatName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDurableMatName.Appearance = appearance4;
            this.uTextSearchDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDurableMatName.Location = new System.Drawing.Point(676, 10);
            this.uTextSearchDurableMatName.Name = "uTextSearchDurableMatName";
            this.uTextSearchDurableMatName.ReadOnly = true;
            this.uTextSearchDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMatName.TabIndex = 12;
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(561, 10);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMatName.TabIndex = 11;
            this.uLabelSearchDurableMatName.Text = "2";
            // 
            // uTextSearchDurableMatCode
            // 
            appearance28.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance28;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDurableMatCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchDurableMatCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchDurableMatCode.Location = new System.Drawing.Point(420, 10);
            this.uTextSearchDurableMatCode.Name = "uTextSearchDurableMatCode";
            this.uTextSearchDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDurableMatCode.TabIndex = 10;
            this.uTextSearchDurableMatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchDurableMatCode_KeyDown);
            this.uTextSearchDurableMatCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDurableMatCode_EditorButtonClick);
            // 
            // uLabelSearchDurableMat
            // 
            this.uLabelSearchDurableMat.Location = new System.Drawing.Point(305, 12);
            this.uLabelSearchDurableMat.Name = "uLabelSearchDurableMat";
            this.uLabelSearchDurableMat.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchDurableMat.TabIndex = 9;
            this.uLabelSearchDurableMat.Text = "1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 9;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance9;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance31;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 15;
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 16;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uLabelCumUsage);
            this.uGroupBox2.Controls.Add(this.uTextCumUsage);
            this.uGroupBox2.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox2.Controls.Add(this.uLabelChangeUsage);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimitUpper);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimit);
            this.uGroupBox2.Controls.Add(this.uLabelUsageLimitLower);
            this.uGroupBox2.Controls.Add(this.uLabelCurUsage);
            this.uGroupBox2.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox2.Controls.Add(this.uComboUsageJudgeCode);
            this.uGroupBox2.Controls.Add(this.uTextChangeUsage);
            this.uGroupBox2.Controls.Add(this.uDateUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimitUpper);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimit);
            this.uGroupBox2.Controls.Add(this.uTextUsageLimitLower);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeName);
            this.uGroupBox2.Controls.Add(this.uTextCurUsage);
            this.uGroupBox2.Controls.Add(this.uTextJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uLabelJudgeChargeID);
            this.uGroupBox2.Controls.Add(this.uLabelUsageJudgeDate);
            this.uGroupBox2.Controls.Add(this.uLabelUsageJudgeCode);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 96);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 592);
            this.uGroupBox2.TabIndex = 48;
            // 
            // uLabelCumUsage
            // 
            this.uLabelCumUsage.Location = new System.Drawing.Point(484, 28);
            this.uLabelCumUsage.Name = "uLabelCumUsage";
            this.uLabelCumUsage.Size = new System.Drawing.Size(110, 20);
            this.uLabelCumUsage.TabIndex = 48;
            this.uLabelCumUsage.Text = "13";
            // 
            // uTextCumUsage
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Appearance = appearance11;
            this.uTextCumUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCumUsage.Location = new System.Drawing.Point(600, 28);
            this.uTextCumUsage.Name = "uTextCumUsage";
            this.uTextCumUsage.ReadOnly = true;
            this.uTextCumUsage.Size = new System.Drawing.Size(100, 21);
            this.uTextCumUsage.TabIndex = 47;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(128, 100);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(796, 21);
            this.uTextEtcDesc.TabIndex = 46;
            this.uTextEtcDesc.Text = "ultraTextEditor15";
            // 
            // uLabelChangeUsage
            // 
            this.uLabelChangeUsage.Location = new System.Drawing.Point(12, 28);
            this.uLabelChangeUsage.Name = "uLabelChangeUsage";
            this.uLabelChangeUsage.Size = new System.Drawing.Size(110, 20);
            this.uLabelChangeUsage.TabIndex = 36;
            this.uLabelChangeUsage.Text = "11";
            // 
            // uLabelUsageLimitUpper
            // 
            this.uLabelUsageLimitUpper.Location = new System.Drawing.Point(484, 52);
            this.uLabelUsageLimitUpper.Name = "uLabelUsageLimitUpper";
            this.uLabelUsageLimitUpper.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimitUpper.TabIndex = 37;
            this.uLabelUsageLimitUpper.Text = "12";
            // 
            // uLabelUsageLimit
            // 
            this.uLabelUsageLimit.Location = new System.Drawing.Point(248, 52);
            this.uLabelUsageLimit.Name = "uLabelUsageLimit";
            this.uLabelUsageLimit.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimit.TabIndex = 37;
            this.uLabelUsageLimit.Text = "12";
            // 
            // uLabelUsageLimitLower
            // 
            this.uLabelUsageLimitLower.Location = new System.Drawing.Point(12, 52);
            this.uLabelUsageLimitLower.Name = "uLabelUsageLimitLower";
            this.uLabelUsageLimitLower.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageLimitLower.TabIndex = 37;
            this.uLabelUsageLimitLower.Text = "12";
            // 
            // uLabelCurUsage
            // 
            this.uLabelCurUsage.Location = new System.Drawing.Point(248, 28);
            this.uLabelCurUsage.Name = "uLabelCurUsage";
            this.uLabelCurUsage.Size = new System.Drawing.Size(110, 20);
            this.uLabelCurUsage.TabIndex = 38;
            this.uLabelCurUsage.Text = "13";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 100);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 42;
            this.uLabelEtcDesc.Text = "19";
            // 
            // uComboUsageJudgeCode
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboUsageJudgeCode.Appearance = appearance2;
            this.uComboUsageJudgeCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboUsageJudgeCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboUsageJudgeCode.Location = new System.Drawing.Point(128, 76);
            this.uComboUsageJudgeCode.Name = "uComboUsageJudgeCode";
            this.uComboUsageJudgeCode.Size = new System.Drawing.Size(100, 21);
            this.uComboUsageJudgeCode.TabIndex = 38;
            this.uComboUsageJudgeCode.Text = "ultraComboEditor1";
            // 
            // uTextChangeUsage
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeUsage.Appearance = appearance13;
            this.uTextChangeUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeUsage.Location = new System.Drawing.Point(128, 28);
            this.uTextChangeUsage.Name = "uTextChangeUsage";
            this.uTextChangeUsage.ReadOnly = true;
            this.uTextChangeUsage.Size = new System.Drawing.Size(100, 21);
            this.uTextChangeUsage.TabIndex = 38;
            // 
            // uDateUsageJudgeDate
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUsageJudgeDate.Appearance = appearance5;
            this.uDateUsageJudgeDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateUsageJudgeDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateUsageJudgeDate.Location = new System.Drawing.Point(364, 76);
            this.uDateUsageJudgeDate.Name = "uDateUsageJudgeDate";
            this.uDateUsageJudgeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateUsageJudgeDate.TabIndex = 45;
            // 
            // uTextUsageLimitUpper
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitUpper.Appearance = appearance12;
            this.uTextUsageLimitUpper.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitUpper.Location = new System.Drawing.Point(600, 52);
            this.uTextUsageLimitUpper.Name = "uTextUsageLimitUpper";
            this.uTextUsageLimitUpper.ReadOnly = true;
            this.uTextUsageLimitUpper.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageLimitUpper.TabIndex = 38;
            // 
            // uTextUsageLimit
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimit.Appearance = appearance33;
            this.uTextUsageLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimit.Location = new System.Drawing.Point(364, 52);
            this.uTextUsageLimit.Name = "uTextUsageLimit";
            this.uTextUsageLimit.ReadOnly = true;
            this.uTextUsageLimit.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageLimit.TabIndex = 38;
            // 
            // uTextUsageLimitLower
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitLower.Appearance = appearance34;
            this.uTextUsageLimitLower.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUsageLimitLower.Location = new System.Drawing.Point(128, 52);
            this.uTextUsageLimitLower.Name = "uTextUsageLimitLower";
            this.uTextUsageLimitLower.ReadOnly = true;
            this.uTextUsageLimitLower.Size = new System.Drawing.Size(100, 21);
            this.uTextUsageLimitLower.TabIndex = 38;
            this.uTextUsageLimitLower.ValueChanged += new System.EventHandler(this.ultraTextEditor11_ValueChanged);
            // 
            // uTextJudgeChargeName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Appearance = appearance15;
            this.uTextJudgeChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextJudgeChargeName.Location = new System.Drawing.Point(704, 76);
            this.uTextJudgeChargeName.Name = "uTextJudgeChargeName";
            this.uTextJudgeChargeName.ReadOnly = true;
            this.uTextJudgeChargeName.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeName.TabIndex = 44;
            // 
            // uTextCurUsage
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Appearance = appearance35;
            this.uTextCurUsage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCurUsage.Location = new System.Drawing.Point(364, 28);
            this.uTextCurUsage.Name = "uTextCurUsage";
            this.uTextCurUsage.ReadOnly = true;
            this.uTextCurUsage.Size = new System.Drawing.Size(100, 21);
            this.uTextCurUsage.TabIndex = 38;
            // 
            // uTextJudgeChargeID
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextJudgeChargeID.Appearance = appearance6;
            this.uTextJudgeChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance1.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance1;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextJudgeChargeID.ButtonsRight.Add(editorButton2);
            this.uTextJudgeChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextJudgeChargeID.Location = new System.Drawing.Point(600, 76);
            this.uTextJudgeChargeID.Name = "uTextJudgeChargeID";
            this.uTextJudgeChargeID.Size = new System.Drawing.Size(100, 21);
            this.uTextJudgeChargeID.TabIndex = 43;
            this.uTextJudgeChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextJudgeChargeID_KeyDown);
            this.uTextJudgeChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextJudgeChargeID_EditorButtonClick);
            // 
            // uLabelJudgeChargeID
            // 
            this.uLabelJudgeChargeID.Location = new System.Drawing.Point(484, 76);
            this.uLabelJudgeChargeID.Name = "uLabelJudgeChargeID";
            this.uLabelJudgeChargeID.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudgeChargeID.TabIndex = 40;
            this.uLabelJudgeChargeID.Text = "18";
            // 
            // uLabelUsageJudgeDate
            // 
            this.uLabelUsageJudgeDate.Location = new System.Drawing.Point(248, 76);
            this.uLabelUsageJudgeDate.Name = "uLabelUsageJudgeDate";
            this.uLabelUsageJudgeDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageJudgeDate.TabIndex = 41;
            this.uLabelUsageJudgeDate.Text = "17";
            // 
            // uLabelUsageJudgeCode
            // 
            this.uLabelUsageJudgeCode.Location = new System.Drawing.Point(12, 76);
            this.uLabelUsageJudgeCode.Name = "uLabelUsageJudgeCode";
            this.uLabelUsageJudgeCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelUsageJudgeCode.TabIndex = 41;
            this.uLabelUsageJudgeCode.Text = "16";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uLabelLotNo);
            this.uGroupBox1.Controls.Add(this.uTextLotNo);
            this.uGroupBox1.Controls.Add(this.uTextPlantCode);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Controls.Add(this.uLabelDurableMatName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uTextPlantName);
            this.uGroupBox1.Controls.Add(this.uTextSpec);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatName);
            this.uGroupBox1.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 80);
            this.uGroupBox1.TabIndex = 47;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(716, 28);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelLotNo.TabIndex = 40;
            this.uLabelLotNo.Text = "5";
            // 
            // uTextLotNo
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance20;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Location = new System.Drawing.Point(832, 28);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextLotNo.TabIndex = 41;
            // 
            // uTextPlantCode
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance8;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(876, 52);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 39;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 20;
            this.uLabelPlant.Text = "3";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(248, 52);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipCode.TabIndex = 25;
            this.uLabelEquipCode.Text = "8";
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(248, 28);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatCode.TabIndex = 21;
            this.uLabelDurableMatCode.Text = "4";
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 52);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 23;
            this.uLabelSpec.Text = "6";
            // 
            // uLabelDurableMatName
            // 
            this.uLabelDurableMatName.Location = new System.Drawing.Point(484, 28);
            this.uLabelDurableMatName.Name = "uLabelDurableMatName";
            this.uLabelDurableMatName.Size = new System.Drawing.Size(110, 20);
            this.uLabelDurableMatName.TabIndex = 22;
            this.uLabelDurableMatName.Text = "5";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(484, 52);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipName.TabIndex = 34;
            this.uLabelEquipName.Text = "9";
            // 
            // uTextPlantName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance16;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(128, 28);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantName.TabIndex = 38;
            // 
            // uTextSpec
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance18;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(128, 52);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextSpec.TabIndex = 38;
            // 
            // uTextEquipName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance3;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(600, 52);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipName.TabIndex = 38;
            // 
            // uTextDurableMatName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance27;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(600, 28);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 38;
            // 
            // uTextDurableMatCode
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance17;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(364, 28);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatCode.TabIndex = 38;
            // 
            // uTextEquipCode
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance14;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(364, 52);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 38;
            // 
            // frmDMM0012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMM0012";
            this.Load += new System.EventHandler(this.frmDMM0012_Load);
            this.Activated += new System.EventHandler(this.frmDMM0012_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMM0012_FormClosing);
            this.Resize += new System.EventHandler(this.frmDMM0012_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCumUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUsageJudgeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateUsageJudgeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUsageLimitLower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCurUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextJudgeChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMat;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimitLower;
        private Infragistics.Win.Misc.UltraLabel uLabelCurUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUsageJudgeCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeUsage;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateUsageJudgeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimitLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCurUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextJudgeChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelJudgeChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageJudgeCode;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimitUpper;
        private Infragistics.Win.Misc.UltraLabel uLabelUsageLimit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimitUpper;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUsageLimit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCumUsage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCumUsage;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;

    }
}