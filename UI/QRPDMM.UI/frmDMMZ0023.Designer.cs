﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0023));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMat = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateTimeTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateTimeFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurable = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxRepairList = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridRepairList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairList)).BeginInit();
            this.uGroupBoxRepairList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDurableMat);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateTimeTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateTimeFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDurable);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextLotNo
            // 
            this.uTextLotNo.Location = new System.Drawing.Point(732, 12);
            this.uTextLotNo.MaxLength = 40;
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextLotNo.TabIndex = 4;
            // 
            // uTextDurableMat
            // 
            this.uTextDurableMat.Location = new System.Drawing.Point(940, 12);
            this.uTextDurableMat.MaxLength = 20;
            this.uTextDurableMat.Name = "uTextDurableMat";
            this.uTextDurableMat.Size = new System.Drawing.Size(12, 21);
            this.uTextDurableMat.TabIndex = 3;
            this.uTextDurableMat.Visible = false;
            // 
            // uDateTimeTo
            // 
            this.uDateTimeTo.Location = new System.Drawing.Point(228, 12);
            this.uDateTimeTo.Name = "uDateTimeTo";
            this.uDateTimeTo.Size = new System.Drawing.Size(100, 21);
            this.uDateTimeTo.TabIndex = 2;
            // 
            // uDateTimeFrom
            // 
            this.uDateTimeFrom.Location = new System.Drawing.Point(116, 12);
            this.uDateTimeFrom.Name = "uDateTimeFrom";
            this.uDateTimeFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateTimeFrom.TabIndex = 2;
            // 
            // uLabel
            // 
            this.uLabel.Location = new System.Drawing.Point(216, 16);
            this.uLabel.Name = "uLabel";
            this.uLabel.Size = new System.Drawing.Size(12, 8);
            this.uLabel.TabIndex = 1;
            this.uLabel.Text = "~";
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(628, 12);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotNo.TabIndex = 1;
            // 
            // uLabelDurable
            // 
            this.uLabelDurable.Location = new System.Drawing.Point(916, 12);
            this.uLabelDurable.Name = "uLabelDurable";
            this.uLabelDurable.Size = new System.Drawing.Size(20, 20);
            this.uLabelDurable.TabIndex = 1;
            this.uLabelDurable.Visible = false;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 1;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(844, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(8, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(852, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(52, 21);
            this.uComboSearchPlant.TabIndex = 0;
            this.uComboSearchPlant.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxRepairList
            // 
            this.uGroupBoxRepairList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxRepairList.Controls.Add(this.uGridRepairList);
            this.uGroupBoxRepairList.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxRepairList.Name = "uGroupBoxRepairList";
            this.uGroupBoxRepairList.Size = new System.Drawing.Size(1070, 760);
            this.uGroupBoxRepairList.TabIndex = 2;
            // 
            // uGridRepairList
            // 
            this.uGridRepairList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepairList.DisplayLayout.Appearance = appearance5;
            this.uGridRepairList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepairList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridRepairList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepairList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridRepairList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepairList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepairList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepairList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridRepairList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepairList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepairList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepairList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridRepairList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepairList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepairList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridRepairList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridRepairList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepairList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepairList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridRepairList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepairList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridRepairList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepairList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepairList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepairList.Location = new System.Drawing.Point(12, 12);
            this.uGridRepairList.Name = "uGridRepairList";
            this.uGridRepairList.Size = new System.Drawing.Size(1044, 740);
            this.uGridRepairList.TabIndex = 0;
            // 
            // uComboSearchDurableMatName
            // 
            this.uComboSearchDurableMatName.Location = new System.Drawing.Point(472, 10);
            this.uComboSearchDurableMatName.MaxLength = 50;
            this.uComboSearchDurableMatName.Name = "uComboSearchDurableMatName";
            this.uComboSearchDurableMatName.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchDurableMatName.TabIndex = 11;
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(336, 12);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(132, 20);
            this.uLabelSearchDurableMatName.TabIndex = 12;
            this.uLabelSearchDurableMatName.Text = "금형치공구명";
            // 
            // frmDMMZ0023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxRepairList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0023";
            this.Load += new System.EventHandler(this.frmDMMZ0023_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0023_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDMMZ0023_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairList)).EndInit();
            this.uGroupBoxRepairList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepairList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeFrom;
        private Infragistics.Win.Misc.UltraLabel uLabel;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRepairList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepairList;
        private Infragistics.Win.Misc.UltraLabel uLabelDurable;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMat;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
    }
}