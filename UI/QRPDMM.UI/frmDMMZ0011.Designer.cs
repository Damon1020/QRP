﻿namespace QRPDMM.UI
{
    partial class frmDMMZ0011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDMMZ0011));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelRepairReleaseCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairRequestNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBoxRepairResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEquipStopType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipStopType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProblem_measure = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProblem_measure = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelRepairReleaseRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairReleaseRequestDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextRepairRealeaseRequestUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepairRealeaseRequestUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRepairReleaseRequestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUnusual = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepairReleaseComposition = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairResult)).BeginInit();
            this.uGroupBoxRepairResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblem_measure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReleaseRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRealeaseRequestUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRealeaseRequestUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnusual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uDateSearchToDate
            // 
            appearance44.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDate.Appearance = appearance44;
            this.uDateSearchToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDate.Location = new System.Drawing.Point(236, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 5;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(216, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(20, 16);
            this.ultraLabel1.TabIndex = 4;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            appearance43.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDate.Appearance = appearance43;
            this.uDateSearchFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 3;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 2;
            this.uLabelSearchDate.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(448, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(344, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance6;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 4;
            this.uGrid1.Text = "ultraGrid1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxRepairResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabelEquipName);
            this.uGroupBox1.Controls.Add(this.uLabelEquipCode);
            this.uGroupBox1.Controls.Add(this.uTextVendor);
            this.uGroupBox1.Controls.Add(this.uLabelVendor);
            this.uGroupBox1.Controls.Add(this.uTextSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Controls.Add(this.uTextMoldName);
            this.uGroupBox1.Controls.Add(this.uLabelMoldName);
            this.uGroupBox1.Controls.Add(this.uTextMoldCode);
            this.uGroupBox1.Controls.Add(this.uLabelMoldCode);
            this.uGroupBox1.Controls.Add(this.uTextPlant);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uTextRepairRequestNum);
            this.uGroupBox1.Controls.Add(this.uLabelRepairReleaseCode);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1048, 88);
            this.uGroupBox1.TabIndex = 0;
            // 
            // uLabelRepairReleaseCode
            // 
            this.uLabelRepairReleaseCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelRepairReleaseCode.Name = "uLabelRepairReleaseCode";
            this.uLabelRepairReleaseCode.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairReleaseCode.TabIndex = 3;
            this.uLabelRepairReleaseCode.Text = "ultraLabel1";
            // 
            // uTextRepairRequestNum
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestNum.Appearance = appearance21;
            this.uTextRepairRequestNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRequestNum.Location = new System.Drawing.Point(148, 12);
            this.uTextRepairRequestNum.Name = "uTextRepairRequestNum";
            this.uTextRepairRequestNum.ReadOnly = true;
            this.uTextRepairRequestNum.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairRequestNum.TabIndex = 16;
            // 
            // uTextPlant
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance20;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(444, 12);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 18;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(308, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(130, 20);
            this.uLabelPlant.TabIndex = 17;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextMoldCode
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldCode.Appearance = appearance19;
            this.uTextMoldCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldCode.Location = new System.Drawing.Point(148, 36);
            this.uTextMoldCode.Name = "uTextMoldCode";
            this.uTextMoldCode.ReadOnly = true;
            this.uTextMoldCode.Size = new System.Drawing.Size(150, 21);
            this.uTextMoldCode.TabIndex = 20;
            // 
            // uLabelMoldCode
            // 
            this.uLabelMoldCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelMoldCode.Name = "uLabelMoldCode";
            this.uLabelMoldCode.Size = new System.Drawing.Size(130, 20);
            this.uLabelMoldCode.TabIndex = 19;
            this.uLabelMoldCode.Text = "ultraLabel1";
            // 
            // uTextMoldName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldName.Appearance = appearance18;
            this.uTextMoldName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldName.Location = new System.Drawing.Point(444, 36);
            this.uTextMoldName.Name = "uTextMoldName";
            this.uTextMoldName.ReadOnly = true;
            this.uTextMoldName.Size = new System.Drawing.Size(150, 21);
            this.uTextMoldName.TabIndex = 22;
            // 
            // uLabelMoldName
            // 
            this.uLabelMoldName.Location = new System.Drawing.Point(308, 36);
            this.uLabelMoldName.Name = "uLabelMoldName";
            this.uLabelMoldName.Size = new System.Drawing.Size(130, 20);
            this.uLabelMoldName.TabIndex = 21;
            this.uLabelMoldName.Text = "ultraLabel1";
            // 
            // uTextSpec
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance17;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(740, 12);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(300, 21);
            this.uTextSpec.TabIndex = 24;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(604, 12);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(130, 20);
            this.uLabelSpec.TabIndex = 23;
            this.uLabelSpec.Text = "ultraLabel1";
            // 
            // uTextVendor
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance16;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(740, 36);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(300, 21);
            this.uTextVendor.TabIndex = 26;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(604, 36);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(130, 20);
            this.uLabelVendor.TabIndex = 25;
            this.uLabelVendor.Text = "ultraLabel1";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(308, 60);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipName.TabIndex = 28;
            this.uLabelEquipName.Text = "ultraLabel1";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(12, 60);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipCode.TabIndex = 27;
            this.uLabelEquipCode.Text = "ultraLabel1";
            // 
            // uTextEquipName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance23;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(444, 60);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipName.TabIndex = 30;
            // 
            // uTextEquipCode
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance15;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(148, 60);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipCode.TabIndex = 29;
            // 
            // uGroupBoxRepairResult
            // 
            this.uGroupBoxRepairResult.Controls.Add(this.uTextProblem_measure);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelProblem_measure);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextEquipStopType);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelEquipStopType);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairResult);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelRepairResult);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairUserName);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairUserID);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelRepairUser);
            this.uGroupBoxRepairResult.Controls.Add(this.uTextRepairDate);
            this.uGroupBoxRepairResult.Controls.Add(this.uLabelRepairDate);
            this.uGroupBoxRepairResult.Location = new System.Drawing.Point(12, 104);
            this.uGroupBoxRepairResult.Name = "uGroupBoxRepairResult";
            this.uGroupBoxRepairResult.Size = new System.Drawing.Size(1048, 80);
            this.uGroupBoxRepairResult.TabIndex = 1;
            // 
            // uTextEquipStopType
            // 
            this.uTextEquipStopType.Location = new System.Drawing.Point(148, 52);
            this.uTextEquipStopType.Name = "uTextEquipStopType";
            this.uTextEquipStopType.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipStopType.TabIndex = 36;
            // 
            // uLabelEquipStopType
            // 
            this.uLabelEquipStopType.Location = new System.Drawing.Point(12, 52);
            this.uLabelEquipStopType.Name = "uLabelEquipStopType";
            this.uLabelEquipStopType.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipStopType.TabIndex = 35;
            this.uLabelEquipStopType.Text = "ultraLabel1";
            // 
            // uTextRepairResult
            // 
            this.uTextRepairResult.Location = new System.Drawing.Point(792, 28);
            this.uTextRepairResult.Name = "uTextRepairResult";
            this.uTextRepairResult.Size = new System.Drawing.Size(150, 21);
            this.uTextRepairResult.TabIndex = 34;
            // 
            // uLabelRepairResult
            // 
            this.uLabelRepairResult.Location = new System.Drawing.Point(656, 28);
            this.uLabelRepairResult.Name = "uLabelRepairResult";
            this.uLabelRepairResult.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairResult.TabIndex = 33;
            this.uLabelRepairResult.Text = "ultraLabel1";
            // 
            // uTextRepairUserName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Appearance = appearance40;
            this.uTextRepairUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairUserName.Location = new System.Drawing.Point(545, 28);
            this.uTextRepairUserName.Name = "uTextRepairUserName";
            this.uTextRepairUserName.ReadOnly = true;
            this.uTextRepairUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserName.TabIndex = 32;
            // 
            // uTextRepairUserID
            // 
            this.uTextRepairUserID.Location = new System.Drawing.Point(444, 28);
            this.uTextRepairUserID.Name = "uTextRepairUserID";
            this.uTextRepairUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairUserID.TabIndex = 31;
            // 
            // uLabelRepairUser
            // 
            this.uLabelRepairUser.Location = new System.Drawing.Point(308, 28);
            this.uLabelRepairUser.Name = "uLabelRepairUser";
            this.uLabelRepairUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairUser.TabIndex = 30;
            this.uLabelRepairUser.Text = "ultraLabel1";
            // 
            // uTextRepairDate
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDate.Appearance = appearance33;
            this.uTextRepairDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairDate.Location = new System.Drawing.Point(148, 28);
            this.uTextRepairDate.Name = "uTextRepairDate";
            this.uTextRepairDate.ReadOnly = true;
            this.uTextRepairDate.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairDate.TabIndex = 29;
            // 
            // uLabelRepairDate
            // 
            this.uLabelRepairDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelRepairDate.Name = "uLabelRepairDate";
            this.uLabelRepairDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairDate.TabIndex = 28;
            this.uLabelRepairDate.Text = "ultraLabel1";
            // 
            // uTextProblem_measure
            // 
            this.uTextProblem_measure.Location = new System.Drawing.Point(444, 52);
            this.uTextProblem_measure.Name = "uTextProblem_measure";
            this.uTextProblem_measure.Size = new System.Drawing.Size(499, 21);
            this.uTextProblem_measure.TabIndex = 38;
            // 
            // uLabelProblem_measure
            // 
            this.uLabelProblem_measure.Location = new System.Drawing.Point(308, 52);
            this.uLabelProblem_measure.Name = "uLabelProblem_measure";
            this.uLabelProblem_measure.Size = new System.Drawing.Size(130, 20);
            this.uLabelProblem_measure.TabIndex = 37;
            this.uLabelProblem_measure.Text = "ultraLabel1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uButtonDelete);
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Controls.Add(this.uLabelRepairReleaseComposition);
            this.uGroupBox2.Controls.Add(this.uTextUnusual);
            this.uGroupBox2.Controls.Add(this.uLabelUnusual);
            this.uGroupBox2.Controls.Add(this.uTextRepairRealeaseRequestUserName);
            this.uGroupBox2.Controls.Add(this.uTextRepairRealeaseRequestUserID);
            this.uGroupBox2.Controls.Add(this.uLabelRepairReleaseRequestUser);
            this.uGroupBox2.Controls.Add(this.uDateRepairReleaseRequestDate);
            this.uGroupBox2.Controls.Add(this.uLabelRepairReleaseRequestDate);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 188);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1048, 504);
            this.uGroupBox2.TabIndex = 2;
            // 
            // uLabelRepairReleaseRequestDate
            // 
            this.uLabelRepairReleaseRequestDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelRepairReleaseRequestDate.Name = "uLabelRepairReleaseRequestDate";
            this.uLabelRepairReleaseRequestDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairReleaseRequestDate.TabIndex = 36;
            this.uLabelRepairReleaseRequestDate.Text = "ultraLabel1";
            // 
            // uDateRepairReleaseRequestDate
            // 
            this.uDateRepairReleaseRequestDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRepairReleaseRequestDate.Location = new System.Drawing.Point(148, 12);
            this.uDateRepairReleaseRequestDate.Name = "uDateRepairReleaseRequestDate";
            this.uDateRepairReleaseRequestDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairReleaseRequestDate.TabIndex = 37;
            // 
            // uTextRepairRealeaseRequestUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRealeaseRequestUserName.Appearance = appearance29;
            this.uTextRepairRealeaseRequestUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairRealeaseRequestUserName.Location = new System.Drawing.Point(493, 12);
            this.uTextRepairRealeaseRequestUserName.Name = "uTextRepairRealeaseRequestUserName";
            this.uTextRepairRealeaseRequestUserName.ReadOnly = true;
            this.uTextRepairRealeaseRequestUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairRealeaseRequestUserName.TabIndex = 40;
            // 
            // uTextRepairRealeaseRequestUserID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepairRealeaseRequestUserID.Appearance = appearance3;
            this.uTextRepairRealeaseRequestUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPDMM.UI.Properties.Resources.btn_Zoom;
            appearance4.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRepairRealeaseRequestUserID.ButtonsRight.Add(editorButton1);
            this.uTextRepairRealeaseRequestUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRepairRealeaseRequestUserID.Location = new System.Drawing.Point(392, 12);
            this.uTextRepairRealeaseRequestUserID.Name = "uTextRepairRealeaseRequestUserID";
            this.uTextRepairRealeaseRequestUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRepairRealeaseRequestUserID.TabIndex = 39;
            // 
            // uLabelRepairReleaseRequestUser
            // 
            this.uLabelRepairReleaseRequestUser.Location = new System.Drawing.Point(256, 12);
            this.uLabelRepairReleaseRequestUser.Name = "uLabelRepairReleaseRequestUser";
            this.uLabelRepairReleaseRequestUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairReleaseRequestUser.TabIndex = 38;
            this.uLabelRepairReleaseRequestUser.Text = "ultraLabel1";
            // 
            // uTextUnusual
            // 
            this.uTextUnusual.Location = new System.Drawing.Point(148, 36);
            this.uTextUnusual.Name = "uTextUnusual";
            this.uTextUnusual.Size = new System.Drawing.Size(445, 21);
            this.uTextUnusual.TabIndex = 42;
            // 
            // uLabelUnusual
            // 
            this.uLabelUnusual.Location = new System.Drawing.Point(12, 36);
            this.uLabelUnusual.Name = "uLabelUnusual";
            this.uLabelUnusual.Size = new System.Drawing.Size(130, 20);
            this.uLabelUnusual.TabIndex = 41;
            this.uLabelUnusual.Text = "ultraLabel1";
            // 
            // uLabelRepairReleaseComposition
            // 
            this.uLabelRepairReleaseComposition.Location = new System.Drawing.Point(12, 60);
            this.uLabelRepairReleaseComposition.Name = "uLabelRepairReleaseComposition";
            this.uLabelRepairReleaseComposition.Size = new System.Drawing.Size(130, 20);
            this.uLabelRepairReleaseComposition.TabIndex = 43;
            this.uLabelRepairReleaseComposition.Text = "ultraLabel1";
            // 
            // uGrid2
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance25;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance36;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance39.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance39;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance41;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance42;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 116);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1024, 380);
            this.uGrid2.TabIndex = 44;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 84);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 45;
            this.uButtonDelete.Text = "ultraButton1";
            // 
            // frmDMMZ0011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDMMZ0011";
            this.Load += new System.EventHandler(this.frmDMMZ0011_Load);
            this.Activated += new System.EventHandler(this.frmDMMZ0011_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRequestNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRepairResult)).EndInit();
            this.uGroupBoxRepairResult.ResumeLayout(false);
            this.uGroupBoxRepairResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipStopType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblem_measure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReleaseRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRealeaseRequestUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairRealeaseRequestUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnusual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairReleaseCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairRequestNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldName;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipStopType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipStopType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairResult;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProblem_measure;
        private Infragistics.Win.Misc.UltraLabel uLabelProblem_measure;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairReleaseRequestDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairReleaseRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairRealeaseRequestUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairRealeaseRequestUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairReleaseRequestUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraLabel uLabelRepairReleaseComposition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelUnusual;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
    }
}