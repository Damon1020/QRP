﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 금형치공구관리                                        */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMMZ0013.cs                                        */
/* 프로그램명   : 기초재고등록                                          */
/* 작성자       : 이종호, 코딩 : 남현식                                 */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Infragistics.Win.UltraWinGrid;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0013 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmDMMZ0013()
        {
            InitializeComponent();
        }

        private void frmDMMZ0013_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmDMMZ0013_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정

            titleArea.mfSetLabelText("기초재고등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method 
            SetToolAuth();
            InitText();
            InitLabel();
            InitComboBox(); 
            InitGrid(); 
            InitButton();
            

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMMZ0013_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInventory, "입고창고", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRegisterDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMoveChargeID, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextMoveChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextMoveChargeName.Text = m_resSys.GetString("SYS_USERNAME");
                // 값 초기화

                this.uDateMoveDate.Value = DateTime.Now;

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                //this.uComboInventory.Text = "선택";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                #region Header
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Default
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0,false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Spec", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "SerialFlag", "Serial", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "F");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Qty", "현재고량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");


                //grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatNameCh", "SparePart명_중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatNameEn", "SparePart명_영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Vendor", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Model", "모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Min", "최소재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Right,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid1, 0, "Max", "최대재고량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Right
                //    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region Lot
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableGRD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Default
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "LotNo", "LOT NO", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableGRD, 0, "DeleteFlag", "DeleteFlag", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 100, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");
                #endregion

                #region DropDown Binding

                ////DataTable dtUseFlag = new DataTable();

                ////dtUseFlag.Columns.Add("Key", typeof(String));
                ////dtUseFlag.Columns.Add("Value", typeof(String));

                ////DataRow row1 = dtUseFlag.NewRow();
                ////row1["Key"] = "T";
                ////row1["Value"] = "사용함";
                ////dtUseFlag.Rows.Add(row1);

                ////DataRow row2 = dtUseFlag.NewRow();
                ////row2["Key"] = "F";
                ////row2["Value"] = "사용안함";
                ////dtUseFlag.Rows.Add(row2);

                ////wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "SerialFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "SerialFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadUnit("", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);

                uGroupBoxLot.Expanded = false;
                #endregion

                // Font Size
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //채널연결

                wGrid.mfAddRowGrid(uGrid1, 0);

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONT"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(uButtonOK, "확인", m_resSys.GetString("SYS_FON"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                //DataTable dtSearch = new DataTable();
                //dtSearch.Columns.Add("PlantCode");
                //dtSearch.Columns.Add("PlantName");
                //dtSearch.Columns.Add("DurableMatCode");
                //dtSearch.Columns.Add("DurableMatName");
                //dtSearch.Columns.Add("Spec");
                //dtSearch.Columns.Add("SerialFlag");
                //dtSearch.Columns.Add("Qty");
                //dtSearch.Columns.Add("UnitCode");

                //DataRow drSearch = dtSearch.NewRow();
                //drSearch["PlantCode"] = "1";
                //drSearch["PlantName"] = "PlantName";
                //drSearch["DurableMatCode"] = "1";
                //drSearch["DurableMatName"] = "DurableMatName";
                //drSearch["Spec"] = "Spec";
                //drSearch["SerialFlag"] = "T";
                //drSearch["Qty"] = "5";
                //drSearch["UnitCode"] = "EA";

                //dtSearch.Rows.Add(drSearch);

                //uGrid1.DataSource = dtSearch;
                //uGrid1.DataBind();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtDurableStock = new DataTable();
                DataTable dtDurableStockMoveHist = new DataTable();
                DataTable dtSaveType = new DataTable();
                DataTable dtDelType = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();
                int intRowCheck = 0;

                #region 필수입력사항
                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                if (this.uDateMoveDate.Value == null || this.uDateMoveDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000384", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uDateMoveDate.DropDown();
                    return;
                }

                if (this.uTextMoveChargeID.Text.Equals(string.Empty) || this.uTextMoveChargeName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000386", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextMoveChargeID.Focus();
                    return;
                }

                if (this.uComboInventory.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000870", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboInventory.DropDown();
                    return;
                }

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;
                            string strQty = uGrid1.Rows[i].Cells["Qty"].Value.ToString();
                            if (this.uGrid1.Rows[i].Cells["Qty"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid1.Rows[i].RowSelectorNumber + "M000510", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["Qty"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", this.uGrid1.Rows[i].RowSelectorNumber + "M000499", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["UnitCode"];
                                this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("T"))
                            {
                                int intCtn = 0;
                                string strPlantCode = this.uComboPlant.Value.ToString();
                                string strDurableMatCode = uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();

                                for (int j = 0; j < uGridDurableGRD.Rows.Count; j++)
                                {
                                    //if (uGridDurableGRD.Rows[j].Cells["PlantCode"].Value.ToString().Equals(strPlantCode) &&
                                    //    uGridDurableGRD.Rows[j].Cells["DurableMatCode"].Value.ToString().Equals(strDurableMatCode) &&
                                    //    uGridDurableGRD.Rows[j].Cells["Deleteflag"].Value.ToString().Equals("F"))

                                    if (uGridDurableGRD.Rows[j].Cells["DurableMatCode"].Value.ToString().Equals(strDurableMatCode) &&
                                        uGridDurableGRD.Rows[j].Cells["Deleteflag"].Value.ToString().Equals("F"))
                                    {
                                        intCtn += 1;
                                    }
                                }

                                if (!Convert.ToInt32(strQty).Equals(intCtn))
                                {
                                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M000081", Infragistics.Win.HAlign.Center);

                                    uGrid1_DoubleClickCell(this, new Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs(uGrid1.Rows[i].Cells["PlantCode"]));

                                    return;
                                }

                                for (int j = 0; j < uGridDurableGRD.Rows.Count; j++)
                                {
                                    this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];
                                    if (this.uGridDurableGRD.Rows[j].Hidden == false)
                                    {
                                        if (this.uGridDurableGRD.Rows[j].Cells["LotNo"].Value.ToString() == "")
                                        {
                                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", this.uGridDurableGRD.Rows[j].RowSelectorNumber + "M000466", Infragistics.Win.HAlign.Center);

                                            // Focus Cell
                                            this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[j].Cells["LotNo"];
                                            this.uGridDurableGRD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        //}
                    }
                }

                #endregion

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001051", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }


                DialogResult dir = new DialogResult();


                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);



                if (dir == DialogResult.No)
                {
                    return;
                }


                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                //-------- 1.  MASDurableLot(현재 DurableLot 마스터 테이블) 에 저장 : 리스트의 Lot그리드에 있는 내용만 저장함 -------//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);
                DataTable dtDurableLot = clsDurableLot.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true &&
                            uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("F"))
                        {
                           
                        }
                        else if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true &&
                            uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("T"))
                        {
                            for (int j = 0; j < uGridDurableGRD.Rows.Count; j++)
                            {
                                if (uGridDurableGRD.Rows[j].Cells["PlantCode"].Value.ToString().Equals(uComboPlant.Value.ToString()) &&
                                    uGridDurableGRD.Rows[j].Cells["DurableMatCode"].Value.ToString().Equals(uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString()) &&
                                    uGridDurableGRD.Rows[j].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                                {
                                    row = dtDurableLot.NewRow();

                                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                                    row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["LotNo"] = uGridDurableGRD.Rows[j].Cells["LotNo"].Value.ToString();
                                    row["GRDate"] = this.uDateMoveDate.Value.ToString();
                                    row["DiscardFlag"] = "F";
                                    row["DiscardDate"] = "";
                                    row["DiscardChargeID"] = "";
                                    row["DiscardReason"] = "";
                                    row["ChangeUsage"] = "0";
                                    row["CurUsage"] = "0";

                                    row["CumUsage"] = "0";
                                    row["LastInspectDate"] = "";
                                    row["LimitArrivalDate"] = "";
                                    dtDurableLot.Rows.Add(row);
                                }
                            }
                        }
                    }
                }
                

                //-------- 2.  DMMDurableStock(현재 Durable 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                dtDurableStock = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true &&
                            uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("F"))
                        {
                            row = dtDurableStock.NewRow();

                            row["PlantCode"] = this.uComboPlant.Value.ToString();
                            row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                            row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                            row["Qty"] = this.uGrid1.Rows[i].Cells["Qty"].Value.ToString();
                            row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();
                            row["LotNo"] = string.Empty; 

                            dtDurableStock.Rows.Add(row);
                        }
                        else if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true &&
                            uGrid1.Rows[i].Cells["SerialFlag"].Value.ToString().Equals("T"))
                        {
                            for (int j = 0; j < uGridDurableGRD.Rows.Count; j++)
                            {
                                if (uGridDurableGRD.Rows[j].Cells["PlantCode"].Value.ToString().Equals(uComboPlant.Value.ToString()) &&
                                    uGridDurableGRD.Rows[j].Cells["DurableMatCode"].Value.ToString().Equals(uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString()) &&
                                    uGridDurableGRD.Rows[j].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                                {
                                    row = dtDurableStock.NewRow();

                                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                                    row["DurableInventoryCode"] = this.uComboInventory.Value.ToString();
                                    row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["LotNo"] = uGridDurableGRD.Rows[j].Cells["LotNo"].Value.ToString();
                                    row["Qty"] = "1";
                                    row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                                    dtDurableStock.Rows.Add(row);
                                }
                            }
                        }
                    }
                }

                //-------- 3.  DMMDurableStock(현재 Durable 재고정보 테이블) 에 저장 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < dtDurableStock.Rows.Count; i++)
                {
                    //this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    row = dtDurableStockMoveHist.NewRow();

                    row["MoveGubunCode"] = "M01";       //기초재고일 경우는 "M01"
                    row["DocCode"] = "";
                    row["MoveDate"] = this.uDateMoveDate.Value.ToString();
                    row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                    row["PlantCode"] = dtDurableStock.Rows[i]["PlantCode"].ToString();
                    row["DurableInventoryCode"] = dtDurableStock.Rows[i]["DurableInventoryCode"].ToString();
                    row["EquipCode"] = "";
                    row["LotNo"] = dtDurableStock.Rows[i]["LotNo"].ToString();
                    row["DurableMatCode"] = dtDurableStock.Rows[i]["DurableMatCode"].ToString();
                    row["MoveQty"] = dtDurableStock.Rows[i]["Qty"].ToString();
                    row["UnitCode"] = dtDurableStock.Rows[i]["UnitCode"].ToString();

                    dtDurableStockMoveHist.Rows.Add(row);
                }



                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 처리 로직 //
                // 저장함수 호출

                //-------- BL 호출 : Durable 재고이동 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveBasicStock(dtDurableLot, dtDurableStock, dtDurableStockMoveHist, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }





            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitComboBox();
                InitLabel();
                InitText();
                InitGrid();

                DataTable dtNew = new DataTable();
                dtNew.Columns.Add("PlantCode");
                dtNew.Columns.Add("PlantName");
                dtNew.Columns.Add("DurableMatCode");
                dtNew.Columns.Add("DurableMatName");
                dtNew.Columns.Add("Spec");
                dtNew.Columns.Add("SerialFlag");
                dtNew.Columns.Add("Qty");
                dtNew.Columns.Add("UnitCode");

                uGrid1.DataSource = dtNew;
                uGrid1.DataBind();

                dtNew = new DataTable();
                dtNew.Columns.Add("PlantCode");
                dtNew.Columns.Add("DurableMatCode");
                dtNew.Columns.Add("DurableMatName");
                dtNew.Columns.Add("LotNo");
                dtNew.Columns.Add("DeleteFlag");

                uGridDurableGRD.DataSource = dtNew;
                uGridDurableGRD.DataBind();

                //this.uGrid1.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0 && (this.uGridDurableGRD.Rows.Count == 0 || this.uGroupBoxLot.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000811", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                if (this.uGrid1.Rows.Count > 0)
                {
                    grd.mfDownLoadGridToExcel(this.uGrid1);
                }

                if (this.uGridDurableGRD.Rows.Count > 0 && this.uGroupBoxLot.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridDurableGRD);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        // Grid Cell Update Event
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀수정시 RowSelector 이미지 처리
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 빈행일시 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //InitText();
                InitGrid();

                DataTable dtNew = new DataTable();
                dtNew.Columns.Add("PlantCode");
                dtNew.Columns.Add("PlantName");
                dtNew.Columns.Add("DurableMatCode");
                dtNew.Columns.Add("DurableMatName");
                dtNew.Columns.Add("Spec");
                dtNew.Columns.Add("SerialFlag");
                dtNew.Columns.Add("Qty");
                dtNew.Columns.Add("UnitCode");

                uGrid1.DataSource = dtNew;
                uGrid1.DataBind();

                dtNew = new DataTable();
                dtNew.Columns.Add("PlantCode");
                dtNew.Columns.Add("DurableMatCode");
                dtNew.Columns.Add("DurableMatName");
                dtNew.Columns.Add("LotNo");
                dtNew.Columns.Add("DeleteFlag");

                uGridDurableGRD.DataSource = dtNew;
                uGridDurableGRD.DataBind();

                this.uComboInventory.Items.Clear();

                string strPlantCode = this.uComboPlant.Value.ToString();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                ////////----------------- 현 재고가 없는 SparePart 정보 그리드에 바인딩------------/////////////

                // BL호출
                //brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                //QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                //brwChannel.mfCredentials(clsDurableStock);

                //// Call Method
                //DataTable dtDurableStock = clsDurableStock.mfReadMASDurable_InputDurableStock(strPlantCode, m_resSys.GetString("SYS_LANG"));

                ////테이터바인드
                //uGrid1.DataSource = dtDurableStock;
                //uGrid1.DataBind();

                //if (dtDurableStock.Rows.Count > 0)
                //{
                //    uGrid1.Enabled = true;
                //}
                //else
                //{
                //    uGrid1.Enabled = false;
                //}

                if (strPlantCode == "")
                    return;

                ////////----------------- SparePart 창고 콤보 불러오기------------/////////////
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                // Call Method
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboInventory, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "DurableInventoryCode", "DurableInventoryName", dtDurableInventory);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGroupBoxLot_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxLot.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    uGroupBoxLot.Location = point;
                    this.uGrid1.Height = 50;

                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        uGridDurableGRD.Rows[i].Hidden = false;
                    }
                }
                else
                {
                    Point point = new Point(0, 825);
                    uGroupBoxLot.Location = point;
                    this.uGrid1.Height = 740;
                    //for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    //{
                    //    this.uGrid1.Rows[i].Fixed = false;
                    //}
                    for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                    {
                        uGridDurableGRD.Rows[i].Hidden = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            try
            {
                
                //for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                //{
                //    if (uGridDurableGRD.Rows[i].Hidden == true)
                //    {
                //        uGridDurableGRD.Rows[i].Hidden = false;
                //    }

                //}

                if (e.Cell.Value == null)
                    return;

                //필요 정보 저장
                string strPlant = this.uComboPlant.Value.ToString();
                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();
                string strDurableMatName = e.Cell.Row.Cells["DurableMatName"].Value.ToString();
                string strSerialFlag = e.Cell.Row.Cells["SerialFlag"].Value.ToString();
                string strPackage = e.Cell.Row.Cells["Spec"].Value.ToString();
                

                //시리얼 사용여부가 F , 현재고량이 0 이하면 리턴
                this.uGrid1.ActiveCell = e.Cell.Row.Cells["SerialFlag"];
                if (strSerialFlag.Equals("F") || Convert.ToInt32(this.uGrid1.ActiveRow.Cells["Qty"].Value) <= 0)
                    return;

                //공장정보나 치공구정보가 없을 경우 리턴
                if (strPlant.Equals(string.Empty) || strDurableMatCode.Equals(string.Empty))
                {
                    return;
                }
                
                //ExpandGroupBox를 펼친다.
                if(this.uGroupBoxLot.Expanded.Equals(false))
                    uGroupBoxLot.Expanded = true;

                //선택한 줄을 고정시킨다.
                e.Cell.Row.Fixed = true;

                if (this.uGridDurableGRD.Rows.Count > 0)
                {
                    this.uGridDurableGRD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableGRD.Rows.All);
                    this.uGridDurableGRD.DeleteSelectedRows(false);
                }

                //for문을 돌기위한 값저장
                
                int intQty = Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value);
                int intCnt = 0;

                for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                {
                    //if (!uGridDurableGRD.Rows[i].Cells["PlantCode"].Value.ToString().Equals(strPlant) &&
                    //    !uGridDurableGRD.Rows[i].Cells["DurableMatCode"].Value.ToString().Equals(strDurableMatCode))
                    if (!uGridDurableGRD.Rows[i].Cells["DurableMatCode"].Value.ToString().Equals(strDurableMatCode))
                    {
                        //uGridDurableGRD.Rows[i].Hidden = true;
                    }
                    else
                    {
                        if (uGridDurableGRD.Rows[i].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                        {
                            //uGridDurableGRD.Rows[i].Hidden = false;
                            intCnt += 1;
                        }
                    }
                }

                DataTable dtLot = new DataTable();
                dtLot.Columns.Add("PlantCode");
                dtLot.Columns.Add("DurableMatCode");
                dtLot.Columns.Add("DurableMatName");
                dtLot.Columns.Add("Package");
                dtLot.Columns.Add("LotNo");
                dtLot.Columns.Add("DeleteFlag");

                if (!intCnt.Equals(intQty))
                {
                    if (this.uGridDurableGRD.DisplayLayout.Override.AllowAddNew.Equals(AllowAddNew.No))
                        this.uGridDurableGRD.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;

                    if (uGridDurableGRD.Rows.Count.Equals(0))
                    {
                        uGridDurableGRD.DataSource = dtLot;
                        uGridDurableGRD.DataBind();
                    }

                    for (int i = 0; i < intQty - intCnt; i++)
                    //for (int i = 0; i < intQty ; i++)
                    {
                        //wGrid.mfAddRowGrid(uGridDurableGRD, 0);

                        UltraGridRow row = this.uGridDurableGRD.DisplayLayout.Bands[0].AddNew();

                        
                        row.Cells["PlantCode"].Value = strPlant;
                        row.Cells["DurableMatCode"].Value = strDurableMatCode;
                        row.Cells["DurableMatName"].Value = strDurableMatName;
                        row.Cells["Package"].Value = strPackage;
                        row.Cells["DeleteFlag"].Value = "F";
                        
                    }

                    this.uGridDurableGRD.DisplayLayout.Bands[0].AddNew();

                    this.uGridDurableGRD.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    this.uGridDurableGRD.ActiveCell = this.uGridDurableGRD.Rows[0].Cells[0];
                }

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                uGroupBoxLot.Expanded = false;
                //for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                //{
                //    uGridDurableGRD.Rows[i].Hidden = false;
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            } 
        }

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < uGridDurableGRD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDurableGRD.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        uGridDurableGRD.Rows[i].Hidden = true;
                        uGridDurableGRD.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }

        
        /// <summary>
        /// 그리드 셀버튼 클릭시 치공구팝업창을 띄운다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_ClickCellButton(object sender, CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uComboPlant.Value.ToString();


                if (strPlantCode == "")
                {
                    DialogResult dir = new DialogResult();

                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }


                frmPOP0007 frm = new frmPOP0007();
                frm.PlantCode = strPlantCode;
                frm.ShowDialog();

                string strDurableMatCode = frm.DurableMatCode;

                if (strDurableMatCode == "")
                {
                    return;
                }

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    //this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (!i.Equals(e.Cell.Row.Index) &&
                            this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.Equals(strDurableMatCode))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001120", Infragistics.Win.HAlign.Center);

                            return;
                        }
                    }
                }

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);

                DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                if (dtDurableMat.Rows.Count > 0)
                {
                    e.Cell.Row.Cells["SerialFlag"].Value = dtDurableMat.Rows[0]["SerialFlag"].ToString();

                    this.uGrid1.Rows[e.Cell.Row.Index].Cells["SerialFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }

                e.Cell.Row.Cells["DurableMatCode"].Value = frm.DurableMatCode;
                e.Cell.Row.Cells["DurableMatName"].Value = frm.DurableMatName;
                e.Cell.Row.Cells["Spec"].Value = frm.Package;

                e.Cell.Row.Cells["DurableMatCode"].SetValue(frm.DurableMatCode, false);
                e.Cell.Row.Cells["DurableMatName"].SetValue(frm.DurableMatName, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableGRD_AfterCellUpdate(object sender, CellEventArgs e)
        {
            try
            {
                // 셀수정시 RowSelector 이미지 처리
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 빈행일시 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableGRD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                
                if (e.Cell.Column.Key.Equals("LotNo"))
                {
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                    QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                    brwChannel.mfCredentials(clsDurableLot);

                    string strLotNo = e.Cell.Value.ToString();
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();

                    if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }

                    if (strLotNo == "" || strDurableMatCode == "")
                        return;

                    // Call Method
                    DataTable dtDurableLot = clsDurableLot.mfReadDurableLot_Detail(strPlantCode, strDurableMatCode, strLotNo, m_resSys.GetString("SYS_LANG"));

                    //테이터바인드
                    if (dtDurableLot.Rows.Count > 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M001258", Infragistics.Win.HAlign.Center);

                        // Focus
                        e.Cell.Value = "";
                        return;

                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmDMMZ0013_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextMoveChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
                Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();

                uTextID = this.uTextMoveChargeID;
                uTextName = this.uTextMoveChargeName;

                String strUserID = uTextID.Text;
                uTextName.Text = "";

                if (e.KeyCode == Keys.Enter)
                {


                    // 공장콤보박스 미선택\시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000615",
                                            Infragistics.Win.HAlign.Right);

                            uTextName.Text = "";
                            uTextID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextMoveChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult dir = new DialogResult();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000597", "M000265"
                                                , Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;

                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextMoveChargeID.Text = frmPOP.UserID;
                this.uTextMoveChargeName.Text = frmPOP.UserName;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
    }
}


