﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 치공구관리                                            */
/* 모듈(분류)명 : 치공구관리                                            */
/* 프로그램ID   : frmDMMZ0020.cs                                        */
/* 프로그램명   : 치공구이동등록                                        */
/* 작성자       :  코딩 : 남현식                                        */
/* 작성일자     : 2011-06-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPDMM.UI
{
    public partial class frmDMMZ0020 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmDMMZ0020()
        {
            InitializeComponent();
        }

        private void frmDMMZ0020_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmDMMZ0020_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("치공구이동등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitCombo();
            InitText();
            InitButton();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmDMMZ0020_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정
                uTextMoveChargeID.Text = m_resSys.GetString("SYS_USERID");
                uTextMoveChargeName.Text = m_resSys.GetString("SYS_USERNAME");


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                WinLabel lbl = new WinLabel();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveDate, "반납일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMoveChargeID, "반납담당자", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정

                grd.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryCode", "현재창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DurableMatName", "치공구명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 50, Infragistics.Win.HAlign.Left
                    , Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Qty", "현재고수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 50, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ChgInventoryCode", "이동창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ChgQty", "이동수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "#,###", "nnnnnnnnnn", "0");


                //Grid초기화 후 Font크기를 아래와 같이 적용
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄추가
                grd.mfAddRowGrid(this.uGrid1, 0);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();

                this.uComboPlant.Items.Clear();

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {


            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                DataRow row;
                int intRowCheck = 0;

                // 필수입력사항 확인

                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }
                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;

                        if (this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000538", strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["DurableInventoryCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000464",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["DurableMatCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000517", strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["ChgInventoryCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString() == "0")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000516",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["ChgQty"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001237", Infragistics.Win.HAlign.Center);
                    return;
                }


                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                // 채널연결
                QRPBrowser brwChannel = new QRPBrowser();



                //-------- 1.  DMMDurableStock(현재 DurableMat 재고정보 테이블) 에 저장 : 현재창고수량 (-) 처리 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);

                DataTable dtDurableStock_Minus = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtDurableStock_Minus.NewRow();

                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString(); ;
                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                        row["Qty"] = "-" + this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStock_Minus.Rows.Add(row);
                    }
                }

                //-------- 2.  EQUDurableStock(현재 DurableMat 재고정보 테이블) 에 저장 : 이동창고수량 (+) 처리 -------//
                DataTable dtDurableStock_Plus = clsDurableStock.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtDurableStock_Plus.NewRow();

                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString(); ;
                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                        row["Qty"] = this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStock_Plus.Rows.Add(row);
                    }
                }


                //-------- 3.  EQUDurableStockMoveHist(재고이력 테이블) 에 저장 : 현재창고수량 (-) -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                brwChannel.mfCredentials(clsDurableStockMoveHist);

                DataTable dtDurableStockMoveHist_Minus = clsDurableStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtDurableStockMoveHist_Minus.NewRow();

                        row["MoveGubunCode"] = "M05";       //자재이동일 경우는 "M05"
                        row["DocCode"] = "";
                        row["MoveDate"] = this.uDateMoveDate.Value.ToString();
                        row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                        row["EquipCode"] = "";
                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                        row["MoveQty"] = "-" + this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStockMoveHist_Minus.Rows.Add(row);
                    }
                }

                //-------- 4.  EQUDurableStockMoveHist(재고이력 테이블) 에 저장 : 이동창고수량 (-) -------//

                DataTable dtDurableStockMoveHist_Plus = clsDurableStockMoveHist.mfSetDatainfo();

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        row = dtDurableStockMoveHist_Plus.NewRow();

                        row["MoveGubunCode"] = "M05";       //자재이동일 경우는 "M05"
                        row["DocCode"] = "";
                        row["MoveDate"] = this.uDateMoveDate.Value.ToString();
                        row["MoveChargeID"] = this.uTextMoveChargeID.Text;
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["ChgInventoryCode"].Value.ToString();
                        row["EquipCode"] = "";
                        row["DurableMatCode"] = this.uGrid1.Rows[i].Cells["DurableMatCode"].Value.ToString();
                        row["LotNo"] = this.uGrid1.Rows[i].Cells["LotNo"].Value.ToString();
                        row["MoveQty"] = this.uGrid1.Rows[i].Cells["ChgQty"].Value.ToString();
                        row["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();

                        dtDurableStockMoveHist_Plus.Rows.Add(row);
                    }
                }




                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------- BL 호출 : DurableMat 자재 저장을 위한 BL호출 -------//
                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.SAVEDurableStock), "SAVEDurableStock");
                QRPDMM.BL.DMMICP.SAVEDurableStock clsSAVEDurableStock = new QRPDMM.BL.DMMICP.SAVEDurableStock();
                brwChannel.mfCredentials(clsSAVEDurableStock);
                string rtMSG = clsSAVEDurableStock.mfSaveMove(dtDurableStock_Minus, dtDurableStock_Plus, dtDurableStockMoveHist_Minus, dtDurableStockMoveHist_Plus, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information,  500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    mfCreate();
                    mfSearch();

                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                InitText();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                //InitCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001173", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGrid1);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid1.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboPlant.Value.ToString();

                if (strPlantCode == "")
                    return;


                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);
                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "DurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "ChgInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDurableInventory);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);




                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinGrid wGrid = new WinGrid();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                //컬럼
                String strColumn = e.Cell.Column.Key;
                int intIndex = e.Cell.Row.Index;

                string strPlantCode = uComboPlant.Value.ToString();
                //string strDurableInventoryCode = uGrid1.ActiveCell.Row.Cells["DurableInventoryCode"].Value.ToString();
                string strDurableInventoryCode = e.Cell.Row.Cells["DurableInventoryCode"].Value.ToString();

                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                brwChannel.mfCredentials(clsDurableStock);


                if (strDurableInventoryCode == "")
                    return;

                //if (strColumn == "DurableInventoryCode")
                //{

                //    DataTable dtDurableStock = clsDurableStock.mfReadDurableStock_SPCombo(strPlantCode, strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                //    wGrid.mfSetGridCellValueList(this.uGrid1, intIndex, "DurableMatCode", "", "", dtDurableStock);
                //    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                //}


                ////줄번호
                //if (strColumn == "DurableMatCode")
                //{

                //    string strDurableMatCode = uGrid1.ActiveCell.Row.Cells["DurableMatCode"].Value.ToString();

                //    if (strDurableMatCode == "")
                //        return;

                //    DataTable dtDurableStockDetail = clsDurableStock.mfReadDurableStock_Detail(strPlantCode, strDurableInventoryCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                //    uGrid1.ActiveCell.Row.Cells["DurableMatName"].Value = dtDurableStockDetail.Rows[0]["DurableMatName"].ToString();
                //    uGrid1.ActiveCell.Row.Cells["Qty"].Value = dtDurableStockDetail.Rows[0]["Qty"].ToString();
                //    uGrid1.ActiveCell.Row.Cells["Spec"].Value = dtDurableStockDetail.Rows[0]["Spec"].ToString();
                //    uGrid1.ActiveCell.Row.Cells["UnitCode"].Value = dtDurableStockDetail.Rows[0]["UnitCode"].ToString();
                //    uGrid1.ActiveCell.Row.Cells["UnitName"].Value = dtDurableStockDetail.Rows[0]["UnitName"].ToString();
                //}


                if (strColumn == "ChgInventoryCode")
                {

                    string strChgInventoryCode = e.Cell.Row.Cells["ChgInventoryCode"].Value.ToString();

                    if (strDurableInventoryCode == strChgInventoryCode)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001262", Infragistics.Win.HAlign.Center);


                        this.uGrid1.ActiveCell.Row.Cells["ChgInventoryCode"].Value = "";
                        return;
                    }
                }

                if (strColumn == "ChgQty")
                {

                    int intChgQty = Convert.ToInt32(e.Cell.Row.Cells["ChgQty"].Value.ToString());
                    int intQty = Convert.ToInt32(e.Cell.Row.Cells["Qty"].Value.ToString());

                    if (intChgQty > intQty)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M001261", Infragistics.Win.HAlign.Center);


                        this.uGrid1.ActiveCell.Row.Cells["ChgInventoryCode"].Value = "";
                        this.uGrid1.ActiveCell.Row.Cells["ChgQty"].Value = "0";
                        return;
                    }
                }
            }


            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //int intSelectItem = Convert.ToInt32(e.Cell.ValueList.SelectedItemIndex.ToString());

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString());

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                titleArea.Focus();
                WinGrid wGrid = new WinGrid();

                string _strPlantCode = this.uComboPlant.Value.ToString();

                if (_strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                #region DurableMatCode
                if (e.Cell.Column.Key.ToString().Equals("DurableMatCode"))
                {
                    string strInventoryCode = e.Cell.Row.Cells["DurableInventoryCode"].Value.ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);
                    DataTable dtEquipDurableBOM = clsDurableStock.mfReadDurableStock_SPCombo_Lot(_strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));


                    e.Cell.Row.Cells["DurableMatCode"].Value = dtEquipDurableBOM.Rows[intSelectItem]["DurableMatCode"].ToString();
                    e.Cell.Row.Cells["DurableMatName"].Value = dtEquipDurableBOM.Rows[intSelectItem]["DurableMatName"].ToString();
                    //e.Cell.Row.Cells["ChgDurableMatName"].Value = dtEquipDurableBOM.Rows[intSelectItem]["DurableMatName"].ToString();
                    e.Cell.Row.Cells["Qty"].Value = dtEquipDurableBOM.Rows[intSelectItem]["Qty"].ToString();
                    e.Cell.Row.Cells["LotNo"].Value = dtEquipDurableBOM.Rows[intSelectItem]["LotNo"].ToString();
                    e.Cell.Row.Cells["Spec"].Value = dtEquipDurableBOM.Rows[intSelectItem]["Spec"].ToString();
                    e.Cell.Row.Cells["UnitCode"].Value = dtEquipDurableBOM.Rows[intSelectItem]["UnitCode"].ToString();
                    e.Cell.Row.Cells["UnitName"].Value = dtEquipDurableBOM.Rows[intSelectItem]["UnitName"].ToString();

                    if (e.Cell.Row.Cells["LotNo"].Value.ToString() != "")
                    {
                        e.Cell.Row.Cells["Qty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        e.Cell.Row.Cells["Qty"].Value = "1";
                        e.Cell.Row.Cells["ChgQty"].Value = "1";
                    }
                    else
                    {
                        e.Cell.Row.Cells["Qty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        e.Cell.Row.Cells["ChgQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        
                        e.Cell.Row.Cells["ChgQty"].Value = "0";
                    }

                }
                #endregion

                #region DurableInventoryCode
                if (e.Cell.Column.Key.ToString().Equals("DurableInventoryCode"))
                {

                    e.Cell.Row.Cells["DurableMatCode"].Value = "";
                    e.Cell.Row.Cells["DurableMatName"].Value = "";
                    e.Cell.Row.Cells["LotNo"].Value = "";
                    e.Cell.Row.Cells["Spec"].Value = "";
                    e.Cell.Row.Cells["Qty"].Value = 0;
                    e.Cell.Row.Cells["ChgInventoryCode"].Value = "";
                    e.Cell.Row.Cells["ChgQty"].Value = 0;


                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);
                    DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(_strPlantCode, m_resSys.GetString("SYS_LANG"));

                    string strInventoryCode = dtDurableInventory.Rows[intSelectItem]["DurableInventoryCode"].ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    DataTable dtStock = new DataTable();

                    dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(_strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));


                    string strDropDownGridHKey = "DurableMatCode,DurableMatName,LotNo,Qty"; //,UnitCode,UnitName";
                    string strDropDownGridHName = "치공구코드,치공구명,LOTNO,교체수량";
                    grd.mfSetGridCellValueGridList(uGrid1, 0, e.Cell.Row.Index, "DurableMatCode", Infragistics.Win.ValueListDisplayStyle.DataValue,
                        strDropDownGridHKey, strDropDownGridHName, "DurableMatName", "DurableMatCode", dtStock);
                }
                #endregion

                #region ChgInventoryCode
                if (e.Cell.Column.Key.ToString().Equals("ChgInventoryCode"))
                {

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);
                    DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(_strPlantCode, m_resSys.GetString("SYS_LANG"));

                    string strInventoryCode = dtDurableInventory.Rows[intSelectItem]["DurableInventoryCode"].ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    DataTable dtStock = new DataTable();

                    dtStock = clsDurableStock.mfReadDurableStock_SPCombo_Lot(_strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));


                    string strDropDownGridHKey = "DurableMatCode,DurableMatName,LotNo,Qty"; //,UnitCode,UnitName";
                    string strDropDownGridHName = "치공구코드,치공구명,LOTNO,교체수량";
                    grd.mfSetGridCellValueGridList(uGrid1, 0, e.Cell.Row.Index, "DurableMatCode", Infragistics.Win.ValueListDisplayStyle.DataValue,
                        strDropDownGridHKey, strDropDownGridHName, "DurableMatName", "DurableMatCode", dtStock);

                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        #region 사용자 검색
        /// <summary>
        /// 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextMoveChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                uTextMoveChargeID.Text = frmUser.UserID;
                uTextMoveChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextMoveChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextMoveChargeID.Text;

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID,m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics
                            .Win.UltraMessageBox.MessageBoxStyle.Vista
                             , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);
                        this.uComboPlant.DropDown();

                        return;
                    }
                    else
                    {
                        this.uTextMoveChargeName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }

                }
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextMoveChargeID.Text = "";
                    this.uTextMoveChargeName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
    }
}
