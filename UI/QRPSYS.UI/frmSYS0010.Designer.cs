﻿namespace QRPSYS.UI
{
    partial class frmSYS0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0010));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            this.uGridMenuList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextProgramName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProgramID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProgramName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgramID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxLang = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridProgramLanguage = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMenuList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLang)).BeginInit();
            this.uGroupBoxLang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProgramLanguage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGridMenuList
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMenuList.DisplayLayout.Appearance = appearance1;
            this.uGridMenuList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMenuList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMenuList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMenuList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridMenuList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMenuList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridMenuList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMenuList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMenuList.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMenuList.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridMenuList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMenuList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMenuList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMenuList.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridMenuList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMenuList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMenuList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridMenuList.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridMenuList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMenuList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridMenuList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridMenuList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMenuList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridMenuList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMenuList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMenuList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMenuList.Location = new System.Drawing.Point(0, 40);
            this.uGridMenuList.Name = "uGridMenuList";
            this.uGridMenuList.Size = new System.Drawing.Size(1070, 400);
            this.uGridMenuList.TabIndex = 1;
            this.uGridMenuList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridMenuList_DoubleClickRow);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGroupBoxLang);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 440);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1070, 410);
            this.ultraGroupBox1.TabIndex = 3;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox2.Controls.Add(this.uTextProgramName);
            this.ultraGroupBox2.Controls.Add(this.uTextProgramID);
            this.ultraGroupBox2.Controls.Add(this.uLabelProgramName);
            this.ultraGroupBox2.Controls.Add(this.uLabelProgramID);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 36);
            this.ultraGroupBox2.TabIndex = 7;
            // 
            // uTextProgramName
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProgramName.Appearance = appearance13;
            this.uTextProgramName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProgramName.Location = new System.Drawing.Point(379, 8);
            this.uTextProgramName.Name = "uTextProgramName";
            this.uTextProgramName.ReadOnly = true;
            this.uTextProgramName.Size = new System.Drawing.Size(152, 21);
            this.uTextProgramName.TabIndex = 12;
            // 
            // uTextProgramID
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProgramID.Appearance = appearance14;
            this.uTextProgramID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProgramID.Location = new System.Drawing.Point(112, 8);
            this.uTextProgramID.Name = "uTextProgramID";
            this.uTextProgramID.ReadOnly = true;
            this.uTextProgramID.Size = new System.Drawing.Size(152, 21);
            this.uTextProgramID.TabIndex = 13;
            // 
            // uLabelProgramName
            // 
            this.uLabelProgramName.Location = new System.Drawing.Point(272, 8);
            this.uLabelProgramName.Name = "uLabelProgramName";
            this.uLabelProgramName.Size = new System.Drawing.Size(100, 20);
            this.uLabelProgramName.TabIndex = 10;
            // 
            // uLabelProgramID
            // 
            this.uLabelProgramID.Location = new System.Drawing.Point(8, 8);
            this.uLabelProgramID.Name = "uLabelProgramID";
            this.uLabelProgramID.Size = new System.Drawing.Size(100, 20);
            this.uLabelProgramID.TabIndex = 11;
            // 
            // uGroupBoxLang
            // 
            this.uGroupBoxLang.Controls.Add(this.uGridProgramLanguage);
            this.uGroupBoxLang.Controls.Add(this.ultraGroupBox3);
            this.uGroupBoxLang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBoxLang.Location = new System.Drawing.Point(3, 36);
            this.uGroupBoxLang.Name = "uGroupBoxLang";
            this.uGroupBoxLang.Size = new System.Drawing.Size(1064, 371);
            this.uGroupBoxLang.TabIndex = 8;
            // 
            // uGridProgramLanguage
            // 
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProgramLanguage.DisplayLayout.Appearance = appearance18;
            this.uGridProgramLanguage.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProgramLanguage.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProgramLanguage.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProgramLanguage.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridProgramLanguage.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProgramLanguage.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridProgramLanguage.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProgramLanguage.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProgramLanguage.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProgramLanguage.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGridProgramLanguage.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProgramLanguage.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProgramLanguage.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProgramLanguage.DisplayLayout.Override.CellAppearance = appearance19;
            this.uGridProgramLanguage.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProgramLanguage.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProgramLanguage.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance25.TextHAlignAsString = "Left";
            this.uGridProgramLanguage.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGridProgramLanguage.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProgramLanguage.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridProgramLanguage.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridProgramLanguage.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProgramLanguage.DisplayLayout.Override.TemplateAddRowAppearance = appearance22;
            this.uGridProgramLanguage.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProgramLanguage.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProgramLanguage.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProgramLanguage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridProgramLanguage.Location = new System.Drawing.Point(3, 40);
            this.uGridProgramLanguage.Name = "uGridProgramLanguage";
            this.uGridProgramLanguage.Size = new System.Drawing.Size(1058, 328);
            this.uGridProgramLanguage.TabIndex = 3;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1058, 40);
            this.ultraGroupBox3.TabIndex = 2;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(8, 8);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 1;
            this.uButtonDeleteRow.Text = "ultraButton1";
            // 
            // frmSYS0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.uGridMenuList);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0010";
            this.Load += new System.EventHandler(this.frmSYS0010_Load);
            this.Activated += new System.EventHandler(this.frmSYS0010_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSYS0010_FormClosing);
            this.Resize += new System.EventHandler(this.frmSYS0010_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMenuList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLang)).EndInit();
            this.uGroupBoxLang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProgramLanguage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMenuList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramID;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramName;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxLang;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProgramLanguage;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
    }
}