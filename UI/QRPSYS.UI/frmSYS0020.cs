﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 시스템관리                                            */
/* 프로그램ID   : frmSYS0029.cs                                         */
/* 프로그램명   : 비밀번호변경                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSYS.UI
{
    public partial class frmSYS0020 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0020()
        {
            InitializeComponent();
        }

        private void frmSYS0020_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, false, false, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0020_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitText();
            InitLabel();
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResoureceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("비밀번호변경", m_resSys.GetString("SYS_FONTNAME"), 12);
                uTextUserID.Text = m_resSys.GetString("SYS_USERID");
                uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResoureceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelUser, "사용자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPassword, "변경할 암호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPasswordOK, "암호 확인", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바기능
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (uTextPassword.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001024", "M000595",
                                Infragistics.Win.HAlign.Right);
                    uTextPassword.Focus();
                    return;
                }
                
                if (uTextPasswordOK.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001024", "M000795",
                                Infragistics.Win.HAlign.Right);
                    uTextPasswordOK.Focus();
                    return;
                }

                if (uTextPasswordOK.Text != uTextPassword.Text)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001024", "M000910",
                                Infragistics.Win.HAlign.Right);
                    uTextPasswordOK.Focus();
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", "M000936",
                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    string strRtn = clsUser.mfSaveSYSUserPassword(uTextUserID.Text, uTextPassword.Text, m_resSys.GetString("SYS_USERIP"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn.mfDecodingErrMessage(strRtn);
                    //----//
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;

                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        uTextPassword.Text = "";
                        uTextPasswordOK.Text = "";
                        return;
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001135", "M001037", "M000953",
                             Infragistics.Win.HAlign.Right);
                        return;
                    }

                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

    }
}
