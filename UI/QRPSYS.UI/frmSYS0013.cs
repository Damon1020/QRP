﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0013.cs                                         */
/* 프로그램명   : 사용자공통코드정보                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//Using추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPSYS.UI
{
    public partial class frmSYS0013 : Form , IToolbar
    {
        //리소스정보 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0013()
        {
            InitializeComponent();
        }

        private void frmSYS0013_Activated(object sender, EventArgs e)
        {
            //활성된 툴바 기능 사용여부
            QRPBrowser ToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0013_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitGrid();

            //Form내의 Grid의 열속성을 아래와 같이 적용하여 저장한다.
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSYS0013_Resize);
        }

        void frmSYS0013_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridUserCommonCode.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridUserCommonCode.Dock = DockStyle.None;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 타이틀 설정
        /// </summary>
        private void InitTitle()
        {
            try
            { 
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 타이틀 설정
                titleArea.mfSetLabelText("사용자공통코드정보", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }

        }

        /// <summary>
        /// 그리드 컨트롤 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                //사용자공통코드정보 그리드 기본설정
                wGrid.mfInitGeneralGrid(this.uGridUserCommonCode,true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //사용자공통코드정보 그리드 컬럼설정

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ModuleGubun", "모듈구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComGubunCode", "구분코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComGubunName", "구분명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComGubunNameCh", "구분명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComGubunNameEn", "구분명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComCode", "공통코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComCodeName", "공통코드명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComCodeNameCh", "공통코드명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 140, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "ComCodeNameEn", "공통코드명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 140, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "DisplaySeq", "공통코드 리스트순서", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn", "0");

                wGrid.mfSetGridColumn(this.uGridUserCommonCode, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //그리드 폰트 지정
                this.uGridUserCommonCode.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridUserCommonCode.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //사용여부 콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtComcode = clsCommonCode.mfReadCommonCode("C0001",m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridUserCommonCode, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComcode);

                //모듈구분 콤보박스
                DataTable dtModule = clsCommonCode.mfReadCommonCode("C0055", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridUserCommonCode, 0, "ModuleGubun", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtModule);

                //빈출 추가
                wGrid.mfAddRowGrid(this.uGridUserCommonCode, 0);

                this.uGridUserCommonCode.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridUserCommonCode_AfterCellUpdate);
                this.uGridUserCommonCode.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridUserCommonCode_CellChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        #endregion

        #region 툴바 기능

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //Systems ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //시스템 사용자공통코드 정보BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCommonCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCommonCode);

                //시스템 사용자공통코드 조회매서드 호출
                DataTable dtUserCommonCode = clsUserCommonCode.mfReadUserCommonCode(m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridUserCommonCode.DataSource = dtUserCommonCode;
                this.uGridUserCommonCode.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtUserCommonCode.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                         Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK 수정불가처리
                    for (int i = 0; i < this.uGridUserCommonCode.Rows.Count; i++)
                    {
                        this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridUserCommonCode.Rows[i].Cells["ComCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridUserCommonCode, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //Systems ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGridUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001047", Infragistics.Win.HAlign.Right);

                    return;
                }

                #region 시스템사용자공통코드정보 저장

                //시스템사용자공통코드 정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCommonCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCommonCode);

                //시스템사용자공통코드 저장매서드 호출
                DataTable dtUserCommonCode = clsUserCommonCode.mfSetDataInfo();
                
                string strLang = m_resSys.GetString("SYS_LANG");

                if (this.uGridUserCommonCode.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridUserCommonCode.Rows.Count; i++)
                    {
                        if (this.uGridUserCommonCode.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            string strRow = this.uGridUserCommonCode.Rows[i].RowSelectorNumber.ToString();

                            #region 필수입력사항 확인

                            if (this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , strRow + msg.GetMessge_Text("M000501",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"];
                                this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            if (this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , strRow + msg.GetMessge_Text("M000491",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"];
                                this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridUserCommonCode.Rows[i].Cells["ComGubunName"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , strRow + msg.GetMessge_Text("M000490",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComGubunName"];
                                this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridUserCommonCode.Rows[i].Cells["ComCode"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , strRow + msg.GetMessge_Text("M000483",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComCode"];
                                this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            if (this.uGridUserCommonCode.Rows[i].Cells["ComCodeName"].Value.Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , strRow + msg.GetMessge_Text("M000484",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComCodeName"];
                                this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #endregion

                            //정보저장
                            DataRow drCom = dtUserCommonCode.NewRow();

                            drCom["ModuleGubun"] = this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"].Value.ToString();
                            drCom["ComGubunCode"] = this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString();
                            drCom["ComGubunName"] = this.uGridUserCommonCode.Rows[i].Cells["ComGubunName"].Value.ToString();
                            drCom["ComGubunNameCh"] = this.uGridUserCommonCode.Rows[i].Cells["ComGubunNameCh"].Value.ToString();
                            drCom["ComGubunNameEn"] = this.uGridUserCommonCode.Rows[i].Cells["ComGubunNameEn"].Value.ToString();
                            drCom["ComCode"] = this.uGridUserCommonCode.Rows[i].Cells["ComCode"].Value.ToString();
                            drCom["ComCodeName"] = this.uGridUserCommonCode.Rows[i].Cells["ComCodeName"].Value.ToString();
                            drCom["ComCodeNameCh"] = this.uGridUserCommonCode.Rows[i].Cells["ComCodeNameCh"].Value.ToString();
                            drCom["ComCodeNameEn"] = this.uGridUserCommonCode.Rows[i].Cells["ComCodeNameEn"].Value.ToString();
                            drCom["DisplaySeq"] = this.uGridUserCommonCode.Rows[i].Cells["DisplaySeq"].Value.ToString();
                            drCom["UseFlag"] = this.uGridUserCommonCode.Rows[i].Cells["UseFlag"].Value.ToString();

                            dtUserCommonCode.Rows.Add(drCom);
                        }
                        
                    }
                }

                if (dtUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001032", "M001047", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion

                //저장여부 메세지 박스를 띄운다.

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //사용자 공통코드 저장매서드 호출
                    string strErrRtn = clsUserCommonCode.mfSaveUserCommonCode(dtUserCommonCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //Systems ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드에 정보가 없을 경우 삭제정보 없음
                if (this.uGridUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000634", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 사용자공통코드삭제정보 저장

                //사용자공통코드 정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCommonCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCommonCode);

                //사용자 공통코드 삭제컬럼정보 매서드 호출
                DataTable dtUserCommonCode = clsUserCommonCode.mfSetDeleteDataInfo();

                string strLang = m_resSys.GetString("SYS_LANG");

                if (this.uGridUserCommonCode.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridUserCommonCode.Rows.Count; i++)
                    {
                        if (this.uGridUserCommonCode.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            //그리드 체크박스에 체크가된 줄을 저장한다.
                            if (Convert.ToBoolean(this.uGridUserCommonCode.Rows[i].Cells["Check"].Value) == true)
                            {
                                #region 필수 입력사항
                                string strRow = this.uGridUserCommonCode.Rows[i].RowSelectorNumber.ToString();

                                if (this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"].Value.ToString().Equals(string.Empty))
                                {

                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001226",strLang)
                                                        , strRow + msg.GetMessge_Text("M000501",strLang)
                                                        , Infragistics.Win.HAlign.Right);
                                    //PerFormAction
                                    this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"];
                                    this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString().Equals(string.Empty))
                                {

                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001226",strLang)
                                                        , strRow + msg.GetMessge_Text("M000491",strLang)
                                                        , Infragistics.Win.HAlign.Right);
                                    //PerFormAction
                                    this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"];
                                    this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                                if (this.uGridUserCommonCode.Rows[i].Cells["ComCode"].Value.ToString().Equals(string.Empty))
                                {

                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264",strLang)
                                                        , msg.GetMessge_Text("M001226",strLang)
                                                        , strRow + msg.GetMessge_Text("M000483",strLang)
                                                        , Infragistics.Win.HAlign.Right);
                                    //PerFormAction
                                    this.uGridUserCommonCode.ActiveCell = this.uGridUserCommonCode.Rows[i].Cells["ComCode"];
                                    this.uGridUserCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }

                                #endregion

                                DataRow drCom = dtUserCommonCode.NewRow();

                                drCom["ModuleGubun"] = this.uGridUserCommonCode.Rows[i].Cells["ModuleGubun"].Value.ToString();
                                drCom["ComGubunCode"] = this.uGridUserCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString();
                                drCom["ComCode"] = this.uGridUserCommonCode.Rows[i].Cells["ComCode"].Value.ToString();

                                dtUserCommonCode.Rows.Add(drCom);
                            }
                        }

                    }

                }

                if (dtUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000634", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                //그리드에 정보가 없을 경우 삭제정보 없음
                if (dtUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000634", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }

                //삭제 여부 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //시스템공통코드정보 삭제 매서드 실행
                    string strErrRtn = clsUserCommonCode.mfDeleteUserCommonCode(dtUserCommonCode);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //Systems ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uGridUserCommonCode.Rows.Count > 0)
                {
                    //그리드의 모든행을 선택하여 줄을 삭제한다.
                    this.uGridUserCommonCode.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridUserCommonCode.Rows.All);
                    this.uGridUserCommonCode.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //Systems ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridUserCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000808", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridUserCommonCode);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        #endregion

        // 신규로 추가한 행에 대해 데이터를 모두 Clear한 행을 없애기
        private void uGridUserCommonCode_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridUserCommonCode, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);

        }

        //신규로 추가한 행 및 수정한 행을 표시하기 위해 이벤트에 아래 코드를 작성하여 RowSelector란에 편집이미지를 나타나게한다.
        private void uGridUserCommonCode_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }

        //Form내의 Grid의 열속성을 저장한다.
        private void frmSYS0013_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

    }
}
