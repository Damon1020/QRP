﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0001.cs                                         */
/* 프로그램명   : 메뉴정보팝업창                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPSYS.UI
{
    public partial class frmSYS0005D1 : Form
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //메뉴정보 속성
        private string strProgramID;
        private string strProgramName;
        private string strUpperProgramID;
        private string strUpperProgramName;
        private string strModuleID;
        private string strUseFlag;
        private string strUseFlagName;
        private string strMenuLevel;
        private string strMenuOrder;

        
        //프로그램ID
        public string ProgramID
        {
            get { return strProgramID; }
            set { strProgramID = value; }
        }
        //프로그램명
        public string ProgramName
        {
            get { return strProgramName; }
            set { strProgramName = value; }
        }
        //상위프로그램ID
        public string UpperProgramID
        {
            get { return strUpperProgramID; }
            set { strUpperProgramID = value; }
        }
        //상위프로그램명
        public string UpperProgramName
        {
            get { return strUpperProgramName; }
            set { strUpperProgramName = value; }
        }
        //ModuleID
        public string ModuleID
        {
            get { return strModuleID; }
            set { strModuleID = value; }
        }
        //사용여부
        public string UseFlag
        {
            get { return strUseFlag; }
            set { strUseFlag = value; }
        }
        //사용여부
        public string UseFlagName
        {
            get { return strUseFlagName; }
            set { strUseFlagName = value; }
        }
        //메뉴레벨
        public string MenuLevel
        {
            get { return strMenuLevel; }
            set { strMenuLevel = value; }
        }
        //메뉴순서
        public string MenuOrder
        {
            get { return strMenuOrder; }
            set { strMenuOrder = value; }
        }


        public frmSYS0005D1()
        {
            InitializeComponent();
        }

        private void frmSYS0005D1_Load(object sender, EventArgs e)
        {

            strProgramID = "";
            strProgramName = "";
            strUpperProgramID = "";
            strUpperProgramName = "";
            strModuleID = "";
            strUseFlag = "";
            strUseFlagName = "";
            strMenuLevel = "";
            strMenuOrder = "";

            //컨트롤 초기화
            InitLabel();
            InitGrid();
            InitButton();
            //InitComboBox();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchMenu, "조건", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                // 메뉴 정보 그리드 기본설정 //
                wGrd.mfInitGeneralGrid(this.uGridSYSMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //컬럼 설정후 그리드의 폰트사이즈를 9로한다.
                this.uGridSYSMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSYSMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            //try
            //{
            //    //SystemResourceInfo
            //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //    // 메뉴정보 BL호출
            //    QRPBrowser brwChannel = new QRPBrowser();
            //    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
            //    QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
            //    brwChannel.mfCredentials(clsMenu);

            //    // 메뉴정보 콤보 매서드 호출
            //    DataTable dtMenu = clsMenu.mfReadMenuCombo(m_resSys.GetString("SYS_LANG"));

            //    WinComboEditor wCom = new WinComboEditor();

            //    wCom.mfSetComboEditor(this.uComboSearchMenu, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
            //        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
            //        , "", "", "전체", "ProgramID", "ProgramName", dtMenu);

            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //}
        }

        #endregion

        #region 이벤트

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Search();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridSYSMenu.ActiveRow.Index >= 0)
                {
                    strProgramID = this.uGridSYSMenu.ActiveRow.Cells["ProgramID"].Text.ToString();
                    strProgramName = this.uGridSYSMenu.ActiveRow.Cells["ProgramName"].Text.ToString();

                    strUpperProgramID = this.uGridSYSMenu.ActiveRow.Cells["UpperProgramID"].Text.ToString();
                    strUpperProgramName = this.uGridSYSMenu.ActiveRow.Cells["UpperProgramName"].Text.ToString();
                    strModuleID = this.uGridSYSMenu.ActiveRow.Cells["ModuleID"].Text.ToString();

                    strUseFlag = this.uGridSYSMenu.ActiveRow.Cells["UseFlag"].Text.ToString();
                    strUseFlagName = this.uGridSYSMenu.ActiveRow.Cells["UseFlagName"].Text.ToString();
                    strMenuLevel = this.uGridSYSMenu.ActiveRow.Cells["MenuLevel"].Text.ToString();
                    strMenuOrder = this.uGridSYSMenu.ActiveRow.Cells["MenuOrder"].Text.ToString();
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridSYSMenu_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strProgramID = e.Row.Cells["ProgramID"].Text.ToString();
                strProgramName = e.Row.Cells["ProgramName"].Text.ToString();

                strUpperProgramID = e.Row.Cells["UpperProgramID"].Text.ToString();
                strUpperProgramName = e.Row.Cells["UpperProgramName"].Text.ToString();

                strModuleID = e.Row.Cells["ModuleID"].Text.ToString();

                strUseFlag = e.Row.Cells["UseFlag"].Text.ToString();
                strUseFlagName = e.Row.Cells["UseFlagName"].Text.ToString();

                strMenuLevel = e.Row.Cells["MenuLevel"].Text.ToString();
                strMenuOrder = e.Row.Cells["MenuOrder"].Text.ToString();
                
                this.Close();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //프로그램ID
                string strProgramID = this.uTextProgramID.Text.Trim();

                //메뉴정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu),"Menu");
                QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(clsMenu);

                // 메뉴정보 조회 매서드 호출
                DataTable dtMenu = clsMenu.mfReadMenu(strProgramID, m_resSys.GetString("SYS_LANG"));

                // 그리드에 바인드 
                this.uGridSYSMenu.DataSource = dtMenu;
                this.uGridSYSMenu.DataBind();

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtMenu.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSYSMenu, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
