﻿namespace QRPSYS.UI
{
    partial class frmSYS0022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0022));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionSearchLang = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelSearchLanguage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMessage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMessage = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxMessageInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextMessageEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMessageEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMessageCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMessageCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMessage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMessage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMessageCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMessageCode = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCommonMessageList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMessageInfo)).BeginInit();
            this.uGroupBoxMessageInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonMessageList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSearchLang);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLanguage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMessage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMessage);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uOptionSearchLang
            // 
            this.uOptionSearchLang.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSearchLang.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            this.uOptionSearchLang.ItemOrigin = new System.Drawing.Point(0, 4);
            valueListItem1.DataValue = "KOR";
            valueListItem1.DisplayText = "한글";
            valueListItem2.DataValue = "CHN";
            valueListItem2.DisplayText = "중문";
            valueListItem3.DataValue = "ENG";
            valueListItem3.DisplayText = "영문";
            this.uOptionSearchLang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.uOptionSearchLang.Location = new System.Drawing.Point(428, 12);
            this.uOptionSearchLang.Name = "uOptionSearchLang";
            this.uOptionSearchLang.Size = new System.Drawing.Size(200, 20);
            this.uOptionSearchLang.TabIndex = 3;
            this.uOptionSearchLang.TextIndentation = 2;
            this.uOptionSearchLang.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelSearchLanguage
            // 
            this.uLabelSearchLanguage.Location = new System.Drawing.Point(324, 12);
            this.uLabelSearchLanguage.Name = "uLabelSearchLanguage";
            this.uLabelSearchLanguage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLanguage.TabIndex = 2;
            this.uLabelSearchLanguage.Text = "선택";
            // 
            // uTextSearchMessage
            // 
            this.uTextSearchMessage.Location = new System.Drawing.Point(116, 12);
            this.uTextSearchMessage.Name = "uTextSearchMessage";
            this.uTextSearchMessage.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchMessage.TabIndex = 1;
            // 
            // uLabelSearchMessage
            // 
            this.uLabelSearchMessage.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchMessage.Name = "uLabelSearchMessage";
            this.uLabelSearchMessage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMessage.TabIndex = 0;
            this.uLabelSearchMessage.Text = "Message";
            // 
            // uGroupBoxMessageInfo
            // 
            this.uGroupBoxMessageInfo.Controls.Add(this.uTextMessageEn);
            this.uGroupBoxMessageInfo.Controls.Add(this.uLabelMessageEn);
            this.uGroupBoxMessageInfo.Controls.Add(this.uTextMessageCh);
            this.uGroupBoxMessageInfo.Controls.Add(this.uLabelMessageCh);
            this.uGroupBoxMessageInfo.Controls.Add(this.uTextMessage);
            this.uGroupBoxMessageInfo.Controls.Add(this.uLabelMessage);
            this.uGroupBoxMessageInfo.Controls.Add(this.uTextMessageCode);
            this.uGroupBoxMessageInfo.Controls.Add(this.uLabelMessageCode);
            this.uGroupBoxMessageInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxMessageInfo.Location = new System.Drawing.Point(0, 770);
            this.uGroupBoxMessageInfo.Name = "uGroupBoxMessageInfo";
            this.uGroupBoxMessageInfo.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxMessageInfo.TabIndex = 4;
            // 
            // uTextMessageEn
            // 
            this.uTextMessageEn.Location = new System.Drawing.Point(740, 52);
            this.uTextMessageEn.Name = "uTextMessageEn";
            this.uTextMessageEn.Size = new System.Drawing.Size(200, 21);
            this.uTextMessageEn.TabIndex = 9;
            // 
            // uLabelMessageEn
            // 
            this.uLabelMessageEn.Location = new System.Drawing.Point(636, 52);
            this.uLabelMessageEn.Name = "uLabelMessageEn";
            this.uLabelMessageEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelMessageEn.TabIndex = 8;
            this.uLabelMessageEn.Text = "MessageEn";
            // 
            // uTextMessageCh
            // 
            this.uTextMessageCh.Location = new System.Drawing.Point(428, 52);
            this.uTextMessageCh.Name = "uTextMessageCh";
            this.uTextMessageCh.Size = new System.Drawing.Size(200, 21);
            this.uTextMessageCh.TabIndex = 7;
            // 
            // uLabelMessageCh
            // 
            this.uLabelMessageCh.Location = new System.Drawing.Point(324, 52);
            this.uLabelMessageCh.Name = "uLabelMessageCh";
            this.uLabelMessageCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelMessageCh.TabIndex = 6;
            this.uLabelMessageCh.Text = "MessageCh";
            // 
            // uTextMessage
            // 
            this.uTextMessage.Location = new System.Drawing.Point(116, 52);
            this.uTextMessage.Name = "uTextMessage";
            this.uTextMessage.Size = new System.Drawing.Size(200, 21);
            this.uTextMessage.TabIndex = 5;
            // 
            // uLabelMessage
            // 
            this.uLabelMessage.Location = new System.Drawing.Point(12, 52);
            this.uLabelMessage.Name = "uLabelMessage";
            this.uLabelMessage.Size = new System.Drawing.Size(100, 20);
            this.uLabelMessage.TabIndex = 4;
            this.uLabelMessage.Text = "Message";
            // 
            // uTextMessageCode
            // 
            this.uTextMessageCode.Location = new System.Drawing.Point(116, 28);
            this.uTextMessageCode.Name = "uTextMessageCode";
            this.uTextMessageCode.Size = new System.Drawing.Size(200, 21);
            this.uTextMessageCode.TabIndex = 3;
            // 
            // uLabelMessageCode
            // 
            this.uLabelMessageCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelMessageCode.Name = "uLabelMessageCode";
            this.uLabelMessageCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelMessageCode.TabIndex = 2;
            this.uLabelMessageCode.Text = "MessageCode";
            // 
            // uGridCommonMessageList
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCommonMessageList.DisplayLayout.Appearance = appearance5;
            this.uGridCommonMessageList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCommonMessageList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonMessageList.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonMessageList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridCommonMessageList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonMessageList.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridCommonMessageList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCommonMessageList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCommonMessageList.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCommonMessageList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridCommonMessageList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCommonMessageList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCommonMessageList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCommonMessageList.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridCommonMessageList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCommonMessageList.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonMessageList.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridCommonMessageList.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridCommonMessageList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCommonMessageList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridCommonMessageList.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridCommonMessageList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCommonMessageList.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridCommonMessageList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCommonMessageList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCommonMessageList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCommonMessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCommonMessageList.Location = new System.Drawing.Point(0, 80);
            this.uGridCommonMessageList.Name = "uGridCommonMessageList";
            this.uGridCommonMessageList.Size = new System.Drawing.Size(1070, 690);
            this.uGridCommonMessageList.TabIndex = 5;
            // 
            // frmSYS0022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCommonMessageList);
            this.Controls.Add(this.uGroupBoxMessageInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0022";
            this.Load += new System.EventHandler(this.frmSYS0022_Load);
            this.Activated += new System.EventHandler(this.frmSYS0022_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSYS0022_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMessageInfo)).EndInit();
            this.uGroupBoxMessageInfo.ResumeLayout(false);
            this.uGroupBoxMessageInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMessageCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonMessageList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSearchLang;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLanguage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMessage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMessage;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxMessageInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMessageEn;
        private Infragistics.Win.Misc.UltraLabel uLabelMessageEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMessageCh;
        private Infragistics.Win.Misc.UltraLabel uLabelMessageCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMessage;
        private Infragistics.Win.Misc.UltraLabel uLabelMessage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMessageCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMessageCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCommonMessageList;
    }
}