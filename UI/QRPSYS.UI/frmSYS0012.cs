﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0012.cs                                         */
/* 프로그램명   : 시스템공통코드정보                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//Using추가

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSYS.UI
{
    public partial class frmSYS0012 : Form,IToolbar
    {
        //리소스정보 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0012()
        {
            InitializeComponent();
        }

        private void frmSYS0012_Activated(object sender, EventArgs e)
        {
            //활성된 툴바 기능 사용여부
            QRPBrowser ToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0012_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSYS0012_Resize);
        }

        void frmSYS0012_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridSysCommonCode.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridSysCommonCode.Dock = DockStyle.None;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 & 타이틀 설정

        /// <summary>
        /// 타이을설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("시스템공통코드정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그리드 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                //시스템공통코드정보 그리드 기본설정
                wGrid.mfInitGeneralGrid(this.uGridSysCommonCode, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //시스템공통코드정보 그리드 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComGubunCode", "구분코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComGubunName", "구분명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComGubunNameCh", "구분명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComGubunNameEn", "구분명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComCode", "공통코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComCodeName", "공통코드명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComCodeNameCh", "공통코드명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 140, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "ComCodeNameEn", "공통코드명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 140, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "DisplaySeq", "공통코드순서", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn", "0");

                wGrid.mfSetGridColumn(this.uGridSysCommonCode, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //그리드 폰트 설정
                this.uGridSysCommonCode.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSysCommonCode.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                // 공통코드 사용여부 콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridSysCommonCode, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridSysCommonCode, 0);

                this.uGridSysCommonCode.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSysCommonCode_AfterCellUpdate);
                this.uGridSysCommonCode.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSysCommonCode_CellChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }



        #endregion


        #region 툴바.

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //시스템공통코드 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                //시스템공통코드 정보 조회 매서드 호출

                DataTable dtCommonCode = clsCommonCode.mfReadCommonCode(m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridSysCommonCode.DataSource = dtCommonCode;
                this.uGridSysCommonCode.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtCommonCode.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                           Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK 수정불가 처리
                    for (int i = 0; i < this.uGridSysCommonCode.Rows.Count; i++)
                    {
                        this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSysCommonCode.Rows[i].Cells["ComCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSysCommonCode, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드의 줄이 0이면 데이터가 없음
                if (this.uGridSysCommonCode.Rows.Count == 0)
                {
                    
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000878", "M001041", Infragistics.Win.HAlign.Right);
                    return;
                    
                }

                #region 시스템공통코드 정보 저장

                //시스템공통코드 정보BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtCommmonCode = clsCommonCode.mfSetDataInfo();
                string strLang = m_resSys.GetString("SYS_LANG");
                //수정 하거나 신규 정보 저장

                if (this.uGridSysCommonCode.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridSysCommonCode.Rows.Count; i++)
                    {
                        // 편집이미지가 있는 행을 저장한다.
                        if (this.uGridSysCommonCode.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            #region 필수 입력 확인
                            string strRow = this.uGridSysCommonCode.Rows[i].RowSelectorNumber.ToString();

                            if (this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000491",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerformAction
                                this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"];
                                this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                                
                            }

                            if (this.uGridSysCommonCode.Rows[i].Cells["ComGubunName"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000492",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerformAction
                                this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComGubunName"];
                                this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }

                            if (this.uGridSysCommonCode.Rows[i].Cells["ComCode"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000483",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerformAction
                                this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComCode"];
                                this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }

                            if (this.uGridSysCommonCode.Rows[i].Cells["ComCodeName"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000484",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerformAction
                                this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComCodeName"];
                                this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }

                            #endregion

                            DataRow drCom = dtCommmonCode.NewRow();

                            drCom["ComGubunCode"] = this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString();     //구분코드
                            drCom["ComGubunName"] = this.uGridSysCommonCode.Rows[i].Cells["ComGubunName"].Value.ToString();     //구분명
                            drCom["ComGubunNameCh"] = this.uGridSysCommonCode.Rows[i].Cells["ComGubunNameCh"].Value.ToString(); //구분명중문
                            drCom["ComGubunNameEn"] = this.uGridSysCommonCode.Rows[i].Cells["ComGubunNameEn"].Value.ToString(); //구분명영문
                            drCom["ComCode"] = this.uGridSysCommonCode.Rows[i].Cells["ComCode"].Value.ToString();               //공통코드
                            drCom["ComCodeName"] = this.uGridSysCommonCode.Rows[i].Cells["ComCodeName"].Value.ToString();       //공통코드명
                            drCom["ComCodeNameCh"] = this.uGridSysCommonCode.Rows[i].Cells["ComCodeNameCh"].Value.ToString();   //공통코드명 중문
                            drCom["ComCodeNameEn"] = this.uGridSysCommonCode.Rows[i].Cells["ComCodeNameEn"].Value.ToString();   //공통코드명 영문
                            drCom["DisplaySeq"] = this.uGridSysCommonCode.Rows[i].Cells["DisplaySeq"].Value.ToString();         //공통코드순번
                            drCom["UseFlag"] = this.uGridSysCommonCode.Rows[i].Cells["UseFlag"].Value.ToString();               //사용여부

                            dtCommmonCode.Rows.Add(drCom);
                        }
                    }

                }

                if (dtCommmonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000878", "M001041", Infragistics.Win.HAlign.Right);
                    return;
                }
                #endregion

                // 저장여부 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //시스템공통코드 저장 매서드 실행
                    string strErrRtn = clsCommonCode.mfSaveCommonCode(dtCommmonCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);


                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드의 줄이 0이면 데이터가 없음
                if (this.uGridSysCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000878", "M000639", Infragistics.Win.HAlign.Right);
                    return;
                }


                #region 시스템 공통코드  정보저장

                //시스템공통코드 컬럼설정
                DataTable dtCommonCode = new DataTable();

                dtCommonCode.Columns.Add("ComGubunCode", typeof(string));   //구분코드
                dtCommonCode.Columns.Add("ComCode", typeof(string));        //공통코드


                string strLang = m_resSys.GetString("SYS_LANG");
                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                if(this.uGridSysCommonCode.Rows.Count > 0)
                    this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridSysCommonCode.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridSysCommonCode.Rows[i].Cells["Check"].Value) == true)
                    {
                        string strRow = this.uGridSysCommonCode.Rows[i].RowSelectorNumber.ToString();
                        //필수 입력 확인
                        if (this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000491",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                            //PerformAction
                            this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"];
                            this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                            return;
                        }

                        if (this.uGridSysCommonCode.Rows[i].Cells["ComCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000483",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                            //PerformAction
                            this.uGridSysCommonCode.ActiveCell = this.uGridSysCommonCode.Rows[i].Cells["ComCode"];
                            this.uGridSysCommonCode.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                            return;
                        }

                        //시스템공통코드 정보 저장
                        DataRow drCom = dtCommonCode.NewRow();

                        drCom["ComGubunCode"] = this.uGridSysCommonCode.Rows[i].Cells["ComGubunCode"].Value.ToString(); ;
                        drCom["ComCode"] = this.uGridSysCommonCode.Rows[i].Cells["ComCode"].Value.ToString(); ;
                        
                        dtCommonCode.Rows.Add(drCom);
                    }
                }

                #endregion

                //삭제정보저장 정보가 없는경우
                if (dtCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000878", "M000639", Infragistics.Win.HAlign.Right);
                    return;
                }

                //삭제여부 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //시스템 공통코드 정보 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                    QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                    brwChannel.mfCredentials(clsCommonCode);

                    //시스템 공통코드 삭제매서드 실행
                    string strErrRtn = clsCommonCode.mfDeleteCommonCode(dtCommonCode);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M000638", "M000676",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",strLang)
                                                         , msg.GetMessge_Text("M000638",strLang), ErrRtn.ErrMessage
                                                         , Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //-- 전체 행을 선택 하여 삭제한다 --//
                if (this.uGridSysCommonCode.Rows.Count > 0)
                {
                    this.uGridSysCommonCode.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridSysCommonCode.Rows.All);
                    this.uGridSysCommonCode.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSysCommonCode.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000808", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                WinGrid grd = new WinGrid();
                //처리 로직//

                if (this.uGridSysCommonCode.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridSysCommonCode);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfPrint()
        {
        }

        #endregion

        private void uGridSysCommonCode_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        private void frmSYS0012_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uGridSysCommonCode_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            if (grd.mfCheckCellDataInRow(this.uGridSysCommonCode, 0, e.Cell.Row.Index))
                e.Cell.Row.Delete(false);
        }
    }
}
