﻿namespace QRPSYS.UI
{
    partial class frmSYS0021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0021));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxLangInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLangEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLangEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLangCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLangCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLang = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLangID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLangID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionSearchLang = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelSearchLanguage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLang = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCommonLangList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLangInfo)).BeginInit();
            this.uGroupBoxLangInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonLangList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxLangInfo
            // 
            this.uGroupBoxLangInfo.Controls.Add(this.uTextLangEn);
            this.uGroupBoxLangInfo.Controls.Add(this.uLabelLangEn);
            this.uGroupBoxLangInfo.Controls.Add(this.uTextLangCh);
            this.uGroupBoxLangInfo.Controls.Add(this.uLabelLangCh);
            this.uGroupBoxLangInfo.Controls.Add(this.uTextLang);
            this.uGroupBoxLangInfo.Controls.Add(this.uLabelLang);
            this.uGroupBoxLangInfo.Controls.Add(this.uTextLangID);
            this.uGroupBoxLangInfo.Controls.Add(this.uLabelLangID);
            this.uGroupBoxLangInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxLangInfo.Location = new System.Drawing.Point(0, 770);
            this.uGroupBoxLangInfo.Name = "uGroupBoxLangInfo";
            this.uGroupBoxLangInfo.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxLangInfo.TabIndex = 3;
            // 
            // uTextLangEn
            // 
            this.uTextLangEn.Location = new System.Drawing.Point(740, 52);
            this.uTextLangEn.Name = "uTextLangEn";
            this.uTextLangEn.Size = new System.Drawing.Size(200, 21);
            this.uTextLangEn.TabIndex = 9;
            // 
            // uLabelLangEn
            // 
            this.uLabelLangEn.Location = new System.Drawing.Point(636, 52);
            this.uLabelLangEn.Name = "uLabelLangEn";
            this.uLabelLangEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelLangEn.TabIndex = 8;
            this.uLabelLangEn.Text = "LangID";
            // 
            // uTextLangCh
            // 
            this.uTextLangCh.Location = new System.Drawing.Point(428, 52);
            this.uTextLangCh.Name = "uTextLangCh";
            this.uTextLangCh.Size = new System.Drawing.Size(200, 21);
            this.uTextLangCh.TabIndex = 7;
            // 
            // uLabelLangCh
            // 
            this.uLabelLangCh.Location = new System.Drawing.Point(324, 52);
            this.uLabelLangCh.Name = "uLabelLangCh";
            this.uLabelLangCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelLangCh.TabIndex = 6;
            this.uLabelLangCh.Text = "LangID";
            // 
            // uTextLang
            // 
            this.uTextLang.Location = new System.Drawing.Point(116, 52);
            this.uTextLang.Name = "uTextLang";
            this.uTextLang.Size = new System.Drawing.Size(200, 21);
            this.uTextLang.TabIndex = 5;
            // 
            // uLabelLang
            // 
            this.uLabelLang.Location = new System.Drawing.Point(12, 52);
            this.uLabelLang.Name = "uLabelLang";
            this.uLabelLang.Size = new System.Drawing.Size(100, 20);
            this.uLabelLang.TabIndex = 4;
            this.uLabelLang.Text = "LangID";
            // 
            // uTextLangID
            // 
            this.uTextLangID.Location = new System.Drawing.Point(116, 28);
            this.uTextLangID.Name = "uTextLangID";
            this.uTextLangID.Size = new System.Drawing.Size(200, 21);
            this.uTextLangID.TabIndex = 3;
            // 
            // uLabelLangID
            // 
            this.uLabelLangID.Location = new System.Drawing.Point(12, 28);
            this.uLabelLangID.Name = "uLabelLangID";
            this.uLabelLangID.Size = new System.Drawing.Size(100, 20);
            this.uLabelLangID.TabIndex = 2;
            this.uLabelLangID.Text = "LangID";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSearchLang);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLanguage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLang);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLang);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uOptionSearchLang
            // 
            this.uOptionSearchLang.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSearchLang.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            this.uOptionSearchLang.ItemOrigin = new System.Drawing.Point(0, 4);
            valueListItem1.DataValue = "KOR";
            valueListItem1.DisplayText = "한글";
            valueListItem2.DataValue = "CHN";
            valueListItem2.DisplayText = "중문";
            valueListItem3.DataValue = "ENG";
            valueListItem3.DisplayText = "영문";
            this.uOptionSearchLang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.uOptionSearchLang.Location = new System.Drawing.Point(428, 12);
            this.uOptionSearchLang.Name = "uOptionSearchLang";
            this.uOptionSearchLang.Size = new System.Drawing.Size(200, 20);
            this.uOptionSearchLang.TabIndex = 3;
            this.uOptionSearchLang.TextIndentation = 2;
            this.uOptionSearchLang.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelSearchLanguage
            // 
            this.uLabelSearchLanguage.Location = new System.Drawing.Point(324, 12);
            this.uLabelSearchLanguage.Name = "uLabelSearchLanguage";
            this.uLabelSearchLanguage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLanguage.TabIndex = 2;
            this.uLabelSearchLanguage.Text = "선택";
            // 
            // uTextSearchLang
            // 
            this.uTextSearchLang.Location = new System.Drawing.Point(116, 12);
            this.uTextSearchLang.Name = "uTextSearchLang";
            this.uTextSearchLang.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchLang.TabIndex = 1;
            // 
            // uLabelSearchLang
            // 
            this.uLabelSearchLang.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchLang.Name = "uLabelSearchLang";
            this.uLabelSearchLang.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLang.TabIndex = 0;
            this.uLabelSearchLang.Text = "용어";
            // 
            // uGridCommonLangList
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCommonLangList.DisplayLayout.Appearance = appearance5;
            this.uGridCommonLangList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCommonLangList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridCommonLangList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCommonLangList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCommonLangList.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCommonLangList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridCommonLangList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCommonLangList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCommonLangList.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridCommonLangList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCommonLangList.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridCommonLangList.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridCommonLangList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCommonLangList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridCommonLangList.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridCommonLangList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCommonLangList.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridCommonLangList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCommonLangList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCommonLangList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCommonLangList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCommonLangList.Location = new System.Drawing.Point(0, 80);
            this.uGridCommonLangList.Name = "uGridCommonLangList";
            this.uGridCommonLangList.Size = new System.Drawing.Size(1070, 690);
            this.uGridCommonLangList.TabIndex = 6;
            // 
            // frmSYS0021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCommonLangList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBoxLangInfo);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0021";
            this.Load += new System.EventHandler(this.frmSYS0021_Load);
            this.Activated += new System.EventHandler(this.frmSYS0021_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSYS0021_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLangInfo)).EndInit();
            this.uGroupBoxLangInfo.ResumeLayout(false);
            this.uGroupBoxLangInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLangID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonLangList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxLangInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLangEn;
        private Infragistics.Win.Misc.UltraLabel uLabelLangEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLangCh;
        private Infragistics.Win.Misc.UltraLabel uLabelLangCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLang;
        private Infragistics.Win.Misc.UltraLabel uLabelLang;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLangID;
        private Infragistics.Win.Misc.UltraLabel uLabelLangID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSearchLang;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLanguage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLang;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLang;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCommonLangList;
    }
}