﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0010P1.cs                                       */
/* 프로그램명   : 다국어공통용어관리 POPUP                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-03-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPSYS.UI
{
    public partial class frmSYS0010P1 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        #region 전역변수 설정

        private string m_strLangID;
        private string m_strLang;
        private string m_strLangCh;
        private string m_strLangEn;

        public string LangID
        {
            get { return m_strLangID; }
            set { m_strLangID = value; }
        }

        public string Lang
        {
            get { return m_strLang; }
            set { m_strLang = value; }
        }

        public string LangCh
        {
            get { return m_strLangCh; }
            set { m_strLangCh = value; }
        }

        public string LangEn
        {
            get { return m_strLangEn; }
            set { m_strLangEn = value; }
        }

        #endregion

        public frmSYS0010P1()
        {
            InitializeComponent();
        }

        private void frmSYS0010P1_Load(object sender, EventArgs e)
        {
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("다국어공통용어관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            InitLabel();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
            grd.mfLoadGridColumnProperty(this);

            // 선택 OptionSet Log-In언어값으로 설정
            this.uOptionSearchLang.Value = m_resSys.GetString("SYS_LANG");
            this.uTextSearchLang.Focus();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        private void frmSYS0010P1_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method

        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinButton wButton = new QRPCOM.QRPUI.WinButton();

                wButton.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                wButton.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchLang, "용어", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLanguage, "선택", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCommonLangList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCommonLangList, 0, "LangID", "LangID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonLangList, 0, "Lang", "용어한글", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonLangList, 0, "LangCh", "용어중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonLangList, 0, "LangEn", "용어영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Font Size 설정
                this.uGridCommonLangList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCommonLangList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 편집불가상태로
                this.uGridCommonLangList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridCommonLangList.DisplayLayout.Bands[0].Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 조회 메소드
        /// </summary>
        private void Search()
        {
            try
            {
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult Result = new DialogResult();

                // 입력사항 확인
                if (this.uOptionSearchLang.CheckedIndex.Equals(-1))
                {
                    ////Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    ////    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////    , "확인창", "검색조건 확인", "한글, 중문, 영문중 하나를 선택해 주세요."
                    ////    , Infragistics.Win.HAlign.Right);

                    this.uOptionSearchLang.CheckedItem.DataValue = m_resSys.GetString("SYS_LANG");
                }
                string strLang = this.uOptionSearchLang.CheckedItem.DataValue.ToString();

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.CommonLang), "CommonLang");
                QRPSYS.BL.SYSLAN.CommonLang clsCLang = new QRPSYS.BL.SYSLAN.CommonLang();
                brwChannel.mfCredentials(clsCLang);

                //DataTable dtCLangList = clsCLang.mfReadSYSCommonLang(this.uTextSearchLang.Text, m_resSys.GetString("SYS_LANG"));
                DataTable dtCLangList = clsCLang.mfReadSYSCommonLang(this.uTextSearchLang.Text, strLang);

                this.uGridCommonLangList.SetDataBinding(dtCLangList, string.Empty);

                if (dtCLangList.Rows.Count > 0)
                {
                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridCommonLangList, 0);
                }
                else
                {
                    Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001099", "M001104", "M001102"
                        , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // 리스트 더블클릭이벤트
        private void uGridCommonLangList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                LangID = e.Row.Cells["LangID"].Value.ToString();
                Lang = e.Row.Cells["Lang"].Value.ToString();
                LangCh = e.Row.Cells["LangCh"].Value.ToString();
                LangEn = e.Row.Cells["LangEn"].Value.ToString();

                this.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchLang_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Search();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
