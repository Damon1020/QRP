﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0008.cs                                         */
/* 프로그램명   : 부서별 권한정보                                       */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-09-27                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-10-11 : 저장,삭제,엑셀출력 추가 (권종구)         */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//추가 Using

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using QRPSYS.BL.SYSPGM;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPSYS.UI
{
    public partial class frmSYS0008 : Form, IToolbar
    {
        //다국어 지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0008()
        {
            InitializeComponent();
        }

        private void frmSYS0008_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmSYS0008_Load(object sender, EventArgs e)
        {
            // 컨트롤 초기화
            SetToolAuth();
            InitCombo();
            InitGrid();
            InitGruopBox();
            InitLabel();
            InitTitle();

            //ExpandGroupBox 숨김
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSYS0008_Resize);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //타이틀설정
                titleArea.mfSetLabelText("부서별 권한정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGruopBox()
        {
            try
            {
                // SystemInfo ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                //그룹박스 컨트롤 설정
                wGroupBox.mfSetGroupBox(this.uGroupBoxMenu, GroupBoxType.LIST, "메뉴별 권한정보", m_resSys.GetString("SYS_FONTNAME")
                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //그룹박스 폰트설정
                this.uGroupBoxMenu.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxMenu.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 콤보박스 컨트롤 초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(plant);
                
                // 공장정보 콤보박스 매서드 실행
                DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //공정 콤보박스 리스트 & 컨트롤 설정
                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                //기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 레이블 컨트롤 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //Systme ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_LANG"), true, true);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelDept, "부서", m_resSys.GetString("SYS_LANG"), true, false);

                lbl.mfSetLabel(this.uLabelSearchDeptCode, "부서코드", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelSearchDeptName, "부서명", m_resSys.GetString("SYS_LANG"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 컨트롤 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            { 
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //uGridDept 기본 설정
                grd.mfInitGeneralGrid(this.uGridDept, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //uGridDept 컬럼 설정
                grd.mfSetGridColumn(this.uGridDept, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "DeptNameCh", "부서명(중문)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "DeptNameEn", "부서명(영문)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDept, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////uGridMenu 기본 설정
                ////grd.mfInitGeneralGrid(this.uGridMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                ////    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                ////    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                ////    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //////uGridMenu 컬럼 설정
                ////grd.mfSetGridColumn(this.uGridMenu, 0, "ProgramID", "ProgramID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "ProgramName", "ProgramName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "SearchFlag", "검색", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "SaveFlag", "저장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "DeleteFlag", "삭제", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "PrintFlag", "출력", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////grd.mfSetGridColumn(this.uGridMenu, 0, "ExcelFlag", "엑셀", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //그리드 폰트설정
                this.uGridDept.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDept.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //this.uGridMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                //this.uGridMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                //this.uGridMenu.DisplayLayout.Bands[0].Columns["UserFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
                //this.uGridMenu.DisplayLayout.Bands[0].Columns["SearchFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
                //this.uGridMenu.DisplayLayout.Bands[0].Columns["SaveFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
                //this.uGridMenu.DisplayLayout.Bands[0].Columns["DeleteFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
                //this.uGridMenu.DisplayLayout.Bands[0].Columns["PrintFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
                //this.uGridMenu.DisplayLayout.Bands[0].Columns["ExcelFlag"].Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;

                this.uGridDept.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridDept_DoubleClickRow);
                this.uGridMenu.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridMenu_AfterCellUpdate);
                this.uGridMenu.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridMenu_CellChange);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                if (intBandIndex == 0)
                {
                    //메뉴별 권한정보 그리드 기본설정
                    wGrid.mfInitGeneralGrid(this.uGridMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                        , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                    
                    this.uGridMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                    this.uGridMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                }
                //메뉴별 권한정보 그리드 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "UpperProgramID", "UpperProgramID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "SearchFlag", "검색", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "SaveFlag", "저장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "DeleteFlag", "삭제", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "PrintFlag", "출력", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ExcelFlag", "엑셀", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //계층레벨추가
                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// Text초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                this.uTextPlantName.Text = "";
                this.uTextDeptCode.Text = "";
                this.uTextDeptName.Text = "";
                this.uTextPlantCode.Clear();

                
                //-- 전체 행을 선택 하여 삭제한다 --//
                if (this.uGridMenu.Rows.Count > 0)
                {
                    this.uGridMenu.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridMenu.Rows.All);
                    this.uGridMenu.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //공장 미선택시 메세지 박스를 띄우고 이벤트발생
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001235", "M000266", Infragistics.Win.HAlign.Right);

                    //DropDown
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                String strSearchPlantcode = this.uComboSearchPlant.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                DataTable dtDept = new DataTable();
                dtDept = clsDept.mfReadSYSDept_Like(strSearchPlantcode,this.uTextSearchDeptCode.Text,this.uTextSearchDeptName.Text,m_resSys.GetString("SYS_LANG"));

                this.uGridDept.DataSource = dtDept;
                this.uGridDept.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult Result = new DialogResult();
                
                if (dtDept.Rows.Count == 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDept, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //ExpandGroupBox가 숨겨져있거나 공장텍스트가 공백일 경우 저장할 정보 없음.
                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextPlantCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 메뉴별권한정보 저장

                //부서별 권한정보 DataColumn정보가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.DeptAuth), "DeptAuth");
                QRPSYS.BL.SYSPGM.DeptAuth clsDeptAuth = new QRPSYS.BL.SYSPGM.DeptAuth();
                brwChannel.mfCredentials(clsDeptAuth);

                DataTable dtDeptAuth = clsDeptAuth.mfSetDataInfo();
                DataTable dtSave = new DataTable();

                if (this.uGridMenu.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridMenu.ActiveCell = this.uGridMenu.Rows[0].Cells[0];

                    Infragistics.Win.UltraWinGrid.RowsCollection rows = this.uGridMenu.Rows;
                    dtSave = mfGetAllRowsGridWithBand(rows, dtDeptAuth);                    
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001264", "M001047",
                             Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                //저장 여부 메세지를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //부서별 권한정보 저장 매서드 실행
                    //string strErrRtn = clsDeptAuth.mfSaveDeptAuth(dtDeptAuth, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    string strErrRtn = clsDeptAuth.mfSaveDeptAuth(dtSave, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    
                    // Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private DataTable mfGetAllRowsGridWithBand(Infragistics.Win.UltraWinGrid.RowsCollection rows, DataTable dtFormat)
        {
            DataTable dtRtn = new DataTable();
            dtRtn = dtFormat;
            try
            {
                foreach (Infragistics.Win.UltraWinGrid.UltraGridRow row in rows)
                {
                    DataRow dr = dtFormat.NewRow();
                    
                    dr["PlantCode"] = this.uTextPlantCode.Text;                                     //공장
                    dr["DeptCode"] = this.uTextDeptCode.Text;                                       //부서
                    dr["ProgramID"] = row.Cells["ProgramID"].Value.ToString();                      //프로그램ID
                    dr["UseFlag"] = row.Cells["UseFlag"].Value.ToString().Substring(0, 1);          //사용여부
                    dr["SerachFlag"] = row.Cells["SearchFlag"].Value.ToString().Substring(0, 1);    //조회
                    dr["SaveFlag"] = row.Cells["SaveFlag"].Value.ToString().Substring(0, 1);        //저장
                    dr["DeleteFlag"] = row.Cells["DeleteFlag"].Value.ToString().Substring(0, 1);    //삭제
                    dr["PrintFlag"] = row.Cells["PrintFlag"].Value.ToString().Substring(0, 1);      //프린트
                    dr["ExcelFlag"] = row.Cells["ExcelFlag"].Value.ToString().Substring(0, 1);      //엑셀출력
                    dtRtn.Rows.Add(dr);

                    //-- Chind Band의 Row 저장 --//
                    if (row.ChildBands != null)
                    {
                        foreach (Infragistics.Win.UltraWinGrid.UltraGridChildBand childband in row.ChildBands)
                        {
                            Infragistics.Win.UltraWinGrid.RowsCollection childrows = childband.Rows;
                            DataTable dt = new DataTable();
                            if (childrows != null)
                                dt = mfGetAllRowsGridWithBand(childrows, dtFormat);
                        }
                    }
                }
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //ExpandGeoupBox가 숨김상태거나 공장정보가 없거나 부서정보가 없는 경우 삭제 불가능
                if (this.uGroupBoxContentsArea.Expanded.Equals(false) || this.uTextPlantCode.Text.Equals(string.Empty) || this.uTextDeptCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }

                //공장코드, 부서코드 저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strDeptCode = this.uTextDeptCode.Text;

                //삭제 여부 메세지 박스를 띄운다. 삭제를 원하면 삭제 한다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //부서별 권한정보 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.DeptAuth), "DeptAuth");
                    QRPSYS.BL.SYSPGM.DeptAuth clsDeptAuth = new QRPSYS.BL.SYSPGM.DeptAuth();
                    brwChannel.mfCredentials(clsDeptAuth);

                    //부서별 권한정보 삭제 매서드 실행
                    string strErrRtn = clsDeptAuth.mfDeleteDeptAuth(strPlantCode, strDeptCode);


                    // strErrRtn Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라서 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{
                //    this.uGroupBoxContentsArea.Expanded = true;
                //}
                //else
                //{
                //    InitText();
                //    this.uGridDept.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDept.Rows.All);
                //    this.uGridDept.DeleteSelectedRows(false);
                //}

                
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                if (this.uGridDept.Rows.Count == 0 && this.uGridMenu.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000808", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid wGrid = new WinGrid();
                if (this.uGridDept.Rows.Count > 0)
                    wGrid.mfDownLoadGridToExcel(this.uGridDept);

                if (this.uGridMenu.Rows.Count > 0)
                    wGrid.mfDownLoadGridToExcel(this.uGridMenu);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        public void mfPrint()
        {
            try
            { }
            catch (Exception ex)
            { }
            finally
            { }
        }

        #endregion
 

        #region 이벤트

        //ExpandGroupBox 숨김 펼침 에 따라 위치,가 변한다.
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 140);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDept.Height = 40;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDept.Height = 740;

                    for (int i = 0; i < uGridDept.Rows.Count; i++)
                    {
                        this.uGridDept.Rows[i].Fixed = false;
                    }

                    //InitText();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //셀 정보 변경 시 편집이미지가 나타난다.
        private void uGridMenu_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            this.uGridMenu.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
        }

        // 부서정보 더블 클릭 시 Expand GeoupBox안에 해당 부서의 권한 설정을 할 수있다.
        private void uGridDept_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                //선택한 줄의 내용이 있는 경우 
                //if (!grd.mfCheckCellDataInRow(this.uGridMenu, 0, e.Row.Index))
                //{
                    // ExpandGroupBox가 숨겨져있으면 펼친다.
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                    }
                    //선택한 줄고정
                    e.Row.Fixed = true;

                    string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                    string strDeptCode = e.Row.Cells["DeptCode"].Value.ToString();

                    this.uTextPlantCode.Text = strPlantCode;
                    this.uTextPlantName.Text = e.Row.Cells["PlantName"].Value.ToString();
                    this.uTextDeptCode.Text = e.Row.Cells["DeptCode"].Value.ToString();
                    this.uTextDeptName.Text = e.Row.Cells["DeptName"].Value.ToString();

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.DeptAuth), "DeptAuth");
                    QRPSYS.BL.SYSPGM.DeptAuth deptAuth = new QRPSYS.BL.SYSPGM.DeptAuth();
                    brwChannel.mfCredentials(deptAuth);

                    DataTable dtAuth = new DataTable();
                    dtAuth = deptAuth.mfReadDeptAuth(strPlantCode, strDeptCode, m_resSys.GetString("SYS_LANG"));

                    DataSet ds = grd.mfCreateHierarchicalDataSet(dtAuth, "ProgramID", "UpperProgramID");
                    this.uGridMenu.DataSource = ds;

                    //this.uGridMenu.DataSource = dtAuth;
                    //this.uGridMenu.DataBind();
                    for (int i = 0; i < this.uGridMenu.DisplayLayout.Bands.Count; i++)
                    {
                        ChangeGridColumn(i);
                        brwChannel.mfSetFormLanguage(this, this.uGridMenu);
                        if (i > 0)
                            uGridMenu.DisplayLayout.Bands[i].ColHeadersVisible = false;
                    }

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();
                    if (dtAuth.Rows.Count == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        //WinGrid grd = new WinGrid();
                        grd.mfSetAutoResizeColWidth(this.uGridMenu, 0);
                    }
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

       

        //폼 닫기전에 그리드 설정값 저장
        private void frmSYS0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmSYS0008_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGridMenu_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.HasChild())
                {
                    string strValue = e.Cell.Value.ToString();
                    string strKey = e.Cell.Column.Key;
                    CheckChild(e.Cell.Row.ChildBands, strKey, strValue);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void CheckChild(Infragistics.Win.UltraWinGrid.ChildBandsCollection ChildBands, string strKey, string strValue)
        {
            foreach (Infragistics.Win.UltraWinGrid.UltraGridChildBand uChildBand in ChildBands)
            {
                CheckChild(uChildBand, strKey, strValue);
            }
        }

        private void CheckChild(Infragistics.Win.UltraWinGrid.UltraGridChildBand uChildBand, string strKey, string strValue)
        {
            foreach (Infragistics.Win.UltraWinGrid.UltraGridRow uGRow in uChildBand.Rows)
            {
                uGRow.Cells[strKey].Value = strValue;

                if (uGRow.HasChild())
                {
                    CheckChild(uGRow.ChildBands, strKey, strValue);
                }
            }
        }
    }
}
