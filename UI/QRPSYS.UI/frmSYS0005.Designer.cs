﻿namespace QRPSYS.UI
{
    partial class frmSYS0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0005));
            this.uGridSysMenu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextMenuOrder = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextMenuLevel = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextProgramNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProgramNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUpperProgramName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModuleID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUpperProgramID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProgramName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProgramID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProgramNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgramNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMenuOrder = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMenuLevel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModuleID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUpperProgram = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgramName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgramID = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSysMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMenuOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMenuLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperProgramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModuleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperProgramID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramID)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridSysMenu
            // 
            this.uGridSysMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSysMenu.DisplayLayout.Appearance = appearance1;
            this.uGridSysMenu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSysMenu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSysMenu.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSysMenu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridSysMenu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSysMenu.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridSysMenu.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSysMenu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSysMenu.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSysMenu.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridSysMenu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSysMenu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSysMenu.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSysMenu.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridSysMenu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSysMenu.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSysMenu.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridSysMenu.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridSysMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSysMenu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridSysMenu.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridSysMenu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSysMenu.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridSysMenu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSysMenu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSysMenu.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSysMenu.Location = new System.Drawing.Point(0, 40);
            this.uGridSysMenu.Name = "uGridSysMenu";
            this.uGridSysMenu.Size = new System.Drawing.Size(1070, 660);
            this.uGridSysMenu.TabIndex = 1;
            this.uGridSysMenu.Text = "ultraGrid1";
            this.uGridSysMenu.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridSysMenu_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 138);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 704);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 138);
            this.uGroupBoxContentsArea.TabIndex = 2;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMenuOrder);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMenuLevel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProgramNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProgramNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUpperProgramName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModuleID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUpperProgramID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProgramName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProgramID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProgramNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProgramNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMenuOrder);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMenuLevel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModuleID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUpperProgram);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProgramName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProgramID);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 118);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextMenuOrder
            // 
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Text = "1";
            this.uTextMenuOrder.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMenuOrder.ButtonsRight.Add(spinEditorButton1);
            this.uTextMenuOrder.Location = new System.Drawing.Point(416, 84);
            this.uTextMenuOrder.MaxValue = 200;
            this.uTextMenuOrder.MinValue = 1;
            this.uTextMenuOrder.Name = "uTextMenuOrder";
            this.uTextMenuOrder.PromptChar = ' ';
            this.uTextMenuOrder.Size = new System.Drawing.Size(100, 21);
            this.uTextMenuOrder.TabIndex = 9;
            this.uTextMenuOrder.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.MenuLevelOrder_EditorSpinButtonClick);
            this.uTextMenuOrder.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.MenuLevelAndOrder_EditorButtonClick);
            // 
            // uTextMenuLevel
            // 
            appearance20.BackColor = System.Drawing.Color.White;
            this.uTextMenuLevel.Appearance = appearance20;
            this.uTextMenuLevel.BackColor = System.Drawing.Color.White;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Text = "1";
            this.uTextMenuLevel.ButtonsLeft.Add(editorButton2);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMenuLevel.ButtonsRight.Add(spinEditorButton2);
            this.uTextMenuLevel.Location = new System.Drawing.Point(136, 84);
            this.uTextMenuLevel.MaxValue = 100;
            this.uTextMenuLevel.MinValue = 1;
            this.uTextMenuLevel.Name = "uTextMenuLevel";
            this.uTextMenuLevel.PromptChar = ' ';
            this.uTextMenuLevel.ReadOnly = true;
            this.uTextMenuLevel.Size = new System.Drawing.Size(100, 21);
            this.uTextMenuLevel.TabIndex = 8;
            this.uTextMenuLevel.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.MenuLevelOrder_EditorSpinButtonClick);
            this.uTextMenuLevel.ValueChanged += new System.EventHandler(this.uTextMenuLevel_ValueChanged);
            this.uTextMenuLevel.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.MenuLevelAndOrder_EditorButtonClick);
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(416, 60);
            this.uComboUseFlag.MaxLength = 5;
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(120, 21);
            this.uComboUseFlag.TabIndex = 6;
            // 
            // uTextProgramNameEn
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramNameEn.Appearance = appearance16;
            this.uTextProgramNameEn.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramNameEn.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProgramNameEn.Location = new System.Drawing.Point(692, 36);
            this.uTextProgramNameEn.MaxLength = 100;
            this.uTextProgramNameEn.Name = "uTextProgramNameEn";
            this.uTextProgramNameEn.Size = new System.Drawing.Size(120, 21);
            this.uTextProgramNameEn.TabIndex = 4;
            // 
            // uTextProgramNameCh
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramNameCh.Appearance = appearance15;
            this.uTextProgramNameCh.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramNameCh.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProgramNameCh.Location = new System.Drawing.Point(416, 36);
            this.uTextProgramNameCh.MaxLength = 100;
            this.uTextProgramNameCh.Name = "uTextProgramNameCh";
            this.uTextProgramNameCh.Size = new System.Drawing.Size(120, 21);
            this.uTextProgramNameCh.TabIndex = 3;
            // 
            // uTextUpperProgramName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperProgramName.Appearance = appearance17;
            this.uTextUpperProgramName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperProgramName.Location = new System.Drawing.Point(816, 60);
            this.uTextUpperProgramName.Name = "uTextUpperProgramName";
            this.uTextUpperProgramName.ReadOnly = true;
            this.uTextUpperProgramName.Size = new System.Drawing.Size(120, 21);
            this.uTextUpperProgramName.TabIndex = 1;
            // 
            // uTextModuleID
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextModuleID.Appearance = appearance19;
            this.uTextModuleID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextModuleID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextModuleID.Location = new System.Drawing.Point(136, 60);
            this.uTextModuleID.MaxLength = 50;
            this.uTextModuleID.Name = "uTextModuleID";
            this.uTextModuleID.Size = new System.Drawing.Size(120, 21);
            this.uTextModuleID.TabIndex = 5;
            this.uTextModuleID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextModuleID_KeyDown);
            // 
            // uTextUpperProgramID
            // 
            appearance18.Image = global::QRPSYS.UI.Properties.Resources.btn_Zoom;
            appearance18.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance18;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUpperProgramID.ButtonsRight.Add(editorButton3);
            this.uTextUpperProgramID.Location = new System.Drawing.Point(692, 60);
            this.uTextUpperProgramID.MaxLength = 100;
            this.uTextUpperProgramID.Name = "uTextUpperProgramID";
            this.uTextUpperProgramID.Size = new System.Drawing.Size(120, 21);
            this.uTextUpperProgramID.TabIndex = 7;
            this.uTextUpperProgramID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUpperProgramID_EditorButtonClick);
            // 
            // uTextProgramName
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramName.Appearance = appearance14;
            this.uTextProgramName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProgramName.Location = new System.Drawing.Point(136, 36);
            this.uTextProgramName.MaxLength = 100;
            this.uTextProgramName.Name = "uTextProgramName";
            this.uTextProgramName.Size = new System.Drawing.Size(120, 21);
            this.uTextProgramName.TabIndex = 2;
            // 
            // uTextProgramID
            // 
            appearance13.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramID.Appearance = appearance13;
            this.uTextProgramID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProgramID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProgramID.Location = new System.Drawing.Point(136, 12);
            this.uTextProgramID.MaxLength = 100;
            this.uTextProgramID.Name = "uTextProgramID";
            this.uTextProgramID.Size = new System.Drawing.Size(120, 21);
            this.uTextProgramID.TabIndex = 1;
            // 
            // uLabelProgramNameEn
            // 
            this.uLabelProgramNameEn.Location = new System.Drawing.Point(568, 36);
            this.uLabelProgramNameEn.Name = "uLabelProgramNameEn";
            this.uLabelProgramNameEn.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgramNameEn.TabIndex = 0;
            this.uLabelProgramNameEn.Text = "ultraLabel1";
            // 
            // uLabelProgramNameCh
            // 
            this.uLabelProgramNameCh.Location = new System.Drawing.Point(292, 36);
            this.uLabelProgramNameCh.Name = "uLabelProgramNameCh";
            this.uLabelProgramNameCh.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgramNameCh.TabIndex = 0;
            this.uLabelProgramNameCh.Text = "ultraLabel1";
            // 
            // uLabelMenuOrder
            // 
            this.uLabelMenuOrder.Location = new System.Drawing.Point(292, 84);
            this.uLabelMenuOrder.Name = "uLabelMenuOrder";
            this.uLabelMenuOrder.Size = new System.Drawing.Size(120, 20);
            this.uLabelMenuOrder.TabIndex = 0;
            this.uLabelMenuOrder.Text = "ultraLabel1";
            // 
            // uLabelMenuLevel
            // 
            this.uLabelMenuLevel.Location = new System.Drawing.Point(12, 84);
            this.uLabelMenuLevel.Name = "uLabelMenuLevel";
            this.uLabelMenuLevel.Size = new System.Drawing.Size(120, 20);
            this.uLabelMenuLevel.TabIndex = 0;
            this.uLabelMenuLevel.Text = "ultraLabel1";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(292, 60);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(120, 20);
            this.uLabelUseFlag.TabIndex = 0;
            this.uLabelUseFlag.Text = "ultraLabel1";
            // 
            // uLabelModuleID
            // 
            this.uLabelModuleID.Location = new System.Drawing.Point(12, 60);
            this.uLabelModuleID.Name = "uLabelModuleID";
            this.uLabelModuleID.Size = new System.Drawing.Size(120, 20);
            this.uLabelModuleID.TabIndex = 0;
            this.uLabelModuleID.Text = "ultraLabel1";
            // 
            // uLabelUpperProgram
            // 
            this.uLabelUpperProgram.Location = new System.Drawing.Point(568, 60);
            this.uLabelUpperProgram.Name = "uLabelUpperProgram";
            this.uLabelUpperProgram.Size = new System.Drawing.Size(120, 20);
            this.uLabelUpperProgram.TabIndex = 0;
            this.uLabelUpperProgram.Text = "ultraLabel1";
            // 
            // uLabelProgramName
            // 
            this.uLabelProgramName.Location = new System.Drawing.Point(12, 36);
            this.uLabelProgramName.Name = "uLabelProgramName";
            this.uLabelProgramName.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgramName.TabIndex = 0;
            this.uLabelProgramName.Text = "ultraLabel1";
            // 
            // uLabelProgramID
            // 
            this.uLabelProgramID.Location = new System.Drawing.Point(12, 12);
            this.uLabelProgramID.Name = "uLabelProgramID";
            this.uLabelProgramID.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgramID.TabIndex = 0;
            this.uLabelProgramID.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmSYS0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridSysMenu);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0005";
            this.Load += new System.EventHandler(this.frmSYS0005_Load);
            this.Activated += new System.EventHandler(this.frmSYS0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSYS0005_FormClosing);
            this.Resize += new System.EventHandler(this.frmSYS0005_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSysMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMenuOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMenuLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperProgramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModuleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperProgramID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgramID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSysMenu;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperProgramName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperProgramID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgramID;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelUpperProgram;
        private Infragistics.Win.Misc.UltraLabel uLabelProgramName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModuleID;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelModuleID;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextMenuOrder;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextMenuLevel;
        private Infragistics.Win.Misc.UltraLabel uLabelMenuOrder;
        private Infragistics.Win.Misc.UltraLabel uLabelMenuLevel;
    }
}