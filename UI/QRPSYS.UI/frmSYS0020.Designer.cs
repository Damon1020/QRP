﻿namespace QRPSYS.UI
{
    partial class frmSYS0020
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0020));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPasswordOK = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPasswordOK = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPassword = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPasswordOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1054, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uTextUserID
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserID.Appearance = appearance1;
            this.uTextUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserID.Location = new System.Drawing.Point(120, 52);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.ReadOnly = true;
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 17;
            // 
            // uLabelUser
            // 
            this.uLabelUser.Location = new System.Drawing.Point(16, 52);
            this.uLabelUser.Name = "uLabelUser";
            this.uLabelUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelUser.TabIndex = 16;
            this.uLabelUser.Text = "ultraLabel1";
            // 
            // uTextUserName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance33;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(224, 52);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 18;
            // 
            // uTextPasswordOK
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPasswordOK.Appearance = appearance28;
            this.uTextPasswordOK.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPasswordOK.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPasswordOK.Location = new System.Drawing.Point(120, 100);
            this.uTextPasswordOK.Name = "uTextPasswordOK";
            this.uTextPasswordOK.PasswordChar = '*';
            this.uTextPasswordOK.Size = new System.Drawing.Size(150, 21);
            this.uTextPasswordOK.TabIndex = 21;
            // 
            // uTextPassword
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPassword.Appearance = appearance27;
            this.uTextPassword.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPassword.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPassword.Location = new System.Drawing.Point(120, 76);
            this.uTextPassword.Name = "uTextPassword";
            this.uTextPassword.PasswordChar = '*';
            this.uTextPassword.Size = new System.Drawing.Size(150, 21);
            this.uTextPassword.TabIndex = 22;
            // 
            // uLabelPasswordOK
            // 
            this.uLabelPasswordOK.Location = new System.Drawing.Point(16, 100);
            this.uLabelPasswordOK.Name = "uLabelPasswordOK";
            this.uLabelPasswordOK.Size = new System.Drawing.Size(100, 20);
            this.uLabelPasswordOK.TabIndex = 19;
            this.uLabelPasswordOK.Text = "ultraLabel2";
            // 
            // uLabelPassword
            // 
            this.uLabelPassword.Location = new System.Drawing.Point(16, 76);
            this.uLabelPassword.Name = "uLabelPassword";
            this.uLabelPassword.Size = new System.Drawing.Size(100, 20);
            this.uLabelPassword.TabIndex = 20;
            this.uLabelPassword.Text = "ultraLabel2";
            // 
            // frmSYS0020
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1054, 814);
            this.ControlBox = false;
            this.Controls.Add(this.uTextPasswordOK);
            this.Controls.Add(this.uTextPassword);
            this.Controls.Add(this.uLabelPasswordOK);
            this.Controls.Add(this.uLabelPassword);
            this.Controls.Add(this.uTextUserName);
            this.Controls.Add(this.uTextUserID);
            this.Controls.Add(this.uLabelUser);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0020";
            this.Load += new System.EventHandler(this.frmSYS0020_Load);
            this.Activated += new System.EventHandler(this.frmSYS0020_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPasswordOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPasswordOK;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPassword;
        private Infragistics.Win.Misc.UltraLabel uLabelPasswordOK;
        private Infragistics.Win.Misc.UltraLabel uLabelPassword;
    }
}