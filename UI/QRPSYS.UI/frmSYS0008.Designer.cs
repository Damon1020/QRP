﻿namespace QRPSYS.UI
{
    partial class frmSYS0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0008));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchDeptCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDeptName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDeptCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDept = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxMenu = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridMenu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDeptCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDept = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDeptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMenu)).BeginInit();
            this.uGroupBoxMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDeptName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDeptCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDeptName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDeptCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uTextSearchDeptName
            // 
            this.uTextSearchDeptName.Location = new System.Drawing.Point(696, 12);
            this.uTextSearchDeptName.MaxLength = 50;
            this.uTextSearchDeptName.Name = "uTextSearchDeptName";
            this.uTextSearchDeptName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDeptName.TabIndex = 4;
            // 
            // uTextSearchDeptCode
            // 
            this.uTextSearchDeptCode.Location = new System.Drawing.Point(456, 12);
            this.uTextSearchDeptCode.MaxLength = 10;
            this.uTextSearchDeptCode.Name = "uTextSearchDeptCode";
            this.uTextSearchDeptCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDeptCode.TabIndex = 4;
            // 
            // uLabelSearchDeptName
            // 
            this.uLabelSearchDeptName.Location = new System.Drawing.Point(592, 12);
            this.uLabelSearchDeptName.Name = "uLabelSearchDeptName";
            this.uLabelSearchDeptName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDeptName.TabIndex = 3;
            // 
            // uLabelSearchDeptCode
            // 
            this.uLabelSearchDeptCode.Location = new System.Drawing.Point(352, 12);
            this.uLabelSearchDeptCode.Name = "uLabelSearchDeptCode";
            this.uLabelSearchDeptCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDeptCode.TabIndex = 3;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPlant.TabIndex = 2;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            // 
            // uGridDept
            // 
            this.uGridDept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDept.DisplayLayout.Appearance = appearance17;
            this.uGridDept.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDept.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDept.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDept.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridDept.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDept.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridDept.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDept.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDept.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDept.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridDept.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDept.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDept.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDept.DisplayLayout.Override.CellAppearance = appearance18;
            this.uGridDept.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDept.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDept.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance24.TextHAlignAsString = "Left";
            this.uGridDept.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridDept.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDept.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridDept.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridDept.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDept.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.uGridDept.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDept.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDept.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDept.Location = new System.Drawing.Point(0, 80);
            this.uGridDept.Name = "uGridDept";
            this.uGridDept.Size = new System.Drawing.Size(1070, 720);
            this.uGridDept.TabIndex = 5;
            this.uGridDept.Text = "ultraGrid1";
            this.uGridDept.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridDept_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxMenu);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDeptName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDeptCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDept);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxMenu
            // 
            this.uGroupBoxMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxMenu.Controls.Add(this.uGridMenu);
            this.uGroupBoxMenu.Location = new System.Drawing.Point(12, 44);
            this.uGroupBoxMenu.Name = "uGroupBoxMenu";
            this.uGroupBoxMenu.Size = new System.Drawing.Size(1040, 617);
            this.uGroupBoxMenu.TabIndex = 5;
            // 
            // uGridMenu
            // 
            this.uGridMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMenu.DisplayLayout.Appearance = appearance6;
            this.uGridMenu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMenu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMenu.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMenu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridMenu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMenu.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridMenu.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMenu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMenu.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMenu.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridMenu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMenu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMenu.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMenu.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridMenu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMenu.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMenu.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.uGridMenu.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.uGridMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMenu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.uGridMenu.DisplayLayout.Override.RowAppearance = appearance28;
            this.uGridMenu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMenu.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.uGridMenu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMenu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMenu.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMenu.Location = new System.Drawing.Point(12, 12);
            this.uGridMenu.Name = "uGridMenu";
            this.uGridMenu.Size = new System.Drawing.Size(1020, 592);
            this.uGridMenu.TabIndex = 0;
            this.uGridMenu.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMenu_AfterCellUpdate);
            this.uGridMenu.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMenu_CellChange);
            // 
            // uTextDeptName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Appearance = appearance2;
            this.uTextDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Location = new System.Drawing.Point(452, 12);
            this.uTextDeptName.Name = "uTextDeptName";
            this.uTextDeptName.ReadOnly = true;
            this.uTextDeptName.Size = new System.Drawing.Size(100, 21);
            this.uTextDeptName.TabIndex = 4;
            // 
            // uTextDeptCode
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptCode.Appearance = appearance4;
            this.uTextDeptCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptCode.Location = new System.Drawing.Point(348, 12);
            this.uTextDeptCode.Name = "uTextDeptCode";
            this.uTextDeptCode.ReadOnly = true;
            this.uTextDeptCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDeptCode.TabIndex = 3;
            // 
            // uLabelDept
            // 
            this.uLabelDept.Location = new System.Drawing.Point(242, 12);
            this.uLabelDept.Name = "uLabelDept";
            this.uLabelDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelDept.TabIndex = 2;
            // 
            // uTextPlantCode
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance14;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(556, 12);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 1;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextPlantName
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance30;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(116, 12);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantName.TabIndex = 1;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // frmSYS0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridDept);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0008";
            this.Load += new System.EventHandler(this.frmSYS0008_Load);
            this.Activated += new System.EventHandler(this.frmSYS0008_Activated);
            this.Resize += new System.EventHandler(this.frmSYS0008_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDeptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMenu)).EndInit();
            this.uGroupBoxMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDept;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelDept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxMenu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeptCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMenu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDeptName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDeptCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDeptCode;
    }
}