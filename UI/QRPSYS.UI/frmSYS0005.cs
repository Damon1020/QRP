﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0005.cs                                         */
/* 프로그램명   : 메뉴정보                                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.Xml;

namespace QRPSYS.UI
{
    public partial class frmSYS0005 : Form , IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0005()
        {
            InitializeComponent();
        }

        private void frmSYS0005_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser ();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0005_Load(object sender, EventArgs e)
        {
            //컨트롤 초기화(초기 설정해준다.)
            SetToolAuth();
            InitTitle();
            InitLable();
            InitCombo();
            InitGrid();
            InitExpandGroup();

            //this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfLoadGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        
        /// <summary>
        /// 타이틀 초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceinFo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("메뉴정보", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLable()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelProgramID, "프로그램ID", m_resSys.GetString("SYS_FONTNAME"), true, true);    //ProgramID

                wLabel.mfSetLabel(this.uLabelProgramName, "프로그램명", m_resSys.GetString("SYS_FONTNAME"), true, true);  //ProgramName
                wLabel.mfSetLabel(this.uLabelProgramNameCh, "프로그램명 중문", m_resSys.GetString("SYS_FONTNAME"), true, true);//ProgramNameCh
                wLabel.mfSetLabel(this.uLabelProgramNameEn, "프로그램명 영문", m_resSys.GetString("SYS_FONTNAME"), true, true);//ProgramNameEn

                wLabel.mfSetLabel(this.uLabelUpperProgram, "상위프로그램", m_resSys.GetString("SYS_FONTNAME"), true, false);//UpperProgram

                wLabel.mfSetLabel(this.uLabelModuleID, "모듈ID", m_resSys.GetString("SYS_FONTNAME"), true, true);     //ModuleID

                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);     //UseFlag
                wLabel.mfSetLabel(this.uLabelMenuLevel, "메뉴레벨", m_resSys.GetString("SYS_FONTNAME"), true, false);   //MenuLevel
                wLabel.mfSetLabel(this.uLabelMenuOrder, "메뉴순서", m_resSys.GetString("SYS_FONTNAME"), true, false);   //MenuOrder

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 공통코드 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                //공통코드 (사용여부)조회 매서드 호출
                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                // 사용여부 콤보박스에 추가 //
                WinComboEditor wCom = new WinComboEditor();
                wCom.mfSetComboEditor(this.uComboUseFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                    , "", "", "선택", "ComCode", "ComCodeName", dtUseFlag);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                int intBandIndex = 0;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                // 메뉴 정보 그리드 기본설정 //
                wGrd.mfInitGeneralGrid(this.uGridSysMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "ProgramNameCh", "프로그램명 중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "ProgramNameEn", "프로그램명 영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //계층레벨추가
                wGrd.mfSetGridColumn(this.uGridSysMenu, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //컬럼 설정후 그리드의 폰트사이즈를 9로한다.
                this.uGridSysMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSysMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                //wGrd.mfInitGeneralGrid(this.uGridSysMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                //    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                //    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                //    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "ProgramNameCh", "프로그램명 중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "ProgramNameEn", "프로그램명 영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //계층레벨추가
                wGrd.mfChangeGridColumnStyle(this.uGridSysMenu, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region 툴바

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //if(this.uGroupBoxContentsArea.Expanded == true)
                //{
                //    this.uGroupBoxContentsArea.Expanded = false;
                //}
                InitExpandGroup();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                WinMessageBox msg = new WinMessageBox();


                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //메뉴정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(clsMenu);

                //메뉴정보 조회 매서드호출
                DataTable dtMenu = new DataTable("Menu");
                dtMenu = clsMenu.mfReadMenu(m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
                DataSet ds = grd.mfCreateHierarchicalDataSet(dtMenu, "ProgramID", "UpperProgramID");
                this.uGridSysMenu.DataSource = ds;
                
                //Binding한 후 DataSet의 Band마다 Header의 컬럼속성을 지정함.
                for (int i = 0; i < uGridSysMenu.DisplayLayout.Bands.Count;i++ )
                {
                    ChangeGridColumn(i);
                    brwChannel.mfSetFormLanguage(this, this.uGridSysMenu);
                    // 부모 Band가 없는경우 -- 최상위 Band
                    if (uGridSysMenu.DisplayLayout.Bands[i].ParentBand == null)
                    {
                        uGridSysMenu.DisplayLayout.Bands[i].ColHeadersVisible = true;
                    }
                    // 하위 Band
                    else
                    {
                        // Header 보이느냐 마느냐
                        uGridSysMenu.DisplayLayout.Bands[i].ColHeadersVisible = false;
                    }
                    
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if(dtMenu.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridSysMenu, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                // -------------------------------------  필 수 입 력 사 항 체 크 -----------------------------------//
                if (this.uTextProgramID.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001217", Infragistics.Win.HAlign.Right);
                    
                    //Focus
                    this.uTextProgramID.Focus();
                    return;

                }
                else if (this.uTextProgramName.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001220", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextProgramName.Focus();
                    return;

                }
                else if (this.uTextProgramNameCh.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001219", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextProgramNameCh.Focus();
                    return;
                }
                else if (this.uTextProgramNameEn.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001218", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextProgramNameEn.Focus();
                    return;
                }
                else if (this.uTextModuleID.Text.Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000405", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextModuleID.Focus();
                    return;
                }
                //메뉴 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(clsMenu);

                //메뉴 컬럼정보 가져오기
                DataTable dtMenu = clsMenu.mfSetDataInfo();

                //메뉴 정보 저장 
                DataRow drMenu;
                drMenu = dtMenu.NewRow();
                drMenu["ProgramID"] = this.uTextProgramID.Text.Trim();          //프로그램ID
                drMenu["ProgramName"] = this.uTextProgramName.Text.Trim();      //프로그램명
                drMenu["ProgramNameCh"] = this.uTextProgramNameCh.Text.Trim();  //프로그램명 중문
                drMenu["ProgramNameEn"] = this.uTextProgramNameEn.Text.Trim();  //프로그램명 영문
                drMenu["ModuleID"] = this.uTextModuleID.Text.Trim();            //모듈ID
                drMenu["UpperProgramID"] = this.uTextUpperProgramID.Text.Trim();//상위프로그램ID
                drMenu["UseFlag"] = this.uComboUseFlag.Value.ToString();        //사용여부
                drMenu["MenuLevel"] = this.uTextMenuLevel.Value.ToString();     //메뉴레벨
                drMenu["MenuOrder"] = this.uTextMenuOrder.Value.ToString();     //메뉴순서
                dtMenu.Rows.Add(drMenu);

                //  저장 여부 메세지 박스를 띄운다 ..
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 팝업창을 띄우고 마우스를 변경한다.
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //메뉴정보 저장 매서드 호출 //
                    string strErrRtn = clsMenu.mfSaveMenu(dtMenu, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //마우스커서를 기본값으로 바꾸고 팝업창을 닫는다.
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    
                    // 처리 결과에 따라 메세지 박스를 띄운다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        //mfSearch();
                        InitExpandGroup();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
               
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextProgramID.ReadOnly == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uTextProgramID.Text == "")
                {
                    
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001217", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextProgramID.Focus();
                    return;
                    
                }

                //프로그램아이디저장
                string strProgramID = this.uTextProgramID.Text;

                // 삭제여부 메세지박스를 띄운다.//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 팝업창을띄우고 마우스커서를 변경한다
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    //메뉴정보BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                    QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                    brwChannel.mfCredentials(clsMenu);

                    //메뉴정보 삭제 매서드 호출
                    string strErrRtn = clsMenu.mfDeleteMenu(strProgramID);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 마우스커서를 기본값으로 바꾸고 팝업창을 닫는다.
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{

                //    this.uGroupBoxContentsArea.Expanded = true;
                //}
                //if (this.uGridSysMenu.Rows.Count > 0)
                //{
                //    //-- 전체 행을 선택 하여 삭제한다 --//
                //    this.uGridSysMenu.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridSysMenu.Rows.All);
                //    this.uGridSysMenu.DeleteSelectedRows(false);
                //}
                InitExpandGroup();
                this.uTextProgramID.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSysMenu.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000808", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                WinGrid grd = new WinGrid();

                //처리 로직//
                if (this.uGridSysMenu.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridSysMenu);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        #endregion

        #region 이벤트

        // ExpandGroupBox가 펼쳐지거나 접혀질때 크기와 위치가 바뀐다.
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                ////if (uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    Point point = new Point(0, 105);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridSysMenu.Height = 60;
                ////}
                ////else
                ////{
                ////    Point point = new Point(0, 810);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridSysMenu.Height = 770;
                ////    for (int i = 0; i < this.uGridSysMenu.Rows.Count; i++)
                ////    {
                ////        this.uGridSysMenu.Rows[i].Fixed = false;
                ////    }
                ////    InitExpandGroup();
                ////}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 스핀버튼을 클릭하였을 때 Value값이 증가하거나 감소된다.
        private void MenuLevelOrder_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                //-- 스핀버튼
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > (int)ed.MinValue)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //상위프로그램 텍스트박스 에디터버튼을 클릭 시 메뉴정보를 보여준다.
        private void uTextUpperProgramID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // 모듈ID 저장
                string strModuleID = this.uTextModuleID.Text.Trim();

                //메뉴정보 팝업창을 띄운다
                frmSYS0005D1 frmMenuPOP = new frmSYS0005D1();
                frmMenuPOP.ShowDialog();


                if (frmMenuPOP.ModuleID != "" && strModuleID != frmMenuPOP.ModuleID)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000895", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    this.uTextUpperProgramID.Text = frmMenuPOP.ProgramID;
                    this.uTextUpperProgramName.Text = frmMenuPOP.ProgramName;


                    SearchMenuOrder();
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드의 셀을 더블클릭할 시 클릭한 줄에 정보가 있는경우 ExpandGroupBox안에 정보가 나온다 .
        private void uGridSysMenu_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ////// ExpandGroupBox 닫혀있으면 펼친다
                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    this.uGroupBoxContentsArea.Expanded = true;
                ////}
                ////// 선택한 줄을 고정시킨다.
                ////e.Cell.Row.Fixed = true;
                

                //////선택한줄의 정보가 있을경우 ExpandGroupbox 안에 정보가 뿌려진다
                ////QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                ////if (grd.mfCheckCellDataInRow(this.uGridSysMenu, 0, e.Cell.Row.Index) == false)
                ////{
                ////    this.uTextProgramID.ReadOnly = true;
                ////    this.uTextModuleID.Text = e.Cell.Row.Cells["ModuleID"].Value.ToString();
                ////    this.uTextProgramID.Text = e.Cell.Row.Cells["ProgramID"].Value.ToString(); 
                ////    this.uTextProgramName.Text = e.Cell.Row.Cells["ProgramName"].Value.ToString();
                ////    this.uTextProgramNameCh.Text = e.Cell.Row.Cells["ProgramNameCh"].Value.ToString();
                ////    this.uTextProgramNameEn.Text = e.Cell.Row.Cells["ProgramNameEn"].Value.ToString();
                ////    this.uTextUpperProgramID.Text = e.Cell.Row.Cells["UpperProgramID"].Value.ToString();
                ////    this.uTextUpperProgramName.Text = e.Cell.Row.Cells["UpperProgramName"].Value.ToString();
                ////    this.uComboUseFlag.Value = e.Cell.Row.Cells["UseFlag"].Value;
                ////    this.uTextMenuLevel.Value = e.Cell.Row.Cells["MenuLevel"].Value;
                ////    this.uTextMenuOrder.Value = e.Cell.Row.Cells["MenuOrder"].Value;
                ////}

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridSysMenu, 0, e.Cell.Row.Index) == false)
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                    QRPSYS.BL.SYSPGM.Menu clsMn = new QRPSYS.BL.SYSPGM.Menu();
                    brwChannel.mfCredentials(clsMn);

                    string strProgramID = e.Cell.Row.Cells["ProgramID"].Value.ToString();
                    string strLang = m_resSys.GetString("SYS_LANG");

                    DataTable dtMenuDetail = clsMn.mfReadMenu_Detail(strProgramID, strLang);

                    if (dtMenuDetail.Rows.Count > 0)
                    {
                        this.uTextProgramID.ReadOnly = true;
                        this.uTextProgramID.Appearance.BackColor = Color.Gainsboro;
                        this.uTextModuleID.Text = dtMenuDetail.Rows[0]["ModuleID"].ToString();
                        this.uTextProgramID.Text = dtMenuDetail.Rows[0]["ProgramID"].ToString();
                        this.uTextProgramName.Text = dtMenuDetail.Rows[0]["ProgramName"].ToString();
                        this.uTextProgramNameCh.Text = dtMenuDetail.Rows[0]["ProgramNameCh"].ToString();
                        this.uTextProgramNameEn.Text = dtMenuDetail.Rows[0]["ProgramNameEn"].ToString();
                        this.uTextUpperProgramID.Text = dtMenuDetail.Rows[0]["UpperProgramID"].ToString();
                        this.uTextUpperProgramName.Text = dtMenuDetail.Rows[0]["UpperProgramName"].ToString();
                        this.uComboUseFlag.Value = dtMenuDetail.Rows[0]["UseFlag"];
                        this.uTextMenuLevel.Value = dtMenuDetail.Rows[0]["MenuLevel"];
                        this.uTextMenuOrder.Value = dtMenuDetail.Rows[0]["MenuOrder"];
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //메뉴레벨 정보가 바뀔 때마다 이벤트발생
        private void uTextMenuLevel_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SearchMenuOrder();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// ExpandGroupBox 안의 컨트롤 초기화
        /// </summary>
        private void InitExpandGroup()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextProgramID.ReadOnly = false;
                this.uTextProgramID.Appearance.BackColor = Color.PowderBlue;

                this.uTextModuleID.Text = "";
                this.uTextProgramID.Text = "";
                this.uTextProgramName.Text = "";
                this.uTextProgramNameCh.Text = "";
                this.uTextProgramNameEn.Text = "";
                this.uTextUpperProgramID.Text = "";
                this.uTextUpperProgramName.Text = "";
                this.uComboUseFlag.Value = "T";
                this.uTextMenuLevel.Value = 1;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메뉴순번조회
        /// </summary>
        private void SearchMenuOrder()
        {
            try
            {
                //상위프로그램이 입력이 되어있고 메뉴레벨이 0 아닐 때 
                if (this.uTextMenuLevel.Value.ToString() != "0" && this.uTextModuleID.Text.Trim() != "")// && this.uTextUpperProgramID.Text != "" && this.uTextUpperProgramName.Text != "")
                {
                    //메뉴레벨 상위프로그램아이디 저장
                    string strMenuLevel = this.uTextMenuLevel.Value.ToString();
                    string strUpperProgramID = this.uTextUpperProgramID.Text;

                    // 메뉴정보 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                    QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                    brwChannel.mfCredentials(clsMenu);
                    //메뉴순번 조회 매서드 호출
                    DataTable dtMenuOrder = clsMenu.mfReadMenuOrder(strUpperProgramID, strMenuLevel);

                    //메뉴순번 저장
                    this.uTextMenuOrder.Value = dtMenuOrder.Rows[0]["MenuOrder"];

                }
                else
                {
                    this.uTextMenuOrder.Value = 1;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSYS0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void MenuLevelAndOrder_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

            // 값을 1로 초기화
            ed.Value = 1;
        }

        private void uTextModuleID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextUpperProgramID.Clear();
                    this.uTextUpperProgramName.Clear();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void frmSYS0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }        
    }
}
