﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0010.cs                                         */
/* 프로그램명   : 화면별 다국어설정                                     */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가 Using
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using QRPSYS.BL.SYSPGM;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Xml;

namespace QRPSYS.UI
{
    public partial class frmSYS0010 : Form , IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0010()
        {
            InitializeComponent();
        }

        private void frmSYS0010_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser ToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0010_Load(object sender, EventArgs e)
        {
            //화면 초기화
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitButton();
            InitGrid();
            
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        //폼이 닫히기 전에 그리드 설정 저장
        private void frmSYS0010_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀 설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 타이틀 설정
                titleArea.mfSetLabelText("화면별 다국어설정", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //레이블 컨트롤 Text,폰트,필수,이미지,처리
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelProgramID, "프로그램ID", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProgramName, "프로그램명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 그룹박스 컨트롤초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGrb = new WinGroupBox();

                //그룹박스 속성지정
                wGrb.mfSetGroupBox(this.uGroupBoxLang, GroupBoxType.LIST, "컨트롤별 다국어정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //GroupBox 폰트설정
                this.uGroupBoxLang.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxLang.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton wBtn = new WinButton();
                wBtn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                // 이벤트
                this.uButtonDeleteRow.Click += new EventHandler(uButtonDeleteRow_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 컨트롤초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                int intBandIndex = 0;
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                // 메뉴 정보 그리드 기본설정 //
                wGrd.mfInitGeneralGrid(this.uGridMenuList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "ProgramNameCh", "프로그램명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "ProgramNameEn", "프로그램명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //계층레벨추가
                wGrd.mfSetGridColumn(this.uGridMenuList, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                // 그리드 편집불가상태로
                this.uGridMenuList.DisplayLayout.Bands[intBandIndex].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;


                //컨트롤별 다국어정보 그리드 기본설정
                wGrd.mfInitGeneralGrid(this.uGridProgramLanguage, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컨트롤별 다국어정보 그리드 컬럼설정
                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "ControlName", "ControlName", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "ControlKey", "ControlKey", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "LangID", "LangID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "Lang", "용어한글", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "LangCh", "용어중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridProgramLanguage, 0, "LangEn", "용어영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //그리드 폰트크기 설정
                this.uGridMenuList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMenuList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridProgramLanguage.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProgramLanguage.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 이벤트
                this.uGridProgramLanguage.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridProgramLanguage_AfterCellUpdate);
                this.uGridProgramLanguage.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridProgramLanguage_CellChange);
                this.uGridProgramLanguage.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridProgramLanguage_ClickCellButton);
                this.uGridMenuList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridMenuList_DoubleClickRow);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "ProgramNameCh", "프로그램명 중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "ProgramNameEn", "프로그램명 영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //계층레벨추가
                wGrd.mfChangeGridColumnStyle(this.uGridMenuList, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region ToolBar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //메뉴정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(clsMenu);

                //메뉴정보 조회 매서드 호출
                DataTable dtMenu = clsMenu.mfReadMenu(m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridMenuList.DataSource = dtMenu;
                this.uGridMenuList.DataBind();

                QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
                DataSet ds = grd.mfCreateHierarchicalDataSet(dtMenu, "ProgramID", "UpperProgramID");
                this.uGridMenuList.DataSource = ds;

                //Binding한 후 DataSet의 Band마다 Header의 컬럼속성을 지정함.
                for (int i = 0; i < uGridMenuList.DisplayLayout.Bands.Count; i++)
                {
                    ChangeGridColumn(i);
                    brwChannel.mfSetFormLanguage(this, this.uGridMenuList);
                    // 부모 Band가 없는경우 -- 최상위 Band
                    if (uGridMenuList.DisplayLayout.Bands[i].ParentBand == null)
                    {
                        uGridMenuList.DisplayLayout.Bands[i].ColHeadersVisible = true;
                    }
                    // 하위 Band
                    else
                    {
                        // Header 보이느냐 마느냐
                        uGridMenuList.DisplayLayout.Bands[i].ColHeadersVisible = false;
                    }
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtMenu.Rows.Count.Equals(0))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                         Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                #region 필수입력사항 확인 및 저장용 데이터 테이블 설정

                if (this.uTextProgramID.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001329"
                                                , "M001281"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                // 컨트롤별 언어설정BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.ProgramLang), "ProgramLang");
                QRPSYS.BL.SYSLAN.ProgramLang clsProgLang = new QRPSYS.BL.SYSLAN.ProgramLang();
                brwChannel.mfCredentials(clsProgLang);

                // 컬럼설정
                DataTable dtSaveProgLang = clsProgLang.mfSetDataInfo();
                DataTable dtDelProgLang = clsProgLang.mfSetDataInfo();

                // 프로그램ID 저장
                string strProgramID = this.uTextProgramID.Text;

                this.uGridProgramLanguage.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                string strLang = m_resSys.GetString("SYS_LANG");

                // 프로그램 컨트롤별 언어설정 그리드정보 저장
                if (this.uGridProgramLanguage.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridProgramLanguage.Rows.Count; i++)
                    {
                        // 행삭제 여부 확인
                        if (this.uGridProgramLanguage.Rows[i].Hidden.Equals(false))
                        {
                            // 저장의 경우 편집이미지가 있는 행만 저장
                            if (this.uGridProgramLanguage.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                // 필수 입력확인
                                if (this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString().Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001285", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Focus
                                    this.uGridProgramLanguage.ActiveCell = this.uGridProgramLanguage.Rows[i].Cells["ControlName"];
                                    this.uGridProgramLanguage.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else if (this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString().Contains("uGrid") &&
                                        this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Value.ToString().Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001284", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Focus
                                    this.uGridProgramLanguage.ActiveCell = this.uGridProgramLanguage.Rows[i].Cells["ControlKey"];
                                    this.uGridProgramLanguage.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else
                                {
                                    DataRow drRow = dtSaveProgLang.NewRow();
                                    drRow["ProgramID"] = this.uTextProgramID.Text;
                                    drRow["ControlName"] = this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString();
                                    ////if (this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString().Contains("Grid"))
                                    ////    drRow["ControlKey"] = this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Value.ToString();
                                    ////else
                                    ////    drRow["ControlKey"] = string.Empty;
                                    drRow["ControlKey"] = this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Value.ToString();
                                    drRow["LangID"] = this.uGridProgramLanguage.Rows[i].Cells["LangID"].Value.ToString();
                                    dtSaveProgLang.Rows.Add(drRow);
                                }
                            }
                        }
                        else
                        {
                            // 삭제
                            // ControlName이 있는경우 데이터 테이블에 저장
                            if (!this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString().Equals(string.Empty))
                            {
                                DataRow drRow = dtDelProgLang.NewRow();
                                drRow["ProgramID"] = this.uTextProgramID.Text;
                                drRow["ControlName"] = this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Value.ToString();
                                drRow["ControlKey"] = this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Value.ToString();
                                dtDelProgLang.Rows.Add(drRow);
                            }
                        }
                    }
                }

                if (!(dtSaveProgLang.Rows.Count > 0) && !(dtDelProgLang.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001034"
                                                , "M001047"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                // 저장여부를 묻는 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //컨트롤별 언어설정 저장 매서드 실행
                    string strErrRtn = clsProgLang.mfSaveSYSProgramLang(dtSaveProgLang, dtDelProgLang, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn) ;

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        // 초기화
                        Clear();

                        CreateXML();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M001037", "M000953",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                         , msg.GetMessge_Text("M001037",m_resSys.GetString("SYS_LANG"))
                                                         , ErrRtn.ErrMessage,
                                                         Infragistics.Win.HAlign.Right);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uTextProgramID.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001329"
                                                , "M001332"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                //삭제 프로그램ID 저장
                string strProgramID = this.uTextProgramID.Text;

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //컨트롤 언어별설정 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.ProgramLang), "ProgramLang");
                    QRPSYS.BL.SYSLAN.ProgramLang clsProgLang = new QRPSYS.BL.SYSLAN.ProgramLang();
                    brwChannel.mfCredentials(clsProgLang);

                    //컨트롤 언어별 설정 삭제 매서드 호출
                    string strErrRtn = clsProgLang.mfDeleteSYSProgramLang(strProgramID);
                    
                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    if (ErrRtn.ErrNum == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        Clear();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M000638", "M000676",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                         , msg.GetMessge_Text("M000638",m_resSys.GetString("SYS_LANG"))
                                                         , ErrRtn.ErrMessage,
                                                         Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (!(this.uGridProgramLanguage.Rows.Count > 0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000808", "M001326", Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    if (this.uGridProgramLanguage.Rows.Count > 0)
                        grd.mfDownLoadGridToExcel(this.uGridProgramLanguage);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            
        }
        
        #endregion


        #region 이벤트

        //프로그램 컨트롤 언어설정 그리드의 선택된 줄을 숨김처리한다.
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridProgramLanguage.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridProgramLanguage.Rows[i].Cells["Check"].Value))
                    {
                        this.uGridProgramLanguage.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }
        
        //컨트롤 언어설정 그리드 셀변경시 편집이미지가 나타난다.
        private void uGridProgramLanguage_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //컨트롤 언어설정 그리드 의 줄의 내용이없을 시 줄삭제
        private void uGridProgramLanguage_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridProgramLanguage, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //헤더그리드 더블 클릭 시 해당 줄의 정보를 ExpandGroupBox에 보여준다.
        private void uGridMenuList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //프로그램 ID 저장
                string strProgramID = e.Row.Cells["ProgramID"].Value.ToString();

                // 폼인경우에만
                if (strProgramID.Contains("frm"))
                {
                    //텍스트컨트롤에 해당 정보 삽입
                    this.uTextProgramID.Text = strProgramID;
                    this.uTextProgramName.Text = e.Row.Cells["ProgramName"].Value.ToString();

                    //다국어설정 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.ProgramLang), "ProgramLang");
                    QRPSYS.BL.SYSLAN.ProgramLang clsProgLang = new QRPSYS.BL.SYSLAN.ProgramLang();
                    brwChannel.mfCredentials(clsProgLang);

                    //다국어설정 정보 조회 매서드 호출
                    DataTable dtProgLang = clsProgLang.mfReadSYSProgramLang(strProgramID, m_resSys.GetString("SYS_LANG"));

                    //컨트롤 다국어설정 그리드에 바인드.
                    this.uGridProgramLanguage.SetDataBinding(dtProgLang, string.Empty);

                    if (dtProgLang.Rows.Count > 0)
                    {
                        //PK 수정불가 처리
                        for (int i = 0; i < this.uGridProgramLanguage.Rows.Count; i++)
                        {
                            this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            this.uGridProgramLanguage.Rows[i].Cells["ControlName"].Appearance.BackColor = Color.Gainsboro;
                            this.uGridProgramLanguage.Rows[i].Cells["ControlKey"].Appearance.BackColor = Color.Gainsboro;
                        }
                    }
                    this.uGridProgramLanguage.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                }
                else
                {
                    // 하위정보가 있는경우 Expand
                    if (e.Row.HasChild())
                    {
                        e.Row.Expanded = true;
                    }

                    Clear();
                    this.uGridProgramLanguage.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridProgramLanguage_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("LangID"))
                {
                    frmSYS0010P1 frmPOP = new frmSYS0010P1();
                    frmPOP.ShowDialog();

                    if (!string.IsNullOrEmpty(frmPOP.LangID))
                    {
                        e.Cell.Row.Cells["LangID"].Value = frmPOP.LangID;
                        e.Cell.Row.Cells["Lang"].Value = frmPOP.Lang;
                        e.Cell.Row.Cells["LangCh"].Value = frmPOP.LangCh;
                        e.Cell.Row.Cells["LangEn"].Value = frmPOP.LangEn;

                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        
                        // 공백줄추가
                        if(e.Cell.Row.IsTemplateAddRow)
                            this.uGridProgramLanguage.DisplayLayout.Bands[0].AddNew();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextProgramID.Clear();
                this.uTextProgramName.Clear();

                //-- 전체 행을 선택 하여 삭제한다 --//
                if (this.uGridProgramLanguage.Rows.Count > 0)
                {
                    this.uGridProgramLanguage.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridProgramLanguage.Rows.All);
                    this.uGridProgramLanguage.DeleteSelectedRows(false);
                }

                this.uGridProgramLanguage.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void CreateXML()
        {
            try
            {
                #region 다국어 XML 파일 생성
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.ProgramLang), "ProgramLang");
                QRPSYS.BL.SYSLAN.ProgramLang clsProgramLang = new QRPSYS.BL.SYSLAN.ProgramLang();
                brwChannel.mfCredentials(clsProgramLang);

                DataTable dtRtn = clsProgramLang.mfReadSYSProgramLangForXML();

                XmlDocument xmlRtn = new XmlDocument();
                xmlRtn.LoadXml("<?MultiLanguageDocument for=QRPBrowser.exe ?><Root>" + dtRtn.Rows[0][0].ToString() + "</Root>");

                XmlNode nodeLang = xmlRtn.CreateElement("Lang");
                xmlRtn.SelectSingleNode("/Root").AppendChild(nodeLang);
                XmlNodeList xmlLang = xmlRtn.SelectNodes("/Root/*");

                foreach (XmlNode node in xmlLang)
                {
                    if (node.Name.Equals("Program"))
                        xmlRtn.SelectSingleNode("/Root/Lang").AppendChild(node);
                }

                System.IO.FileInfo fileLang = new System.IO.FileInfo(Application.StartupPath + "\\MultiLang.xml");

                if (!fileLang.Exists)
                {
                    xmlRtn.Save(Application.StartupPath + "\\MultiLang.xml");
                    fileLang = new System.IO.FileInfo(Application.StartupPath + "\\MultiLang.xml");
                    fileLang.IsReadOnly = true;
                }
                else
                {
                    XmlDocument xmlMultiLang = new XmlDocument();
                    xmlMultiLang.Load(fileLang.FullName);

                    DateTime dtOld = DateTime.Parse(xmlMultiLang.SelectSingleNode("Root/MaxModifyDate").InnerText);
                    DateTime dtNew = DateTime.Parse(xmlRtn.SelectSingleNode("Root/MaxModifyDate").InnerText);

                    if (dtNew > dtOld)
                    {
                        fileLang.IsReadOnly = false;
                        fileLang.Delete();
                        xmlRtn.Save(Application.StartupPath + "\\MultiLang.xml");
                        fileLang = new System.IO.FileInfo(Application.StartupPath + "\\MultiLang.xml");
                        fileLang.IsReadOnly = true;
                    }
                }

                clsProgramLang.Dispose();
                dtRtn.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSYS0010_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    this.ultraGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    this.uGridMenuList.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    this.ultraGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGridMenuList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

                if (this.Height > 850)
                {
                    this.ultraGroupBox1.Height = this.Height - this.titleArea.Height - this.uGridMenuList.Height;
                }
                else
                {
                    this.ultraGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGridMenuList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
