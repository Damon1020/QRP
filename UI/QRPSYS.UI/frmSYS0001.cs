﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 사용자관리                                            */
/* 프로그램ID   : frmSYS0001.cs                                         */
/* 프로그램명   : 부서정보                                              */
/* 작성자       : 윤경희                                                */
/* 작성일자     : 2011-09-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

//Serial통신 테스트
using System.IO.Ports;

namespace QRPSYS.UI
{
    public partial class frmSYS0001 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        
        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";


        //Serial COM Port 변수
        private SerialPort spCOM1;

        public frmSYS0001()
        {
            InitializeComponent();
        }

        private void frmSYS0001_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0001_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("부서정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetRunMode();
            SetToolAuth();
            // 컨트롤 초기화
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            if (m_resSys.GetString("SYS_USERID") != "testusr")
                uGroupBoxCOM.Visible = false;

            uComboCOMPort.SelectedIndex = 0;
            uComboBaudRate.SelectedIndex = 1;
            uComboDataBit.SelectedIndex = 0;
            uComboParity.SelectedIndex = 0;
            uComboStopBit.SelectedIndex = 3;
            uComboHandShake.SelectedIndex = 0;
            uOptionCOMRead.Value = 1;
            uTextData.Text = "";

            this.Resize += new EventHandler(frmSYS0001_Resize);
        }

        void frmSYS0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridDeptList.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridDeptList.Dock = DockStyle.None;
                    ////Point p = new Point(0, 80);
                    ////this.uGridDeptList.Location = p;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 1.컨트롤 초기화

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel winLabel = new WinLabel();
                winLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 검색조건 : 공장
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 검색 리스트 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDeptList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "DeptNameCh", "부서명 중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "DeptNameEn", "부서명 영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDeptList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridDeptList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeptList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 2.ToolBar 정보
        // 검색

        public void mfSearch()
        {
            try
            {                
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                String strSearchPlantcode = this.uComboSearchPlant.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);
                
                DataTable dtDept = new DataTable();
                dtDept = clsDept.mfReadSYSDept(strSearchPlantcode, m_resSys.GetString("SYS_LANG"));

                this.uGridDeptList.DataSource = dtDept;
                this.uGridDeptList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtDept.Rows.Count == 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDeptList, 0);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장

        public void mfSave()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }
        
        // 삭제        
        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        // 신규
        public void mfCreate()
        {
        }

        // 출력
        public void mfPrint()
        {
        }

        // 엑셀다운
        public void mfExcel()
        {
            //처리 로직//
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridDeptList);
        }

        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            try
            {
                QRPSYS.BL.SYSUSR.Dept clsDept;
                if (m_bolDebugMode == false)
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                    clsDept = new QRPSYS.BL.SYSUSR.Dept();
                    brwChannel.mfCredentials(clsDept);
                }
                else
                    clsDept = new QRPSYS.BL.SYSUSR.Dept(m_strDBConn);

                //string strResult = clsDept.mfSaveWMSTestDept();

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region COM 통신하기
        private void OpenSerialCOM(int intPortNum, int intBaudRate, System.IO.Ports.Parity nParity, System.IO.Ports.StopBits nStopBit, int intDataBit, System.IO.Ports.Handshake nHandShake)
        {
            try
            {

                spCOM1 = new SerialPort();
                spCOM1.PortName = "COM" + intPortNum.ToString();
                spCOM1.BaudRate = intBaudRate;
                spCOM1.Parity = nParity;
                spCOM1.StopBits = nStopBit;
                spCOM1.DataBits = intDataBit;
                spCOM1.Handshake = nHandShake;
                if (spCOM1.IsOpen) spCOM1.Close();
                spCOM1.Open();

                spCOM1.DataReceived += new SerialDataReceivedEventHandler(spCOM1_DataReceived);

                if (spCOM1.IsOpen)
                    MessageBox.Show("성공적으로 연결되었습니다.");
                else
                    MessageBox.Show("연결을 하지 못했습니다.");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void spCOM1_DataReceived(object sender, SerialDataReceivedEventArgs s)
        {
            try
            {
                if (uOptionCOMRead.Value.ToString() == "1")
                {
                    //읽기방법1)
                    //this.Invoke();
                    int intLength = 0;
                    intLength = spCOM1.BytesToRead;
                    byte[] btTemp = new byte[intLength];
                    spCOM1.Read(btTemp, 0, intLength);
                    if (intLength > 0)
                    {
                        for (int i = 0; i < intLength; i++)
                        {
                            uTextData.Text = uTextData.Text + string.Format("{0:x}", btTemp[i]);
                            uTextData.Text = uTextData.Text + uTextData.Text.Length.ToString();
                        }
                    }
                }


                if (uOptionCOMRead.Value.ToString() == "2")
                {
                    //읽기방법2)
                    uTextData.Text = uTextData.Text + string.Format("{0:x}", spCOM1.ReadByte());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButCOMOpen_Click(object sender, EventArgs e)
        {
            try
            {
                //Serial통신을 위한 초기화//

                int intCOMPort = Convert.ToInt32(uComboCOMPort.SelectedItem.DataValue.ToString());
                int intBaudRate = Convert.ToInt32(uComboBaudRate.SelectedItem.DataValue.ToString());

                Parity nParity = Parity.Even;
                if (uComboParity.SelectedItem.DataValue.ToString() == "Parity.Even")
                    nParity = Parity.Even;
                else if (uComboParity.SelectedItem.DataValue.ToString() == "Parity.Mark")
                    nParity = Parity.Mark;
                else if (uComboParity.SelectedItem.DataValue.ToString() == "Parity.None")
                    nParity = Parity.None;
                else if (uComboParity.SelectedItem.DataValue.ToString() == "Parity.Odd")
                    nParity = Parity.Odd;
                else if (uComboParity.SelectedItem.DataValue.ToString() == "Parity.Space")
                    nParity = Parity.Space;

                StopBits nStopBit = StopBits.None;
                if (uComboStopBit.SelectedItem.DataValue.ToString() == "StopBits.None")
                    nStopBit = StopBits.None;
                else if (uComboStopBit.SelectedItem.DataValue.ToString() == "StopBits.One")
                    nStopBit = StopBits.One;
                else if (uComboStopBit.SelectedItem.DataValue.ToString() == "StopBits.OnePointFive")
                    nStopBit = StopBits.OnePointFive;
                else if (uComboStopBit.SelectedItem.DataValue.ToString() == "StopBits.Two")
                    nStopBit = StopBits.Two;
                
                int intDataBit = Convert.ToInt32(uComboDataBit.SelectedItem.DataValue.ToString());
                
                Handshake nHandShake = Handshake.None;
                if (uComboHandShake.SelectedItem.DataValue.ToString() == "Handshake.None")
                    nHandShake = Handshake.None;
                else if (uComboHandShake.SelectedItem.DataValue.ToString() == "Handshake.RequestToSend")
                    nHandShake = Handshake.RequestToSend;
                else if (uComboHandShake.SelectedItem.DataValue.ToString() == "Handshake.RequestToSendXOnXOff")
                    nHandShake = Handshake.RequestToSendXOnXOff;
                else if (uComboHandShake.SelectedItem.DataValue.ToString() == "Handshake.XOnXOff")
                    nHandShake = Handshake.XOnXOff;

                OpenSerialCOM(intCOMPort, intBaudRate, nParity, nStopBit, intDataBit, nHandShake);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButCOMClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (spCOM1.IsOpen)
                {
                    spCOM1.DataReceived -= new SerialDataReceivedEventHandler(spCOM1_DataReceived);
                    spCOM1.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

    }
}
