﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0017.cs                                         */
/* 프로그램명   : 외부시스템연결정보                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-17                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Infragistics.Shared;
//using Infragistics.Win;
//using Infragistics.Win.UltraWinGrid;
//using System.Diagnostics;

//Using추가

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPSYS.UI
{
    public partial class frmSYS0017 : Form , IToolbar
    {
        //리소스정보를 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0017()
        {
            InitializeComponent();
        }

        private void frmSYS0017_Activated(object sender, EventArgs e)
        {
            //활성화된 툴바 버튼 사용여부
            QRPBrowser ToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0017_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitCombo();
            InitGrid();

            //Form내의 Grid에 대해 저장된 열속성을 아래와 같이 적용한다.
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSYS0017_Resize);
        }

        void frmSYS0017_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridExtInterfaceInfo.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridExtInterfaceInfo.Dock = DockStyle.None;
                    //this.uGridCommonMessageList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("외부시스템연동정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 레이블컨트롤 설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보컨트롤 설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();

                //콤보리스트생성
                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                    , "", "", "선택", "PlantCode", "PlantName", dtPlant);

                //기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //외부시스템 연결정보 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridExtInterfaceInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //외부시스템 연결정보 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "Code", "코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "InterfaceName", "외부시스템명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "ProgramID", "연결ProgramID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "ProgramName", "연결프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridExtInterfaceInfo, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //그리드 폰트설정
                this.uGridExtInterfaceInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridExtInterfaceInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #region DropDown


                //공장정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //그리드에 공장콤보생성
                grd.mfSetGridColumnValueList(this.uGridExtInterfaceInfo, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //공통코드 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //그리드에 사용여부콤보 생성
                grd.mfSetGridColumnValueList(this.uGridExtInterfaceInfo, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);

                //메뉴정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                QRPSYS.BL.SYSPGM.Menu clsMenu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(clsMenu);
                
                DataTable dtProgram = clsMenu.mfReadMenu("",m_resSys.GetString("SYS_LANG"));

                string strDropDownGridHKey = "ProgramID,ProgramName,ModuleID,UpperProgramName,UseFlagName,MenuLevel,MenuOrder";
                string strDropDownGridHName = "프로그램ID,프로그램명,모듈ID,상위프로그램,사용여부,메뉴레벨,메뉴순서";
                
                //그리드에 프로그램콤보그리드 생성
                grd.mfSetGridColumnValueGridList(this.uGridExtInterfaceInfo, 0, "ProgramID", Infragistics.Win.ValueListDisplayStyle.DataValue, strDropDownGridHKey, strDropDownGridHName, "ProgramName", "ProgramID", dtProgram);

                #endregion


                //빈줄추가
                grd.mfAddRowGrid(this.uGridExtInterfaceInfo, 0);

                this.uGridExtInterfaceInfo.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridExtInterfaceInfo_AfterCellUpdate);
                this.uGridExtInterfaceInfo.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridExtInterfaceInfo_CellChange);
                this.uGridExtInterfaceInfo.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridExtInterfaceInfo_CellListSelect);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바버튼

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001226", "M000266", Infragistics.Win.HAlign.Right);
                    
                    //DropDown
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //외부시스템연결정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.ExtInterfaceInfo), "ExtInterfaceInfo");
                QRPSYS.BL.SYSPGM.ExtInterfaceInfo clsExtInterfaceInfo = new QRPSYS.BL.SYSPGM.ExtInterfaceInfo();
                brwChannel.mfCredentials(clsExtInterfaceInfo);

                //외부시스템연결정보 조회매서드 호출
                DataTable dtExtInterfaceInfo = clsExtInterfaceInfo.mfReadExtInterfaceInfoList(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridExtInterfaceInfo.DataSource = dtExtInterfaceInfo;
                this.uGridExtInterfaceInfo.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtExtInterfaceInfo.Rows.Count == 0)
                {

                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                         Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK수정불가처리
                    for (int i = 0; i < this.uGridExtInterfaceInfo.Rows.Count; i++)
                    {
                        this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridExtInterfaceInfo.Rows[i].Cells["Code"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridExtInterfaceInfo, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드에 정보가 없는 경우 저장정보 없음
                if (this.uGridExtInterfaceInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001033", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 외부시스템 연결정보 저장

                //외부시스템 연결정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.ExtInterfaceInfo), "ExtInterfaceInfo");
                QRPSYS.BL.SYSPGM.ExtInterfaceInfo clsExtInterfaceInfo = new QRPSYS.BL.SYSPGM.ExtInterfaceInfo();
                brwChannel.mfCredentials(clsExtInterfaceInfo);

                //외부시스템정보 컬럼정보가져오기
                DataTable dtExtInterfaceInfo = clsExtInterfaceInfo.mfSetDateInfo();


                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGridExtInterfaceInfo.Rows.Count; i++)
                {
                    if (this.uGridExtInterfaceInfo.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        #region 필수입력사항확인
                        string strRow = this.uGridExtInterfaceInfo.Rows[i].RowSelectorNumber.ToString();

                        if (this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001230",strLang)
                                                , strRow + msg.GetMessge_Text("M000577",strLang)
                                                , Infragistics.Win.HAlign.Right);
                            //PerFormAction
                            this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"];
                            this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        if (this.uGridExtInterfaceInfo.Rows[i].Cells["Code"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001230",strLang)
                                                , strRow + msg.GetMessge_Text("M000580",strLang)
                                                , Infragistics.Win.HAlign.Right);
                            //PerFormAction
                            this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["Code"];
                            this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (this.uGridExtInterfaceInfo.Rows[i].Cells["InterfaceName"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001230",strLang)
                                                , strRow + msg.GetMessge_Text("M000818",strLang)
                                                , Infragistics.Win.HAlign.Right);
                            //PerFormAction
                            this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["InterfaceName"];
                            this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (this.uGridExtInterfaceInfo.Rows[i].Cells["ProgramID"].Text.Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001230",strLang)
                                                , strRow + msg.GetMessge_Text("M000817",strLang)
                                                , Infragistics.Win.HAlign.Right);
                            //PerFormAction
                            this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["ProgramID"];
                            this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }

                        #endregion

                        DataRow drInfo;
                        drInfo = dtExtInterfaceInfo.NewRow();

                        drInfo["PlantCode"] = this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"].Value.ToString();   //공장
                        drInfo["Code"] = this.uGridExtInterfaceInfo.Rows[i].Cells["Code"].Value.ToString();             //코드
                        drInfo["InterfaceName"] = this.uGridExtInterfaceInfo.Rows[i].Cells["InterfaceName"].Value.ToString();//외부시스템명
                        drInfo["ProgramID"] = this.uGridExtInterfaceInfo.Rows[i].Cells["ProgramID"].Text;       //연결프로그램ID
                        drInfo["UseFlag"] = this.uGridExtInterfaceInfo.Rows[i].Cells["UseFlag"].Value.ToString();           //사용여부

                        dtExtInterfaceInfo.Rows.Add(drInfo);
                    }
                }

                //담은 저장정보가 없으면 메세지박스
                if (dtExtInterfaceInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001033", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //외부시스템 연결정보 저장매서드 호출
                    string strErrRtn = clsExtInterfaceInfo.mfSaveExtInterfaceInfo(dtExtInterfaceInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라 메세지박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M001037", "M000953",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",strLang)
                                                         , msg.GetMessge_Text("M001037",strLang)
                                                         , ErrRtn.ErrMessage
                                                         , Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드에 정보가 없는 경우 삭제 정보없음
                if (this.uGridExtInterfaceInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000634", "M000635", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 외부시스템연결정보 저장

                //외부시스템 연결정보 컬럼만들기
                DataTable dtExtInterfaceInfo = new DataTable();

                dtExtInterfaceInfo.Columns.Add("PlantCode", typeof(string));
                dtExtInterfaceInfo.Columns.Add("Code", typeof(string));

                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                //편집이미지가 나타나 있고 체크박스에 체크가 되어있는 정보를 저장한다.
                for (int i = 0; i < this.uGridExtInterfaceInfo.Rows.Count; i++)
                {
                    if (this.uGridExtInterfaceInfo.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (Convert.ToBoolean(this.uGridExtInterfaceInfo.Rows[i].Cells["Check"].Value) == true)
                        {
                            #region 필수입력사항 체크

                            string strRow = this.uGridExtInterfaceInfo.Rows[i].RowSelectorNumber.ToString();

                            if (this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001230",strLang)
                                                    , strRow + msg.GetMessge_Text("M000577",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerFormAction
                                this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"];
                                this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            if (this.uGridExtInterfaceInfo.Rows[i].Cells["Code"].Value.ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001230",strLang)
                                                    , strRow + msg.GetMessge_Text("M000580",strLang)
                                                    , Infragistics.Win.HAlign.Right);
                                //PerFormAction
                                this.uGridExtInterfaceInfo.ActiveCell = this.uGridExtInterfaceInfo.Rows[i].Cells["Code"];
                                this.uGridExtInterfaceInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #endregion

                            DataRow drInfo;
                            drInfo = dtExtInterfaceInfo.NewRow();

                            drInfo["PlantCode"] = this.uGridExtInterfaceInfo.Rows[i].Cells["PlantCode"].Value.ToString();
                            drInfo["Code"] = this.uGridExtInterfaceInfo.Rows[i].Cells["Code"].Value.ToString();

                            dtExtInterfaceInfo.Rows.Add(drInfo);
                        }

                    }
                }

                //담은정보에 정보가 없는 경우 메세지 박스를 띄운다
                if (dtExtInterfaceInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001264", "M000634", "M000635", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //외부시스템연결정보 BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.ExtInterfaceInfo), "ExtInterfaceInfo");
                    QRPSYS.BL.SYSPGM.ExtInterfaceInfo clsExtInterfaceInfo = new QRPSYS.BL.SYSPGM.ExtInterfaceInfo();
                    brwChannel.mfCredentials(clsExtInterfaceInfo);

                    //외부시스템연결정보 삭제매서드 호출
                    string strErrRtn = clsExtInterfaceInfo.mfDeleteExtInterfaceInfo(dtExtInterfaceInfo);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    //처리결과에 따라 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M000638", "M000676",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",strLang)
                                                         , msg.GetMessge_Text("M000638",strLang)
                                                         , ErrRtn.ErrMessage,
                                                         Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //그리드에 정보가 없을 시 엑셀출력정보 없음
                if (this.uGridExtInterfaceInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000808", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridExtInterfaceInfo);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {

        }

        public void mfPrint()
        {
        }

        #endregion

        #region 이벤트

        //신규로 추가한 행 및 수정한 행을 표시하기 위해 RowSelector란에 편집이미지를 나타나게 한다.
        private void uGridExtInterfaceInfo_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
               QRPGlobal grdImg = new QRPGlobal();
               e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //신규로 추가한 행에 대해 데이터를 모두 Clear한 행을 없애기
        private void uGridExtInterfaceInfo_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridExtInterfaceInfo, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드안에 있는 연결ProgramID 콤보박스를 선택시 
        private void uGridExtInterfaceInfo_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ProgramID"))
                {
                    //선택한 프로그램ID와 명 저장
                    string strProgramValue = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    string strProgramKey = e.Cell.Column.ValueList.GetText(e.Cell.Column.ValueList.SelectedItemIndex);
                    
                    //공백인 경우
                    if (strProgramKey.Equals(string.Empty))
                    {
                        if (!e.Cell.Row.Cells["ProgramName"].Value.ToString().Equals(string.Empty))
                        {
                            e.Cell.Row.Cells["ProgramName"].Value = "";
                        }
                    }
                    //공백이 아닌경우
                    else
                    {

                        //e.Cell.SelText = strProgramID;
                        e.Cell.Row.Cells["ProgramName"].Value = strProgramValue;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }       
       
        private void frmSYS0017_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Grid의 열속성을 아래와 같이 적용하여 저장한다.
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
       
        #endregion





    }
}
