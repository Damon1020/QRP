﻿namespace QRPSYS.UI
{
    partial class frmSYS0001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0001));
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem17 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem25 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem26 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem20 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem21 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem22 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem23 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem24 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem18 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem13 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem14 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem15 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem16 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxCOM = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionCOMRead = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextData = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboHandShake = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboStopBit = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboParity = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboDataBit = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboBaudRate = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboCOMPort = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButCOMClose = new Infragistics.Win.Misc.UltraButton();
            this.uButCOMOpen = new Infragistics.Win.Misc.UltraButton();
            this.uGridDeptList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCOM)).BeginInit();
            this.uGroupBoxCOM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCOMRead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandShake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStopBit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboParity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDataBit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBaudRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCOMPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeptList)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraButton1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(660, 8);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(144, 24);
            this.ultraButton1.TabIndex = 3;
            this.ultraButton1.Text = "WMS I/F Test";
            this.ultraButton1.Visible = false;
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 2;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.uLabelSearchPlant.Appearance = appearance15;
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxCOM
            // 
            this.uGroupBoxCOM.Controls.Add(this.uOptionCOMRead);
            this.uGroupBoxCOM.Controls.Add(this.uTextData);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel6);
            this.uGroupBoxCOM.Controls.Add(this.uComboHandShake);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel5);
            this.uGroupBoxCOM.Controls.Add(this.uComboStopBit);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel4);
            this.uGroupBoxCOM.Controls.Add(this.uComboParity);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel3);
            this.uGroupBoxCOM.Controls.Add(this.uComboDataBit);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel2);
            this.uGroupBoxCOM.Controls.Add(this.uComboBaudRate);
            this.uGroupBoxCOM.Controls.Add(this.ultraLabel1);
            this.uGroupBoxCOM.Controls.Add(this.uComboCOMPort);
            this.uGroupBoxCOM.Controls.Add(this.uButCOMClose);
            this.uGroupBoxCOM.Controls.Add(this.uButCOMOpen);
            this.uGroupBoxCOM.Location = new System.Drawing.Point(0, 556);
            this.uGroupBoxCOM.Name = "uGroupBoxCOM";
            this.uGroupBoxCOM.Size = new System.Drawing.Size(1068, 224);
            this.uGroupBoxCOM.TabIndex = 10;
            this.uGroupBoxCOM.Text = "ultraGroupBox1";
            // 
            // uOptionCOMRead
            // 
            valueListItem7.DataValue = "1";
            valueListItem7.DisplayText = "한글자씩 읽기(방법1)";
            valueListItem17.DataValue = "2";
            valueListItem17.DisplayText = "한꺼번에 읽기(방법2)";
            this.uOptionCOMRead.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem17});
            this.uOptionCOMRead.Location = new System.Drawing.Point(308, 84);
            this.uOptionCOMRead.Name = "uOptionCOMRead";
            this.uOptionCOMRead.Size = new System.Drawing.Size(96, 48);
            this.uOptionCOMRead.TabIndex = 15;
            // 
            // uTextData
            // 
            this.uTextData.Location = new System.Drawing.Point(416, 24);
            this.uTextData.Multiline = true;
            this.uTextData.Name = "uTextData";
            this.uTextData.Size = new System.Drawing.Size(524, 192);
            this.uTextData.TabIndex = 14;
            this.uTextData.Text = "ultraTextEditor1";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(16, 176);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel6.TabIndex = 13;
            this.ultraLabel6.Text = "HandShake";
            // 
            // uComboHandShake
            // 
            valueListItem25.DataValue = "Handshake.None";
            valueListItem25.DisplayText = "None";
            valueListItem6.DataValue = "Handshake.RequestToSend";
            valueListItem6.DisplayText = "RequestToSend";
            valueListItem8.DataValue = "Handshake.RequestToSendXOnXOff";
            valueListItem8.DisplayText = "RequestToSendXOnXOff";
            valueListItem26.DataValue = "Handshake.XOnXOff";
            valueListItem26.DisplayText = "XOnXOff";
            this.uComboHandShake.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem25,
            valueListItem6,
            valueListItem8,
            valueListItem26});
            this.uComboHandShake.Location = new System.Drawing.Point(124, 176);
            this.uComboHandShake.Name = "uComboHandShake";
            this.uComboHandShake.Size = new System.Drawing.Size(176, 21);
            this.uComboHandShake.TabIndex = 12;
            this.uComboHandShake.Text = "ultraComboEditor4";
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(16, 148);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel5.TabIndex = 11;
            this.ultraLabel5.Text = "StopBit";
            // 
            // uComboStopBit
            // 
            valueListItem2.DataValue = "StopBits.None";
            valueListItem2.DisplayText = "None";
            valueListItem3.DataValue = "StopBits.One";
            valueListItem3.DisplayText = "One";
            valueListItem4.DataValue = "StopBits.OnePointFive";
            valueListItem4.DisplayText = "OnePointFive";
            valueListItem5.DataValue = "StopBits.Two";
            valueListItem5.DisplayText = "Two";
            this.uComboStopBit.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem3,
            valueListItem4,
            valueListItem5});
            this.uComboStopBit.Location = new System.Drawing.Point(124, 148);
            this.uComboStopBit.Name = "uComboStopBit";
            this.uComboStopBit.Size = new System.Drawing.Size(176, 21);
            this.uComboStopBit.TabIndex = 10;
            this.uComboStopBit.Text = "ultraComboEditor4";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(16, 120);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel4.TabIndex = 9;
            this.ultraLabel4.Text = "Parity";
            // 
            // uComboParity
            // 
            valueListItem20.DataValue = "Parity.Even";
            valueListItem20.DisplayText = "Even";
            valueListItem21.DataValue = "Parity.Odd";
            valueListItem21.DisplayText = "Odd";
            valueListItem22.DataValue = "Parity.None";
            valueListItem22.DisplayText = "None";
            valueListItem23.DataValue = "Parity.Mark";
            valueListItem23.DisplayText = "Mark";
            valueListItem24.DataValue = "Parity.Space";
            valueListItem24.DisplayText = "Space";
            this.uComboParity.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem20,
            valueListItem21,
            valueListItem22,
            valueListItem23,
            valueListItem24});
            this.uComboParity.Location = new System.Drawing.Point(124, 120);
            this.uComboParity.Name = "uComboParity";
            this.uComboParity.Size = new System.Drawing.Size(176, 21);
            this.uComboParity.TabIndex = 8;
            this.uComboParity.Text = "ultraComboEditor4";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(16, 92);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel3.TabIndex = 7;
            this.ultraLabel3.Text = "Data Bit";
            // 
            // uComboDataBit
            // 
            valueListItem1.DataValue = "8";
            valueListItem1.DisplayText = "8";
            this.uComboDataBit.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1});
            this.uComboDataBit.Location = new System.Drawing.Point(124, 92);
            this.uComboDataBit.Name = "uComboDataBit";
            this.uComboDataBit.Size = new System.Drawing.Size(176, 21);
            this.uComboDataBit.TabIndex = 6;
            this.uComboDataBit.Text = "ultraComboEditor3";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(16, 64);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel2.TabIndex = 5;
            this.ultraLabel2.Text = "Baud Rate";
            // 
            // uComboBaudRate
            // 
            valueListItem9.DataValue = "2400";
            valueListItem9.DisplayText = "2400";
            valueListItem10.DataValue = "19200";
            valueListItem10.DisplayText = "19200";
            valueListItem18.DataValue = "9600";
            valueListItem18.DisplayText = "9600";
            this.uComboBaudRate.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem10,
            valueListItem18});
            this.uComboBaudRate.Location = new System.Drawing.Point(124, 64);
            this.uComboBaudRate.Name = "uComboBaudRate";
            this.uComboBaudRate.Size = new System.Drawing.Size(176, 21);
            this.uComboBaudRate.TabIndex = 4;
            this.uComboBaudRate.Text = "ultraComboEditor2";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(16, 36);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(92, 20);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "COM Port";
            // 
            // uComboCOMPort
            // 
            valueListItem11.DataValue = "1";
            valueListItem11.DisplayText = "1";
            valueListItem12.DataValue = "2";
            valueListItem12.DisplayText = "2";
            valueListItem13.DataValue = "3";
            valueListItem13.DisplayText = "3";
            valueListItem14.DataValue = "4";
            valueListItem14.DisplayText = "4";
            valueListItem15.DataValue = "5";
            valueListItem15.DisplayText = "5";
            valueListItem16.DataValue = "6";
            valueListItem16.DisplayText = "6";
            this.uComboCOMPort.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem11,
            valueListItem12,
            valueListItem13,
            valueListItem14,
            valueListItem15,
            valueListItem16});
            this.uComboCOMPort.Location = new System.Drawing.Point(124, 36);
            this.uComboCOMPort.Name = "uComboCOMPort";
            this.uComboCOMPort.Size = new System.Drawing.Size(176, 21);
            this.uComboCOMPort.TabIndex = 2;
            this.uComboCOMPort.Text = "ultraComboEditor1";
            // 
            // uButCOMClose
            // 
            this.uButCOMClose.Location = new System.Drawing.Point(956, 28);
            this.uButCOMClose.Name = "uButCOMClose";
            this.uButCOMClose.Size = new System.Drawing.Size(104, 40);
            this.uButCOMClose.TabIndex = 1;
            this.uButCOMClose.Text = "COM포트 닫기";
            // 
            // uButCOMOpen
            // 
            this.uButCOMOpen.Location = new System.Drawing.Point(308, 36);
            this.uButCOMOpen.Name = "uButCOMOpen";
            this.uButCOMOpen.Size = new System.Drawing.Size(100, 40);
            this.uButCOMOpen.TabIndex = 0;
            this.uButCOMOpen.Text = "COM포트 열기";
            // 
            // uGridDeptList
            // 
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeptList.DisplayLayout.Appearance = appearance30;
            this.uGridDeptList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeptList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeptList.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeptList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridDeptList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeptList.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridDeptList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeptList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeptList.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeptList.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGridDeptList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeptList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeptList.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeptList.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridDeptList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeptList.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeptList.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance37.TextHAlignAsString = "Left";
            this.uGridDeptList.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGridDeptList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeptList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeptList.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridDeptList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeptList.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.uGridDeptList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeptList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeptList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeptList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridDeptList.Location = new System.Drawing.Point(0, 80);
            this.uGridDeptList.Name = "uGridDeptList";
            this.uGridDeptList.Size = new System.Drawing.Size(1070, 770);
            this.uGridDeptList.TabIndex = 11;
            // 
            // frmSYS0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxCOM);
            this.Controls.Add(this.uGridDeptList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0001";
            this.Load += new System.EventHandler(this.frmSYS0001_Load);
            this.Activated += new System.EventHandler(this.frmSYS0001_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCOM)).EndInit();
            this.uGroupBoxCOM.ResumeLayout(false);
            this.uGroupBoxCOM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCOMRead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandShake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStopBit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboParity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDataBit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBaudRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCOMPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeptList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxCOM;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionCOMRead;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextData;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHandShake;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStopBit;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboParity;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDataBit;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboBaudRate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCOMPort;
        private Infragistics.Win.Misc.UltraButton uButCOMClose;
        private Infragistics.Win.Misc.UltraButton uButCOMOpen;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeptList;
    }
}