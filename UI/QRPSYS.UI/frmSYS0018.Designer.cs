﻿namespace QRPSYS.UI
{
    partial class frmSYS0018
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0018));
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.treeReplyMessage = new System.Windows.Forms.TreeView();
            this.treeSendMessage = new System.Windows.Forms.TreeView();
            this.uLabelReturn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReplyMessage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSendMessage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReturn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridLog = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLog)).BeginInit();
            this.SuspendLayout();
            // 
            // uDateSearchToDate
            // 
            this.uDateSearchToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDate.Location = new System.Drawing.Point(232, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 19;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 18;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            this.uDateSearchFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 17;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 16;
            this.uLabelSearchDate.Text = "ultraLabel2";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.treeReplyMessage);
            this.uGroupBoxContentsArea.Controls.Add(this.treeSendMessage);
            this.uGroupBoxContentsArea.Controls.Add(this.uLabelReturn);
            this.uGroupBoxContentsArea.Controls.Add(this.uLabelReplyMessage);
            this.uGroupBoxContentsArea.Controls.Add(this.uLabelSendMessage);
            this.uGroupBoxContentsArea.Controls.Add(this.uTextReturn);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 530);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 320);
            this.uGroupBoxContentsArea.TabIndex = 228;
            // 
            // treeReplyMessage
            // 
            this.treeReplyMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeReplyMessage.Location = new System.Drawing.Point(536, 28);
            this.treeReplyMessage.Name = "treeReplyMessage";
            this.treeReplyMessage.Size = new System.Drawing.Size(514, 256);
            this.treeReplyMessage.TabIndex = 23;
            // 
            // treeSendMessage
            // 
            this.treeSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.treeSendMessage.Location = new System.Drawing.Point(12, 28);
            this.treeSendMessage.Name = "treeSendMessage";
            this.treeSendMessage.Size = new System.Drawing.Size(516, 256);
            this.treeSendMessage.TabIndex = 22;
            // 
            // uLabelReturn
            // 
            this.uLabelReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uLabelReturn.Location = new System.Drawing.Point(12, 292);
            this.uLabelReturn.Name = "uLabelReturn";
            this.uLabelReturn.Size = new System.Drawing.Size(140, 20);
            this.uLabelReturn.TabIndex = 19;
            this.uLabelReturn.Text = "ultraLabel2";
            // 
            // uLabelReplyMessage
            // 
            this.uLabelReplyMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uLabelReplyMessage.Location = new System.Drawing.Point(540, 4);
            this.uLabelReplyMessage.Name = "uLabelReplyMessage";
            this.uLabelReplyMessage.Size = new System.Drawing.Size(510, 20);
            this.uLabelReplyMessage.TabIndex = 18;
            this.uLabelReplyMessage.Text = "ultraLabel2";
            // 
            // uLabelSendMessage
            // 
            this.uLabelSendMessage.Location = new System.Drawing.Point(16, 4);
            this.uLabelSendMessage.Name = "uLabelSendMessage";
            this.uLabelSendMessage.Size = new System.Drawing.Size(512, 20);
            this.uLabelSendMessage.TabIndex = 17;
            this.uLabelSendMessage.Text = "ultraLabel2";
            // 
            // uTextReturn
            // 
            this.uTextReturn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTextReturn.Location = new System.Drawing.Point(156, 292);
            this.uTextReturn.Name = "uTextReturn";
            this.uTextReturn.Size = new System.Drawing.Size(898, 21);
            this.uTextReturn.TabIndex = 2;
            this.uTextReturn.Text = "ultraTextEditor3";
            // 
            // uGridLog
            // 
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridLog.DisplayLayout.Appearance = appearance6;
            this.uGridLog.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridLog.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLog.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLog.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridLog.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLog.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridLog.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridLog.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridLog.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridLog.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridLog.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridLog.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridLog.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridLog.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridLog.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridLog.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLog.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridLog.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridLog.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridLog.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridLog.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridLog.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridLog.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridLog.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridLog.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridLog.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridLog.Location = new System.Drawing.Point(0, 80);
            this.uGridLog.Name = "uGridLog";
            this.uGridLog.Size = new System.Drawing.Size(1070, 450);
            this.uGridLog.TabIndex = 230;
            // 
            // frmSYS0018
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridLog);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSYS0018";
            this.Text = "frmSYS0018";
            this.Load += new System.EventHandler(this.frmSYS0018_Load);
            this.Activated += new System.EventHandler(this.frmSYS0018_Activated);
            this.Resize += new System.EventHandler(this.frmSYS0018_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.uGroupBoxContentsArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraLabel uLabelReturn;
        private Infragistics.Win.Misc.UltraLabel uLabelReplyMessage;
        private Infragistics.Win.Misc.UltraLabel uLabelSendMessage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturn;
        private System.Windows.Forms.TreeView treeSendMessage;
        private System.Windows.Forms.TreeView treeReplyMessage;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLog;
    }
}