﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0015.cs                                         */
/* 프로그램명   : 시스템연결정보                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-17                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//using추가

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSYS.UI
{
    public partial class frmSYS0015 : Form ,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0015()
        {
            InitializeComponent();
        }

        private void frmSYS0015_Activated(object sender, EventArgs e)
        {
            //활성화된 툴바 사용설정
            QRPBrowser Toolbar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            Toolbar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0015_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitCombo();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSYS0015_Resize);
        }

        void frmSYS0015_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridSYSAcce.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridSYSAcce.Dock = DockStyle.None;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀 설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //SystemResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("시스템연결정보",m_resSys.GetString("SYS_FONTNAME"),12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블 컨트롤 설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant,"공장",m_resSys.GetString("SYS_FONTNAME"),true,true);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스 설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResouceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();

                //공장콤보 조회 매서드 실행
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //공장콤보정보 콤보박스에 생성
                WinComboEditor wCom = new WinComboEditor();
                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false
                    , "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "PlantCode", "PlantName", dtPlant);

                //공장 기본갑 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 컨트롤 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //SystemResoucrInfi
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //시스템연결정보 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridSYSAcce, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                     , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                     , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //시스템연결정보 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "Code", "코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "SystemName", "시스템명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "SystemAddressPath", "시스템연결정보", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "AccessID", "연결ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "AccessPassword", "연결암호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "EtcAccessInfo1", "기타연결정보1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSYSAcce, 0, "EtcAccessInfo2", "기타연결정보2", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                this.uGridSYSAcce.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSYSAcce.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //공장정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();

                //공장콤보 조회 매서드 실행
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //그리드 공장콤보 생성
                grd.mfSetGridColumnValueList(this.uGridSYSAcce, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //빈줄 추가
                grd.mfAddRowGrid(this.uGridSYSAcce, 0);

                this.uGridSYSAcce.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSYSAcce_AfterCellUpdate);
                this.uGridSYSAcce.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSYSAcce_CellChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 활성화된 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();


                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty)) 
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    //DropDown
                    this.uComboSearchPlant.DropDown();
                    return;

                }

                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //시스템연결정보BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSystemAccessInfo);

                // 시스템연결정보 조회 매서드 호출
                DataTable dtAccessInfo = clsSystemAccessInfo.mfReadSystemAccessInfoList(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridSYSAcce.DataSource = dtAccessInfo;
                this.uGridSYSAcce.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtAccessInfo.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK 수정불가처리
                    for (int i = 0; i < this.uGridSYSAcce.Rows.Count; i++)
                    {
                        this.uGridSYSAcce.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSYSAcce.Rows[i].Cells["Code"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSYSAcce, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드의 정보가 없는 경우 저장할 정보가 없음
                if (this.uGridSYSAcce.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001010", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 시스템연결정보 저장

                //시스템연결정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSystemAccessInfo);

                //시스템연결 컬럼정보 매서드 호출
                DataTable dtAccessInfo = clsSystemAccessInfo.mfSetDataInfo();

                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGridSYSAcce.Rows.Count; i++)
                {
                    //편집이미지가 있는 줄을 저장한다.
                    if (this.uGridSYSAcce.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        #region 필수입력확인창

                        string strRow = this.uGridSYSAcce.Rows[i].RowSelectorNumber.ToString();

                        if (this.uGridSYSAcce.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001226",strLang)
                                                , strRow + msg.GetMessge_Text("M000577",strLang)
                                                , Infragistics.Win.HAlign.Right);

                            //PerFormAction
                            this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["PlantCode"];
                            this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        if (this.uGridSYSAcce.Rows[i].Cells["Code"].Value.ToString().Equals(string.Empty))
                        {

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001226",strLang)
                                                , strRow + msg.GetMessge_Text("M000580",strLang)
                                                , Infragistics.Win.HAlign.Right);

                            //PerFormAction
                            this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["Code"];
                            this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (this.uGridSYSAcce.Rows[i].Cells["SystemName"].Value.ToString().Equals(string.Empty))
                        {

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001226",strLang)
                                                , strRow + msg.GetMessge_Text("M000578",strLang)
                                                , Infragistics.Win.HAlign.Right);

                            //PerFormAction
                            this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["SystemName"];
                            this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (this.uGridSYSAcce.Rows[i].Cells["SystemAddressPath"].Value.ToString().Equals(string.Empty))
                        {

                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001226",strLang)
                                                , strRow + msg.GetMessge_Text("M000579",strLang)
                                                , Infragistics.Win.HAlign.Right);

                            //PerFormAction
                            this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["SystemAddressPath"];
                            this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        #endregion

                        // 저장정보 저장
                        DataRow drAcce;
                        drAcce = dtAccessInfo.NewRow();

                        drAcce["PlantCode"] = this.uGridSYSAcce.Rows[i].Cells["PlantCode"].Value.ToString();                //공장
                        drAcce["Code"] = this.uGridSYSAcce.Rows[i].Cells["Code"].Value.ToString();                          //Code
                        drAcce["SystemName"] = this.uGridSYSAcce.Rows[i].Cells["SystemName"].Value.ToString();              //시스템명
                        drAcce["SystemAddressPath"] = this.uGridSYSAcce.Rows[i].Cells["SystemAddressPath"].Value.ToString();//시스템연결정보
                        drAcce["AccessID"] = this.uGridSYSAcce.Rows[i].Cells["AccessID"].Value.ToString();                  //연결ID
                        drAcce["AccessPassword"] = this.uGridSYSAcce.Rows[i].Cells["AccessPassword"].Value.ToString();      //연결Passwrod
                        drAcce["EtcAccessInfo1"] = this.uGridSYSAcce.Rows[i].Cells["EtcAccessInfo1"].Value.ToString();      //기타연결정보1
                        drAcce["EtcAccessInfo2"] = this.uGridSYSAcce.Rows[i].Cells["EtcAccessInfo2"].Value.ToString();      //기타연결정보2

                        dtAccessInfo.Rows.Add(drAcce);
                    }
                   
                }
                if (dtAccessInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001032", "M001010", Infragistics.Win.HAlign.Right);
                    return;
                }
                #endregion


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //시스템연결정보 저장 매서드 호출
                    string strErrRtn = clsSystemAccessInfo.mfSaveSystemAccessInfo(dtAccessInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decdiong
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라서 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M001037", "M000953",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001135",strLang)
                                                         , msg.GetMessge_Text("M001037",strLang)
                                                         , ErrRtn.ErrMessage
                                                         , Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //Systme ReosurceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //그리드에 정보가 없으면 삭제정보없음
                if (this.uGridSYSAcce.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000634", "M000623", Infragistics.Win.HAlign.Right);

                    return;
                }

                #region 삭제정보저장

                //시스템연결정보 삭제 컬럼설정
                DataTable dtAccessInfo = new DataTable();

                dtAccessInfo.Columns.Add("PlantCode", typeof(string));
                dtAccessInfo.Columns.Add("Code", typeof(string));

                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGridSYSAcce.Rows.Count; i++)
                {

                    if (this.uGridSYSAcce.Rows[i].RowSelectorAppearance.Image != null) 
                    {
                        if (Convert.ToBoolean(this.uGridSYSAcce.Rows[i].Cells["Check"].Value) == true)
                        {
                            #region 필수입력확인

                            string strRow = this.uGridSYSAcce.Rows[i].RowSelectorNumber.ToString();

                            if (this.uGridSYSAcce.Rows[i].Cells["PlantCode"].Value.Equals(string.Empty))
                            {

                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000577",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["PlantCode"];
                                this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            if (this.uGridSYSAcce.Rows[i].Cells["Code"].Value.Equals(string.Empty))
                            {

                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M001226",strLang)
                                                    , strRow + msg.GetMessge_Text("M000580",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                //PerFormAction
                                this.uGridSYSAcce.ActiveCell = this.uGridSYSAcce.Rows[i].Cells["Code"];
                                this.uGridSYSAcce.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #endregion

                            //시스템연결정보 저장
                            DataRow drAcce;
                            drAcce = dtAccessInfo.NewRow();

                            drAcce["PlantCode"] = this.uGridSYSAcce.Rows[i].Cells["PlantCode"].Value.ToString();
                            drAcce["Code"] = this.uGridSYSAcce.Rows[i].Cells["Code"].Value.ToString();

                            dtAccessInfo.Rows.Add(drAcce);
                        }
                    }
                }

                //삭제정보가없는경우 메세지박스를띄운다.
                if (dtAccessInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000634", "M000623", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                
                    //시스템연결정보 BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSystemAccessInfo);

                    //시스템연결정보 삭제 매서드 호출
                    string strErrRtn = clsSystemAccessInfo.mfDeleteSystemAccessInfo(dtAccessInfo);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리 결과에 따라서 메세지박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M000638", "M000676",
                                                         Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "M001135", "M000638", ErrRtn.ErrMessage,
                                                         Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfCreate()
        {
           
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSYSAcce.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000810", "M000804", Infragistics.Win.HAlign.Right);

                    return;
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridSYSAcce);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        #endregion

        //신규로 추가한 행 및 수정한 행을 표시하기 위해 RowSelector란에 편집이미지를 나타나게 한다.
        private void uGridSYSAcce_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //신규로 추가한 행에 대해 데이터를 모두 Clear한 행을 없애기 
        private void uGridSYSAcce_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridSYSAcce, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSYS0015_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}
