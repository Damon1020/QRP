﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0009.cs                                         */
/* 프로그램명   : 사용자권한정보                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-10-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPSYS.UI
{
    public partial class frmSYS0009 : Form , IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0009()
        {
            InitializeComponent();
        }

        private void frmSYS0009_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser ToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0009_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitGrid();
            InitComboBox();
            

            //Expand GroupBox를 숨김상태로 변경한다.
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀 설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //titleArea에 타이틀 설정
                titleArea.mfSetLabelText("사용자별권한정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 레이블 컨트롤 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                //레이블 컨트롤 초기화
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchUserID, "사용자", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelSearchUserName, "사용자명", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelUser, "사용자", m_resSys.GetString("SYS_LANG"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그룹박스 컨트롤 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //메뉴별 권한정보 그룹박스 설정
                WinGroupBox wGroupBox = new WinGroupBox();

                //그룹박스 컨트롤 설정
                wGroupBox.mfSetGroupBox(this.uGroupBoxMenu, GroupBoxType.LIST, "메뉴별 권한정보", m_resSys.GetString("SYS_FONTNAME")
                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //그룹박스 폰트설정
                this.uGroupBoxMenu.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxMenu.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(plant);

                // 공장정보 콤보박스 매서드 실행
                DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //공정 콤보박스 리스트 & 컨트롤 설정
                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                //사용자 정보 그리드 기본 설정
                wGrid.mfInitGeneralGrid(this.uGridUserInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //사용자 정보 그리드 컬럼 설정
                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "TelNum", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "HpNum", "핸드폰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserInfo, 0, "EMail", "이메일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////메뉴별 권한정보 그리드 기본설정
                ////wGrid.mfInitGeneralGrid(this.uGridMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                ////    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                ////    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                ////    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //////메뉴별 권한정보 그리드 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "ProgramID", "ProgramID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "ProgramName", "ProgramName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "SearchFlag", "검색", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "SaveFlag", "저장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "DeleteFlag", "삭제", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "PrintFlag", "출력", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMenu, 0, "ExcelFlag", "엑셀", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //그리드 폰트 설정
                this.uGridUserInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridUserInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //this.uGridMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                //this.uGridMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                this.uGridUserInfo.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridUserInfo_DoubleClickRow);
                this.uGridMenu.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridMenu_AfterCellUpdate);
                this.uGridMenu.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridMenu_CellChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                if (intBandIndex == 0)
                {
                    //메뉴별 권한정보 그리드 기본설정
                    wGrid.mfInitGeneralGrid(this.uGridMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                        , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                    this.uGridMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                    this.uGridMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                }
                //메뉴별 권한정보 그리드 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "UpperProgramID", "UpperProgramID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "SearchFlag", "검색", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "SaveFlag", "저장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "DeleteFlag", "삭제", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "PrintFlag", "출력", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "ExcelFlag", "엑셀", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //계층레벨추가
                wGrid.mfChangeGridColumnStyle(this.uGridMenu, intBandIndex, "HierarchyLevel", "HierarchyLevel", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            	
            }
            finally
            {
            }
        }

        /// <summary>
        /// ExpandGroupBox안의 컨트롤 텍스트 Clear
        /// </summary>
        private void InitText()
        {
            try
            {
                this.uTextPlantCode.Clear();
                this.uTextPlantName.Clear();
                this.uTextUserID.Clear();
                this.uTextUserName.Clear();

                
                if (this.uGridMenu.Rows.Count > 0)
                {
                    //-- 전체 행을 선택 하여 삭제한다 --//
                    this.uGridMenu.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridMenu.Rows.All);
                    this.uGridMenu.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Tool Bar

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                //ExpandGroupBox가 펼쳐져 있으면 숨긴다.
                if (this.uGroupBoxContentsArea.Expanded == true)
                {

                    this.uGroupBoxContentsArea.Expanded = false;
                }

                // 검색조건의 공장코드가 공백인경우 에러메세지 박스를 띄운다.
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);
                    
                    //DropDown
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                //공정정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();



                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //사용자정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //사용자정보조회 매서드 실행
                DataTable dtUser = clsUser.mfReadSYSUser_Like(strPlantCode, this.uTextSearchUserID.Text,this.uTextSearchUserName.Text, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridUserInfo.DataSource = dtUser;
                this.uGridUserInfo.DataBind();

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtUser.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMenu, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //ExpandGroupBox가 숨김 상태이거나 UserID가 공백인 경우 저장할 정보가 없음
                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextUserID.Text.Equals(string.Empty) || this.uGridMenu.Rows.Count.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M000962", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                //사용자 권한정보BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth),"UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth clsUserAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(clsUserAuth);

                //사용자권한 컬럼정보 가져오기
                DataTable dtUserAuth = clsUserAuth.mfSetDataInfo();

                DataTable dtSave = new DataTable();

                if (this.uGridMenu.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridMenu.ActiveCell = this.uGridMenu.Rows[0].Cells[0];

                    Infragistics.Win.UltraWinGrid.RowsCollection rows = this.uGridMenu.Rows;
                    dtSave = mfGetAllRowsGridWithBand(rows, dtUserAuth);
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000962", "M001047",
                             Infragistics.Win.HAlign.Right);
                    return;
                }

                // 저장 여부메세지 박스를 띄운다. 
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //사용자권한 저장 매서드 호출
                    string strErrRtn = clsUserAuth.mfSaveUserAuth(dtUserAuth, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private DataTable mfGetAllRowsGridWithBand(Infragistics.Win.UltraWinGrid.RowsCollection rows, DataTable dtFormat)
        {
            DataTable dtRtn = new DataTable();
            dtRtn = dtFormat;
            try
            {
                foreach (Infragistics.Win.UltraWinGrid.UltraGridRow row in rows)
                {
                    DataRow dr = dtFormat.NewRow();

                    dr["UserID"] = this.uTextUserID.Text;                                           //사용자ID
                    dr["ProgramID"] = row.Cells["ProgramID"].Value.ToString();                      //프로그램ID
                    dr["UseFlag"] = row.Cells["UseFlag"].Value.ToString().Substring(0, 1);          //사용여부
                    dr["SearchFlag"] = row.Cells["SearchFlag"].Value.ToString().Substring(0, 1);    //조회
                    dr["SaveFlag"] = row.Cells["SaveFlag"].Value.ToString().Substring(0, 1);        //저장
                    dr["DeleteFlag"] = row.Cells["DeleteFlag"].Value.ToString().Substring(0, 1);    //삭제
                    dr["PrintFlag"] = row.Cells["PrintFlag"].Value.ToString().Substring(0, 1);      //프린트
                    dr["ExcelFlag"] = row.Cells["ExcelFlag"].Value.ToString().Substring(0, 1);      //엑셀출력
                    dtRtn.Rows.Add(dr);

                    //-- Chind Band의 Row 저장 --//
                    if (row.ChildBands != null)
                    {
                        foreach (Infragistics.Win.UltraWinGrid.UltraGridChildBand childband in row.ChildBands)
                        {
                            Infragistics.Win.UltraWinGrid.RowsCollection childrows = childband.Rows;
                            DataTable dt = new DataTable();
                            if (childrows != null)
                                dt = mfGetAllRowsGridWithBand(childrows, dtFormat);
                        }
                    }
                }
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();


                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextUserID.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                         "M001264", "M000962", "M000627", Infragistics.Win.HAlign.Right);
                    return;
                }

                //유저 아이디저장
                string strUser = this.uTextUserID.Text;

                //삭제여부 메세지 박스를 띄운다.
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //유저 정보BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                    QRPSYS.BL.SYSPGM.UserAuth clsUserAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                    brwChannel.mfCredentials(clsUserAuth);

                    //유저권한정보 삭제 매서드 실행
                    string strErrRtn = clsUserAuth.mfDeleteUserAuth(strUser);

                    // Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 처리결과에 따라서 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{
                //    this.uGroupBoxContentsArea.Expanded = true;
                //}
                
                //InitText();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridMenu.Rows.Count == 0 && this.uGridUserInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000808", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }
                
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                WinGrid grd = new WinGrid();

                if (this.uGridMenu.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridMenu);

                if (this.uGridUserInfo.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridUserInfo);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        public void mfPrint()
        {
        }

        #endregion


        #region Event.

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    //this.uGridUserInfo.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    //this.uGridUserInfo.Height = 740;

                    // 고정행 풀기
                    this.uGridUserInfo.DisplayLayout.Rows.FixedRows.Clear();

                    //InitText();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //사용자정보 그리드의 사용자를 더블 클릭하였을 경우 해당하는 메뉴권한을 그리드로 보여준다.
        private void uGridUserInfo_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ExpandGroupBox가 숨김 상태면 펼친다.
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }

                // 고정행 풀기
                this.uGridUserInfo.DisplayLayout.Rows.FixedRows.Clear();

                //선택한 줄 고정
                e.Row.Fixed = true;

                //유저 저장
                string strUser = e.Row.Cells["UserID"].Value.ToString();

                //공장정보 유저정보 컨트롤에 삽입
                this.uTextPlantCode.Text = e.Row.Cells["PlantCode"].Value.ToString();
                this.uTextPlantName.Text = e.Row.Cells["PlantName"].Value.ToString();
                this.uTextUserID.Text = strUser;
                this.uTextUserName.Text = e.Row.Cells["UserName"].Value.ToString();

                //유저 권한정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth clsUserAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(clsUserAuth);

                //유저 권한정보 조회 매서드 호출
                DataTable dtMenu = new DataTable("Menu");
                dtMenu = clsUserAuth.mfReadUserAuth(strUser, m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
                DataSet ds = grd.mfCreateHierarchicalDataSet(dtMenu, "ProgramID", "UpperProgramID");
                this.uGridMenu.DataSource = ds;

                //this.uGridMenu.DataSource = dtMenu;
                //this.uGridMenu.DataBind();

                for (int i = 0; i < this.uGridMenu.DisplayLayout.Bands.Count; i++)
                {
                    ChangeGridColumn(i);
                    brwChannel.mfSetFormLanguage(this, this.uGridMenu);
                    if (i > 0)
                        uGridMenu.DisplayLayout.Bands[i].ColHeadersVisible = false;
                }

                //유저 권한정보가 있는 경우 그리드에 바인드
                if (dtMenu.Rows.Count > 0)
                {
                    //WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMenu, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmSYS0009_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmSYS0009_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridUserInfo.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridUserInfo.Dock = DockStyle.None;
                    this.uGridUserInfo.Height = 850 - this.titleArea.Height - this.uGroupBoxSearchArea.Height -
                                                this.uGroupBoxContentsArea.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMenu_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                //if (!e.Cell.Value.ToString().ToUpper().Equals("TRUE") && !e.Cell.Value.ToString().ToUpper().Equals("FALSE"))
                //    return;

                //// 하위메뉴가 없다면 리턴시킨다.
                //if (e.Cell.Row.ChildBands == null)
                //    return;

                ////체크박스값 저장
                //bool bol = Convert.ToBoolean(e.Cell.Value);
                
                ////해당 셀의 하위메뉴수대로 루프를 돈다.
                //for (int i = 0; i < e.Cell.Column.Layout.Bands.Count; i++)
                //{
                //    Infragistics.Win.UltraWinGrid.UltraGridBand childBand = e.Cell.Column.Layout.Bands[i];
                //    Infragistics.Win.UltraWinGrid.UltraGridColumn childBoolColumn = childBand.Columns[e.Cell.Column.Key];

                //    // Update all of the child band's Bool columns to be checked or not, as specified
                //    Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization boolSynchronization = childBoolColumn.Header.CheckBoxSynchronizationResolved;
                //    if (boolSynchronization == Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.RowsCollection)
                //    {
                //        if (e.Cell.Row.ChildBands != null)
                //        {
                //            Infragistics.Win.UltraWinGrid.RowsCollection childRows = e.Cell.Row.ChildBands[0].Rows;//row.ChildBands[0].Rows;

                //            for (int j = 0; j < childRows.Count; j++)
                //                childRows[j].Cells[e.Cell.Column.Key].Value = bol;
                //        }
                //    }
                //}

                if (e.Cell.Row.HasChild())
                {
                    string strValue = e.Cell.Value.ToString();
                    string strKey = e.Cell.Column.Key;
                    CheckChild(e.Cell.Row.ChildBands, strKey, strValue);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void CheckChild(Infragistics.Win.UltraWinGrid.ChildBandsCollection ChildBands, string strKey, string strValue)
        {
            foreach (Infragistics.Win.UltraWinGrid.UltraGridChildBand uChildBand in ChildBands)
            {
                CheckChild(uChildBand, strKey, strValue);
            }
        }

        private void CheckChild(Infragistics.Win.UltraWinGrid.UltraGridChildBand uChildBand, string strKey, string strValue)
        {
            foreach (Infragistics.Win.UltraWinGrid.UltraGridRow uGRow in uChildBand.Rows)
            {
                uGRow.Cells[strKey].Value = strValue;

                if (uGRow.HasChild())
                {
                    CheckChild(uGRow.ChildBands, strKey, strValue);
                }
            }
        }

        private void uGridMenu_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            this.uGridMenu.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
        }
    }
}
