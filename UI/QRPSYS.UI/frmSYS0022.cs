﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : frmSYS0022.cs                                         */
/* 프로그램명   : 다국어메세지관리                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-03-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                           : ~~~~  추가                               */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPSYS.UI
{
    public partial class frmSYS0022 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSYS0022()
        {
            InitializeComponent();
        }

        private void frmSYS0022_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSYS0022_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmSYS0022_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("다국어메세지관리", m_resSys.GetString("SYS_FONTNAME"), 12);
            SetToolAuth();

            // 초기화 메소드
            InitGroupBox();
            InitGrid();
            InitLabel();
            InitEtc();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region IToolbar 멤버

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                this.uTextMessageCode.Clear();
                this.uTextMessage.Clear();
                this.uTextMessageCh.Clear();
                this.uTextMessageEn.Clear();

                // PK 수정가능상태

                this.uTextMessageCode.ReadOnly = false;
                this.uTextMessageCode.Appearance.BackColor = Color.PowderBlue;

                this.uTextMessageCode.Focus();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 입력사항 확인
                if (this.uTextMessageCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M001339"
                        , Infragistics.Win.HAlign.Right);

                    this.uTextMessageCode.Focus();
                    return;
                }
                else
                {
                    // 기존등록된 자료인지 확인
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.Message), "Message");
                    QRPSYS.BL.SYSLAN.Message clsMessage = new QRPSYS.BL.SYSLAN.Message();
                    brwChannel.mfCredentials(clsMessage);

                    DataTable dtCheck = clsMessage.mfReadSYSMessage_Check(this.uTextMessageCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtCheck.Rows.Count > 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000636", "M000922"
                            , Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000636", "M001283"
                        , Infragistics.Win.HAlign.Right);

                        mfSearch();
                        return;
                    }

                    // 삭제하는 경우
                    if (Result == DialogResult.Yes)
                    {
                        // 삭제 메소드 호출
                        string strErrRtn = clsMessage.mfDeleteSYSMessage(this.uTextMessageCode.Text);

                        // 결과검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000638", "M000638", "M000926"
                                , Infragistics.Win.HAlign.Right);

                            mfSearch();
                            return;
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000638", "M000638", "M000923"
                                , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M000638",m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M000638",m_resSys.GetString("SYS_LANG"))
                                , ErrRtn.ErrMessage
                                , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridCommonMessageList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridCommonMessageList);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 입력사항 확인
                if (this.uTextMessageCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M001339"
                        , Infragistics.Win.HAlign.Right);

                    this.uTextMessageCode.Focus();
                    return;
                }
                else
                {
                    // 중복 확인
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.Message), "Message");
                    QRPSYS.BL.SYSLAN.Message clsMessage = new QRPSYS.BL.SYSLAN.Message();
                    brwChannel.mfCredentials(clsMessage);

                    DataTable dtCheck = clsMessage.mfReadSYSMessage_Check(this.uTextMessageCode.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtCheck.Rows.Count > 0)
                    {
                        // 용어가 모두 공백이면 그냥 저장한다.
                        if (dtCheck.Rows[0]["Message"].ToString().Equals(string.Empty) &&
                            dtCheck.Rows[0]["MessageCh"].ToString().Equals(string.Empty) &&
                            dtCheck.Rows[0]["MessageEn"].ToString().Equals(string.Empty))
                        {
                            Result = DialogResult.Yes;
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001122", "M001280"
                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        // 중복이 아니면 그냥 저장
                        Result = DialogResult.Yes;
                    }

                    // 저장하는 경우
                    if (Result == DialogResult.Yes)
                    {
                        // DataTable Column설정
                        DataTable dtMessageList = clsMessage.mfSetDataInfo();

                        DataRow drRow = dtMessageList.NewRow();
                        drRow["MessageCode"] = this.uTextMessageCode.Text;
                        drRow["Message"] = this.uTextMessage.Text;
                        drRow["MessageCh"] = this.uTextMessageCh.Text;
                        drRow["MessageEn"] = this.uTextMessageEn.Text;
                        dtMessageList.Rows.Add(drRow);

                        // 저장 메소드 호출
                        string strErrRtn = clsMessage.mfSaveSYSMessage(dtMessageList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001022", "M001023", "M000930"
                                , Infragistics.Win.HAlign.Right);

                            mfSearch();
                            mfCreate();
                            return;
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001022", "M001023", "M000953"
                                , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001022",m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M001023",m_resSys.GetString("SYS_LANG"))
                                , ErrRtn.ErrMessage
                                , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                    else
                    {
                        // 취소했을경우 MessageCode 수정가능하도록 Focus
                        this.uTextMessageCode.Focus();
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 입력사항 확인
                if (this.uOptionSearchLang.CheckedIndex.Equals(-1))
                {
                    ////Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    ////    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////    , "확인창", "검색조건 확인", "한글, 중문, 영문중 하나를 선택해 주세요."
                    ////    , Infragistics.Win.HAlign.Right);

                    this.uOptionSearchLang.CheckedItem.DataValue = m_resSys.GetString("SYS_LANG");
                }

                ////// Clear
                ////mfCreate();

                // 조회 후 기존 스크롤 위치로 가기위한 변수설정
                int intScrolPosition = this.uGridCommonMessageList.ActiveRowScrollRegion.ScrollPosition;

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.Message), "Message");
                QRPSYS.BL.SYSLAN.Message clsMessage = new QRPSYS.BL.SYSLAN.Message();
                brwChannel.mfCredentials(clsMessage);

                DataTable dtCLangList = clsMessage.mfReadSYSMessage(this.uTextSearchMessage.Text, this.uOptionSearchLang.CheckedItem.DataValue.ToString());

                this.uGridCommonMessageList.SetDataBinding(dtCLangList, string.Empty);

                if (dtCLangList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridCommonMessageList, 0);

                    this.uGridCommonMessageList.ActiveRowScrollRegion.ScrollPosition = intScrolPosition;
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001099", "M001104", "M001102"
                        , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxMessageInfo, GroupBoxType.INFO, "다국어 메세지 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBoxMessageInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxMessageInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //m_resSys.Close();
                //m_resSys.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchMessage, "Message", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLanguage, "선택", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelMessageCode, "MessageCode", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMessage, "Message 한글", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMessageCh, "Message 중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMessageEn, "Message 영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCommonMessageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCommonMessageList, 0, "MessageCode", "MessageCode", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonMessageList, 0, "Message", "Message한글", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonMessageList, 0, "MessageCh", "Message중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCommonMessageList, 0, "MessageEn", "Message영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Font Size 설정
                this.uGridCommonMessageList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCommonMessageList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 편집불가상태로
                this.uGridCommonMessageList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridCommonMessageList.DisplayLayout.Bands[0].Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

                this.uGridCommonMessageList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridCommonMessageList_DoubleClickRow);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화

        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // TextBox MaxLenth 설정
                this.uTextSearchMessage.MaxLength = 4000;
                this.uTextMessageCode.MaxLength = 10;
                this.uTextMessage.MaxLength = 4000;
                this.uTextMessageCh.MaxLength = 4000;
                this.uTextMessageEn.MaxLength = 4000;

                // LangID 필수입력사항 처리
                this.uTextMessageCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextMessageCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;

                // 선택 OptionSet Log-In언어값으로 설정
                this.uOptionSearchLang.Value = m_resSys.GetString("SYS_LANG");

                this.Resize += new EventHandler(frmSYS0022_Resize);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void frmSYS0022_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    this.uGridCommonMessageList.Dock = DockStyle.Fill;
                }
                else
                {
                    this.uGridCommonMessageList.Dock = DockStyle.None;
                    //this.uGridCommonMessageList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uGridCommonMessageList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextMessageCode.Text = e.Row.Cells["MessageCode"].Value.ToString();
                this.uTextMessage.Text = e.Row.Cells["Message"].Value.ToString();
                this.uTextMessageCh.Text = e.Row.Cells["MessageCh"].Value.ToString();
                this.uTextMessageEn.Text = e.Row.Cells["MessageEn"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.Message), "Message");
                QRPSYS.BL.SYSLAN.Message clsMessage = new QRPSYS.BL.SYSLAN.Message();
                brwChannel.mfCredentials(clsMessage);

                ////// 변수 설정
                ////string strMessageCode = e.Row.Cells["MessageCode"].Value.ToString();

                ////DataTable dtMessageDetail = clsMessage.mfReadSYSMessage_Check(strMessageCode, m_resSys.GetString("SYS_LANG"));

                ////if (dtMessageDetail.Rows.Count > 0)
                ////{
                ////    this.uTextMessageCode.Text = e.Row.Cells["MessageCode"].Value.ToString();
                ////    this.uTextMessage.Text = e.Row.Cells["Message"].Value.ToString();
                ////    this.uTextMessageCh.Text = e.Row.Cells["MessageCh"].Value.ToString();
                ////    this.uTextMessageEn.Text = e.Row.Cells["MessageEn"].Value.ToString();
                ////}


                // PK 수정불가상태
                this.uTextMessageCode.ReadOnly = true;
                this.uTextMessageCode.Appearance.BackColor = Color.Gainsboro;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
