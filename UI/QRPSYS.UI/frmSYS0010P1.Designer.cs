﻿namespace QRPSYS.UI
{
    partial class frmSYS0010P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSYS0010P1));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionSearchLang = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelSearchLanguage = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uTextSearchLang = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLang = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCommonLangList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).BeginInit();
            this.uGroupSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonLangList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(650, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupSearchArea
            // 
            this.uGroupSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchArea.Appearance = appearance1;
            this.uGroupSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchArea.Controls.Add(this.uOptionSearchLang);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchLanguage);
            this.uGroupSearchArea.Controls.Add(this.uButtonSearch);
            this.uGroupSearchArea.Controls.Add(this.uTextSearchLang);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchLang);
            this.uGroupSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchArea.Name = "uGroupSearchArea";
            this.uGroupSearchArea.Size = new System.Drawing.Size(650, 40);
            this.uGroupSearchArea.TabIndex = 1;
            // 
            // uOptionSearchLang
            // 
            this.uOptionSearchLang.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSearchLang.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            this.uOptionSearchLang.ItemOrigin = new System.Drawing.Point(0, 4);
            valueListItem1.DataValue = "KOR";
            valueListItem1.DisplayText = "한글";
            valueListItem2.DataValue = "CHN";
            valueListItem2.DisplayText = "중문";
            valueListItem3.DataValue = "ENG";
            valueListItem3.DisplayText = "영문";
            this.uOptionSearchLang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.uOptionSearchLang.Location = new System.Drawing.Point(352, 12);
            this.uOptionSearchLang.Name = "uOptionSearchLang";
            this.uOptionSearchLang.Size = new System.Drawing.Size(200, 20);
            this.uOptionSearchLang.TabIndex = 6;
            this.uOptionSearchLang.TextIndentation = 2;
            this.uOptionSearchLang.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelSearchLanguage
            // 
            this.uLabelSearchLanguage.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearchLanguage.Name = "uLabelSearchLanguage";
            this.uLabelSearchLanguage.Size = new System.Drawing.Size(60, 20);
            this.uLabelSearchLanguage.TabIndex = 5;
            this.uLabelSearchLanguage.Text = "선택";
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(556, 8);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 4;
            this.uButtonSearch.Text = "검색";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uTextSearchLang
            // 
            this.uTextSearchLang.Location = new System.Drawing.Point(76, 12);
            this.uTextSearchLang.Name = "uTextSearchLang";
            this.uTextSearchLang.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchLang.TabIndex = 3;
            this.uTextSearchLang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchLang_KeyDown);
            // 
            // uLabelSearchLang
            // 
            this.uLabelSearchLang.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchLang.Name = "uLabelSearchLang";
            this.uLabelSearchLang.Size = new System.Drawing.Size(60, 20);
            this.uLabelSearchLang.TabIndex = 2;
            this.uLabelSearchLang.Text = "용어";
            // 
            // uGridCommonLangList
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCommonLangList.DisplayLayout.Appearance = appearance5;
            this.uGridCommonLangList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCommonLangList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCommonLangList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridCommonLangList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCommonLangList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCommonLangList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCommonLangList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridCommonLangList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCommonLangList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCommonLangList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridCommonLangList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCommonLangList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCommonLangList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridCommonLangList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridCommonLangList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCommonLangList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridCommonLangList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridCommonLangList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCommonLangList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridCommonLangList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCommonLangList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCommonLangList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCommonLangList.Location = new System.Drawing.Point(0, 80);
            this.uGridCommonLangList.Name = "uGridCommonLangList";
            this.uGridCommonLangList.Size = new System.Drawing.Size(650, 330);
            this.uGridCommonLangList.TabIndex = 2;
            this.uGridCommonLangList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridCommonLangList_DoubleClickRow);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(560, 410);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 5;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // frmSYS0010P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(650, 440);
            this.ControlBox = false;
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uGridCommonLangList);
            this.Controls.Add(this.uGroupSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSYS0010P1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmSYS0010P1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSYS0010P1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).EndInit();
            this.uGroupSearchArea.ResumeLayout(false);
            this.uGroupSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCommonLangList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLang;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLang;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCommonLangList;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSearchLang;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLanguage;
    }
}