﻿namespace QRPSTA.UI
{
    partial class frmSTA0091
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0091));
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridMatList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridProcList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboMaterialType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMaterialType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridMonthly = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uChart = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uTabInfo = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatList)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabInfo)).BeginInit();
            this.uTabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridMatList);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1061, 294);
            // 
            // uGridMatList
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMatList.DisplayLayout.Appearance = appearance2;
            this.uGridMatList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMatList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridMatList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridMatList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMatList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMatList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMatList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridMatList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMatList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMatList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridMatList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMatList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridMatList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridMatList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMatList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridMatList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridMatList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMatList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridMatList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMatList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMatList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMatList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridMatList.Location = new System.Drawing.Point(0, 0);
            this.uGridMatList.Name = "uGridMatList";
            this.uGridMatList.Size = new System.Drawing.Size(1061, 294);
            this.uGridMatList.TabIndex = 319;
            this.uGridMatList.Text = "uGridMonth";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridProcList);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1061, 294);
            // 
            // uGridProcList
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcList.DisplayLayout.Appearance = appearance14;
            this.uGridProcList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridProcList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridProcList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridProcList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridProcList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridProcList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridProcList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridProcList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridProcList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridProcList.Location = new System.Drawing.Point(0, 0);
            this.uGridProcList.Name = "uGridProcList";
            this.uGridProcList.Size = new System.Drawing.Size(1061, 294);
            this.uGridProcList.TabIndex = 320;
            this.uGridProcList.Text = "uGridMonth";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboMaterialType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelMaterialType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uComboMaterialType
            // 
            this.uComboMaterialType.Location = new System.Drawing.Point(572, 12);
            this.uComboMaterialType.Name = "uComboMaterialType";
            this.uComboMaterialType.Size = new System.Drawing.Size(100, 21);
            this.uComboMaterialType.TabIndex = 5;
            this.uComboMaterialType.Text = "ultraComboEditor1";
            // 
            // uLabelMaterialType
            // 
            this.uLabelMaterialType.Location = new System.Drawing.Point(468, 12);
            this.uLabelMaterialType.Name = "uLabelMaterialType";
            this.uLabelMaterialType.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialType.TabIndex = 4;
            this.uLabelMaterialType.Text = "자재유형";
            // 
            // uTextSearchYear
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance17;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(80, 21);
            this.uTextSearchYear.TabIndex = 3;
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            this.uLabelSearchYear.Text = "조회년도";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridMonthly
            // 
            this.uGridMonthly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMonthly.DisplayLayout.Appearance = appearance27;
            this.uGridMonthly.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMonthly.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMonthly.DisplayLayout.GroupByBox.Appearance = appearance28;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMonthly.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.uGridMonthly.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance30.BackColor2 = System.Drawing.SystemColors.Control;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMonthly.DisplayLayout.GroupByBox.PromptAppearance = appearance30;
            this.uGridMonthly.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMonthly.DisplayLayout.MaxRowScrollRegions = 1;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMonthly.DisplayLayout.Override.ActiveCellAppearance = appearance31;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMonthly.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.uGridMonthly.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMonthly.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMonthly.DisplayLayout.Override.CardAreaAppearance = appearance33;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            appearance34.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMonthly.DisplayLayout.Override.CellAppearance = appearance34;
            this.uGridMonthly.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMonthly.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMonthly.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance36.TextHAlignAsString = "Left";
            this.uGridMonthly.DisplayLayout.Override.HeaderAppearance = appearance36;
            this.uGridMonthly.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMonthly.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.uGridMonthly.DisplayLayout.Override.RowAppearance = appearance37;
            this.uGridMonthly.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMonthly.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.uGridMonthly.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMonthly.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMonthly.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMonthly.Location = new System.Drawing.Point(0, 80);
            this.uGridMonthly.Name = "uGridMonthly";
            this.uGridMonthly.Size = new System.Drawing.Size(1070, 150);
            this.uGridMonthly.TabIndex = 4;
            // 
            // uChart
            // 
            this.uChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChart.Axis.PE = paintElement1;
            this.uChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.LineThickness = 1;
            this.uChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MajorGridLines.Visible = true;
            this.uChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MinorGridLines.Visible = false;
            this.uChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X.Visible = true;
            this.uChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.Visible = false;
            this.uChart.Axis.X2.LineThickness = 1;
            this.uChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MajorGridLines.Visible = true;
            this.uChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MinorGridLines.Visible = false;
            this.uChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X2.Visible = false;
            this.uChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.LineThickness = 1;
            this.uChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MajorGridLines.Visible = true;
            this.uChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MinorGridLines.Visible = false;
            this.uChart.Axis.Y.TickmarkInterval = 50;
            this.uChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y.Visible = true;
            this.uChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.Visible = false;
            this.uChart.Axis.Y2.LineThickness = 1;
            this.uChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChart.Axis.Y2.TickmarkInterval = 50;
            this.uChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y2.Visible = false;
            this.uChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z.Labels.ItemFormatString = "";
            this.uChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.LineThickness = 1;
            this.uChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MajorGridLines.Visible = true;
            this.uChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MinorGridLines.Visible = false;
            this.uChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z.Visible = false;
            this.uChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.Visible = false;
            this.uChart.Axis.Z2.LineThickness = 1;
            this.uChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z2.Visible = false;
            this.uChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChart.Effects.Effects.Add(gradientEffect1);
            this.uChart.Location = new System.Drawing.Point(3, 237);
            this.uChart.Name = "uChart";
            this.uChart.Size = new System.Drawing.Size(1065, 252);
            this.uChart.TabIndex = 329;
            this.uChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uTabInfo
            // 
            this.uTabInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabInfo.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabInfo.Controls.Add(this.ultraTabPageControl1);
            this.uTabInfo.Controls.Add(this.ultraTabPageControl2);
            this.uTabInfo.Location = new System.Drawing.Point(3, 500);
            this.uTabInfo.Name = "uTabInfo";
            this.uTabInfo.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabInfo.Size = new System.Drawing.Size(1065, 320);
            this.uTabInfo.TabIndex = 330;
            ultraTab3.Key = "Imp";
            ultraTab3.TabPage = this.ultraTabPageControl1;
            ultraTab3.Text = "수입검사 이상발생건";
            ultraTab4.Key = "Proc";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "공정검사 이상발생건";
            this.uTabInfo.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1061, 294);
            // 
            // frmSTA0091
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uTabInfo);
            this.Controls.Add(this.uChart);
            this.Controls.Add(this.uGridMonthly);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0091";
            this.Load += new System.EventHandler(this.frmSTA0091_Load);
            this.Activated += new System.EventHandler(this.frmSTA0091_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0091_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatList)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabInfo)).EndInit();
            this.uTabInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMonthly;
        private Infragistics.Win.UltraWinChart.UltraChart uChart;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabInfo;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMatList;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcList;
    }
}