﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPSTA.UI
{
    public partial class frmSTA0078 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0078()
        {
            InitializeComponent();
        }

        private void frmSTA0078_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 화면 리사이즈시 컴포넌트 크기 조정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0078_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGridProcLoss.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                }
                else
                {
                    uGridProcLoss.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0078_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("공정불량현황", m_resSys.GetString("SYS_FONTNAME"), 12);

            InitCombobox();
            InitLabel();
            InitGrid();
            InitYear();

            SetToolAuth();
        }

        /// <summary>
        /// 사용권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 종료시 컬럼 위치 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0078_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region 초기화

        /// <summary>
        /// 콤보박스
        /// </summary>
        private void InitCombobox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인딩
                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

                ArrayList arrDataTypeKey = new ArrayList();
                ArrayList arrDataTypeValue = new ArrayList();

                string strLang = m_resSys.GetString("SYS_LANG");
                WinMessageBox msg = new WinMessageBox();

                arrDataTypeKey.Add("");
                arrDataTypeValue.Add(msg.GetMessge_Text("M001497", strLang));
                arrDataTypeKey.Add("1");
                arrDataTypeValue.Add(msg.GetMessge_Text("M000241", strLang));
                arrDataTypeKey.Add("2");
                arrDataTypeValue.Add(msg.GetMessge_Text("M000242", strLang));

                wCombo.mfSetComboEditor(this.uComboSearchDataType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", arrDataTypeKey, arrDataTypeValue);

                //DetailProcessOperationType 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessDetailProcessOperationType(this.uComboSearchPlant.Value.ToString());

                wCombo.mfSetComboEditor(this.uComboSearchProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", msg.GetMessge_Text("M001498",strLang), "DetailProcessOperationType", "ComboName", dtProcess);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// Label
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "연도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDataType, "항목유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                wGrid.mfInitGeneralGrid(this.uGridProcLoss, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                                                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 40
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "ProcessCode", "공정", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "ProcessName", "공정명", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "InspectDate", "발생일", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10 
                                                 , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                                 , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "InspectTime", "발생시간", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                                                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "CUSTOMERPRODUCTSPEC", "고객사PartNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 500
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                                                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                                                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLoss, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 500
                                                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridProcLoss.DisplayLayout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridProcLoss.DisplayLayout.Bands[0].Columns["FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridProcLoss.DisplayLayout.Bands[0].Columns["InspectQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridProcLoss.DisplayLayout.Bands[0].Columns["InspectQty"].MaskInput = "nnn,nnn,nnn";

                this.uGridProcLoss.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar 메소드
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000881", "M000212", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchMonth.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000881", "M000214", Infragistics.Win.HAlign.Center);

                    this.uComboSearchMonth.DropDown();
                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                    QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                    brwChannel.mfCredentials(clsReport);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strMonth = this.uComboSearchMonth.Value.ToString();
                    string strProcessOperationType = this.uComboSearchProcessOperationType.Value.ToString();
                    string strDataType = this.uComboSearchDataType.Value.ToString();

                    DataTable dtProcLoss = clsReport.mfReadINSProcInspect_frmSTA0078_PSTS(strPlantCode, strYear, strMonth, strProcessOperationType, strDataType, m_resSys.GetString("SYS_LANG"));

                    this.uGridProcLoss.DataSource = dtProcLoss;
                    this.uGridProcLoss.DataBind();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (this.uGridProcLoss.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000203", "M000204", Infragistics.Win.HAlign.Center);

                        return;
                    }
                    else
                    {
                        for (int i = 0; i < this.uGridProcLoss.Rows.Count; i++)
                        {
                            this.uGridProcLoss.Rows[i].Cells["Seq"].Value = i + 1;
                            if (Convert.ToInt32(this.uGridProcLoss.Rows[i].Cells["Seq"].Value) < 10)
                            {
                                this.uGridProcLoss.Rows[i].Cells["Seq"].Value = "000" + this.uGridProcLoss.Rows[i].Cells["Seq"].Value.ToString();
                            }
                            else if (Convert.ToInt32(this.uGridProcLoss.Rows[i].Cells["Seq"].Value) < 100)
                            {
                                this.uGridProcLoss.Rows[i].Cells["Seq"].Value = "00" + this.uGridProcLoss.Rows[i].Cells["Seq"].Value.ToString();
                            }
                            else if (Convert.ToInt32(this.uGridProcLoss.Rows[i].Cells["Seq"].Value) < 1000)
                            {
                                this.uGridProcLoss.Rows[i].Cells["Seq"].Value = "0" + this.uGridProcLoss.Rows[i].Cells["Seq"].Value.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridProcLoss.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridProcLoss);
                }
                if (this.uGridProcLoss.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }   
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        #endregion

    }
}
