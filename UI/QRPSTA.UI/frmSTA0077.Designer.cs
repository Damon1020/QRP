﻿namespace QRPSTA.UI
{
    partial class frmSTA0077
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0077));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxWeekly1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridWeekly1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxWeekly2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridWeekly2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxWeekly1)).BeginInit();
            this.uGroupBoxWeekly1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridWeekly1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxWeekly2)).BeginInit();
            this.uGroupBoxWeekly2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridWeekly2)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(288, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 330;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(640, 12);
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(72, 21);
            this.uComboSearchMonth.TabIndex = 326;
            this.uComboSearchMonth.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMonth
            // 
            this.uLabelSearchMonth.Location = new System.Drawing.Point(536, 12);
            this.uLabelSearchMonth.Name = "uLabelSearchMonth";
            this.uLabelSearchMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMonth.TabIndex = 325;
            this.uLabelSearchMonth.Text = "ultraLabel1";
            // 
            // uTextSearchYear
            // 
            appearance25.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance25;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(392, 12);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(72, 21);
            this.uTextSearchYear.TabIndex = 324;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(119, 21);
            this.uComboSearchPlant.TabIndex = 4;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGroupBoxWeekly1
            // 
            this.uGroupBoxWeekly1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxWeekly1.Controls.Add(this.uGridWeekly1);
            this.uGroupBoxWeekly1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxWeekly1.Name = "uGroupBoxWeekly1";
            this.uGroupBoxWeekly1.Size = new System.Drawing.Size(1070, 344);
            this.uGroupBoxWeekly1.TabIndex = 6;
            // 
            // uGridWeekly1
            // 
            this.uGridWeekly1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridWeekly1.DisplayLayout.Appearance = appearance29;
            this.uGridWeekly1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridWeekly1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly1.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridWeekly1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGridWeekly1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridWeekly1.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGridWeekly1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridWeekly1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridWeekly1.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridWeekly1.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.uGridWeekly1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridWeekly1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly1.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            appearance30.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridWeekly1.DisplayLayout.Override.CellAppearance = appearance30;
            this.uGridWeekly1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridWeekly1.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly1.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance37.TextHAlignAsString = "Left";
            this.uGridWeekly1.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGridWeekly1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridWeekly1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridWeekly1.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridWeekly1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridWeekly1.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridWeekly1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridWeekly1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridWeekly1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridWeekly1.Location = new System.Drawing.Point(5, 7);
            this.uGridWeekly1.Name = "uGridWeekly1";
            this.uGridWeekly1.Size = new System.Drawing.Size(1059, 327);
            this.uGridWeekly1.TabIndex = 0;
            this.uGridWeekly1.Text = "ultraGrid1";
            // 
            // uGroupBoxWeekly2
            // 
            this.uGroupBoxWeekly2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxWeekly2.Controls.Add(this.uGridWeekly2);
            this.uGroupBoxWeekly2.Location = new System.Drawing.Point(0, 440);
            this.uGroupBoxWeekly2.Name = "uGroupBoxWeekly2";
            this.uGroupBoxWeekly2.Size = new System.Drawing.Size(1070, 344);
            this.uGroupBoxWeekly2.TabIndex = 7;
            // 
            // uGridWeekly2
            // 
            this.uGridWeekly2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridWeekly2.DisplayLayout.Appearance = appearance42;
            this.uGridWeekly2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridWeekly2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance39.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly2.DisplayLayout.GroupByBox.Appearance = appearance39;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridWeekly2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance40;
            this.uGridWeekly2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance41.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance41.BackColor2 = System.Drawing.SystemColors.Control;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridWeekly2.DisplayLayout.GroupByBox.PromptAppearance = appearance41;
            this.uGridWeekly2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridWeekly2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridWeekly2.DisplayLayout.Override.ActiveCellAppearance = appearance50;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridWeekly2.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGridWeekly2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridWeekly2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly2.DisplayLayout.Override.CardAreaAppearance = appearance44;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridWeekly2.DisplayLayout.Override.CellAppearance = appearance43;
            this.uGridWeekly2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridWeekly2.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridWeekly2.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance49.TextHAlignAsString = "Left";
            this.uGridWeekly2.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGridWeekly2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridWeekly2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.BorderColor = System.Drawing.Color.Silver;
            this.uGridWeekly2.DisplayLayout.Override.RowAppearance = appearance48;
            this.uGridWeekly2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridWeekly2.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.uGridWeekly2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridWeekly2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridWeekly2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridWeekly2.Location = new System.Drawing.Point(5, 7);
            this.uGridWeekly2.Name = "uGridWeekly2";
            this.uGridWeekly2.Size = new System.Drawing.Size(1059, 327);
            this.uGridWeekly2.TabIndex = 0;
            this.uGridWeekly2.Text = "ultraGrid2";
            // 
            // frmSTA0077
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxWeekly2);
            this.Controls.Add(this.uGroupBoxWeekly1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0077";
            this.Load += new System.EventHandler(this.frmSTA0077_Load);
            this.Activated += new System.EventHandler(this.frmSTA0077_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0077_FormClosing);
            this.Resize += new System.EventHandler(this.frmSTA0077_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxWeekly1)).EndInit();
            this.uGroupBoxWeekly1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridWeekly1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxWeekly2)).EndInit();
            this.uGroupBoxWeekly2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridWeekly2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxWeekly1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxWeekly2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridWeekly1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridWeekly2;
    }
}