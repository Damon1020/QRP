﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 통계분석                                              */
/* 모듈(분류)명 : 수입검사 통계분석                                     */
/* 프로그램ID   : frmSTA0024.cs                                         */
/* 프로그램명   : P(불량률) 관리도 분석                                 */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-07-24                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
namespace QRPSTA.UI
{
    public partial class frmSTA0024 : Form,IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0024()
        {
            InitializeComponent();
        }


        private void frmSTA0024_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0024_Load(object sender, EventArgs e)
        {
            SetToolAuth();

            //컨트롤초기화
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitEtc();
        }

        private void frmSTA0024_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    this.uGroupBoxGridList.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBoxInfo.Width;
                    this.uGroupBoxChart.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    Point pt = new Point(this.uGroupBoxGridList.Width, this.uGroupBoxInfo.Location.Y);
                    this.uGroupBoxInfo.Location = pt;
                    this.uGroupBoxSearchArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    Point pt = new Point(800, 110);
                    this.uGroupBoxInfo.Location = pt;
                    this.uGroupBoxGridList.Width = 800;
                    this.uGroupBoxChart.Width = 1060;
                    this.uGroupBoxSearchArea.Width = 1060;
                    this.uGroupBoxChart.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBoxGridList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBoxInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridList);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                #region 검색조건 확인

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216", "M000266", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchInspectToDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216", "M000052", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (this.uComboSearchMaterial.SelectedItem == null || this.uComboSearchMaterial.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216", "M000965", Infragistics.Win.HAlign.Center);
                    this.uComboSearchMaterial.DropDown();
                    return;
                }
                ////else if (this.uComboSearchRev.SelectedItem == null || this.uComboSearchRev.Value.ToString().Equals(string.Empty))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "M001264", "M000216", "M000965", Infragistics.Win.HAlign.Center);
                ////    this.uComboSearchRev.DropDown();
                ////    return;
                ////}
                else if (this.uComboSearchInspectItem.Value == null || this.uComboSearchInspectItem.Value == DBNull.Value || this.uComboSearchInspectItem.Value.ToString().Equals(string.Empty) ||
                    this.uComboSearchInspectItem.SelectedRow == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216", "M000193", Infragistics.Win.HAlign.Center);

                    this.uComboSearchInspectItem.Focus();
                    this.uComboSearchInspectItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                    return;
                }
                ////else if (this.uComboSearchVendor.Value.ToString().Equals(string.Empty) || this.uComboSearchVendor.SelectedItem == null)
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "M001264", "M000216", "M001274", Infragistics.Win.HAlign.Center);
                ////    this.uComboSearchVendor.DropDown();
                ////    return;
                ////}
                ////else if (this.uComboSearchMaterialGrade.SelectedItem == null || this.uComboSearchMaterialGrade.Value.ToString().Equals(string.Empty))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "M001264", "M000216", "M001413", Infragistics.Win.HAlign.Center);

                ////    this.uComboSearchMaterialGrade.DropDown();
                ////    return;
                ////}

                #endregion

                else
                {
                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region 검색조건 설정

                    // 검색조건
                    DateTime dateInspectFromDate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).AddDays(-1);
                    DateTime dateInspectToDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);
                    // 날짜를 제외한 검색조건 설정
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strMaterialCode = this.uComboSearchMaterial.Value.ToString();
                    string strRev = this.uComboSearchRev.Value.ToString();
                    string strInspectItemCode = this.uComboSearchInspectItem.SelectedRow.GetCellValue("InspectItemCode").ToString();
                    string strVendorCode = this.uComboSearchVendor.Value.ToString();
                    string strMaterialGrade = this.uComboSearchMaterialGrade.Value.ToString();

                    #endregion

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                    QRPSTA.BL.STAIMP.ImportStaticD clsStaticD = new QRPSTA.BL.STAIMP.ImportStaticD();
                    brwChannel.mfCredentials(clsStaticD);

                    DataTable dtInspectCount = clsStaticD.mfReadINSMatStaticD_Count(strPlantCode
                                                                                , strMaterialCode
                                                                                , strRev
                                                                                , strInspectItemCode
                                                                                , strVendorCode
                                                                                , strMaterialGrade
                                                                                , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                                , m_resSys.GetString("SYS_LANG"));

                    this.uGridList.SetDataBinding(dtInspectCount, string.Empty);

                    if (dtInspectCount.Rows.Count > 0)
                    {
                        // 그래프
                        QRPSTA.STASummary stSummary = new STASummary();
                        QRPSTA.STASPC clsSPC = new STASPC();
                        stSummary = clsSPC.mfDrawPChart(this.uChartPchart, dtInspectCount.DefaultView.ToTable(false, "SampleSize")
                                        , dtInspectCount.DefaultView.ToTable(false, "FaultQty")
                                        , "P 관리도", string.Empty, string.Empty
                                        , m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                        // 통계값
                        this.uTextCL.Text = string.Format("{0:f5}", stSummary.MeanDA);
                        this.uTextTOTQty.Text = Convert.ToInt32(dtInspectCount.Compute("SUM(SampleSize)", string.Empty)).ToString();
                        this.uTextTOTFaultQty.Text = Convert.ToInt32(dtInspectCount.Compute("SUM(FaultQty)", string.Empty)).ToString();
                        this.uTextTOTFaultRate.Text = string.Format("{0:f5}", (100.0m * (Convert.ToDecimal(this.uTextTOTFaultQty.Text) / Convert.ToDecimal(this.uTextTOTQty.Text))));

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001104"
                            , "M000171", Infragistics.Win.HAlign.Center);

                        Clear();

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGroupBox wGroupBox = new QRPCOM.QRPUI.WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxChart, GroupBoxType.CHART, "P관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxGridList, GroupBoxType.LIST, "조회데이터", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxInfo, GroupBoxType.INFO, "통계정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxChart.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxChart.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxGridList.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxGridList.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검색기간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchRev, "Rev", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialGrade, "자재등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelTOTQty, "총검사수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTOTFaultQty, "총불량수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTOTFaultRate, "총불량률(%)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCL, "CL", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCombo = new QRPCOM.QRPUI.WinComboEditor();
                WinComboGrid wComboG = new WinComboGrid();

                // 검사항목 콤보박스
                wComboG.mfInitGeneralComboGrid(this.uComboSearchInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";

                if (strLang.Equals("KOR"))
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위";
                else if (strLang.Equals("CHN"))
                    strName = "项目序号,检查项目编号,检查项目名,规格上限,规格下限,规格范围";
                else if (strLang.Equals("ENG"))
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위";
                else
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위";
                string[] strColumns = strName.Split(',');

                this.uComboSearchInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ItemNum", strColumns[0], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemCode", strColumns[1], false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemName", strColumns[2], false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "UpperSpec", strColumns[3], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "LowerSpec", strColumns[4], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeCode", strColumns[5], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeName", strColumns[5], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SampleSize", "SampleSize", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                // 이벤트 설정
                this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
                this.uComboSearchMaterial.AfterEnterEditMode += new EventHandler(uComboSearchMaterial_AfterEnterEditMode);
                this.uComboSearchMaterial.ValueChanged += new EventHandler(uComboSearchMaterial_ValueChanged);
                this.uComboSearchVendor.AfterEnterEditMode += new EventHandler(uComboSearchVendor_AfterEnterEditMode);
                this.uComboSearchVendor.ValueChanged += new EventHandler(uComboSearchVendor_ValueChanged);
                this.uComboSearchRev.ValueChanged += new EventHandler(uComboSearchRev_ValueChanged);

                // 공장콤보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), string.Empty, string.Empty
                    , "PlantCode", "PlantName", dtPlant);

                // 거래처 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);
                DataTable dtVendor = clsVendor.mfReadVendorPopup(m_resSys.GetString("SYS_LANG"));
                dtVendor.Columns.Add("VendorCodeName", typeof(string));
                for (int i = 0; i < dtVendor.Rows.Count; i++)
                {
                    dtVendor.Rows[i]["VendorCodename"] = dtVendor.Rows[i]["VendorCode"].ToString() + "  |  " + dtVendor.Rows[i]["VendorName"].ToString();
                }
                wCombo.mfSetComboEditor(this.uComboSearchVendor, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ""
                    , "VendorCode", "VendorCodename", dtVendor);

                // 자재등급 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);
                DataTable dtCom = clsCom.mfReadCommonCode("C0013", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchMaterialGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "ComCode", "ComCodeName", dtCom);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "InspectTime", "검사시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "SampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, "nnn,nnn,nnn,nnn,nnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, "nnn,nnn,nnn,nnn,nnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "FaultRate", "불량비율", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn.nnnnnnnnnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "PFaultRate", "불량률(%)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn.nnnnnnnnnn", string.Empty);

                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집부가상태로
                this.uGridList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // 타이틀 설정함수 호출
                this.titleArea.mfSetLabelText("P(불량률) 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

                // 기본값 설정
                this.uDateSearchInspectFromDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();

                setTabIndex(this.Controls);

                this.uDateSearchInspectFromDate.TabIndex = 1;
                this.uDateSearchInspectFromDate.TabStop = true;
                this.uDateSearchInspectToDate.TabIndex = 2;
                this.uDateSearchInspectToDate.TabStop = true;
                this.uComboSearchMaterial.TabIndex = 3;
                this.uComboSearchMaterial.TabStop = true;
                this.uComboSearchInspectItem.TabIndex = 4;
                this.uComboSearchInspectItem.TabStop = true;
                this.uComboSearchVendor.TabIndex = 5;
                this.uComboSearchVendor.TabStop = true;
                this.uComboSearchRev.TabIndex = 6;
                this.uComboSearchRev.TabStop = true;
                this.uComboSearchMaterialGrade.TabIndex = 7;
                this.uComboSearchMaterialGrade.TabStop = true;

                Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...

        // 공장콤보 값 변경 이벤트
        void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMat);

                DataTable dtMat = clsMat.mfReadMASMaterial_MaterialCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchMaterial.Items.Clear();

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchMaterial, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "MaterialCode", "MaterialName", dtMat);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 거래처 콤보 EditMode 후 Text 전체 선택되도록 하는 이벤트
        void uComboSearchVendor_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchVendor.SelectAll();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 자재 콤보 EditMode 후 Text 전체 선택되도록 하는 이벤트
        void uComboSearchMaterial_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchMaterial.SelectAll();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        void uComboSearchMaterial_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ////if (this.uComboSearchMaterial.Value == null ||
                ////    this.uComboSearchMaterial.Value == DBNull.Value ||
                ////    this.uComboSearchMaterial.Value.ToString().Equals(string.Empty))
                ////    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uComboSearchMaterial.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsMatstd = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsMatstd);

                DataTable dtRev = clsMatstd.mfReadSTAIMP_Rev_Combo(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchRev.Items.Clear();

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchRev, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "RevCode", "RevName", dtRev);


                InitInspectItemCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        void uComboSearchRev_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        void uComboSearchVendor_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods...

        // Tabindex 설정 함수
        private void setTabIndex(Control.ControlCollection ctls)
        {
            try
            {
                foreach (Control ctl in ctls)
                {
                    if (ctl.GetType().Equals(typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor)))
                    {
                        Infragistics.Win.UltraWinEditors.UltraTextEditor uText = (Infragistics.Win.UltraWinEditors.UltraTextEditor)ctl;
                        uText.Appearance.BackColor = Color.Gainsboro;
                        uText.ReadOnly = true;
                        uText.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    }
                    ctl.TabIndex = 0;
                    ctl.TabStop = false;
                    if (ctl.HasChildren)
                        setTabIndex(ctl.Controls);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 초기화 메소드
        private void Clear()
        {
            try
            {
                if (this.uGridList.DataSource != null)
                {
                    DataTable dtEmpty = ((DataTable)this.uGridList.DataSource).Clone();
                    this.uGridList.SetDataBinding(dtEmpty, string.Empty);
                }

                this.uTextCL.Clear();
                this.uTextTOTFaultQty.Clear();
                this.uTextTOTFaultRate.Clear();
                this.uTextTOTQty.Clear();

                ClearChart(this.uChartPchart);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 차트 초기화 메소드
        private void ClearChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        //검사항목 콤보박스 설정
        private void InitInspectItemCombo()
        {
            try
            {
                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchMaterial.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchRev.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchVendor.SelectedIndex.Equals(-1))
                    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uComboSearchMaterial.Value.ToString();
                string strRev = this.uComboSearchRev.Value.ToString();
                string strVendorCode = this.uComboSearchVendor.Value.ToString();
                string strDataType = "2";

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsstd = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsstd);

                DataTable dtInspectItem = clsstd.mfReadINSMatStatic_InspectItemCombo(strPlantCode, strMaterialCode, strVendorCode
                                    , strRev, strDataType, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchInspectItem.SetDataBinding(dtInspectItem, string.Empty);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion
    }
}
