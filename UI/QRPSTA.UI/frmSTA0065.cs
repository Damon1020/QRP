﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0065.cs                                         */
/* 프로그램명   : 공정능력분석                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-11-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0065 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0065()
        {
            InitializeComponent();
        }

        private void frmSTA0065_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmSTA0065_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0065_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("공정능력분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                //this.uTextSearchProductCode.MaxLength = 20;
                //this.uTextSearchCustomerCode.MaxLength = 10;

                this.uDateSearchInspectFromDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.CHART, "Histogram", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //wLabel.mfSetLabel(this.uLabelSearchMaterial, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipCode, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //wLabel.mfSetLabel(this.uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSpecUpper, "규격상한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecLower, "규격하한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecRange, "규격범위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAverage, "평균", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStdDeviation, "표준편차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMax, "Max", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMin, "Min", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCp, "Cp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpk, "Cpk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpl, "Cpl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpu, "Cpu", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboGrid wCombo = new WinComboGrid();
                WinComboEditor wComboe = new WinComboEditor();

                // SearchArea

                QRPBrowser brwChannel = new QRPBrowser();

                //고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustomer);

                //공장 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 검사항목 콤보박스
                wCombo.mfInitGeneralComboGrid(this.uComboSearchInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ItemNum", "항목순번", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemCode", "검사항목코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemName", "검사항목명", false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "StackSeq", "Stack", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "Generation", "Generation", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "UpperSpec", "규격상한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "LowerSpec", "규격하한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeCode", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeName", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ProcessInspectSS", "SampleSize", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                //this.uComboInspectItem.Text = "선택";

                // 공정콤보
                wCombo.mfInitGeneralComboGrid(this.uComboSearchProcess, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcess.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wCombo.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessCode", "공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessName", "공정명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectTime", "시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectValue", "검사값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                //Infragistics.Win.UltraWinGrid.RowSelectedEventArgs aa = new Infragistics.Win.UltraWinGrid.RowSelectedEventArgs(this.uComboInspectItem, this.uComboInspectItem.SelectedRow);
                //// 이벤트 호출
                //uComboInspectItem_RowSelected(this.uComboInspectItem, aa);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGrid1.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                }
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 검색조건 이벤트
        #region TextBox
        // 검색조건 제품코드 팝업창 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = this.uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                //this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                //this.uTextSearchProductName.Text = frmPOP.ProductName;
                //this.uTextSearchPackage.Text = frmPOP.Package;
                //this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                //this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;

                InitInspectItemCombo();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 키다운 이벤트
        private void uTextSearchProductCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Enter)
                //{
                //    // 공백이 아닐시
                //    if (!this.uTextSearchProductCode.Text.Equals(string.Empty))
                //    {
                //        // SystemInfo ResourceSet
                //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //        WinMessageBox msg = new WinMessageBox();

                //        string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //        if (strPlantCode.Equals(string.Empty))
                //        {
                //            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "공장 입력확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                //            this.uComboSearchPlant.DropDown();
                //            return;
                //        }
                //        else
                //        {
                //            string strProductCode = this.uTextSearchProductCode.Text;

                //            QRPBrowser brwChannel = new QRPBrowser();
                //            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                //            QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                //            brwChannel.mfCredentials(clsProduct);

                //            DataTable dtProduct = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                //            if (dtProduct.Rows.Count > 0)
                //            {
                //                this.uTextSearchProductCode.Text = dtProduct.Rows[0]["ProductCode"].ToString();
                //                this.uTextSearchProductName.Text = dtProduct.Rows[0]["ProductName"].ToString();
                //                this.uTextSearchCustomerCode.Text = dtProduct.Rows[0]["CustomerCode"].ToString();
                //                this.uTextSearchCustomerName.Text = dtProduct.Rows[0]["CustomerName"].ToString();
                //                this.uTextSearchPackage.Text = dtProduct.Rows[0]["Package"].ToString();

                //                InitInspectItemCombo();
                //            }
                //            else
                //            {
                //                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                                        , "확인창", "제품정보 조회결과", "제품정보를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);

                //                this.uTextSearchProductCode.Clear();
                //                this.uTextSearchProductName.Clear();
                //                this.uTextSearchCustomerCode.Clear();
                //                this.uTextSearchCustomerName.Clear();
                //                this.uTextSearchPackage.Clear();
                //            }
                //        }
                //    }
                //}
                //else if (e.KeyCode.Equals(Keys.Back) || e.KeyCode.Equals(Keys.Delete))
                //{
                //    if (this.uTextSearchProductCode.TextLength <= 1 || this.uTextSearchProductCode.SelectedText == this.uTextSearchProductCode.Text)
                //    {
                //        this.uTextSearchProductName.Clear();
                //        this.uTextSearchCustomerCode.Clear();
                //        this.uTextSearchCustomerName.Clear();
                //        this.uTextSearchPackage.Clear();
                //    }
                //}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색조건 고객사코드 팝업창 이벤트
        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                //this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                //this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ComboBox
        // 검색조건 공장코드 선택시 공정그룹 콤보박스 선택하는 코드
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtProcessGroup = new DataTable();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //검색조건 값 초기화
                this.uComboSearchProcessGroup.Items.Clear();
                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchProcessGroup.Items.Clear();
                this.uTextSearchStackSeq.Clear();
                this.uTextSearchGeneration.Clear();

                //this.uTextSearchProductCode.Clear();
                //this.uTextSearchProductName.Clear();
                //this.uTextSearchPackage.Clear();
                //this.uTextSearchCustomerCode.Clear();
                //this.uTextSearchCustomerName.Clear();

                //분석데이터 초기화
                Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    WinComboEditor wCombo = new WinComboEditor();

                    //PackageGroup5, Package 콤보박스
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "5", m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchPackageGroup5, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                    DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "Package", "ComboName", dtPackage);

                    //공정그룹 콤보박스
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcessGroup = clsProcess.mfReadMASProcessGroup(this.uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "ProcessGroup", "ComboName", dtProcessGroup);

                    //검사항목 콤보박스 (고객, Package, 공정 모두 선택시 처리)
                    //InitInspectItemCombo();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 공정그룹 선택시 공정콤보박스 설정 이벤트
        private void uComboSearchProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtProcess = new DataTable();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();

                if (!strPlantCode.Equals(string.Empty) && !strProcessGroup.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadMASProcessCombo_ProcessGroup(strPlantCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));
                }

                this.uComboSearchProcess.DataSource = dtProcess;
                this.uComboSearchProcess.DataBind();

                this.uComboSearchProcess.Value = null;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 공정콤보 설정시 검사항목 콤보박스 설정하기
        private void uComboSearchProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uComboSearchProcess.Value.Equals(null) || !this.uComboSearchProcess.Value.Equals(string.Empty))
                {
                    InitInspectItemCombo();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region DateTime
        private void uDateSearchInspectFromDate_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                DateTime dtFrom = DateTime.Parse(this.uDateSearchInspectFromDate.Value.ToString());
                DateTime dtTo = DateTime.Parse(this.uDateSearchInspectToDate.Value.ToString());

                if (DateTime.Compare(dtFrom, dtTo) > 0)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "입력확인", "검사일자 입력 확인"
                        , "From검사일이 To검사일보다 큽니다. 다시 선택해 주세요.", Infragistics.Win.HAlign.Center);

                    this.uDateSearchInspectFromDate.DropDown();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uDateSearchInspectToDate_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                DateTime dtFrom = DateTime.Parse(this.uDateSearchInspectFromDate.Value.ToString());
                DateTime dtTo = DateTime.Parse(this.uDateSearchInspectToDate.Value.ToString());

                if (DateTime.Compare(dtFrom, dtTo) > 0)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "입력확인", "검사일자 입력 확인"
                        , "From검사일이 To검사일보다 작습니다. 다시 선택해 주세요.", Infragistics.Win.HAlign.Center);

                    this.uDateSearchInspectToDate.DropDown();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion
        #endregion

        #region Method...
        // 검사항목 콤보박스 설정 메소드
        private void InitInspectItemCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "입력사항 확인", "공장을 선택해 주세요", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.Focus();
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "입력사항 확인", "Package을 선택해 주세요", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPackage.Focus();
                    this.uComboSearchPackage.DropDown();
                    return;
                }

                if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "입력사항 확인", "공정Type을 선택해 주세요", Infragistics.Win.HAlign.Center);

                    this.uComboSearchProcessGroup.Focus();
                    return;
                }

                //if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                    , "확인창", "입력사항 확인", "공정을 선택해 주세요", Infragistics.Win.HAlign.Center);

                //    this.uComboSearchProcess.Focus();
                //    return;
                //}

                // SystemInfo ResourceSet
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();

                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();

                string strProcessCode = "";
                if (this.uComboSearchProcess.Value != null)
                    strProcessCode = this.uComboSearchProcess.Value.ToString();

                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtItem = clsStatic.mfReadINSProcessStatic_InspectItemCombo(strPlantCode, strPackage, strProcessGroupCode, strProcessCode, strCustomerCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchInspectItem.DataSource = dtItem;
                this.uComboSearchInspectItem.DataBind();

                if (!(dtItem.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                    , "검사항목이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }            
        }

        // 차트 초기화 메소드
        private void ClearChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                //DataTable dtInit = new DataTable();
                //dtInit.Columns.Add("1", typeof(Int32));
                ////dtInit.Columns.Add("b", typeof(Int32));

                //DataRow drRow = dtInit.NewRow();
                //drRow["1"] = 0;
                ////drRow["b"] = 0;
                //dtInit.Rows.Add(drRow);

                //uChart.DataSource = dtInit;
                //uChart.DataBind();

                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextUpperSpec.Clear();
                this.uTextLowerSpec.Clear();
                this.uTextSpecRange.Clear();
                this.uTextMean.Clear();
                this.uTextStdDev.Clear();
                this.uTextMax.Clear();
                this.uTextMin.Clear();

                this.uTextCp.Clear();
                this.uTextCpk.Clear();
                this.uTextCpu.Clear();
                this.uTextCpl.Clear();

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                ClearChart(this.uChartHistogram);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Double 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">Double로 반환받을 값</param>
        /// <returns></returns>
        private Double ReturnDoubleValue(string value)
        {
            Double result = 0.0;

            if (Double.TryParse(value, out result))
                return result;
            else
                return 0.0; ;
        }
        #endregion

        #region 검사항목 콤보 이벤트
        // 검사항목 선택시 필수입력사항 입력되었는지 확인
        private void uComboInspectItem_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공장 입력확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchPlant.DropDown();
                }
                //else if (this.uTextSearchPackage.Text.Equals(string.Empty))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "제품코드 입력확인", "Package코드가 입력되지 않았습니다, 제품코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                //    e.Cancel = true;
                //    this.uTextSearchProductCode.Focus();
                //}
                //else if (this.uDateSearchInspectFromDate.Value.Equals(null) || this.uDateSearchInspectFromDate.Value.Equals(DBNull.Value))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "검사일자 입력확인", "유효하지 않은 검사일자입니다. 검사일자를 선택해 주세요.", Infragistics.Win.HAlign.Center);

                //    e.Cancel = true;
                //    this.uDateSearchInspectFromDate.DropDown();
                //}
                else if (this.uDateSearchInspectToDate.Value.Equals(null) || this.uDateSearchInspectToDate.Value.Equals(DBNull.Value))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검사일자 입력확인", "유효하지 않은 검사일자입니다. 검사일자를 선택해 주세요.", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uDateSearchInspectToDate.DropDown();
                }
                else if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공정그룹 입력확인", "공정그룹을 선택해주세요", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchProcessGroup.DropDown();
                }
                else if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공정 입력확인", "공정을 선택해주세요", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchProcess.Focus();
                    this.uComboSearchProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검사항목 콤보박스 선택시 데이터 조회 이벤트
        private void uComboInspectItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (e.Row == null)
                    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 규격상/하한, 규격범위 텍스트 박스 설정
                this.uTextUpperSpec.Text = string.Format("{0:f5}", e.Row.Cells["UpperSpec"].Value);
                this.uTextLowerSpec.Text = string.Format("{0:f5}", e.Row.Cells["LowerSpec"].Value);
                this.uTextSpecRange.Text = e.Row.Cells["SpecRangeName"].Value.ToString();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProductCode = ""; // this.uTextSearchProductCode.Text;
                string strProcessCode = this.uComboSearchProcess.Value.ToString();
                string strCustomerCode = ""; // this.uTextSearchCustomerCode.Text;
                string strStackSeq = e.Row.Cells["StackSeq"].Value.ToString();
                string strGeneration = e.Row.Cells["Generation"].Value.ToString();
                string strInspectItemCode = e.Row.Cells["InspectItemCode"].Value.ToString();
                string strInspectDateFrom = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).ToString("yyyy-MM-dd");
                string strInspectDateTo = Convert.ToDateTime(this.uDateSearchInspectToDate.Value).ToString("yyyy-MM-dd");

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtValue = clsStatic.mfReadINSProcessStatic_XMR(strPlantCode
                                                                        , strProductCode
                                                                        , strProcessCode
                                                                        , ""
                                                                        , strCustomerCode
                                                                        , strStackSeq
                                                                        , strGeneration
                                                                        , strInspectItemCode
                                                                        , ""
                                                                        , strInspectDateFrom
                                                                        , strInspectDateTo
                                                                        , m_resSys.GetString("SYS_LANG"));

                if (dtValue.Rows.Count > 0)
                {
                    this.uGrid1.DataSource = dtValue;
                    this.uGrid1.DataBind();

                    QRPSTA.STABasic structHistorgam = new STABasic();
                    QRPSTA.STASPC clsSTAPRC = new STASPC();

                    double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                    double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);

                    structHistorgam = clsSTAPRC.mfDrawHistogram(this.uChartHistogram, dtValue.DefaultView.ToTable(false, "InspectValue"), dblLowerSpec
                                                                , dblUpperSpec, e.Row.Cells["SpecRangeCode"].Value.ToString()
                                                                , 2, "", "", "", m_resSys.GetString("SYS_FONTNAME"), 20, 10);

                    this.uTextMean.Text = string.Format("{0:f5}", structHistorgam.Mean);
                    this.uTextStdDev.Text = string.Format("{0:f5}", structHistorgam.StdDev);
                    this.uTextMin.Text = string.Format("{0:f5}", structHistorgam.Min);
                    this.uTextMax.Text = string.Format("{0:f5}", structHistorgam.Max);

                    this.uTextCp.Text = string.Format("{0:f5}", structHistorgam.Cp);
                    this.uTextCpk.Text = string.Format("{0:f5}", structHistorgam.Cpk);
                    this.uTextCpl.Text = string.Format("{0:f5}", structHistorgam.Cpl);
                    this.uTextCpu.Text = string.Format("{0:f5}", structHistorgam.Cpu);
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                        , "검사값이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);

                    Clear();

                    this.uGrid1.DataSource = dtValue;
                    this.uGrid1.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        //설비텍스트의 에디트 버튼을 클릭할 경우 설비팝업창이 뜬다.
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();


                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "필수입력창", "필수입력확인창", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                //frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비텍스트에 코드를 입력후 엔터키를 누를 시 자동조회가 된다.
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //설비코드저장
                string strEquipCode = this.uTextSearchEquipCode.Text;

                if (e.KeyData.Equals(Keys.Enter) && !strEquipCode.Equals(string.Empty))
                {
                    //System ResourcesInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //공장정보저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    //공장정보가 공백일 경우
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "공장정보확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //설비정보BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보조회매서드 실행
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //설비정보가 있는 경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "확인창", "조회처리결과확인", "입력하신 설비코드가 존재하지 않습니다.", Infragistics.Win.HAlign.Right);
                        if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                            this.uTextSearchEquipName.Clear();
                    }


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비텍스트 정보가 변할시 설비이름이 초기화
        private void uTextSearchEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                {
                    this.uTextSearchEquipName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
