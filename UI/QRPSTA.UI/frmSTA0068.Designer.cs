﻿namespace QRPSTA.UI
{
    partial class frmSTA0068
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0068));
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridProcPPMList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchProcInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComoSearchPackageGroup1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcessOperationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcessOperationType = new Infragistics.Win.Misc.UltraLabel();
            this.uChart = new Infragistics.Win.UltraWinChart.UltraChart();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcPPMList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridProcPPMList
            // 
            this.uGridProcPPMList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcPPMList.DisplayLayout.Appearance = appearance14;
            this.uGridProcPPMList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcPPMList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcPPMList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcPPMList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridProcPPMList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcPPMList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridProcPPMList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcPPMList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcPPMList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcPPMList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridProcPPMList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcPPMList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcPPMList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcPPMList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridProcPPMList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcPPMList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcPPMList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridProcPPMList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridProcPPMList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcPPMList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcPPMList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridProcPPMList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcPPMList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridProcPPMList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcPPMList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcPPMList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcPPMList.Location = new System.Drawing.Point(4, 112);
            this.uGridProcPPMList.Name = "uGridProcPPMList";
            this.uGridProcPPMList.Size = new System.Drawing.Size(1060, 728);
            this.uGridProcPPMList.TabIndex = 279;
            this.uGridProcPPMList.Text = "ultraGrid1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProcInspectType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProcInspectType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchPackage);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchPackage);
            this.ultraGroupBox1.Controls.Add(this.uComoSearchPackageGroup1);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchPackageGroup1);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uDateSearchInspectDateTo);
            this.ultraGroupBox1.Controls.Add(this.uDateSearchInspectDateFrom);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchInspectDate);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchInspectType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchInspectType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchDetailProcessOperationType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchDetailProcessOperationType);
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, 44);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1060, 68);
            this.ultraGroupBox1.TabIndex = 310;
            // 
            // uComboSearchProcInspectType
            // 
            this.uComboSearchProcInspectType.Location = new System.Drawing.Point(920, 12);
            this.uComboSearchProcInspectType.Name = "uComboSearchProcInspectType";
            this.uComboSearchProcInspectType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchProcInspectType.TabIndex = 323;
            this.uComboSearchProcInspectType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProcInspectType
            // 
            this.uLabelSearchProcInspectType.Location = new System.Drawing.Point(816, 12);
            this.uLabelSearchProcInspectType.Name = "uLabelSearchProcInspectType";
            this.uLabelSearchProcInspectType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcInspectType.TabIndex = 322;
            this.uLabelSearchProcInspectType.Text = "공정검사구분";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(213, 21);
            this.uComboSearchPackage.TabIndex = 321;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 320;
            this.uLabelSearchPackage.Text = "Package";
            // 
            // uComoSearchPackageGroup1
            // 
            this.uComoSearchPackageGroup1.Location = new System.Drawing.Point(444, 36);
            this.uComoSearchPackageGroup1.Name = "uComoSearchPackageGroup1";
            this.uComoSearchPackageGroup1.Size = new System.Drawing.Size(120, 21);
            this.uComoSearchPackageGroup1.TabIndex = 319;
            this.uComoSearchPackageGroup1.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup1
            // 
            this.uLabelSearchPackageGroup1.Location = new System.Drawing.Point(340, 36);
            this.uLabelSearchPackageGroup1.Name = "uLabelSearchPackageGroup1";
            this.uLabelSearchPackageGroup1.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackageGroup1.TabIndex = 318;
            this.uLabelSearchPackageGroup1.Text = "Package1";
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(676, 12);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchProductActionType.TabIndex = 317;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProductActionType
            // 
            this.uLabelSearchProductActionType.Location = new System.Drawing.Point(572, 12);
            this.uLabelSearchProductActionType.Name = "uLabelSearchProductActionType";
            this.uLabelSearchProductActionType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProductActionType.TabIndex = 316;
            this.uLabelSearchProductActionType.Text = "제품구분";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(444, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchCustomer.TabIndex = 315;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(340, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 314;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uDateSearchInspectDateTo
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateTo.Appearance = appearance17;
            this.uDateSearchInspectDateTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateTo.Location = new System.Drawing.Point(228, 12);
            this.uDateSearchInspectDateTo.Name = "uDateSearchInspectDateTo";
            this.uDateSearchInspectDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateTo.TabIndex = 312;
            // 
            // uDateSearchInspectDateFrom
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateFrom.Appearance = appearance27;
            this.uDateSearchInspectDateFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateFrom.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchInspectDateFrom.Name = "uDateSearchInspectDateFrom";
            this.uDateSearchInspectDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateFrom.TabIndex = 313;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(216, 20);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel2.TabIndex = 311;
            this.ultraLabel2.Text = "~";
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectDate.TabIndex = 310;
            this.uLabelSearchInspectDate.Text = "검사일";
            // 
            // uComboSearchInspectType
            // 
            this.uComboSearchInspectType.Location = new System.Drawing.Point(920, 36);
            this.uComboSearchInspectType.Name = "uComboSearchInspectType";
            this.uComboSearchInspectType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchInspectType.TabIndex = 335;
            this.uComboSearchInspectType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectType
            // 
            this.uLabelSearchInspectType.Location = new System.Drawing.Point(816, 36);
            this.uLabelSearchInspectType.Name = "uLabelSearchInspectType";
            this.uLabelSearchInspectType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectType.TabIndex = 334;
            this.uLabelSearchInspectType.Text = "검사유형";
            // 
            // uComboSearchDetailProcessOperationType
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Appearance = appearance2;
            this.uComboSearchDetailProcessOperationType.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Location = new System.Drawing.Point(676, 36);
            this.uComboSearchDetailProcessOperationType.Name = "uComboSearchDetailProcessOperationType";
            this.uComboSearchDetailProcessOperationType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchDetailProcessOperationType.TabIndex = 327;
            this.uComboSearchDetailProcessOperationType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchDetailProcessOperationType
            // 
            this.uLabelSearchDetailProcessOperationType.Location = new System.Drawing.Point(572, 36);
            this.uLabelSearchDetailProcessOperationType.Name = "uLabelSearchDetailProcessOperationType";
            this.uLabelSearchDetailProcessOperationType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDetailProcessOperationType.TabIndex = 326;
            this.uLabelSearchDetailProcessOperationType.Text = "공정구분";
            // 
            // uChart
            // 
            this.uChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChart.Axis.PE = paintElement1;
            this.uChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.LineThickness = 1;
            this.uChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MajorGridLines.Visible = true;
            this.uChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MinorGridLines.Visible = false;
            this.uChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X.Visible = true;
            this.uChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.Visible = false;
            this.uChart.Axis.X2.LineThickness = 1;
            this.uChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MajorGridLines.Visible = true;
            this.uChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MinorGridLines.Visible = false;
            this.uChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X2.Visible = false;
            this.uChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.LineThickness = 1;
            this.uChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MajorGridLines.Visible = true;
            this.uChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MinorGridLines.Visible = false;
            this.uChart.Axis.Y.TickmarkInterval = 20;
            this.uChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y.Visible = true;
            this.uChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.Visible = false;
            this.uChart.Axis.Y2.LineThickness = 1;
            this.uChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChart.Axis.Y2.TickmarkInterval = 20;
            this.uChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y2.Visible = false;
            this.uChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z.Labels.ItemFormatString = "";
            this.uChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.LineThickness = 1;
            this.uChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MajorGridLines.Visible = true;
            this.uChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MinorGridLines.Visible = false;
            this.uChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z.Visible = false;
            this.uChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.Visible = false;
            this.uChart.Axis.Z2.LineThickness = 1;
            this.uChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z2.Visible = false;
            this.uChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChart.Effects.Effects.Add(gradientEffect1);
            this.uChart.Location = new System.Drawing.Point(4, 540);
            this.uChart.Name = "uChart";
            this.uChart.Size = new System.Drawing.Size(1060, 308);
            this.uChart.TabIndex = 311;
            this.uChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            this.uChart.Visible = false;
            // 
            // frmSTA0068
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridProcPPMList);
            this.Controls.Add(this.uChart);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0068";
            this.Text = "frmSTA0068";
            this.Load += new System.EventHandler(this.frmSTA0068_Load);
            this.Activated += new System.EventHandler(this.frmSTA0068_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0068_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcPPMList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcPPMList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcInspectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComoSearchPackageGroup1;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductActionType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcessOperationType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcessOperationType;
        private Infragistics.Win.UltraWinChart.UltraChart uChart;
    }
}