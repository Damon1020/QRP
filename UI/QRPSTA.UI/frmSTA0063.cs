﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0063.cs                                         */
/* 프로그램명   : Xbar/S 관리도 분석                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0063 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0063()
        {
            InitializeComponent();
        }

        private void frmSTA0063_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0063_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("Xbar/S 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSTA0063_Resize);
        }

        void frmSTA0063_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    this.uGroupBox2.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBox1.Width;
                    this.uGroupBox3.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBox1.Width;
                }
                else
                {
                    this.uGroupBox2.Width = 616;
                    this.uGroupBox3.Width = 616;
                    this.uGroupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBox3.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                //this.uTextSearchProductCode.MaxLength = 20;
                //this.uTextSearchCustomerCode.MaxLength = 10;

                this.uDateSearchInspectFromDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.CHART, "X-bar 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.CHART, "S 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup5, "PackageGroup", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipCode, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //wLabel.mfSetLabel(this.uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //wLabel.mfSetLabel(this.uLabelSpecUpper, "규격상한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecLower, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.uLabelSpecRange, "규격범위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelXBar, "XBar관리도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS, "S관리도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLCL, "LCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCL, "CL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUCL, "UCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStdDeviation, "표준편차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMax, "Max", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMin, "Min", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelOverUCL, "OverUCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderLCL, "UnderLCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOOC, "OOC", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverUSL, "OverUSL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderLSL, "UnderLSL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOOS, "OOS", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverRun, "OverRun", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderRun, "UnderRun", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRun, "RUN", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverTrend, "OverTrend", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderTrend, "UnderTrend", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTrend, "TREND", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCp, "Cp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpk, "Cpk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpl, "Cpl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpu, "Cpu", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPp, "Pp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpk, "Ppk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpl, "Ppl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpu, "Ppu", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboGrid wCombo = new WinComboGrid();
                WinComboEditor wComboe = new WinComboEditor();

                // SearchArea
                QRPBrowser brwChannel = new QRPBrowser();

                //고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustomer);

                //공장 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 검사항목 콤보박스
                wCombo.mfInitGeneralComboGrid(this.uComboSearchInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ItemNum", "항목순번", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemCode", "검사항목코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemName", "검사항목명", false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "StackSeq", "Stack", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "Generation", "Generation", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "UpperSpec", "규격상한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "LowerSpec", "규격하한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeCode", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeName", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ProcessInspectSS", "SampleSize", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                //this.uComboInspectItem.Text = "선택";

                // 공정콤보
                wCombo.mfInitGeneralComboGrid(this.uComboSearchProcess, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcess.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wCombo.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessCode", "공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessName", "공정명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectTime", "시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdDev", "표준편차", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DataRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MinValue", "최소값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MaxValue", "최대값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000262", "M000266", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchInspectToDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000205", "M000052", Infragistics.Win.HAlign.Center);
                    return;
                }

                else if (this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) || this.uComboSearchCustomer.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000289", "M001274", Infragistics.Win.HAlign.Center);
                    this.uComboSearchProcessGroup.DropDown();
                    return;
                }
                else if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty) || this.uComboSearchPackage.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001092", "M000100", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPackage.DropDown();
                    return;
                }
                else if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) || this.uComboSearchProcessGroup.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000289", "M000290", Infragistics.Win.HAlign.Center);
                    this.uComboSearchProcessGroup.DropDown();
                    return;
                }
                //else if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "공정 입력확인", "공정을 선택해주세요", Infragistics.Win.HAlign.Center);

                //    this.uComboSearchProcess.Focus();
                //    this.uComboSearchProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                //    return;
                //}
                else if (this.uComboSearchInspectItem.Value == null || this.uComboSearchInspectItem.Value == DBNull.Value || this.uComboSearchInspectItem.Value.ToString().Equals(string.Empty) ||
                    this.uComboSearchInspectItem.SelectedRow == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000189", "M000193", Infragistics.Win.HAlign.Center);

                    this.uComboSearchInspectItem.Focus();
                    this.uComboSearchInspectItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                    return;
                }

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 검색조건 설정

                // 이전 분기데이터 조회 하기 위한 날짜 설정
                DateTime dateInspectFromDate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).AddDays(-1);
                DateTime dateInspectToDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);
                DateTime datePreqFromDate = new DateTime();
                DateTime datePreqToDate = new DateTime();
                if (dateInspectToDate.Month.Equals(1) || dateInspectToDate.Month.Equals(2) || dateInspectToDate.Month.Equals(3))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 9, 30);
                    datePreqToDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 12, 31);
                }
                else if (dateInspectToDate.Month.Equals(4) || dateInspectToDate.Month.Equals(5) || dateInspectToDate.Month.Equals(6))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 12, 31);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 3, 31);
                }
                else if (dateInspectToDate.Month.Equals(7) || dateInspectToDate.Month.Equals(8) || dateInspectToDate.Month.Equals(9))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.Year, 3, 31);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 6, 30);
                }
                else if (dateInspectToDate.Month.Equals(10) || dateInspectToDate.Month.Equals(11) || dateInspectToDate.Month.Equals(12))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.Year, 6, 30);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 9, 30);
                }

                // 이전 3개월 데이터 조회하기 위한 날짜 설정
                DateTime dateFromDate = new DateTime(Convert.ToDateTime(this.uDateSearchInspectToDate.Value).AddMonths(-3).Year, Convert.ToDateTime(this.uDateSearchInspectToDate.Value).AddMonths(-3).Month, 1).AddDays(-1);   // 저번달로 부터 3개월전 첫일
                DateTime dateToDate = new DateTime(Convert.ToDateTime(this.uDateSearchInspectToDate.Value).Year, Convert.ToDateTime(this.uDateSearchInspectToDate.Value).Month, 1).AddDays(-1);                                 // 저번달 마지막일

                // 날짜를 제외한 검색조건 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();
                string strProcessCode = "";
                if (this.uComboSearchProcess.Value != null)
                    strProcessCode = this.uComboSearchProcess.Value.ToString();
                string strInspectItemCode = this.uComboSearchInspectItem.Value.ToString();
                string strStackSeq = this.uTextSearchStackSeq.Text;
                string strGeneration = this.uTextSearchGeneration.Text;
                this.uTextSearchStackSeq.Text = strStackSeq;
                this.uTextSearchGeneration.Text = strGeneration;
                string strEquipCode = this.uTextSearchEquipCode.Text;

                #endregion

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                // 바인딩될 데이터는 검색조건에 검사일로 조회
                DataTable dtBindData = clsStatic.mfReadINSProcessStatic_XBar_Xn(strPlantCode
                                                                            , strPackage
                                                                            , strProcessCode
                                                                            , strProcessGroupCode
                                                                            , strCustomerCode
                                                                            , strStackSeq
                                                                            , strGeneration
                                                                            , strInspectItemCode
                                                                            , strEquipCode
                                                                            , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                            , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                            , m_resSys.GetString("SYS_LANG"));

                // 데이터 바인딩
                this.uGrid1.SetDataBinding(dtBindData, string.Empty);

                if (dtBindData.Rows.Count > 0)
                {
                    // Data조회
                    // 이전 3개월 데이터
                    DataTable dtData = clsStatic.mfReadINSProcessStatic_XBar_Xn(strPlantCode
                                                                                , strPackage
                                                                                , strProcessCode
                                                                                , strProcessGroupCode
                                                                                , strCustomerCode
                                                                                , strStackSeq
                                                                                , strGeneration
                                                                                , strInspectItemCode
                                                                                , strEquipCode
                                                                                , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                , dateToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                                , m_resSys.GetString("SYS_LANG"));

                    // 이전 분기 데이터
                    DataTable dtPreqData = clsStatic.mfReadINSProcessStatic_XBar_Xn(strPlantCode
                                                                                , strPackage
                                                                                , strProcessCode
                                                                                , strProcessGroupCode
                                                                                , strCustomerCode
                                                                                , strStackSeq
                                                                                , strGeneration
                                                                                , strInspectItemCode
                                                                                , strEquipCode
                                                                                , datePreqFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                , datePreqToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                                , m_resSys.GetString("SYS_LANG"));
                    if (dtPreqData.Rows.Count.Equals(0) && dtData.Rows.Count.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001118"
                            , "M001461", Infragistics.Win.HAlign.Center);

                        ////// POPUP창 Close
                        ////this.MdiParent.Cursor = Cursors.Default;
                        ////m_ProgressPopup.mfCloseProgressPopup(this);

                        ////Clear();

                        ////return;
                    }

                    QRPSTA.STASummary structXBar = new STASummary();
                    QRPSTA.STASummary structS = new STASummary();
                    QRPSTA.STASPC clsSTAPRC = new STASPC();
                    STAControlLimit XBarCL = new STAControlLimit();
                    DataTable dtXValue = new DataTable();

                    // 변수 설정
                    double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                    double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);
                    int intSampleSize;
                    if (uTextSampleSize.Text == "" || uTextSampleSize.Text == "0")
                        intSampleSize = 5;
                    else
                        intSampleSize = Convert.ToInt32(Convert.ToDouble(this.uTextSampleSize.Text));

                    if (dtPreqData.Rows.Count > 0)
                    {
                        //이전분기 데이터가 있는 경우 이전분기 데이터로 관리한계선을 구한다.
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithS(dtPreqData.DefaultView.ToTable(false, "Mean"), dtPreqData.DefaultView.ToTable(false, "StdDev"), intSampleSize);
                    }
                    else
                    {
                        //이전분기 데이터가 있는 경우 이전분기 데이터로 관리한계선을 구한다.
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithS(dtData.DefaultView.ToTable(false, "Mean"), dtData.DefaultView.ToTable(false, "StdDev"), intSampleSize);
                    }

                    structXBar = clsSTAPRC.mfDrawXBarChartWithSChart4SubGroupMean(this.uChartXBar, dtBindData.DefaultView.ToTable(false, "Mean"), dtBindData.DefaultView.ToTable(false, "StdDev")
                                                    , dblLowerSpec, dblUpperSpec, this.uTextSpecRangeCode.Text, true, XBarCL.XBRLCL, XBarCL.XBCL, XBarCL.XBRUCL, intSampleSize, true, true, "", ""
                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    structS = clsSTAPRC.mfDrawSChart4SubGroupMean(this.uChartS, dtBindData.DefaultView.ToTable(false, "StdDev"), true, XBarCL.XLCL, XBarCL.XCL, XBarCL.XUCL
                                                , intSampleSize, "", "", "", m_resSys.GetString("SYS_FONTNAME")
                                                        , 50, 10, 0);

                    // 통계값 계산을 위한 조회
                    dtXValue = clsStatic.mfReadINSProcessStatic_XMR(strPlantCode
                                                                        , strPackage
                                                                        , strProcessCode
                                                                        , strProcessGroupCode
                                                                        , strCustomerCode
                                                                        , strStackSeq
                                                                        , strGeneration
                                                                        , strInspectItemCode
                                                                        , strEquipCode
                                                                        , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                        , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                        , m_resSys.GetString("SYS_LANG"));

                    // 통계값 계산
                    QRPSTA.STABAS clsSTABas = new STABAS();
                    STABasic clsBasic = clsSTABas.mfCalcBasicStat(dtXValue.DefaultView.ToTable(false, "InspectValue"), dblLowerSpec, dblUpperSpec, uTextSpecRangeCode.Text);

                    this.uTextCp.Text = string.Format("{0:f5}", structXBar.CpDA);
                    this.uTextCpk.Text = string.Format("{0:f5}", structXBar.CpkDA);
                    this.uTextCpl.Text = string.Format("{0:f5}", structXBar.CplDA);
                    this.uTextCpu.Text = string.Format("{0:f5}", structXBar.CpuDA);
                    this.uTextPp.Text = string.Format("{0:f5}", clsBasic.Cp);
                    this.uTextPpk.Text = string.Format("{0:f5}", clsBasic.Cpk);
                    this.uTextPpl.Text = string.Format("{0:f5}", clsBasic.Cpl);
                    this.uTextPpu.Text = string.Format("{0:f5}", clsBasic.Cpu);

                    this.uTextXBarCL.Text = string.Format("{0:f5}", XBarCL.XBCL);
                    this.uTextXBarLCL.Text = string.Format("{0:f5}", XBarCL.XBSLCL);
                    this.uTextXBarUCL.Text = string.Format("{0:f5}", XBarCL.XBSUCL);
                    this.uTextSCL.Text = string.Format("{0:f5}", XBarCL.SCL);
                    this.uTextSLCL.Text = string.Format("{0:f5}", XBarCL.SLCL);
                    this.uTextSUCL.Text = string.Format("{0:f5}", XBarCL.SUCL);


                    this.uTextStdDev.Text = string.Format("{0:f5}", structXBar.StdDevDA);
                    this.uTextMin.Text = string.Format("{0:f5}", structXBar.MinDA);
                    this.uTextMax.Text = string.Format("{0:f5}", structXBar.MaxDA);

                    this.uTextOverUCL.Text = structXBar.UCLUpperCount.ToString();
                    this.uTextOverUCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UCLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLCL.Text = structXBar.LCLLowerCount.ToString();
                    this.uTextUnderLCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LCLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextOOC.Text = structXBar.CLOverCount.ToString();

                    this.uTextOverUSL.Text = structXBar.USLUpperCount.ToString();
                    this.uTextOverUSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.USLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLSL.Text = structXBar.LSLLowerCount.ToString();
                    this.uTextUnderLSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LSLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextOOS.Text = structXBar.SLOverCount.ToString();

                    this.uTextOverRun.Text = structXBar.UpwardRunCount.ToString();
                    this.uTextOverRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderRun.Text = structXBar.DownwardRunCount.ToString();
                    this.uTextUnderRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextRun.Text = (structXBar.DownwardRunCount + structXBar.UpwardRunCount).ToString();

                    this.uTextOverTrend.Text = structXBar.UpwardTrendCount.ToString();
                    this.uTextOverTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderTrend.Text = structXBar.DownwardTrendCount.ToString();
                    this.uTextUnderTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextTrend.Text = (structXBar.UpwardTrendCount + structXBar.DownwardTrendCount).ToString();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001101"
                        , "M000171", Infragistics.Win.HAlign.Center);

                    Clear();
                }

                #region 기존소스 주석처리

                /*
                //Infragistics.Win.UltraWinGrid.RowSelectedEventArgs aa = new Infragistics.Win.UltraWinGrid.RowSelectedEventArgs(this.uComboInspectItem, this.uComboInspectItem.SelectedRow);
                // 이벤트 호출
                //uComboInspectItem_RowSelected(this.uComboInspectItem, aa);

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공장 입력확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "제품코드 입력확인", "Package코드가 입력되지 않았습니다, 제품코드를 입력해주세요", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPackage.DropDown();
                    return;
                }
                if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공정그룹 입력확인", "공정그룹을 선택해주세요", Infragistics.Win.HAlign.Center);
                    this.uComboSearchProcessGroup.DropDown();
                    return;
                }
                //if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                                , "확인창", "공정 입력확인", "공정을 선택해주세요", Infragistics.Win.HAlign.Center);

                //    this.uComboSearchProcess.Focus();
                //    this.uComboSearchProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                //    return;
                //}
                if (this.uComboSearchInspectItem.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검사항목 입력확인", "검사항목을 선택해주세요", Infragistics.Win.HAlign.Center);

                    this.uComboSearchInspectItem.Focus();
                    this.uComboSearchInspectItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                    return;
                }

                // 검색조건 날짜로 검색하는 것에서 ToDate날짜 기준으로 3개월 전 데이터 가져오기(-- 2012.02.22 이종호 --)
                ////string strInspectDateFrom = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).ToString("yyyy-MM-dd");
                ////string strInspectDateTo = Convert.ToDateTime(this.uDateSearchInspectToDate.Value).ToString("yyyy-MM-dd");
                // 3개월 이전 날짜 체크
                DateTime dateInspectDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);
                DateTime dateFromDate = new DateTime(dateInspectDate.AddMonths(-3).Year, dateInspectDate.AddMonths(-3).Month, 1);   // 저번달로 부터 3개월전 첫일
                DateTime dateToDate = new DateTime(dateInspectDate.Year, dateInspectDate.Month, 1).AddDays(-1);                     // 저번달 마지막일
                string strInspectDateFrom = dateFromDate.ToString("yyyy-MM-dd");
                string strInspectDateTo = dateToDate.ToString("yyyy-MM-dd");

                DateTime dtFromDate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value);
                DateTime dtToDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);
                TimeSpan total = dtToDate - dtFromDate;
                if (total.TotalDays < 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검색기간 입력확인", "From일자를 To일자보다 이전 날짜로 선택해주세요", Infragistics.Win.HAlign.Center);
                    return;
                }

                string strPreQDateFrom = "";
                string strPreQDateTo = "";
                //이전분기 일자 구하기
                if (strInspectDateTo.Substring(5, 2) == "01" || strInspectDateTo.Substring(5, 2) == "02" || strInspectDateTo.Substring(5, 2) == "03")
                {
                    strPreQDateFrom = (Convert.ToInt32(strInspectDateTo.Substring(0, 4)) - 1).ToString() + "-10-01";
                    strPreQDateTo = (Convert.ToInt32(strInspectDateTo.Substring(0, 4)) - 1).ToString() + "-12-31";
                }
                else if (strInspectDateTo.Substring(5, 2) == "04" || strInspectDateTo.Substring(5, 2) == "05" || strInspectDateTo.Substring(5, 2) == "06")
                {
                    strPreQDateFrom = strInspectDateTo.Substring(0, 4) + "-01-01";
                    strPreQDateTo = strInspectDateTo.Substring(0, 4) + "-03-31";
                }
                else if (strInspectDateTo.Substring(5, 2) == "07" || strInspectDateTo.Substring(5, 2) == "08" || strInspectDateTo.Substring(5, 2) == "09")
                {
                    strPreQDateFrom = strInspectDateTo.Substring(0, 4) + "-04-01";
                    strPreQDateTo = strInspectDateTo.Substring(0, 4) + "-06-30";
                }
                else if (strInspectDateTo.Substring(5, 2) == "10" || strInspectDateTo.Substring(5, 2) == "11" || strInspectDateTo.Substring(5, 2) == "12")
                {
                    strPreQDateFrom = strInspectDateTo.Substring(0, 4) + "-08-01";
                    strPreQDateTo = strInspectDateTo.Substring(0, 4) + "-09-30";
                }

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProductCode = ""; // this.uTextSearchProductCode.Text;
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();

                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();

                string strProcessCode = "";
                if (this.uComboSearchProcess.Value != null)
                    strProcessCode = this.uComboSearchProcess.Value.ToString();

                string strInspectItemCode = this.uComboSearchInspectItem.Value.ToString(); //e.Row.Cells["InspectItemCode"].Value.ToString();
                string strStackSeq = this.uTextSearchStackSeq.Text; //e.Row.Cells["StackSeq"].Value.ToString();
                string strGeneration = this.uTextSearchGeneration.Text; //e.Row.Cells["Generation"].Value.ToString();
                this.uTextSearchStackSeq.Text = strStackSeq;
                this.uTextSearchGeneration.Text = strGeneration;

                string strEquipCode = this.uTextSearchEquipCode.Text;

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtValue = clsStatic.mfReadINSProcessStatic_XBar(strPlantCode
                                                                        , strPackage
                                                                        , strProcessCode
                                                                        , strProcessGroupCode
                                                                        , strCustomerCode
                                                                        , strStackSeq
                                                                        , strGeneration
                                                                        , strInspectItemCode
                                                                        , strEquipCode
                                                                        , strInspectDateFrom
                                                                        , strInspectDateTo
                                                                        , m_resSys.GetString("SYS_LANG"));

                DataTable dtPreQValue = clsStatic.mfReadINSProcessStatic_XBar(strPlantCode
                                                                        , strPackage
                                                                        , strProcessCode
                                                                        , strProcessGroupCode
                                                                        , strCustomerCode
                                                                        , strStackSeq
                                                                        , strGeneration
                                                                        , strInspectItemCode
                                                                        , strEquipCode
                                                                        , strPreQDateFrom
                                                                        , strPreQDateTo
                                                                        , m_resSys.GetString("SYS_LANG"));

                DataTable dtValue_Grid = clsStatic.mfReadINSProcessStatic_XBar_Xn(strPlantCode
                                                                            , strPackage
                                                                            , strProcessCode
                                                                            , strProcessGroupCode
                                                                            , strCustomerCode
                                                                            , strStackSeq
                                                                            , strGeneration
                                                                            , strInspectItemCode
                                                                            , strEquipCode
                                                                            , strInspectDateFrom
                                                                            , strInspectDateTo
                                                                            , m_resSys.GetString("SYS_LANG"));

                DataTable dtPreValue_Grid = clsStatic.mfReadINSProcessStatic_XBar_Xn(strPlantCode
                                                                                            , strPackage
                                                                                            , strProcessCode
                                                                                            , strProcessGroupCode
                                                                                            , strCustomerCode
                                                                                            , strStackSeq
                                                                                            , strGeneration
                                                                                            , strInspectItemCode
                                                                                            , strEquipCode
                                                                                            , strPreQDateFrom
                                                                                            , strPreQDateTo
                                                                                            , m_resSys.GetString("SYS_LANG"));

                if (dtValue.Rows.Count > 0 || dtPreQValue.Rows.Count > 0)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticCL), "ProcessStaticCL");
                    QRPSTA.BL.STAPRC.ProcessStaticCL clsStaticCL = new QRPSTA.BL.STAPRC.ProcessStaticCL();
                    brwChannel.mfCredentials(clsStaticCL);

                    QRPSTA.STASummary structXBar = new STASummary();
                    QRPSTA.STASummary structS = new STASummary();
                    QRPSTA.STASPC clsSTAPRC = new STASPC();

                    double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                    double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);

                    int intSampleSize;
                    if (uTextSampleSize.Text == "" || uTextSampleSize.Text == "0")
                        intSampleSize = 5;
                    else
                        intSampleSize = Convert.ToInt32(Convert.ToDouble(uTextSampleSize.Text));

                    STAControlLimit XBarCL = new STAControlLimit();
                    //이전분기 데이터가 있는 경우 이전분기 데이터로 관리한계선을 구한다.
                    if (dtPreQValue.Rows.Count > 0)
                    {
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithS(dtPreQValue.DefaultView.ToTable(false, "Mean"), dtPreQValue.DefaultView.ToTable(false, "StdDev"), intSampleSize);

                        structXBar = clsSTAPRC.mfDrawXBarChartWithSChart4SubGroupMean(this.uChartXbar, dtPreQValue.DefaultView.ToTable(false, "Mean"), dtPreQValue.DefaultView.ToTable(false, "StdDev")
                                                    , dblLowerSpec, dblUpperSpec, this.uTextSpecRangeCode.Text, true, XBarCL.XBSLCL, XBarCL.XBSUCL, true, true, "", ""
                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                        structS = clsSTAPRC.mfDrawSChart4SubGroupMean(this.uChartS, dtPreQValue.DefaultView.ToTable(false, "StdDev"), intSampleSize, "", "", "", m_resSys.GetString("SYS_FONTNAME")
                                                            , 50, 10, 0);
                    }

                    // 이전분기 데이터가 없는 경우 이전 3개월치 데이터로 관리한계선을 구한다.
                    else
                    {
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithS(dtValue.DefaultView.ToTable(false, "Mean"), dtValue.DefaultView.ToTable(false, "StdDev"), intSampleSize);

                        structXBar = clsSTAPRC.mfDrawXBarChartWithSChart4SubGroupMean(this.uChartXbar, dtValue.DefaultView.ToTable(false, "Mean"), dtValue.DefaultView.ToTable(false, "StdDev")
                                                    , dblLowerSpec, dblUpperSpec, this.uTextSpecRangeCode.Text, true, XBarCL.XBSLCL, XBarCL.XBSUCL, true, true, "", ""
                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                        structS = clsSTAPRC.mfDrawSChart4SubGroupMean(this.uChartS, dtValue.DefaultView.ToTable(false, "StdDev"), intSampleSize, "", "", "", m_resSys.GetString("SYS_FONTNAME")
                                                            , 50, 10, 0);
                    }

                    if (dtPreValue_Grid.Rows.Count > 0)
                        this.uGrid1.SetDataBinding(dtPreValue_Grid, string.Empty);
                    else
                        this.uGrid1.SetDataBinding(dtValue_Grid, string.Empty);

                    DataTable dtXValue = new DataTable();
                    if (dtPreQValue.Rows.Count > 0)
                    {
                        dtXValue = clsStatic.mfReadINSProcessStatic_XMR(strPlantCode
                                                                            , strPackage
                                                                            , strProcessCode
                                                                            , strProcessGroupCode
                                                                            , strCustomerCode
                                                                            , strStackSeq
                                                                            , strGeneration
                                                                            , strInspectItemCode
                                                                            , strEquipCode
                                                                            , strPreQDateFrom
                                                                            , strPreQDateTo
                                                                            , m_resSys.GetString("SYS_LANG"));
                    }
                    else
                    {
                        dtXValue = clsStatic.mfReadINSProcessStatic_XMR(strPlantCode
                                                                            , strPackage
                                                                            , strProcessCode
                                                                            , strProcessGroupCode
                                                                            , strCustomerCode
                                                                            , strStackSeq
                                                                            , strGeneration
                                                                            , strInspectItemCode
                                                                            , strEquipCode
                                                                            , strInspectDateFrom
                                                                            , strInspectDateTo
                                                                            , m_resSys.GetString("SYS_LANG"));
                    }

                    QRPSTA.STABAS clsSTABas = new STABAS();
                    STABasic clsBasic = clsSTABas.mfCalcBasicStat(dtXValue.DefaultView.ToTable(false, "InspectValue"), dblLowerSpec, dblUpperSpec, uTextSpecRangeCode.Text);

                    this.uTextPp.Text = clsBasic.Cp.ToString();
                    this.uTextPpk.Text = clsBasic.Cpk.ToString();
                    this.uTextPpl.Text = clsBasic.Cpl.ToString();
                    this.uTextPpu.Text = clsBasic.Cpu.ToString();

                    this.uTextXBarCL.Text = string.Format("{0:f5}", structXBar.MeanDA);
                    this.uTextXBarLCL.Text = string.Format("{0:f5}", structXBar.LCLDA);
                    this.uTextXBarUCL.Text = string.Format("{0:f5}", structXBar.UCLDA);
                    this.uTextSCL.Text = string.Format("{0:f5}", structS.MeanDA);
                    this.uTextSLCL.Text = string.Format("{0:f5}", structS.LCLDA);
                    this.uTextSUCL.Text = string.Format("{0:f5}", structS.UCLDA);


                    this.uTextStdDev.Text = string.Format("{0:f5}", structXBar.StdDevDA);
                    this.uTextMin.Text = string.Format("{0:f5}", structXBar.MinDA);
                    this.uTextMax.Text = string.Format("{0:f5}", structXBar.MaxDA);

                    this.uTextOverUCL.Text = structXBar.UCLUpperCount.ToString();
                    this.uTextOverUCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UCLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLCL.Text = structXBar.LCLLowerCount.ToString();
                    this.uTextUnderLCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LCLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextOOC.Text = structXBar.CLOverCount.ToString();

                    this.uTextOverUSL.Text = structXBar.USLUpperCount.ToString();
                    this.uTextOverUSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.USLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLSL.Text = structXBar.LSLLowerCount.ToString();
                    this.uTextUnderLSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LSLLowerCount / structXBar.CountDA) * 100).ToString()) + "%";
                    this.uTextOOS.Text = structXBar.SLOverCount.ToString();

                    this.uTextOverRun.Text = structXBar.UpwardRunCount.ToString();
                    this.uTextOverRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderRun.Text = structXBar.DownwardRunCount.ToString();
                    this.uTextUnderRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextRun.Text = (structXBar.DownwardRunCount + structXBar.UpwardRunCount).ToString();

                    this.uTextOverTrend.Text = structXBar.UpwardTrendCount.ToString();
                    this.uTextOverTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderTrend.Text = structXBar.DownwardTrendCount.ToString();
                    this.uTextUnderTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextTrend.Text = (structXBar.UpwardTrendCount + structXBar.DownwardTrendCount).ToString();

                    this.uTextCp.Text = string.Format("{0:f5}", structXBar.CpDA);
                    this.uTextCpk.Text = string.Format("{0:f5}", structXBar.CpkDA);
                    this.uTextCpl.Text = string.Format("{0:f5}", structXBar.CplDA);
                    this.uTextCpu.Text = string.Format("{0:f5}", structXBar.CpuDA);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                        , "검사값이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);

                    Clear();

                    ////this.uGrid1.DataSource = dtValue;
                    ////this.uGrid1.DataBind();

                    this.uGrid1.DataSource = dtValue_Grid;
                    this.uGrid1.DataBind();
                }
                 * */

                #endregion

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGrid1.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 검색조건 이벤트
        #region ComboBox
        // 검색조건 공장코드 선택시 공정그룹 콤보박스 선택하는 코드
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtProcessGroup = new DataTable();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //검색조건 값 초기화
                this.uComboSearchProcessGroup.Items.Clear();
                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchProcessGroup.Items.Clear();
                this.uTextSearchStackSeq.Clear();
                this.uTextSearchGeneration.Clear();

                //this.uTextSearchProductCode.Clear();
                //this.uTextSearchProductName.Clear();
                //this.uTextSearchPackage.Clear();
                //this.uTextSearchCustomerCode.Clear();
                //this.uTextSearchCustomerName.Clear();

                //분석데이터 초기화
                Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    WinComboEditor wCombo = new WinComboEditor();

                    //PackageGroup5, Package 콤보박스
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "5", m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchPackageGroup5, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                    DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "Package", "ComboName", dtPackage);

                    //공정그룹 콤보박스
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcessGroup = clsProcess.mfReadMASProcessGroup(this.uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "ProcessGroup", "ComboName", dtProcessGroup);

                    //검사항목 콤보박스 (고객, Package, 공정 모두 선택시 처리)
                    //InitInspectItemCombo();
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 공정그룹 선택시 공정콤보박스 설정 이벤트
        private void uComboSearchProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Infragistics.Win.ValueListItem vlItem = this.uComboSearchProcessGroup.ValueList.FindByDataValue(this.uComboSearchProcessGroup.Value);
                if (vlItem != null)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DataTable dtProcess = new DataTable();

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();

                    if (!strPlantCode.Equals(string.Empty) && !strProcessGroup.Equals(string.Empty))
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                        QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                        brwChannel.mfCredentials(clsProcess);

                        dtProcess = clsProcess.mfReadMASProcessCombo_ProcessGroup(strPlantCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));
                    }

                    this.uComboSearchProcess.DataSource = dtProcess;
                    this.uComboSearchProcess.DataBind();

                    this.uComboSearchProcess.Value = null;

                    //검색-검사항목을 설정한다.
                    if (!(this.uComboSearchProcessGroup.SelectedItem == null) && !(this.uComboSearchCustomer.SelectedItem == null) && !(this.uComboSearchPackage.SelectedItem == null))
                    {
                        if (!this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) &&
                            !this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) &&
                            !this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                            InitInspectItemCombo();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 공정콤보 설정시 검사항목 콤보박스 설정하기
        private void uComboSearchProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //검색-검사항목을 설정한다.
                if (!(this.uComboSearchProcessGroup.SelectedItem == null) && !(this.uComboSearchCustomer.SelectedItem == null) && !(this.uComboSearchPackage.SelectedItem == null))
                {
                    if (!this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) &&
                        !this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) &&
                        !this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                        InitInspectItemCombo();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion
        #endregion

        #region Method...
        // 검사항목 콤보박스 설정 메소드
        private void InitInspectItemCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.Focus();
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000097", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPackage.Focus();
                    this.uComboSearchPackage.DropDown();
                    return;
                }

                if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000283", Infragistics.Win.HAlign.Center);

                    this.uComboSearchProcessGroup.Focus();
                    return;
                }

                if (this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) || this.uComboSearchCustomer.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M001274", Infragistics.Win.HAlign.Center);

                    this.uComboSearchCustomer.Focus();
                    this.uComboSearchCustomer.DropDown();
                    return;
                }

                //if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                    , "확인창", "입력사항 확인", "공정을 선택해 주세요", Infragistics.Win.HAlign.Center);

                //    this.uComboSearchProcess.Focus();
                //    return;
                //}

                // SystemInfo ResourceSet
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                
                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();

                string strProcessCode = "";
                if (this.uComboSearchProcess.Value != null)
                    strProcessCode = this.uComboSearchProcess.Value.ToString();

                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtItem = clsStatic.mfReadINSProcessStatic_InspectItemCombo(strPlantCode, strPackage, strProcessGroupCode, strProcessCode, strCustomerCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchInspectItem.DataSource = dtItem;
                this.uComboSearchInspectItem.DataBind();

                ////if (!(dtItem.Rows.Count > 0))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                ////    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001101"
                ////    , "M000196", Infragistics.Win.HAlign.Center);
                ////}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 차트 초기화 메소드
        private void ClearChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                //DataTable dtInit = new DataTable();
                //dtInit.Columns.Add("1", typeof(Int32));
                ////dtInit.Columns.Add("b", typeof(Int32));

                //DataRow drRow = dtInit.NewRow();
                //drRow["1"] = 0;
                ////drRow["b"] = 0;
                //dtInit.Rows.Add(drRow);

                //uChart.DataSource = dtInit;
                //uChart.DataBind();

                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                //this.uTextUpperSpec.Clear();
                //this.uTextLowerSpec.Clear();
                //this.uTextSpecRange.Clear();
                this.uTextStdDev.Clear();
                this.uTextMax.Clear();
                this.uTextMin.Clear();
                this.uTextXBarLCL.Clear();
                this.uTextXBarCL.Clear();
                this.uTextXBarUCL.Clear();
                this.uTextSLCL.Clear();
                this.uTextSCL.Clear();
                this.uTextSUCL.Clear();

                this.uTextOverUCL.Clear();
                this.uTextOverUCLP.Clear();
                this.uTextUnderLCLP.Clear();
                this.uTextUnderLCL.Clear();
                this.uTextCp.Clear();
                this.uTextOOC.Clear();
                this.uTextOverUSLP.Clear();
                this.uTextOverUSL.Clear();
                this.uTextUnderLSLP.Clear();
                this.uTextUnderLSL.Clear();
                this.uTextCpk.Clear();
                this.uTextOOS.Clear();
                this.uTextOverRunP.Clear();
                this.uTextOverRun.Clear();
                this.uTextUnderRunP.Clear();
                this.uTextUnderRun.Clear();
                this.uTextCpu.Clear();
                this.uTextRun.Clear();
                this.uTextOverTrendP.Clear();
                this.uTextOverTrend.Clear();
                this.uTextUnderTrendP.Clear();
                this.uTextUnderTrend.Clear();
                this.uTextCpl.Clear();
                this.uTextTrend.Clear();

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                ClearChart(this.uChartS);
                ClearChart(this.uChartXBar);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Double 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">Double로 반환받을 값</param>
        /// <returns></returns>
        private Double ReturnDoubleValue(string value)
        {
            Double result = 0.0;

            if (Double.TryParse(value, out result))
                return result;
            else
                return 0.0; ;
        }
        #endregion

        #region 검사항목 콤보 이벤트
        // 검사항목 선택시 필수입력사항 입력되었는지 확인
        private void uComboInspectItem_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000262", "M000266", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchPlant.DropDown();
                }
                if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty) || this.uComboSearchPackage.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001092", "M000100", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchPackage.DropDown();
                }
                ////else if (this.uDateSearchInspectFromDate.Value.Equals(null) || this.uDateSearchInspectFromDate.Value.Equals(DBNull.Value))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "확인창", "검사일자 입력확인", "유효하지 않은 검사일자입니다. 검사일자를 선택해 주세요.", Infragistics.Win.HAlign.Center);

                ////    e.Cancel = true;
                ////    this.uDateSearchInspectFromDate.DropDown();
                ////}
                ////else if (this.uDateSearchInspectToDate.Value.Equals(null) || this.uDateSearchInspectToDate.Value.Equals(DBNull.Value))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "확인창", "검사일자 입력확인", "유효하지 않은 검사일자입니다. 검사일자를 선택해 주세요.", Infragistics.Win.HAlign.Center);

                ////    e.Cancel = true;
                ////    this.uDateSearchInspectToDate.DropDown();
                ////}
                if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) || this.uComboSearchProcessGroup.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000289", "M000290", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchProcessGroup.DropDown();
                }
                ////if (this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "M001264", "M000281", "M000293", Infragistics.Win.HAlign.Center);

                ////    e.Cancel = true;
                ////    this.uComboSearchProcess.Focus();
                ////    this.uComboSearchProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                ////}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검사항목 콤보박스 선택시 데이터 조회 이벤트
        private void uComboInspectItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (e.Row == null)
                    return;

                ////// SystemInfo ResourceSet
                ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 규격상/하한, 규격범위 텍스트 박스 설정
                this.uTextUpperSpec.Text = string.Format("{0:f5}", e.Row.Cells["UpperSpec"].Value);
                this.uTextLowerSpec.Text = string.Format("{0:f5}", e.Row.Cells["LowerSpec"].Value);
                this.uTextSpecRangeCode.Text = string.Format("{0:f5}", e.Row.Cells["SpecRangeCode"].Value);

                // 한쪽 규격만 있는경우 규격없는것은 공백처리 -2012.04.23
                if (this.uTextSpecRangeCode.Text.Equals("U"))
                    this.uTextUpperSpec.Text = string.Empty;
                else if (this.uTextSpecRangeCode.Text.Equals("L"))
                    this.uTextLowerSpec.Text = string.Empty;

                this.uTextSpecRange.Text = e.Row.Cells["SpecRangeName"].Value.ToString();
                this.uTextSampleSize.Text = e.Row.Cells["ProcessInspectSS"].Value.ToString();

                ////string strPlantCode = this.uComboSearchPlant.Value.ToString();
                ////string strProductCode = ""; // this.uTextSearchProductCode.Text;
                ////string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                ////string strPackage = this.uComboSearchPackage.Value.ToString();
                ////string strProcessCode = this.uComboSearchProcess.Value.ToString();
                ////string strInspectItemCode = e.Row.Cells["InspectItemCode"].Value.ToString();
                string strStackSeq = e.Row.Cells["StackSeq"].Value.ToString();
                string strGeneration = e.Row.Cells["Generation"].Value.ToString();
                this.uTextSearchStackSeq.Text = strStackSeq;
                uTextSearchGeneration.Text = strGeneration;

                ////string strInspectDateFrom = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).ToString("yyyy-MM-dd");
                ////string strInspectDateTo = Convert.ToDateTime(this.uDateSearchInspectToDate.Value).ToString("yyyy-MM-dd");

                ////// BL 호출
                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                ////QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                ////brwChannel.mfCredentials(clsStatic);

                ////DataTable dtValue = clsStatic.mfReadINSProcessStatic_XBar(strPlantCode
                ////                                                        , strPackage
                ////                                                        , strProcessCode
                ////                                                        , strCustomerCode
                ////                                                        , strStackSeq
                ////                                                        , strGeneration
                ////                                                        , strInspectItemCode
                ////                                                        , strInspectDateFrom
                ////                                                        , strInspectDateTo
                ////                                                        , m_resSys.GetString("SYS_LANG"));

                ////if (dtValue.Rows.Count > 0)
                ////{
                ////    this.uGrid1.DataSource = dtValue;
                ///    this.uGrid1.DataBind();

                ////    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticCL), "ProcessStaticCL");
                ////    QRPSTA.BL.STAPRC.ProcessStaticCL clsStaticCL = new QRPSTA.BL.STAPRC.ProcessStaticCL();
                ////    brwChannel.mfCredentials(clsStaticCL);

                ////    DataTable dtCL = clsStaticCL.mfReadINSProcessStaticCL_XMR(strPlantCode, strProductCode, strProcessCode, strInspectItemCode
                ////                                                    , Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).ToString("yyyy-MM-dd"));

                ////    QRPSTA.STASummary structXBar = new STASummary();
                ////    QRPSTA.STASummary structR = new STASummary();
                ////    QRPSTA.STASPC clsSTAPRC = new STASPC();

                ////    double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                ////    double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);

                ////    if (dtCL.Rows.Count > 0)
                ////    {
                ////        structXBar = clsSTAPRC.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartXBar, dtValue.DefaultView.ToTable(false, "Mean")
                ////                                        , dtValue.DefaultView.ToTable(false, "DataRange"), dblLowerSpec
                ////                                        , dblUpperSpec, e.Row.Cells["SpecRangeCode"].Value.ToString(), true
                ////                                        , ReturnDoubleValue(dtCL.Rows[0]["XBRLCL"].ToString()), ReturnDoubleValue(dtCL.Rows[0]["XBRUCL"].ToString()), true, true, "", ""
                ////                                        , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);

                ////        structR = clsSTAPRC.mfDrawRChart4SubGroupMean(this.uChartR, dtValue.DefaultView.ToTable(false, "Mean"), "", "", "", m_resSys.GetString("SYS_FONTNAME")
                ////                                            , 20, 10, 0);
                ////    }
                ////    else
                ////    {
                ////        structXBar = clsSTAPRC.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartXBar, dtValue.DefaultView.ToTable(false, "Mean")
                ////                                        , dtValue.DefaultView.ToTable(false, "DataRange"), dblLowerSpec
                ////                                        , dblUpperSpec, e.Row.Cells["SpecRangeCode"].Value.ToString(), false
                ////                                        , 0, 0, true, true, "", ""
                ////                                        , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);

                ////        structR = clsSTAPRC.mfDrawMRChart(this.uChartR, dtValue.DefaultView.ToTable(false, "Mean"), "", "", "", m_resSys.GetString("SYS_FONTNAME")
                ////                                            , 20, 10, 0);
                ////    }

                ////    this.uTextMean.Text = string.Format("{0:f5}", structXBar.MeanDA);
                ////    this.uTextStdDev.Text = string.Format("{0:f5}", structXBar.StdDevDA);
                ////    this.uTextMin.Text = string.Format("{0:f5}", structXBar.MinDA);
                ////    this.uTextMax.Text = string.Format("{0:f5}", structXBar.MaxDA);

                ////    this.uTextOverUCL.Text = structXBar.UCLUpperCount.ToString();
                ////    this.uTextOverUCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UCLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextUnderLCL.Text = structXBar.LCLLowerCount.ToString();
                ////    this.uTextUnderLCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LCLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextOOC.Text = structXBar.CLOverCount.ToString();

                ////    this.uTextOverUSL.Text = structXBar.USLUpperCount.ToString();
                ////    this.uTextOverUSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.USLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextUnderLSL.Text = structXBar.LSLLowerCount.ToString();
                ////    this.uTextUnderLSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LSLLowerCount / structXBar.CountDA) * 100).ToString()) + "%";
                ////    this.uTextOOS.Text = structXBar.SLOverCount.ToString();

                ////    this.uTextOverRun.Text = structXBar.UpwardRunCount.ToString();
                ////    this.uTextOverRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextUnderRun.Text = structXBar.DownwardRunCount.ToString();
                ////    this.uTextUnderRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextRun.Text = (structXBar.DownwardRunCount + structXBar.UpwardRunCount).ToString();

                ////    this.uTextOverTrend.Text = structXBar.UpwardTrendCount.ToString();
                ////    this.uTextOverTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextUnderTrend.Text = structXBar.DownwardTrendCount.ToString();
                ////    this.uTextUnderTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                ////    this.uTextTrend.Text = (structXBar.UpwardTrendCount + structXBar.DownwardTrendCount).ToString();

                ////    this.uTextCp.Text = string.Format("{0:f5}", structXBar.CpDA);
                ////    this.uTextCpk.Text = string.Format("{0:f5}", structXBar.CpkDA);
                ////    this.uTextCpl.Text = string.Format("{0:f5}", structXBar.CplDA);
                ////    this.uTextCpu.Text = string.Format("{0:f5}", structXBar.CpuDA);
                ////}
                ////else
                ////{
                ////    WinMessageBox msg = new WinMessageBox();
                ////    DialogResult Result = new DialogResult();

                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                ////        , "검사값이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);

                ////    Clear();

                ////    this.uGrid1.DataSource = dtValue;
                ////    this.uGrid1.DataBind();
                ////}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void frmSTA0063_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uComboSearchPackage_ValueChanged(object sender, EventArgs e)
        {
            if (!(this.uComboSearchProcessGroup.SelectedItem == null) && !(this.uComboSearchCustomer.SelectedItem == null) && !(this.uComboSearchPackage.SelectedItem == null))
            {
                if (!this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) && 
                    !this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) &&
                    !this.uComboSearchPackage.Value.ToString().Equals(string.Empty))
                    InitInspectItemCombo();
            }
        }

        //설비텍스트의 에디트 버튼을 클릭할 경우 설비팝업창이 뜬다.
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();


                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001232", "M001235", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                //frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비텍스트에 코드를 입력후 엔터키를 누를 시 자동조회가 된다.
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //설비코드저장
                string strEquipCode = this.uTextSearchEquipCode.Text;

                if (e.KeyData.Equals(Keys.Enter) && !strEquipCode.Equals(string.Empty))
                {
                    //System ResourcesInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();

                    //공장정보저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    //공장정보가 공백일 경우
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //설비정보BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보조회매서드 실행
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //설비정보가 있는 경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M001118", "M000901", Infragistics.Win.HAlign.Right);
                        if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                            this.uTextSearchEquipName.Clear();
                    }


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비텍스트 정보가 변할시 설비이름이 초기화
        private void uTextSearchEquipCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                {
                    this.uTextSearchEquipName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
