﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0032.cs                                         */
/* 프로그램명   : np(불량수) 관리도 분석                                */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0032 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0032()
        {
            InitializeComponent();
        }

        private void frmSTA0032_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0032_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitEtc();

            this.Resize += new EventHandler(frmSTA0032_Resize);
        }

        private void frmSTA0032_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    this.uGroupBoxGridList.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBoxInfo.Width;
                    this.uGroupBoxChart.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    Point pt = new Point(this.uGroupBoxGridList.Width, this.uGroupBoxInfo.Location.Y);
                    this.uGroupBoxInfo.Location = pt;
                    this.uGroupBoxSearchArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    Point pt = new Point(800, 110);
                    this.uGroupBoxInfo.Location = pt;
                    this.uGroupBoxGridList.Width = 800;
                    this.uGroupBoxChart.Width = 1060;
                    this.uGroupBoxSearchArea.Width = 1060;
                    this.uGroupBoxChart.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBoxGridList.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBoxInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                #region 검색조건 확인

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000262", "M000266", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchInspectToDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000205", "M000052", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (this.uComboSearchCustomer.Value.ToString().Equals(string.Empty) || this.uComboSearchCustomer.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000289", "M001274", Infragistics.Win.HAlign.Center);
                    this.uComboSearchProcessGroup.DropDown();
                    return;
                }
                else if (this.uComboSearchPackage.Value.ToString().Equals(string.Empty) || this.uComboSearchPackage.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001092", "M000100", Infragistics.Win.HAlign.Center);
                    this.uComboSearchPackage.DropDown();
                    return;
                }
                else if (this.uComboSearchProcessGroup.Value.ToString().Equals(string.Empty) || this.uComboSearchProcessGroup.SelectedItem == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000289", "M000290", Infragistics.Win.HAlign.Center);
                    this.uComboSearchProcessGroup.DropDown();
                    return;
                }
                ////else if (this.uComboSearchProcess.Value == null || this.uComboSearchProcess.Value == DBNull.Value || this.uComboSearchProcess.Value.ToString().Equals(string.Empty) ||
                ////    this.uComboSearchProcess.SelectedRow == null)
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                                , "M001264", "M000281", "M000293", Infragistics.Win.HAlign.Center);

                ////    this.uComboSearchProcess.Focus();
                ////    this.uComboSearchProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                ////    return;
                ////}
                else if (this.uComboSearchInspectItem.Value == null || this.uComboSearchInspectItem.Value == DBNull.Value || this.uComboSearchInspectItem.Value.ToString().Equals(string.Empty) ||
                    this.uComboSearchInspectItem.SelectedRow == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000189", "M000193", Infragistics.Win.HAlign.Center);

                    this.uComboSearchInspectItem.Focus();
                    this.uComboSearchInspectItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                    return;
                }

                #endregion

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 검색조건 설정

                // 검색일자
                DateTime dateInspectFromDate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).AddDays(-1);
                DateTime dateInspectToDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);

                // 날짜를 제외한 검색조건 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();
                string strProcessCode = string.Empty;
                if (this.uComboSearchProcess.Value != null && this.uComboSearchProcess.SelectedRow != null)
                    strProcessCode = this.uComboSearchProcess.SelectedRow.Cells["ProcessCode"].Value.ToString();
                string strInspectItemCode = string.Empty;
                string strStackSeq = string.Empty;
                string strGeneration = string.Empty;
                if (this.uComboSearchInspectItem.SelectedRow != null)
                {
                    strInspectItemCode = this.uComboSearchInspectItem.SelectedRow.Cells["InspectItemCode"].Value.ToString();
                    strStackSeq = this.uComboSearchInspectItem.SelectedRow.Cells["StackSeq"].Value.ToString();
                    strGeneration = this.uComboSearchInspectItem.SelectedRow.Cells["Generation"].Value.ToString();
                }
                string strEquipCode = this.uTextSearchEquipCode.Text;

                #endregion

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtInspectCount = clsStatic.mfReadINSProcessStaticD_Count(strPlantCode, strPackage, strProcessCode, strProcessGroupCode, strCustomerCode
                                                                        , strStackSeq, strGeneration, strInspectItemCode, strEquipCode
                                                                        , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                        , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                        , m_resSys.GetString("SYS_LANG"));

                this.uGridList.SetDataBinding(dtInspectCount, string.Empty);

                if (dtInspectCount.Rows.Count > 0)
                {
                    // 검사수량 체크
                    DataTable dtCheckSampleSize = dtInspectCount.DefaultView.ToTable(true, "SampleSize");
                    if (dtCheckSampleSize.Rows.Count > 1)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001104"
                        , "M001480", Infragistics.Win.HAlign.Center);

                        Clear();

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        return;
                    }

                    // 그래프
                    QRPSTA.STASummary stSummary = new STASummary();
                    QRPSTA.STASPC clsSPC = new STASPC();
                    stSummary = clsSPC.mfDrawNPChart(this.uChartPchart, Convert.ToInt32(dtCheckSampleSize.Rows[0]["SampleSize"])
                                    , dtInspectCount.DefaultView.ToTable(false, "FaultQty")
                                    , "nP 관리도", string.Empty, string.Empty
                                    , m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    // 통계값
                    this.uTextLCL.Text = string.Format("{0:f5}", stSummary.LCLDA);
                    this.uTextCL.Text = string.Format("{0:f5}", stSummary.MeanDA);
                    this.uTextUCL.Text = string.Format("{0:f5}", stSummary.UCLDA);
                    this.uTextTOTQty.Text = Convert.ToInt32(dtInspectCount.Compute("SUM(SampleSize)", string.Empty)).ToString();
                    this.uTextTOTFaultQty.Text = Convert.ToInt32(dtInspectCount.Compute("SUM(FaultQty)", string.Empty)).ToString();
                    this.uTextTOTFaultRate.Text = string.Format("{0:f5}", (100.0m * (Convert.ToDecimal(this.uTextTOTFaultQty.Text) / Convert.ToDecimal(this.uTextTOTQty.Text))));

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001104"
                            , "M000171", Infragistics.Win.HAlign.Center);

                    Clear();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGroupBox wGroupBox = new QRPCOM.QRPUI.WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxChart, GroupBoxType.CHART, "P관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxGridList, GroupBoxType.LIST, "조회데이터", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxInfo, GroupBoxType.INFO, "통계정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxChart.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxChart.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxGridList.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxGridList.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검색일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup5, "PackageGroup", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipCode, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelTOTQty, "총검사수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTOTFaultQty, "총불량수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTOTFaultRate, "총불량률(%)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLCL, "LCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCL, "CL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUCL, "UCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCombo = new QRPCOM.QRPUI.WinComboEditor();
                WinComboGrid wComboG = new WinComboGrid();

                // 검사항목 콤보박스
                wComboG.mfInitGeneralComboGrid(this.uComboSearchInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";

                if (strLang.Equals("KOR"))
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위,규격범위,공정코드,공정명";
                else if (strLang.Equals("CHN"))
                    strName = "项目序号,检查项目编号,检查项目名,规格上限,规格下限,规格范围,工程编号,工程名";

                else if (strLang.Equals("ENG"))
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위,규격범위,공정코드,공정명";
                else
                    strName = "항목순번,검사항목코드,검사항목명,규격상한,규격하한,규격범위,규격범위,공정코드,공정명";
                string[] strColumns = strName.Split(',');

                this.uComboSearchInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ItemNum", strColumns[0], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemCode", strColumns[1], false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemName", strColumns[2], false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "StackSeq", "Stack", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "Generation", "Generation", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "UpperSpec", strColumns[3], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "LowerSpec", strColumns[4], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeCode", strColumns[5], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeName", strColumns[5], false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ProcessInspectSS", "SampleSize", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                // 공정콤보
                wComboG.mfInitGeneralComboGrid(this.uComboSearchProcess, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcess.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wComboG.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessCode", strColumns[6], false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchProcess, 0, "ProcessName", strColumns[7], false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                // 공장콤보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), string.Empty, string.Empty
                    , "PlantCode", "PlantName", dtPlant);

                // 고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustomer);

                // PackageGroup5
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();

                DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(m_resSys.GetString("SYS_PLANTCODE"), "5", string.Empty, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackageGroup5, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                // Package 콤보박스
                DataTable dtPackage = clsProduct.mfReadMASProduct_PackageCombo(m_resSys.GetString("SYS_PLANTCODE"), "", "5", "", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageCode", "PackageName", dtPackage);

                //공정그룹 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcessGroup = clsProcess.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ProcessGroup", "ComboName", dtProcessGroup);

                // 공정
                DataTable dtProcess = clsProcess.mfReadMASProcessCombo_ProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), "", m_resSys.GetString("SYS_LANG"));

                this.uComboSearchProcess.DataSource = dtProcess;
                this.uComboSearchProcess.DataBind();

                // 이벤트설정
                this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
                this.uComboSearchCustomer.ValueChanged += new EventHandler(uComboSearchCustomer_ValueChanged);
                this.uComboSearchPackageGroup5.ValueChanged += new EventHandler(uComboSearchPackageGroup5_ValueChanged);
                this.uComboSearchPackage.ValueChanged += new EventHandler(uComboSearchPackage_ValueChanged);
                this.uComboSearchProcessGroup.ValueChanged += new EventHandler(uComboSearchProcessGroup_ValueChanged);
                this.uComboSearchProcess.ValueChanged += new EventHandler(uComboSearchProcess_ValueChanged);
                this.uComboSearchCustomer.AfterEnterEditMode += new EventHandler(uComboSearchCustomer_AfterEnterEditMode);
                this.uComboSearchPackage.AfterEnterEditMode += new EventHandler(uComboSearchPackage_AfterEnterEditMode);
                this.uComboSearchInspectItem.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(uComboSearchInspectItem_RowSelected);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "InspectTime", "검사시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "SampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, "nnn,nnn,nnn,nnn,nnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, "nnn,nnn,nnn,nnn,nnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "FaultRate", "불량비율", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn.nnnnnnnnnn", string.Empty);

                wGrid.mfSetGridColumn(this.uGridList, 0, "PFaultRate", "불량률(%)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn.nnnnnnnnnn", string.Empty);

                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집부가상태로
                this.uGridList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // 타이틀 설정함수 호출
                this.titleArea.mfSetLabelText("nP(불량률) 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

                // 기본값 설정
                this.uDateSearchInspectFromDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();

                setTabIndex(this.Controls);

                this.uDateSearchInspectFromDate.TabIndex = 1;
                this.uDateSearchInspectFromDate.TabStop = true;
                this.uDateSearchInspectToDate.TabIndex = 2;
                this.uDateSearchInspectToDate.TabStop = true;
                this.uComboSearchCustomer.TabIndex = 3;
                this.uComboSearchCustomer.TabStop = true;
                this.uComboSearchPackageGroup5.TabIndex = 4;
                this.uComboSearchPackageGroup5.TabStop = true;
                this.uComboSearchPackage.TabIndex = 5;
                this.uComboSearchPackage.TabStop = true;
                this.uComboSearchProcessGroup.TabIndex = 6;
                this.uComboSearchProcessGroup.TabStop = true;
                this.uComboSearchProcess.TabIndex = 7;
                this.uComboSearchProcess.TabStop = true;
                this.uTextSearchEquipCode.TabIndex = 8;
                this.uTextSearchEquipCode.TabStop = true;
                this.uComboSearchInspectItem.TabIndex = 9;
                this.uComboSearchInspectItem.TabStop = true;

                this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchEquipCode_EditorButtonClick);
                this.uTextSearchEquipCode.KeyDown += new KeyEventHandler(uTextSearchEquipCode_KeyDown);

                Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events...

        // 검사항목 콤보 Row 선택 이벤트
        void uComboSearchInspectItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Package 콤보 Edit 모드시 전체 텍스트 선택하는 이벤트
        void uComboSearchPackage_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchPackage.SelectAll();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객사 콤보 Edit 모드시 전체 텍스트 선택하는 이벤트
        void uComboSearchCustomer_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchCustomer.SelectAll();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정그룹 콤보 값 변경 이벤트
        void uComboSearchProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = string.Empty;
                string strProcessGroup = string.Empty;

                if (this.uComboSearchProcessGroup.SelectedIndex.Equals(-1))
                {
                    return;
                    //this.uComboSearchProcessGroup.Value = strProcessGroup;
                }
                else
                {
                    strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();
                }

                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                else
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                // 빈 데이터테이블 바인딩
                if (this.uComboSearchProcess.DataSource != null)
                    this.uComboSearchProcess.SetDataBinding(((DataTable)this.uComboSearchProcess.DataSource).Clone(), string.Empty);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadMASProcessCombo_ProcessGroup(strPlantCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchProcess.SetDataBinding(dtProcess, string.Empty);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정콤보 값 변경 이벤트
        void uComboSearchProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // PackageGroup 콤보박스 값 변경 이벤트
        void uComboSearchPackageGroup5_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = string.Empty;
                string strCustomerCode = string.Empty;
                string strPackageGroup = string.Empty;

                if (this.uComboSearchPackageGroup5.SelectedIndex.Equals(-1))
                {
                    return;
                    //this.uComboSearchPackageGroup5.Value = strPackageGroup;
                }
                else
                {
                    strPackageGroup = this.uComboSearchPackageGroup5.Value.ToString();
                }

                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                else
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchCustomer.Value = strCustomerCode;
                }
                else
                {
                    strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                }

                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboSearchPackage.Items.Clear();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();

                DataTable dtPackage = clsProduct.mfReadMASProduct_PackageCombo(m_resSys.GetString("SYS_PLANTCODE"), strCustomerCode, "5"
                                                                            , strPackageGroup, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageCode", "PackageName", dtPackage);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Package 콤보 값 변경 이벤트
        void uComboSearchPackage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객사 콤보 값 변경 이벤트
        void uComboSearchCustomer_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = string.Empty;
                string strCustomerCode = string.Empty;

                if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                {
                    return;
                    //this.uComboSearchCustomer.Value = strCustomerCode;
                }
                else
                {
                    strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                }

                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                else
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboSearchPackageGroup5.Items.Clear();

                // PackageGroup 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();

                DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode
                                                                                , "5"
                                                                                , strCustomerCode
                                                                                , m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackageGroup5, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                DataTable dtPackage = clsProduct.mfReadMASProduct_PackageCombo(m_resSys.GetString("SYS_PLANTCODE"), strCustomerCode, "5"
                                                                            , string.Empty, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageCode", "PackageName", dtPackage);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보 값 변경 이벤트
        void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = string.Empty;
                string strCustomerCode = string.Empty;

                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                else
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchCustomer.Value = strCustomerCode;
                }
                else
                {
                    strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                }

                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboSearchPackageGroup5.Items.Clear();
                this.uComboSearchProcessGroup.Items.Clear();

                //PackageGroup5, Package 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();

                DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode
                                                                                , "5"
                                                                                , strCustomerCode
                                                                                , m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackageGroup5, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                //공정그룹 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcessGroup = clsProcess.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ProcessGroup", "ComboName", dtProcessGroup);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //설비코드저장
                string strEquipCode = this.uTextSearchEquipCode.Text;

                if (e.KeyData.Equals(Keys.Enter) && !strEquipCode.Equals(string.Empty))
                {
                    //System ResourcesInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //공장정보저장
                    string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                    if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                    {
                        this.uComboSearchPlant.Value = strPlantCode;
                    }
                    else
                    {
                        strPlantCode = this.uComboSearchPlant.Value.ToString();
                    }

                    //////공장정보가 공백일 경우
                    ////if (strPlantCode.Equals(string.Empty))
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                        "M001264", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    ////    this.uComboSearchPlant.DropDown();
                    ////    return;
                    ////}

                    //설비정보BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    //설비정보조회매서드 실행
                    DataTable dtEquip = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //설비정보가 있는 경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextSearchEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M001118", "M000901", Infragistics.Win.HAlign.Right);
                        if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                            this.uTextSearchEquipName.Clear();
                    }
                }
                else if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    if (this.uTextSearchEquipCode.TextLength <= 1 || this.uTextSearchEquipCode.SelectedText == this.uTextSearchEquipCode.Text)
                    {
                        this.uTextSearchEquipName.Clear();
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                {
                    this.uComboSearchPlant.Value = strPlantCode;
                }
                else
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                }

                ////if (strPlantCode.Equals(string.Empty))
                ////{
                ////    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                        , "M001232", "M001235", "M000266", Infragistics.Win.HAlign.Right);
                ////    this.uComboSearchPlant.DropDown();
                ////    return;
                ////}

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods...

        // Tabindex 설정 함수
        private void setTabIndex(Control.ControlCollection ctls)
        {
            try
            {
                foreach (Control ctl in ctls)
                {
                    if (ctl.GetType().Equals(typeof(Infragistics.Win.UltraWinEditors.UltraTextEditor)))
                    {
                        Infragistics.Win.UltraWinEditors.UltraTextEditor uText = (Infragistics.Win.UltraWinEditors.UltraTextEditor)ctl;
                        if (!uText.Name.Equals("uTextSearchEquipCode"))
                        {
                            uText.Appearance.BackColor = Color.Gainsboro;
                            uText.ReadOnly = true;
                            uText.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                        }
                    }
                    ctl.TabIndex = 0;
                    ctl.TabStop = false;
                    if (ctl.HasChildren)
                        setTabIndex(ctl.Controls);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 초기화 메소드
        private void Clear()
        {
            try
            {
                if (this.uGridList.DataSource != null)
                {
                    DataTable dtEmpty = ((DataTable)this.uGridList.DataSource).Clone();
                    this.uGridList.SetDataBinding(dtEmpty, string.Empty);
                }

                this.uTextLCL.Clear();
                this.uTextCL.Clear();
                this.uTextUCL.Clear();
                this.uTextTOTFaultQty.Clear();
                this.uTextTOTFaultRate.Clear();
                this.uTextTOTQty.Clear();

                ClearChart(this.uChartPchart);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 차트 초기화 메소드
        private void ClearChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        //검사항목 콤보박스 설정
        private void InitInspectItemCombo()
        {
            try
            {
                // 검색조건 체크
                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchPackage.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchProcessGroup.SelectedIndex.Equals(-1))
                    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // SystemInfo ResourceSet
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strProcessGroupCode = this.uComboSearchProcessGroup.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                string strProcessCode = string.Empty;
                if (this.uComboSearchProcess.SelectedRow != null)
                    strProcessCode = this.uComboSearchProcess.SelectedRow.Cells["ProcessCode"].Value.ToString();

                string strDataType = "2";

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStatic = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStatic);

                DataTable dtItem = clsStatic.mfReadINSProcessStatic_InspectItemCombo(strPlantCode, strPackage, strProcessGroupCode, strProcessCode
                                                                                , strCustomerCode, strDataType, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchInspectItem.DataSource = dtItem;
                this.uComboSearchInspectItem.DataBind();

                this.uComboSearchInspectItem.ResetText();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion
    }
}
