﻿namespace QRPSTA.UI
{
    partial class frmSTA0032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0032));
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement2 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxChart = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChartPchart = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uGroupBoxInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTOTFaultRate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTFaultRate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTOTFaultQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTFaultQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTOTQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTQty = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxGridList = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uLabelSearchInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackageGroup5 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup5 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uComboSearchProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChart)).BeginInit();
            this.uGroupBoxChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartPchart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).BeginInit();
            this.uGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGridList)).BeginInit();
            this.uGroupBoxGridList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackageGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxChart
            // 
            this.uGroupBoxChart.Controls.Add(this.uChartPchart);
            this.uGroupBoxChart.Location = new System.Drawing.Point(0, 510);
            this.uGroupBoxChart.Name = "uGroupBoxChart";
            this.uGroupBoxChart.Size = new System.Drawing.Size(1060, 330);
            this.uGroupBoxChart.TabIndex = 252;
            // 
            // uChartPchart
            // 
            this.uChartPchart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement2.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement2.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartPchart.Axis.PE = paintElement2;
            this.uChartPchart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartPchart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.LineThickness = 1;
            this.uChartPchart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.X.Visible = true;
            this.uChartPchart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartPchart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartPchart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.Visible = false;
            this.uChartPchart.Axis.X2.LineThickness = 1;
            this.uChartPchart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.X2.Visible = false;
            this.uChartPchart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartPchart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartPchart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.LineThickness = 1;
            this.uChartPchart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Y.TickmarkInterval = 50;
            this.uChartPchart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Y.Visible = true;
            this.uChartPchart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartPchart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.Visible = false;
            this.uChartPchart.Axis.Y2.LineThickness = 1;
            this.uChartPchart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Y2.TickmarkInterval = 50;
            this.uChartPchart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Y2.Visible = false;
            this.uChartPchart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Z.Labels.ItemFormatString = "";
            this.uChartPchart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.LineThickness = 1;
            this.uChartPchart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Z.Visible = false;
            this.uChartPchart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartPchart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.Visible = false;
            this.uChartPchart.Axis.Z2.LineThickness = 1;
            this.uChartPchart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Z2.Visible = false;
            this.uChartPchart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartPchart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartPchart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartPchart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartPchart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartPchart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uChartPchart.Effects.Effects.Add(gradientEffect2);
            this.uChartPchart.Location = new System.Drawing.Point(3, 0);
            this.uChartPchart.Name = "uChartPchart";
            this.uChartPchart.Size = new System.Drawing.Size(1054, 327);
            this.uChartPchart.TabIndex = 0;
            this.uChartPchart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uGroupBoxInfo
            // 
            this.uGroupBoxInfo.Controls.Add(this.uTextUCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelUCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextLCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelLCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTFaultRate);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTFaultRate);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTFaultQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTFaultQty);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTQty);
            this.uGroupBoxInfo.Location = new System.Drawing.Point(800, 110);
            this.uGroupBoxInfo.Name = "uGroupBoxInfo";
            this.uGroupBoxInfo.Size = new System.Drawing.Size(260, 400);
            this.uGroupBoxInfo.TabIndex = 251;
            // 
            // uTextUCL
            // 
            this.uTextUCL.Location = new System.Drawing.Point(116, 168);
            this.uTextUCL.Name = "uTextUCL";
            this.uTextUCL.Size = new System.Drawing.Size(140, 21);
            this.uTextUCL.TabIndex = 11;
            this.uTextUCL.Text = "ultraTextEditor2";
            // 
            // uLabelUCL
            // 
            this.uLabelUCL.Location = new System.Drawing.Point(12, 168);
            this.uLabelUCL.Name = "uLabelUCL";
            this.uLabelUCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelUCL.TabIndex = 10;
            this.uLabelUCL.Text = "UCL";
            // 
            // uTextLCL
            // 
            this.uTextLCL.Location = new System.Drawing.Point(116, 112);
            this.uTextLCL.Name = "uTextLCL";
            this.uTextLCL.Size = new System.Drawing.Size(140, 21);
            this.uTextLCL.TabIndex = 9;
            this.uTextLCL.Text = "ultraTextEditor2";
            // 
            // uLabelLCL
            // 
            this.uLabelLCL.Location = new System.Drawing.Point(12, 112);
            this.uLabelLCL.Name = "uLabelLCL";
            this.uLabelLCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelLCL.TabIndex = 8;
            this.uLabelLCL.Text = "LCL";
            // 
            // uTextCL
            // 
            this.uTextCL.Location = new System.Drawing.Point(116, 140);
            this.uTextCL.Name = "uTextCL";
            this.uTextCL.Size = new System.Drawing.Size(140, 21);
            this.uTextCL.TabIndex = 7;
            this.uTextCL.Text = "ultraTextEditor2";
            // 
            // uLabelCL
            // 
            this.uLabelCL.Location = new System.Drawing.Point(12, 140);
            this.uLabelCL.Name = "uLabelCL";
            this.uLabelCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelCL.TabIndex = 6;
            this.uLabelCL.Text = "CL";
            // 
            // uTextTOTFaultRate
            // 
            this.uTextTOTFaultRate.Location = new System.Drawing.Point(116, 84);
            this.uTextTOTFaultRate.Name = "uTextTOTFaultRate";
            this.uTextTOTFaultRate.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTFaultRate.TabIndex = 5;
            this.uTextTOTFaultRate.Text = "ultraTextEditor2";
            // 
            // uLabelTOTFaultRate
            // 
            this.uLabelTOTFaultRate.Location = new System.Drawing.Point(12, 84);
            this.uLabelTOTFaultRate.Name = "uLabelTOTFaultRate";
            this.uLabelTOTFaultRate.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTFaultRate.TabIndex = 4;
            this.uLabelTOTFaultRate.Text = "총불량률(%)";
            // 
            // uTextTOTFaultQty
            // 
            this.uTextTOTFaultQty.Location = new System.Drawing.Point(116, 56);
            this.uTextTOTFaultQty.Name = "uTextTOTFaultQty";
            this.uTextTOTFaultQty.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTFaultQty.TabIndex = 3;
            this.uTextTOTFaultQty.Text = "ultraTextEditor2";
            // 
            // uLabelTOTFaultQty
            // 
            this.uLabelTOTFaultQty.Location = new System.Drawing.Point(12, 56);
            this.uLabelTOTFaultQty.Name = "uLabelTOTFaultQty";
            this.uLabelTOTFaultQty.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTFaultQty.TabIndex = 2;
            this.uLabelTOTFaultQty.Text = "총불량수";
            // 
            // uTextTOTQty
            // 
            this.uTextTOTQty.Location = new System.Drawing.Point(116, 28);
            this.uTextTOTQty.Name = "uTextTOTQty";
            this.uTextTOTQty.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTQty.TabIndex = 1;
            this.uTextTOTQty.Text = "ultraTextEditor1";
            // 
            // uLabelTOTQty
            // 
            this.uLabelTOTQty.Location = new System.Drawing.Point(12, 28);
            this.uLabelTOTQty.Name = "uLabelTOTQty";
            this.uLabelTOTQty.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTQty.TabIndex = 0;
            this.uLabelTOTQty.Text = "총검사수";
            // 
            // uGroupBoxGridList
            // 
            this.uGroupBoxGridList.Controls.Add(this.uGridList);
            this.uGroupBoxGridList.Location = new System.Drawing.Point(0, 110);
            this.uGroupBoxGridList.Name = "uGroupBoxGridList";
            this.uGroupBoxGridList.Size = new System.Drawing.Size(800, 400);
            this.uGroupBoxGridList.TabIndex = 250;
            // 
            // uGridList
            // 
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridList.DisplayLayout.Appearance = appearance7;
            this.uGridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridList.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridList.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridList.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridList.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance23.TextHAlignAsString = "Left";
            this.uGridList.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridList.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridList.Location = new System.Drawing.Point(3, 0);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(794, 397);
            this.uGridList.TabIndex = 0;
            this.uGridList.Text = "ultraGrid1";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackageGroup5);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackageGroup5);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1060, 70);
            this.uGroupBoxSearchArea.TabIndex = 253;
            // 
            // uTextSearchEquipName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance16;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(528, 40);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(132, 21);
            this.uTextSearchEquipName.TabIndex = 37;
            // 
            // uTextSearchEquipCode
            // 
            appearance15.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(424, 40);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 36;
            // 
            // uLabelSearchEquipCode
            // 
            this.uLabelSearchEquipCode.Location = new System.Drawing.Point(332, 40);
            this.uLabelSearchEquipCode.Name = "uLabelSearchEquipCode";
            this.uLabelSearchEquipCode.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchEquipCode.TabIndex = 288;
            this.uLabelSearchEquipCode.Text = "설비";
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.CheckedListSettings.CheckStateMember = "";
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchInspectItem.DisplayLayout.Appearance = appearance69;
            this.uComboSearchInspectItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchInspectItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance54;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uComboSearchInspectItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchInspectItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance72.BackColor = System.Drawing.SystemColors.Highlight;
            appearance72.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveRowAppearance = appearance72;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            appearance70.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellAppearance = appearance70;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.SystemColors.Control;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.GroupByRowAppearance = appearance74;
            appearance76.TextHAlignAsString = "Left";
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderAppearance = appearance76;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowAppearance = appearance75;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchInspectItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance73;
            this.uComboSearchInspectItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchInspectItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchInspectItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(768, 40);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(172, 22);
            this.uComboSearchInspectItem.TabIndex = 38;
            this.uComboSearchInspectItem.Text = "ultraCombo1";
            // 
            // uLabelSearchInspectItem
            // 
            this.uLabelSearchInspectItem.Location = new System.Drawing.Point(676, 40);
            this.uLabelSearchInspectItem.Name = "uLabelSearchInspectItem";
            this.uLabelSearchInspectItem.Size = new System.Drawing.Size(88, 20);
            this.uLabelSearchInspectItem.TabIndex = 283;
            this.uLabelSearchInspectItem.Text = "검사항목";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(878, 12);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(174, 21);
            this.uComboSearchPackage.TabIndex = 33;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(796, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(79, 20);
            this.uLabelSearchPackage.TabIndex = 281;
            this.uLabelSearchPackage.Text = "Package";
            // 
            // uComboSearchPackageGroup5
            // 
            this.uComboSearchPackageGroup5.Location = new System.Drawing.Point(644, 12);
            this.uComboSearchPackageGroup5.Name = "uComboSearchPackageGroup5";
            this.uComboSearchPackageGroup5.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackageGroup5.TabIndex = 32;
            this.uComboSearchPackageGroup5.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup5
            // 
            this.uLabelSearchPackageGroup5.Location = new System.Drawing.Point(532, 12);
            this.uLabelSearchPackageGroup5.Name = "uLabelSearchPackageGroup5";
            this.uLabelSearchPackageGroup5.Size = new System.Drawing.Size(108, 20);
            this.uLabelSearchPackageGroup5.TabIndex = 279;
            this.uLabelSearchPackageGroup5.Text = "PackageGroup";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(372, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchCustomer.TabIndex = 31;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(300, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(66, 20);
            this.uLabelSearchCustomer.TabIndex = 30;
            this.uLabelSearchCustomer.Text = "고객";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.CheckedListSettings.CheckStateMember = "";
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcess.DisplayLayout.Appearance = appearance78;
            this.uComboSearchProcess.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcess.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance79.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.Appearance = appearance79;
            appearance80.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.BandLabelAppearance = appearance80;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance81.BackColor2 = System.Drawing.SystemColors.Control;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.PromptAppearance = appearance81;
            this.uComboSearchProcess.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcess.DisplayLayout.MaxRowScrollRegions = 1;
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcess.DisplayLayout.Override.ActiveCellAppearance = appearance82;
            appearance83.BackColor = System.Drawing.SystemColors.Highlight;
            appearance83.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcess.DisplayLayout.Override.ActiveRowAppearance = appearance83;
            this.uComboSearchProcess.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcess.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.Override.CardAreaAppearance = appearance84;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            appearance85.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcess.DisplayLayout.Override.CellAppearance = appearance85;
            this.uComboSearchProcess.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcess.DisplayLayout.Override.CellPadding = 0;
            appearance86.BackColor = System.Drawing.SystemColors.Control;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.Override.GroupByRowAppearance = appearance86;
            appearance87.TextHAlignAsString = "Left";
            this.uComboSearchProcess.DisplayLayout.Override.HeaderAppearance = appearance87;
            this.uComboSearchProcess.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcess.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcess.DisplayLayout.Override.RowAppearance = appearance88;
            this.uComboSearchProcess.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance89.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcess.DisplayLayout.Override.TemplateAddRowAppearance = appearance89;
            this.uComboSearchProcess.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcess.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcess.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcess.Location = new System.Drawing.Point(186, 40);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcess.Size = new System.Drawing.Size(138, 22);
            this.uComboSearchProcess.TabIndex = 35;
            this.uComboSearchProcess.Text = "ultraCombo1";
            // 
            // uComboSearchProcessGroup
            // 
            this.uComboSearchProcessGroup.Location = new System.Drawing.Point(82, 40);
            this.uComboSearchProcessGroup.Name = "uComboSearchProcessGroup";
            this.uComboSearchProcessGroup.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchProcessGroup.TabIndex = 34;
            this.uComboSearchProcessGroup.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(12, 40);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(66, 20);
            this.uLabelSearchProcess.TabIndex = 25;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uDateSearchInspectToDate
            // 
            appearance9.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.Appearance = appearance9;
            this.uDateSearchInspectToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectToDate.Location = new System.Drawing.Point(194, 12);
            this.uDateSearchInspectToDate.Name = "uDateSearchInspectToDate";
            this.uDateSearchInspectToDate.Size = new System.Drawing.Size(98, 21);
            this.uDateSearchInspectToDate.TabIndex = 15;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(180, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchInspectFromDate
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.Appearance = appearance8;
            this.uDateSearchInspectFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectFromDate.Location = new System.Drawing.Point(84, 12);
            this.uDateSearchInspectFromDate.Name = "uDateSearchInspectFromDate";
            this.uDateSearchInspectFromDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchInspectFromDate.TabIndex = 13;
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(68, 20);
            this.uLabelSearchInspectDate.TabIndex = 12;
            this.uLabelSearchInspectDate.Text = "검색기간";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(968, 40);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(44, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(944, 40);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(28, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            this.uLabelSearchPlant.Visible = false;
            // 
            // frmSTA0032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBoxChart);
            this.Controls.Add(this.uGroupBoxInfo);
            this.Controls.Add(this.uGroupBoxGridList);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0032";
            this.Load += new System.EventHandler(this.frmSTA0032_Load);
            this.Activated += new System.EventHandler(this.frmSTA0032_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChart)).EndInit();
            this.uGroupBoxChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChartPchart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).EndInit();
            this.uGroupBoxInfo.ResumeLayout(false);
            this.uGroupBoxInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGridList)).EndInit();
            this.uGroupBoxGridList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackageGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxChart;
        private Infragistics.Win.UltraWinChart.UltraChart uChartPchart;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUCL;
        private Infragistics.Win.Misc.UltraLabel uLabelUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelLCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCL;
        private Infragistics.Win.Misc.UltraLabel uLabelCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTFaultRate;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTFaultRate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTFaultQty;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTFaultQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTQty;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTQty;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxGridList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchInspectItem;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackageGroup5;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}