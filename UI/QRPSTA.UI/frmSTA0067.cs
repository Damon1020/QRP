﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0067.cs                                         */
/* 프로그램명   : 공정검사현황                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-12-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0067 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0067()
        {
            InitializeComponent();
        }

        #region IToolbar 멤버
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridProcInspectList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridProcInspectList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DateTime dtFrom = Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value.ToString());
                DateTime dtTo = Convert.ToDateTime(this.uDateSearchInspectDateTo.Value.ToString());
                //if(dtFrom.AddMonths(1) < dtTo)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //        , "M001135", "M001115", "M001242", Infragistics.Win.HAlign.Right);

                //    return;
                //}

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;
                
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsProcReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsProcReport);

                // 공정코드
                string strProcessCode = string.Empty;
                if (!(this.uComboSearchProcessCode.SelectedRow == null))
                    strProcessCode = this.uComboSearchProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();

                // 메소드 호출
                DataTable dtProcInspect = clsProcReport.mfReadINSProcInspect_frmSTA0067_PSTS(m_resSys.GetString("SYS_PLANTCODE")
                                                                                    , Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value).ToString("yyyy-MM-dd")
                                                                                    , Convert.ToDateTime(this.uDateSearchInspectDateTo.Value).ToString("yyyy-MM-dd")
                                                                                    , this.uComboSearchCustomer.Value.ToString()
                                                                                    , this.uComboSearchProductActionType.Value.ToString()
                                                                                    , this.uComoSearchPackageGroup1.Value.ToString()
                                                                                    , this.uComoSearchPackageGroup2.Value.ToString()
                                                                                    , this.uComoSearchPackageGroup3.Value.ToString()
                                                                                    , this.uComboSearchPackage.Value.ToString()
                                                                                    , this.uComboSearchProcInspectType.Value.ToString()
                                                                                    , this.uComboSearchInspectResult.Value.ToString()
                                                                                    , this.uComboSearchDetailProcessOperationType.Value.ToString()
                                                                                    , strProcessCode
                                                                                    , this.uTextSearchCustomerProductSpec.Text
                                                                                    , this.uTextSearchLotNo.Text
                                                                                    , this.uComboSearchInspectType.Value.ToString()
                                                                                    , this.uTextSearchEquipCode.Text.ToString()
                                                                                    , this.uComboSearchInspectItem.Value.ToString()
                                                                                    , m_resSys.GetString("SYS_LANG"));

                this.uGridProcInspectList.DataSource = dtProcInspect;
                this.uGridProcInspectList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtProcInspect.Rows.Count == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {

                    for (int i = 0; i < this.uGridProcInspectList.Rows.Count; i++)
                    {
                        //Sub합계 색깔 적용
                        if (this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectDate"].Value.ToString() != "ZZZTotal")
                        {
                            //공정Type Sub합계 색깔적용
                            if (this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectTime"].Value.ToString() == "99:99:99" &&
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString() != "ZZZTotal")
                            {
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectTime"].Value = "";
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value = "Total";
                                for (int j = this.uGridProcInspectList.DisplayLayout.Bands[0].Columns["DETAILPROCESSOPERATIONTYPE"].Index; j < this.uGridProcInspectList.DisplayLayout.Bands[0].Columns.Count; j++)
                                {
                                    this.uGridProcInspectList.Rows[i].Cells[j].Appearance.BackColor = Color.LemonChiffon;
                                    this.uGridProcInspectList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                }
                            }
                            //일자 Sub합계 색깔적용
                            else if (this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectTime"].Value.ToString() == "99:99:99" &&
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString() == "ZZZTotal")
                            {
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectTime"].Value = "Total";
                                this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value = "";
                                for (int j = this.uGridProcInspectList.DisplayLayout.Bands[0].Columns["InspectTime"].Index; j < this.uGridProcInspectList.DisplayLayout.Bands[0].Columns.Count; j++)
                                {
                                    this.uGridProcInspectList.Rows[i].Cells[j].Appearance.BackColor = Color.PeachPuff;
                                    this.uGridProcInspectList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                }
                            }
                        }
                        //총합계 색깔 적용
                        else
                        {
                            this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectDate"].Value = "Grand Total";
                            this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["InspectTime"].Value = "";
                            this.uGridProcInspectList.DisplayLayout.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value = "";
                            for (int j = 0; j < this.uGridProcInspectList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridProcInspectList.Rows[i].Cells[j].Appearance.BackColor = Color.PeachPuff;
                                this.uGridProcInspectList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                    }

                    //맨앞에 선택 컬럼 제거
                    uGridProcInspectList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridProcInspectList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 폼이벤트
        private void frmSTA0067_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            titleArea.mfSetLabelText("공정검사현황", m_SysRes.GetString("SYS_FONTNAME"), 12);
            SetToolAuth();
            InitEtc();
            InitLabel();
            InitGrid();
            InitComboBox();
        }

        private void frmSTA0067_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0067_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 초기화 메소드

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화

        private void InitEtc()
        {
            try
            {
                this.uTextSearchCustomerProductSpec.MaxLength = 40;
                this.uTextSearchLotNo.MaxLength = 50;

                this.uDateSearchInspectDateFrom.Value = DateTime.Now.ToString("yyyy-MM-dd");//DateTime.Now.ToString("yyyy-MM-dd").Substring(0,8) + "01"; // 검색기간 한달-> 하루로 변경
                this.uDateSearchInspectDateTo.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uDateSearchInspectDateFrom.Appearance.BackColor = Color.Empty;
                this.uDateSearchInspectDateTo.Appearance.BackColor = Color.Empty;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Label 초기화

        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectResult, "판정구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectType, "검사유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcInspectType, "공정검사구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup1, "패키지그룹_1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup2, "패키지그룹_2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup3, "패키지그룹_3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomerProductSpec, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSearchEquipCode, "设备", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ultraLabel1, "检查项目", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 콤보박스 초기화 
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                // 고객사

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                // 필요한 컬럼만 추출
                dtCustomer = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "CustomerCode", "CustomerName", dtCustomer);


                // 제품구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(m_resSys.GetString("SYS_PLANTCODE"), "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "", "ActionTypeCode", "ActionTypeName", dtProductActionType);


                // 제품그룹(PackageGroup1, PackageGroup2, PackageGroup3)
                DataTable dtPackageGroup1 = clsProduct.mfReadMASProduct_PackageGroup(m_resSys.GetString("SYS_PLANTCODE"), "2", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup2 = clsProduct.mfReadMASProduct_PackageGroup(m_resSys.GetString("SYS_PLANTCODE"), "3", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup3 = clsProduct.mfReadMASProduct_PackageGroup(m_resSys.GetString("SYS_PLANTCODE"), "4", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComoSearchPackageGroup1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "PackageGroupCode", "PackageGroupName", dtPackageGroup1);

                wCombo.mfSetComboEditor(this.uComoSearchPackageGroup2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "PackageGroupCode", "PackageGroupName", dtPackageGroup2);

                wCombo.mfSetComboEditor(this.uComoSearchPackageGroup3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "PackageGroupCode", "PackageGroupName", dtPackageGroup3);

                // Package
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "Package", "ComboName", dtPackage);


                // 공정검사구분

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtProcInspectType = clsComCode.mfReadCommonCode("C0020", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "", "ComCode", "ComCodeName", dtProcInspectType);


                // 판정구분
                DataTable dtInspectResult = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchInspectResult, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "", "ComCode", "ComCodeName", dtInspectResult);

                WinComboGrid wComboG = new WinComboGrid();
                // 공정콤보
                wComboG.mfInitGeneralComboGrid(this.uComboSearchProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                string strLang = m_resSys.GetString("SYS_LANG");
                string strProCode = "";
                string strProName = "";
                if(strLang.Equals("KOR"))
                {
                    strProCode = "공정코드";
                    strProName = "공정명";
                }
                else if (strLang.Equals("CHN"))
                {
                    strProCode = "工程编号";
                    strProName = "工程名";
                }
                else if (strLang.Equals("ENG"))
                {
                    strProCode = "공정코드";
                    strProName = "공정명";
                }
                this.uComboSearchProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wComboG.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessCode", strProCode, false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessName", strProName, false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                // 공정 Type
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcessDetailOperationType = clsProcess.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));

                wCombo.mfSetComboEditor(this.uComboSearchDetailProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "DetailProcessOperationType", "ComboName", dtProcessDetailOperationType);


                // 검사유형

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsInspectType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsInspectType);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem InspectItem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(InspectItem);

                //DataTable dtInspectType = clsInspectType.mfReadMASInspectTypeForCombo(m_resSys.GetString("SYS_PLANTCODE"), "", m_resSys.GetString("SYS_LANG"));
                DataTable dtInspectType = clsInspectType.mfReadMASInspectTypeForCombo_STA(m_resSys.GetString("SYS_PLANTCODE"), "", m_resSys.GetString("SYS_LANG"));

                DataTable dtInspectItem = InspectItem.mfReadMASInspectItemComboSTA(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "InspectTypeCode", "InspectTypeName", dtInspectType);

                wCombo.mfSetComboEditor(this.uComboSearchInspectItem, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "", "InspectItemCode", "InspectItemName", dtInspectItem);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // Grid 초기화

        private void InitGrid()
        {
            try
            {
                #region 그리드 일반설정 / 컬럼설정
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcInspectList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                                        , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectDate", "검사일", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectTime", "검사시간", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "ProcInspectType", "공정검사구분", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                // 공정코드추가 : 2012.01.02 Yoon
                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "WorkProcessCode", "공정코드", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "WorkProcessName", "공정명", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "PRODUCTACTIONTYPE", "제품구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "CUSTOMERPRODUCTSPEC", "고객제품코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 설비코드추가 : 2012.01.02 Yoon
                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 설비명 주석처리 : 2012.01.02 Yoon
                //wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                //                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "InspectResultFlag", "판정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.IntegerNonNegative, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "ProcessSampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.IntegerNonNegative, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "FaultName1", "불량명1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "FaultName2", "불량명2", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "FaultName3", "불량명3", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "CauseEquipCode1", "원인설비1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "CauseEquipCode2", "원인설비2", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "CauseEquipCode3", "원인설비3", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectList, 0, "QCNFlag", "QCN적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 그리드수정불가상태
                this.uGridProcInspectList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // MaskInput 설정
                this.uGridProcInspectList.DisplayLayout.Bands[0].Columns["ProcessSampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridProcInspectList.DisplayLayout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트

        // 공정 Type 에 따라 공정 콤보박스 설정하기
        private void uComboSearchDetailProcessOperationType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtProcess = new DataTable();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strProcessGroup = this.uComboSearchDetailProcessOperationType.Value.ToString();

                if (!strPlantCode.Equals(string.Empty) && !strProcessGroup.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadMASProcessCombo_ProcessGroup(strPlantCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));
                }

                this.uComboSearchProcessCode.DataSource = dtProcess;
                this.uComboSearchProcessCode.DataBind();

                this.uComboSearchProcessCode.Value = null;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                //frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = "2210";
                frmEquip.ShowDialog();

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
