﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0070.cs                                         */
/* 프로그램명   : QCN현황                                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-12-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0070 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0070()
        {
            InitializeComponent();
        }

        private void frmSTA0070_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0070_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("QCN현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();
        }

        private void frmSTA0070_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드


        // Label 초기화

        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();
                
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 콤보박스 초기화 
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                //검색조건 - 년
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;

                //검색조건 - 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();
                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2") + "월");
                }
                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

                //검색조건 - 고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer"); // 2
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer(); // 3
                brwChannel.mfCredentials(clsCustomer); // 4
                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "CustomerCode", "CustomerName", dtCustomer);

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);

                //검색조건 - 공정Type 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process"); // 2
                QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process(); // 3
                brwChannel.mfCredentials(clsProc); // 4
                DataTable dtProc = clsProc.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));
                wCombo.mfSetComboEditor(this.uComboSearchDetailProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "DetailProcessOperationType", "ComboName", dtProc);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화
        private void InitEtc()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Grid 초기화
        private void InitGrid()
        {
            try
            {
                this.uGridQCNList.DisplayLayout.GroupByBox.Hidden = true;

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //메뉴별 권한정보 그리드 기본설정
                wGrid.mfInitGeneralGrid(this.uGridQCNList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //맨앞에 선택 컬럼 제거
                uGridQCNList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

                // 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "ProductActionType", "제품구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "CustomerCode", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D11", "11일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D12", "12일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D13", "13일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D14", "14일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D15", "15일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D16", "16일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D17", "17일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D18", "18일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D19", "19일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D20", "20일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D21", "21일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D22", "22일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D23", "23일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D24", "24일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D25", "25일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D26", "26일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D27", "27일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D28", "28일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D29", "29일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D30", "30일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "D31", "31일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridQCNList, 0, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "");

                //고정열 Bold적용
                this.uGridQCNList.DisplayLayout.Bands[0].Columns["DETAILPROCESSOPERATIONTYPE"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridQCNList.DisplayLayout.Bands[0].Columns["ProductActionType"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridQCNList.DisplayLayout.Bands[0].Columns["CustomerCode"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridQCNList.DisplayLayout.Bands[0].Columns["Gubun"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;


                //합격률 행 색깔적용
                for (int i = 0; i < this.uGridQCNList.Rows.Count; i++)
                {
                    if (this.uGridQCNList.Rows[i].Cells["Gubun"].Value.ToString() == "합격률")
                    {
                        for (int j = 0; j < this.uGridQCNList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridQCNList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridQCNList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                }

                //Total열 색깔적용
                this.uGridQCNList.DisplayLayout.Bands[0].Columns[this.uGridQCNList.DisplayLayout.Bands[0].Columns.Count - 1].CellAppearance.BackColor = Color.MistyRose;
                this.uGridQCNList.DisplayLayout.Bands[0].Columns[this.uGridQCNList.DisplayLayout.Bands[0].Columns.Count - 1].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //일자 열에 대한 표시//
                this.uGridQCNList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridQCNList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();            	
            }
            finally
            {
            }
        }

        #endregion

        private void uComboSearchDetailProcessOperationType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strDetailProcType = this.uComboSearchDetailProcessOperationType.Value.ToString();

                //검색조건 - 공정코드
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsAriseProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsAriseProcess);

                DataTable dtProcess = clsAriseProcess.mfReadProcessWithDetailProcessOperationType(strPlantCode, strDetailProcType, m_resSys.GetString("SYS_LANG"));
                WinComboGrid combo = new WinComboGrid();
                combo.mfInitGeneralComboGrid(this.uComboSearchProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboSearchProcessCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessCode", "공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessName", "공정명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                this.uComboSearchProcessCode.DataSource = dtProcess;
                this.uComboSearchProcessCode.DataBind();

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region IToolbar
        public void mfCreate()
        {

        }

        public void mfDelete()
        {

        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridQCNList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridQCNList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "엑셀다운로드", "그리드 엑셀 다운로드", "엑셀로 다운로드 받을 내용이 없습니다.", Infragistics.Win.HAlign.Center);
                }
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfSave()
        {

        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uTextSearchYear.Text.Length < 4)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "입력사항 확인", "조회년도를 4자리로 입력해 주세요", Infragistics.Win.HAlign.Center);
                    
                    this.uTextSearchYear.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(this.uComboSearchMonth.Value.ToString()))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "입력사항 확인", "달을 선택해 주세요.", Infragistics.Win.HAlign.Center);
                    
                    this.uComboSearchMonth.DropDown();
                    return;
                }

                // 매개변수 설정
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                //string strFromInspectDate = Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value).ToString("yyyy-MM-dd");
                //string strToInspectDate = Convert.ToDateTime(this.uDateSearchInspectDateTo.Value).ToString("yyyy-MM-dd");
                string strYearMonth = this.uTextSearchYear.Text + "-" + this.uComboSearchMonth.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strDProcOperationType = this.uComboSearchDetailProcessOperationType.Value.ToString();
                string strProcessCode = "";
                if (this.uComboSearchProcessCode.SelectedRow != null)
                    strProcessCode = this.uComboSearchProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsReport.mfReadINSProcInspect_frmSTA0070(strPlantCode, strYearMonth, strCustomerCode, strProductActionType,
                                                                               strDProcOperationType, strProcessCode);
                
                //일자별로 동적으로 컬럼이 늘어나도록 Binding하기 위해 그리드를 초기화 함.
                this.uGridQCNList.DataSource = null;
                this.uGridQCNList.ResetDisplayLayout();
                this.uGridQCNList.Layouts.Clear();

                //this.uGridQCNList.DataSource = dtSearch;
                //this.uGridQCNList.DataBind();
                
                #region Grid에 뿌려줄 DataTable 형태로 조정
                System.Data.DataTable dtQCNGrid = new System.Data.DataTable("QCN");
                DataColumn[] dcPKQCNGrid = new DataColumn[4];
                dtQCNGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtQCNGrid.Columns.Add("ProductActionType", typeof(string));
                dtQCNGrid.Columns.Add("CustomerCode", typeof(string));
                dtQCNGrid.Columns.Add("Gubun", typeof(string));
                dtQCNGrid.Columns.Add("D01", typeof(double));
                dtQCNGrid.Columns.Add("D02", typeof(double));
                dtQCNGrid.Columns.Add("D03", typeof(double));
                dtQCNGrid.Columns.Add("D04", typeof(double));
                dtQCNGrid.Columns.Add("D05", typeof(double));
                dtQCNGrid.Columns.Add("D06", typeof(double));
                dtQCNGrid.Columns.Add("D07", typeof(double));
                dtQCNGrid.Columns.Add("D08", typeof(double));
                dtQCNGrid.Columns.Add("D09", typeof(double));
                dtQCNGrid.Columns.Add("D10", typeof(double));
                dtQCNGrid.Columns.Add("D11", typeof(double));
                dtQCNGrid.Columns.Add("D12", typeof(double));
                dtQCNGrid.Columns.Add("D13", typeof(double));
                dtQCNGrid.Columns.Add("D14", typeof(double));
                dtQCNGrid.Columns.Add("D15", typeof(double));
                dtQCNGrid.Columns.Add("D16", typeof(double));
                dtQCNGrid.Columns.Add("D17", typeof(double));
                dtQCNGrid.Columns.Add("D18", typeof(double));
                dtQCNGrid.Columns.Add("D19", typeof(double));
                dtQCNGrid.Columns.Add("D20", typeof(double));
                dtQCNGrid.Columns.Add("D21", typeof(double));
                dtQCNGrid.Columns.Add("D22", typeof(double));
                dtQCNGrid.Columns.Add("D23", typeof(double));
                dtQCNGrid.Columns.Add("D24", typeof(double));
                dtQCNGrid.Columns.Add("D25", typeof(double));
                dtQCNGrid.Columns.Add("D26", typeof(double));
                dtQCNGrid.Columns.Add("D27", typeof(double));
                dtQCNGrid.Columns.Add("D28", typeof(double));
                dtQCNGrid.Columns.Add("D29", typeof(double));
                dtQCNGrid.Columns.Add("D30", typeof(double));
                dtQCNGrid.Columns.Add("D31", typeof(double));
                dtQCNGrid.Columns.Add("Total", typeof(double));

                dcPKQCNGrid[0] = dtQCNGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcPKQCNGrid[1] = dtQCNGrid.Columns["ProductActionType"];
                dcPKQCNGrid[2] = dtQCNGrid.Columns["CustomerCode"];
                dcPKQCNGrid[3] = dtQCNGrid.Columns["Gubun"];
                dtQCNGrid.PrimaryKey = dcPKQCNGrid;

                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    DataRow row = dtQCNGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtSearch.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["ProductActionType"] = dtSearch.Rows[i]["ProductActionType"];
                    row["CustomerCode"] = dtSearch.Rows[i]["CustomerCode"];
                    row["Gubun"] = "검사건수";
                    row["D01"] = dtSearch.Rows[i]["D01InspectCount"];
                    row["D02"] = dtSearch.Rows[i]["D02InspectCount"];
                    row["D03"] = dtSearch.Rows[i]["D03InspectCount"];
                    row["D04"] = dtSearch.Rows[i]["D04InspectCount"];
                    row["D05"] = dtSearch.Rows[i]["D05InspectCount"];
                    row["D06"] = dtSearch.Rows[i]["D06InspectCount"];
                    row["D07"] = dtSearch.Rows[i]["D07InspectCount"];
                    row["D08"] = dtSearch.Rows[i]["D08InspectCount"];
                    row["D09"] = dtSearch.Rows[i]["D09InspectCount"];
                    row["D10"] = dtSearch.Rows[i]["D10InspectCount"];
                    row["D11"] = dtSearch.Rows[i]["D11InspectCount"];
                    row["D12"] = dtSearch.Rows[i]["D12InspectCount"];
                    row["D13"] = dtSearch.Rows[i]["D13InspectCount"];
                    row["D14"] = dtSearch.Rows[i]["D14InspectCount"];
                    row["D15"] = dtSearch.Rows[i]["D15InspectCount"];
                    row["D16"] = dtSearch.Rows[i]["D16InspectCount"];
                    row["D17"] = dtSearch.Rows[i]["D17InspectCount"];
                    row["D18"] = dtSearch.Rows[i]["D18InspectCount"];
                    row["D19"] = dtSearch.Rows[i]["D19InspectCount"];
                    row["D20"] = dtSearch.Rows[i]["D20InspectCount"];
                    row["D21"] = dtSearch.Rows[i]["D21InspectCount"];
                    row["D22"] = dtSearch.Rows[i]["D22InspectCount"];
                    row["D23"] = dtSearch.Rows[i]["D23InspectCount"];
                    row["D24"] = dtSearch.Rows[i]["D24InspectCount"];
                    row["D25"] = dtSearch.Rows[i]["D25InspectCount"];
                    row["D26"] = dtSearch.Rows[i]["D26InspectCount"];
                    row["D27"] = dtSearch.Rows[i]["D27InspectCount"];
                    row["D28"] = dtSearch.Rows[i]["D28InspectCount"];
                    row["D29"] = dtSearch.Rows[i]["D29InspectCount"];
                    row["D30"] = dtSearch.Rows[i]["D30InspectCount"];
                    row["D31"] = dtSearch.Rows[i]["D31InspectCount"];
                    row["Total"] = dtSearch.Rows[i]["TotalInspectCount"];
                    dtQCNGrid.Rows.Add(row);

                    row = dtQCNGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtSearch.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["ProductActionType"] = dtSearch.Rows[i]["ProductActionType"];
                    row["CustomerCode"] = dtSearch.Rows[i]["CustomerCode"];
                    row["Gubun"] = "QCN건수";
                    row["D01"] = dtSearch.Rows[i]["D01QCNCount"];
                    row["D02"] = dtSearch.Rows[i]["D02QCNCount"];
                    row["D03"] = dtSearch.Rows[i]["D03QCNCount"];
                    row["D04"] = dtSearch.Rows[i]["D04QCNCount"];
                    row["D05"] = dtSearch.Rows[i]["D05QCNCount"];
                    row["D06"] = dtSearch.Rows[i]["D06QCNCount"];
                    row["D07"] = dtSearch.Rows[i]["D07QCNCount"];
                    row["D08"] = dtSearch.Rows[i]["D08QCNCount"];
                    row["D09"] = dtSearch.Rows[i]["D09QCNCount"];
                    row["D10"] = dtSearch.Rows[i]["D10QCNCount"];
                    row["D11"] = dtSearch.Rows[i]["D11QCNCount"];
                    row["D12"] = dtSearch.Rows[i]["D12QCNCount"];
                    row["D13"] = dtSearch.Rows[i]["D13QCNCount"];
                    row["D14"] = dtSearch.Rows[i]["D14QCNCount"];
                    row["D15"] = dtSearch.Rows[i]["D15QCNCount"];
                    row["D16"] = dtSearch.Rows[i]["D16QCNCount"];
                    row["D17"] = dtSearch.Rows[i]["D17QCNCount"];
                    row["D18"] = dtSearch.Rows[i]["D18QCNCount"];
                    row["D19"] = dtSearch.Rows[i]["D19QCNCount"];
                    row["D20"] = dtSearch.Rows[i]["D20QCNCount"];
                    row["D21"] = dtSearch.Rows[i]["D21QCNCount"];
                    row["D22"] = dtSearch.Rows[i]["D22QCNCount"];
                    row["D23"] = dtSearch.Rows[i]["D23QCNCount"];
                    row["D24"] = dtSearch.Rows[i]["D24QCNCount"];
                    row["D25"] = dtSearch.Rows[i]["D25QCNCount"];
                    row["D26"] = dtSearch.Rows[i]["D26QCNCount"];
                    row["D27"] = dtSearch.Rows[i]["D27QCNCount"];
                    row["D28"] = dtSearch.Rows[i]["D28QCNCount"];
                    row["D29"] = dtSearch.Rows[i]["D29QCNCount"];
                    row["D30"] = dtSearch.Rows[i]["D30QCNCount"];
                    row["D31"] = dtSearch.Rows[i]["D31QCNCount"];
                    row["Total"] = dtSearch.Rows[i]["TotalQCNCount"];
                    dtQCNGrid.Rows.Add(row);

                    row = dtQCNGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtSearch.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["ProductActionType"] = dtSearch.Rows[i]["ProductActionType"];
                    row["CustomerCode"] = dtSearch.Rows[i]["CustomerCode"];
                    row["Gubun"] = "합격률";
                    row["D01"] = dtSearch.Rows[i]["D01Rate"];
                    row["D02"] = dtSearch.Rows[i]["D02Rate"];
                    row["D03"] = dtSearch.Rows[i]["D03Rate"];
                    row["D04"] = dtSearch.Rows[i]["D04Rate"];
                    row["D05"] = dtSearch.Rows[i]["D05Rate"];
                    row["D06"] = dtSearch.Rows[i]["D06Rate"];
                    row["D07"] = dtSearch.Rows[i]["D07Rate"];
                    row["D08"] = dtSearch.Rows[i]["D08Rate"];
                    row["D09"] = dtSearch.Rows[i]["D09Rate"];
                    row["D10"] = dtSearch.Rows[i]["D10Rate"];
                    row["D11"] = dtSearch.Rows[i]["D11Rate"];
                    row["D12"] = dtSearch.Rows[i]["D12Rate"];
                    row["D13"] = dtSearch.Rows[i]["D13Rate"];
                    row["D14"] = dtSearch.Rows[i]["D14Rate"];
                    row["D15"] = dtSearch.Rows[i]["D15Rate"];
                    row["D16"] = dtSearch.Rows[i]["D16Rate"];
                    row["D17"] = dtSearch.Rows[i]["D17Rate"];
                    row["D18"] = dtSearch.Rows[i]["D18Rate"];
                    row["D19"] = dtSearch.Rows[i]["D19Rate"];
                    row["D20"] = dtSearch.Rows[i]["D20Rate"];
                    row["D21"] = dtSearch.Rows[i]["D21Rate"];
                    row["D22"] = dtSearch.Rows[i]["D22Rate"];
                    row["D23"] = dtSearch.Rows[i]["D23Rate"];
                    row["D24"] = dtSearch.Rows[i]["D24Rate"];
                    row["D25"] = dtSearch.Rows[i]["D25Rate"];
                    row["D26"] = dtSearch.Rows[i]["D26Rate"];
                    row["D27"] = dtSearch.Rows[i]["D27Rate"];
                    row["D28"] = dtSearch.Rows[i]["D28Rate"];
                    row["D29"] = dtSearch.Rows[i]["D29Rate"];
                    row["D30"] = dtSearch.Rows[i]["D30Rate"];
                    row["D31"] = dtSearch.Rows[i]["D31Rate"];
                    row["Total"] = dtSearch.Rows[i]["TotalRate"];
                    dtQCNGrid.Rows.Add(row);
                }

                this.uGridQCNList.DataSource = dtQCNGrid;
                this.uGridQCNList.DataBind();

                #endregion

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (uGridQCNList.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                else
                {
                    //DataTable을 바인딩하고 그리드 내용을 수정을하면 DataTable내용도 수정이되어 챠트적용하고 그리드속성을 변경함.
                    ChangeGridColumn(0);
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridQCNList, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

    }
}
