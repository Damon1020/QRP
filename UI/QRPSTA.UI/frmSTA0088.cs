﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0088.cs                                         */
/* 프로그램명   : CCS 품질목표 등록/조회                                */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-18                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0088 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0088()
        {
            InitializeComponent();
        }

        #region Form Event

        private void frmSTA0088_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0088_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0088_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("CCS 품질목표 등록/조회", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 사용자-화면툴바 권한 설정
            SetToolAuth();

            // 초기화 메소드 호출
            InitLabel();
            InitComboBox();
            InitEtc();
            InitGrid();

            WinGrid wGrid = new WinGrid();
            wGrid.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcTarget), "ProcTarget");
                QRPSTA.BL.STAPRC.ProcTarget clsPTar = new QRPSTA.BL.STAPRC.ProcTarget();
                brwChannel.mfCredentials(clsPTar);

                DataTable dtCCSTarList = clsPTar.mfSetDataInfo();
                DataRow drRow;

                this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstRowInGrid);

                // 필수사항 입력확인 및 삭제용 데이터 테이블 생성
                for (int i = 0; i < this.uGridCCSTargetList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridCCSTargetList.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        ////if (this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value == null ||
                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value == DBNull.Value ||
                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 공장을 선택해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Activate();
                        ////    this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        ////    return;
                        ////}
                        ////else if (this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value == null ||
                        ////        this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value == DBNull.Value ||
                        ////        this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 년도를 입력해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridCCSTargetList.Rows[i].Cells["AYear"].Activate();
                        ////    this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}

                        if (this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value == null ||
                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value == DBNull.Value ||
                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000545"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value == null ||
                                this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value == DBNull.Value ||
                                this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000544"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value == null ||
                                this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value == DBNull.Value ||
                                this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000550"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            drRow = dtCCSTarList.NewRow();
                            drRow["PlantCode"] = this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["AYear"] = this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value.ToString();
                            drRow["AQuarter"] = this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value.ToString();
                            drRow["DetailProcessOperationType"] = this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value.ToString();
                            drRow["ProductionType"] = this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value.ToString();
                            dtCCSTarList.Rows.Add(drRow);
                        }
                    }
                }

                if (dtCCSTarList.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsPTar.mfDeleteINSCCSTarget(dtCCSTarList);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000926",
                                                Infragistics.Win.HAlign.Right);

                            // List Refresh
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000923",
                                                Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000622"
                                                    , "M000644"
                                                    , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcTarget), "ProcTarget");
                QRPSTA.BL.STAPRC.ProcTarget clsPTar = new QRPSTA.BL.STAPRC.ProcTarget();
                brwChannel.mfCredentials(clsPTar);

                DataTable dtCCSTarList = clsPTar.mfSetDataInfo();
                DataRow drRow;

                this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstRowInGrid);

                // 필수사항 입력확인 및 저장용 데이터 테이블 생성
                for (int i = 0; i < this.uGridCCSTargetList.Rows.Count; i++)
                {
                    if (this.uGridCCSTargetList.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        ////if (this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value == null ||
                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value == DBNull.Value ||
                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 공장을 선택해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Activate();
                        ////    this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        ////    return;
                        ////}
                        ////else if (this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value == null ||
                        ////        this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value == DBNull.Value ||
                        ////        this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 년도를 입력해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridCCSTargetList.Rows[i].Cells["AYear"].Activate();
                        ////    this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}

                        if (this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value == null ||
                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value == DBNull.Value ||
                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000545"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value == null ||
                                this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value == DBNull.Value ||
                                this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000544"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value == null ||
                                this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value == DBNull.Value ||
                                this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001223"
                                                    , (i + 1).ToString() + "M000550"
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Activate();
                            this.uGridCCSTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            drRow = dtCCSTarList.NewRow();
                            drRow["PlantCode"] = this.uGridCCSTargetList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["AYear"] = this.uGridCCSTargetList.Rows[i].Cells["AYear"].Value.ToString();
                            drRow["AQuarter"] = this.uGridCCSTargetList.Rows[i].Cells["AQuarter"].Value.ToString();
                            drRow["DetailProcessOperationType"] = this.uGridCCSTargetList.Rows[i].Cells["DetailProcessOperationType"].Value.ToString();
                            drRow["ProductionType"] = this.uGridCCSTargetList.Rows[i].Cells["ProductionType"].Value.ToString();
                            drRow["Target"] = ReturnDecimalValue(this.uGridCCSTargetList.Rows[i].Cells["Target"].Value.ToString());
                            drRow["EtcDesc"] = this.uGridCCSTargetList.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dtCCSTarList.Rows.Add(drRow);
                        }
                    }
                }

                if (dtCCSTarList.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000943", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsPTar.mfSaveINSCCSTarget(dtCCSTarList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            // List Refresh
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001009"
                                                    , "M001049"
                                                    , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 필수조건 확인
                if (this.uComboSearchPlant.Value == null || this.uComboSearchPlant.Value == DBNull.Value || this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216"
                                                , "M000266"
                                                , Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Length < 4)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216"
                                                , "M001106"
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else
                {
                    // 검색조건 변수 설정
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strQuarter = this.uComboSearchQuarter.Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcTarget), "ProcTarget");
                    QRPSTA.BL.STAPRC.ProcTarget clsPTar = new QRPSTA.BL.STAPRC.ProcTarget();
                    brwChannel.mfCredentials(clsPTar);

                    DataTable dtCCSTargetList = clsPTar.mfReadINSCCSTarget(strPlantCode, strYear, strQuarter, m_resSys.GetString("SYS_LANG"));

                    this.uGridCCSTargetList.DataSource = dtCCSTargetList;
                    this.uGridCCSTargetList.DataBind();

                    // 중복방지 PrimaryKey 설정
                    DataColumn[] dc = new DataColumn[5];
                    dc[0] = dtCCSTargetList.Columns["PlantCode"];
                    dc[1] = dtCCSTargetList.Columns["AYear"];
                    dc[2] = dtCCSTargetList.Columns["AQuarter"];
                    dc[3] = dtCCSTargetList.Columns["DetailProcessOperationType"];
                    dc[4] = dtCCSTargetList.Columns["ProductionType"];

                    dtCCSTargetList.PrimaryKey = dc;

                    if (dtCCSTargetList.Rows.Count > 0)
                    {
                        WinGrid wGrid = new WinGrid();
                        wGrid.mfSetAutoResizeColWidth(this.uGridCCSTargetList, 0);
                    }

                    // 공장콤보가 전체이면 저장버튼 비활성화
                    if (strPlantCode.Equals(string.Empty))
                    {
                        // 그리드 편집불가상태로
                        this.uGridCCSTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        this.uGridCCSTargetList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    }
                    else
                    {
                        // 그리드 편집가능상태로
                        this.uGridCCSTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                        this.uGridCCSTargetList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    }

                    // 그리드 기본값 변경
                    this.uGridCCSTargetList.DisplayLayout.Bands[0].Columns["PlantCode"].DefaultCellValue = this.uComboSearchPlant.Value;
                    this.uGridCCSTargetList.DisplayLayout.Bands[0].Columns["AYear"].DefaultCellValue = this.uTextSearchYear.Text;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchQuarter, "분기", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                // 분기 콤보 설정
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrText = new System.Collections.ArrayList();

                string strQuarter = Math.Ceiling((Convert.ToDecimal(DateTime.Now.Month) / 3.0m)).ToString();

                arrKey.Add("");
                arrText.Add("전체");
                for (int i = 1; i < 5; i++)
                {
                    arrKey.Add(i);
                    arrText.Add(i.ToString() + " / 4");
                }

                wCombo.mfSetComboEditor(this.uComboSearchQuarter, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , strQuarter, arrKey, arrText);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 분기 기본값
                string strQuarter = Math.Ceiling((Convert.ToDecimal(DateTime.Now.Month) / 3.0m)).ToString();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCCSTargetList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Columns 설정
                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "AYear", "년도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, true, false, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", DateTime.Now.Year.ToString());

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "AQuarter", "분기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", strQuarter);

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "DetailProcessOperationType", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "ProductionType", "제품군", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "Target", "목표합격률", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnnnnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridCCSTargetList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 목표불량율 Column PromptChar 설정
                this.uGridCCSTargetList.DisplayLayout.Bands[0].Columns["Target"].PromptChar = ' ';

                // FontSize 설정
                this.uGridCCSTargetList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSTargetList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // 공장
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridCCSTargetList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                // 분기
                DataTable dtQuarter = new DataTable();
                dtQuarter.Columns.Add("Key", typeof(string));
                dtQuarter.Columns.Add("Value", typeof(string));
                DataRow drRow;
                for (int i = 1; i < 5; i++)
                {
                    drRow = dtQuarter.NewRow();
                    drRow["Key"] = i.ToString();
                    drRow["Value"] = i.ToString() + " / 4";
                    dtQuarter.Rows.Add(drRow);
                }

                wGrid.mfSetGridColumnValueList(this.uGridCCSTargetList, 0, "AQuarter", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtQuarter);

                // 제품군
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                DataTable dtProductType = clsProduct.mfReadMASProduct_ActionType(m_resSys.GetString("SYS_PLANTCODE"), "", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridCCSTargetList, 0, "ProductionType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProductType);

                // 공정Type
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtDetailProcessOperationType = clsProcess.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumnValueList(this.uGridCCSTargetList, 0, "DetailProcessOperationType", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                , "", "선택", dtDetailProcessOperationType);

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridCCSTargetList, 0);

                // 그리드 편집불가상태로
                this.uGridCCSTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // 데이터 중복방지 설정
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcTarget), "ProcTarget");
                QRPSTA.BL.STAPRC.ProcTarget clsPTar = new QRPSTA.BL.STAPRC.ProcTarget();
                brwChannel.mfCredentials(clsPTar);

                DataTable dtPrimary = clsPTar.mfSetDataInfo();

                this.uGridCCSTargetList.DataSource = dtPrimary;
                this.uGridCCSTargetList.DataBind();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 조회년도 TextBox 설정
                this.uTextSearchYear.MaxLength = 4;
                this.uTextSearchYear.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                this.uTextSearchYear.Text = DateTime.Now.Year.ToString();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
        
        // 조회연도 숫자만 입력가능하게 하는 이벤트
        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSTargetList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 공장값 변경시 제품구분, 공정Type 콤보 설정
                if (e.Cell.Column.Key.Equals("PlantCode"))
                {
                    // 제품군
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    DataTable dtProductType = clsProduct.mfReadMASProduct_ActionType(e.Cell.Value.ToString(), "", m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGridCCSTargetList, e.Cell.Row.Index, "ProductionType", "", "선택", dtProductType);

                    // 공정Type
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    DataTable dtDetailProcessOperationType = clsProcess.mfReadProcessDetailProcessOperationType(e.Cell.Value.ToString());

                    wGrid.mfSetGridCellValueList(this.uGridCCSTargetList, e.Cell.Row.Index, "DetailProcessOperationType", "", "선택", dtDetailProcessOperationType);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSTargetList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // RowSelector Image 설정
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCCSTargetList_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            try
            {
                if (e.ErrorType.ToString().Equals("Data"))
                {
                    e.Cancel = true;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001122", "M000873"
                                        , e.DataErrorInfo.Row.Cells["PlantCode"].Text + "M000002" +
                                        e.DataErrorInfo.Row.Cells["AYear"].Text + "M000350" + e.DataErrorInfo.Row.Cells["AQuarter"].Text + "M000599" + "<br/><br/>" +
                                        "M000294" + e.DataErrorInfo.Row.Cells["DetailProcessOperationType"].Text + "<br/><br/>" +
                                        "M001086" + e.DataErrorInfo.Row.Cells["ProductionType"].Text + "<br/><br/>" +
                                        "M001247", Infragistics.Win.HAlign.Right);

                    e.DataErrorInfo.Row.Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }
    }
}
