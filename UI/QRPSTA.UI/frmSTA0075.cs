﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPSTA.UI
{
    public partial class frmSTA0075 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0075()
        {
            InitializeComponent();
        }

        private void frmSTA0075_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true,m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 그리드 리사이즈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0075_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxDaily.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                    uGroupBoxQCNState.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                }
                else
                {
                    uGroupBoxDaily.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uGroupBoxQCNState.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화면 종료시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0075_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmSTA0075_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("Daily 품질일보", m_resSys.GetString("SYS_FONTNAME"), 12);
            SetToolAuth();
            //컨트롤 초기화
            InitLabel();
            InitComboBox();
            InitGrid();
            InitButton();
            InitGroupBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        //사용자 권한 설정
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //Label 초기화
        private void InitLabel()
        {
            try
            { 
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDate, "날짜", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductDivide, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSearchPackage, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, false); // Package
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false); // 고객
            }  
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        //버튼 초기화
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonSendMail, "메일발송", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Mail);

                this.uButtonSendMail.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //ComboBox 초기화
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인잉
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", ""
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroup = new WinGroupBox();

                wGroup.mfSetGroupBox(this.uGroupBoxDaily, GroupBoxType.INFO, "품질일보 현황", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxQCNState, GroupBoxType.INFO, "QCN 현황", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxDaily.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxDaily.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxQCNState.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxQCNState.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //그리드 초기화
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //품질일보 현황 그리드
                grd.mfInitGeneralGrid(this.uGridDailyState, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridDailyState, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "DETAILPROCESSOPERATIONTYPE", "공정타입", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "Divide", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "일별");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "FailLotCount", "불량Lot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridDailyState, 0, "LotCount", "검사Lot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "LRR", "LRR(%)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "SampleSize", "시료수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "FaultRatePPM", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "ProcTarget", "품질목표", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 1000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "CCSFailLot", "CCS Fail(件)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "CCSReqLot", "CCS 의뢰(件)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDailyState, 0, "CCSPassRate", "CCS합격률(%)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////grd.mfSetGridColumn(this.uGridDailyState, 0, "CustomerCode", "고객사 코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 30
                //////        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////grd.mfSetGridColumn(this.uGridDailyState, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                //////        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////grd.mfSetGridColumn(this.uGridDailyState, 0, "FaultTypeCode", "불량유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                //////        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////grd.mfSetGridColumn(this.uGridDailyState, 0, "InspectFaultTypeName", "불량유형명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                //////        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////grd.mfSetGridColumn(this.uGridDailyState, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 1000
                //////        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                //QCN현황 그리드

                grd.mfInitGeneralGrid(this.uGridQCNStats, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Default, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                        , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "DETAILPROCESSOPERATIONTYPE", "공정타입", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "AriseDate", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "AriseTime", "발생시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 15
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 30
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "AriseEquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "InspectFaultTypeCode", "불량현황", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "InspectFaultTypeName", "불량현황", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "FirCauseDesc", "발생원인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 120
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "FirMeasureUserID", "인수자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "FirMeasureUserName", "인수자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "ImputationDeptCode", "귀책부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridQCNStats, 0, "ImputationDeptName", "귀책부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //this.uGridPMList.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.True;
                //this.uGridPMList.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.AutoFree;MergedCellAppearance.TextVAlign

                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FailLotCount"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FailLotCount"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["LotCount"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["LotCount"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["LRR"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["LRR"].MaskInput = "nnn,nnn,nnn.nn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FaultRatePPM"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["FaultRatePPM"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSFailLot"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSFailLot"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSReqLot"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSReqLot"].MaskInput = "nnn,nnn,nnn";
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSPassRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridDailyState.DisplayLayout.Bands[0].Columns["CCSPassRate"].MaskInput = "nnn,nnn,nnn.nn";

                this.uGridQCNStats.DisplayLayout.Bands[0].Columns["InspectFaultTypeName"].CellMultiLine = Infragistics.Win.DefaultableBoolean.True;
                this.uGridQCNStats.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Sychronized;
                this.uGridQCNStats.DisplayLayout.Bands[0].Columns["FirCauseDesc"].CellMultiLine = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 공장코드 변경시 ProductActionType콤보박스 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchProductActionType.Items.Clear();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "", "ActionTypeCode", "ActionTypeName", dtProductActionType);

                //PAKCAGE 정보 조회
                DataTable dtPackge = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                clsProduct.Dispose(); //Resouce 해제

                //PACKAGE 정보 삽입
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "", "Package", "ComboName", dtPackge);

                dtPackge.Dispose(); // Resource 해제

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region ToolBar Method
        public void mfCreate()
        {
            try
            {}
            catch(Exception ex)
            {}
            finally
            {}
        }
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();                                                          // 공장
                string strDate = this.uDateSearchDate.Value.ToString();                                                                 // 검색일
                string strProcessActionType = this.uComboSearchProductActionType.Value.ToString();                                      // 제품구분
                string strCustomer = this.uTextSearchCustomer.Text;                                                                     // 고객
                string strPACKAGE = this.uComboSearchPackage.Value == null ? string.Empty : this.uComboSearchPackage.Value.ToString(); // PACKAGE
                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtDailyQualty = clsReport.mfReadINSProcInspect_frmSTA0075_D1_PSTS(strPlantCode, strDate, strProcessActionType, strCustomer, strPACKAGE, m_resSys.GetString("SYS_LANG"));
                DataTable dtQCNState = clsReport.mfReadINSProcInspect_frmSTA0075_D2_PSTS(strPlantCode, strDate, strProcessActionType, strCustomer, strPACKAGE, m_resSys.GetString("SYS_LANG"));

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGridDailyState.DataSource = dtDailyQualty;
                this.uGridDailyState.DataBind();

                this.uGridQCNStats.DataSource = dtQCNState;
                this.uGridQCNStats.DataBind();

                if (dtDailyQualty.Rows.Count == 0 && dtQCNState.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //WinGrid grd = new WinGrid();
                    //grd.mfSetAutoResizeColWidth(this.uGridDailyState, 0);
                    //grd.mfSetAutoResizeColWidth(this.uGridQCNStats, 0);

                    for (int i = 0; i < uGridDailyState.Rows.Count; i++)
                    {
                        if (uGridDailyState.Rows[i].Cells["Divide"].Text == "CUM")
                        {
                            for (int j = 1; j < uGridDailyState.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridDailyState.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridDailyState.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            { }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            { }
            catch (Exception ex)
            { }
            finally
            { }
        }

        //public void mfExcel()
        //{
        //    try
        //    {
        //        int intLastRow = 4;
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinMessageBox msg = new WinMessageBox();
        //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        //            , "M001264", "M000047", "M000040", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
        //        {
        //            QRPBrowser brwChannel = new QRPBrowser();
        //            brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
        //            QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
        //            brwChannel.mfCredentials(clsReport);

        //            string strPlantCode = this.uComboSearchPlant.Value.ToString();
        //            string strDate = this.uDateSearchDate.Value.ToString();
        //            string strProductType = this.uComboSearchProductActionType.Value.ToString();

        //            // 프로그래스 팝업창 생성
        //            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
        //            Thread threadPop = m_ProgressPopup.mfStartThread();
        //            m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
        //            this.MdiParent.Cursor = Cursors.WaitCursor;

        //            //Sub-Strate, Lead Frame 각각 조회
        //            //DataTable dtSubDaily = clsDaily.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, "Sub");
        //            //DataTable dtLeadDaily = clsDaily.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, "Lead Frame");
        //            DataTable dtSubDaily = clsReport.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, "Sub");
        //            DataTable dtLeadDaily = clsReport.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, "Lead Frame"); ;

        //            int dtSubLength = dtSubDaily.Rows.Count;
        //            int dtLeadLength = dtLeadDaily.Rows.Count;

        //            this.uGridDailyState.DataSource = dtSubDaily;
        //            this.uGridDailyState.DataBind();

        //            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        //            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excel.Workbooks.Add(true);
        //            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Worksheets.get_Item(1);
        //            Microsoft.Office.Interop.Excel.Range range;
        //            //Microsoft.Office.Interop.Excel.DisplayAlerts = false;

        //            excelWorksheet.Name = "Daily 품질일보";
        //            //Sub-Strate로 조회
        //            //전체 Sheet적용 속성
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlToRight);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlDown);
        //            range = excelWorksheet.get_Range("A1", range);

        //            //날짜 표시를 위한 Parameter
        //            DateTime from = Convert.ToDateTime(this.uDateSearchDate.Value.ToString()).AddDays(-1);
        //            DateTime to = Convert.ToDateTime(this.uDateSearchDate.Value.ToString());

        //            //제목
        //            excel.Cells[2, 3] = "●Daily Sub-Strate PKG 공정 품질현황" + "(" + from.ToString("yyyy-MM-dd") + "  22:00 ~ " + to.ToString("yyyy-MM-dd") + " 22:00)"; //(12월11일 22:00 ~ 12월12일 22:00)";
        //            range = excelWorksheet.get_Range("C2", "C2");
        //            range.Font.Size = 24;
        //            range.Font.Bold = true;

        //            excel.Cells[2, 19] = "●Daily Lead Frame 공정 품질현황" + "(" + from.ToString("yyyy-MM-dd") + "  22:00 ~ " + to.ToString("yyyy-MM-dd") + " 22:00)"; //(12월11일 22:00 ~ 12월12일 22:00)";
        //            range = excelWorksheet.get_Range("S2", "S2");
        //            range.Font.Size = 24;
        //            range.Font.Bold = true;


        //            //cum
        //            //※CUM :11/30~12/12		
        //            excel.Cells[3, 3] = "※CUM :" + from.ToString("yyyy-MM-dd") + " ~ " + to.ToString("yyyy-MM-dd");
        //            range = excelWorksheet.get_Range("C3", "C3");
        //            range.Font.Size = 11;
        //            range.Font.Bold = false;
        //            range.Font.Color = System.Drawing.Color.Red.ToArgb();
        //            //workSheet_range.Font.Color = System.Drawing.Color.White.ToArgb();


        //            #region 컬럼명
        //            //Sub 데이터
        //            #region Sub-Strate
        //            excel.Cells[4, 3] = "공정";
        //            range = excelWorksheet.get_Range("C4", "D5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 5] = "불량" + "\r\n" + "Lot수";
        //            range = excelWorksheet.get_Range("E4", "E5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 6] = "검사" + "\r\n" + "Lot수";
        //            range = excelWorksheet.get_Range("F4", "F5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 7] = "LRR" + "\r\n" + "(%)";
        //            range = excelWorksheet.get_Range("G4", "G5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 8] = "불량수";
        //            range = excelWorksheet.get_Range("H4", "H5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 9] = "시료수";
        //            range = excelWorksheet.get_Range("I4", "I5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 10] = "불량율" + "\r\n" + "(ppm)";
        //            range = excelWorksheet.get_Range("J4", "J5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 11] = "품질" + "\r\n" + "목표";
        //            range = excelWorksheet.get_Range("K4", "K5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 12] = "CCS" + "\r\n" + "Fail(件)";
        //            range = excelWorksheet.get_Range("L4", "L5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 13] = "CCS" + "\r\n" + "의뢰(件)";
        //            range = excelWorksheet.get_Range("M4", "M5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 14] = "CCS" + "\r\n" + "합격률(%)";
        //            range = excelWorksheet.get_Range("N4", "N5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[4, 15] = "불량 내용";
        //            //range = excelWorksheet.get_Range("O4", "R4");
        //            //range.MergeCells = true;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 15] = "고객사코드";
        //            //range = excelWorksheet.get_Range("O5", "O5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 16] = "PACKAGE";
        //            //range = excelWorksheet.get_Range("P5", "P5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            ////excel.Cells[4, 18] = "불량유형코드";
        //            ////range = excelWorksheet.get_Range("R5", "R5");
        //            ////range.MergeCells = true;
        //            ////range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            ////range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            ////range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            //excel.Cells[5, 17] = "불량유형명";
        //            //range = excelWorksheet.get_Range("Q5", "Q5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 18] = "구분";
        //            //range = excelWorksheet.get_Range("R5", "R5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //            #endregion
        //            //Lead-Frame 데이터
        //            #region Lead Frame
        //            excel.Cells[4, 19] = "공정";
        //            range = excelWorksheet.get_Range("S4", "T5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 21] = "불량" + "\r\n" + "Lot수";
        //            range = excelWorksheet.get_Range("U4", "U5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 22] = "검사" + "\r\n" + "Lot수";
        //            range = excelWorksheet.get_Range("V4", "V5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 23] = "LRR" + "\r\n" + "(%)";
        //            range = excelWorksheet.get_Range("W4", "W5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 24] = "불량수";
        //            range = excelWorksheet.get_Range("X4", "X5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 25] = "시료수";
        //            range = excelWorksheet.get_Range("Y4", "Y5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 26] = "불량율" + "\r\n" + "(ppm)";
        //            range = excelWorksheet.get_Range("Z4", "Z5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 27] = "품질" + "\r\n" + "목표";
        //            range = excelWorksheet.get_Range("AA4", "AA5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 28] = "CCS" + "\r\n" + "Fail(件)";
        //            range = excelWorksheet.get_Range("AB4", "AB5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 29] = "CCS" + "\r\n" + "의뢰(件)";
        //            range = excelWorksheet.get_Range("AC4", "AC5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[4, 30] = "CCS" + "\r\n" + "합격률(%)";
        //            range = excelWorksheet.get_Range("AD4", "AD5");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[4, 31] = "불량 내용";
        //            //range = excelWorksheet.get_Range("AE4", "AH4");
        //            //range.MergeCells = true;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 31] = "고객사코드";
        //            //range = excelWorksheet.get_Range("AE5", "AE5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 32] = "PACKAGE";
        //            //range = excelWorksheet.get_Range("AF5", "AF5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 33] = "불량유형명";
        //            //range = excelWorksheet.get_Range("AG5", "AG5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            //excel.Cells[5, 34] = "구분";
        //            //range = excelWorksheet.get_Range("AH5", "AH5");
        //            //range.MergeCells = false;
        //            //range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            //range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            //range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //            #endregion

        //            #endregion
        //            //Data Biding
        //            #region 엑셀에 데이터 보내고 그리기
        //            for (int i = 0; i < this.uGridDailyState.Rows.Count; i++)
        //            {
        //                if (i > 0)
        //                {
        //                    if (this.uGridDailyState.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString()
        //                        == this.uGridDailyState.Rows[i - 1].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("C" + (5 + i).ToString(), "C" + (6 + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                    if (this.uGridDailyState.Rows[i].Cells["Divide"].Value.ToString() == this.uGridDailyState.Rows[i - 1].Cells["Divide"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("D" + (5 + i).ToString(), "D" + (6 + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                }

        //                excel.Cells[6 + i, 3] = this.uGridDailyState.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                excel.Cells[6 + i, 4] = this.uGridDailyState.Rows[i].Cells["Divide"].Text;
        //                excel.Cells[6 + i, 5] = this.uGridDailyState.Rows[i].Cells["FailLotCount"].Text;
        //                excel.Cells[6 + i, 6] = this.uGridDailyState.Rows[i].Cells["LotCount"].Text;
        //                excel.Cells[6 + i, 7] = this.uGridDailyState.Rows[i].Cells["LRR"].Text;
        //                excel.Cells[6 + i, 8] = this.uGridDailyState.Rows[i].Cells["FaultQty"].Text;
        //                excel.Cells[6 + i, 9] = this.uGridDailyState.Rows[i].Cells["SampleSize"].Text;
        //                excel.Cells[6 + i, 10] = this.uGridDailyState.Rows[i].Cells["FaultRatePPM"].Text;
        //                excel.Cells[6 + i, 11] = this.uGridDailyState.Rows[i].Cells["ProcTarget"].Text;
        //                excel.Cells[6 + i, 12] = this.uGridDailyState.Rows[i].Cells["CCSFailLot"].Text;
        //                excel.Cells[6 + i, 13] = this.uGridDailyState.Rows[i].Cells["CCSReqLot"].Text;
        //                excel.Cells[6 + i, 14] = this.uGridDailyState.Rows[i].Cells["CCSPassRate"].Text;
        //                //excel.Cells[6 + i, 15] = this.uGridDailyState.Rows[i].Cells["CustomerCode"].Text;
        //                //excel.Cells[6 + i, 16] = this.uGridDailyState.Rows[i].Cells["Package"].Text;
        //                //excel.Cells[6 + i, 17] = this.uGridDailyState.Rows[i].Cells["InspectFaultTypeName"].Text;
        //                //excel.Cells[6 + i, 18] = this.uGridDailyState.Rows[i].Cells["Gubun"].Text;
        //                intLastRow = i;
        //            }
        //            intLastRow = 4;
        //            InitGrid();
        //            this.uGridDailyState.DataSource = dtLeadDaily;
        //            this.uGridDailyState.DataBind();

        //            for (int i = 0; i < this.uGridDailyState.Rows.Count; i++)
        //            {
        //                if (i > 0)
        //                {
        //                    if (this.uGridDailyState.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString()
        //                        == this.uGridDailyState.Rows[i - 1].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("S" + (5 + i).ToString(), "S" + (6 + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                    if (this.uGridDailyState.Rows[i].Cells["Divide"].Value.ToString() == this.uGridDailyState.Rows[i - 1].Cells["Divide"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("T" + (5 + i).ToString(), "T" + (6 + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                }
        //                excel.Cells[6 + i, 19] = this.uGridDailyState.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                excel.Cells[6 + i, 20] = this.uGridDailyState.Rows[i].Cells["Divide"].Text;
        //                excel.Cells[6 + i, 21] = this.uGridDailyState.Rows[i].Cells["FailLotCount"].Text;
        //                excel.Cells[6 + i, 22] = this.uGridDailyState.Rows[i].Cells["LotCount"].Text;
        //                excel.Cells[6 + i, 23] = this.uGridDailyState.Rows[i].Cells["LRR"].Text;
        //                excel.Cells[6 + i, 24] = this.uGridDailyState.Rows[i].Cells["FaultQty"].Text;
        //                excel.Cells[6 + i, 25] = this.uGridDailyState.Rows[i].Cells["SampleSize"].Text;
        //                excel.Cells[6 + i, 26] = this.uGridDailyState.Rows[i].Cells["FaultRatePPM"].Text;
        //                excel.Cells[6 + i, 27] = this.uGridDailyState.Rows[i].Cells["ProcTarget"].Text;
        //                excel.Cells[6 + i, 28] = this.uGridDailyState.Rows[i].Cells["CCSFailLot"].Text;
        //                excel.Cells[6 + i, 29] = this.uGridDailyState.Rows[i].Cells["CCSReqLot"].Text;
        //                excel.Cells[6 + i, 30] = this.uGridDailyState.Rows[i].Cells["CCSPassRate"].Text;
        //                //excel.Cells[6 + i, 31] = this.uGridDailyState.Rows[i].Cells["CustomerCode"].Text;
        //                //excel.Cells[6 + i, 32] = this.uGridDailyState.Rows[i].Cells["Package"].Text;
        //                //excel.Cells[6 + i, 33] = this.uGridDailyState.Rows[i].Cells["InspectFaultTypeName"].Text;
        //                //excel.Cells[6 + i, 34] = this.uGridDailyState.Rows[i].Cells["Gubun"].Text;

        //                intLastRow = i;
                        

        //            }


        //            //Daily그리드 최대길이
        //            int intMaxgrdLength = 0;
        //            if (dtLeadLength > dtSubLength)
        //            {
        //                intMaxgrdLength = dtLeadLength;
        //            }
        //            else
        //            {
        //                intMaxgrdLength = dtSubLength;
        //            }
        //            #endregion
        //            //QCN 현황
        //            #region QCN현황
        //            //Sub, LeadFrame 구분
        //            int intQCNLength = 0;
        //            DataTable dtQCNSub = clsReport.mfReadINSProcInspect_frmSTA0075_D2(strPlantCode, strDate, "Sub", m_resSys.GetString("SYS_LANG"));
        //            DataTable dtQCNLead = clsReport.mfReadINSProcInspect_frmSTA0075_D2(strPlantCode, strDate, "Lead Frame", m_resSys.GetString("SYS_LANG"));
        //            if (dtQCNLead.Rows.Count > dtQCNSub.Rows.Count)
        //            {
        //                intQCNLength = dtQCNLead.Rows.Count;
        //            }
        //            else
        //            {
        //                intQCNLength = dtQCNSub.Rows.Count;
        //            }

        //            //intMaxgrdLength -- 상단 그리드의 최대길이(QCN 그리드 엑셀로 옮길 시작 위치)
        //            #region 컬럼명
        //            //SubData
        //            #region Sub_Strate
        //            excel.Cells[7 + intMaxgrdLength, 3] = "구분";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 3), CellAddress(8 + intMaxgrdLength, 3));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 4] = "공정";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 4), CellAddress(8 + intMaxgrdLength, 5));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 6] = "발생일";
        //            //range = excelWorksheet.get_Range("E" + Convert.ToString(6 + intMaxgrdLength),"E" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 6), CellAddress(8 + intMaxgrdLength, 6));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 7] = "발생시간";
        //            //range = excelWorksheet.get_Range("F" + Convert.ToString(6 + intMaxgrdLength), "F" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 7), CellAddress(8 + intMaxgrdLength, 7));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 8] = "고객사";
        //            //range = excelWorksheet.get_Range("G" + Convert.ToString(6 + intMaxgrdLength), "G" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 8), CellAddress(8 + intMaxgrdLength,8));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 9] = "설비번호";
        //            //range = excelWorksheet.get_Range("H" + Convert.ToString(6 + intMaxgrdLength), "H" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 9), CellAddress(8 + intMaxgrdLength, 9));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 10] = "LotNo";
        //            //range = excelWorksheet.get_Range("I" + Convert.ToString(6 + intMaxgrdLength), "I" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 10), CellAddress(8+ intMaxgrdLength, 10));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 11] = "불량현황";
        //            //range = excelWorksheet.get_Range("J" + Convert.ToString(6 + intMaxgrdLength),"J" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 11), CellAddress(8 + intMaxgrdLength, 13));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 14] = "발생원인";
        //            //range = excelWorksheet.get_Range("K" + Convert.ToString(6+ intMaxgrdLength), "K" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 14), CellAddress(8 + intMaxgrdLength, 14));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 15] = "작업자";
        //            //range = excelWorksheet.get_Range("L" + Convert.ToString(6 + intMaxgrdLength), "L" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 15), CellAddress(8 + intMaxgrdLength, 15));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 16] = "검사자";
        //            //range = excelWorksheet.get_Range("M" + Convert.ToString(6 + intMaxgrdLength), "M" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 16), CellAddress(8 + intMaxgrdLength, 16));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 17] = "인수자";
        //            //range = excelWorksheet.get_Range("N" + Convert.ToString(6 + intMaxgrdLength), "N" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 17), CellAddress(8 + intMaxgrdLength, 17));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 18] = "귀책부서";
        //            //range = excelWorksheet.get_Range("O" + Convert.ToString(6 + intMaxgrdLength), "O" + Convert.ToString(7 + intMaxgrdLength));
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 18), CellAddress(8 + intMaxgrdLength, 18));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            #endregion
        //            //Lead Frame Data
        //            #region LeadFrame
        //            //intMaxgrdLength -- 상단 그리드의 최대길이(QCN 그리드 엑셀로 옮길 시작 위치) 4, 19  /  S4, T5
        //            excel.Cells[7 + intMaxgrdLength, 19] = "구분";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 19), CellAddress(8 + intMaxgrdLength, 19));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 20] = "공정";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 20), CellAddress(8 + intMaxgrdLength, 21));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 22] = "발생일";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 22), CellAddress(8 + intMaxgrdLength, 22));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 23] = "발생시간";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 23), CellAddress(8 + intMaxgrdLength, 23));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 24] = "고객사";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 24), CellAddress(8 + intMaxgrdLength, 24));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 25] = "설비번호";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 25), CellAddress(8 + intMaxgrdLength, 25));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 26] = "LotNo";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 26), CellAddress(8 + intMaxgrdLength, 26));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 27] = "불량현황";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 27), CellAddress(8 + intMaxgrdLength, 29));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 30] = "발생원인";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 30), CellAddress(8 + intMaxgrdLength, 30));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 31] = "작업자";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 31), CellAddress(8 + intMaxgrdLength, 31));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 32] = "검사자";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 32), CellAddress(8 + intMaxgrdLength, 32));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 33] = "인수자";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 33), CellAddress(8 + intMaxgrdLength, 33));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //            excel.Cells[7 + intMaxgrdLength, 34] = "귀책부서";
        //            range = excelWorksheet.get_Range(CellAddress(7 + intMaxgrdLength, 34), CellAddress(8 + intMaxgrdLength, 34));
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //            #endregion
        //            #endregion
        //            //엑셀에 데이터 삽입
        //            //SubStrate
        //            this.uGridQCNStats.DataSource = dtQCNSub;
        //            this.uGridQCNStats.DataBind();
        //            for (int i = 0; i < this.uGridQCNStats.Rows.Count; i++)
        //            {
        //                if (i > 0)
        //                {
        //                    if (this.uGridQCNStats.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString()
        //                           == this.uGridQCNStats.Rows[i - 1].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("D" + (8 + intMaxgrdLength + i).ToString(), "E" + (9 + intMaxgrdLength + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                }
        //                range = excelWorksheet.get_Range("D" + (9 + intMaxgrdLength + i).ToString(), "E" + (9 + intMaxgrdLength + i).ToString());
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.MergeCells = true;

        //                range = excelWorksheet.get_Range("K" + (9 + intMaxgrdLength + i).ToString(), "M" + (9 + intMaxgrdLength + i).ToString());
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.MergeCells = true;

        //                excel.Cells[9 + i + intMaxgrdLength, 3] = this.uGridQCNStats.Rows[i].Cells["Gubun"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 4] = this.uGridQCNStats.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 6] = Convert.ToString(this.uGridQCNStats.Rows[i].Cells["AriseDate"].Value.ToString());
        //                excel.Cells[9 + i + intMaxgrdLength, 7] = this.uGridQCNStats.Rows[i].Cells["AriseTime"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 8] = this.uGridQCNStats.Rows[i].Cells["CustomerName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 9] = this.uGridQCNStats.Rows[i].Cells["AriseEquipCode"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 10] = this.uGridQCNStats.Rows[i].Cells["LotNo"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 11] = this.uGridQCNStats.Rows[i].Cells["InspectFaultTypeName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 14] = this.uGridQCNStats.Rows[i].Cells["FirCauseDesc"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 15] = this.uGridQCNStats.Rows[i].Cells["WorkUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 16] = this.uGridQCNStats.Rows[i].Cells["InspectUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 17] = this.uGridQCNStats.Rows[i].Cells["FirMeasureUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 18] = this.uGridQCNStats.Rows[i].Cells["ImputationDeptName"].Text;
        //            }

        //            //Lead Frame
        //            this.uGridQCNStats.DataSource = dtQCNLead;
        //            this.uGridQCNStats.DataBind();

        //            for (int i = 0; i < this.uGridQCNStats.Rows.Count; i++)
        //            {
        //                if (i > 0)
        //                {
        //                    if (this.uGridQCNStats.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString()
        //                           == this.uGridQCNStats.Rows[i - 1].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString())
        //                    {
        //                        range = excelWorksheet.get_Range("T" + (8 + intMaxgrdLength + i).ToString(), "U" + (9 + intMaxgrdLength + i).ToString());
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;
        //                    }
        //                }
        //                range = excelWorksheet.get_Range("T" + (9 + intMaxgrdLength + i).ToString(), "U" + (9 + intMaxgrdLength + i).ToString());
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.MergeCells = true;

        //                range = excelWorksheet.get_Range("AA" + (9 + intMaxgrdLength + i).ToString(), "AC" + (9 + intMaxgrdLength + i).ToString());
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.MergeCells = true;

        //                excel.Cells[9 + i + intMaxgrdLength, 19] = this.uGridQCNStats.Rows[i].Cells["Gubun"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 20] = this.uGridQCNStats.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 22] = Convert.ToString(this.uGridQCNStats.Rows[i].Cells["AriseDate"].Value.ToString());
        //                excel.Cells[9 + i + intMaxgrdLength, 23] = this.uGridQCNStats.Rows[i].Cells["AriseTime"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 24] = this.uGridQCNStats.Rows[i].Cells["CustomerName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 25] = this.uGridQCNStats.Rows[i].Cells["AriseEquipCode"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 26] = this.uGridQCNStats.Rows[i].Cells["LotNo"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 27] = this.uGridQCNStats.Rows[i].Cells["InspectFaultTypeName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 30] = this.uGridQCNStats.Rows[i].Cells["FirCauseDesc"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 31] = this.uGridQCNStats.Rows[i].Cells["WorkUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 32] = this.uGridQCNStats.Rows[i].Cells["InspectUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 33] = this.uGridQCNStats.Rows[i].Cells["FirMeasureUserName"].Text;
        //                excel.Cells[9 + i + intMaxgrdLength, 34] = this.uGridQCNStats.Rows[i].Cells["ImputationDeptName"].Text;
        //            }
        //            //Line 그리기
        //            range = excelWorksheet.get_Range("C4", "N" + (5 + intMaxgrdLength).ToString());
        //            range.Borders.LineStyle = BorderStyle.FixedSingle;
        //            range = excelWorksheet.get_Range("S4", "AD" + (5 + intMaxgrdLength).ToString());
        //            range.Borders.LineStyle = BorderStyle.FixedSingle;
        //            range = excelWorksheet.get_Range("C" + (7 + intMaxgrdLength).ToString(), "R" + (8 + intMaxgrdLength + dtQCNSub.Rows.Count).ToString());
        //            range.Borders.LineStyle = BorderStyle.FixedSingle;
        //            range = excelWorksheet.get_Range("S" + (7 + intMaxgrdLength).ToString(), "AH" + (8 + intMaxgrdLength + dtQCNLead.Rows.Count).ToString());
        //            range.Borders.LineStyle = BorderStyle.FixedSingle;
        //            //컬럼 폭 조절
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range.ColumnWidth = 0.85;
        //            range = excelWorksheet.get_Range("B1", Type.Missing);
        //            range.ColumnWidth = 0.38;
        //            range = excelWorksheet.get_Range("C1", Type.Missing);
        //            range.ColumnWidth = 8.75;
        //            range = excelWorksheet.get_Range("D1", Type.Missing);
        //            range.ColumnWidth = 5.38;
        //            range = excelWorksheet.get_Range("E1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("F1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("G1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("H1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("I1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("J1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("K1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("L1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("M1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("N1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("O1", Type.Missing);
        //            range.ColumnWidth = 10.75;
        //            range = excelWorksheet.get_Range("P1", Type.Missing);
        //            range.ColumnWidth = 14.25;
        //            range = excelWorksheet.get_Range("Q1", Type.Missing);
        //            range.ColumnWidth = 11.75;
        //            range = excelWorksheet.get_Range("R1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("S1", Type.Missing);
        //            range.ColumnWidth = 8.75;
        //            range = excelWorksheet.get_Range("T1", Type.Missing);
        //            range.ColumnWidth = 5.38;
        //            range = excelWorksheet.get_Range("U1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("V1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("W1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("X1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("Y1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("Z1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("W1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("X1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("Y1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("Z1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("AA1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("AB1", Type.Missing);
        //            range.ColumnWidth = 14.25;
        //            range = excelWorksheet.get_Range("AC1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("AD1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("AE1", Type.Missing);
        //            range.ColumnWidth = 10.75;
        //            range = excelWorksheet.get_Range("AF1", Type.Missing);
        //            range.ColumnWidth = 14.25;
        //            range = excelWorksheet.get_Range("AG1", Type.Missing);
        //            range.ColumnWidth = 11.75;
        //            range = excelWorksheet.get_Range("AH1", Type.Missing);
        //            range.ColumnWidth = 8.88;


        //            #endregion
        //                excelWorksheet.Application.ActiveWindow.Zoom = 75;

        //            DateTime TitleDate = Convert.ToDateTime(this.uDateSearchDate.Value.ToString());
        //            string strFileName = TitleDate.ToString("MM-dd");
        //            string strExePath = System.Windows.Forms.Application.ExecutablePath;
        //            int intPos = strExePath.LastIndexOf(@"\");
        //            strExePath = strExePath.Substring(0, intPos + 1) + "QRPKPI\\";

        //            // POPUP창 Close
        //            this.MdiParent.Cursor = Cursors.Default;
        //            m_ProgressPopup.mfCloseProgressPopup(this);

        //            //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
        //            if (!System.IO.Directory.Exists(strExePath))
        //                System.IO.Directory.CreateDirectory(strExePath);

        //            excelWorkbook.SaveAs(strExePath + strFileName + "_DailyQuality.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //                                 Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //            excel.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //        //엑셀로  발송 후 데이터그리드 원상 복귀
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        QRPBrowser brwChannel = new QRPBrowser();
        //        //brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.STADailyQuality), "STADailyQuality");
        //        //QRPSTA.BL.STAPRC.STADailyQuality clsDaily = new QRPSTA.BL.STAPRC.STADailyQuality();
        //        //brwChannel.mfCredentials(clsDaily);
        //        brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
        //        QRPSTA.BL.STAPRC.INSProcessReport clsReporct = new QRPSTA.BL.STAPRC.INSProcessReport();

        //        string strPlantCode = this.uComboSearchPlant.Value.ToString();
        //        string strDate = this.uDateSearchDate.Value.ToString();
        //        string strProductType = this.uComboSearchProductActionType.Value.ToString();

        //        //DataTable dtRtn1 = clsDaily.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, strProductType);
        //        //DataTable dtRtn2 = clsDaily.mfReadINSProcInspect_frmSTA0075_D2(strPlantCode, strDate, strProductType);
        //        DataTable dtRtn1 = clsReporct.mfReadINSProcInspect_frmSTA0075_D1(strPlantCode, strDate, strProductType);
        //        DataTable dtRtn2 = clsReporct.mfReadINSProcInspect_frmSTA0075_D2(strPlantCode, strDate, strProductType, m_resSys.GetString("SYS_LANG"));

        //        this.uGridDailyState.DataSource = dtRtn1;
        //        this.uGridDailyState.DataBind();

        //        this.uGridQCNStats.DataSource = dtRtn2;
        //        this.uGridQCNStats.DataBind();
        //    }
        //}

        public void mfExcel()
        {
            try
            {
                if (this.uGridDailyState.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridDailyState);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try { }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion

        #region 엑셀관련 함수
        /// <summary>
        /// 지정된 Column Key를 Column 순번으로 변환 합니다.
        /// </summary>
        /// <param name="columnKey">Column Key</param>
        /// <returns>1부터 시작하는 Column key의 순번.</returns>
        private static int ConvertColumnKeyToIndex(string columnKey)
        {
            int index = 0;

            int x = columnKey.Length - 1;

            for (int i = 0; i < columnKey.Length; i++)
            {
                index += (Convert.ToInt32(columnKey[i]) - 64) * Convert.ToInt32(Math.Pow(26d, Convert.ToDouble(x--)));
            }

            return index;
        }

        /// <summary>
        /// 지정된 컬럼 순번을 Column key로 변환 합니다..
        /// </summary>
        /// <param name="columnIndex">1부터 시작하는 Column 순번.</param>
        /// <returns>Column Key</returns>
        private static string ConvertColumnIndexToKey(int columnIndex)
        {
            string key = string.Empty;

            for (int i = Convert.ToInt32(Math.Log(Convert.ToDouble(25d * (Convert.ToDouble(columnIndex) + 1d))) / Math.Log(26d)) - 1; i >= 0; i--)
            {
                int x = Convert.ToInt32(Math.Pow(26d, i + 1d) - 1d) / 25 - 1;
                if (columnIndex > x)
                {
                    key += (char)(((columnIndex - x - 1) / Convert.ToInt32(Math.Pow(26d, i))) % 26d + 65d);
                }
            }

            return key;
        }

        /// <summary>
        /// 새로운 CellAddress 개체 인스턴스를 Cell의 열과 행을 사용해 초기화 합니다.
        /// </summary>
        /// <param name="columnIndex">
        /// 1부터 시작하는 Column 순번.
        /// </param>
        /// <param name="rowIndex">
        /// 1부터 시작하는 Row 순번.
        /// </param>
        public string CellAddress(int intRowIndex, int intColumnIndex)
        {
            string strAddress = string.Empty;

            string strColumnKey = ConvertColumnIndexToKey(intColumnIndex);
            string strRowIndex = intRowIndex.ToString();
            strAddress = string.Format("{0}{1}", strColumnKey, strRowIndex);

            return strAddress;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null; MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion

        //메일 전송 버튼 클릭 이벤트
        private void uButtonSendMail_Click(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            string strExePath = System.Windows.Forms.Application.ExecutablePath;
            int intPos = strExePath.LastIndexOf(@"\");
            strExePath = strExePath.Substring(0, intPos + 1) + "QRPKPI\\";
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                OpenFileDialog fileDialog = new OpenFileDialog();

                fileDialog.InitialDirectory = strExePath;
                fileDialog.Filter = "excel File (*.xls)|*.xls";
                fileDialog.FilterIndex = 1;
                fileDialog.RestoreDirectory = false;
                fileDialog.Multiselect = true;

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string[] strFullPathFile = fileDialog.FileNames;

                    if (!strFullPathFile.Length.Equals(3))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001141", "M001139 " + strFullPathFile.Length.ToString() + "M000159"
                                                            , "M000024", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        //첨부문서를 위한 FileServer 연결정보
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboSearchPlant.Value.ToString(), "S02");
         
                        //첨부화일 저장경로
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboSearchPlant.Value.ToString(), "D0022");
         
                        //첨부문서 FileServer로 보내기
                        QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                        ArrayList arrFile = new ArrayList();
                        ArrayList arrAttachFile = new ArrayList();
         
                        ////파일서버에 Uppload를 시킬 로컬화일 경로
                        for (int i = 0; i < strFullPathFile.Length; i++)
                        {
                            FileInfo fileDoc = new FileInfo(strFullPathFile[i]);
                            arrFile.Add(fileDoc.Name);

                            arrAttachFile.Add(m_resSys.GetString("SYS_USERID") + "\\" + fileDoc.Name);
                        }

         
                        ////메일전송Method에 보낼 Array : 경로는 제외하고 로그인사용자ID + 화일명 으로 구성 ==> 화일중복방지를 위해
                        fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + m_resSys.GetString("SYS_USERID"),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        if (arrFile.Count != 0)
                        {
                            fileAtt.mfFileUpload_NonAssync();
                        }
                        //발송할 Mail 검색
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        //테스트유저에만 메일 발송
                        string strPlantCode = this.uComboSearchPlant.Value.ToString();
                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, "testusr", m_resSys.GetString("SYS_LANG"));

                        //메일 발송 String
                        DateTime Date1 = DateTime.Now;
                        string strDate = Date1.ToString("yyyy-MM-dd");

                        
                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);

                        string strErrMail = string.Empty;
                        string strErrUserName = string.Empty;

                        for (int i = 0; i < dtUser.Rows.Count; i++)
                        {
                            //메일 발송 Method(메일 주소에 bokwang.com이 없을경우는 발송하지 않는다.)
                            if (dtUser.Rows[i]["EMail"].ToString().Contains("bokwang.com"))
                            {
                                strErrMail = dtUser.Rows[i]["EMail"].ToString();
                                strErrUserName = dtUser.Rows[i]["UserName"].ToString();

                                bool boolRtn = clsmail.mfSendSMTPMail(this.uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_EMAIL"), m_resSys.GetString("SYS_USERNAME")
                                                                                                , strErrMail, Date1 + "  품질일보", "품질일보 테스트", arrAttachFile);

                                if (!boolRtn)
                                {
                                    
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M000402", "M000403", strErrUserName + "M000355" + strErrMail + "M000389", Infragistics.Win.HAlign.Right);

                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 고객명이 공백이 아닌 경우 Clear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_ValueChanged(object sender, EventArgs e)
        {
            //고객명이 공백이 아닌경우 Clear
            if (!this.uTextSearchCustomerName.Text.Equals(string.Empty))
                this.uTextSearchCustomerName.Clear();
        }

        /// <summary>
        /// 고객명 직접입력 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Key 정보가 Enter가 아닌경우 Return
                if (e.KeyData != Keys.Enter)
                    return;

                //고객코드가 공백인경우 Return
                string strCustomerCode = this.uTextSearchCustomer.Text;
                if (strCustomerCode.Equals(string.Empty))
                    return;

                //고객정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //고객 정보 조회
                DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));
                clsCustomer.Dispose();

                //정보가 없는경우 메세지출력
                if (dtCustomer.Rows.Count == 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    //입력하신 고객코드가 존재하지 않습니다.
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000255", "M000889", Infragistics.Win.HAlign.Right);

                    return;
                }
                //고객사명 삽입
                this.uTextSearchCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 고객 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                frmPOP0003 frmCmr = new frmPOP0003();
                frmCmr.ShowDialog();

                //팝업창에서 고객 코드를 선택하지 않았다면 Return
                if (frmCmr.CustomerCode == null
                    || frmCmr.CustomerCode.Equals(string.Empty))
                    return;

                //고객정보 삽입
                this.uTextSearchCustomer.Text = frmCmr.CustomerCode;
                this.uTextSearchCustomerName.Text = frmCmr.CustomerName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
