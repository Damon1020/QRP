﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPSTA.UI
{
    public partial class frmSTA0079 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0079()
        {
            InitializeComponent();
        }

        private void frmSTA0079_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("특성 Data 공정품질 실적", m_resSys.GetString("SYS_FONTNAME"), 12);
            InitComboBox();
            InitYear();
            SetToolAuth();
            InitLabel();
            InitGrid();
        }

        private void frmSTA0079_Resize(object sender, EventArgs e)
        {
            if (this.Width > 1070)
            {
                uGridProc.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
            }
            else
            {
                uGridProc.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            }
        }

        private void frmSTA0079_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmSTA0079_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        #region 초기화 메소드
        /// <summary>
        /// 사용권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인딩
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2") + "월");
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "연도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchProductProductActionType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                int intBandIndex = 0;

                wGrid.mfInitGeneralGrid(this.uGridProc, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "InspectTypeName", "검사유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "MonthlyResult", "당해 당월 실적", 4, 0, 10, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
                int i = 0;
                for (i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
                }

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "QuarterResult", "전년 분기별 실적", 14 + i, 0, 4, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "HarfResult", "전년 반기별 실적", 18 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "TotalResult", "Total실적", 21 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);



                this.uGridProc.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProc.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;
                this.uGridProc.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }


        /// <summary>
        /// 연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region Toolbar Method
        public void mfCreate()
        {

        }

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                #region 필수확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "입력사항 확인", "공장을 선택해 주세요.", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "입력사항 확인", "연도를 입력해 주세요.", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchMonth.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "입력사항 확인", "달을 선택해 주세요.", Infragistics.Win.HAlign.Center);

                    this.uComboSearchMonth.DropDown();
                    return;
                }
                #endregion
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                    QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                    brwChannel.mfCredentials(clsReport);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //그리드 컬럼 변경
                    ChangeGridColumn(0);

                    //검색 파라미터
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strMonth = this.uComboSearchMonth.Value.ToString();
                    string strProductActionType = this.uComboSearchProductActionType.Value.ToString();


                    DataTable dtRtn = clsReport.mfReadINSProcInspect_frmSTA0079(strPlantCode, strYear, strMonth, strProductActionType, m_resSys.GetString("SYS_LANG"));
                    dtRtn.TableName = "dtRtn";

                    #region Grid모양에 맞춰서 DataTable 변경
                    int intMonthDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));

                    DataTable dtProcResultGrid = new DataTable("ProcResult");
                    DataColumn[] dcProcResult = new DataColumn[3];
                    dtProcResultGrid.Columns.Add("InspectTypeName", typeof(string));
                    dtProcResultGrid.Columns.Add("InspectItemName", typeof(string));
                    dtProcResultGrid.Columns.Add("Gubun", typeof(string));
                    dtProcResultGrid.Columns.Add("D01", typeof(string));
                    dtProcResultGrid.Columns.Add("D02", typeof(string));
                    dtProcResultGrid.Columns.Add("D03", typeof(string));
                    dtProcResultGrid.Columns.Add("D04", typeof(string));
                    dtProcResultGrid.Columns.Add("D05", typeof(string));
                    dtProcResultGrid.Columns.Add("D06", typeof(string));
                    dtProcResultGrid.Columns.Add("D07", typeof(string));
                    dtProcResultGrid.Columns.Add("D08", typeof(string));
                    dtProcResultGrid.Columns.Add("D09", typeof(string));
                    dtProcResultGrid.Columns.Add("D10", typeof(string));
                    for (int i = 11; i <= intMonthDay; i++)
                    {
                        dtProcResultGrid.Columns.Add("D" + i.ToString(), typeof(string));
                    }
                    dtProcResultGrid.Columns.Add("Q1", typeof(string));
                    dtProcResultGrid.Columns.Add("Q2", typeof(string));
                    dtProcResultGrid.Columns.Add("Q3", typeof(string));
                    dtProcResultGrid.Columns.Add("Q4", typeof(string));
                    dtProcResultGrid.Columns.Add("FirHalf", typeof(string));
                    dtProcResultGrid.Columns.Add("SecHalf", typeof(string));
                    dtProcResultGrid.Columns.Add("Total", typeof(string));

                    dcProcResult[0] = dtProcResultGrid.Columns["InspectTypeName"];
                    dcProcResult[1] = dtProcResultGrid.Columns["InspectItemName"];
                    dcProcResult[2] = dtProcResultGrid.Columns["Gubun"];
                    dtProcResultGrid.PrimaryKey = dcProcResult;

                    for (int i = 0; i < dtRtn.Rows.Count; i++)
                    {
                        //LotCount
                        DataRow Lotrow = dtProcResultGrid.NewRow();
                        Lotrow["InspectTypeName"] = dtRtn.Rows[i]["InspectTypeName"];
                        Lotrow["InspectItemName"] = dtRtn.Rows[i]["InspectItemName"];
                        Lotrow["Gubun"] = "검사Lot";
                        Lotrow["D01"] = string.Format("{0:n0}", dtRtn.Rows[i]["D01LotCount"]);
                        Lotrow["D02"] = string.Format("{0:n0}", dtRtn.Rows[i]["D02LotCount"]);
                        Lotrow["D03"] = string.Format("{0:n0}", dtRtn.Rows[i]["D03LotCount"]);
                        Lotrow["D04"] = string.Format("{0:n0}", dtRtn.Rows[i]["D04LotCount"]);
                        Lotrow["D05"] = string.Format("{0:n0}", dtRtn.Rows[i]["D05LotCount"]);
                        Lotrow["D06"] = string.Format("{0:n0}", dtRtn.Rows[i]["D06LotCount"]);
                        Lotrow["D07"] = string.Format("{0:n0}", dtRtn.Rows[i]["D07LotCount"]);
                        Lotrow["D08"] = string.Format("{0:n0}", dtRtn.Rows[i]["D08LotCount"]);
                        Lotrow["D09"] = string.Format("{0:n0}", dtRtn.Rows[i]["D09LotCount"]);
                        Lotrow["D10"] = string.Format("{0:n0}", dtRtn.Rows[i]["D10LotCount"]);

                        Lotrow["Q1"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q1LotCount"]);
                        Lotrow["Q2"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q2LotCount"]);
                        Lotrow["Q3"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q3LotCount"]);
                        Lotrow["Q4"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q4LotCount"]);
                        Lotrow["FirHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["FirHalfLotCount"]);
                        Lotrow["SecHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["SecHalfLotCount"]);
                        Lotrow["Total"] = string.Format("{0:n0}", dtRtn.Rows[i]["TLotCount"]);

                        //FailLot
                        DataRow FailLotRow = dtProcResultGrid.NewRow();
                        FailLotRow["InspectTypeName"] = dtRtn.Rows[i]["InspectTypeName"];
                        FailLotRow["InspectItemName"] = dtRtn.Rows[i]["InspectItemName"];
                        FailLotRow["Gubun"] = "불량Lot";

                        FailLotRow["D01"] = string.Format("{0:n0}", dtRtn.Rows[i]["D01FailLotCount"]);
                        FailLotRow["D02"] = string.Format("{0:n0}", dtRtn.Rows[i]["D02FailLotCount"]);
                        FailLotRow["D03"] = string.Format("{0:n0}", dtRtn.Rows[i]["D03FailLotCount"]);
                        FailLotRow["D04"] = string.Format("{0:n0}", dtRtn.Rows[i]["D04FailLotCount"]);
                        FailLotRow["D05"] = string.Format("{0:n0}", dtRtn.Rows[i]["D05FailLotCount"]);
                        FailLotRow["D06"] = string.Format("{0:n0}", dtRtn.Rows[i]["D06FailLotCount"]);
                        FailLotRow["D07"] = string.Format("{0:n0}", dtRtn.Rows[i]["D07FailLotCount"]);
                        FailLotRow["D08"] = string.Format("{0:n0}", dtRtn.Rows[i]["D08FailLotCount"]);
                        FailLotRow["D09"] = string.Format("{0:n0}", dtRtn.Rows[i]["D09FailLotCount"]);
                        FailLotRow["D10"] = string.Format("{0:n0}", dtRtn.Rows[i]["D10FailLotCount"]);

                        FailLotRow["Q1"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q1FailLotCount"]);
                        FailLotRow["Q2"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q2FailLotCount"]);
                        FailLotRow["Q3"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q3FailLotCount"]);
                        FailLotRow["Q4"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q4FailLotCount"]);
                        FailLotRow["FirHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["FirHalfFailLotCount"]);
                        FailLotRow["SecHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["SecHalfFailLotCount"]);
                        FailLotRow["Total"] = string.Format("{0:n0}", dtRtn.Rows[i]["TFailLotCount"]);

                        DataRow SampleSizeRow = dtProcResultGrid.NewRow();
                        SampleSizeRow["InspectTypeName"] = dtRtn.Rows[i]["InspectTypeName"];
                        SampleSizeRow["InspectItemName"] = dtRtn.Rows[i]["InspectItemName"];
                        SampleSizeRow["Gubun"] = "시료수";

                        //SampleSizeRow["D01"] = dtRtn.Rows[i]["D01SampleSize"];
                        SampleSizeRow["D01"] = string.Format("{0:n0}", dtRtn.Rows[i]["D01SampleSize"]);
                        SampleSizeRow["D02"] = string.Format("{0:n0}", dtRtn.Rows[i]["D02SampleSize"]);
                        SampleSizeRow["D03"] = string.Format("{0:n0}", dtRtn.Rows[i]["D03SampleSize"]);
                        SampleSizeRow["D04"] = string.Format("{0:n0}", dtRtn.Rows[i]["D04SampleSize"]);
                        SampleSizeRow["D05"] = string.Format("{0:n0}", dtRtn.Rows[i]["D05SampleSize"]);
                        SampleSizeRow["D06"] = string.Format("{0:n0}", dtRtn.Rows[i]["D06SampleSize"]);
                        SampleSizeRow["D07"] = string.Format("{0:n0}", dtRtn.Rows[i]["D07SampleSize"]);
                        SampleSizeRow["D08"] = string.Format("{0:n0}", dtRtn.Rows[i]["D08SampleSize"]);
                        SampleSizeRow["D09"] = string.Format("{0:n0}", dtRtn.Rows[i]["D09SampleSize"]);
                        SampleSizeRow["D10"] = string.Format("{0:n0}", dtRtn.Rows[i]["D10SampleSize"]);

                        SampleSizeRow["Q1"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q1SampleSize"]);
                        SampleSizeRow["Q2"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q2SampleSize"]);
                        SampleSizeRow["Q3"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q3SampleSize"]);
                        SampleSizeRow["Q4"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q4SampleSize"]);

                        SampleSizeRow["FirHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["FirHalfSampleSize"]);
                        SampleSizeRow["SecHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["SecHalfSampleSize"]);
                        //SampleSizeRow["Total"] = dtRtn.Rows[i]["TSampleSize"];
                        SampleSizeRow["Total"] = string.Format("{0:n0}", dtRtn.Rows[i]["TSampleSize"]);

                        DataRow FaultQtyRow = dtProcResultGrid.NewRow();
                        FaultQtyRow["InspectTypeName"] = dtRtn.Rows[i]["InspectTypeName"];
                        FaultQtyRow["InspectItemName"] = dtRtn.Rows[i]["InspectItemName"];
                        FaultQtyRow["Gubun"] = "불량수";

                        FaultQtyRow["D01"] = string.Format("{0:n0}", dtRtn.Rows[i]["D01FaultQty"]);
                        FaultQtyRow["D02"] = string.Format("{0:n0}", dtRtn.Rows[i]["D02FaultQty"]);
                        FaultQtyRow["D03"] = string.Format("{0:n0}", dtRtn.Rows[i]["D03FaultQty"]);
                        FaultQtyRow["D04"] = string.Format("{0:n0}", dtRtn.Rows[i]["D04FaultQty"]);
                        FaultQtyRow["D05"] = string.Format("{0:n0}", dtRtn.Rows[i]["D05FaultQty"]);
                        FaultQtyRow["D06"] = string.Format("{0:n0}", dtRtn.Rows[i]["D06FaultQty"]);
                        FaultQtyRow["D07"] = string.Format("{0:n0}", dtRtn.Rows[i]["D07FaultQty"]);
                        FaultQtyRow["D08"] = string.Format("{0:n0}", dtRtn.Rows[i]["D08FaultQty"]);
                        FaultQtyRow["D09"] = string.Format("{0:n0}", dtRtn.Rows[i]["D09FaultQty"]);
                        FaultQtyRow["D10"] = string.Format("{0:n0}", dtRtn.Rows[i]["D10FaultQty"]);

                        FaultQtyRow["Q1"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q1FaultQty"]);
                        FaultQtyRow["Q2"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q2FaultQty"]);
                        FaultQtyRow["Q3"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q3FaultQty"]);
                        FaultQtyRow["Q4"] = string.Format("{0:n0}", dtRtn.Rows[i]["Q4FaultQty"]);

                        FaultQtyRow["FirHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["FirHalfFaultQty"]);
                        FaultQtyRow["SecHalf"] = string.Format("{0:n0}", dtRtn.Rows[i]["SecHalfFaultQty"]);
                        //FaultQtyRow["Total"] = dtRtn.Rows[i]["TFaultQty"];
                        FaultQtyRow["Total"] = string.Format("{0:n0}", dtRtn.Rows[i]["TFaultQty"]);

                        DataRow FaultRateRow = dtProcResultGrid.NewRow();

                        FaultRateRow["InspectTypeName"] = dtRtn.Rows[i]["InspectTypeName"];
                        FaultRateRow["InspectItemName"] = dtRtn.Rows[i]["InspectItemName"];
                        FaultRateRow["Gubun"] = "실적(ppm)";

                        FaultRateRow["D01"] = dtRtn.Rows[i]["D01FaultRate"];
                        FaultRateRow["D02"] = dtRtn.Rows[i]["D02FaultRate"];
                        FaultRateRow["D03"] = dtRtn.Rows[i]["D03FaultRate"];
                        FaultRateRow["D04"] = dtRtn.Rows[i]["D04FaultRate"];
                        FaultRateRow["D05"] = dtRtn.Rows[i]["D05FaultRate"];
                        FaultRateRow["D06"] = dtRtn.Rows[i]["D06FaultRate"];
                        FaultRateRow["D07"] = dtRtn.Rows[i]["D07FaultRate"];
                        FaultRateRow["D08"] = dtRtn.Rows[i]["D08FaultRate"];
                        FaultRateRow["D09"] = dtRtn.Rows[i]["D09FaultRate"];
                        FaultRateRow["D10"] = dtRtn.Rows[i]["D10FaultRate"];

                        FaultRateRow["Q1"] = dtRtn.Rows[i]["Q1FaultRate"];
                        FaultRateRow["Q2"] = dtRtn.Rows[i]["Q2FaultRate"];
                        FaultRateRow["Q3"] = dtRtn.Rows[i]["Q3FaultRate"];
                        FaultRateRow["Q4"] = dtRtn.Rows[i]["Q4FaultRate"];

                        FaultRateRow["FirHalf"] = dtRtn.Rows[i]["FirHalfFaultRate"];
                        FaultRateRow["SecHalf"] = dtRtn.Rows[i]["SecHalfFaultRate"];
                        FaultRateRow["Total"] = dtRtn.Rows[i]["TFaultRate"];

                        for (int j = 11; j <= intMonthDay; j++)
                        {
                            Lotrow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn.Rows[i]["D" + j.ToString() + "LotCount"]);
                            FailLotRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn.Rows[i]["D" + j.ToString() + "FailLotCount"]);
                            SampleSizeRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn.Rows[i]["D" + j.ToString() + "SampleSize"]);
                            FaultQtyRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn.Rows[i]["D" + j.ToString() + "FaultQty"]);
                            FaultRateRow["D" + j.ToString()] = dtRtn.Rows[i]["D" + j.ToString() + "FaultRate"];
                        }

                        dtProcResultGrid.Rows.Add(Lotrow);
                        dtProcResultGrid.Rows.Add(FailLotRow);
                        dtProcResultGrid.Rows.Add(SampleSizeRow);
                        dtProcResultGrid.Rows.Add(FaultQtyRow);
                        dtProcResultGrid.Rows.Add(FaultRateRow);
                    }
                    this.uGridProc.DataSource = null;
                    this.uGridProc.ResetDisplayLayout();
                    this.uGridProc.Layouts.Clear();

                    ChangeGridColumn(0);

                    uGridProc.DataSource = dtProcResultGrid;
                    uGridProc.DataBind();

                    //색변경
                    for (int i = 0; i < uGridProc.Rows.Count; i++)
                    {
                        if (uGridProc.Rows[i].Cells["Gubun"].Text == "실적(ppm)")
                        {
                            for (int j = 1; j < uGridProc.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridProc.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridProc.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                        this.uGridProc.Rows[i].Cells["Total"].Appearance.BackColor = Color.MistyRose;
                    }

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    #endregion
                }
            }
            catch(Exception ex)
            {}
            finally
            {}
        }

        public void mfSave()
        {

        }
        public void mfPrint()
        {

        }

        public void mfExcel()
        {

        }

        public void mfDelete()
        {

        }
        #endregion

        #region Event
        /// <summary>
        /// 공장콤보 변경시 공정Type콤보 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                System.Data.DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                string strYear = this.uTextSearchYear.Text;
                string strPastYear = Convert.ToString(Convert.ToInt32(strYear) - 1);
                string strMonth = this.uComboSearchMonth.Value.ToString();
                int intMontDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                wGrid.mfInitGeneralGrid(this.uGridProc, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "InspectTypeName", "검사유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "MonthlyResult", strYear + "년" + strMonth + "월 실적", 4, 0, 10, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
                int i = 0;
                for (i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
                }

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "QuarterResult", strPastYear + "년 분기별 실적", 14 + i, 0, 4, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "HarfResult", strPastYear +"년 반기별 실적", 18 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridProc, intBandIndex, "TotalResult", "Total실적", 21 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridProc, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);



                this.uGridProc.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProc.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;
                this.uGridProc.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //그리드 초기화
        private void uGridProc_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                //e.Layout.Bands[0].Columns["D01"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D01"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D01"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D02"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D02"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D02"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D03"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D03"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D03"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D04"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D04"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D04"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D05"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D05"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D05"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D06"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D06"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D06"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D07"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D07"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D07"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D08"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D08"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D08"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D09"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D09"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D09"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["D10"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["D10"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["D10"].CellAppearance.ForeColor = Color.Black;
                for (int i = 11; i < 32; i++)
                {
                    //e.Layout.Bands[0].Columns["D" + i].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    //e.Layout.Bands[0].Columns["D" + i].MaskInput = "nnn,nnn,nnn";
                    e.Layout.Bands[0].Columns["D" + i].CellAppearance.ForeColor = Color.Black;
                }
                //e.Layout.Bands[0].Columns["Q1"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["Q1"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["Q1"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["Q2"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["Q2"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["Q2"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["Q3"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["Q3"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["Q3"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["Q4"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["Q4"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["Q4"].CellAppearance.ForeColor = Color.Black;

                //e.Layout.Bands[0].Columns["FirHalf"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["FirHalf"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["FirHalf"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["SecHalf"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["SecHalf"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["SecHalf"].CellAppearance.ForeColor = Color.Black;
                //e.Layout.Bands[0].Columns["Total"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                //e.Layout.Bands[0].Columns["Total"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["Total"].CellAppearance.ForeColor = Color.Black;

                //for (int i = 0; i < this.uGridProc.Rows.Count; i++)
                //{
                //    if (this.uGridProc.Rows[i].Cells["Gubun"].Value.ToString().Equals("실적(ppm)"))
                //    {
                       
                //    }
                //}
                this.uGridProc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
    }
}
