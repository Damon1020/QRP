﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0074.cs                                         */
/* 프로그램명   : CCS 종합현황                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0074 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0074()
        {
            InitializeComponent();
        }

        private void frmSTA0074_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0074_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0074_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("CCS 종합현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            SetToolAuth();
            InitEtc();
            InitTab();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridCustomerList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridCustomerList);
                }
                if (this.uGridProcTypeList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridProcTypeList);
                }
                if (this.uGridCustomerList.Rows.Count.Equals(0) && this.uGridProcTypeList.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 조회조건 체크
                if (string.IsNullOrEmpty(this.uComboSearchPlantCode.Value.ToString()) || this.uComboSearchPlantCode.Value == DBNull.Value)
                {

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlantCode.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Length < 4)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001107", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(this.uComboSearchType.Value.ToString()) || this.uComboSearchType.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001105", Infragistics.Win.HAlign.Center);

                    this.uComboSearchType.DropDown();
                    return;
                }

                // 변수 설정
                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                string strYear = this.uTextSearchYear.Text;
                string strSearchType = this.uComboSearchType.Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 조회

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                DataTable dtProcTypeList = clsReport.mfReadINSProcInspect_frmSTA0074_D1_PSTS(strPlantCode, strYear, strSearchType, m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerList = clsReport.mfReadINSProcInspect_frmSTA0074_D2_PSTS(strPlantCode, strYear, strSearchType, m_resSys.GetString("SYS_LANG"));

                // 그리드 재설정 메소드 호출
                if (strSearchType.Equals("M"))
                    InitGrid_Month();
                else if (strSearchType.Equals("Q"))
                    InitGrid_Quater();

                this.uGridProcTypeList.DataSource = dtProcTypeList;
                this.uGridProcTypeList.DataBind();

                this.uGridCustomerList.DataSource = dtCustomerList;
                this.uGridCustomerList.DataBind();

                #endregion

                #region 그리드 색상변경


                if (this.uGridProcTypeList.Rows.Count > 0)
                {
                    // 공정Type별

                    int intLastRowIndex = this.uGridProcTypeList.Rows.Count - 1;
                    for (int i = 0; i < this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; i++)
                    {
                        this.uGridProcTypeList.Rows[intLastRowIndex].Cells[i].Appearance.BackColor = Color.MistyRose;
                        this.uGridProcTypeList.Rows[intLastRowIndex].Cells[i].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                    }
                }

                if (this.uGridCustomerList.Rows.Count > 0)
                {
                    // 고객사별
                    int intLastRowIndex = this.uGridCustomerList.Rows.Count - 1;
                    this.uGridCustomerList.Rows[intLastRowIndex].Cells["DETAILPROCESSOPERATIONTYPE"].SetValue(string.Empty, false);

                    for (int i = 0; i < this.uGridCustomerList.Rows.Count; i++)
                    {
                        if (this.uGridCustomerList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Equals("TOTAL"))
                        {
                            for (int j = 1; j < uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.BackColor = Color.DeepSkyBlue;
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                        else if (this.uGridCustomerList.Rows[i].Cells["CustomerName"].Value.ToString().Equals("ZZZGRNAD  TOTAL"))
                        {
                            this.uGridCustomerList.Rows[i].Cells["CustomerName"].Value = "GRNAD  TOTAL";
                            for (int j = 0; j < uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                        else if (i.Equals(intLastRowIndex))
                        {
                            for (int j = 0; j < uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                    }
                }

                #endregion

                // 차트 초기화

                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);

                // Legend Clear
                this.uChart.CompositeChart.Legends.Clear();

                if (this.uGridProcTypeList.Rows.Count > 0)
                {
                    QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridProcTypeList, 0);
                    wGrid.mfSetAutoResizeColWidth(this.uGridCustomerList, 0);
                    // Chart Draw 메소드 호출
                    DrawChart();
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchType, "조회구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                    , m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

                // 조회선택 콤보박스 설정
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                string strLang = m_resSys.GetString("SYS_LANG");
                string strMonth = "";
                string strQua = "";
                if (strLang.Equals("KOR"))
                {
                    strMonth = "월별";
                    strQua = "분기별";
                }
                else if (strLang.Equals("CHN"))
                {
                    strMonth = "月别";
                    strQua = "季度别";
                }
                else if (strLang.Equals("ENG"))
                {
                    strMonth = "월별";
                    strQua = "분기별";
                }

                arrKey.Add("M");
                arrKey.Add("Q");

                arrValue.Add(strMonth);
                arrValue.Add(strQua);

                wCombo.mfSetComboEditor(this.uComboSearchType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "M", arrKey, arrValue);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                this.uGridProcTypeList.DisplayLayout.GroupByBox.Hidden = true;
                this.uGridCustomerList.DisplayLayout.GroupByBox.Hidden = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab 초기화

        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabInfo, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                                            , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                                            , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                                            , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화

        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 조회연도 숫자만 입력가능하게 하는 이벤트

        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정 Type 별 그리드 더블클릭시 고객사 그리드로 이동
        private void uGridProcTypeList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                this.uTabInfo.Tabs[1].Selected = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그리드 재정의 메소드


        /// <summary>
        /// Grid 컬럼설정 메소드

        /// </summary>
        private void InitGrid_Month()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //////// 바운드 컬럼 삭제 안됨;;;;;
                //////for (int i = 0; i < this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; i++)
                //////{
                //////    this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Remove(i);
                //////}

                //////for (int i = 0; i < this.uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; i++)
                //////{
                //////    this.uGridCustomerList.DisplayLayout.Bands[0].Columns.Remove(i);
                //////}

                // 그리드 설정 삭제
                this.uGridProcTypeList.DataSource = null;
                this.uGridProcTypeList.ResetDisplayLayout();
                this.uGridProcTypeList.Layouts.Clear();

                this.uGridCustomerList.DataSource = null;
                this.uGridCustomerList.ResetDisplayLayout();
                this.uGridCustomerList.Layouts.Clear();


                string[] strColumns = GirdLang().Split(',');
                //고객사,공정Type,제품구분,월,의뢰수,불량수,합격률(%)
                
                #region 공정 Type

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "DETAILPROCESSOPERATIONTYPE", strColumns[1], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);//공정Type

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "PRODUCTACTIONTYPE", strColumns[2], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);//제품구분

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth1 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M01", "1" + strColumns[3], 2, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M01RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth1);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M01FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth1);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M01Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth1);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth2 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M02", "2" + strColumns[3], 5, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M02RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth2);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M02FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth2);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M02Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth2);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth3 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M03", "3" + strColumns[3], 8, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M03RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth3);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M03FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth3);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M03Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth3);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth4 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M04", "4" + strColumns[3], 11, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M04RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth4);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M04FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth4);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M04Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth4);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth5 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M05", "5" + strColumns[3], 14, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M05RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth5);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M05FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth5);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M05Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth5);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth6 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M06", "6" + strColumns[3], 17, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M06RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth6);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M06FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth6);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M06Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth6);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth7 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M07", "7" + strColumns[3], 20, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M07RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth7);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M07FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth7);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M07Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth7);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth8 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M08", "8" + strColumns[3], 23, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M08RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth8);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M08FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth8);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M08Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth8);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth9 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M09", "9" + strColumns[3], 26, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M09RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth9);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M09FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth9);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M09Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth9);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth10 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M10", "10" + strColumns[3], 29, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M10RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth10);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M10FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth10);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M10Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth10);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth11 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M11", "11" + strColumns[3], 32, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M11RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth11);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M11FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth11);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M11Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth11);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonth12 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "M12", "12" + strColumns[3], 35, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M12RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth12);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M12FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth12);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "M12Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth12);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTot = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "TOT", "Total", 38, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTRQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "n,nnn", 0, 0, 1, 1, uGroupTot);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTFQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "n,nnn", 1, 0, 1, 1, uGroupTot);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTRate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupTot);//합격률

                // 그리드 편집불가 상태
                this.uGridProcTypeList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                uGroupMonth1.CellAppearance.ForeColor = Color.Black;
                uGroupMonth2.CellAppearance.ForeColor = Color.Black;
                uGroupMonth3.CellAppearance.ForeColor = Color.Black;
                uGroupMonth4.CellAppearance.ForeColor = Color.Black;
                uGroupMonth5.CellAppearance.ForeColor = Color.Black;
                uGroupMonth6.CellAppearance.ForeColor = Color.Black;
                uGroupMonth7.CellAppearance.ForeColor = Color.Black;
                uGroupMonth8.CellAppearance.ForeColor = Color.Black;
                uGroupMonth9.CellAppearance.ForeColor = Color.Black;
                uGroupMonth10.CellAppearance.ForeColor = Color.Black;
                uGroupMonth11.CellAppearance.ForeColor = Color.Black;
                uGroupMonth12.CellAppearance.ForeColor = Color.Black;
                uGroupTot.CellAppearance.ForeColor = Color.Black;

                #endregion

                #region 고객사별

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCustomerList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "CustomerName", strColumns[0], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);//고객사

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "DETAILPROCESSOPERATIONTYPE", strColumns[1], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);//공정Type

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "PRODUCTACTIONTYPE", strColumns[2], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);//제품구분

                uGroupMonth1 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M01", "1" + strColumns[3], 3, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M01RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth1);//"의뢰수"

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M01FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth1);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M01Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth1);//합격률

                uGroupMonth2 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M02", "2" + strColumns[3], 6, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M02RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth2);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M02FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth2);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M02Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth2);//합격률

                uGroupMonth3 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M03", "3" + strColumns[3], 9, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M03RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth3);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M03FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth3);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M03Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth3);//합격률

                uGroupMonth4 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M04", "4" + strColumns[3], 12, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M04RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth4);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M04FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth4);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M04Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth4);//합격률

                uGroupMonth5 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M05", "5" + strColumns[3], 15, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M05RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth5);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M05FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth5);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M05Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth5);//합격률

                uGroupMonth6 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M06", "6" + strColumns[3], 18, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M06RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth6);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M06FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth6);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M06Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth6);//합격률

                uGroupMonth7 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M07", "7" + strColumns[3], 21, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M07RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth7);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M07FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth7);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M07Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth7);//합격률

                uGroupMonth8 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M08", "8" + strColumns[3], 24, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M08RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth8);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M08FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth8);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M08Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth8);//합격률

                uGroupMonth9 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M09", "9" + strColumns[3], 27, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M09RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth9);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M09FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth9);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M09Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth9);//합격률

                uGroupMonth10 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M10", "10" + strColumns[3], 30, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M10RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth10);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M10FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth10);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M10Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth10);//합격률

                uGroupMonth11 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M11", "11" + strColumns[3], 33, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M11RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth11);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M11FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth11);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M11Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth11);//합격률

                uGroupMonth12 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "M12", "12" + strColumns[3], 36, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M12RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupMonth12);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M12FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupMonth12);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M12Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupMonth12);//합격률

                uGroupTot = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "TOT", "Total", 39, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTRQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupTot);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTFQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupTot);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTRate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupTot);//합격률

                // 그리드 편집불가 상태
                this.uGridCustomerList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                uGroupMonth1.CellAppearance.ForeColor = Color.Black;
                uGroupMonth2.CellAppearance.ForeColor = Color.Black;
                uGroupMonth3.CellAppearance.ForeColor = Color.Black;
                uGroupMonth4.CellAppearance.ForeColor = Color.Black;
                uGroupMonth5.CellAppearance.ForeColor = Color.Black;
                uGroupMonth6.CellAppearance.ForeColor = Color.Black;
                uGroupMonth7.CellAppearance.ForeColor = Color.Black;
                uGroupMonth8.CellAppearance.ForeColor = Color.Black;
                uGroupMonth9.CellAppearance.ForeColor = Color.Black;
                uGroupMonth10.CellAppearance.ForeColor = Color.Black;
                uGroupMonth11.CellAppearance.ForeColor = Color.Black;
                uGroupMonth12.CellAppearance.ForeColor = Color.Black;
                uGroupTot.CellAppearance.ForeColor = Color.Black;

                #endregion

                #region Total 컬럼 색상, 폰트설정

                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                #endregion

               

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 컬럼설정 메소드

        /// </summary>
        private void InitGrid_Quater()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //////// 바운드 컬럼 삭제 안됨;;;;;
                ////for (int i = 0; i < this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; i++)
                ////{
                ////    this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Remove(i);
                ////}

                ////for (int i = 0; i < this.uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; i++)
                ////{
                ////    this.uGridCustomerList.DisplayLayout.Bands[0].Columns.Remove(i);
                ////}

                this.uGridProcTypeList.DataSource = null;
                this.uGridProcTypeList.ResetDisplayLayout();
                this.uGridProcTypeList.Layouts.Clear();

                this.uGridCustomerList.DataSource = null;
                this.uGridCustomerList.ResetDisplayLayout();
                this.uGridCustomerList.Layouts.Clear();

                string[] strColumns = GirdLang().Split(',');
                //고객사,공정Type,제품구분,월,의뢰수,불량수,합격률(%)

                #region 공정 Type

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "DETAILPROCESSOPERATIONTYPE", strColumns[1], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);//공정Type

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "PRODUCTACTIONTYPE", strColumns[2], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);//제품구분

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuater1 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "Q01", "1Q", 2, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q01RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater1);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q01FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater1);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q01Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater1);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuater2 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "Q02", "2Q", 5, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q02RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater2);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q02FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater2);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q02Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater2);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuater3 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "Q03", "3Q", 8, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q03RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater3);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q03FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater3);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q03Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater3);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuater4 = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "Q04", "4Q", 11, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q04RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater4);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q04FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater4);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "Q04Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater4);//합격률

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTot = wGrid.mfSetGridGroup(this.uGridProcTypeList, 0, "TOT", "Total", 14, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTRQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupTot);//의뢰수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTFQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupTot);//불량수

                wGrid.mfSetGridColumn(this.uGridProcTypeList, 0, "TOTRate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupTot);//합격률

                // 그리드 편집불가 상태
                this.uGridProcTypeList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridProcTypeList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                uGroupQuater1.CellAppearance.ForeColor = Color.Black;
                uGroupQuater2.CellAppearance.ForeColor = Color.Black;
                uGroupQuater3.CellAppearance.ForeColor = Color.Black;
                uGroupQuater4.CellAppearance.ForeColor = Color.Black;
                uGroupTot.CellAppearance.ForeColor = Color.Black;

                #endregion

                #region 고객사별

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCustomerList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "CustomerName", strColumns[0], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);//고객사

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "DETAILPROCESSOPERATIONTYPE", strColumns[1], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);//공정Type

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "PRODUCTACTIONTYPE", strColumns[2], true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);//제품구분

                uGroupQuater1 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "Q01", "1Q", 3, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q01RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater1);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q01FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater1);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q01Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater1);//합격률

                uGroupQuater2 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "Q02", "2Q", 6, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q02RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater2);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q02FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater2);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q02Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater2);//합격률

                uGroupQuater3 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "Q03", "3Q", 9, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q03RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater3);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q03FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater3);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q03Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater3);//합격률

                uGroupQuater4 = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "Q04", "4Q", 12, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q04RQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupQuater4);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q04FQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupQuater4);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q04Rate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupQuater4);//합격률

                uGroupTot = wGrid.mfSetGridGroup(this.uGridCustomerList, 0, "TOT", "Total", 15, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTRQty", strColumns[4], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 0, 0, 1, 1, uGroupTot);//의뢰수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTFQty", strColumns[5], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn", "", 1, 0, 1, 1, uGroupTot);//불량수

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "TOTRate", strColumns[6], false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn.nnn", "", 2, 0, 1, 1, uGroupTot);//합격률

                // 그리드 편집불가 상태
                this.uGridCustomerList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                uGroupQuater1.CellAppearance.ForeColor = Color.Black;
                uGroupQuater2.CellAppearance.ForeColor = Color.Black;
                uGroupQuater3.CellAppearance.ForeColor = Color.Black;
                uGroupQuater4.CellAppearance.ForeColor = Color.Black;
                uGroupTot.CellAppearance.ForeColor = Color.Black;

                #endregion

                #region Total 컬럼 색상, 폰트설정

                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTFQty"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["TOTRate"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                #endregion

               
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 차트 그리는 메소드


        private void DrawChart()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region DataTable 설정

                // Line Chart 용 컬럼생성
                System.Data.DataTable dtLineChart = new System.Data.DataTable("LineChart");
                dtLineChart.Columns.Add("Month", typeof(string));
                dtLineChart.Columns.Add("TOTRate", typeof(decimal));

                // Data 저장

                DataRow drRow;
                int intLastRow = this.uGridProcTypeList.Rows.Count - 1;
                for (int j = 0; j < this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 3; j++)
                {
                    if (this.uGridProcTypeList.DisplayLayout.Bands[0].Columns[j].Key.Contains("Rate"))
                    {
                        drRow = dtLineChart.NewRow();
                        string strGroupKey = this.uGridProcTypeList.DisplayLayout.Bands[0].Columns[j].Header.Group.Key;
                        drRow["Month"] = this.uGridProcTypeList.DisplayLayout.Bands[0].Groups[strGroupKey].Header.Caption;
                        drRow["TOTRate"] = this.uGridProcTypeList.Rows[intLastRow].Cells[j].Value;
                        dtLineChart.Rows.Add(drRow);
                    }
                }

                // Column Chart 용 컬럼 생성
                System.Data.DataTable dtColumnChart = new System.Data.DataTable("ColumnChart");
                dtColumnChart.Columns.Add("Month", typeof(string));
                for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
                {
                    dtColumnChart.Columns.Add(this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text + "_" + 
                                            this.uGridProcTypeList.Rows[i].Cells["PRODUCTACTIONTYPE"].Text, typeof(Int32));
                }

                // Data 저장

                for (int j = 0; j < this.uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 3; j++)
                {
                    if (this.uGridProcTypeList.DisplayLayout.Bands[0].Columns[j].Key.Contains("RQty"))
                    {
                        drRow = dtColumnChart.NewRow();
                        string strGroupKey = this.uGridProcTypeList.DisplayLayout.Bands[0].Columns[j].Header.Group.Key;
                        drRow["Month"] = this.uGridProcTypeList.DisplayLayout.Bands[0].Groups[strGroupKey].Header.Caption;
                        for (int i = 0; i < this.uGridProcTypeList.Rows.Count - 1; i++)
                        {
                            drRow[this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text + "_" +
                                this.uGridProcTypeList.Rows[i].Cells["PRODUCTACTIONTYPE"].Text] = this.uGridProcTypeList.Rows[i].Cells[j].Value;
                        }
                        dtColumnChart.Rows.Add(drRow);
                    }
                }

                #endregion

                #region 차트 그리기


                // 차트 유형 지정

                this.uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Composite;

                Infragistics.UltraChart.Resources.Appearance.ChartArea chartArea = new Infragistics.UltraChart.Resources.Appearance.ChartArea();
                this.uChart.CompositeChart.ChartAreas.Add(chartArea);

                #region Column Chart

                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisColumn = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisColumn.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                xAxisColumn.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisColumn.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
                xAxisColumn.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisColumn.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisColumn.Extent = 25;
                xAxisColumn.Visible = true;
                xAxisColumn.Labels.Visible = false;
                xAxisColumn.Labels.SeriesLabels.Visible = true;

                // Y축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem yAxisColumn = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                yAxisColumn.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y2_Axis;
                yAxisColumn.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                yAxisColumn.RangeType = Infragistics.UltraChart.Shared.Styles.AxisRangeType.Automatic;
                yAxisColumn.NumericAxisType = Infragistics.UltraChart.Shared.Styles.NumericAxisType.Linear;
                //yAxisColumn.Extent = 150;
                yAxisColumn.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                yAxisColumn.Labels.ItemFormatString = "<DATA_VALUE:#,##0.#>";
                yAxisColumn.Visible = true;

                chartArea.Axes.Add(xAxisColumn);
                chartArea.Axes.Add(yAxisColumn);

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance stackColLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                stackColLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart;
                stackColLayer.ChartArea = chartArea;
                stackColLayer.AxisX = xAxisColumn;
                stackColLayer.AxisY = yAxisColumn;
                this.uChart.CompositeChart.ChartLayers.Add(stackColLayer);

                // Data 설정
                for (int i = 1; i < dtColumnChart.Columns.Count; i++)
                {
                    Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesCol = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                    seriesCol.Label = dtColumnChart.Columns[i].ColumnName;
                    seriesCol.Data.DataSource = dtColumnChart;
                    seriesCol.Data.LabelColumn = "Month";
                    seriesCol.Data.ValueColumn = dtColumnChart.Columns[i].ColumnName;
                    this.uChart.CompositeChart.Series.Add(seriesCol);
                    stackColLayer.SwapRowsAndColumns = true;
                    stackColLayer.Series.Add(seriesCol);
                }

                #endregion

                #region Line Chart
                
                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisLine = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisLine.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X2_Axis;
                xAxisLine.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisLine.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisLine.MajorGridLines.Visible = false;
                xAxisLine.MinorGridLines.Visible = false;
                xAxisLine.Margin.Far.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                xAxisLine.Margin.Near.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                if (this.uComboSearchType.Value.ToString().Equals("Q"))
                {
                    xAxisLine.Margin.Far.Value = 12;
                    xAxisLine.Margin.Near.Value = 12;
                }
                else if (this.uComboSearchType.Value.ToString().Equals("M"))
                {
                    xAxisLine.Margin.Far.Value = 4;
                    xAxisLine.Margin.Near.Value = 4;
                }
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Visible = false;

                // Y축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem yAxisLine = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                yAxisLine.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                yAxisLine.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                yAxisLine.RangeType = Infragistics.UltraChart.Shared.Styles.AxisRangeType.Automatic;
                yAxisLine.NumericAxisType = Infragistics.UltraChart.Shared.Styles.NumericAxisType.Linear;
                yAxisLine.Extent = 150;
                yAxisLine.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                yAxisLine.Labels.ItemFormatString = "<DATA_VALUE:##0.###>";
                yAxisLine.Visible = true;
                yAxisLine.RangeMax = 100;
                yAxisLine.RangeMin = 0;

                chartArea.Axes.Add(xAxisLine);
                chartArea.Axes.Add(yAxisLine);

                string strLang = m_resSys.GetString("SYS_LANG");

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance lineLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                lineLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                lineLayer.ChartArea = chartArea;
                lineLayer.AxisX = xAxisLine;
                lineLayer.AxisY = yAxisLine;
                this.uChart.CompositeChart.ChartLayers.Add(lineLayer);

                WinMessageBox msg = new WinMessageBox();
                // Data 설정
                Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesLine = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                seriesLine.Label = msg.GetMessge_Text("M000793", strLang);
                seriesLine.Data.DataSource = dtLineChart;
                seriesLine.Data.LabelColumn = "Month";
                seriesLine.Data.ValueColumn = "TOTRate";
                seriesLine.PEs.Add(new Infragistics.UltraChart.Resources.Appearance.PaintElement(Color.Salmon));
                this.uChart.CompositeChart.Series.Add(seriesLine);
                lineLayer.Series.Add(seriesLine);
                
                Infragistics.UltraChart.Resources.Appearance.LineChartAppearance appLineChart = new Infragistics.UltraChart.Resources.Appearance.LineChartAppearance();

                // 선모양 설정
                Infragistics.UltraChart.Resources.Appearance.LineAppearance appLine = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                appLine.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                appLine.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Small;
                appLine.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                appLine.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;
                appLine.LineStyle.StartStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;

                // Text 설정
                Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[] appChartText = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[12];
                for (int i = 0; i < appChartText.Length; i++)
                {
                    appChartText[i] = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance();
                    appChartText[i].HorizontalAlign = StringAlignment.Center;
                    appChartText[i].VerticalAlign = StringAlignment.Far;
                    appChartText[i].FontColor = Color.Black;
                    appChartText[i].ItemFormatString = "<DATA_VALUE:##0.###>";
                    appChartText[i].Visible = true;
                    appChartText[i].Row = 0;
                    appChartText[i].Column = i;
                    Font font = new Font(m_resSys.GetString("SYS_FONTNAME"), 8, FontStyle.Bold);
                    appChartText[i].ChartTextFont = font;
                    appLineChart.ChartText.Add(appChartText[i]);
                }
                appLineChart.LineAppearances.Add(appLine);
                lineLayer.ChartTypeAppearance = appLineChart;

                #endregion

                #region 범주 설정

                // Legend 설정
                Infragistics.UltraChart.Resources.Appearance.CompositeLegend Legend = new Infragistics.UltraChart.Resources.Appearance.CompositeLegend();
                Legend.ChartLayers.Add(stackColLayer);
                Legend.ChartLayers.Add(lineLayer);
                Legend.Bounds = new Rectangle(1, 5, 10, 90);
                Legend.BoundsMeasureType = Infragistics.UltraChart.Shared.Styles.MeasureType.Percentage;
                Legend.PE.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.Gradient;
                Legend.PE.FillGradientStyle = Infragistics.UltraChart.Shared.Styles.GradientStyle.ForwardDiagonal;
                Legend.PE.Fill = Color.CornflowerBlue;
                Legend.PE.FillStopColor = Color.Transparent;
                Legend.Border.CornerRadius = 10;
                Legend.Border.Thickness = 0;
                this.uChart.CompositeChart.Legends.Add(Legend);

                #endregion

                // Tooltip 설정
                this.uChart.Tooltips.Display = Infragistics.UltraChart.Shared.Styles.TooltipDisplay.MouseMove;
                this.uChart.Tooltips.Format = Infragistics.UltraChart.Shared.Styles.TooltipStyle.LabelPlusDataValue;

                #endregion

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 고객사,공정Type,제품구분,월,의뢰수,불량수,합격률(%)
        /// </summary>
        /// <returns></returns>
        private string GirdLang()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";
                
                if (strLang.Equals("KOR"))
                    strName = "고객사,공정Type,제품구분,월,의뢰수,불량수,합격률(%)";
                if (strLang.Equals("CHN"))
                    strName = "客户社,工程Type,产品区分,月,委托数,不良数,合格率(%)";
                else
                    strName = "고객사,공정Type,제품구분,월,의뢰수,불량수,합격률(%)";

                return strName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
    }
}
