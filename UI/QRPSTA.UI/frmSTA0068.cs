﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0068.cs                                         */
/* 프로그램명   : 공정품질불량률:PPM                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-12-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0068 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0068()
        {
            InitializeComponent();
        }


        private void frmSTA0068_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0068_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("공정품질불량률 : PPM", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        private void frmSTA0068_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드


        // Label 초기화

        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchInspectType, "검사유형", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcInspectType, "공정검사구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackageGroup1, "패키지그룹_1", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 콤보박스 초기화 
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                //검색조건 - 고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer"); // 2
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer(); // 3
                brwChannel.mfCredentials(clsCustomer); // 4
                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "CustomerCode", "CustomerName", dtCustomer);

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);


                //검색조건 - 공정검사구분 콤보박스 (공통코드에서 가져오기)
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4
                DataTable dtProcInspectGubun = clsComCode.mfReadCommonCode("C0020", m_resSys.GetString("SYS_LANG")); // 5
                wCombo.mfSetComboEditor(this.uComboSearchProcInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "ComCode", "ComCodeName", dtProcInspectGubun);

                //검색조건 - PackageGroup1 콤보박스
                DataTable dtPackageGroup1 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "2", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComoSearchPackageGroup1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "PackageGroupCode", "PackageGroupName", dtPackageGroup1);


                //검색조건 - 패키지그룹 콤보박스
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "Package", "ComboName", dtPackage);

                //검색조건 - 공정Type 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process"); // 2
                QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process(); // 3
                brwChannel.mfCredentials(clsProc); // 4
                DataTable dtProc = clsProc.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));
                wCombo.mfSetComboEditor(this.uComboSearchDetailProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "DetailProcessOperationType", "ComboName", dtProc);

                //검색조건 - 검사유형 콤보박스
                string strInspectGroupCode = "IG02";
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(iType);
                DataTable dtInspectType = iType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "전체", "InspectTypeCode", "InspectTypeName"
                    , dtInspectType);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화

        private void InitEtc()
        {
            try
            {
                this.uDateSearchInspectDateFrom.Value = DateTime.Now.ToString("yyyy-MM-dd").Substring(0,8) + "01";
                this.uDateSearchInspectDateTo.Value = DateTime.Now.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Grid 초기화

        private void InitGrid()
        {
            try
            {
                this.uGridProcPPMList.DisplayLayout.GroupByBox.Hidden = true;
                //// SystemInfo ResourceSet
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinGrid wGrid = new WinGrid();
                
                //// 일반설정
                //wGrid.mfInitGeneralGrid(this.uGridProcPPMList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                //    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                //    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                //    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                ////// 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "ProductActionType", "제품구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "ProcInspectType", "공정검사코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "ProcInspectTypeName", "공정검사구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "PACKAGEGROUP_1", "PackageGroup", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////// Set PontSize
                ////this.uGridProcPPMList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                ////this.uGridProcPPMList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //메뉴별 권한정보 그리드 기본설정
                wGrid.mfInitGeneralGrid(this.uGridProcPPMList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //맨앞에 선택 컬럼 제거
                uGridProcPPMList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

                // 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "CustomerCode", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "ProductActionType", "제품구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "ProcInspectType", "공정검사코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "ProcInspectTypeName", "공정검사구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "PACKAGEGROUP_1", "PackageGroup", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "PACKAGE", "Package", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "GUBUNSEQ", "GUBUNSEQ", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcPPMList, 0, "GUBUN", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //총계 컬럼 추가.//
                wGrid.mfSetGridColumn(this.uGridProcPPMList, 0, "Total", "총계", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //총계를 구한다.//
                for (int i = 0; i < this.uGridProcPPMList.Rows.Count; i++)
                {
                    string strGubun = this.uGridProcPPMList.Rows[i].Cells["GUBUN"].Value.ToString();
                    double dblSum = 0;
                    //각 일자에 대한 총계를 구하여, 총계 열에 보여준다.
                    if (strGubun == "Total LOT" || strGubun == "Scrap LOT" || strGubun == "Total CHIP" || strGubun == "Sample CHIP" || strGubun == "Defect CHIP")
                    {
                        for (int j = this.uGridProcPPMList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; j < this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1; j++)
                        {
                            if (this.uGridProcPPMList.Rows[i].Cells[j].Value.ToString() != "")
                                dblSum = dblSum + Convert.ToDouble(this.uGridProcPPMList.Rows[i].Cells[j].Value.ToString());
                        }
                        this.uGridProcPPMList.Rows[i].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value = dblSum.ToString();
                    }
                    //LLR은 100* 총계ScrapLot / 총계TotalLot 을 구하여 총계 열에 보여준다.
                    else if (strGubun == "LRR")
                    {
                        double dblTotalLot = Convert.ToDouble(this.uGridProcPPMList.Rows[i - 2].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value.ToString());
                        double dblScrapLot = Convert.ToDouble(this.uGridProcPPMList.Rows[i - 1].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value.ToString());
                        this.uGridProcPPMList.Rows[i].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value = (100 * (dblScrapLot / dblTotalLot)).ToString();
                    }
                    else if (strGubun == "PPM")
                    {
                        double dblTotalChip = Convert.ToDouble(this.uGridProcPPMList.Rows[i - 2].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value.ToString());
                        double dblScrapChip = Convert.ToDouble(this.uGridProcPPMList.Rows[i - 1].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value.ToString());
                        this.uGridProcPPMList.Rows[i].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Value = (1000000 * (dblScrapChip / dblTotalChip)).ToString();
                    }

                }

                for(int i = this.uGridProcPPMList.DisplayLayout.Bands[0].Columns["GUBUN"].Index+1; i < this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    this.uGridProcPPMList.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    this.uGridProcPPMList.DisplayLayout.Bands[0].Columns[i].MaskInput = "n,nnn.nn";
                    this.uGridProcPPMList.DisplayLayout.Bands[0].Columns[i].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                }

                //LRR 및 PPM 행에 대해 색깔 적용
                for (int i = 0; i < this.uGridProcPPMList.Rows.Count; i++)
                {
                    if (this.uGridProcPPMList.Rows[i].Cells["GUBUN"].Value.ToString() == "LRR")
                    {
                        for (int j = this.uGridProcPPMList.DisplayLayout.Bands[0].Columns["GUBUN"].Index; j < this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridProcPPMList.Rows[i].Cells[j].Appearance.BackColor = Color.LemonChiffon;
                            this.uGridProcPPMList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }

                    if (this.uGridProcPPMList.Rows[i].Cells["GUBUN"].Value.ToString() == "PPM")
                    {
                        for (int j = this.uGridProcPPMList.DisplayLayout.Bands[0].Columns["GUBUN"].Index-2; j < this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridProcPPMList.Rows[i].Cells[j].Appearance.BackColor = Color.PeachPuff;
                            this.uGridProcPPMList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    //마지막 셀 색깔 적용
                    this.uGridProcPPMList.Rows[i].Cells[this.uGridProcPPMList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.PeachPuff;

                }
                
                //일자 열에 대한 표시//
                this.uGridProcPPMList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcPPMList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();            	
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar
        public void mfCreate()
        {

        }

        public void mfDelete()
        {

        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridProcPPMList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridProcPPMList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "엑셀다운로드", "그리드 엑셀 다운로드", "엑셀로 다운로드 받을 내용이 없습니다.", Infragistics.Win.HAlign.Center);
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfSave()
        {

        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strFromInspectDate = Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value).ToString("yyyy-MM-dd");
                string strToInspectDate = Convert.ToDateTime(this.uDateSearchInspectDateTo.Value).ToString("yyyy-MM-dd");
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strProcInspectType = this.uComboSearchProcInspectType.Value.ToString();
                string strPACKAGEGROUP_1 = this.uComoSearchPackageGroup1.Value.ToString();
                string strPACKAGE = this.uComboSearchPackage.Value.ToString();
                string strDProcOperationType = this.uComboSearchDetailProcessOperationType.Value.ToString();
                string strInspectTypeCode = this.uComboSearchInspectType.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsReport.mfReadINSProcInspect_frmSTA0068(strPlantCode, strFromInspectDate, strToInspectDate, strCustomerCode, strProductActionType,
                                                                               strProcInspectType, strPACKAGEGROUP_1, strPACKAGE, strDProcOperationType,
                                                                               strInspectTypeCode);

                //일자별로 동적으로 컬럼이 늘어나도록 Binding하기 위해 그리드를 초기화 함.
                this.uGridProcPPMList.DataSource = null;
                this.uGridProcPPMList.ResetDisplayLayout();
                this.uGridProcPPMList.Layouts.Clear();

                this.uGridProcPPMList.DataSource = dtSearch;
                this.uGridProcPPMList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (uGridProcPPMList.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                else
                {
                    //DataTable을 바인딩하고 그리드 내용을 수정을하면 DataTable내용도 수정이되어 챠트적용하고 그리드속성을 변경함.
                    ChangeGridColumn(0);
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridProcPPMList, 0);
                }


            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();            	
            }
            finally
            {
            }
        }
        #endregion


        

    }











}
