﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPSTA.UI
{
    public partial class frmSTA0082 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0082()
        {
            InitializeComponent();
        }

        #region 화면 로드 이벤트

        private void frmSTA0082_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("CCS 품질실적", m_resSys.GetString("SYS_FONTNAME"), 12);

            InitComboBox();
            InitGroupBox();
            InitGrid();
            InitValue();
            SetToolAuth();
            InitLabel();
        }

        /// <summary>
        /// 종료시 그리드 컬럼 위치 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0082_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmSTA0082_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        
        /// <summary>
        /// 사이즈 조정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0082_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxDetail.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                    uGroupBoxTotal.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                }
                else
                {
                    uGroupBoxDetail.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uGroupBoxTotal.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 초기화 메소드
        
        /// <summary>
        /// 사용권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //콤보박스 초기화
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //공장 콤보
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                this.uComboSearchPlant.Appearance.BackColor = Color.PowderBlue;

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2"));// + "월");
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그룹박스 설정
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroup = new WinGroupBox();

                wGroup.mfSetGroupBox(this.uGroupBoxDetail, GroupBoxType.INFO, "CCS품질실적 상세", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxTotal, GroupBoxType.INFO, "CCS품질실적 종합", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                int intBandIndex = 0;
                WinGrid wGrid = new WinGrid();

                #region 월 상세 그리드

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";
                if (strLang.Equals("KOR"))
                    strName = "당해 당월 실적,전년 분기별 실적,전년 반기별 실적,Total실적";
                else if (strLang.Equals("CHN"))
                    strName = "当年 当月 实绩,前年 季度别实绩,去年 半年别实绩,Total 实绩";
                else
                    strName = "당해 당월 실적,전년 분기별 실적,전년 반기별 실적,Total실적";

                string[] strGroups = strName.Split(',');


                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "MonthlyResult", strGroups[0], 4, 0, 10, 2, false);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
                int i = 0;
                for (i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
                }

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "QuarterResult", strGroups[1], 14 + i, 0, 4, 2, false);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "HarfResult", strGroups[2], 18 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "TotalResult", strGroups[3], 21 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "MTotal", "월실적", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "YTotal", "년실적", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);
                #endregion

                #region 월 Total그리드

                wGrid.mfInitGeneralGrid(this.uGridTotal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Default, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                        , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridTotal, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTotal, 0, "PRODUCTACTIONTYPE", "제품 구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTotal, 0, "LotCount", "의뢰", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTotal, 0, "FailLotCount", "불합격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTotal, 0, "PassRate", "합격률", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 10
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                this.uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDetail.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;
                this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridTotal.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridTotal.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;
                this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDetail.DisplayLayout.Bands[0].Groups["MonthlyResult"].CellAppearance.ForeColor = Color.Black;
                this.uGridDetail.DisplayLayout.Bands[0].Groups["QuarterResult"].CellAppearance.ForeColor = Color.Black;
                this.uGridDetail.DisplayLayout.Bands[0].Groups["HarfResult"].CellAppearance.ForeColor = Color.Black;
                this.uGridDetail.DisplayLayout.Bands[0].Groups["TotalResult"].CellAppearance.ForeColor = Color.Black;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false); // 고객
                wLabel.mfSetLabel(this.uLabelProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Value초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBarMethod
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //검색 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uTextSearchYear.Text;
                string strMonth = this.uComboSearchMonth.Value.ToString();
                string strProductActionType = this.uComboProductActionType.Value.ToString();
                string strCustomer = this.uTextSearchCustomer.Text; // 고객
                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //각 그리드에 바인딩할 데이터테이블 검색
                DataTable dtDetail = clsReport.mfReadINSProcInspect_frmSTA0082_D1_PSTS(strPlantCode, strYear, strMonth, strProductActionType, strCustomer,m_resSys.GetString("SYS_LANG"));
                DataTable dtTotal = clsReport.mfReadINSProcInspect_frmSTA0082_D2_PSTS(strPlantCode, strYear, strMonth, strProductActionType, strCustomer,m_resSys.GetString("SYS_LANG"));
                //상세 그리드에 맞게 데이터테이블 형태 변경
                DataTable dtCCSDetail = ModifyDataTable(dtDetail, strYear, strMonth);
                //그리드 모양 변경
                ClearGrid();
                //SetGridColumn(0);

                //Data Bind
                this.uGridDetail.DataSource = dtCCSDetail;
                this.uGridDetail.DataBind();

                this.uGridTotal.DataSource = dtTotal;
                this.uGridTotal.DataBind();
                

                string strLang = m_resSys.GetString("SYS_LANG");
                string strGubun = "";
                if (strLang.Equals("KOR"))
                    strGubun = "합격률";
                else if (strLang.Equals("CHN"))
                    strGubun = "合格率";
                else
                    strGubun = "합격률";

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (this.uGridDetail.Rows.Count == 0 && this.uGridTotal.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        if (uGridDetail.Rows[i].Cells["Gubun"].Value.ToString().Equals(strGubun))
                        {
                           
                            for (int j = 1; j < uGridDetail.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridDetail.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridDetail.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                            this.uGridDetail.Rows[i].Cells["MTotal"].Appearance.BackColor = Color.Lavender;
                            this.uGridDetail.Rows[i].Cells["YTotal"].Appearance.BackColor = Color.Lavender;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridDetail.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridDetail);
                }
                if (this.uGridTotal.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridTotal);
                }
                if (this.uGridDetail.Rows.Count.Equals(0) && this.uGridTotal.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            { 
            }
            finally
            { }
        }
        #endregion

        /// <summary>
        /// 공장코드 변경시 제품타입 콤보박스 아이템 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboProductActionType.Items.Clear();

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                System.Data.DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 컬럼, Data 모두 삭제
        /// </summary>
        private void ClearGrid()
        {
            try
            {
                if (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDetail.Rows.All);
                    this.uGridDetail.DeleteSelectedRows(false);
                }
                //this.uGridDetail.DataSource = null;
                //this.uGridDetail.ResetDisplayLayout();
                //this.uGridDetail.Layouts.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 검색된 Detail Table을 그리드 형식에 맞게 변경
        /// </summary>
        /// <param name="dtDetail">Detail Data Table</param>
        /// <param name="strYear">검색 년도</param>
        /// <param name="strMonth">검색 월</param>
        /// <returns></returns>
        private DataTable ModifyDataTable(DataTable dtDetail, string strYear, string strMonth)
        {
            try
            {
                //월 상세 정보 그리드에 맞게 상세 테이블 변경
                dtDetail.TableName = "dtDetail";

                int intMonthDay = 31;
                

                DataTable dtCCSDetail = new DataTable("dtCCSDetail");
                DataColumn[] dcProcResult = new DataColumn[3];
                dtCCSDetail.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtCCSDetail.Columns.Add("PRODUCTACTIONTYPE", typeof(string));
                dtCCSDetail.Columns.Add("Gubun", typeof(string));
                dtCCSDetail.Columns.Add("D01", typeof(string));
                dtCCSDetail.Columns.Add("D02", typeof(string));
                dtCCSDetail.Columns.Add("D03", typeof(string));
                dtCCSDetail.Columns.Add("D04", typeof(string));
                dtCCSDetail.Columns.Add("D05", typeof(string));
                dtCCSDetail.Columns.Add("D06", typeof(string));
                dtCCSDetail.Columns.Add("D07", typeof(string));
                dtCCSDetail.Columns.Add("D08", typeof(string));
                dtCCSDetail.Columns.Add("D09", typeof(string));
                dtCCSDetail.Columns.Add("D10", typeof(string));
                for (int i = 11; i <= intMonthDay; i++)
                {
                    dtCCSDetail.Columns.Add("D" + i.ToString(), typeof(string));
                }
                dtCCSDetail.Columns.Add("Q1", typeof(string));
                dtCCSDetail.Columns.Add("Q2", typeof(string));
                dtCCSDetail.Columns.Add("Q3", typeof(string));
                dtCCSDetail.Columns.Add("Q4", typeof(string));
                dtCCSDetail.Columns.Add("FirHalf", typeof(string));
                dtCCSDetail.Columns.Add("SecHalf", typeof(string));
                dtCCSDetail.Columns.Add("YTotal", typeof(string));
                dtCCSDetail.Columns.Add("MTotal", typeof(string));

                dcProcResult[0] = dtCCSDetail.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcProcResult[1] = dtCCSDetail.Columns["PRODUCTACTIONTYPE"];
                dcProcResult[2] = dtCCSDetail.Columns["Gubun"];
                dtCCSDetail.PrimaryKey = dcProcResult;

                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                
                string strLang = m_resSys.GetString("SYS_LANG");
                string[] strGubun = new string[3];
                if (strLang.Equals("KOR"))
                { strGubun[0] = "의뢰"; strGubun[1] = "불합격"; strGubun[2] = "합격률"; }
                else if (strLang.Equals("CHN"))
                { strGubun[0] = "委托"; strGubun[1] = "不合格"; strGubun[2] = "合格率"; }
                else
                { strGubun[0] = "의뢰"; strGubun[1] = "불합격"; strGubun[2] = "합격률"; }
  

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    //LotCount
                    DataRow Lotrow = dtCCSDetail.NewRow();
                    Lotrow["DETAILPROCESSOPERATIONTYPE"] = dtDetail.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    Lotrow["PRODUCTACTIONTYPE"] = dtDetail.Rows[i]["PRODUCTACTIONTYPE"];
                    Lotrow["Gubun"] = strGubun[0];
                    Lotrow["D01"] = string.Format("{0:n0}", dtDetail.Rows[i]["D01LotCount"]);
                    Lotrow["D02"] = string.Format("{0:n0}", dtDetail.Rows[i]["D02LotCount"]);
                    Lotrow["D03"] = string.Format("{0:n0}", dtDetail.Rows[i]["D03LotCount"]);
                    Lotrow["D04"] = string.Format("{0:n0}", dtDetail.Rows[i]["D04LotCount"]);
                    Lotrow["D05"] = string.Format("{0:n0}", dtDetail.Rows[i]["D05LotCount"]);
                    Lotrow["D06"] = string.Format("{0:n0}", dtDetail.Rows[i]["D06LotCount"]);
                    Lotrow["D07"] = string.Format("{0:n0}", dtDetail.Rows[i]["D07LotCount"]);
                    Lotrow["D08"] = string.Format("{0:n0}", dtDetail.Rows[i]["D08LotCount"]);
                    Lotrow["D09"] = string.Format("{0:n0}", dtDetail.Rows[i]["D09LotCount"]);
                    Lotrow["D10"] = string.Format("{0:n0}", dtDetail.Rows[i]["D10LotCount"]);

                    Lotrow["Q1"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q1LotCount"]);
                    Lotrow["Q2"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q2LotCount"]);
                    Lotrow["Q3"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q3LotCount"]);
                    Lotrow["Q4"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q4LotCount"]);
                    Lotrow["FirHalf"] = string.Format("{0:n0}", dtDetail.Rows[i]["FirHalfLotCount"]);
                    Lotrow["SecHalf"] = string.Format("{0:n0}", dtDetail.Rows[i]["SecHalfLotCount"]);
                    Lotrow["YTotal"] = string.Format("{0:n0}", dtDetail.Rows[i]["TLotCount"]);
                    Lotrow["MTotal"] = string.Format("{0:n0}", dtDetail.Rows[i]["MTLotCount"]);

                    //FailLot
                    DataRow FailLotRow = dtCCSDetail.NewRow();
                    FailLotRow["DETAILPROCESSOPERATIONTYPE"] = dtDetail.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    FailLotRow["PRODUCTACTIONTYPE"] = dtDetail.Rows[i]["PRODUCTACTIONTYPE"];
                    FailLotRow["Gubun"] = strGubun[1];

                    FailLotRow["D01"] = string.Format("{0:n0}", dtDetail.Rows[i]["D01FailLotCount"]);
                    FailLotRow["D02"] = string.Format("{0:n0}", dtDetail.Rows[i]["D02FailLotCount"]);
                    FailLotRow["D03"] = string.Format("{0:n0}", dtDetail.Rows[i]["D03FailLotCount"]);
                    FailLotRow["D04"] = string.Format("{0:n0}", dtDetail.Rows[i]["D04FailLotCount"]);
                    FailLotRow["D05"] = string.Format("{0:n0}", dtDetail.Rows[i]["D05FailLotCount"]);
                    FailLotRow["D06"] = string.Format("{0:n0}", dtDetail.Rows[i]["D06FailLotCount"]);
                    FailLotRow["D07"] = string.Format("{0:n0}", dtDetail.Rows[i]["D07FailLotCount"]);
                    FailLotRow["D08"] = string.Format("{0:n0}", dtDetail.Rows[i]["D08FailLotCount"]);
                    FailLotRow["D09"] = string.Format("{0:n0}", dtDetail.Rows[i]["D09FailLotCount"]);
                    FailLotRow["D10"] = string.Format("{0:n0}", dtDetail.Rows[i]["D10FailLotCount"]);

                    FailLotRow["Q1"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q1FailLotCount"]);
                    FailLotRow["Q2"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q2FailLotCount"]);
                    FailLotRow["Q3"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q3FailLotCount"]);
                    FailLotRow["Q4"] = string.Format("{0:n0}", dtDetail.Rows[i]["Q4FailLotCount"]);
                    FailLotRow["FirHalf"] = string.Format("{0:n0}", dtDetail.Rows[i]["FirHalfFailLotCount"]);
                    FailLotRow["SecHalf"] = string.Format("{0:n0}", dtDetail.Rows[i]["SecHalfFailLotCount"]);
                    FailLotRow["YTotal"] = string.Format("{0:n0}", dtDetail.Rows[i]["TFailLotCount"]);
                    FailLotRow["MTotal"] = string.Format("{0:n0}", dtDetail.Rows[i]["MTFailLotCount"]);
                    

                    DataRow PassRateRow = dtCCSDetail.NewRow();

                    PassRateRow["DETAILPROCESSOPERATIONTYPE"] = dtDetail.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    PassRateRow["PRODUCTACTIONTYPE"] = dtDetail.Rows[i]["PRODUCTACTIONTYPE"];
                    PassRateRow["Gubun"] = strGubun[2];

                    PassRateRow["D01"] = dtDetail.Rows[i]["D01PassRate"];
                    PassRateRow["D02"] = dtDetail.Rows[i]["D02PassRate"];
                    PassRateRow["D03"] = dtDetail.Rows[i]["D03PassRate"];
                    PassRateRow["D04"] = dtDetail.Rows[i]["D04PassRate"];
                    PassRateRow["D05"] = dtDetail.Rows[i]["D05PassRate"];
                    PassRateRow["D06"] = dtDetail.Rows[i]["D06PassRate"];
                    PassRateRow["D07"] = dtDetail.Rows[i]["D07PassRate"];
                    PassRateRow["D08"] = dtDetail.Rows[i]["D08PassRate"];
                    PassRateRow["D09"] = dtDetail.Rows[i]["D09PassRate"];
                    PassRateRow["D10"] = dtDetail.Rows[i]["D10PassRate"];

                    PassRateRow["Q1"] = dtDetail.Rows[i]["Q1PassRate"];
                    PassRateRow["Q2"] = dtDetail.Rows[i]["Q2PassRate"];
                    PassRateRow["Q3"] = dtDetail.Rows[i]["Q3PassRate"];
                    PassRateRow["Q4"] = dtDetail.Rows[i]["Q4PassRate"];

                    PassRateRow["FirHalf"] = dtDetail.Rows[i]["FirHalfPassRate"];
                    PassRateRow["SecHalf"] = dtDetail.Rows[i]["SecHalfPassRate"];
                    PassRateRow["YTotal"] = dtDetail.Rows[i]["TPassRate"];
                    PassRateRow["MTotal"] = dtDetail.Rows[i]["MTPassRate"];

                    for (int j = 11; j <= intMonthDay; j++)
                    {
                        Lotrow["D" + j.ToString()] = string.Format("{0:n0}", dtDetail.Rows[i]["D" + j.ToString() + "LotCount"]);
                        FailLotRow["D" + j.ToString()] = string.Format("{0:n0}", dtDetail.Rows[i]["D" + j.ToString() + "FailLotCount"]);
                        PassRateRow["D" + j.ToString()] = dtDetail.Rows[i]["D" + j.ToString() + "PassRate"];
                    }

                    dtCCSDetail.Rows.Add(Lotrow);
                    dtCCSDetail.Rows.Add(FailLotRow);
                    dtCCSDetail.Rows.Add(PassRateRow);

                    Lotrow.ItemArray.Initialize();
                    FailLotRow.ItemArray.Initialize();
                    PassRateRow.ItemArray.Initialize();
                }

                int IntDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));
                if (IntDay == 31)
                    this.uGridDetail.DisplayLayout.Bands[0].Columns["D31"].Hidden = false;
                else
                    this.uGridDetail.DisplayLayout.Bands[0].Columns["D31"].Hidden = true;

                return dtCCSDetail;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDetail;
            }
            finally
            { }
        }

        private void uGridTotal_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try 
            {
                e.Layout.Bands[0].Columns["LotCount"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["LotCount"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["FailLotCount"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["FailLotCount"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["PassRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["PassRate"].MaskInput = "nnn.nn";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 고객명 직접입력 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Key 정보가 Enter가 아닌경우 Return
                if (e.KeyData != Keys.Enter)
                    return;

                //고객코드가 공백인경우 Return
                string strCustomerCode = this.uTextSearchCustomer.Text;
                if (strCustomerCode.Equals(string.Empty))
                    return;

                //고객정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //고객 정보 조회
                DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));
                clsCustomer.Dispose();

                //정보가 없는경우 메세지출력
                if (dtCustomer.Rows.Count == 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    //입력하신 고객코드가 존재하지 않습니다.
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000255", "M000889", Infragistics.Win.HAlign.Right);

                    return;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 고객 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                frmPOP0003 frmCmr = new frmPOP0003();
                frmCmr.ShowDialog();

                //팝업창에서 고객 코드를 선택하지 않았다면 Return
                if (frmCmr.CustomerCode == null
                    || frmCmr.CustomerCode.Equals(string.Empty))
                    return;

                //고객정보 삽입
                this.uTextSearchCustomer.Text = frmCmr.CustomerCode;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /////// <summary>
        /////// 받아오는 데이터테이블을 적용해서 그리드 모양 변경
        /////// </summary>
        ////private void SetGridColumn(int intBandIndex)
        ////{
        ////    try
        ////    {
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        ////        WinGrid wGrid = new WinGrid();
        ////        //그리드 컬럼 Text수정
        ////        string strYear = this.uTextSearchYear.Text;
        ////        string strPastYear = Convert.ToString(Convert.ToInt32(strYear) - 1);
        ////        string strMonth = this.uComboSearchMonth.Value.ToString();
        ////        int intMontDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));

        ////        wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
        ////               , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////               , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
        ////               , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
        ////               , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

        ////        string strLang = m_resSys.GetString("SYS_LANG");
        ////        string strName = "";
        ////        if (strLang.Equals("KOR"))
        ////            strName = "당해 당월 실적,전년 분기별 실적,전년 반기별 실적,Total실적";
        ////        else if (strLang.Equals("CHN"))
        ////            strName = "当年 当月 实绩,前年 季度别实绩,去年 半年别实绩,Total 实绩";
        ////        else
        ////            strName = "당해 당월 실적,전년 분기별 실적,전년 반기별 실적,Total실적";

        ////        string[] strGroups = strName.Split(',');

        ////        //날짜
        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        ////                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);


        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        ////                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        ////                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

        ////        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "MonthlyResult", strYear + "년" + strMonth + "월 실적", 4, 0, 10, 2, false);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
        ////        int i = 0;
        ////        for (i = 11; i < 32; i++)
        ////        {
        ////            if (i <= intMontDay)
        ////            {
        ////                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
        ////                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
        ////            }
        ////            else
        ////            {
        ////                wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 20
        ////                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
        ////            }
        ////        }

        ////        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "QuarterResult", strYear + "년 분기별 실적", 14 + i, 0, 4, 2, false);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        ////                 , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                 , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

        ////        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "HarfResult", strYear + "년 반기별 실적", 18 + i, 0, 2, 2, false);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

        ////        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridDetail, intBandIndex, "TotalResult", "Total실적", 21 + i, 0, 2, 2, false);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "MTotal", "월실적", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

        ////        wGrid.mfSetGridColumn(this.uGridDetail, intBandIndex, "YTotal", "년Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        ////                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        ////                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);

        ////        this.uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
        ////        this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

        ////        //셀 속성
        ////        this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        ////        this.uGridDetail.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;


        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    { }
        ////}
        ///////// <summary>
        ///////// 합격률 연산 메소드
        ///////// </summary>
        ///////// <param name="LotCount"></param>
        ///////// <param name="FailLotCount"></param>
        ///////// <returns></returns>
        //////private string GetPassRate(double LotCount, double FailLotCount)
        //////{
        //////    try
        //////    {
        //////        double PassRate = 0.00;

        //////        if (LotCount == 0)
        //////        {
        //////            PassRate = 0.00;
        //////        }
        //////        else
        //////        {
        //////            PassRate = (LotCount - FailLotCount) / LotCount * 100;
        //////        }

        //////        return PassRate.ToString("###.##");
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //////        frmErr.ShowDialog();
        //////        return "0";
        //////    }
        //////    finally
        //////    { }
        //////}
    }
}
