﻿namespace QRPSTA.UI
{
    partial class frmSTA0070
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0070));
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uGridQCNList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcessOperationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcessOperationType = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).BeginInit();
            this.SuspendLayout();
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcessCode.DisplayLayout.Appearance = appearance92;
            this.uComboSearchProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.Appearance = appearance93;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance94;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance95.BackColor2 = System.Drawing.SystemColors.Control;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance95;
            this.uComboSearchProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance96.BackColor = System.Drawing.SystemColors.Window;
            appearance96.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance96;
            appearance97.BackColor = System.Drawing.SystemColors.Highlight;
            appearance97.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance97;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance98;
            appearance99.BorderColor = System.Drawing.Color.Silver;
            appearance99.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellAppearance = appearance99;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance100.BackColor = System.Drawing.SystemColors.Control;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance100;
            appearance101.TextHAlignAsString = "Left";
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderAppearance = appearance101;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowAppearance = appearance102;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance103;
            this.uComboSearchProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(728, 36);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(120, 22);
            this.uComboSearchProcessCode.TabIndex = 335;
            this.uComboSearchProcessCode.Text = "ultraCombo1";
            // 
            // uGridQCNList
            // 
            this.uGridQCNList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNList.DisplayLayout.Appearance = appearance14;
            this.uGridQCNList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridQCNList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridQCNList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridQCNList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridQCNList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridQCNList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridQCNList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridQCNList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNList.Location = new System.Drawing.Point(4, 108);
            this.uGridQCNList.Name = "uGridQCNList";
            this.uGridQCNList.Size = new System.Drawing.Size(1060, 740);
            this.uGridQCNList.TabIndex = 317;
            this.uGridQCNList.Text = "ultraGrid1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.uComboSearchMonth);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchMonth);
            this.ultraGroupBox1.Controls.Add(this.uTextSearchYear);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchYear);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProcessCode);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProcess);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchDetailProcessOperationType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchDetailProcessOperationType);
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, 40);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1060, 68);
            this.ultraGroupBox1.TabIndex = 318;
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(300, 12);
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(72, 21);
            this.uComboSearchMonth.TabIndex = 339;
            this.uComboSearchMonth.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMonth
            // 
            this.uLabelSearchMonth.Location = new System.Drawing.Point(196, 12);
            this.uLabelSearchMonth.Name = "uLabelSearchMonth";
            this.uLabelSearchMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMonth.TabIndex = 338;
            this.uLabelSearchMonth.Text = "ultraLabel1";
            // 
            // uTextSearchYear
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance1;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(116, 12);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(72, 21);
            this.uTextSearchYear.TabIndex = 337;
            // 
            // uLabelSearchYear
            // 
            appearance17.BackColor = System.Drawing.Color.LightCyan;
            this.uLabelSearchYear.Appearance = appearance17;
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 336;
            this.uLabelSearchYear.Text = "년도";
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(728, 12);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchProductActionType.TabIndex = 317;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProductActionType
            // 
            this.uLabelSearchProductActionType.Location = new System.Drawing.Point(624, 12);
            this.uLabelSearchProductActionType.Name = "uLabelSearchProductActionType";
            this.uLabelSearchProductActionType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProductActionType.TabIndex = 316;
            this.uLabelSearchProductActionType.Text = "제품구분";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(488, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchCustomer.TabIndex = 315;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(384, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 314;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(624, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 334;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uComboSearchDetailProcessOperationType
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Appearance = appearance2;
            this.uComboSearchDetailProcessOperationType.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Location = new System.Drawing.Point(488, 36);
            this.uComboSearchDetailProcessOperationType.Name = "uComboSearchDetailProcessOperationType";
            this.uComboSearchDetailProcessOperationType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchDetailProcessOperationType.TabIndex = 327;
            this.uComboSearchDetailProcessOperationType.Text = "ultraComboEditor1";
            this.uComboSearchDetailProcessOperationType.ValueChanged += new System.EventHandler(this.uComboSearchDetailProcessOperationType_ValueChanged);
            // 
            // uLabelSearchDetailProcessOperationType
            // 
            this.uLabelSearchDetailProcessOperationType.Location = new System.Drawing.Point(384, 36);
            this.uLabelSearchDetailProcessOperationType.Name = "uLabelSearchDetailProcessOperationType";
            this.uLabelSearchDetailProcessOperationType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDetailProcessOperationType.TabIndex = 326;
            this.uLabelSearchDetailProcessOperationType.Text = "공정구분";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 316;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmSTA0070
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridQCNList);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0070";
            this.Text = "frmSTA0070";
            this.Load += new System.EventHandler(this.frmSTA0070_Load);
            this.Activated += new System.EventHandler(this.frmSTA0070_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0070_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcessCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductActionType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcessOperationType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcessOperationType;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
    }
}