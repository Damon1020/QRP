﻿/*----------------------------------------------------------------------*/
/* 시스템명     :                                                       */
/* 모듈(분류)명 :                                                       */
/* 프로그램ID   : frmSTA0086.cs                                         */
/* 프로그램명   : 수입검사 월별현황                                     */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2012-02-22                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPSTA.UI
{
    public partial class frmSTA0086 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0086()
        {
            InitializeComponent();
        }

        private void frmSTA0086_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("수입검사 월별현황", m_SysRes.GetString("SYS_FONTNAME"), 12);
            SetToolAuth();

            InitGrid();
            InitLabel();
            InitYear();
            InitComboBox();
        }

        private void frmSTA0086_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0086_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "연도", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 검색연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                   , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                WinGrid wGrid = new WinGrid();

            #region 자재 종류별 그리드
                wGrid.mfInitGeneralGrid(this.uGridMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridMat, 0, "ConsumableTypeCode", "자재종류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 40
                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "Gubun", "항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMat, 0, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            #endregion

            #region 고객사별 그리드
                wGrid.mfInitGeneralGrid(this.uGridCustomer, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                            , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                            , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                            , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "ConsumableTypeCode", "자재종류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 40
                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "VendorName", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "","", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "Gubun", "항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomer, 0, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridCustomer.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCustomer.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { } 
        }
        #endregion

        #region IToolBar Method
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information,500,500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M000266", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001108", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.INSMaterialReport), "INSMaterialReport");
                    QRPSTA.BL.STAIMP.INSMaterialReport clsReport = new QRPSTA.BL.STAIMP.INSMaterialReport();
                    brwChannel.mfCredentials(clsReport);

                    //Progress 창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;

                    //자재종류별 DataTable
                    DataTable dtMat = clsReport.mfReadINSMaterialInspect_frmSTA0086_D1(strPlantCode, strYear);
                    //자재종류별 고객사 DataTable
                    DataTable dtMatCustomer = clsReport.mfReadINSMaterialInspect_frmSTA0086_D2(strPlantCode, strYear, m_resSys.GetString("SYS_LANG"));

                    //ProgressPop 종료
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtMat.Rows.Count.Equals(0) && dtMatCustomer.Rows.Count.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001135", "M001115", "M000204", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strName = string.Empty;

                        if (strLang.Equals("KOR"))
                            strName = "검사건수,불량건수,건 불량률,시료수,불량수,시료 불량률";
                        else if (strLang.Equals("CHN"))
                            strName = "检查件数,不良件数,件 不良率,样品数,不良数,样品 不良率";
                        else if (strLang.Equals("ENG"))
                            strName = "검사건수,불량건수,건 불량률,시료수,불량수,시료 불량률";

                        string[] strCoulmns = strName.Split(',');

                        if (dtMat.Rows.Count > 0)
                        {
                            #region 자재별 데이터 테이블 그리드에 맞춰서 변환
                            //그리드와 일치하는 데이터테이블 생성(PrimaryKey = ConsumableTypeCode, Gubun)
                            DataTable dtMatGrid = new DataTable("MatGrid");
                            DataColumn[] dcMat = new DataColumn[2];
                            dtMatGrid.Columns.Add("ConsumableTypeCode", typeof(string));
                            dtMatGrid.Columns.Add("Gubun", typeof(string));
                            dtMatGrid.Columns.Add("M01", typeof(string));
                            dtMatGrid.Columns.Add("M02", typeof(string));
                            dtMatGrid.Columns.Add("M03", typeof(string));
                            dtMatGrid.Columns.Add("M04", typeof(string));
                            dtMatGrid.Columns.Add("M05", typeof(string));
                            dtMatGrid.Columns.Add("M06", typeof(string));
                            dtMatGrid.Columns.Add("M07", typeof(string));
                            dtMatGrid.Columns.Add("M08", typeof(string));
                            dtMatGrid.Columns.Add("M09", typeof(string));
                            dtMatGrid.Columns.Add("M10", typeof(string));
                            dtMatGrid.Columns.Add("M11", typeof(string));
                            dtMatGrid.Columns.Add("M12", typeof(string));
                            dtMatGrid.Columns.Add("Total", typeof(string));

                            dcMat[0] = dtMatGrid.Columns["ConsumableTypeCode"];
                            dcMat[1] = dtMatGrid.Columns["Gubun"];
                            dtMatGrid.PrimaryKey = dcMat;


                            
                            //검색된 데이터 테이블의 첫줄부터 컬럼명에 따라 정리
                            for (int i = 0; i < dtMat.Rows.Count; i++)
                            {
                                //LotCount
                                DataRow LotRow = dtMatGrid.NewRow();
                                LotRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];
                                LotRow["Gubun"] = strCoulmns[0]; //"검사건수";
                                LotRow["M01"] = string.Format("{0:n0}", dtMat.Rows[i]["M01LotCount"]);
                                LotRow["M02"] = string.Format("{0:n0}", dtMat.Rows[i]["M02LotCount"]);
                                LotRow["M03"] = string.Format("{0:n0}", dtMat.Rows[i]["M03LotCount"]);
                                LotRow["M04"] = string.Format("{0:n0}", dtMat.Rows[i]["M04LotCount"]);
                                LotRow["M05"] = string.Format("{0:n0}", dtMat.Rows[i]["M05LotCount"]);
                                LotRow["M06"] = string.Format("{0:n0}", dtMat.Rows[i]["M06LotCount"]);
                                LotRow["M07"] = string.Format("{0:n0}", dtMat.Rows[i]["M07LotCount"]);
                                LotRow["M08"] = string.Format("{0:n0}", dtMat.Rows[i]["M08LotCount"]);
                                LotRow["M09"] = string.Format("{0:n0}", dtMat.Rows[i]["M09LotCount"]);
                                LotRow["M10"] = string.Format("{0:n0}", dtMat.Rows[i]["M10LotCount"]);
                                LotRow["M11"] = string.Format("{0:n0}", dtMat.Rows[i]["M11LotCount"]);
                                LotRow["M12"] = string.Format("{0:n0}", dtMat.Rows[i]["M12LotCount"]);
                                LotRow["Total"] = string.Format("{0:n0}", dtMat.Rows[i]["TLotCount"]);

                                //FailLotCount
                                DataRow FailLotRow = dtMatGrid.NewRow();
                                FailLotRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];
                                FailLotRow["Gubun"] = strCoulmns[1]; //"불량건수";
                                FailLotRow["M01"] = string.Format("{0:n0}", dtMat.Rows[i]["M01FailLotCount"]);
                                FailLotRow["M02"] = string.Format("{0:n0}", dtMat.Rows[i]["M02FailLotCount"]);
                                FailLotRow["M03"] = string.Format("{0:n0}", dtMat.Rows[i]["M03FailLotCount"]);
                                FailLotRow["M04"] = string.Format("{0:n0}", dtMat.Rows[i]["M04FailLotCount"]);
                                FailLotRow["M05"] = string.Format("{0:n0}", dtMat.Rows[i]["M05FailLotCount"]);
                                FailLotRow["M06"] = string.Format("{0:n0}", dtMat.Rows[i]["M06FailLotCount"]);
                                FailLotRow["M07"] = string.Format("{0:n0}", dtMat.Rows[i]["M07FailLotCount"]);
                                FailLotRow["M08"] = string.Format("{0:n0}", dtMat.Rows[i]["M08FailLotCount"]);
                                FailLotRow["M09"] = string.Format("{0:n0}", dtMat.Rows[i]["M09FailLotCount"]);
                                FailLotRow["M10"] = string.Format("{0:n0}", dtMat.Rows[i]["M10FailLotCount"]);
                                FailLotRow["M11"] = string.Format("{0:n0}", dtMat.Rows[i]["M11FailLotCount"]);
                                FailLotRow["M12"] = string.Format("{0:n0}", dtMat.Rows[i]["M12FailLotCount"]);
                                FailLotRow["Total"] = string.Format("{0:n0}", dtMat.Rows[i]["TFailLotCount"]);

                                //Lot합격률
                                DataRow PassRateRow = dtMatGrid.NewRow();
                                PassRateRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];
                                PassRateRow["Gubun"] = strCoulmns[2]; //"건 불량률";
                                PassRateRow["M01"] = string.Format("{0:F2}", dtMat.Rows[i]["M01PassRate"]);
                                PassRateRow["M02"] = string.Format("{0:F2}", dtMat.Rows[i]["M02PassRate"]);
                                PassRateRow["M03"] = string.Format("{0:F2}", dtMat.Rows[i]["M03PassRate"]);
                                PassRateRow["M04"] = string.Format("{0:F2}", dtMat.Rows[i]["M04PassRate"]);
                                PassRateRow["M05"] = string.Format("{0:F2}", dtMat.Rows[i]["M05PassRate"]);
                                PassRateRow["M06"] = string.Format("{0:F2}", dtMat.Rows[i]["M06PassRate"]);
                                PassRateRow["M07"] = string.Format("{0:F2}", dtMat.Rows[i]["M07PassRate"]);
                                PassRateRow["M08"] = string.Format("{0:F2}", dtMat.Rows[i]["M08PassRate"]);
                                PassRateRow["M09"] = string.Format("{0:F2}", dtMat.Rows[i]["M09PassRate"]);
                                PassRateRow["M10"] = string.Format("{0:F2}", dtMat.Rows[i]["M10PassRate"]);
                                PassRateRow["M11"] = string.Format("{0:F2}", dtMat.Rows[i]["M11PassRate"]);
                                PassRateRow["M12"] = string.Format("{0:F2}", dtMat.Rows[i]["M12PassRate"]);
                                PassRateRow["Total"] = string.Format("{0:F2}", dtMat.Rows[i]["TPassRate"]);

                                //시료수
                                DataRow SampleSizeRow = dtMatGrid.NewRow();
                                SampleSizeRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];
                                if(dtMat.Rows[i]["ConsumableTypeCode"].ToString().Contains("WIRE"))
                                    SampleSizeRow["Gubun"] = strCoulmns[3] + "(KFT)"; //"시료수(KFT)";
                                else
                                    SampleSizeRow["Gubun"] = strCoulmns[3]; //"시료수(KFT)";

                                SampleSizeRow["M01"] = string.Format("{0:n0}", dtMat.Rows[i]["M01SampleSize"]);
                                SampleSizeRow["M02"] = string.Format("{0:n0}", dtMat.Rows[i]["M02SampleSize"]);
                                SampleSizeRow["M03"] = string.Format("{0:n0}", dtMat.Rows[i]["M03SampleSize"]);
                                SampleSizeRow["M04"] = string.Format("{0:n0}", dtMat.Rows[i]["M04SampleSize"]);
                                SampleSizeRow["M05"] = string.Format("{0:n0}", dtMat.Rows[i]["M05SampleSize"]);
                                SampleSizeRow["M06"] = string.Format("{0:n0}", dtMat.Rows[i]["M06SampleSize"]);
                                SampleSizeRow["M07"] = string.Format("{0:n0}", dtMat.Rows[i]["M07SampleSize"]);
                                SampleSizeRow["M08"] = string.Format("{0:n0}", dtMat.Rows[i]["M08SampleSize"]);
                                SampleSizeRow["M09"] = string.Format("{0:n0}", dtMat.Rows[i]["M09SampleSize"]);
                                SampleSizeRow["M10"] = string.Format("{0:n0}", dtMat.Rows[i]["M10SampleSize"]);
                                SampleSizeRow["M11"] = string.Format("{0:n0}", dtMat.Rows[i]["M11SampleSize"]);
                                SampleSizeRow["M12"] = string.Format("{0:n0}", dtMat.Rows[i]["M12SampleSize"]);
                                SampleSizeRow["Total"] = string.Format("{0:n0}", dtMat.Rows[i]["TSampleSize"]);

                                //불량수
                                DataRow FaultQtyRow = dtMatGrid.NewRow();
                                FaultQtyRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];

                                if (dtMat.Rows[i]["ConsumableTypeCode"].ToString().Contains("WIRE"))
                                    FaultQtyRow["Gubun"] = strCoulmns[4] + "(KFT)"; //"불량수(KFT)";
                                else
                                    FaultQtyRow["Gubun"] = strCoulmns[4]; //"불량수(KFT)";
                                
                                FaultQtyRow["M01"] = string.Format("{0:n0}", dtMat.Rows[i]["M01FaultQty"]);
                                FaultQtyRow["M02"] = string.Format("{0:n0}", dtMat.Rows[i]["M02FaultQty"]);
                                FaultQtyRow["M03"] = string.Format("{0:n0}", dtMat.Rows[i]["M03FaultQty"]);
                                FaultQtyRow["M04"] = string.Format("{0:n0}", dtMat.Rows[i]["M04FaultQty"]);
                                FaultQtyRow["M05"] = string.Format("{0:n0}", dtMat.Rows[i]["M05FaultQty"]);
                                FaultQtyRow["M06"] = string.Format("{0:n0}", dtMat.Rows[i]["M06FaultQty"]);
                                FaultQtyRow["M07"] = string.Format("{0:n0}", dtMat.Rows[i]["M07FaultQty"]);
                                FaultQtyRow["M08"] = string.Format("{0:n0}", dtMat.Rows[i]["M08FaultQty"]);
                                FaultQtyRow["M09"] = string.Format("{0:n0}", dtMat.Rows[i]["M09FaultQty"]);
                                FaultQtyRow["M10"] = string.Format("{0:n0}", dtMat.Rows[i]["M10FaultQty"]);
                                FaultQtyRow["M11"] = string.Format("{0:n0}", dtMat.Rows[i]["M11FaultQty"]);
                                FaultQtyRow["M12"] = string.Format("{0:n0}", dtMat.Rows[i]["M12FaultQty"]);
                                FaultQtyRow["Total"] = string.Format("{0:n0}", dtMat.Rows[i]["TFaultQty"]);

                                //시료 불량율
                                DataRow FaultRateRow = dtMatGrid.NewRow();
                                FaultRateRow["ConsumableTypeCode"] = dtMat.Rows[i]["ConsumableTypeCode"];
                                FaultRateRow["Gubun"] = strCoulmns[5];//"시료 불량률";
                                FaultRateRow["M01"] = string.Format("{0:n2}", dtMat.Rows[i]["M01FaultRate"]);
                                FaultRateRow["M02"] = string.Format("{0:n2}", dtMat.Rows[i]["M02FaultRate"]);
                                FaultRateRow["M03"] = string.Format("{0:n2}", dtMat.Rows[i]["M03FaultRate"]);
                                FaultRateRow["M04"] = string.Format("{0:n2}", dtMat.Rows[i]["M04FaultRate"]);
                                FaultRateRow["M05"] = string.Format("{0:n2}", dtMat.Rows[i]["M05FaultRate"]);
                                FaultRateRow["M06"] = string.Format("{0:n2}", dtMat.Rows[i]["M06FaultRate"]);
                                FaultRateRow["M07"] = string.Format("{0:n2}", dtMat.Rows[i]["M07FaultRate"]);
                                FaultRateRow["M08"] = string.Format("{0:n2}", dtMat.Rows[i]["M08FaultRate"]);
                                FaultRateRow["M09"] = string.Format("{0:n2}", dtMat.Rows[i]["M09FaultRate"]);
                                FaultRateRow["M10"] = string.Format("{0:n2}", dtMat.Rows[i]["M10FaultRate"]);
                                FaultRateRow["M11"] = string.Format("{0:n2}", dtMat.Rows[i]["M11FaultRate"]);
                                FaultRateRow["M12"] = string.Format("{0:n2}", dtMat.Rows[i]["M12FaultRate"]);
                                FaultRateRow["Total"] = string.Format("{0:n2}", dtMat.Rows[i]["TFaultRate"]);

                                //각 Row를 Grid에 바인딩할 DataTable에 넣는다
                                dtMatGrid.Rows.Add(LotRow);
                                dtMatGrid.Rows.Add(FailLotRow);
                                dtMatGrid.Rows.Add(PassRateRow);
                                dtMatGrid.Rows.Add(SampleSizeRow);
                                dtMatGrid.Rows.Add(FaultQtyRow);
                                dtMatGrid.Rows.Add(FaultRateRow);
                            }
                            #endregion

                            #region Grid바인딩, 불량율 색변경
                            //만들어진 DataTable을 Gird에 바인딩
                            this.uGridMat.DataSource = dtMatGrid;
                            this.uGridMat.DataBind();

                            //합격률 줄의 색, 글씨 변경
                            for (int i = 0; i < this.uGridMat.Rows.Count; i++)
                            {
                                if (this.uGridMat.Rows[i].Cells["Gubun"].Text == strCoulmns[2])//"건 불량률")
                                {
                                    for (int j = 1; j < this.uGridMat.DisplayLayout.Bands[0].Columns.Count; j++)
                                    {
                                        this.uGridMat.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                                        this.uGridMat.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                    }
                                }
                                else if (this.uGridMat.Rows[i].Cells["Gubun"].Text == strCoulmns[5])//"시료 불량률")
                                {
                                    for (int j = 1; j < this.uGridMat.DisplayLayout.Bands[0].Columns.Count; j++)
                                    {
                                        this.uGridMat.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                                        this.uGridMat.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dtMatCustomer.Rows.Count > 0)
                        {
                            #region 자재별, 고객사별 데이터 테이블 그리드에 맞춰서 변환
                            //그리드와 일치하는 데이터테이블 생성(PrimaryKey = ConsumableTypeCode,VendorName, Gubun)
                            DataTable dtMatCustomerGrid = new DataTable("MatCustomerGrid");
                            DataColumn[] dcMatCustmer = new DataColumn[3];
                            dtMatCustomerGrid.Columns.Add("ConsumableTypeCode", typeof(string));
                            dtMatCustomerGrid.Columns.Add("VendorName", typeof(string));
                            dtMatCustomerGrid.Columns.Add("Gubun", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M01", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M02", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M03", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M04", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M05", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M06", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M07", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M08", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M09", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M10", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M11", typeof(string));
                            dtMatCustomerGrid.Columns.Add("M12", typeof(string));
                            dtMatCustomerGrid.Columns.Add("Total", typeof(string));

                            dcMatCustmer[0] = dtMatCustomerGrid.Columns["ConsumableTypeCode"];
                            dcMatCustmer[1] = dtMatCustomerGrid.Columns["VendorName"];
                            dcMatCustmer[2] = dtMatCustomerGrid.Columns["Gubun"];
                            dtMatCustomerGrid.PrimaryKey = dcMatCustmer;

                            for (int i = 0; i < dtMatCustomer.Rows.Count; i++)
                            {
                                //LotCount
                                DataRow LotRow = dtMatCustomerGrid.NewRow();
                                LotRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                LotRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];
                                LotRow["Gubun"] = strCoulmns[0];// "검사건수";
                                LotRow["M01"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M01LotCount"]);
                                LotRow["M02"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M02LotCount"]);
                                LotRow["M03"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M03LotCount"]);
                                LotRow["M04"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M04LotCount"]);
                                LotRow["M05"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M05LotCount"]);
                                LotRow["M06"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M06LotCount"]);
                                LotRow["M07"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M07LotCount"]);
                                LotRow["M08"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M08LotCount"]);
                                LotRow["M09"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M09LotCount"]);
                                LotRow["M10"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M10LotCount"]);
                                LotRow["M11"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M11LotCount"]);
                                LotRow["M12"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M12LotCount"]);
                                LotRow["Total"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["TLotCount"]);

                                //FailLotCount
                                DataRow FailLotRow = dtMatCustomerGrid.NewRow();
                                FailLotRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                FailLotRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];
                                FailLotRow["Gubun"] = strCoulmns[1];// "불량건수";
                                FailLotRow["M01"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M01FailLotCount"]);
                                FailLotRow["M02"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M02FailLotCount"]);
                                FailLotRow["M03"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M03FailLotCount"]);
                                FailLotRow["M04"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M04FailLotCount"]);
                                FailLotRow["M05"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M05FailLotCount"]);
                                FailLotRow["M06"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M06FailLotCount"]);
                                FailLotRow["M07"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M07FailLotCount"]);
                                FailLotRow["M08"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M08FailLotCount"]);
                                FailLotRow["M09"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M09FailLotCount"]);
                                FailLotRow["M10"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M10FailLotCount"]);
                                FailLotRow["M11"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M11FailLotCount"]);
                                FailLotRow["M12"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M12FailLotCount"]);
                                FailLotRow["Total"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["TFailLotCount"]);

                                //Lot합격률
                                DataRow PassRateRow = dtMatCustomerGrid.NewRow();
                                PassRateRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                PassRateRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];
                                PassRateRow["Gubun"] = strCoulmns[2]; //"건 불량률";
                                PassRateRow["M01"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M01PassRate"]);
                                PassRateRow["M02"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M02PassRate"]);
                                PassRateRow["M03"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M03PassRate"]);
                                PassRateRow["M04"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M04PassRate"]);
                                PassRateRow["M05"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M05PassRate"]);
                                PassRateRow["M06"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M06PassRate"]);
                                PassRateRow["M07"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M07PassRate"]);
                                PassRateRow["M08"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M08PassRate"]);
                                PassRateRow["M09"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M09PassRate"]);
                                PassRateRow["M10"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M10PassRate"]);
                                PassRateRow["M11"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M11PassRate"]);
                                PassRateRow["M12"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["M12PassRate"]);
                                PassRateRow["Total"] = string.Format("{0:F2}", dtMatCustomer.Rows[i]["TPassRate"]);

                                //시료수
                                DataRow SampleSizeRow = dtMatCustomerGrid.NewRow();
                                SampleSizeRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                SampleSizeRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];

                                if (dtMatCustomer.Rows[i]["ConsumableTypeCode"].ToString().Contains("WIRE"))
                                    SampleSizeRow["Gubun"] = strCoulmns[3] + "(KFT)"; //"시료수(KFT)";
                                else
                                    SampleSizeRow["Gubun"] = strCoulmns[3]; //"시료수(KFT)";
   
                                SampleSizeRow["M01"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M01SampleSize"]);
                                SampleSizeRow["M02"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M02SampleSize"]);
                                SampleSizeRow["M03"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M03SampleSize"]);
                                SampleSizeRow["M04"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M04SampleSize"]);
                                SampleSizeRow["M05"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M05SampleSize"]);
                                SampleSizeRow["M06"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M06SampleSize"]);
                                SampleSizeRow["M07"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M07SampleSize"]);
                                SampleSizeRow["M08"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M08SampleSize"]);
                                SampleSizeRow["M09"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M09SampleSize"]);
                                SampleSizeRow["M10"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M10SampleSize"]);
                                SampleSizeRow["M11"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M11SampleSize"]);
                                SampleSizeRow["M12"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M12SampleSize"]);
                                SampleSizeRow["Total"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["TSampleSize"]);

                                //불량수
                                DataRow FaultQtyRow = dtMatCustomerGrid.NewRow();
                                FaultQtyRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                FaultQtyRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];

                                if (dtMatCustomer.Rows[i]["ConsumableTypeCode"].ToString().Contains("WIRE"))
                                    FaultQtyRow["Gubun"] = strCoulmns[4] + "(KFT)"; //"불량수(KFT)";
                                else
                                    FaultQtyRow["Gubun"] = strCoulmns[4]; //"불량수(KFT)";

                                FaultQtyRow["M01"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M01FaultQty"]);
                                FaultQtyRow["M02"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M02FaultQty"]);
                                FaultQtyRow["M03"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M03FaultQty"]);
                                FaultQtyRow["M04"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M04FaultQty"]);
                                FaultQtyRow["M05"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M05FaultQty"]);
                                FaultQtyRow["M06"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M06FaultQty"]);
                                FaultQtyRow["M07"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M07FaultQty"]);
                                FaultQtyRow["M08"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M08FaultQty"]);
                                FaultQtyRow["M09"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M09FaultQty"]);
                                FaultQtyRow["M10"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M10FaultQty"]);
                                FaultQtyRow["M11"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M11FaultQty"]);
                                FaultQtyRow["M12"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["M12FaultQty"]);
                                FaultQtyRow["Total"] = string.Format("{0:n0}", dtMatCustomer.Rows[i]["TFaultQty"]);

                                //시료 불량율
                                DataRow FaultRateRow = dtMatCustomerGrid.NewRow();
                                FaultRateRow["ConsumableTypeCode"] = dtMatCustomer.Rows[i]["ConsumableTypeCode"];
                                FaultRateRow["VendorName"] = dtMatCustomer.Rows[i]["VendorName"];
                                FaultRateRow["Gubun"] = strCoulmns[5]; //"시료 불량률";
                                FaultRateRow["M01"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M01FaultRate"]);
                                FaultRateRow["M02"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M02FaultRate"]);
                                FaultRateRow["M03"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M03FaultRate"]);
                                FaultRateRow["M04"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M04FaultRate"]);
                                FaultRateRow["M05"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M05FaultRate"]);
                                FaultRateRow["M06"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M06FaultRate"]);
                                FaultRateRow["M07"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M07FaultRate"]);
                                FaultRateRow["M08"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M08FaultRate"]);
                                FaultRateRow["M09"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M09FaultRate"]);
                                FaultRateRow["M10"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M10FaultRate"]);
                                FaultRateRow["M11"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M11FaultRate"]);
                                FaultRateRow["M12"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["M12FaultRate"]);
                                FaultRateRow["Total"] = string.Format("{0:n2}", dtMatCustomer.Rows[i]["TFaultRate"]);

                                dtMatCustomerGrid.Rows.Add(LotRow);
                                dtMatCustomerGrid.Rows.Add(FailLotRow);
                                dtMatCustomerGrid.Rows.Add(PassRateRow);
                                dtMatCustomerGrid.Rows.Add(SampleSizeRow);
                                dtMatCustomerGrid.Rows.Add(FaultQtyRow);
                                dtMatCustomerGrid.Rows.Add(FaultRateRow);
                            }
                            #endregion

                            #region Grid바인딩, 불량율 색 변경
                            //만들어진 DataTable을 Gird에 바인딩
                            this.uGridCustomer.DataSource = dtMatCustomerGrid;
                            this.uGridCustomer.DataBind();

                            //합격률 줄의 색, 글씨 변경
                            for (int i = 0; i < this.uGridCustomer.Rows.Count; i++)
                            {
                                if (this.uGridCustomer.Rows[i].Cells["Gubun"].Text == strCoulmns[2])//"건 불량률")
                                {
                                    for (int j = 1; j < this.uGridCustomer.DisplayLayout.Bands[0].Columns.Count; j++)
                                    {
                                        this.uGridCustomer.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                                        this.uGridCustomer.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                    }
                                }
                                else if (this.uGridCustomer.Rows[i].Cells["Gubun"].Text == strCoulmns[5])//"시료 불량률")
                                {
                                    for (int j = 1; j < this.uGridCustomer.DisplayLayout.Bands[0].Columns.Count; j++)
                                    {
                                        this.uGridCustomer.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                                        this.uGridCustomer.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        public void mfSave()
        { }

        public void mfDelete()
        { }

        public void mfCreate()
        { }

        public void mfPrint()
        { }

        public void mfExcel()
        {
            if (this.uGridMat.Rows.Count > 0)
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfDownLoadGridToExcel(this.uGridMat);
            }
            if (this.uGridCustomer.Rows.Count > 0)
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfDownLoadGridToExcel(this.uGridCustomer);
            }
            if (this.uGridMat.Rows.Count.Equals(0) && this.uGridCustomer.Rows.Count.Equals(0))
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
            }
        }
        #endregion

        //연도 검색 텍스트박스에 숫자만 입력
        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) || e.KeyChar == Convert.ToChar(46)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 화면 리사이즈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0086_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uTabInfo.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                }
                else
                {
                    uTabInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
    }
}
