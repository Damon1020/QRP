﻿namespace QRPSTA.UI
{
    partial class frmSTA0067
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0067));
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComoSearchPackageGroup3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup3 = new Infragistics.Win.Misc.UltraLabel();
            this.uComoSearchPackageGroup2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup2 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcessOperationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcessOperationType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectResult = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectResult = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComoSearchPackageGroup1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridProcInspectList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspectList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComoSearchPackageGroup3);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackageGroup3);
            this.uGroupBoxSearchArea.Controls.Add(this.uComoSearchPackageGroup2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackageGroup2);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDetailProcessOperationType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDetailProcessOperationType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectResult);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectResult);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComoSearchPackageGroup1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackageGroup1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 110);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComoSearchPackageGroup3
            // 
            this.uComoSearchPackageGroup3.Location = new System.Drawing.Point(778, 32);
            this.uComoSearchPackageGroup3.Name = "uComoSearchPackageGroup3";
            this.uComoSearchPackageGroup3.Size = new System.Drawing.Size(103, 21);
            this.uComoSearchPackageGroup3.TabIndex = 278;
            this.uComoSearchPackageGroup3.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup3
            // 
            this.uLabelSearchPackageGroup3.Location = new System.Drawing.Point(689, 32);
            this.uLabelSearchPackageGroup3.Name = "uLabelSearchPackageGroup3";
            this.uLabelSearchPackageGroup3.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackageGroup3.TabIndex = 277;
            this.uLabelSearchPackageGroup3.Text = "Package3";
            // 
            // uComoSearchPackageGroup2
            // 
            this.uComoSearchPackageGroup2.Location = new System.Drawing.Point(579, 32);
            this.uComoSearchPackageGroup2.Name = "uComoSearchPackageGroup2";
            this.uComoSearchPackageGroup2.Size = new System.Drawing.Size(103, 21);
            this.uComoSearchPackageGroup2.TabIndex = 276;
            this.uComoSearchPackageGroup2.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup2
            // 
            this.uLabelSearchPackageGroup2.Location = new System.Drawing.Point(490, 32);
            this.uLabelSearchPackageGroup2.Name = "uLabelSearchPackageGroup2";
            this.uLabelSearchPackageGroup2.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackageGroup2.TabIndex = 275;
            this.uLabelSearchPackageGroup2.Text = "Package2";
            // 
            // uComboSearchInspectType
            // 
            this.uComboSearchInspectType.Location = new System.Drawing.Point(778, 8);
            this.uComboSearchInspectType.Name = "uComboSearchInspectType";
            this.uComboSearchInspectType.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchInspectType.TabIndex = 274;
            this.uComboSearchInspectType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectType
            // 
            this.uLabelSearchInspectType.Location = new System.Drawing.Point(689, 8);
            this.uLabelSearchInspectType.Name = "uLabelSearchInspectType";
            this.uLabelSearchInspectType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchInspectType.TabIndex = 273;
            this.uLabelSearchInspectType.Text = "검사유형";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(99, 80);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(183, 21);
            this.uTextSearchLotNo.TabIndex = 272;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(10, 80);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchLotNo.TabIndex = 271;
            this.uLabelSearchLotNo.Text = "LotNo";
            // 
            // uTextSearchCustomerProductSpec
            // 
            this.uTextSearchCustomerProductSpec.Location = new System.Drawing.Point(99, 56);
            this.uTextSearchCustomerProductSpec.Name = "uTextSearchCustomerProductSpec";
            this.uTextSearchCustomerProductSpec.Size = new System.Drawing.Size(183, 21);
            this.uTextSearchCustomerProductSpec.TabIndex = 270;
            // 
            // uLabelSearchCustomerProductSpec
            // 
            this.uLabelSearchCustomerProductSpec.Location = new System.Drawing.Point(10, 56);
            this.uLabelSearchCustomerProductSpec.Name = "uLabelSearchCustomerProductSpec";
            this.uLabelSearchCustomerProductSpec.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomerProductSpec.TabIndex = 269;
            this.uLabelSearchCustomerProductSpec.Text = "고객사제품코드";
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcessCode.DisplayLayout.Appearance = appearance2;
            this.uComboSearchProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uComboSearchProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellAppearance = appearance9;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowAppearance = appearance12;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uComboSearchProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(579, 56);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(103, 22);
            this.uComboSearchProcessCode.TabIndex = 268;
            this.uComboSearchProcessCode.Text = "ultraCombo1";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(490, 56);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchProcess.TabIndex = 267;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uComboSearchDetailProcessOperationType
            // 
            this.uComboSearchDetailProcessOperationType.Location = new System.Drawing.Point(381, 56);
            this.uComboSearchDetailProcessOperationType.Name = "uComboSearchDetailProcessOperationType";
            this.uComboSearchDetailProcessOperationType.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchDetailProcessOperationType.TabIndex = 266;
            this.uComboSearchDetailProcessOperationType.Text = "ultraComboEditor1";
            this.uComboSearchDetailProcessOperationType.ValueChanged += new System.EventHandler(this.uComboSearchDetailProcessOperationType_ValueChanged);
            // 
            // uLabelSearchDetailProcessOperationType
            // 
            this.uLabelSearchDetailProcessOperationType.Location = new System.Drawing.Point(291, 56);
            this.uLabelSearchDetailProcessOperationType.Name = "uLabelSearchDetailProcessOperationType";
            this.uLabelSearchDetailProcessOperationType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchDetailProcessOperationType.TabIndex = 265;
            this.uLabelSearchDetailProcessOperationType.Text = "공정구분";
            // 
            // uComboSearchInspectResult
            // 
            this.uComboSearchInspectResult.Location = new System.Drawing.Point(381, 80);
            this.uComboSearchInspectResult.Name = "uComboSearchInspectResult";
            this.uComboSearchInspectResult.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchInspectResult.TabIndex = 264;
            this.uComboSearchInspectResult.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectResult
            // 
            this.uLabelSearchInspectResult.Location = new System.Drawing.Point(291, 80);
            this.uLabelSearchInspectResult.Name = "uLabelSearchInspectResult";
            this.uLabelSearchInspectResult.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchInspectResult.TabIndex = 263;
            this.uLabelSearchInspectResult.Text = "판정구분";
            // 
            // uComboSearchProcInspectType
            // 
            this.uComboSearchProcInspectType.Location = new System.Drawing.Point(778, 56);
            this.uComboSearchProcInspectType.Name = "uComboSearchProcInspectType";
            this.uComboSearchProcInspectType.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchProcInspectType.TabIndex = 262;
            this.uComboSearchProcInspectType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProcInspectType
            // 
            this.uLabelSearchProcInspectType.Location = new System.Drawing.Point(689, 56);
            this.uLabelSearchProcInspectType.Name = "uLabelSearchProcInspectType";
            this.uLabelSearchProcInspectType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchProcInspectType.TabIndex = 261;
            this.uLabelSearchProcInspectType.Text = "공정검사구분";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(99, 32);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(183, 21);
            this.uComboSearchPackage.TabIndex = 260;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(10, 32);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackage.TabIndex = 259;
            this.uLabelSearchPackage.Text = "Package";
            // 
            // uComoSearchPackageGroup1
            // 
            this.uComoSearchPackageGroup1.Location = new System.Drawing.Point(381, 32);
            this.uComoSearchPackageGroup1.Name = "uComoSearchPackageGroup1";
            this.uComoSearchPackageGroup1.Size = new System.Drawing.Size(103, 21);
            this.uComoSearchPackageGroup1.TabIndex = 258;
            this.uComoSearchPackageGroup1.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup1
            // 
            this.uLabelSearchPackageGroup1.Location = new System.Drawing.Point(291, 32);
            this.uLabelSearchPackageGroup1.Name = "uLabelSearchPackageGroup1";
            this.uLabelSearchPackageGroup1.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackageGroup1.TabIndex = 257;
            this.uLabelSearchPackageGroup1.Text = "Package1";
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(579, 8);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchProductActionType.TabIndex = 256;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProductActionType
            // 
            this.uLabelSearchProductActionType.Location = new System.Drawing.Point(490, 8);
            this.uLabelSearchProductActionType.Name = "uLabelSearchProductActionType";
            this.uLabelSearchProductActionType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchProductActionType.TabIndex = 255;
            this.uLabelSearchProductActionType.Text = "제품구분";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(381, 8);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchCustomer.TabIndex = 254;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(291, 8);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 253;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uDateSearchInspectDateTo
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateTo.Appearance = appearance17;
            this.uDateSearchInspectDateTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateTo.Location = new System.Drawing.Point(195, 8);
            this.uDateSearchInspectDateTo.Name = "uDateSearchInspectDateTo";
            this.uDateSearchInspectDateTo.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchInspectDateTo.TabIndex = 251;
            // 
            // uDateSearchInspectDateFrom
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateFrom.Appearance = appearance27;
            this.uDateSearchInspectDateFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateFrom.Location = new System.Drawing.Point(99, 8);
            this.uDateSearchInspectDateFrom.Name = "uDateSearchInspectDateFrom";
            this.uDateSearchInspectDateFrom.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchInspectDateFrom.TabIndex = 252;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(185, 16);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(10, 8);
            this.ultraLabel2.TabIndex = 250;
            this.ultraLabel2.Text = "~";
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(10, 8);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchInspectDate.TabIndex = 249;
            this.uLabelSearchInspectDate.Text = "검사일";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridProcInspectList
            // 
            this.uGridProcInspectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcInspectList.DisplayLayout.Appearance = appearance28;
            this.uGridProcInspectList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcInspectList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectList.DisplayLayout.GroupByBox.Appearance = appearance29;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspectList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridProcInspectList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspectList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridProcInspectList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcInspectList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcInspectList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcInspectList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridProcInspectList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcInspectList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcInspectList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridProcInspectList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcInspectList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridProcInspectList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridProcInspectList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcInspectList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcInspectList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridProcInspectList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcInspectList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridProcInspectList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcInspectList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcInspectList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcInspectList.Location = new System.Drawing.Point(0, 150);
            this.uGridProcInspectList.Name = "uGridProcInspectList";
            this.uGridProcInspectList.Size = new System.Drawing.Size(909, 690);
            this.uGridProcInspectList.TabIndex = 2;
            this.uGridProcInspectList.Text = "ultraGrid1";
            // 
            // uTextSearchEquipCode
            // 
            appearance15.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(579, 81);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(103, 21);
            this.uTextSearchEquipCode.TabIndex = 289;
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // uLabelSearchEquipCode
            // 
            this.uLabelSearchEquipCode.Location = new System.Drawing.Point(490, 82);
            this.uLabelSearchEquipCode.Name = "uLabelSearchEquipCode";
            this.uLabelSearchEquipCode.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchEquipCode.TabIndex = 291;
            this.uLabelSearchEquipCode.Text = "设备";
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(778, 80);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(103, 21);
            this.uComboSearchInspectItem.TabIndex = 293;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(689, 80);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(86, 20);
            this.ultraLabel1.TabIndex = 292;
            this.ultraLabel1.Text = "检查项目";
            // 
            // frmSTA0067
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridProcInspectList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0067";
            this.Load += new System.EventHandler(this.frmSTA0067_Load);
            this.Activated += new System.EventHandler(this.frmSTA0067_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0067_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComoSearchPackageGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspectList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductActionType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcInspectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComoSearchPackageGroup1;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup1;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcessOperationType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcessOperationType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectResult;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectResult;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomerProductSpec;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcInspectList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComoSearchPackageGroup2;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComoSearchPackageGroup3;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectItem;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}