﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0068.cs                                         */
/* 프로그램명   : CCS현황                                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-12-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0069 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0069()
        {
            InitializeComponent();
        }

        private void frmSTA0069_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0069_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("CCS현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        private void frmSTA0069_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드


        // Label 초기화

        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchReqDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 콤보박스 초기화 
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                //검색조건 - 고객 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer"); // 2
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer(); // 3
                brwChannel.mfCredentials(clsCustomer); // 4
                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "CustomerCode", "CustomerName", dtCustomer);

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);

                //검색조건 - 공정Type 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process"); // 2
                QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process(); // 3
                brwChannel.mfCredentials(clsProc); // 4
                DataTable dtProc = clsProc.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));
                wCombo.mfSetComboEditor(this.uComboSearchDetailProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "DetailProcessOperationType", "ComboName", dtProc);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화
        private void InitEtc()
        {
            try
            {
                this.uDateSearchReqDateFrom.Value = DateTime.Now.ToString("yyyy-MM-dd").Substring(0, 8) + "01";
                this.uDateSearchReqDateTo.Value = DateTime.Now.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Grid 초기화
        private void InitGrid()
        {
            try
            {
                this.uGridCCSList.DisplayLayout.GroupByBox.Hidden = true;
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //메뉴별 권한정보 그리드 기본설정
                wGrid.mfInitGeneralGrid(this.uGridCCSList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //맨앞에 선택 컬럼 제거
                uGridCCSList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

                // 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridCCSList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridCCSList, 0, "ProcessName", "공정명", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridCCSList, 0, "ProductActionType", "제품구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridCCSList, 0, "CustomerCode", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridCCSList, 0, "GUBUN", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //총계 컬럼 추가.//
                wGrid.mfSetGridColumn(this.uGridCCSList, 0, "Total", "총계", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //총계를 구한다.//
                for (int i = 0; i < this.uGridCCSList.Rows.Count; i++)
                {
                    string strGubun = this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString();
                    double dblSum = 0;
                    //각 일자에 대한 총계를 구하여, 총계 열에 보여준다.
                    if (strGubun == "1.의뢰건수" || strGubun.Substring(0,5) == "ZZZ1.")
                    {
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1; j++)
                        {
                            if (this.uGridCCSList.Rows[i].Cells[j].Value.ToString() != "")
                                dblSum = dblSum + Convert.ToDouble(this.uGridCCSList.Rows[i].Cells[j].Value.ToString());
                        }
                        this.uGridCCSList.Rows[i].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Value = dblSum.ToString();
                    }
                    else if (strGubun == "2.불량건수" || strGubun.Substring(0, 5) == "ZZZ2.")
                    {
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1; j++)
                        {
                            if (this.uGridCCSList.Rows[i].Cells[j].Value.ToString() != "")
                                dblSum = dblSum + Convert.ToDouble(this.uGridCCSList.Rows[i].Cells[j].Value.ToString());
                        }
                        this.uGridCCSList.Rows[i].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Value = dblSum.ToString();
                    }
                    else if (strGubun == "3.합격률" || strGubun.Substring(0, 5) == "ZZZ3.")
                    {
                        int intReqCount = 0;
                        int intFaultCount = 0;
                        //쿼리에서는 합격률의 합계가 나오므로 합격률을 다시 구한다.//
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1; j++)
                        {
                            if (this.uGridCCSList.Rows[i - 2].Cells[j].Value != DBNull.Value)
                            {
                                intReqCount = Convert.ToInt32(this.uGridCCSList.Rows[i - 2].Cells[j].Value);
                                intFaultCount = Convert.ToInt32(this.uGridCCSList.Rows[i - 1].Cells[j].Value);
                                this.uGridCCSList.Rows[i].Cells[j].Value = 100 * (intReqCount - intFaultCount) / intReqCount;
                            }
                        }

                        intReqCount = Convert.ToInt32(this.uGridCCSList.Rows[i-2].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Value);
                        intFaultCount = Convert.ToInt32(this.uGridCCSList.Rows[i - 1].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Value);
                        this.uGridCCSList.Rows[i].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Value = 100* (intReqCount - intFaultCount) / intReqCount;
                    }
                }

                //일별 열에 대한 Format 지정
                for (int i = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; i < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; i++)
                {
                    this.uGridCCSList.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
                    this.uGridCCSList.DisplayLayout.Bands[0].Columns[i].MaskInput = "n,nnn.nn";
                    this.uGridCCSList.DisplayLayout.Bands[0].Columns[i].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                }

                //합계 행에 대해 색깔 적용
                for (int i = 0; i < this.uGridCCSList.Rows.Count; i++)
                {
                    //고객사 합계인 경우
                    if (!this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["ProcessName"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString().Contains("ZZZ"))
                    {
                        this.uGridCCSList.Rows[i].Hidden = true;
                        //for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index - 1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                        //{
                        //    this.uGridCCSList.Rows[i].Cells[j].Appearance.BackColor = Color.LemonChiffon;
                        //    this.uGridCCSList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        //}
                    }

                    //제품구분 합계인 경우
                    if (!this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["ProcessName"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString().Contains("ZZZ"))
                    {
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value = "Total";
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index-1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.BackColor = Color.PeachPuff;
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }

                    //공정명 합계인 경우
                    if (!this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Contains("ZZZ") &&
                        !this.uGridCCSList.Rows[i].Cells["ProcessName"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString().Contains("ZZZ"))
                    {
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value = "Total";
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index-2; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.BackColor = Color.PaleTurquoise;
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }

                    //공정Type 합계인 경우
                    if (!this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["ProcessName"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString().Contains("ZZZ"))
                    {
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value = "Total";
                        this.uGridCCSList.Rows[i].Cells["ProcessName"].Value = "Total";
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index - 3; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.BackColor = Color.PaleGreen;
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }

                    //총계 합계인 경우
                    if (this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["ProcessName"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value.ToString().Contains("ZZZ") &&
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value.ToString().Contains("ZZZ"))
                    {
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "";
                        this.uGridCCSList.Rows[i].Cells["CustomerCode"].Value = "";
                        this.uGridCCSList.Rows[i].Cells["ProductActionType"].Value = "";
                        this.uGridCCSList.Rows[i].Cells["ProcessName"].Value = "";
                        this.uGridCCSList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value = "Grand Total";
                        for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index - 4; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridCCSList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }

                    //마지막 열 색깔 적용
                    this.uGridCCSList.Rows[i].Cells[this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.MistyRose;

                    if (i % 3 == 0)
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "1.의뢰건수";
                    else if (i % 3 == 1)
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "2.불량건수";
                    else if (i % 3 == 2)
                        this.uGridCCSList.Rows[i].Cells["GUBUN"].Value = "3.합격률";
                }

                //마지막 열은 총불량건수 / 총의뢰건수 로 나타내기 위해 재계산한다.
                int intLastRowIndex = this.uGridCCSList.Rows.Count - 1;
                for (int j = this.uGridCCSList.DisplayLayout.Bands[0].Columns["GUBUN"].Index + 1; j < this.uGridCCSList.DisplayLayout.Bands[0].Columns.Count; j++)
                {
                    //의뢰건수가 있는 경우
                    if (this.uGridCCSList.Rows[intLastRowIndex-2].Cells[j].Value.ToString() != "" && this.uGridCCSList.Rows[intLastRowIndex-1].Cells[j].Value.ToString() != "")
                    {
                        int intReqCount = Convert.ToInt32(this.uGridCCSList.Rows[intLastRowIndex-2].Cells[j].Value);
                        int intFaultCount = Convert.ToInt32(this.uGridCCSList.Rows[intLastRowIndex-1].Cells[j].Value);
                        if (intReqCount > 0)
                            this.uGridCCSList.Rows[intLastRowIndex].Cells[j].Value = 100 * (intReqCount - intFaultCount) / intReqCount;

                    }
                }

                //일자 열에 대한 표시//
                this.uGridCCSList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridCCSList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();            	
            }
            finally
            {
            }
        }

        #endregion

        private void uComboSearchDetailProcessOperationType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strDetailProcType = this.uComboSearchDetailProcessOperationType.Value.ToString();

                //검색조건 - 공정코드
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsAriseProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsAriseProcess);

                DataTable dtProcess = clsAriseProcess.mfReadProcessWithDetailProcessOperationType(strPlantCode, strDetailProcType, m_resSys.GetString("SYS_LANG"));
                WinComboGrid combo = new WinComboGrid();
                combo.mfInitGeneralComboGrid(this.uComboSearchProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboSearchProcessCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessCode", "공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessName", "공정명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                this.uComboSearchProcessCode.DataSource = dtProcess;
                this.uComboSearchProcessCode.DataBind();

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region IToolbar
        public void mfCreate()
        {

        }

        public void mfDelete()
        {

        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridCCSList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridCCSList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "엑셀다운로드", "그리드 엑셀 다운로드", "엑셀로 다운로드 받을 내용이 없습니다.", Infragistics.Win.HAlign.Center);
                }
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfSave()
        {

        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strFromReqDate = Convert.ToDateTime(this.uDateSearchReqDateFrom.Value).ToString("yyyy-MM-dd");
                string strToReqDate = Convert.ToDateTime(this.uDateSearchReqDateTo.Value).ToString("yyyy-MM-dd");
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strDProcOperationType = this.uComboSearchDetailProcessOperationType.Value.ToString();
                string strProcessCode = "";
                if (this.uComboSearchProcessCode.SelectedRow != null)
                    strProcessCode = this.uComboSearchProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsReport.mfReadINSProcInspect_frmSTA0069(strPlantCode, strFromReqDate, strToReqDate, strCustomerCode, strProductActionType,
                                                                               strDProcOperationType, strProcessCode);

                //일자별로 동적으로 컬럼이 늘어나도록 Binding하기 위해 그리드를 초기화 함.
                this.uGridCCSList.DataSource = null;
                this.uGridCCSList.ResetDisplayLayout();
                this.uGridCCSList.Layouts.Clear();

                this.uGridCCSList.DataSource = dtSearch;
                this.uGridCCSList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (uGridCCSList.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                else
                {
                    //DataTable을 바인딩하고 그리드 내용을 수정을하면 DataTable내용도 수정이되어 챠트적용하고 그리드속성을 변경함.
                    ChangeGridColumn(0);
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridCCSList, 0);

                    #region 꺽은선 그래프 적용 부분
                    DataTable dtBindValue = new DataTable();    //측정값
                    dtBindValue.Columns.Add("Label", typeof(string));
                    //일자 열을 동적으로 증가
                    DateTime date = Convert.ToDateTime(strFromReqDate);
                    while (date < Convert.ToDateTime(strToReqDate))
                    {
                        dtBindValue.Columns.Add(date.ToString("MM-dd"), typeof(double));
                        date = date.AddDays(1);
                    }
                    
                    //////공정Type을 전체로 선택한 경우 공정Type에 대한 합격류를 챠트에 보여준다.
                    ////if (strDProcOperationType == "")
                    ////{
                    DataRow[] drChart = dtSearch.Select("DETAILPROCESSOPERATIONTYPE <> 'ZZZ3.합격률합계' AND ProcessName = 'ZZZ3.합격률합계' AND ProductActionType = 'ZZZ3.합격률합계' AND CustomerCode = 'ZZZ3.합격률합계' AND GUBUN LIKE 'ZZZ3.%'");
                    for (int i = 0; i < drChart.Length; i++)
                    {
                        DataRow dr = dtBindValue.NewRow();
                        dr["Label"] = drChart[i]["DETAILPROCESSOPERATIONTYPE"];
                        for (int j = 1; j < dtBindValue.Columns.Count; j++)
                        {
                            if (drChart[i][5 + j-1] != System.DBNull.Value)
                                dr[j] = Convert.ToDouble(drChart[i][5 + j-1]);
                        }
                        dtBindValue.Rows.Add(dr);
                    }
                    ////}
                    ////else
                    ////{
                    ////    DataRow[] drChart = dtSearch.Select("DETAILPROCESSOPERATIONTYPE <> 'ZZZ3.합격률합계' AND ProcessName <> 'ZZZ3.합격률합계' AND ProductActionType = 'ZZZ3.합격률합계' AND CustomerCode = 'ZZZ3.합격률합계' AND GUBUN LIKE 'ZZZ3.%'");
                    ////    for (int i = 0; i < drChart.Length; i++)
                    ////    {
                    ////        DataRow dr = dtBindValue.NewRow();
                    ////        dr["Label"] = drChart[i]["ProductActionType"];
                    ////        for (int j = 0; j < dtBindValue.Columns.Count; j++)
                    ////        {
                    ////            if (drChart[i][5 + j] != System.DBNull.Value)
                    ////                dr[j + 1] = Convert.ToDouble(drChart[i][5 + j]);
                    ////        }
                    ////        dtBindValue.Rows.Add(dr);
                    ////    }
                    ////}
                    //챠트 유형 지정
                    uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                    uChart.AreaChart.NullHandling = Infragistics.UltraChart.Shared.Styles.NullHandling.DontPlot;

                    //Line챠트의 Line 속성 지정
                    Infragistics.UltraChart.Resources.Appearance.LineAppearance ValueLineApp = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                    ValueLineApp.Thickness = 4;
                    ValueLineApp.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                    ValueLineApp.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.Square;
                    //ValueLineApp.IconAppearance.PE.Fill = System.Drawing.Color.Black;
                    ValueLineApp.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                    ValueLineApp.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Auto;
                    uChart.LineChart.LineAppearances.Add(ValueLineApp);

                    //범례 유형 지정
                    uChart.Legend.Visible = true;
                    uChart.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Right;
                    uChart.Legend.Margins.Left = 0;
                    uChart.Legend.Margins.Right = 0;
                    uChart.Legend.Margins.Top = 0;
                    uChart.Legend.Margins.Bottom = 0;
                    uChart.Legend.SpanPercentage = 10;

                    //////공정Type을 전체로 선택한 경우 공정Type에 대한 챠트제목을 보여준다.
                    ////if (strDProcOperationType == "")
                    ////{
                        uChart.TitleTop.Text = "공정Type별 CCS합격률";
                        uChart.TitleTop.HorizontalAlign = StringAlignment.Center;
                        uChart.TitleTop.VerticalAlign = StringAlignment.Center;
                        uChart.TitleTop.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 12, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point);
                        uChart.TitleTop.Margins.Left = 0;
                        uChart.TitleTop.Margins.Right = 0;
                        uChart.TitleTop.Margins.Top = 0;
                        uChart.TitleTop.Margins.Bottom = 0;
                    ////}
                    ////else
                    ////{
                    ////    uChart.TitleTop.Text = "(" + strDProcOperationType + ") 제품유형별 CCS합격률";
                    ////    uChart.TitleTop.HorizontalAlign = StringAlignment.Center;
                    ////    uChart.TitleTop.VerticalAlign = StringAlignment.Center;
                    ////    uChart.TitleTop.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 12, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point);
                    ////    uChart.TitleTop.Margins.Left = 0;
                    ////    uChart.TitleTop.Margins.Right = 0;
                    ////    uChart.TitleTop.Margins.Top = 0;
                    ////    uChart.TitleTop.Margins.Bottom = 0;
                    ////}
                    //X축 제목
                    Infragistics.UltraChart.Resources.Appearance.AxisItem axisX = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                    axisX.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                    axisX.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                    axisX.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;   // 라인으로 그리는 경우 ContinuousData, 막대로 그리는 경우 GroupBySeries
                    axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
                    axisX.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                    axisX.Labels.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                    axisX.Extent = 5;
                    uChart.TitleBottom.Text = "의뢰일";
                    uChart.TitleBottom.HorizontalAlign = StringAlignment.Center;
                    uChart.TitleBottom.VerticalAlign = StringAlignment.Center;
                    uChart.TitleBottom.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                    uChart.TitleBottom.Margins.Left = 0;
                    uChart.TitleBottom.Margins.Right = 0;
                    uChart.TitleBottom.Margins.Top = 5;
                    uChart.TitleBottom.Margins.Bottom = 5;
                    uChart.TitleBottom.Visible = true;

                    //Y축 제목
                    Infragistics.UltraChart.Resources.Appearance.AxisItem axisY = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                    axisY.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                    axisY.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                    axisY.Labels.ItemFormatString = "<DATA_VALUE:0.#>";
                    axisY.Labels.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                    axisY.Extent = 5;
                    uChart.TitleLeft.Text = "합격률(%)";
                    uChart.TitleLeft.HorizontalAlign = StringAlignment.Center;
                    uChart.TitleLeft.VerticalAlign = StringAlignment.Center;
                    uChart.TitleLeft.Font = new Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                    uChart.TitleLeft.Margins.Left = 5;
                    uChart.TitleLeft.Margins.Right = 5;
                    uChart.TitleLeft.Margins.Top = 0;
                    uChart.TitleLeft.Margins.Bottom = 0;
                    uChart.TitleLeft.Visible = true;
                    uChart.LineChart.HighLightLines = true;
                    //uChart.LineChart
                    if (dtBindValue.Rows.Count > 0)
                    {
                        uChart.DataSource = dtBindValue;
                        uChart.DataBind();
                    }
                    #endregion
                }


            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
    }
}
