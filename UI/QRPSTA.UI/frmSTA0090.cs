﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0090.cs                                         */
/* 프로그램명   : 수입 품질목표 등록/조회                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-18                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0090 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0090()
        {
            InitializeComponent();
        }

        #region Form Event

        private void frmSTA0090_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0090_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0090_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("수입 품질목표 등록/조회", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 사용자-화면툴바 권한 설정
            SetToolAuth();

            // 초기화 메소드 호출
            InitLabel();
            InitComboBox();
            InitEtc();
            InitGrid();

            WinGrid wGrid = new WinGrid();
            wGrid.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.MatTarget), "MatTarget");
                QRPSTA.BL.STAIMP.MatTarget clsMTar = new QRPSTA.BL.STAIMP.MatTarget();
                brwChannel.mfCredentials(clsMTar);

                DataTable dtMatTarList = clsMTar.mfSetDataInfo();
                DataRow drRow;

                this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstRowInGrid);

                // 필수사항 입력확인 및 삭제용 데이터 테이블 생성
                for (int i = 0; i < this.uGridMatTargetList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridMatTargetList.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        ////if (this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value == null ||
                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value == DBNull.Value ||
                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 공장을 선택해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Activate();
                        ////    this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        ////    return;
                        ////}
                        ////else if (this.uGridMatTargetList.Rows[i].Cells["AYear"].Value == null ||
                        ////        this.uGridMatTargetList.Rows[i].Cells["AYear"].Value == DBNull.Value ||
                        ////        this.uGridMatTargetList.Rows[i].Cells["AYear"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 년도를 입력해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridMatTargetList.Rows[i].Cells["AYear"].Activate();
                        ////    this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}

                        if (this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value == null ||
                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value == DBNull.Value ||
                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수사항 입력확인"
                                                    , (i + 1).ToString() + "번째 줄의 분기를 선택해 주세요."
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Activate();
                            this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value == null ||
                                this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value == DBNull.Value ||
                                this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수사항 입력확인"
                                                    , (i + 1).ToString() + "번째 줄의 자재종류를 선택해 주세요."
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridMatTargetList.Rows[i].Cells["DetailProcessOperationType"].Activate();
                            this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            drRow = dtMatTarList.NewRow();
                            drRow["PlantCode"] = this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["AYear"] = this.uGridMatTargetList.Rows[i].Cells["AYear"].Value.ToString();
                            drRow["AQuarter"] = this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value.ToString();
                            drRow["ConsumableTypeCode"] = this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value.ToString();
                            dtMatTarList.Rows.Add(drRow);
                        }
                    }
                }

                if (dtMatTarList.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "삭제확인", "입력한 정보를 삭제하시겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsMTar.mfDeleteINSMatTarget(dtMatTarList);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "삭제처리결과", "입력한 정보를 성공적으로 삭제했습니다.",
                                                Infragistics.Win.HAlign.Right);

                            // List Refresh
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
                                                Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "삭제처리결과", ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "삭제 데이터 확인"
                                                    , "삭제할 정보가 없습니다. 삭제할 정보를 선택하신 후 삭제버튼을 눌러 주세요."
                                                    , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.MatTarget), "MatTarget");
                QRPSTA.BL.STAIMP.MatTarget clsMTar = new QRPSTA.BL.STAIMP.MatTarget();
                brwChannel.mfCredentials(clsMTar);

                DataTable dtMatTarList = clsMTar.mfSetDataInfo();
                DataRow drRow;

                this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstRowInGrid);

                // 필수사항 입력확인 및 저장용 데이터 테이블 생성
                for (int i = 0; i < this.uGridMatTargetList.Rows.Count; i++)
                {
                    if (this.uGridMatTargetList.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        ////if (this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value == null ||
                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value == DBNull.Value ||
                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 공장을 선택해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Activate();
                        ////    this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        ////    return;
                        ////}
                        ////else if (this.uGridMatTargetList.Rows[i].Cells["AYear"].Value == null ||
                        ////        this.uGridMatTargetList.Rows[i].Cells["AYear"].Value == DBNull.Value ||
                        ////        this.uGridMatTargetList.Rows[i].Cells["AYear"].Value.ToString().Equals(string.Empty))
                        ////{
                        ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        ////                            , "확인창", "필수사항 입력확인"
                        ////                            , (i + 1).ToString() + "번째 줄의 년도를 입력해 주세요."
                        ////                            , Infragistics.Win.HAlign.Right);

                        ////    this.uGridMatTargetList.Rows[i].Cells["AYear"].Activate();
                        ////    this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}

                        if (this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value == null ||
                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value == DBNull.Value ||
                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수사항 입력확인"
                                                    , (i + 1).ToString() + "번째 줄의 분기를 선택해 주세요."
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Activate();
                            this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value == null ||
                                this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value == DBNull.Value ||
                                this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수사항 입력확인"
                                                    , (i + 1).ToString() + "번째 줄의 자재종류를 선택해 주세요."
                                                    , Infragistics.Win.HAlign.Right);

                            this.uGridMatTargetList.Rows[i].Cells["DetailProcessOperationType"].Activate();
                            this.uGridMatTargetList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            drRow = dtMatTarList.NewRow();
                            drRow["PlantCode"] = this.uGridMatTargetList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["AYear"] = this.uGridMatTargetList.Rows[i].Cells["AYear"].Value.ToString();
                            drRow["AQuarter"] = this.uGridMatTargetList.Rows[i].Cells["AQuarter"].Value.ToString();
                            drRow["ConsumableTypeCode"] = this.uGridMatTargetList.Rows[i].Cells["ConsumableTypeCode"].Value.ToString();
                            drRow["Target"] = ReturnDecimalValue(this.uGridMatTargetList.Rows[i].Cells["Target"].Value.ToString());
                            drRow["EtcDesc"] = this.uGridMatTargetList.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dtMatTarList.Rows.Add(drRow);
                        }
                    }
                }

                if (dtMatTarList.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "저장확인", "입력한 정보를 저장하시겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsMTar.mfSaveINSMatTarget(dtMatTarList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                Infragistics.Win.HAlign.Right);

                            // List Refresh
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "처리결과", "저장처리결과", ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "저장 데이터 확인"
                                                    , "저장할 정보가 없습니다. 신규 작성 혹은 수정 후 저장버튼을 눌러주세요."
                                                    , Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 필수조건 확인
                if (this.uComboSearchPlant.Value == null || this.uComboSearchPlant.Value == DBNull.Value || this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검색조건 입력확인"
                                                , "공장을 선택해 주세요."
                                                , Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Length < 4)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "검색조건 입력확인"
                                                , "조회년도 4자리를 입력해 주세요."
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else
                {
                    // 검색조건 변수 설정
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strQuarter = this.uComboSearchQuarter.Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.MatTarget), "MatTarget");
                    QRPSTA.BL.STAIMP.MatTarget clsMTar = new QRPSTA.BL.STAIMP.MatTarget();
                    brwChannel.mfCredentials(clsMTar);

                    DataTable dtMatTargetList = clsMTar.mfReadINSMatTarget(strPlantCode, strYear, strQuarter, m_resSys.GetString("SYS_LANG"));

                    this.uGridMatTargetList.DataSource = dtMatTargetList;
                    this.uGridMatTargetList.DataBind();

                    // 중복방지 PrimaryKey 설정
                    DataColumn[] dc = new DataColumn[5];
                    dc[0] = dtMatTargetList.Columns["PlantCode"];
                    dc[1] = dtMatTargetList.Columns["AYear"];
                    dc[2] = dtMatTargetList.Columns["AQuarter"];
                    dc[3] = dtMatTargetList.Columns["ConsumableTypeCode"];

                    ////dtMatTargetList.PrimaryKey = dc;

                    if (dtMatTargetList.Rows.Count > 0)
                    {
                        WinGrid wGrid = new WinGrid();
                        wGrid.mfSetAutoResizeColWidth(this.uGridMatTargetList, 0);
                    }

                    // 공장콤보가 전체이면 저장버튼 비활성화
                    if (strPlantCode.Equals(string.Empty))
                    {
                        // 그리드 편집불가상태로
                        this.uGridMatTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        this.uGridMatTargetList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    }
                    else
                    {
                        // 그리드 편집가능상태로
                        this.uGridMatTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                        this.uGridMatTargetList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    }

                    // 그리드 기본값 변경
                    this.uGridMatTargetList.DisplayLayout.Bands[0].Columns["PlantCode"].DefaultCellValue = this.uComboSearchPlant.Value;
                    this.uGridMatTargetList.DisplayLayout.Bands[0].Columns["AYear"].DefaultCellValue = this.uTextSearchYear.Text;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchQuarter, "분기", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                // 분기 콤보 설정
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrText = new System.Collections.ArrayList();

                string strQuarter = Math.Ceiling((Convert.ToDecimal(DateTime.Now.Month) / 3.0m)).ToString();

                arrKey.Add("");
                arrText.Add("전체");
                for (int i = 1; i < 5; i++)
                {
                    arrKey.Add(i);
                    arrText.Add(i.ToString() + " / 4");
                }

                wCombo.mfSetComboEditor(this.uComboSearchQuarter, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , strQuarter, arrKey, arrText);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 분기 기본값
                string strQuarter = Math.Ceiling((Convert.ToDecimal(DateTime.Now.Month) / 3.0m)).ToString();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridMatTargetList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Columns 설정
                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "AYear", "년도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, true, false, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", DateTime.Now.Year.ToString());

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "AQuarter", "분기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", strQuarter);

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "Target", "목표불량률", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnnnnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridMatTargetList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 목표불량율 Column PromptChar 설정
                this.uGridMatTargetList.DisplayLayout.Bands[0].Columns["Target"].PromptChar = ' ';

                // FontSize 설정
                this.uGridMatTargetList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridMatTargetList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // 공장
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridMatTargetList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                // 분기
                DataTable dtQuarter = new DataTable();
                dtQuarter.Columns.Add("Key", typeof(string));
                dtQuarter.Columns.Add("Value", typeof(string));
                DataRow drRow;
                for (int i = 1; i < 5; i++)
                {
                    drRow = dtQuarter.NewRow();
                    drRow["Key"] = i.ToString();
                    drRow["Value"] = i.ToString() + " / 4";
                    dtQuarter.Rows.Add(drRow);
                }

                wGrid.mfSetGridColumnValueList(this.uGridMatTargetList, 0, "AQuarter", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtQuarter);

                // 자재종류
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsCon = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsCon);

                DataTable dtConsumableType = clsCon.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridMatTargetList, 0, "ConsumableTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtConsumableType);

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridMatTargetList, 0);

                // 그리드 편집불가상태로
                this.uGridMatTargetList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // 데이터 중복방지 설정
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.MatTarget), "MatTarget");
                QRPSTA.BL.STAIMP.MatTarget clsMTar = new QRPSTA.BL.STAIMP.MatTarget();
                brwChannel.mfCredentials(clsMTar);

                DataTable dtPrimary = clsMTar.mfSetDataInfo();

                this.uGridMatTargetList.DataSource = dtPrimary;
                this.uGridMatTargetList.DataBind();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 조회년도 TextBox 설정
                this.uTextSearchYear.MaxLength = 4;
                this.uTextSearchYear.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                this.uTextSearchYear.Text = DateTime.Now.Year.ToString();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 조회연도 숫자만 입력가능하게 하는 이벤트
        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMatTargetList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // RowSelector Image 설정
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMatTargetList_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            try
            {
                if (e.ErrorType.ToString().Equals("Data"))
                {
                    e.Cancel = true;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "중복확인", "입력 데이터 중복 Error"
                                        , e.DataErrorInfo.Row.Cells["PlantCode"].Text + " 공장의 " +
                                        e.DataErrorInfo.Row.Cells["AYear"].Text + "년도 " + e.DataErrorInfo.Row.Cells["AQuarter"].Text + "분기"+"<br/><br/>" +
                                        "자재종류 : " + e.DataErrorInfo.Row.Cells["ConsumableTypeCode"].Text + "<br/><br/>" +
                                        "항목은 이미 있습니다.", Infragistics.Win.HAlign.Right);

                    e.DataErrorInfo.Row.Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }
    }
}
