﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;
using System.Globalization;

//Infragistics
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Diagnostics;





namespace QRPSTA.UI
{
    public partial class frmSTA0077 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0077()
        {
            InitializeComponent();
        }

        private void frmSTA0077_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0077_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("주별 품질지수", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitComboBox();
            InitGrid();
            InitLabel();
            InitYear();
            InitGroupBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        /// <summary>
        /// 사용권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 폼 종료시 그리드 위치 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0077_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //폼 리사이즈시 컴포넌트 크기 조절
        private void frmSTA0077_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxWeekly1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                    uGroupBoxWeekly2.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                }
                else
                {
                    uGroupBoxWeekly1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uGroupBoxWeekly2.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인잉
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", ""
                    , "PlantCode", "PlantName", dtPlant);

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //Label 초기화
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchYear, "년", m_resSys.GetString("SYS_LANG"), true, true);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// 검색툴바 연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //상단 그리드
                wGrid.mfInitGeneralGrid(this.uGridWeekly1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "CustomerName", "고객", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                //1주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1stWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "1stWeek", "1W", 4, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W1SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup1stWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W1FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup1stWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W1FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup1stWeek);

                //2주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup2ndWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "2ndWeek", "2W", 7, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W2SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup2ndWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W2FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup2ndWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W2FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup2ndWeek);

                //3주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup3rdWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "3rdWeek", "3W", 10, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W3SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup3rdWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W3FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup3rdWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W3FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup3rdWeek);

                //4주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup4thWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "4thWeek", "4W", 13, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W4SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup4thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W4FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup4thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W4FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup4thWeek);

                //5주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup5thWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "5thWeek", "5W", 16, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W5SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup5thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W5FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup5thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "W5FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup5thWeek);

                //Total
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "Total", "Total", 19, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);

                wGrid.mfSetGridColumn(this.uGridWeekly1, 0, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal);
                
                //하단 그리드

                wGrid.mfInitGeneralGrid(this.uGridWeekly2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                //1주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1stWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup1stWeek2", "1W", 3, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W1SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup1stWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W1FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup1stWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W1FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup1stWeek2);

                //2주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup2ndWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup2ndWeek2", "2W", 6, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W2SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup2ndWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W2FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup2ndWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W2FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup2ndWeek2);

                //3주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup3rdWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup3rdWeek2", "3W", 9, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W3SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup3rdWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W3FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup3rdWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W3FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup3rdWeek2);

                //4주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup4thWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup4thWeek2", "4W", 12, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W4SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup4thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W4FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup4thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W4FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup4thWeek2);

                //5주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup5thWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup5thWeek2", "5W", 15, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup5thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup5thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup5thWeek2);

                //Total
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroupTotal2", "Total", 18, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal2);
                
                this.uGridWeekly1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void InitGroupBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox wGroup = new WinGroupBox();

                wGroup.mfSetGroupBox(this.uGroupBoxWeekly1, GroupBoxType.INFO, "고객사별 품질지수", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxWeekly2, GroupBoxType.INFO, "공정Type별 품질지수", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxWeekly1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxWeekly1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxWeekly2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxWeekly2.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void SetGridCellStatus(int intweek)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                #region uGridWeekly1 속성 변경
                //////////셀 속성 설정 SampleSize
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4SampleSize"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5SampleSize"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5SampleSize"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalSampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalSampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalSampleSize"].CellAppearance.ForeColor = Color.Black;
                //FaultQty
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultQty"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultQty"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultQty"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultQty"].CellAppearance.ForeColor = Color.Black;
                //FaultRate
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultRate"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultRate"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultRate"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultRate"].CellAppearance.ForeColor = Color.Black;
                #endregion

                #region uGridWeekly2
                //////////셀 속성 설정 SampleSize
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3SampleSize"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4SampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4SampleSize"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5SampleSize"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5SampleSize"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalSampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalSampleSize"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalSampleSize"].CellAppearance.ForeColor = Color.Black;
                //FaultQty
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultQty"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultQty"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultQty"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultQty"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultQty"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultQty"].CellAppearance.ForeColor = Color.Black;
                //FaultRate
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultRate"].CellAppearance.ForeColor = Color.Black;

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultRate"].CellAppearance.ForeColor = Color.Black;

                if (intweek == 5)
                {
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultRate"].MaskInput = "nnn,nnn,nnn";
                    this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultRate"].CellAppearance.ForeColor = Color.Black;
                }

                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultRate"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultRate"].MaskInput = "nnn,nnn,nnn";
                this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultRate"].CellAppearance.ForeColor = Color.Black;
                #endregion
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion


        #region IToolBar
        public void mfCreate()
        {

        }

        public void mfSearch()
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000881", "M000813", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else if (this.uComboSearchMonth.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000881", "M000363", Infragistics.Win.HAlign.Center);

                    this.uComboSearchMonth.DropDown();
                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                    QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                    brwChannel.mfCredentials(clsReport);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strMonth = this.uComboSearchMonth.Value.ToString();

                    DataTable dtRtn1 = clsReport.mfReadINSProcInspect_frmSTA0077_D1_PSTS(strPlantCode, strYear, strMonth, m_resSys.GetString("SYS_LANG"));
                    DataTable dtRtn2 = clsReport.mfReadINSProcInspect_frmSTA0077_D2_PSTS(strPlantCode, strYear, strMonth, m_resSys.GetString("SYS_LANG"));

                    this.uGridWeekly1.DataSource = null;
                    this.uGridWeekly1.ResetDisplayLayout();
                    this.uGridWeekly1.Layouts.Clear();

                    this.uGridWeekly2.DataSource = null;
                    this.uGridWeekly2.ResetDisplayLayout();
                    this.uGridWeekly2.Layouts.Clear();

                    ChangeGridColumn(0, dtRtn1, dtRtn2);

                    brwChannel.mfSetFormLanguage(this);

                    this.uGridWeekly1.DataSource = dtRtn1;
                    this.uGridWeekly1.DataBind();

                    this.uGridWeekly2.DataSource = dtRtn2;
                    this.uGridWeekly2.DataBind();

                    for (int i = 0; i < this.uGridWeekly1.Rows.Count; i++)
                    {
                        if (this.uGridWeekly1.Rows[i].Cells["W1SampleSize"].Value.ToString().Equals("0.00000") && this.uGridWeekly1.Rows[i].Cells["W2SampleSize"].Value.ToString().Equals("0.00000")
                            && this.uGridWeekly1.Rows[i].Cells["W3SampleSize"].Value.ToString().Equals("0.00000") && this.uGridWeekly1.Rows[i].Cells["W4SampleSize"].Value.ToString().Equals("0.00000")
                            && (this.uGridWeekly1.Rows[i].Cells["W5SampleSize"].Value.ToString().Equals(string.Empty) || this.uGridWeekly1.Rows[i].Cells["W5SampleSize"].Value.ToString().Equals("0.00000")))
                        {
                            this.uGridWeekly1.Rows[i].Hidden = true;
                        }
                    }
                    for (int i = 0; i < this.uGridWeekly2.Rows.Count; i++)
                    {
                        if (this.uGridWeekly2.Rows[i].Cells["W1SampleSize"].Value.ToString().Equals("0.00000") && this.uGridWeekly2.Rows[i].Cells["W2SampleSize"].Value.ToString().Equals("0.00000")
                            && this.uGridWeekly2.Rows[i].Cells["W3SampleSize"].Value.ToString().Equals("0.00000") && this.uGridWeekly2.Rows[i].Cells["W4SampleSize"].Value.ToString().Equals("0.00000")
                            && (this.uGridWeekly2.Rows[i].Cells["W5SampleSize"].Value.ToString().Equals(string.Empty) || this.uGridWeekly2.Rows[i].Cells["W5SampleSize"].Value.ToString().Equals("0.00000")))
                        {
                            this.uGridWeekly2.Rows[i].Hidden = true;
                        }
                        if (this.uGridWeekly2.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Equals("Sum"))
                        {
                            this.uGridWeekly2.Rows[i].CellAppearance.BackColor = Color.MistyRose;
                            this.uGridWeekly2.Rows[i].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtRtn1.Rows.Count == 0 && dtRtn2.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        //검색결과가 있을경우, 각 그리드의 Sum, Grand Sum 부분에 색을 입히고, 글씨를 크게 한다.
                        for (int i = 0; i < this.uGridWeekly1.Rows.Count; i++)
                        {
                            if (this.uGridWeekly1.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Value.ToString().Equals("Summary"))
                            {
                                this.uGridWeekly1.Rows[i].CellAppearance.BackColor = Color.MistyRose;
                                this.uGridWeekly1.Rows[i].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                            if (this.uGridWeekly1.Rows[i].Cells["CustomerName"].Value.ToString().Equals("Grand Summary"))
                            {
                                this.uGridWeekly1.Rows[i].CellAppearance.BackColor = Color.Lavender;
                                this.uGridWeekly1.Rows[i].Appearance.FontData.Bold = DefaultableBoolean.True;
                            }
                        }
                        for (int i = 0; i < this.uGridWeekly2.Rows.Count; i++)
                        {
                            if (this.uGridWeekly2.Rows[i].Cells["PRODUCTACTIONTYPE"].Value.ToString().Equals("Sum"))
                            {
                                this.uGridWeekly2.Rows[i].CellAppearance.BackColor = Color.Lavender;
                                this.uGridWeekly2.Rows[i].Appearance.FontData.Bold = DefaultableBoolean.True;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfSave()
        {

        }

        public void mfDelete()
        {

        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridWeekly1.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridWeekly1);
                }
                if (this.uGridWeekly2.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridWeekly2);
                }
                if (this.uGridWeekly1.Rows.Count.Equals(0) && this.uGridWeekly2.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfPrint()
        {

        }
        #endregion




        public void GetSummay(int intweek)
        {
            try
            {
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Clear();
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Clear();
                #region uGridWeekly1
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1SampleSize"]);

                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W1FaultQty"]);

                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2SampleSize"]);
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W2FaultQty"]);

                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3SampleSize"]);
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W3FaultQty"]);

                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4SampleSize"]);
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W4FaultQty"]);

                if (intweek == 5)
                {
                    this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                        , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5SampleSize"]);
                    this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                        , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["W5FaultQty"]);
                }
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalSampleSize"]);
                this.uGridWeekly1.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly1.DisplayLayout.Bands[0].Columns["TotalFaultQty"]);
                #endregion
                #region uGridWeekly2
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1SampleSize"]);
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W1FaultQty"]);

                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2SampleSize"]);
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W2FaultQty"]);

                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3SampleSize"]);
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W3FaultQty"]);

                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4SampleSize"]);
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W4FaultQty"]);

                if (intweek == 5)
                {
                    this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                        , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5SampleSize"]);
                    this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                        , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["W5FaultQty"]);
                }

                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalSampleSize"]);
                this.uGridWeekly2.DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                                                                                                    , this.uGridWeekly2.DisplayLayout.Bands[0].Columns["TotalFaultQty"]);
                #endregion
            }
            catch(Exception ex)
            { }
            finally
            { }
        }

        private void ChangeGridColumn(int intBandIndex, DataTable dt1, DataTable dt2)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //검색 월의 시작일, 요일, 검색월의 종료, 종료일
                //DateTime first_day = DateTime.Now;
                //int first_week_day_count = 7 - Convert.ToInt32(first_day.DayOfWeek);
                DateTime first_day = Convert.ToDateTime(this.uTextSearchYear.Text + "-" + this.uComboSearchMonth.Value.ToString() + "-01");
                DateTime last_day = first_day.AddMonths(+1).AddDays(-1);

                
                int intFirstDayWeekDate = Convert.ToInt32(first_day.DayOfWeek);
                int intLastDayWeekDate = Convert.ToInt32(last_day.DayOfWeek);


                //string strLang = m_resSys.GetString("SYS_LANG");

                //string strName = string.Empty;
                //if (strLang.Equals("KOR"))
                //{
                //    strName = "고객,공정Type,제품 구분";
                //}
                //else if(strLang.Equals("CHN"))
                //{
                //}
                //else if (strLang.Equals("ENG"))
                //{

                //}

                //상단 그리드
                wGrid.mfInitGeneralGrid(this.uGridWeekly1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "CustomerName", "고객", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                //1주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1stWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "1stWeek", "1W", 4, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W1SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup1stWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W1FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup1stWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W1FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup1stWeek);

                //2주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup2ndWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "2ndWeek", "2W", 7, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W2SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup2ndWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W2FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup2ndWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W2FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup2ndWeek);

                //3주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup3rdWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "3rdWeek", "3W", 10, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W3SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup3rdWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W3FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup3rdWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W3FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup3rdWeek);

                //4주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup4thWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "4thWeek", "4W", 13, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W4SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup4thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W4FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup4thWeek);

                wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W4FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup4thWeek);

                //if (!dt1.Rows[0]["W5SampleSize"].ToString().Equals(string.Empty) || !dt2.Rows[0]["W5SampleSize"].ToString().Equals(string.Empty))
                if(intFirstDayWeekDate < 4 && intLastDayWeekDate > 2)
                {
                    //5주차
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup5thWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "5thWeek", "5W", 16, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W5SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup5thWeek);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W5FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup5thWeek);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "W5FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup5thWeek);

                    //Total
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "Total", "Total", 19, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal);
                }
                else
                {
                    //Total
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGridWeekly1, intBandIndex, "Total", "Total", 16, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);

                    wGrid.mfSetGridColumn(this.uGridWeekly1, intBandIndex, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal);
                }
                //하단 그리드

                wGrid.mfInitGeneralGrid(this.uGridWeekly2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                //1주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1stWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroup1stWeek2", "1W", 3, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W1SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup1stWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W1FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup1stWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W1FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup1stWeek2);

                //2주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup2ndWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroup2ndWeek2", "2W", 6, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W2SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup2ndWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W2FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup2ndWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W2FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup2ndWeek2);

                //3주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup3rdWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroup3rdWeek2", "3W", 9, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W3SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup3rdWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W3FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup3rdWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W3FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup3rdWeek2);

                //4주차
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup4thWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroup4thWeek2", "4W", 12, 0, 3, 2, false);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W4SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup4thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W4FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup4thWeek2);

                wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "W4FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup4thWeek2);

                //if (!dt1.Rows[0]["W5SampleSize"].ToString().Equals(string.Empty) || !dt2.Rows[0]["W5SampleSize"].ToString().Equals(string.Empty))
                if (intFirstDayWeekDate < 4 && intLastDayWeekDate > 2)
                {
                    ////5주차
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup5thWeek2 = wGrid.mfSetGridGroup(this.uGridWeekly2, 0, "uGroup5thWeek2", "5W", 15, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5SampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroup5thWeek2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroup5thWeek2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, 0, "W5FaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroup5thWeek2);

                    //Total
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroupTotal2", "Total", 18, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal2);
                    SetGridCellStatus(5);
                }
                else
                {
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal2 = wGrid.mfSetGridGroup(this.uGridWeekly2, intBandIndex, "uGroupTotal2", "Total", 15, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalSampleSize", "검사시료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalFaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal2);

                    wGrid.mfSetGridColumn(this.uGridWeekly2, intBandIndex, "TotalFaultRate", "불량률(PPM)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupTotal2);
                    SetGridCellStatus(4);
                }
                this.uGridWeekly1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridWeekly2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridWeekly1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridWeekly2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            }
            catch(Exception ex)
            {}
            finally
            {}
        }

        /// <summary>
        /// 검색 월의 주차 계산
        /// </summary>
        /// <param name="strdate"></param>
        /// <returns></returns>
        ////public string getWeekDegree(string strdate)
        ////{
        ////    try
        ////    {

        ////        DateTime date = Convert.ToDateTime(strdate);

        ////        CultureInfo cul = CultureInfo.CurrentCulture;
        ////        CalendarWeekRule rule = cul.DateTimeFormat.CalendarWeekRule;
        ////        DayOfWeek week = cul.DateTimeFormat.FirstDayOfWeek;
        ////        int h = cul.Calendar.GetWeekOfYear(date, rule, week);

        ////        return h.ToString();
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        ShowDialog();
        ////        return strdate;
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}
    }
}
