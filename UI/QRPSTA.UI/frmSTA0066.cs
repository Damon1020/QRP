﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사통계분석                                      */
/* 프로그램ID   : frmSTA0066.cs                                         */
/* 프로그램명   : 수입능력분석                                          */
/* 작성자       : 정결                                                  */
/* 작성일자     : 2011-11-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0066 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0066()
        {
            InitializeComponent();
        }

        private void frmSTA0066_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0066_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmSTA0066_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("수입능력분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            InitComboBox();
            InitLabel();
            InitGrid();
            InitGroupBox();
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.CHART, "Histogram", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(uLabelInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelSearchMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(uLabelSearchSpec, "Spec", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelSearchVendor, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelSpecUpper, "규격상한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelSpecLower, "규격하한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelSpecRange, "규격범위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelAverage, "평균", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelStdDeviation, "표준편차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelMin, "Min", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelMax, "Max", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(uLabelCp, "Cp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelCpk, "Cpk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelCpl, "Cpl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelCpu, "Cpu", m_resSys.GetString("SYS_FONTNAME"), true, false);


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        ,false, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                        , m_resSys.GetString("SYS_PLANTCODE"), "선택", "", "PlantCode", "PlantName", dtPlant);

                // 검사항목 콤보박스
                WinComboGrid wComboG = new WinComboGrid();
                wComboG.mfInitGeneralComboGrid(this.uComboInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "ItemNum", "항목순번", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "InspectItemCode", "검사항목코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "InspectItemName", "검사항목명", false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "UpperSpec", "규격상한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "LowerSpec", "규격하한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "SpecRangeCode", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboInspectItem, 0, "SpecRangeName", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                this.uComboInspectItem.Text = "선택";


            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectTime", "시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectValue", "검사값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                return dtMaterial;
            }
            finally
            {
            }
        }

        //검사항목 콤보 설정
        private void InitInspectItemCombo()
        {
            try
            {
                if (!this.uComboSearchPlant.Value.ToString().Equals(string.Empty) && !this.uTextSearchMaterialCode.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor combo = new WinComboEditor();
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strMaterialCode = this.uTextSearchMaterialCode.Text;
                    string strSpecNo = this.uTextSearchSpecNum.Text;
                    string strVendorCode = this.uTextSearchVendorCode.Text;


                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                    QRPSTA.BL.STAIMP.ImportStaticD clsStaticD = new QRPSTA.BL.STAIMP.ImportStaticD();
                    brwChannel.mfCredentials(clsStaticD);

                    DataTable dtItem = clsStaticD.mfReadINSMatStatic_InspectItemCombo(strPlantCode, strMaterialCode, strVendorCode, strSpecNo, m_resSys.GetString("SYS_LANG"));

                    this.uComboInspectItem.DataSource = dtItem;
                    this.uComboInspectItem.DataBind();

                    if (!(dtItem.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                        , "검사항목이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
                    }

                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        // Chart초기화
        private void InitChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void InitValue()
        {
            try
            {
                this.uComboInspectItem.Value = null;
                this.uTextUpperSpec.Text = "";
                this.uTextLowerSpec.Text = "";
                this.uTextSpecRange.Text = "";
                this.uTextMean.Text = "";
                this.uTextStdDev.Text = "";
                this.uTextMin.Text  ="";
                this.uTextMax.Text = "";

                this.uTextCp.Text = "";
                this.uTextCpk.Text = "";
                this.uTextCpl.Text = "";
                this.uTextCpu.Text = "";

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                InitChart(uChartHistogram);

            }
            catch (Exception ex)
            { }
            finally
            { }
        }


        #region 검색조건 선택 이벤트
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "입력확인", "공장을 선택해주세요.",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0015 frmPOP = new frmPOP0015();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uTextSearchSpecNum.Text = frmPOP.SpecNo;

                InitInspectItemCombo();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 선택 공장 변경시 검사항목 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 자재 검색 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchMaterialCode_EditorButtonClick_1(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "입력확인", "공장을 선택해주세요.",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0015 frmPOP = new frmPOP0015();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uTextSearchSpecNum.Text = frmPOP.SpecNo;

                InitInspectItemCombo();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 자재코드 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text == "")
                    {
                        this.uTextSearchMaterialName.Text = "";
                        this.uTextSearchSpecNum.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                    this.uTextSearchSpecNum.Text = dtMaterial.Rows[i]["Spec"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "자재정보를 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                                this.uTextSearchSpecNum.Text = "";
                            }
                        }
                    }
                    InitInspectItemCombo();
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSearchMaterialCode.Text = "";
                    this.uTextSearchMaterialName.Text = "";
                    this.uTextSearchSpecNum.Text = "";
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 고객사 검색 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmVendor = new frmPOP0004();
                frmVendor.ShowDialog();

                this.uTextSearchVendorCode.Text = frmVendor.CustomerCode;
                this.uTextSearchVendorName.Text = frmVendor.CustomerName;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion

        #region 검사항목 콤보그리드 관련 이벤트
        /// <summary>
        /// 필수입력사항 확인
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboInspectItem_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공장 입력확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uComboSearchPlant.DropDown();
                }
                else if (this.uTextSearchMaterialCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "자재코드 입력확인", "자재코드를 입력해주세요.", Infragistics.Win.HAlign.Center);

                    e.Cancel = true;
                    this.uTextSearchMaterialCode.Focus();
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// String을 Double로 변경
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private Double ReturnDoubleValue(string value)
        {
            Double result = 0.0;

            if (Double.TryParse(value, out result))
                return result;
            else
                return 0.0; ;
        }

        /// <summary>
        /// 검사항목 콤보그리드 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboInspectItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 규격상/하한, 규격범위 텍스트 박스 설정
                this.uTextUpperSpec.Text = string.Format("{0:f5}", e.Row.Cells["UpperSpec"].Value);
                this.uTextLowerSpec.Text = string.Format("{0:f5}", e.Row.Cells["LowerSpec"].Value);
                this.uTextSpecRange.Text = e.Row.Cells["SpecRangeName"].Value.ToString();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uTextSearchMaterialCode.Text;
                string strSpec = this.uTextSearchSpecNum.Text;
                string strVendorCode = this.uTextSearchVendorCode.Text;
                string strInspectItemCode = e.Row.Cells["InspectItemCode"].Value.ToString();
                string strInpectDateFrom = Convert.ToDateTime(this.uDateInspectDateFrom.Value).ToString("yyyy-MM-dd 22:00:00");
                string strInspectDateTo = Convert.ToDateTime(this.uDateInspectDateTo.Value).ToString("yyyy-MM-dd 21:59:59");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsStaticD = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsStaticD);

                DataTable dtValue = clsStaticD.mfReadINSMatStatic_XMR(strPlantCode, strMaterialCode, strSpec, strInspectItemCode, strVendorCode, string.Empty
                                                                        , strInpectDateFrom, strInspectDateTo, m_resSys.GetString("SYS_LANG"));
                if (dtValue.Rows.Count > 0)
                {
                    if (dtValue.Rows.Count > 0)
                    {
                        this.uGrid1.DataSource = dtValue;
                        this.uGrid1.DataBind();

                        QRPSTA.STABasic structHistorgam = new STABasic();
                        QRPSTA.STASPC clsSTASPC = new STASPC();

                        double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                        double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);

                        structHistorgam = clsSTASPC.mfDrawHistogram(this.uChartHistogram, dtValue.DefaultView.ToTable(false, "InspectValue"), dblLowerSpec
                                                                    , dblUpperSpec, e.Row.Cells["SpecRangeCode"].Value.ToString()
                                                                    , 2, "", "", "", m_resSys.GetString("SYS_FONTNAME"), 20, 10);

                        this.uTextMean.Text = string.Format("{0:f5}", structHistorgam.Mean);
                        this.uTextStdDev.Text = string.Format("{0:f5}", structHistorgam.StdDev);
                        this.uTextMin.Text = string.Format("{0:f5}", structHistorgam.Min);
                        this.uTextMax.Text = string.Format("{0:f5}", structHistorgam.Max);

                        this.uTextCp.Text = string.Format("{0:f5}", structHistorgam.Cp);
                        this.uTextCpk.Text = string.Format("{0:f5}", structHistorgam.Cpk);
                        this.uTextCpl.Text = string.Format("{0:f5}", structHistorgam.Cpl);
                        this.uTextCpu.Text = string.Format("{0:f5}", structHistorgam.Cpu);
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
                            , "검사값이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);


                        InitValue();

                        this.uGrid1.DataSource = dtValue;
                        this.uGrid1.DataBind();
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion
    }
}
