﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPSTA.UI
{
    public partial class frmSTA0080 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0080()
        {
            InitializeComponent();
        }

        private void frmSTA0080_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0080_Load(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 타이틀 Text 설정함수 호출
                this.titleArea.mfSetLabelText("QCN 현황", m_resSys.GetString("SYS_FONTNAME"), 12);
                InitComboBox();
                InitLable();
                InitGrid();
                InitYear();
                InitEtc();
                SetToolAuth();
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// 종료시 컬럼 위치 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0080_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 화면 크기 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0080_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070 && this.Height > 850)
                {
                    //uGridQCN.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;
                    this.uGridQCN.Dock = DockStyle.Fill;
                }
                else
                {
                    //uGridQCN.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGridQCN.Dock = DockStyle.None;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화
        /// <summary>
        /// 사용권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인딩
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    if(m_resSys.GetString("SYS_LANG").Equals("CHN"))
                        arrValue.Add(i.ToString("D2") + "月");
                    else
                        arrValue.Add(i.ToString("D2") + "월");
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

                // QCN/ITR 구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtGubun = clsCom.mfReadCommonCode("C0068", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "", "ComCode", "ComCodeName", dtGubun);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitEtc()
        {
            try
            {
                this.uLabelSearchPlant.Hide();
                this.uLabelSearchYear.Hide();
                this.uLabelSearchMonth.Hide();

                this.uComboSearchMonth.Hide();
                this.uComboSearchPlant.Hide();
                this.uTextSearchYear.Hide();

                this.uDateSearchFromDate.Appearance.BackColor = Color.PowderBlue;
                this.uDateSearchToDate.Appearance.BackColor = Color.PowderBlue;

                this.uDateSearchFromDate.Value = DateTime.Now.ToString("yyyy-MM-dd 22:00:00");
                this.uDateSearchToDate.Value = DateTime.Now.ToString("yyyy-MM-dd 21:59:59");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label초기화
        /// </summary>
        private void InitLable()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelSearchDate, "검색일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                this.ultraLabel1.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                this.ultraLabel1.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;

                wLabel.mfSetLabel(this.uLabelSearchGubun, "QCN/ITR", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, false); // Package
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false); // 고객
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                wGrid.mfInitGeneralGrid(this.uGridQCN, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "AriseDate", "발행일", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "AriseTime", "발행시간", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "QCNITR", "QCN/ITR 여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "AriseEquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "CUSTOMERPRODUCTSPEC", "고객 PartNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "InspectUserName", "발행자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "ImputationDeptName", "귀책부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "QCConfirmDate", "완료일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FirCauseDesc", "원인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FirMeasureDesc", "대책", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FirCorrectDesc", "장비조치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "SecActionDesc", "자재조치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "ThiFCostQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "","", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "ThiFCostPerson", "인원", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "ThiFCostTime", "시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "ThiFCostScrap", "Scrap", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "WorkStepMaterialFlag", "자재 재검", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "WorkStepEquipFlag", "장비 재검", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "","", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FMManFlag", "Man", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FMEquipFlag", "Equip", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FMMethodFlag", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FMEnviroFlag", "Enviro", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCN, 0, "FMMaterialFlag", "Material", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridQCN.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion



        #region IToolBar Method
        public void mfCreate()
        {
        }

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);
                
                // 검색조건 확인
                DialogResult result = new DialogResult();
                if (this.uDateSearchFromDate.Value == null || this.uDateSearchFromDate.Value == DBNull.Value)// ||
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000216", "M000209", Infragistics.Win.HAlign.Center);

                    this.uDateSearchFromDate.DropDown();

                    return;
                }
                else if (this.uDateSearchToDate.Value == null || this.uDateSearchToDate.Value == DBNull.Value)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000216", "M000218", Infragistics.Win.HAlign.Center);

                    this.uDateSearchToDate.DropDown();

                    return;
                }
                else if (this.uDateSearchFromDate.DateTime.Date > this.uDateSearchToDate.DateTime.Date)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000216", "M000052", Infragistics.Win.HAlign.Center);
                    return;
                }

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                ////string strPlantCode = this.uComboSearchPlant.Value.ToString();
                ////string strYear = this.uTextSearchYear.Text;
                ////string strMonth = this.uComboSearchMonth.Value.ToString();

                ////DataTable dtQCN = clsReport.mfReadINSProcInspect_frmSTA0080(strPlantCode, strYear, strMonth, m_resSys.GetString("SYS_LANG"));

                // PSTS
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(this.uDateSearchFromDate.Value).AddDays(-1).ToString("yyyy-MM-dd 22:00:00");    // 검색시작일
                string strToDate = Convert.ToDateTime(this.uDateSearchToDate.Value).ToString("yyyy-MM-dd 22:00:00");                    // 검색종료일
                string strQCNITR = this.uComboSearchGubun.Value.ToString();                                                             //QCN ITR 여부
                string strPACKAGE = this.uComboSearchPackage.Value == null ? string.Empty : this.uComboSearchPackage.Value.ToString(); //Package
                string strCustomer = this.uTextSearchCustomer.Text;                                                                     // 고객

                DataTable dtQCN = clsReport.mfReadINSProcInspect_frmSTA0080_PSTS(strPlantCode, strFromDate, strToDate, strQCNITR, 
                                                                                strCustomer, strPACKAGE,m_resSys.GetString("SYS_LANG"));

                this.uGridQCN.DataSource = dtQCN;
                this.uGridQCN.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtQCN.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000203", "M000204", Infragistics.Win.HAlign.Center);

                    return;
                }
                ////else        // 하기 주석처리부분 쿼리내에서 처리(PSTS)
                ////{
                ////    for (int i = 0; i < this.uGridQCN.Rows.Count; i++)
                ////    {
                ////        this.uGridQCN.Rows[i].Cells["Seq"].Value = i + 1;
                ////        if (Convert.ToInt32(this.uGridQCN.Rows[i].Cells["Seq"].Value) < 10)
                ////        {
                ////            this.uGridQCN.Rows[i].Cells["Seq"].Value = "000" + this.uGridQCN.Rows[i].Cells["Seq"].Value.ToString();
                ////        }
                ////        else if (Convert.ToInt32(this.uGridQCN.Rows[i].Cells["Seq"].Value) < 100)
                ////        {
                ////            this.uGridQCN.Rows[i].Cells["Seq"].Value = "00" + this.uGridQCN.Rows[i].Cells["Seq"].Value.ToString();
                ////        }
                ////        else if (Convert.ToInt32(this.uGridQCN.Rows[i].Cells["Seq"].Value) < 1000)
                ////        {
                ////            this.uGridQCN.Rows[i].Cells["Seq"].Value = "0" + this.uGridQCN.Rows[i].Cells["Seq"].Value.ToString();
                ////        }
                ////    }
                ////}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {

        }

        public void mfDelete()
        {

        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridQCN.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridQCN);
                }
                if (this.uGridQCN.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        #endregion

        private void uGridQCN_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["FaultQty"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["InspectQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["InspectQty"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["ThiFCostQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["ThiFCostQty"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["ThiFCostPerson"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["ThiFCostPerson"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["ThiFCostTime"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["ThiFCostTime"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["ThiFCostScrap"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["ThiFCostScrap"].MaskInput = "nnn,nnn,nnn";
            }
            catch(Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 공장 변경시 PACKAGE Combo, 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //공장정보 저장
                string strPlantCode = this.uComboSearchPlant.Value == null ? string.Empty : this.uComboSearchPlant.Value.ToString();

                //제품정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //PAKCAGE 정보 조회
                DataTable dtPackge = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                clsProduct.Dispose(); //Resouce 해제

                WinComboEditor com = new WinComboEditor();

                //Item Clear
                this.uComboSearchPackage.Items.Clear();
                //PACKAGE 정보 삽입
                com.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "", "Package", "ComboName", dtPackge);

                dtPackge.Dispose(); // Resource 해제
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 고객명이 공백이 아닌 경우 Clear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_ValueChanged(object sender, EventArgs e)
        {
            //고객명이 공백이 아닌경우 Clear
            if (!this.uTextSearchCustomerName.Text.Equals(string.Empty))
                this.uTextSearchCustomerName.Clear();
        }

        /// <summary>
        /// 고객명 직접입력 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {  
                //Key 정보가 Enter가 아닌경우 Return
                if(e.KeyData != Keys.Enter)
                    return;

                //고객코드가 공백인경우 Return
                string strCustomerCode = this.uTextSearchCustomer.Text;
                if(strCustomerCode.Equals(string.Empty))
                    return;

                //고객정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer),"Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //고객 정보 조회
                DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode,m_resSys.GetString("SYS_LANG"));
                clsCustomer.Dispose();

                //정보가 없는경우 메세지출력
                if(dtCustomer.Rows.Count == 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    //입력하신 고객코드가 존재하지 않습니다.
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000255", "M000889", Infragistics.Win.HAlign.Right);

                    return;
                }
                //고객사명 삽입
                this.uTextSearchCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 고객 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomer_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try {

                frmPOP0003 frmCmr = new frmPOP0003();
                frmCmr.ShowDialog();

                //팝업창에서 고객 코드를 선택하지 않았다면 Return
                if (frmCmr.CustomerCode == null 
                    || frmCmr.CustomerCode.Equals(string.Empty))
                    return;

                //고객정보 삽입
                this.uTextSearchCustomer.Text = frmCmr.CustomerCode;
                this.uTextSearchCustomerName.Text = frmCmr.CustomerName;

            
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        
    }
}
