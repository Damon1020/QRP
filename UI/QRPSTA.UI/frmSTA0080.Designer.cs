﻿namespace QRPSTA.UI
{
    partial class frmSTA0080
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0080));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridQCN = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 4;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uComboSearchGubun
            // 
            this.uComboSearchGubun.Location = new System.Drawing.Point(704, 36);
            this.uComboSearchGubun.MaxLength = 5;
            this.uComboSearchGubun.Name = "uComboSearchGubun";
            this.uComboSearchGubun.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchGubun.TabIndex = 344;
            // 
            // uLabelSearchGubun
            // 
            this.uLabelSearchGubun.Location = new System.Drawing.Point(600, 36);
            this.uLabelSearchGubun.Name = "uLabelSearchGubun";
            this.uLabelSearchGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchGubun.TabIndex = 343;
            this.uLabelSearchGubun.Text = "QCN/ITR";
            // 
            // uDateSearchToDate
            // 
            this.uDateSearchToDate.Location = new System.Drawing.Point(228, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 342;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(216, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 340;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchFromDate
            // 
            this.uDateSearchFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 339;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 338;
            this.uLabelSearchDate.Text = "SearchDate";
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(1008, 4);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchYear.TabIndex = 337;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(1044, 8);
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchMonth.TabIndex = 336;
            this.uComboSearchMonth.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMonth
            // 
            this.uLabelSearchMonth.Location = new System.Drawing.Point(1020, 8);
            this.uLabelSearchMonth.Name = "uLabelSearchMonth";
            this.uLabelSearchMonth.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchMonth.TabIndex = 335;
            this.uLabelSearchMonth.Text = "ultraLabel1";
            // 
            // uTextSearchYear
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance1;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(1044, 20);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(20, 21);
            this.uTextSearchYear.TabIndex = 334;
            // 
            // uComboSearchPlant
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Appearance = appearance14;
            this.uComboSearchPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Location = new System.Drawing.Point(1028, 20);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchPlant.TabIndex = 332;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1016, 4);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 330;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridQCN
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCN.DisplayLayout.Appearance = appearance5;
            this.uGridQCN.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCN.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCN.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCN.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridQCN.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCN.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridQCN.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCN.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCN.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCN.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridQCN.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCN.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCN.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCN.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridQCN.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCN.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCN.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridQCN.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridQCN.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCN.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCN.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridQCN.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCN.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridQCN.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCN.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCN.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridQCN.Location = new System.Drawing.Point(0, 100);
            this.uGridQCN.Name = "uGridQCN";
            this.uGridQCN.Size = new System.Drawing.Size(1070, 750);
            this.uGridQCN.TabIndex = 7;
            this.uGridQCN.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGridQCN_InitializeLayout);
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(444, 36);
            this.uComboSearchPackage.MaxLength = 50;
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackage.TabIndex = 345;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(340, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 343;
            this.uLabelSearchPackage.Text = "PACKAGE";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 343;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uTextSearchCustomer
            // 
            appearance15.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomer.ButtonsRight.Add(editorButton1);
            this.uTextSearchCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextSearchCustomer.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchCustomer.MaxLength = 20;
            this.uTextSearchCustomer.Name = "uTextSearchCustomer";
            this.uTextSearchCustomer.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomer.TabIndex = 346;
            this.uTextSearchCustomer.ValueChanged += new System.EventHandler(this.uTextSearchCustomer_ValueChanged);
            this.uTextSearchCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomer_KeyDown);
            this.uTextSearchCustomer.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomer_EditorButtonClick);
            // 
            // uTextSearchCustomerName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance16;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(220, 36);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerName.TabIndex = 347;
            // 
            // frmSTA0080
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridQCN);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0080";
            this.Text = "frmSTA0080";
            this.Load += new System.EventHandler(this.frmSTA0080_Load);
            this.Activated += new System.EventHandler(this.frmSTA0080_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0080_FormClosing);
            this.Resize += new System.EventHandler(this.frmSTA0080_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCN;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomer;
    }
}