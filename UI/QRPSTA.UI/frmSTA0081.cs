﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0081 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0081()
        {
            InitializeComponent();
        }

        private void frmSTA0081_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("CCS 불량현황", m_SysRes.GetString("SYS_FONTNAME"), 12);
            InitComboBox();
            InitLabel();
            InitGrid();
            InitYear();
            SetToolAuth();
        }

        private void frmSTA0081_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0081_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                //Plant DataTable
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                //콤보박스에 데이터 바인딩
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                this.uComboSearchPlant.Appearance.BackColor = Color.PowderBlue;

                // 월 콤보박스
                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "연도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AriseDate", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AriseTime", "발생시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CUSTOMERPRODUCTSPEC", "고객 PartNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "QualityItemSS", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ReqLotSeq", "의뢰차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ReqDate", "의뢰일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ReqTime", "의뢰시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CompleteDate", "완료일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CompleteTime", "완료시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CCSReqTypeName", "의뢰사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200,false,false,200
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                 this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 검색툴바 연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        #region IToolBar 메소드
        public void mfCreate()
        { }

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uTextSearchYear.Text;
                string strMonth = this.uComboSearchMonth.Value.ToString();

                DataTable dtCCSFault = clsReport.mfReadINSProcInspect_frmSTA0081_PSTS(strPlantCode, strYear, strMonth, m_resSys.GetString("SYS_LANG"));

                this.uGrid.DataSource = dtCCSFault;
                this.uGrid.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (this.uGrid.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000203", "M000204", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    for (int i = 0; i < this.uGrid.Rows.Count; i++)
                    {
                        this.uGrid.Rows[i].Cells["Seq"].Value = i + 1;
                        if (Convert.ToInt32(this.uGrid.Rows[i].Cells["Seq"].Value) < 10)
                        {
                            this.uGrid.Rows[i].Cells["Seq"].Value = "000" + this.uGrid.Rows[i].Cells["Seq"].Value.ToString();
                        }
                        else if (Convert.ToInt32(this.uGrid.Rows[i].Cells["Seq"].Value) < 100)
                        {
                            this.uGrid.Rows[i].Cells["Seq"].Value = "00" + this.uGrid.Rows[i].Cells["Seq"].Value.ToString();
                        }
                        else if (Convert.ToInt32(this.uGrid.Rows[i].Cells["Seq"].Value) < 1000)
                        {
                            this.uGrid.Rows[i].Cells["Seq"].Value = "0" + this.uGrid.Rows[i].Cells["Seq"].Value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        { }

        public void mfSave()
        { }

        public void mfExcel()
        {
            try
            {
                if (this.uGrid.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid);
                }
                if (this.uGrid.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }   
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfPrint()
        { }


        #endregion
        /// <summary>
        /// Form Resize Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0081_Resize(object sender, EventArgs e)
        {
            if (this.Width > 1070)
            {
                uGrid.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;

            }
            else
            {
                uGrid.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            }
        }

        /// <summary>
        /// 그리드 레이아웃 초기화 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["FaultQty"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["QualityItemSS"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["QualityItemSS"].MaskInput = "nnn,nnn,nnn";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

    }
}
