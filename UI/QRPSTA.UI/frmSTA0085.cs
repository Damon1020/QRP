﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.Office.Interop.Excel;

namespace QRPSTA.UI
{
    public partial class frmSTA0085 : Form, QRPCOM.QRPGLO.IToolbar
    {
        public override Size MinimumSize
        {
            get
            {
                Size size = new Size(1070, 850);
                return size;
            }
            set
            {
                Size size = new Size(1070, 850);
                base.MinimumSize = size;
            }
        }

        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0085()
        {
            InitializeComponent();
        }

        private void frmSTA0085_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0085_Load(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
                // Title 설정
                titleArea.mfSetLabelText("수입검사 불량현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

                // 컨트롤 초기화
                InitEvent();
                SetToolAuth();
                InitLabel();
                InitComboBox();
                InitEtc();
                InitGrid();

                this.uComboSearchPlantCode.Hide();
                this.uLabelSearchPlant.Hide();

                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void frmSTA0085_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드

        private void InitEvent()
        {
            this.uComboSearchPlantCode.ValueChanged += new EventHandler(uComboSearchPlantCode_ValueChanged);
            this.uGridMatAbnormal.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(uGridMatAbnormal_InitializeLayout);
            this.uGridProcAbnormal.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(uGridProcAbnormal_InitializeLayout);
            this.uTabChartInfo.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(uTabChartInfo_SelectedTabChanged);
            this.uTextSearchYear.KeyPress += new KeyPressEventHandler(uTextSearchYear_KeyPress);
        }

        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitEtc()
        {
            try
            {
                this.uTextSearchYear.MaxLength = 4;
                this.uTextSearchYear.Text = Convert.ToString(DateTime.Today.Year);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitComboBox()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                System.Data.DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                int intBandIndex = 0;

                #region 목표율
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "ConsumableTypeCode", "자재유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M01Target", "01월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M02Target", "02월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M03Target", "03월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M04Target", "04월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M05Target", "05월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M06Target", "06월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M07Target", "07월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M08Target", "08월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M09Target", "09월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M10Target", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M11Target", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "M12Target", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, intBandIndex, "TTarget", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 수입이상 그리드
                wGrid.mfInitGeneralGrid(this.uGridMatAbnormal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "StdNumber", "관리번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "ConsumableTypeCode", "자재명", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "WriteDate", "접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "VendorName", "협력업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "Spec", "자재규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "LotSize", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "JudgeDate", "판정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "InspectQty", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatAbnormal, intBandIndex, "InspectDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 300, false, false, 2000
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region 공정이상 발생

                string strT1 = string.Empty;
                string strT2 = string.Empty;
                if (m_resSys.GetString("SYS_LANG").Equals("KOR"))
                { strT1 = "입고현황"; strT2 = "이상현황"; }

                else if (m_resSys.GetString("SYS_LANG").Equals("CHN"))
                { strT1 = "入库现状"; strT2 = "异常现状"; }
                else if (m_resSys.GetString("SYS_LANG").Equals("ENG"))
                { strT1 = "입고현황"; strT2 = "이상현황"; }


                wGrid.mfInitGeneralGrid(this.uGridProcAbnormal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 85, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "ConsumableTypeCode", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "VendorName", "협력업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "Spec", "자재규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                //Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1stWeek = wGrid.mfSetGridGroup(this.uGridWeekly1, 0, "1stWeek", "1W", 4, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupGR = wGrid.mfSetGridGroup(this.uGridProcAbnormal, 0, "GRState", strT1, 5, 0, 2, 2, false);


                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "GRQty", "수량(K)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupGR);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupGR);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "FailLotNo", "불량Lot", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 2, null);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupAbnormalState = wGrid.mfSetGridGroup(this.uGridProcAbnormal, 0, "AbnormalState", strT2, 7, 0, 4, 2, false);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "AriseProcessName", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupAbnormalState);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "AriseDate", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupAbnormalState);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "LotSize", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupAbnormalState);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "AriseLotNo", "발생LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupAbnormalState);

                wGrid.mfSetGridColumn(this.uGridProcAbnormal, intBandIndex, "FaultTypeName", "불량명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 1, 2, null);



                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        private void uComboSearchPlantCode_ValueChanged(object sender, EventArgs e)
        {
            System.Data.DataTable dtType = new System.Data.DataTable();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinComboEditor wCombo = new WinComboEditor();
            try
            {

                if(!this.uComboSearchPlantCode.Value.ToString().Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                    QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                    brwChannel.mfCredentials(clsCom);
                    dtType = clsCom.mfReadCommonCode("MaterialGroup", m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        , "", "", "전체", "ComCode", "ComCodeName", dtType);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #region Toolbar Method
        public void mfSearch()
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (this.uComboSearchPlantCode.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlantCode.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000881", "M000813", Infragistics.Win.HAlign.Right);

                    this.uTextSearchYear.Focus();
                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.INSMaterialReport), "INSMaterialReport");
                    QRPSTA.BL.STAIMP.INSMaterialReport clsReport = new QRPSTA.BL.STAIMP.INSMaterialReport();
                    brwChannel.mfCredentials(clsReport);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                    string strYear = this.uTextSearchYear.Text;
                    string strConsumableTypeCode = this.uComboSearchType.Value.ToString();

                    System.Data.DataTable dtTarget =  clsReport.mfReadINSMatInspect_frmSTA0085_D1(strPlantCode, strYear, strConsumableTypeCode, m_resSys.GetString("SYS_LANG"));
                    System.Data.DataTable dtMat = clsReport.mfReadINSMatInspect_frmSTA0085_D2(strPlantCode, strYear, strConsumableTypeCode, m_resSys.GetString("SYS_LANG"));
                    System.Data.DataTable dtProc = clsReport.mfReadINSMatInspect_frmSTA0085_D3(strPlantCode, strYear, strConsumableTypeCode, m_resSys.GetString("SYS_LANG"));

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);


                    if (dtTarget.Rows.Count.Equals(0) && dtMat.Rows.Count.Equals(0) && dtProc.Rows.Count.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M000199", "M000203", "M000204", Infragistics.Win.HAlign.Right);
                    }

                    else
                    {
                        this.uGrid1.DataSource = dtTarget;
                        this.uGridMatAbnormal.DataSource = dtMat;
                        this.uGridProcAbnormal.DataSource = dtProc;
                        this.uGrid1.DataBind();
                        this.uGridMatAbnormal.DataBind();
                        this.uGridProcAbnormal.DataBind();


                        #region ChartTab 설정

                        string strAll = msg.GetMessge_Text("M001498", m_resSys.GetString("SYS_LANG"));

                        uTabChartInfo.Tabs.Clear();
                        uTabChartInfo.Tabs.Add("ALL", strAll);//"전체");
                        for (int i = 0; i < dtTarget.Rows.Count; i++)
                        {
                            uTabChartInfo.Tabs.Add(dtTarget.Rows[i]["ConsumableTypeCode"].ToString(), dtTarget.Rows[i]["ConsumableTypeCode"].ToString());
                        }
                        DrawChart();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {

        }

        public void mfDelete()
        {

        }

        public void mfCreate()
        {

        }
        
        public void mfExcel()
        {

        }

        public void mfPrint()
        {

        }
        #endregion


        #endregion

        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void DrawChart()
        {
            try
            {
                
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                System.Data.DataTable dtChart = new System.Data.DataTable("TargetChart");
                dtChart.Columns.Add("ConsumableTypeCode", typeof(string));
                dtChart.Columns.Add("1", typeof(double));
                dtChart.Columns.Add("2", typeof(double));
                dtChart.Columns.Add("3", typeof(double));
                dtChart.Columns.Add("4", typeof(double));
                dtChart.Columns.Add("5", typeof(double));
                dtChart.Columns.Add("6", typeof(double));
                dtChart.Columns.Add("7", typeof(double));
                dtChart.Columns.Add("8", typeof(double));
                dtChart.Columns.Add("9", typeof(double));
                dtChart.Columns.Add("10", typeof(double));
                dtChart.Columns.Add("11", typeof(double));
                dtChart.Columns.Add("12", typeof(double));

                if (uTabChartInfo.SelectedTab.Key == "ALL")
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        DataRow dr = dtChart.NewRow();
                        dr["ConsumableTypeCode"] = uGrid1.Rows[i].Cells["ConsumableTypeCode"].Value.ToString();
                        dr["1"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M01Target"].Value.ToString());
                        dr["2"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M02Target"].Value.ToString());
                        dr["3"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M03Target"].Value.ToString());
                        dr["4"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M04Target"].Value.ToString());
                        dr["5"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M05Target"].Value.ToString());
                        dr["6"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M06Target"].Value.ToString());
                        dr["7"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M07Target"].Value.ToString());
                        dr["8"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M08Target"].Value.ToString());
                        dr["9"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M09Target"].Value.ToString());
                        dr["10"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M10Target"].Value.ToString());
                        dr["11"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M11Target"].Value.ToString());
                        dr["12"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M12Target"].Value.ToString());
                        dtChart.Rows.Add(dr);
                    }
                }
                else
                {
                    string strConsumableTypeCode = uTabChartInfo.SelectedTab.Key;
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (this.uGrid1.Rows[i].Cells["ConsumableTypeCode"].Value.ToString().Equals(strConsumableTypeCode))
                        {
                            DataRow dr = dtChart.NewRow();
                            dr["ConsumableTypeCode"] = uGrid1.Rows[i].Cells["ConsumableTypeCode"].Value.ToString();
                            dr["1"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M01Target"].Value.ToString());
                            dr["2"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M02Target"].Value.ToString());
                            dr["3"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M03Target"].Value.ToString());
                            dr["4"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M04Target"].Value.ToString());
                            dr["5"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M05Target"].Value.ToString());
                            dr["6"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M06Target"].Value.ToString());
                            dr["7"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M07Target"].Value.ToString());
                            dr["8"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M08Target"].Value.ToString());
                            dr["9"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M09Target"].Value.ToString());
                            dr["10"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M10Target"].Value.ToString());
                            dr["11"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M11Target"].Value.ToString());
                            dr["12"] = Convert.ToDouble(this.uGrid1.Rows[i].Cells["M12Target"].Value.ToString());
                            dtChart.Rows.Add(dr);
                        }
                    }
                }
                //챠트 유형 지정
                uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                uChart.AreaChart.NullHandling = Infragistics.UltraChart.Shared.Styles.NullHandling.DontPlot;

                //Line챠트의 Line 속성 지정
                Infragistics.UltraChart.Resources.Appearance.LineAppearance ValueLineApp = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                ValueLineApp.Thickness = 4;
                ValueLineApp.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                ValueLineApp.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.Square;
                //ValueLineApp.IconAppearance.PE.Fill = System.Drawing.Color.Black;
                ValueLineApp.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                ValueLineApp.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Auto;
                uChart.LineChart.LineAppearances.Add(ValueLineApp);

                //범례 유형 지정
                uChart.Legend.Visible = true;
                uChart.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Right;
                uChart.Legend.Margins.Left = 0;
                uChart.Legend.Margins.Right = 0;
                uChart.Legend.Margins.Top = 0;
                uChart.Legend.Margins.Bottom = 0;
                uChart.Legend.SpanPercentage = 10;


                uChart.TitleTop.Text = "";
                uChart.TitleTop.HorizontalAlign = StringAlignment.Center;
                uChart.TitleTop.VerticalAlign = StringAlignment.Center;
                uChart.TitleTop.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 12, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point);
                uChart.TitleTop.Margins.Left = 0;
                uChart.TitleTop.Margins.Right = 0;
                uChart.TitleTop.Margins.Top = 0;
                uChart.TitleTop.Margins.Bottom = 0;

                string strQuaily = string.Empty;
                if (m_resSys.GetString("SYS_LANG").Equals("KOR"))
                    strQuaily = "품질목표";
                else if (m_resSys.GetString("SYS_LANG").Equals("CHN"))
                    strQuaily = "品质目标";
                else if (m_resSys.GetString("SYS_LANG").Equals("ENG"))
                    strQuaily = "";

                //X축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisX = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisX.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                axisX.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                axisX.Labels.ItemFormatString = "<DATA_VALUE:0.#>";
                axisX.Extent = 5;
                uChart.TitleLeft.Text = strQuaily;
                uChart.TitleLeft.HorizontalAlign = StringAlignment.Center;
                uChart.TitleLeft.VerticalAlign = StringAlignment.Center;
                uChart.TitleLeft.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                uChart.TitleLeft.Margins.Left = 5;
                uChart.TitleLeft.Margins.Right = 5;
                uChart.TitleLeft.Margins.Top = 0;
                uChart.TitleLeft.Margins.Bottom = 0;
                uChart.TitleLeft.Visible = true;
                uChart.LineChart.HighLightLines = true;

                if (dtChart.Rows.Count > 0)
                {
                    uChart.DataSource = dtChart;
                    uChart.DataBind();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTabChartInfo_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            DrawChart();
        }


        private void uGridProcAbnormal_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["GRQty"].CellAppearance.ForeColor = Color.Black;
                e.Layout.Bands[0].Columns["GRQty"].MaskInput = "n,nnn";
                e.Layout.Bands[0].Columns["LotNo"].CellAppearance.ForeColor = Color.Black;
                e.Layout.Bands[0].Columns["AriseProcessName"].CellAppearance.ForeColor = Color.Black;
                e.Layout.Bands[0].Columns["LotSize"].CellAppearance.ForeColor = Color.Black;
                e.Layout.Bands[0].Columns["AriseDate"].CellAppearance.ForeColor = Color.Black;
                e.Layout.Bands[0].Columns["LotSize"].MaskInput = "n,nnn";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridMatAbnormal_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["LotSize"].MaskInput = "n,nnn";
                e.Layout.Bands[0].Columns["FaultQty"].MaskInput = "n,nnn";
                e.Layout.Bands[0].Columns["InspectQty"].MaskInput = "n,nnn";

            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void frmSTA0085_Resize(object sender, EventArgs e)
        {
            ////try
            ////{
            ////    if (this.Width > 1070)
            ////    {
            ////        uGrid1.Width = this.Width;
            ////        uTabChartInfo.Width = this.Width;
            ////        uTabInfo.Width = this.Width;
            ////    }
            ////    else
            ////    {
            ////        uGrid1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            ////        uTabChartInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            ////        uTabInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            ////    }
            ////}
            ////catch (System.Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }
    }
}
