﻿namespace QRPSTA.UI
{
    partial class frmSTA0064
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0064));
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement2 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            this.uTextSpecRangeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uTextSampleSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uComboSearchRev = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateSearchInspectFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelMax = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStdDeviation = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchMaterial = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchMaterialGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchRev = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchVendor = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMin = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMin = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStdDev = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelR = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelXBar = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextXBarLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextXBarUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTrend = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTrend = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUnderTrendP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUnderTrend = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnderTrend = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOverTrendP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOverTrend = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOverTrend = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRun = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUnderRunP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUnderRun = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnderRun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOverRunP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOverRun = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOverRun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOOS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOOS = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChartR = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uTextUnderLSLP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUnderLSL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnderLSL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOverUSLP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOverUSL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOverUSL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOOC = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOOC = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUnderLCLP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUnderLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnderLCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextOverUCLP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOverUCL = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextOverUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextXBarCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCL = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSpecRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLowerSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecLower = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUpperSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupBoxControlLimit = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextPpu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPpu = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPpl = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPpl = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPpk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPpk = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPp = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCpu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpu = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCpl = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpl = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCpk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpk = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCp = new Infragistics.Win.Misc.UltraLabel();
            this.uChartXBar = new Infragistics.Win.UltraWinChart.UltraChart();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRangeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSampleSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderTrendP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverTrendP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderRunP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverRunP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLSLP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUSLP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLCLP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUCLP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxControlLimit)).BeginInit();
            this.uGroupBoxControlLimit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartXBar)).BeginInit();
            this.SuspendLayout();
            // 
            // uTextSpecRangeCode
            // 
            appearance65.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRangeCode.Appearance = appearance65;
            this.uTextSpecRangeCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRangeCode.Location = new System.Drawing.Point(364, 28);
            this.uTextSpecRangeCode.Name = "uTextSpecRangeCode";
            this.uTextSpecRangeCode.ReadOnly = true;
            this.uTextSpecRangeCode.Size = new System.Drawing.Size(44, 21);
            this.uTextSpecRangeCode.TabIndex = 74;
            this.uTextSpecRangeCode.Visible = false;
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.CheckedListSettings.CheckStateMember = "";
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchInspectItem.DisplayLayout.Appearance = appearance69;
            this.uComboSearchInspectItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchInspectItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance54;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uComboSearchInspectItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchInspectItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance72.BackColor = System.Drawing.SystemColors.Highlight;
            appearance72.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveRowAppearance = appearance72;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            appearance70.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellAppearance = appearance70;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.SystemColors.Control;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.GroupByRowAppearance = appearance74;
            appearance76.TextHAlignAsString = "Left";
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderAppearance = appearance76;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowAppearance = appearance75;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchInspectItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance73;
            this.uComboSearchInspectItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchInspectItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchInspectItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(116, 40);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(220, 22);
            this.uComboSearchInspectItem.TabIndex = 22;
            this.uComboSearchInspectItem.Text = "ultraCombo1";
            this.uComboSearchInspectItem.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboSearchInspectItem_RowSelected);
            // 
            // uTextSampleSize
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSampleSize.Appearance = appearance55;
            this.uTextSampleSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSampleSize.Location = new System.Drawing.Point(364, 4);
            this.uTextSampleSize.Name = "uTextSampleSize";
            this.uTextSampleSize.ReadOnly = true;
            this.uTextSampleSize.Size = new System.Drawing.Size(44, 21);
            this.uTextSampleSize.TabIndex = 75;
            this.uTextSampleSize.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 25;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uComboSearchRev
            // 
            this.uComboSearchRev.Location = new System.Drawing.Point(916, 12);
            this.uComboSearchRev.Name = "uComboSearchRev";
            this.uComboSearchRev.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchRev.TabIndex = 294;
            this.uComboSearchRev.Text = "ultraComboEditor2";
            this.uComboSearchRev.ValueChanged += new System.EventHandler(this.uComboSearchRev_ValueChanged);
            // 
            // uDateSearchInspectFromDate
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.Appearance = appearance8;
            this.uDateSearchInspectFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchInspectFromDate.Name = "uDateSearchInspectFromDate";
            this.uDateSearchInspectFromDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchInspectFromDate.TabIndex = 13;
            // 
            // uLabelMax
            // 
            this.uLabelMax.Location = new System.Drawing.Point(284, 124);
            this.uLabelMax.Name = "uLabelMax";
            this.uLabelMax.Size = new System.Drawing.Size(68, 20);
            this.uLabelMax.TabIndex = 72;
            this.uLabelMax.Text = "최대값";
            // 
            // uLabelStdDeviation
            // 
            this.uLabelStdDeviation.Location = new System.Drawing.Point(284, 76);
            this.uLabelStdDeviation.Name = "uLabelStdDeviation";
            this.uLabelStdDeviation.Size = new System.Drawing.Size(68, 20);
            this.uLabelStdDeviation.TabIndex = 71;
            this.uLabelStdDeviation.Text = "표준편차";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchRev);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRev);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 64);
            this.uGroupBoxSearchArea.TabIndex = 29;
            // 
            // uComboSearchMaterial
            // 
            this.uComboSearchMaterial.Location = new System.Drawing.Point(448, 12);
            this.uComboSearchMaterial.Name = "uComboSearchMaterial";
            this.uComboSearchMaterial.Size = new System.Drawing.Size(356, 21);
            this.uComboSearchMaterial.TabIndex = 293;
            this.uComboSearchMaterial.Text = "ultraComboEditor1";
            this.uComboSearchMaterial.ValueChanged += new System.EventHandler(this.uComboSearchMaterial_ValueChanged);
            // 
            // uComboSearchMaterialGrade
            // 
            this.uComboSearchMaterialGrade.Location = new System.Drawing.Point(916, 40);
            this.uComboSearchMaterialGrade.Name = "uComboSearchMaterialGrade";
            this.uComboSearchMaterialGrade.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchMaterialGrade.TabIndex = 291;
            this.uComboSearchMaterialGrade.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMaterialGrade
            // 
            this.uLabelSearchMaterialGrade.Location = new System.Drawing.Point(812, 40);
            this.uLabelSearchMaterialGrade.Name = "uLabelSearchMaterialGrade";
            this.uLabelSearchMaterialGrade.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchMaterialGrade.TabIndex = 292;
            this.uLabelSearchMaterialGrade.Text = "자재등급";
            // 
            // uLabelSearchRev
            // 
            this.uLabelSearchRev.Location = new System.Drawing.Point(812, 12);
            this.uLabelSearchRev.Name = "uLabelSearchRev";
            this.uLabelSearchRev.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchRev.TabIndex = 289;
            this.uLabelSearchRev.Text = "Rev";
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(344, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchMaterial.TabIndex = 288;
            this.uLabelSearchMaterial.Text = "자재";
            // 
            // uLabelSearchInspectItem
            // 
            this.uLabelSearchInspectItem.Location = new System.Drawing.Point(12, 40);
            this.uLabelSearchInspectItem.Name = "uLabelSearchInspectItem";
            this.uLabelSearchInspectItem.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchInspectItem.TabIndex = 283;
            this.uLabelSearchInspectItem.Text = "검사항목";
            // 
            // uComboSearchVendor
            // 
            this.uComboSearchVendor.Location = new System.Drawing.Point(448, 40);
            this.uComboSearchVendor.Name = "uComboSearchVendor";
            this.uComboSearchVendor.Size = new System.Drawing.Size(356, 21);
            this.uComboSearchVendor.TabIndex = 15;
            this.uComboSearchVendor.Text = "ultraComboEditor1";
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(344, 40);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchVendor.TabIndex = 30;
            this.uLabelSearchVendor.Text = "거래처";
            // 
            // uDateSearchInspectToDate
            // 
            appearance9.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.Appearance = appearance9;
            this.uDateSearchInspectToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectToDate.Location = new System.Drawing.Point(232, 12);
            this.uDateSearchInspectToDate.Name = "uDateSearchInspectToDate";
            this.uDateSearchInspectToDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchInspectToDate.TabIndex = 14;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchInspectDate.TabIndex = 12;
            this.uLabelSearchInspectDate.Text = "검색기간";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1052, -9);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1028, -8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uLabelMin
            // 
            this.uLabelMin.Location = new System.Drawing.Point(284, 100);
            this.uLabelMin.Name = "uLabelMin";
            this.uLabelMin.Size = new System.Drawing.Size(68, 20);
            this.uLabelMin.TabIndex = 70;
            this.uLabelMin.Text = "최소값";
            // 
            // uTextMax
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            appearance17.TextHAlignAsString = "Right";
            this.uTextMax.Appearance = appearance17;
            this.uTextMax.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMax.Location = new System.Drawing.Point(356, 124);
            this.uTextMax.Name = "uTextMax";
            this.uTextMax.ReadOnly = true;
            this.uTextMax.Size = new System.Drawing.Size(72, 21);
            this.uTextMax.TabIndex = 69;
            // 
            // uTextMin
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            appearance42.TextHAlignAsString = "Right";
            this.uTextMin.Appearance = appearance42;
            this.uTextMin.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMin.Location = new System.Drawing.Point(356, 100);
            this.uTextMin.Name = "uTextMin";
            this.uTextMin.ReadOnly = true;
            this.uTextMin.Size = new System.Drawing.Size(72, 21);
            this.uTextMin.TabIndex = 68;
            // 
            // uTextStdDev
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            appearance48.TextHAlignAsString = "Right";
            this.uTextStdDev.Appearance = appearance48;
            this.uTextStdDev.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDev.Location = new System.Drawing.Point(356, 76);
            this.uTextStdDev.Name = "uTextStdDev";
            this.uTextStdDev.ReadOnly = true;
            this.uTextStdDev.Size = new System.Drawing.Size(72, 21);
            this.uTextStdDev.TabIndex = 67;
            // 
            // uLabelR
            // 
            this.uLabelR.Location = new System.Drawing.Point(180, 52);
            this.uLabelR.Name = "uLabelR";
            this.uLabelR.Size = new System.Drawing.Size(84, 20);
            this.uLabelR.TabIndex = 66;
            this.uLabelR.Text = "R관리도";
            // 
            // uLabelXBar
            // 
            this.uLabelXBar.Location = new System.Drawing.Point(84, 52);
            this.uLabelXBar.Name = "uLabelXBar";
            this.uLabelXBar.Size = new System.Drawing.Size(84, 20);
            this.uLabelXBar.TabIndex = 65;
            this.uLabelXBar.Text = "XBar관리도";
            // 
            // uTextRLCL
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            appearance4.TextHAlignAsString = "Right";
            this.uTextRLCL.Appearance = appearance4;
            this.uTextRLCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRLCL.Location = new System.Drawing.Point(180, 124);
            this.uTextRLCL.Name = "uTextRLCL";
            this.uTextRLCL.ReadOnly = true;
            this.uTextRLCL.Size = new System.Drawing.Size(84, 21);
            this.uTextRLCL.TabIndex = 64;
            // 
            // uTextRUCL
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            appearance16.TextHAlignAsString = "Right";
            this.uTextRUCL.Appearance = appearance16;
            this.uTextRUCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRUCL.Location = new System.Drawing.Point(180, 76);
            this.uTextRUCL.Name = "uTextRUCL";
            this.uTextRUCL.ReadOnly = true;
            this.uTextRUCL.Size = new System.Drawing.Size(84, 21);
            this.uTextRUCL.TabIndex = 63;
            // 
            // uTextRCL
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            appearance7.TextHAlignAsString = "Right";
            this.uTextRCL.Appearance = appearance7;
            this.uTextRCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRCL.Location = new System.Drawing.Point(180, 100);
            this.uTextRCL.Name = "uTextRCL";
            this.uTextRCL.ReadOnly = true;
            this.uTextRCL.Size = new System.Drawing.Size(84, 21);
            this.uTextRCL.TabIndex = 62;
            // 
            // uTextXBarLCL
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            appearance10.TextHAlignAsString = "Right";
            this.uTextXBarLCL.Appearance = appearance10;
            this.uTextXBarLCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextXBarLCL.Location = new System.Drawing.Point(84, 124);
            this.uTextXBarLCL.Name = "uTextXBarLCL";
            this.uTextXBarLCL.ReadOnly = true;
            this.uTextXBarLCL.Size = new System.Drawing.Size(84, 21);
            this.uTextXBarLCL.TabIndex = 61;
            // 
            // uLabelLCL
            // 
            this.uLabelLCL.Location = new System.Drawing.Point(12, 124);
            this.uLabelLCL.Name = "uLabelLCL";
            this.uLabelLCL.Size = new System.Drawing.Size(68, 20);
            this.uLabelLCL.TabIndex = 60;
            this.uLabelLCL.Text = "LCL";
            // 
            // uTextXBarUCL
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            appearance12.TextHAlignAsString = "Right";
            this.uTextXBarUCL.Appearance = appearance12;
            this.uTextXBarUCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextXBarUCL.Location = new System.Drawing.Point(84, 76);
            this.uTextXBarUCL.Name = "uTextXBarUCL";
            this.uTextXBarUCL.ReadOnly = true;
            this.uTextXBarUCL.Size = new System.Drawing.Size(84, 21);
            this.uTextXBarUCL.TabIndex = 59;
            // 
            // uLabelUCL
            // 
            this.uLabelUCL.Location = new System.Drawing.Point(12, 76);
            this.uLabelUCL.Name = "uLabelUCL";
            this.uLabelUCL.Size = new System.Drawing.Size(68, 20);
            this.uLabelUCL.TabIndex = 58;
            this.uLabelUCL.Text = "UCL";
            // 
            // uTextTrend
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            appearance56.TextHAlignAsString = "Right";
            this.uTextTrend.Appearance = appearance56;
            this.uTextTrend.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTrend.Location = new System.Drawing.Point(364, 680);
            this.uTextTrend.Name = "uTextTrend";
            this.uTextTrend.ReadOnly = true;
            this.uTextTrend.Size = new System.Drawing.Size(70, 21);
            this.uTextTrend.TabIndex = 57;
            // 
            // uLabelTrend
            // 
            this.uLabelTrend.Location = new System.Drawing.Point(260, 680);
            this.uLabelTrend.Name = "uLabelTrend";
            this.uLabelTrend.Size = new System.Drawing.Size(100, 20);
            this.uLabelTrend.TabIndex = 56;
            this.uLabelTrend.Text = "ultraLabel13";
            // 
            // uTextUnderTrendP
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            appearance32.TextHAlignAsString = "Right";
            this.uTextUnderTrendP.Appearance = appearance32;
            this.uTextUnderTrendP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderTrendP.Location = new System.Drawing.Point(188, 704);
            this.uTextUnderTrendP.Name = "uTextUnderTrendP";
            this.uTextUnderTrendP.ReadOnly = true;
            this.uTextUnderTrendP.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderTrendP.TabIndex = 55;
            // 
            // uTextUnderTrend
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            appearance33.TextHAlignAsString = "Right";
            this.uTextUnderTrend.Appearance = appearance33;
            this.uTextUnderTrend.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderTrend.Location = new System.Drawing.Point(116, 704);
            this.uTextUnderTrend.Name = "uTextUnderTrend";
            this.uTextUnderTrend.ReadOnly = true;
            this.uTextUnderTrend.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderTrend.TabIndex = 54;
            // 
            // uLabelUnderTrend
            // 
            this.uLabelUnderTrend.Location = new System.Drawing.Point(12, 704);
            this.uLabelUnderTrend.Name = "uLabelUnderTrend";
            this.uLabelUnderTrend.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnderTrend.TabIndex = 53;
            this.uLabelUnderTrend.Text = "ultraLabel12";
            // 
            // uTextOverTrendP
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            appearance34.TextHAlignAsString = "Right";
            this.uTextOverTrendP.Appearance = appearance34;
            this.uTextOverTrendP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverTrendP.Location = new System.Drawing.Point(188, 680);
            this.uTextOverTrendP.Name = "uTextOverTrendP";
            this.uTextOverTrendP.ReadOnly = true;
            this.uTextOverTrendP.Size = new System.Drawing.Size(70, 21);
            this.uTextOverTrendP.TabIndex = 52;
            // 
            // uTextOverTrend
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            appearance35.TextHAlignAsString = "Right";
            this.uTextOverTrend.Appearance = appearance35;
            this.uTextOverTrend.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverTrend.Location = new System.Drawing.Point(116, 680);
            this.uTextOverTrend.Name = "uTextOverTrend";
            this.uTextOverTrend.ReadOnly = true;
            this.uTextOverTrend.Size = new System.Drawing.Size(70, 21);
            this.uTextOverTrend.TabIndex = 51;
            // 
            // uLabelOverTrend
            // 
            this.uLabelOverTrend.Location = new System.Drawing.Point(12, 680);
            this.uLabelOverTrend.Name = "uLabelOverTrend";
            this.uLabelOverTrend.Size = new System.Drawing.Size(100, 20);
            this.uLabelOverTrend.TabIndex = 50;
            this.uLabelOverTrend.Text = "ultraLabel11";
            // 
            // uTextRun
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            appearance37.TextHAlignAsString = "Right";
            this.uTextRun.Appearance = appearance37;
            this.uTextRun.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRun.Location = new System.Drawing.Point(364, 632);
            this.uTextRun.Name = "uTextRun";
            this.uTextRun.ReadOnly = true;
            this.uTextRun.Size = new System.Drawing.Size(70, 21);
            this.uTextRun.TabIndex = 48;
            // 
            // uLabelRun
            // 
            this.uLabelRun.Location = new System.Drawing.Point(260, 632);
            this.uLabelRun.Name = "uLabelRun";
            this.uLabelRun.Size = new System.Drawing.Size(100, 20);
            this.uLabelRun.TabIndex = 47;
            this.uLabelRun.Text = "ultraLabel10";
            // 
            // uTextUnderRunP
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            appearance38.TextHAlignAsString = "Right";
            this.uTextUnderRunP.Appearance = appearance38;
            this.uTextUnderRunP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderRunP.Location = new System.Drawing.Point(188, 656);
            this.uTextUnderRunP.Name = "uTextUnderRunP";
            this.uTextUnderRunP.ReadOnly = true;
            this.uTextUnderRunP.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderRunP.TabIndex = 46;
            // 
            // uTextUnderRun
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            appearance39.TextHAlignAsString = "Right";
            this.uTextUnderRun.Appearance = appearance39;
            this.uTextUnderRun.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderRun.Location = new System.Drawing.Point(116, 656);
            this.uTextUnderRun.Name = "uTextUnderRun";
            this.uTextUnderRun.ReadOnly = true;
            this.uTextUnderRun.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderRun.TabIndex = 45;
            // 
            // uLabelUnderRun
            // 
            this.uLabelUnderRun.Location = new System.Drawing.Point(12, 656);
            this.uLabelUnderRun.Name = "uLabelUnderRun";
            this.uLabelUnderRun.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnderRun.TabIndex = 44;
            this.uLabelUnderRun.Text = "ultraLabel9";
            // 
            // uTextOverRunP
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            appearance40.TextHAlignAsString = "Right";
            this.uTextOverRunP.Appearance = appearance40;
            this.uTextOverRunP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverRunP.Location = new System.Drawing.Point(188, 632);
            this.uTextOverRunP.Name = "uTextOverRunP";
            this.uTextOverRunP.ReadOnly = true;
            this.uTextOverRunP.Size = new System.Drawing.Size(70, 21);
            this.uTextOverRunP.TabIndex = 43;
            // 
            // uTextOverRun
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            appearance41.TextHAlignAsString = "Right";
            this.uTextOverRun.Appearance = appearance41;
            this.uTextOverRun.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverRun.Location = new System.Drawing.Point(116, 632);
            this.uTextOverRun.Name = "uTextOverRun";
            this.uTextOverRun.ReadOnly = true;
            this.uTextOverRun.Size = new System.Drawing.Size(70, 21);
            this.uTextOverRun.TabIndex = 42;
            // 
            // uLabelOverRun
            // 
            this.uLabelOverRun.Location = new System.Drawing.Point(12, 632);
            this.uLabelOverRun.Name = "uLabelOverRun";
            this.uLabelOverRun.Size = new System.Drawing.Size(100, 20);
            this.uLabelOverRun.TabIndex = 41;
            this.uLabelOverRun.Text = "ultraLabel8";
            // 
            // uTextOOS
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            appearance43.TextHAlignAsString = "Right";
            this.uTextOOS.Appearance = appearance43;
            this.uTextOOS.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOOS.Location = new System.Drawing.Point(364, 584);
            this.uTextOOS.Name = "uTextOOS";
            this.uTextOOS.ReadOnly = true;
            this.uTextOOS.Size = new System.Drawing.Size(70, 21);
            this.uTextOOS.TabIndex = 39;
            // 
            // uLabelOOS
            // 
            this.uLabelOOS.Location = new System.Drawing.Point(260, 584);
            this.uLabelOOS.Name = "uLabelOOS";
            this.uLabelOOS.Size = new System.Drawing.Size(100, 20);
            this.uLabelOOS.TabIndex = 38;
            this.uLabelOOS.Text = "ultraLabel7";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uChartR);
            this.uGroupBox3.Location = new System.Drawing.Point(440, 492);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(616, 344);
            this.uGroupBox3.TabIndex = 28;
            // 
            // uChartR
            // 
            this.uChartR.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartR.Axis.PE = paintElement1;
            this.uChartR.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartR.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartR.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartR.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X.LineThickness = 1;
            this.uChartR.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.X.MajorGridLines.Visible = true;
            this.uChartR.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.X.MinorGridLines.Visible = false;
            this.uChartR.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.X.Visible = true;
            this.uChartR.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartR.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartR.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartR.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.X2.Labels.Visible = false;
            this.uChartR.Axis.X2.LineThickness = 1;
            this.uChartR.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.X2.MajorGridLines.Visible = true;
            this.uChartR.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.X2.MinorGridLines.Visible = false;
            this.uChartR.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.X2.Visible = false;
            this.uChartR.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartR.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartR.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartR.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y.LineThickness = 1;
            this.uChartR.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Y.MajorGridLines.Visible = true;
            this.uChartR.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Y.MinorGridLines.Visible = false;
            this.uChartR.Axis.Y.TickmarkInterval = 50;
            this.uChartR.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.Y.Visible = true;
            this.uChartR.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartR.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartR.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartR.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Y2.Labels.Visible = false;
            this.uChartR.Axis.Y2.LineThickness = 1;
            this.uChartR.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartR.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartR.Axis.Y2.TickmarkInterval = 50;
            this.uChartR.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.Y2.Visible = false;
            this.uChartR.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartR.Axis.Z.Labels.ItemFormatString = "";
            this.uChartR.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartR.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z.LineThickness = 1;
            this.uChartR.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Z.MajorGridLines.Visible = true;
            this.uChartR.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Z.MinorGridLines.Visible = false;
            this.uChartR.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.Z.Visible = false;
            this.uChartR.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartR.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartR.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartR.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartR.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartR.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartR.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartR.Axis.Z2.Labels.Visible = false;
            this.uChartR.Axis.Z2.LineThickness = 1;
            this.uChartR.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartR.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartR.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartR.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartR.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartR.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartR.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartR.Axis.Z2.Visible = false;
            this.uChartR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartR.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartR.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartR.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartR.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uChartR.Effects.Effects.Add(gradientEffect1);
            this.uChartR.Location = new System.Drawing.Point(3, 0);
            this.uChartR.Name = "uChartR";
            this.uChartR.Size = new System.Drawing.Size(610, 341);
            this.uChartR.TabIndex = 0;
            this.uChartR.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartR.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uTextUnderLSLP
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            appearance44.TextHAlignAsString = "Right";
            this.uTextUnderLSLP.Appearance = appearance44;
            this.uTextUnderLSLP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderLSLP.Location = new System.Drawing.Point(188, 608);
            this.uTextUnderLSLP.Name = "uTextUnderLSLP";
            this.uTextUnderLSLP.ReadOnly = true;
            this.uTextUnderLSLP.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderLSLP.TabIndex = 37;
            // 
            // uTextUnderLSL
            // 
            appearance45.BackColor = System.Drawing.Color.Gainsboro;
            appearance45.TextHAlignAsString = "Right";
            this.uTextUnderLSL.Appearance = appearance45;
            this.uTextUnderLSL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderLSL.Location = new System.Drawing.Point(116, 608);
            this.uTextUnderLSL.Name = "uTextUnderLSL";
            this.uTextUnderLSL.ReadOnly = true;
            this.uTextUnderLSL.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderLSL.TabIndex = 36;
            // 
            // uLabelUnderLSL
            // 
            this.uLabelUnderLSL.Location = new System.Drawing.Point(12, 608);
            this.uLabelUnderLSL.Name = "uLabelUnderLSL";
            this.uLabelUnderLSL.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnderLSL.TabIndex = 35;
            this.uLabelUnderLSL.Text = "ultraLabel6";
            // 
            // uTextOverUSLP
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            appearance46.TextHAlignAsString = "Right";
            this.uTextOverUSLP.Appearance = appearance46;
            this.uTextOverUSLP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverUSLP.Location = new System.Drawing.Point(188, 584);
            this.uTextOverUSLP.Name = "uTextOverUSLP";
            this.uTextOverUSLP.ReadOnly = true;
            this.uTextOverUSLP.Size = new System.Drawing.Size(70, 21);
            this.uTextOverUSLP.TabIndex = 34;
            // 
            // uTextOverUSL
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            appearance47.TextHAlignAsString = "Right";
            this.uTextOverUSL.Appearance = appearance47;
            this.uTextOverUSL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverUSL.Location = new System.Drawing.Point(116, 584);
            this.uTextOverUSL.Name = "uTextOverUSL";
            this.uTextOverUSL.ReadOnly = true;
            this.uTextOverUSL.Size = new System.Drawing.Size(70, 21);
            this.uTextOverUSL.TabIndex = 33;
            // 
            // uLabelOverUSL
            // 
            this.uLabelOverUSL.Location = new System.Drawing.Point(12, 584);
            this.uLabelOverUSL.Name = "uLabelOverUSL";
            this.uLabelOverUSL.Size = new System.Drawing.Size(100, 20);
            this.uLabelOverUSL.TabIndex = 32;
            this.uLabelOverUSL.Text = "ultraLabel5";
            // 
            // uTextOOC
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            appearance49.TextHAlignAsString = "Right";
            this.uTextOOC.Appearance = appearance49;
            this.uTextOOC.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOOC.Location = new System.Drawing.Point(366, 536);
            this.uTextOOC.Name = "uTextOOC";
            this.uTextOOC.ReadOnly = true;
            this.uTextOOC.Size = new System.Drawing.Size(70, 21);
            this.uTextOOC.TabIndex = 30;
            // 
            // uLabelOOC
            // 
            this.uLabelOOC.Location = new System.Drawing.Point(262, 536);
            this.uLabelOOC.Name = "uLabelOOC";
            this.uLabelOOC.Size = new System.Drawing.Size(100, 20);
            this.uLabelOOC.TabIndex = 29;
            this.uLabelOOC.Text = "ultraLabel4";
            // 
            // uTextUnderLCLP
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            appearance50.TextHAlignAsString = "Right";
            this.uTextUnderLCLP.Appearance = appearance50;
            this.uTextUnderLCLP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderLCLP.Location = new System.Drawing.Point(188, 560);
            this.uTextUnderLCLP.Name = "uTextUnderLCLP";
            this.uTextUnderLCLP.ReadOnly = true;
            this.uTextUnderLCLP.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderLCLP.TabIndex = 28;
            // 
            // uTextUnderLCL
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            appearance51.TextHAlignAsString = "Right";
            this.uTextUnderLCL.Appearance = appearance51;
            this.uTextUnderLCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnderLCL.Location = new System.Drawing.Point(116, 560);
            this.uTextUnderLCL.Name = "uTextUnderLCL";
            this.uTextUnderLCL.ReadOnly = true;
            this.uTextUnderLCL.Size = new System.Drawing.Size(70, 21);
            this.uTextUnderLCL.TabIndex = 27;
            // 
            // uLabelUnderLCL
            // 
            this.uLabelUnderLCL.Location = new System.Drawing.Point(12, 560);
            this.uLabelUnderLCL.Name = "uLabelUnderLCL";
            this.uLabelUnderLCL.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnderLCL.TabIndex = 26;
            this.uLabelUnderLCL.Text = "ultraLabel2";
            // 
            // uTextOverUCLP
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            appearance52.TextHAlignAsString = "Right";
            this.uTextOverUCLP.Appearance = appearance52;
            this.uTextOverUCLP.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverUCLP.Location = new System.Drawing.Point(188, 536);
            this.uTextOverUCLP.Name = "uTextOverUCLP";
            this.uTextOverUCLP.ReadOnly = true;
            this.uTextOverUCLP.Size = new System.Drawing.Size(70, 21);
            this.uTextOverUCLP.TabIndex = 25;
            // 
            // uLabelOverUCL
            // 
            this.uLabelOverUCL.Location = new System.Drawing.Point(12, 536);
            this.uLabelOverUCL.Name = "uLabelOverUCL";
            this.uLabelOverUCL.Size = new System.Drawing.Size(100, 20);
            this.uLabelOverUCL.TabIndex = 23;
            this.uLabelOverUCL.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance19;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance29;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 152);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(420, 376);
            this.uGrid1.TabIndex = 22;
            this.uGrid1.Text = "ultraGrid1";
            // 
            // uTextOverUCL
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            appearance53.TextHAlignAsString = "Right";
            this.uTextOverUCL.Appearance = appearance53;
            this.uTextOverUCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOverUCL.Location = new System.Drawing.Point(116, 536);
            this.uTextOverUCL.Name = "uTextOverUCL";
            this.uTextOverUCL.ReadOnly = true;
            this.uTextOverUCL.Size = new System.Drawing.Size(70, 21);
            this.uTextOverUCL.TabIndex = 24;
            // 
            // uTextXBarCL
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            appearance36.TextHAlignAsString = "Right";
            this.uTextXBarCL.Appearance = appearance36;
            this.uTextXBarCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextXBarCL.Location = new System.Drawing.Point(84, 100);
            this.uTextXBarCL.Name = "uTextXBarCL";
            this.uTextXBarCL.ReadOnly = true;
            this.uTextXBarCL.Size = new System.Drawing.Size(84, 21);
            this.uTextXBarCL.TabIndex = 11;
            // 
            // uLabelCL
            // 
            this.uLabelCL.Location = new System.Drawing.Point(12, 100);
            this.uLabelCL.Name = "uLabelCL";
            this.uLabelCL.Size = new System.Drawing.Size(68, 20);
            this.uLabelCL.TabIndex = 10;
            this.uLabelCL.Text = "CL";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextSampleSize);
            this.uGroupBox1.Controls.Add(this.uTextSpecRangeCode);
            this.uGroupBox1.Controls.Add(this.uLabelMax);
            this.uGroupBox1.Controls.Add(this.uLabelStdDeviation);
            this.uGroupBox1.Controls.Add(this.uLabelMin);
            this.uGroupBox1.Controls.Add(this.uTextMax);
            this.uGroupBox1.Controls.Add(this.uTextMin);
            this.uGroupBox1.Controls.Add(this.uTextStdDev);
            this.uGroupBox1.Controls.Add(this.uLabelR);
            this.uGroupBox1.Controls.Add(this.uLabelXBar);
            this.uGroupBox1.Controls.Add(this.uTextRLCL);
            this.uGroupBox1.Controls.Add(this.uTextRUCL);
            this.uGroupBox1.Controls.Add(this.uTextRCL);
            this.uGroupBox1.Controls.Add(this.uTextXBarLCL);
            this.uGroupBox1.Controls.Add(this.uLabelLCL);
            this.uGroupBox1.Controls.Add(this.uTextXBarUCL);
            this.uGroupBox1.Controls.Add(this.uLabelUCL);
            this.uGroupBox1.Controls.Add(this.uTextTrend);
            this.uGroupBox1.Controls.Add(this.uLabelTrend);
            this.uGroupBox1.Controls.Add(this.uTextUnderTrendP);
            this.uGroupBox1.Controls.Add(this.uTextUnderTrend);
            this.uGroupBox1.Controls.Add(this.uLabelUnderTrend);
            this.uGroupBox1.Controls.Add(this.uTextOverTrendP);
            this.uGroupBox1.Controls.Add(this.uTextOverTrend);
            this.uGroupBox1.Controls.Add(this.uLabelOverTrend);
            this.uGroupBox1.Controls.Add(this.uTextRun);
            this.uGroupBox1.Controls.Add(this.uLabelRun);
            this.uGroupBox1.Controls.Add(this.uTextUnderRunP);
            this.uGroupBox1.Controls.Add(this.uTextUnderRun);
            this.uGroupBox1.Controls.Add(this.uLabelUnderRun);
            this.uGroupBox1.Controls.Add(this.uTextOverRunP);
            this.uGroupBox1.Controls.Add(this.uTextOverRun);
            this.uGroupBox1.Controls.Add(this.uLabelOverRun);
            this.uGroupBox1.Controls.Add(this.uTextOOS);
            this.uGroupBox1.Controls.Add(this.uLabelOOS);
            this.uGroupBox1.Controls.Add(this.uTextUnderLSLP);
            this.uGroupBox1.Controls.Add(this.uTextUnderLSL);
            this.uGroupBox1.Controls.Add(this.uLabelUnderLSL);
            this.uGroupBox1.Controls.Add(this.uTextOverUSLP);
            this.uGroupBox1.Controls.Add(this.uTextOverUSL);
            this.uGroupBox1.Controls.Add(this.uLabelOverUSL);
            this.uGroupBox1.Controls.Add(this.uTextOOC);
            this.uGroupBox1.Controls.Add(this.uLabelOOC);
            this.uGroupBox1.Controls.Add(this.uTextUnderLCLP);
            this.uGroupBox1.Controls.Add(this.uTextUnderLCL);
            this.uGroupBox1.Controls.Add(this.uLabelUnderLCL);
            this.uGroupBox1.Controls.Add(this.uTextOverUCLP);
            this.uGroupBox1.Controls.Add(this.uTextOverUCL);
            this.uGroupBox1.Controls.Add(this.uLabelOverUCL);
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Controls.Add(this.uTextXBarCL);
            this.uGroupBox1.Controls.Add(this.uLabelCL);
            this.uGroupBox1.Controls.Add(this.uTextSpecRange);
            this.uGroupBox1.Controls.Add(this.uTextLowerSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpecLower);
            this.uGroupBox1.Controls.Add(this.uTextUpperSpec);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 104);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(440, 736);
            this.uGroupBox1.TabIndex = 26;
            // 
            // uTextSpecRange
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Appearance = appearance13;
            this.uTextSpecRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Location = new System.Drawing.Point(276, 28);
            this.uTextSpecRange.Name = "uTextSpecRange";
            this.uTextSpecRange.ReadOnly = true;
            this.uTextSpecRange.Size = new System.Drawing.Size(84, 21);
            this.uTextSpecRange.TabIndex = 9;
            // 
            // uTextLowerSpec
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            appearance18.TextHAlignAsString = "Right";
            this.uTextLowerSpec.Appearance = appearance18;
            this.uTextLowerSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLowerSpec.Location = new System.Drawing.Point(84, 28);
            this.uTextLowerSpec.Name = "uTextLowerSpec";
            this.uTextLowerSpec.ReadOnly = true;
            this.uTextLowerSpec.Size = new System.Drawing.Size(92, 21);
            this.uTextLowerSpec.TabIndex = 7;
            // 
            // uLabelSpecLower
            // 
            this.uLabelSpecLower.Location = new System.Drawing.Point(12, 28);
            this.uLabelSpecLower.Name = "uLabelSpecLower";
            this.uLabelSpecLower.Size = new System.Drawing.Size(68, 20);
            this.uLabelSpecLower.TabIndex = 6;
            this.uLabelSpecLower.Text = "규격";
            // 
            // uTextUpperSpec
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            appearance3.TextHAlignAsString = "Right";
            this.uTextUpperSpec.Appearance = appearance3;
            this.uTextUpperSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperSpec.Location = new System.Drawing.Point(180, 28);
            this.uTextUpperSpec.Name = "uTextUpperSpec";
            this.uTextUpperSpec.ReadOnly = true;
            this.uTextUpperSpec.Size = new System.Drawing.Size(92, 21);
            this.uTextUpperSpec.TabIndex = 5;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uChartXBar);
            this.uGroupBox2.Controls.Add(this.uGroupBoxControlLimit);
            this.uGroupBox2.Location = new System.Drawing.Point(440, 104);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(616, 388);
            this.uGroupBox2.TabIndex = 27;
            // 
            // uGroupBoxControlLimit
            // 
            this.uGroupBoxControlLimit.Controls.Add(this.uTextPpu);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelPpu);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextPpl);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelPpl);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextPpk);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelPpk);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextPp);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelPp);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextCpu);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelCpu);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextCpl);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelCpl);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextCpk);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelCpk);
            this.uGroupBoxControlLimit.Controls.Add(this.uTextCp);
            this.uGroupBoxControlLimit.Controls.Add(this.uLabelCp);
            this.uGroupBoxControlLimit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxControlLimit.Location = new System.Drawing.Point(3, 327);
            this.uGroupBoxControlLimit.Name = "uGroupBoxControlLimit";
            this.uGroupBoxControlLimit.Size = new System.Drawing.Size(610, 58);
            this.uGroupBoxControlLimit.TabIndex = 1;
            // 
            // uTextPpu
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            appearance11.TextHAlignAsString = "Right";
            this.uTextPpu.Appearance = appearance11;
            this.uTextPpu.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPpu.Location = new System.Drawing.Point(534, 31);
            this.uTextPpu.Name = "uTextPpu";
            this.uTextPpu.ReadOnly = true;
            this.uTextPpu.Size = new System.Drawing.Size(70, 21);
            this.uTextPpu.TabIndex = 45;
            // 
            // uLabelPpu
            // 
            this.uLabelPpu.Location = new System.Drawing.Point(462, 31);
            this.uLabelPpu.Name = "uLabelPpu";
            this.uLabelPpu.Size = new System.Drawing.Size(65, 20);
            this.uLabelPpu.TabIndex = 44;
            this.uLabelPpu.Text = "ultraLabel4";
            // 
            // uTextPpl
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            appearance57.TextHAlignAsString = "Right";
            this.uTextPpl.Appearance = appearance57;
            this.uTextPpl.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPpl.Location = new System.Drawing.Point(382, 31);
            this.uTextPpl.Name = "uTextPpl";
            this.uTextPpl.ReadOnly = true;
            this.uTextPpl.Size = new System.Drawing.Size(70, 21);
            this.uTextPpl.TabIndex = 43;
            // 
            // uLabelPpl
            // 
            this.uLabelPpl.Location = new System.Drawing.Point(310, 31);
            this.uLabelPpl.Name = "uLabelPpl";
            this.uLabelPpl.Size = new System.Drawing.Size(65, 20);
            this.uLabelPpl.TabIndex = 42;
            this.uLabelPpl.Text = "ultraLabel5";
            // 
            // uTextPpk
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            appearance59.TextHAlignAsString = "Right";
            this.uTextPpk.Appearance = appearance59;
            this.uTextPpk.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPpk.Location = new System.Drawing.Point(230, 31);
            this.uTextPpk.Name = "uTextPpk";
            this.uTextPpk.ReadOnly = true;
            this.uTextPpk.Size = new System.Drawing.Size(70, 21);
            this.uTextPpk.TabIndex = 41;
            // 
            // uLabelPpk
            // 
            this.uLabelPpk.Location = new System.Drawing.Point(158, 31);
            this.uLabelPpk.Name = "uLabelPpk";
            this.uLabelPpk.Size = new System.Drawing.Size(65, 20);
            this.uLabelPpk.TabIndex = 40;
            this.uLabelPpk.Text = "ultraLabel2";
            // 
            // uTextPp
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            appearance60.TextHAlignAsString = "Right";
            this.uTextPp.Appearance = appearance60;
            this.uTextPp.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPp.Location = new System.Drawing.Point(78, 31);
            this.uTextPp.Name = "uTextPp";
            this.uTextPp.ReadOnly = true;
            this.uTextPp.Size = new System.Drawing.Size(70, 21);
            this.uTextPp.TabIndex = 39;
            // 
            // uLabelPp
            // 
            this.uLabelPp.Location = new System.Drawing.Point(6, 31);
            this.uLabelPp.Name = "uLabelPp";
            this.uLabelPp.Size = new System.Drawing.Size(65, 20);
            this.uLabelPp.TabIndex = 38;
            this.uLabelPp.Text = "ultraLabel1";
            // 
            // uTextCpu
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            appearance61.TextHAlignAsString = "Right";
            this.uTextCpu.Appearance = appearance61;
            this.uTextCpu.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpu.Location = new System.Drawing.Point(534, 7);
            this.uTextCpu.Name = "uTextCpu";
            this.uTextCpu.ReadOnly = true;
            this.uTextCpu.Size = new System.Drawing.Size(70, 21);
            this.uTextCpu.TabIndex = 37;
            // 
            // uLabelCpu
            // 
            this.uLabelCpu.Location = new System.Drawing.Point(462, 7);
            this.uLabelCpu.Name = "uLabelCpu";
            this.uLabelCpu.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpu.TabIndex = 36;
            this.uLabelCpu.Text = "ultraLabel4";
            // 
            // uTextCpl
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            appearance62.TextHAlignAsString = "Right";
            this.uTextCpl.Appearance = appearance62;
            this.uTextCpl.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpl.Location = new System.Drawing.Point(382, 7);
            this.uTextCpl.Name = "uTextCpl";
            this.uTextCpl.ReadOnly = true;
            this.uTextCpl.Size = new System.Drawing.Size(70, 21);
            this.uTextCpl.TabIndex = 35;
            // 
            // uLabelCpl
            // 
            this.uLabelCpl.Location = new System.Drawing.Point(310, 7);
            this.uLabelCpl.Name = "uLabelCpl";
            this.uLabelCpl.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpl.TabIndex = 34;
            this.uLabelCpl.Text = "ultraLabel5";
            // 
            // uTextCpk
            // 
            appearance63.BackColor = System.Drawing.Color.Gainsboro;
            appearance63.TextHAlignAsString = "Right";
            this.uTextCpk.Appearance = appearance63;
            this.uTextCpk.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpk.Location = new System.Drawing.Point(230, 7);
            this.uTextCpk.Name = "uTextCpk";
            this.uTextCpk.ReadOnly = true;
            this.uTextCpk.Size = new System.Drawing.Size(70, 21);
            this.uTextCpk.TabIndex = 33;
            // 
            // uLabelCpk
            // 
            this.uLabelCpk.Location = new System.Drawing.Point(158, 7);
            this.uLabelCpk.Name = "uLabelCpk";
            this.uLabelCpk.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpk.TabIndex = 32;
            this.uLabelCpk.Text = "ultraLabel2";
            // 
            // uTextCp
            // 
            appearance64.BackColor = System.Drawing.Color.Gainsboro;
            appearance64.TextHAlignAsString = "Right";
            this.uTextCp.Appearance = appearance64;
            this.uTextCp.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCp.Location = new System.Drawing.Point(78, 7);
            this.uTextCp.Name = "uTextCp";
            this.uTextCp.ReadOnly = true;
            this.uTextCp.Size = new System.Drawing.Size(70, 21);
            this.uTextCp.TabIndex = 31;
            // 
            // uLabelCp
            // 
            this.uLabelCp.Location = new System.Drawing.Point(6, 7);
            this.uLabelCp.Name = "uLabelCp";
            this.uLabelCp.Size = new System.Drawing.Size(65, 20);
            this.uLabelCp.TabIndex = 30;
            this.uLabelCp.Text = "ultraLabel1";
            // 
            // uChartXBar
            // 
            this.uChartXBar.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement2.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement2.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartXBar.Axis.PE = paintElement2;
            this.uChartXBar.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartXBar.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartXBar.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartXBar.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X.LineThickness = 1;
            this.uChartXBar.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.X.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.X.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.X.Visible = true;
            this.uChartXBar.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartXBar.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartXBar.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.X2.Labels.Visible = false;
            this.uChartXBar.Axis.X2.LineThickness = 1;
            this.uChartXBar.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.X2.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.X2.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.X2.Visible = false;
            this.uChartXBar.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartXBar.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartXBar.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartXBar.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y.LineThickness = 1;
            this.uChartXBar.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Y.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Y.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.Y.TickmarkInterval = 50;
            this.uChartXBar.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.Y.Visible = true;
            this.uChartXBar.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartXBar.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartXBar.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartXBar.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Y2.Labels.Visible = false;
            this.uChartXBar.Axis.Y2.LineThickness = 1;
            this.uChartXBar.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.Y2.TickmarkInterval = 50;
            this.uChartXBar.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.Y2.Visible = false;
            this.uChartXBar.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartXBar.Axis.Z.Labels.ItemFormatString = "";
            this.uChartXBar.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z.LineThickness = 1;
            this.uChartXBar.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Z.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Z.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.Z.Visible = false;
            this.uChartXBar.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartXBar.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartXBar.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartXBar.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartXBar.Axis.Z2.Labels.Visible = false;
            this.uChartXBar.Axis.Z2.LineThickness = 1;
            this.uChartXBar.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartXBar.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartXBar.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartXBar.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartXBar.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartXBar.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartXBar.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartXBar.Axis.Z2.Visible = false;
            this.uChartXBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartXBar.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartXBar.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartXBar.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartXBar.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartXBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uChartXBar.Effects.Effects.Add(gradientEffect2);
            this.uChartXBar.Location = new System.Drawing.Point(3, 0);
            this.uChartXBar.Name = "uChartXBar";
            this.uChartXBar.Size = new System.Drawing.Size(610, 327);
            this.uChartXBar.TabIndex = 2;
            this.uChartXBar.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartXBar.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // frmSTA0064
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBox3);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0064";
            this.Load += new System.EventHandler(this.frmSTA0064_Load);
            this.Activated += new System.EventHandler(this.frmSTA0064_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0064_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRangeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSampleSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderTrendP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverTrendP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderRunP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverRunP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChartR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLSLP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUSLP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLCLP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnderLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUCLP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOverUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextXBarCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxControlLimit)).EndInit();
            this.uGroupBoxControlLimit.ResumeLayout(false);
            this.uGroupBoxControlLimit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPpk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartXBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRangeCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSampleSize;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchRev;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelMax;
        private Infragistics.Win.Misc.UltraLabel uLabelStdDeviation;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRev;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelMin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdDev;
        private Infragistics.Win.Misc.UltraLabel uLabelR;
        private Infragistics.Win.Misc.UltraLabel uLabelXBar;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRLCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextXBarLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelLCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextXBarUCL;
        private Infragistics.Win.Misc.UltraLabel uLabelUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTrend;
        private Infragistics.Win.Misc.UltraLabel uLabelTrend;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderTrendP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderTrend;
        private Infragistics.Win.Misc.UltraLabel uLabelUnderTrend;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverTrendP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverTrend;
        private Infragistics.Win.Misc.UltraLabel uLabelOverTrend;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRun;
        private Infragistics.Win.Misc.UltraLabel uLabelRun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderRunP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderRun;
        private Infragistics.Win.Misc.UltraLabel uLabelUnderRun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverRunP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverRun;
        private Infragistics.Win.Misc.UltraLabel uLabelOverRun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOOS;
        private Infragistics.Win.Misc.UltraLabel uLabelOOS;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinChart.UltraChart uChartR;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderLSLP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderLSL;
        private Infragistics.Win.Misc.UltraLabel uLabelUnderLSL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverUSLP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverUSL;
        private Infragistics.Win.Misc.UltraLabel uLabelOverUSL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOOC;
        private Infragistics.Win.Misc.UltraLabel uLabelOOC;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderLCLP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnderLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelUnderLCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverUCLP;
        private Infragistics.Win.Misc.UltraLabel uLabelOverUCL;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOverUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextXBarCL;
        private Infragistics.Win.Misc.UltraLabel uLabelCL;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRange;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLowerSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperSpec;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxControlLimit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPpu;
        private Infragistics.Win.Misc.UltraLabel uLabelPpu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPpl;
        private Infragistics.Win.Misc.UltraLabel uLabelPpl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPpk;
        private Infragistics.Win.Misc.UltraLabel uLabelPpk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPp;
        private Infragistics.Win.Misc.UltraLabel uLabelPp;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpu;
        private Infragistics.Win.Misc.UltraLabel uLabelCpu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpl;
        private Infragistics.Win.Misc.UltraLabel uLabelCpl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpk;
        private Infragistics.Win.Misc.UltraLabel uLabelCpk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCp;
        private Infragistics.Win.Misc.UltraLabel uLabelCp;
        private Infragistics.Win.UltraWinChart.UltraChart uChartXBar;

    }
}