﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 수입검사규격서 UI                                     */
/* 모듈(분류)명 : 수입검사규격서 SpecNo POPUP                           */
/* 프로그램ID   : frmPOP0015.cs                                         */
/* 프로그램명   : 수입검사규격서 SpecNo POPUP                           */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-09-02                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;


namespace QRPSTA.UI
{
    public partial class frmPOP0015 : Form
    {
        // 리소소 호출
        QRPGlobal SysRes = new QRPGlobal();

        // 규격서정보 속성
        private string strPlantCode;
        private string strStdNumber;
        private string strStdSeq;
        private string strMaterialCode;
        private string strMaterialName;
        private string strSpecNo;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string StdNumber
        {
            get { return strStdNumber; }
            set { strStdNumber = value; }
        }

        public string StdSeq
        {
            get { return strStdSeq; }
            set { strStdSeq = value; }
        }

        public string MaterialCode
        {
            get { return strMaterialCode; }
            set { strMaterialCode = value; }
        }

        public string MaterialName
        {
            get { return strMaterialName; }
            set { strMaterialName = value; }
        }

        public string SpecNo
        {
            get { return strSpecNo; }
            set { strSpecNo = value; }
        }

        public frmPOP0015()
        {
            InitializeComponent();
        }

        private void frmPOP0015_Load(object sender, EventArgs e)
        {
            //초기화 메소드 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.uComboSearchPlant.Value = strPlantCode;
            this.uTextSearchMaterialCode.Text = strMaterialCode;
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화


        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "PlantCode", "PlantName", dt);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "StdSeq", "표준번호순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "SpecNo", "SpecNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼초기화


        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        // 조회 Method
        private void Search()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            this.Cursor = Cursors.WaitCursor;

            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
            QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
            brwChannel.mfCredentials(clsHeader);

            string strPlantCode = this.uComboSearchPlant.Value.ToString();
            string strMaterialCode = this.uTextSearchMaterialCode.Text;

            DataTable dtHeader = clsHeader.mfReadISoMaterialInspectSpec_SpecNo(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

            this.uGridList.DataSource = dtHeader;
            this.uGridList.DataBind();

            this.Cursor = Cursors.Default;

            DialogResult DResult = new DialogResult();
            WinMessageBox msg = new WinMessageBox();
            if (dtHeader.Rows.Count == 0)
                DResult = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
        }

        // 자재 팝업창

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.ShowDialog();
                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색버튼 클릭 이벤트

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        // 확인버튼 클릭 이벤트

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            strPlantCode = this.uGridList.ActiveRow.Cells["PlantCode"].Value.ToString();
            strStdNumber = this.uGridList.ActiveRow.Cells["StdNumber"].Value.ToString();
            strStdSeq = this.uGridList.ActiveRow.Cells["StdSeq"].Value.ToString();
            strMaterialCode = this.uGridList.ActiveRow.Cells["MaterialCode"].Value.ToString();
            strMaterialName = this.uGridList.ActiveRow.Cells["MaterialName"].Value.ToString();
            strSpecNo = this.uGridList.ActiveRow.Cells["SpecNo"].Value.ToString();

            this.Close();
        }

        // 닫기버튼 클릭 이벤트

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // 그리드 더블클릭 이벤트

        private void uGridList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
            strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();
            strStdSeq = e.Row.Cells["StdSeq"].Value.ToString();
            strMaterialCode = e.Row.Cells["MaterialCode"].Value.ToString();
            strMaterialName = e.Row.Cells["MaterialName"].Value.ToString();
            strSpecNo = e.Row.Cells["SpecNo"].Value.ToString();

            this.Close();
        }

        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text == "")
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            //제품명 조회 메소드 호출하여 처리결과 정보를 리턴 받는다.
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "자재를 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialName.Text = "";
                                this.uTextSearchMaterialCode.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.Text == this.uTextSearchMaterialCode.SelectedText)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                return dtMaterial;
            }
            finally
            {
            }
        }
    }
}
