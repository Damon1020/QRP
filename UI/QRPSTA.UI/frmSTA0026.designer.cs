﻿namespace QRPSTA.UI
{
    partial class frmSTA0026
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0026));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxListInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridListInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxCControl = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChart1 = new Infragistics.Win.UltraWinChart.UltraChart();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxListInfo)).BeginInit();
            this.uGroupBoxListInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridListInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCControl)).BeginInit();
            this.uGroupBoxCControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChart1)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraDateTimeEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraDateTimeEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor7);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor6);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 245;
            // 
            // ultraDateTimeEditor2
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor2.Appearance = appearance19;
            this.ultraDateTimeEditor2.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(600, 12);
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor2.TabIndex = 233;
            // 
            // ultraDateTimeEditor1
            // 
            appearance54.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.Appearance = appearance54;
            this.ultraDateTimeEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(488, 12);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 233;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(588, 16);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel2.TabIndex = 230;
            this.ultraLabel2.Text = "~";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(372, 36);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(110, 20);
            this.uLabel3.TabIndex = 229;
            this.uLabel3.Text = "3";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(12, 36);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(110, 20);
            this.uLabel2.TabIndex = 228;
            this.uLabel2.Text = "2";
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(372, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(110, 20);
            this.uLabel1.TabIndex = 227;
            this.uLabel1.Text = "1";
            // 
            // ultraTextEditor7
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance18;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(604, 36);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor7.TabIndex = 3;
            // 
            // ultraTextEditor2
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance16;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(244, 36);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor2.TabIndex = 3;
            // 
            // ultraTextEditor6
            // 
            appearance14.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance14;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor6.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor6.Location = new System.Drawing.Point(488, 36);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor6.TabIndex = 2;
            // 
            // ultraTextEditor1
            // 
            appearance17.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance17;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor1.ButtonsRight.Add(editorButton2);
            this.ultraTextEditor1.Location = new System.Drawing.Point(128, 36);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor1.TabIndex = 2;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // uGroupBoxListInfo
            // 
            this.uGroupBoxListInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxListInfo.Controls.Add(this.uGridListInfo);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel4);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel5);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel6);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel7);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel8);
            this.uGroupBoxListInfo.Controls.Add(this.ultraTextEditor9);
            this.uGroupBoxListInfo.Controls.Add(this.ultraTextEditor8);
            this.uGroupBoxListInfo.Controls.Add(this.ultraTextEditor5);
            this.uGroupBoxListInfo.Controls.Add(this.ultraTextEditor4);
            this.uGroupBoxListInfo.Controls.Add(this.ultraTextEditor3);
            this.uGroupBoxListInfo.Controls.Add(this.uLabel9);
            this.uGroupBoxListInfo.Controls.Add(this.ultraComboEditor1);
            this.uGroupBoxListInfo.Location = new System.Drawing.Point(0, 100);
            this.uGroupBoxListInfo.Name = "uGroupBoxListInfo";
            this.uGroupBoxListInfo.Size = new System.Drawing.Size(1070, 370);
            this.uGroupBoxListInfo.TabIndex = 246;
            // 
            // uGridListInfo
            // 
            this.uGridListInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridListInfo.DisplayLayout.Appearance = appearance5;
            this.uGridListInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridListInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridListInfo.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridListInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridListInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridListInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridListInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridListInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridListInfo.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridListInfo.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridListInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridListInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridListInfo.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridListInfo.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridListInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridListInfo.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridListInfo.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridListInfo.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridListInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridListInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridListInfo.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridListInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridListInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridListInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridListInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridListInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridListInfo.Location = new System.Drawing.Point(12, 56);
            this.uGridListInfo.Name = "uGridListInfo";
            this.uGridListInfo.Size = new System.Drawing.Size(1048, 304);
            this.uGridListInfo.TabIndex = 233;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(12, 28);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 231;
            this.uLabel4.Text = "4";
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(268, 28);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(100, 20);
            this.uLabel5.TabIndex = 232;
            this.uLabel5.Text = "5";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(508, 28);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(100, 20);
            this.uLabel6.TabIndex = 227;
            this.uLabel6.Text = "6";
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 52);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(100, 20);
            this.uLabel7.TabIndex = 228;
            this.uLabel7.Text = "7";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(268, 52);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(100, 20);
            this.uLabel8.TabIndex = 229;
            this.uLabel8.Text = "8";
            // 
            // ultraTextEditor9
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance15;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(612, 52);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor9.TabIndex = 3;
            // 
            // ultraTextEditor8
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance20;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(612, 28);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor8.TabIndex = 3;
            // 
            // ultraTextEditor5
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance21;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(372, 52);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor5.TabIndex = 3;
            // 
            // ultraTextEditor4
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance22;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(372, 28);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor4.TabIndex = 3;
            // 
            // ultraTextEditor3
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance23;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(116, 52);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(110, 21);
            this.ultraTextEditor3.TabIndex = 3;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(508, 52);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(100, 20);
            this.uLabel9.TabIndex = 230;
            this.uLabel9.Text = "9";
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.Location = new System.Drawing.Point(116, 28);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(130, 21);
            this.ultraComboEditor1.TabIndex = 1;
            this.ultraComboEditor1.Text = "ultraComboEditor1";
            // 
            // uGroupBoxCControl
            // 
            this.uGroupBoxCControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxCControl.Controls.Add(this.uChart1);
            this.uGroupBoxCControl.Location = new System.Drawing.Point(0, 476);
            this.uGroupBoxCControl.Name = "uGroupBoxCControl";
            this.uGroupBoxCControl.Size = new System.Drawing.Size(1070, 370);
            this.uGroupBoxCControl.TabIndex = 246;
            // 
            // uChart1
            // 
            this.uChart1.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChart1.Axis.PE = paintElement1;
            this.uChart1.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart1.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart1.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart1.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X.LineThickness = 1;
            this.uChart1.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.X.MajorGridLines.Visible = true;
            this.uChart1.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.X.MinorGridLines.Visible = false;
            this.uChart1.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.X.Visible = true;
            this.uChart1.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart1.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart1.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart1.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.X2.Labels.Visible = false;
            this.uChart1.Axis.X2.LineThickness = 1;
            this.uChart1.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.X2.MajorGridLines.Visible = true;
            this.uChart1.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.X2.MinorGridLines.Visible = false;
            this.uChart1.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.X2.Visible = false;
            this.uChart1.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart1.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart1.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart1.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y.LineThickness = 1;
            this.uChart1.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Y.MajorGridLines.Visible = true;
            this.uChart1.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Y.MinorGridLines.Visible = false;
            this.uChart1.Axis.Y.TickmarkInterval = 40;
            this.uChart1.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.Y.Visible = true;
            this.uChart1.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart1.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart1.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart1.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Y2.Labels.Visible = false;
            this.uChart1.Axis.Y2.LineThickness = 1;
            this.uChart1.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Y2.MajorGridLines.Visible = true;
            this.uChart1.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Y2.MinorGridLines.Visible = false;
            this.uChart1.Axis.Y2.TickmarkInterval = 40;
            this.uChart1.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.Y2.Visible = false;
            this.uChart1.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart1.Axis.Z.Labels.ItemFormatString = "";
            this.uChart1.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart1.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z.LineThickness = 1;
            this.uChart1.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Z.MajorGridLines.Visible = true;
            this.uChart1.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Z.MinorGridLines.Visible = false;
            this.uChart1.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.Z.Visible = false;
            this.uChart1.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart1.Axis.Z2.Labels.ItemFormatString = "";
            this.uChart1.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart1.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart1.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart1.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart1.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart1.Axis.Z2.Labels.Visible = false;
            this.uChart1.Axis.Z2.LineThickness = 1;
            this.uChart1.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart1.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Z2.MajorGridLines.Visible = true;
            this.uChart1.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart1.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart1.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart1.Axis.Z2.MinorGridLines.Visible = false;
            this.uChart1.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart1.Axis.Z2.Visible = false;
            this.uChart1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChart1.ColorModel.AlphaLevel = ((byte)(150));
            this.uChart1.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChart1.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChart1.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChart1.Effects.Effects.Add(gradientEffect1);
            this.uChart1.Location = new System.Drawing.Point(12, 28);
            this.uChart1.Name = "uChart1";
            this.uChart1.Size = new System.Drawing.Size(1044, 328);
            this.uChart1.TabIndex = 0;
            this.uChart1.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChart1.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 244;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmSTA0026
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxCControl);
            this.Controls.Add(this.uGroupBoxListInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0026";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.Activated += new System.EventHandler(this.Form7_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxListInfo)).EndInit();
            this.uGroupBoxListInfo.ResumeLayout(false);
            this.uGroupBoxListInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridListInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCControl)).EndInit();
            this.uGroupBoxCControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxListInfo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxCControl;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridListInfo;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.UltraWinChart.UltraChart uChart1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
    }
}