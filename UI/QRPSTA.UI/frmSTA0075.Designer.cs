﻿namespace QRPSTA.UI
{
    partial class frmSTA0075
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0075));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonSendMail = new Infragistics.Win.Misc.UltraButton();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateSearchDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProductDivide = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxDaily = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDailyState = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxQCNState = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridQCNStats = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDaily)).BeginInit();
            this.uGroupBoxDaily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDailyState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxQCNState)).BeginInit();
            this.uGroupBoxQCNState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNStats)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uButtonSendMail);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProductDivide);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uTextSearchCustomerName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance16;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(224, 36);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerName.TabIndex = 352;
            // 
            // uTextSearchCustomer
            // 
            appearance15.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomer.ButtonsRight.Add(editorButton1);
            this.uTextSearchCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextSearchCustomer.Location = new System.Drawing.Point(120, 36);
            this.uTextSearchCustomer.MaxLength = 20;
            this.uTextSearchCustomer.Name = "uTextSearchCustomer";
            this.uTextSearchCustomer.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomer.TabIndex = 351;
            this.uTextSearchCustomer.ValueChanged += new System.EventHandler(this.uTextSearchCustomer_ValueChanged);
            this.uTextSearchCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomer_KeyDown);
            this.uTextSearchCustomer.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomer_EditorButtonClick);
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(448, 36);
            this.uComboSearchPackage.MaxLength = 50;
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackage.TabIndex = 350;
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(16, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 348;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(344, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 349;
            this.uLabelSearchPackage.Text = "PACKAGE";
            // 
            // uButtonSendMail
            // 
            this.uButtonSendMail.Location = new System.Drawing.Point(960, 8);
            this.uButtonSendMail.Name = "uButtonSendMail";
            this.uButtonSendMail.Size = new System.Drawing.Size(100, 25);
            this.uButtonSendMail.TabIndex = 25;
            this.uButtonSendMail.Click += new System.EventHandler(this.uButtonSendMail_Click);
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(448, 12);
            this.uComboSearchProductActionType.MaxLength = 40;
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(119, 21);
            this.uComboSearchProductActionType.TabIndex = 24;
            // 
            // uDateSearchDate
            // 
            this.uDateSearchDate.Location = new System.Drawing.Point(120, 12);
            this.uDateSearchDate.Name = "uDateSearchDate";
            this.uDateSearchDate.Size = new System.Drawing.Size(116, 21);
            this.uDateSearchDate.TabIndex = 23;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1028, 32);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(36, 21);
            this.uComboSearchPlant.TabIndex = 4;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(16, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 3;
            // 
            // uLabelSearchProductDivide
            // 
            this.uLabelSearchProductDivide.Location = new System.Drawing.Point(344, 12);
            this.uLabelSearchProductDivide.Name = "uLabelSearchProductDivide";
            this.uLabelSearchProductDivide.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProductDivide.TabIndex = 2;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1008, 32);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGroupBoxDaily
            // 
            this.uGroupBoxDaily.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxDaily.Controls.Add(this.uGridDailyState);
            this.uGroupBoxDaily.Location = new System.Drawing.Point(0, 100);
            this.uGroupBoxDaily.Name = "uGroupBoxDaily";
            this.uGroupBoxDaily.Size = new System.Drawing.Size(1070, 358);
            this.uGroupBoxDaily.TabIndex = 6;
            // 
            // uGridDailyState
            // 
            this.uGridDailyState.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDailyState.DisplayLayout.Appearance = appearance13;
            this.uGridDailyState.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDailyState.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDailyState.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDailyState.DisplayLayout.GroupByBox.BandLabelAppearance = appearance25;
            this.uGridDailyState.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance26.BackColor2 = System.Drawing.SystemColors.Control;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDailyState.DisplayLayout.GroupByBox.PromptAppearance = appearance26;
            this.uGridDailyState.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDailyState.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDailyState.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDailyState.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridDailyState.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDailyState.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDailyState.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDailyState.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridDailyState.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDailyState.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDailyState.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridDailyState.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridDailyState.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDailyState.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridDailyState.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridDailyState.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDailyState.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridDailyState.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDailyState.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDailyState.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDailyState.Location = new System.Drawing.Point(5, 8);
            this.uGridDailyState.Name = "uGridDailyState";
            this.uGridDailyState.Size = new System.Drawing.Size(1060, 322);
            this.uGridDailyState.TabIndex = 5;
            this.uGridDailyState.Text = "ultraGrid1";
            // 
            // uGroupBoxQCNState
            // 
            this.uGroupBoxQCNState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxQCNState.Controls.Add(this.uGridQCNStats);
            this.uGroupBoxQCNState.Location = new System.Drawing.Point(0, 460);
            this.uGroupBoxQCNState.Name = "uGroupBoxQCNState";
            this.uGroupBoxQCNState.Size = new System.Drawing.Size(1070, 358);
            this.uGroupBoxQCNState.TabIndex = 7;
            // 
            // uGridQCNStats
            // 
            this.uGridQCNStats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNStats.DisplayLayout.Appearance = appearance1;
            this.uGridQCNStats.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNStats.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNStats.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNStats.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridQCNStats.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNStats.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridQCNStats.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNStats.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNStats.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNStats.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridQCNStats.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNStats.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNStats.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNStats.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridQCNStats.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNStats.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNStats.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridQCNStats.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridQCNStats.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNStats.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNStats.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridQCNStats.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNStats.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridQCNStats.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNStats.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNStats.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNStats.Location = new System.Drawing.Point(5, 8);
            this.uGridQCNStats.Name = "uGridQCNStats";
            this.uGridQCNStats.Size = new System.Drawing.Size(1060, 314);
            this.uGridQCNStats.TabIndex = 6;
            this.uGridQCNStats.Text = "ultraGrid1";
            // 
            // frmSTA0075
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 820);
            this.Controls.Add(this.uGroupBoxQCNState);
            this.Controls.Add(this.uGroupBoxDaily);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0075";
            this.Load += new System.EventHandler(this.frmSTA0075_Load);
            this.Activated += new System.EventHandler(this.frmSTA0075_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0075_FormClosing);
            this.Resize += new System.EventHandler(this.frmSTA0075_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDaily)).EndInit();
            this.uGroupBoxDaily.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDailyState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxQCNState)).EndInit();
            this.uGroupBoxQCNState.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNStats)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductDivide;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraButton uButtonSendMail;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDaily;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDailyState;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxQCNState;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNStats;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
    }
}