﻿namespace QRPSTA.UI
{
    partial class frmSTA0079
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0079));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridProc = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProc)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProductProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(256, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 337;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(564, 12);
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(72, 21);
            this.uComboSearchMonth.TabIndex = 336;
            this.uComboSearchMonth.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMonth
            // 
            this.uLabelSearchMonth.Location = new System.Drawing.Point(460, 12);
            this.uLabelSearchMonth.Name = "uLabelSearchMonth";
            this.uLabelSearchMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMonth.TabIndex = 335;
            this.uLabelSearchMonth.Text = "ultraLabel1";
            // 
            // uTextSearchYear
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance1;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(360, 12);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(72, 21);
            this.uTextSearchYear.TabIndex = 334;
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(772, 12);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(119, 21);
            this.uComboSearchProductActionType.TabIndex = 333;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uComboSearchPlant
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Appearance = appearance14;
            this.uComboSearchPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(119, 21);
            this.uComboSearchPlant.TabIndex = 332;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchProductProductActionType
            // 
            this.uLabelSearchProductProductActionType.Location = new System.Drawing.Point(668, 12);
            this.uLabelSearchProductProductActionType.Name = "uLabelSearchProductProductActionType";
            this.uLabelSearchProductProductActionType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProductProductActionType.TabIndex = 331;
            this.uLabelSearchProductProductActionType.Text = "ultraLabel1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 330;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridProc
            // 
            this.uGridProc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProc.DisplayLayout.Appearance = appearance2;
            this.uGridProc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProc.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProc.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridProc.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProc.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridProc.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProc.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProc.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProc.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridProc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProc.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProc.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridProc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProc.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProc.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridProc.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridProc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridProc.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridProc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProc.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridProc.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProc.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProc.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProc.Location = new System.Drawing.Point(0, 80);
            this.uGridProc.Name = "uGridProc";
            this.uGridProc.Size = new System.Drawing.Size(1070, 760);
            this.uGridProc.TabIndex = 6;
            this.uGridProc.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGridProc_InitializeLayout);
            // 
            // frmSTA0079
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridProc);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0079";
            this.Load += new System.EventHandler(this.frmSTA0079_Load);
            this.Activated += new System.EventHandler(this.frmSTA0079_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0079_FormClosing);
            this.Resize += new System.EventHandler(this.frmSTA0079_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}