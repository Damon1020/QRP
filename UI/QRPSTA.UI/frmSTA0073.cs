﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0073.cs                                         */
/* 프로그램명   : QCN 종합현황                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-10                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0073 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0073()
        {
            InitializeComponent();
        }

        #region Form Events...
        private void frmSTA0073_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("QCN 종합현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            SetToolAuth();
            InitEtc();
            InitTab();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        private void frmSTA0073_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0073_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridCustomerList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridCustomerList);
                }
                if (this.uGridImputeDeptList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridImputeDeptList);
                }
                if(this.uGridCustomerList.Rows.Count.Equals(0) && this.uGridImputeDeptList.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 조회조건 체크
                if (string.IsNullOrEmpty(this.uComboSearchPlantCode.Value.ToString()) || this.uComboSearchPlantCode.Value == DBNull.Value)
                {

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlantCode.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Length < 4)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001107", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }

                // 변수 설정
                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                string strYear = this.uTextSearchYear.Text;

                // 그리드 DropDown 설정 메소드 호출
                SetGridDropDown();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 조회

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                DataTable dtDeptList = clsReport.mfReadINSProcInspect_frmSTA0073_D1(strPlantCode, strYear, m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerList = clsReport.mfReadINSProcInspect_frmSTA0073_D2(strPlantCode, strYear, m_resSys.GetString("SYS_LANG"));

                this.uGridImputeDeptList.DataSource = dtDeptList;
                this.uGridImputeDeptList.DataBind();

                this.uGridCustomerList.DataSource = dtCustomerList;
                this.uGridCustomerList.DataBind();

                #endregion

                #region 그리드 색상변경

                if (this.uGridImputeDeptList.Rows.Count > 0)
                {
                    // 귀책부서별
                    int intLastRowIndex = this.uGridImputeDeptList.Rows.Count - 1;
                    for (int i = 0; i < this.uGridImputeDeptList.DisplayLayout.Bands[0].Columns.Count; i++)
                    {
                        this.uGridImputeDeptList.Rows[intLastRowIndex].Cells[i].Appearance.BackColor = Color.MistyRose;
                        this.uGridImputeDeptList.Rows[intLastRowIndex].Cells[i].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                    }
                }

                if (this.uGridCustomerList.Rows.Count > 0)
                {
                    // 고객사별
                    int intLastRowIndex = this.uGridCustomerList.Rows.Count - 1;
                    this.uGridCustomerList.Rows[intLastRowIndex].Cells["ImputationDeptCode"].SetValue(string.Empty, false);

                    for (int i = 0; i < this.uGridCustomerList.Rows.Count; i++)
                    {
                        if (this.uGridCustomerList.Rows[i].Cells["ImputationDeptCode"].Value.ToString().Equals("Total"))
                        {
                            for (int j = 1; j < uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                        else if (i.Equals(intLastRowIndex))
                        {
                            for (int j = 0; j < uGridCustomerList.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridCustomerList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                    }
                }

                #endregion

                // 차트 초기화
                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);

                // Legend Clear
                this.uChart.CompositeChart.Legends.Clear();

                if (this.uGridImputeDeptList.Rows.Count > 0)
                {
                    // Chart Draw 메소드 호출
                    DrawChart();
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                    , m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 귀책부서별
                
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridImputeDeptList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "ImputationDeptCode", "귀책부서코드", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridImputeDeptList, 0, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 그리드 편집불가 상태
                this.uGridImputeDeptList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridImputeDeptList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridImputeDeptList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridImputeDeptList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                #endregion

                #region 고객사별

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridCustomerList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "CustomerCode", "고객", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "ImputationDeptCode", "귀책부서코드", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCustomerList, 0, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 그리드 편집불가 상태
                this.uGridCustomerList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // Font Size 설정
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCustomerList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridCustomerList.DisplayLayout.Bands[0].Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                #endregion

                #region DropDown 설정

                // 고객사
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridCustomerList, 0, "CustomerCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "GrandTotal", "GrandTotal", dtCustomer);

                DataTable dtTemp = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGridImputeDeptList, 0, "ImputationDeptCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "Total", "Total", dtTemp);
                wGrid.mfSetGridColumnValueList(this.uGridCustomerList, 0, "ImputationDeptCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "Total", "Total", dtTemp);

                #endregion

                #region Total 컬럼 색상, 폰트설정

                this.uGridImputeDeptList.DisplayLayout.Bands[0].Columns["Total"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridImputeDeptList.DisplayLayout.Bands[0].Columns["Total"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["Total"].CellAppearance.BackColor = Color.MistyRose;
                this.uGridCustomerList.DisplayLayout.Bands[0].Columns["Total"].CellAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabInfo, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                                            , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                                            , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                                            , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 그리드 DropDown 설정 메소드
        /// </summary>
        private void SetGridDropDown()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                QRPBrowser brwChannel = new QRPBrowser();

                // BL 연결
                // 부서
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                WinGrid wGrid = new WinGrid();
                wGrid.mfSetGridColumnValueList(this.uGridImputeDeptList, 0, "ImputationDeptCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "Total", "Total", dtDept);
                wGrid.mfSetGridColumnValueList(this.uGridCustomerList, 0, "ImputationDeptCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "Total", "Total", dtDept);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 차트 그리는 메소드
        private void DrawChart()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region DataTable 설정
                
                // 컬럼생성
                System.Data.DataTable dtChart = new System.Data.DataTable("QCNChart");
                dtChart.Columns.Add("Month", typeof(string));
                for (int i = 0; i < this.uGridImputeDeptList.Rows.Count; i++)
                {
                    dtChart.Columns.Add(this.uGridImputeDeptList.Rows[i].Cells["ImputationDeptCode"].Text, typeof(Int32));
                }

                // Data 저장
                DataRow drRow;
                for (int j = 1; j <= 12; j++)
                {
                    drRow = dtChart.NewRow();
                    drRow["Month"] = this.uGridImputeDeptList.DisplayLayout.Bands[0].Columns[j].Header.Caption;
                    for (int i = 0; i < this.uGridImputeDeptList.Rows.Count; i++)
                    {
                        drRow[this.uGridImputeDeptList.Rows[i].Cells["ImputationDeptCode"].Text] = this.uGridImputeDeptList.Rows[i].Cells[j].Value;
                    }
                    dtChart.Rows.Add(drRow);
                }

                #endregion

                #region 차트 그리기

                // 차트 유형 지정
                this.uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Composite;

                Infragistics.UltraChart.Resources.Appearance.ChartArea chartArea = new Infragistics.UltraChart.Resources.Appearance.ChartArea();
                this.uChart.CompositeChart.ChartAreas.Add(chartArea);

                #region Column Chart

                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisColumn = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisColumn.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                xAxisColumn.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisColumn.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
                xAxisColumn.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisColumn.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisColumn.Extent = 25;

                // Y축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem yAxis = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                yAxis.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                yAxis.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                yAxis.RangeType = Infragistics.UltraChart.Shared.Styles.AxisRangeType.Automatic;
                yAxis.NumericAxisType = Infragistics.UltraChart.Shared.Styles.NumericAxisType.Linear;
                yAxis.Extent = 150;
                yAxis.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                yAxis.Labels.ItemFormatString = "<DATA_VALUE:#,###>";

                // Y축 간격 설정
                yAxis.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.DataInterval;
                
                // Max값 찾기
                double dblMaxValue = Convert.ToDouble((object)dtChart.Compute("MAX(Total)", ""));
                //double dblMaxValue = dtChart.AsEnumerable().Max(w => w.Field<double>("Total"));
                double intInterval = Math.Round(dblMaxValue / 10, 0, MidpointRounding.AwayFromZero);

                if (intInterval > 0)
                    yAxis.TickmarkInterval = intInterval;
                else
                    yAxis.TickmarkInterval = 1;


                chartArea.Axes.Add(xAxisColumn);
                chartArea.Axes.Add(yAxis);

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance stackColLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                stackColLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.StackColumnChart;
                stackColLayer.ChartArea = chartArea;
                stackColLayer.AxisX = xAxisColumn;
                stackColLayer.AxisY = yAxis;
                this.uChart.CompositeChart.ChartLayers.Add(stackColLayer);

                // Data 설정
                for (int i = 1; i < dtChart.Columns.Count - 1; i++)
                {
                    Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesCol = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                    seriesCol.Label = dtChart.Columns[i].ColumnName;
                    seriesCol.Data.DataSource = dtChart;
                    seriesCol.Data.LabelColumn = "Month";
                    seriesCol.Data.ValueColumn = dtChart.Columns[i].ColumnName;
                    seriesCol.DataBind();
                    
                    this.uChart.CompositeChart.Series.Add(seriesCol);
                    stackColLayer.SwapRowsAndColumns = true;
                    stackColLayer.Series.Add(seriesCol);
                }

                #endregion

                #region Line Chart

                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisLine = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisLine.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X2_Axis;
                xAxisLine.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisLine.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisLine.MajorGridLines.Visible = false;
                xAxisLine.MinorGridLines.Visible = false;
                xAxisLine.Margin.Far.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                xAxisLine.Margin.Far.Value = 4;
                xAxisLine.Margin.Near.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                xAxisLine.Margin.Near.Value = 4;
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Visible = false;

                chartArea.Axes.Add(xAxisLine);

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance lineLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                lineLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                lineLayer.ChartArea = chartArea;
                lineLayer.AxisX = xAxisLine;
                lineLayer.AxisY = yAxis;
                this.uChart.CompositeChart.ChartLayers.Add(lineLayer);

                // Data 설정
                Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesLine = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                seriesLine.Label = "Total";
                seriesLine.Data.DataSource = dtChart;
                seriesLine.Data.LabelColumn = "Month";
                seriesLine.Data.ValueColumn = "Total";
                seriesLine.PEs.Add(new Infragistics.UltraChart.Resources.Appearance.PaintElement(Color.Salmon));
                this.uChart.CompositeChart.Series.Add(seriesLine);
                lineLayer.Series.Add(seriesLine);

                Infragistics.UltraChart.Resources.Appearance.LineChartAppearance appLineChart = new Infragistics.UltraChart.Resources.Appearance.LineChartAppearance();

                // 선모양 설정
                Infragistics.UltraChart.Resources.Appearance.LineAppearance appLine = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                appLine.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                appLine.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Auto;
                appLine.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                appLine.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;
                appLine.LineStyle.StartStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;

                // Text 설정
                Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[] appChartText = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[12];
                for (int i = 0; i < appChartText.Length; i++)
                {
                    appChartText[i] = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance();
                    appChartText[i].HorizontalAlign = StringAlignment.Center;
                    appChartText[i].VerticalAlign = StringAlignment.Far;
                    appChartText[i].FontColor = Color.Black;
                    appChartText[i].ItemFormatString = "<DATA_VALUE:#,###>";
                    appChartText[i].Visible = true;
                    appChartText[i].Row = 0;
                    appChartText[i].Column = i;
                    Font font = new Font(m_resSys.GetString("SYS_FONTNAME"), 8, FontStyle.Bold);
                    appChartText[i].ChartTextFont = font;
                    appLineChart.ChartText.Add(appChartText[i]);
                }
                appLineChart.LineAppearances.Add(appLine);
                lineLayer.ChartTypeAppearance = appLineChart;

                #endregion

                #region 범주 설정

                // Legend 설정
                Infragistics.UltraChart.Resources.Appearance.CompositeLegend Legend = new Infragistics.UltraChart.Resources.Appearance.CompositeLegend();
                Legend.ChartLayers.Add(stackColLayer);
                Legend.ChartLayers.Add(lineLayer);
                Legend.Bounds = new Rectangle(1, 5, 10, 90);
                Legend.BoundsMeasureType = Infragistics.UltraChart.Shared.Styles.MeasureType.Percentage;
                Legend.PE.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.Gradient;
                Legend.PE.FillGradientStyle = Infragistics.UltraChart.Shared.Styles.GradientStyle.ForwardDiagonal;
                Legend.PE.Fill = Color.CornflowerBlue;
                Legend.PE.FillStopColor = Color.Transparent;
                Legend.Border.CornerRadius = 10;
                Legend.Border.Thickness = 0;
                this.uChart.CompositeChart.Legends.Add(Legend);

                #endregion

                // Tooltip 설정
                this.uChart.Tooltips.Display = Infragistics.UltraChart.Shared.Styles.TooltipDisplay.MouseMove;
                this.uChart.Tooltips.Format = Infragistics.UltraChart.Shared.Styles.TooltipStyle.LabelPlusDataValue;

                #endregion

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 조회연도 숫자만 입력가능하게 하는 이벤트
        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 귀책부서 그리드 더블클릭시 고객사별 탭으로 이동하는 이벤트
        private void uGridImputeDeptList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                this.uTabInfo.Tabs[1].Selected = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }
    }
}
