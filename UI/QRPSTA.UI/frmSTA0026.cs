﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 통계분석                                              */
/* 모듈(분류)명 : 수입검사 통계분석                                     */
/* 프로그램ID   : frmSTA0026.cs                                         */
/* 프로그램명   : C(결점수) 관리도 분석                                 */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0026 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0026()
        {
            InitializeComponent();
        }

        private void Form7_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            //컨트롤초기화
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀설정
                titleArea.mfSetLabelText("C(결점수) 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch
            { }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel1, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel2, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel3, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabel4, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel5, "규격상한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel6, "규격하한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "규격범위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel8, "평균", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel9, "시료수", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch
            { }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGridListInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridListInfo, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridListInfo, 0, "측정일", "측정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridListInfo, 0, "측정시간", "측정시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridListInfo, 0, "결점수", "결점수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 15
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridListInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridListInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch
            { }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBoxListInfo, GroupBoxType.LIST, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBoxCControl, GroupBoxType.CHART, "C 관리도", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxListInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxListInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxCControl.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxCControl.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch
            { }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 초기화
                WinComboEditor combo = new WinComboEditor();

                // Call BL
                // Plant
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(plant);

                DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                combo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택", "PlantCode", "PlantName"
                    , dtPlant);
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        #region 툴바
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion
    }
}
