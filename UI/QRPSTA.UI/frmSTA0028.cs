﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0028.cs                                         */
/* 프로그램명   : 통계값분석                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-10-28 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;    //COM+ 서비스 사용하기위함
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0028 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0028()
        {
            InitializeComponent();
        }

        private void frmSTA0028_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0028_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            // 컨트롤 초기화
            InitEtc();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch(Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        private void InitEtc()
        {
            try
            {
                // SystemInfo Resource 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 타이틀 설정함수 호출
                this.titleArea.mfSetLabelText("통계값분석", m_resSys.GetString("SYS_FONTNAME"), 12);

                this.uTextSearchCustomerCode.MaxLength = 20;
                this.uTextSearchProductCode.MaxLength = 20;

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "분석일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE")
                    , "", "전체", "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region HeaderGrid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcessStaticList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "CustomerCode", "고객사코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "ADate", "분석일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "StackSeq", "StackSeq", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "Generation", "Generation", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 40
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "MeanDA", "평균", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "StdDevDA", "표준편차", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "MinDA", "최소값", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "MaxDA", "최대값", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "CpDA", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticList, 0, "CpkDA", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridProcessStaticList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcessStaticList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region DetailGrid
                // 상세그리드
                wGrid.mfInitGeneralGrid(this.uGridProcessStaticDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Default, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "CustomerCode", "고객사코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "InspectItemCode", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "LotSize", "LotSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "UpperSpec", "UpperSpec", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "LowerSpec", "LowerSpec", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "Mean", "Mean", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "StdDev", "StdDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "MaxValue", "MaxValue", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "MinValue", "MinValue", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "DataRange", "DataRange", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcessStaticDetail, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strInspectItemCode = this.uComboSearchInspectItem.Value.ToString();
                string strProductCode = this.uTextSearchProductCode.Text;
                string strCustomerCode = this.uTextSearchCustomerCode.Text;
                string strFromADate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).ToString("yyyy-MM-dd");
                string strToADate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value).ToString("yyyy-MM-dd");

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStaticD = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStaticD);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtStatic = clsStaticD.mfReadINSProcessStaticD(strPlantCode, strProductCode, strInspectItemCode, strCustomerCode, strFromADate, strToADate, m_resSys.GetString("SYS_LANG"));

                this.uGridProcessStaticList.DataSource = dtStatic;
                this.uGridProcessStaticList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = false;

                // 조회결과가 없을시 메세지창
                if (!(dtStatic.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 검색조건 이벤트
        // 검색조건 공장콤보박스 값에 따라 검사항목 콤보박스 설정하는 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor combo = new WinComboEditor();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtItem = new DataTable();

                this.uComboSearchInspectItem.Items.Clear();
                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsITem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsITem);

                    dtItem = clsITem.mfReadMASInspectItemCombo(strPlantCode, "", "", m_resSys.GetString("SYS_LANG"));
                }

                combo.mfSetComboEditor(this.uComboSearchInspectItem, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                                        , "", "", "선택", "InspectItemCode", "InspectItemName", dtItem);

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 팝업창 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = this.uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
                this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 키다운 이벤트
        private void uTextSearchProductCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // 공백이 아닐시
                    if (!this.uTextSearchProductCode.Text.Equals(string.Empty))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        string strPlantCode = this.uComboSearchPlant.Value.ToString();

                        if (strPlantCode.Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "확인창", "공장 입력확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                            return;
                        }
                        else
                        {
                            string strProductCode = this.uTextSearchProductCode.Text;

                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                            brwChannel.mfCredentials(clsProduct);

                            DataTable dtProduct = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                            if (dtProduct.Rows.Count > 0)
                            {
                                this.uTextSearchProductCode.Text = dtProduct.Rows[0]["ProductCode"].ToString();
                                this.uTextSearchProductName.Text = dtProduct.Rows[0]["ProductName"].ToString();
                                this.uTextSearchCustomerCode.Text = dtProduct.Rows[0]["CustomerCode"].ToString();
                                this.uTextSearchCustomerName.Text = dtProduct.Rows[0]["CustomerName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                        , "확인창", "제품정보 조회결과", "제품정보를 찾을 수 없습니다.", Infragistics.Win.HAlign.Right);

                                this.uTextSearchProductName.Clear();
                                this.uTextSearchCustomerCode.Clear();
                                this.uTextSearchCustomerName.Clear();
                            }
                        }
                    }
                    else if (e.KeyCode.Equals(Keys.Back) || e.KeyCode.Equals(Keys.Delete))
                    {
                        if (this.uTextSearchProductCode.TextLength <= 1 || this.uTextSearchProductCode.SelectedText == this.uTextSearchProductCode.Text)
                        {
                            this.uTextSearchProductName.Clear();
                            this.uTextSearchCustomerCode.Clear();
                            this.uTextSearchCustomerName.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 검색조건 고객코드 팝업창 이벤트
        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;

                    this.uGridProcessStaticList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;

                    for (int i = 0; i < this.uGridProcessStaticList.Rows.Count; i++)
                    {
                        this.uGridProcessStaticList.Rows[i].Fixed = false;
                    }

                    this.uGridProcessStaticList.Height = 725;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시 상세정보 조회 이벤트
        private void uGridProcessStaticList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 행고정
                e.Row.Fixed = true;

                // 검색조건 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strProductCode = e.Row.Cells["ProductCode"].Value.ToString();
                string strProcessCode = e.Row.Cells["ProcessCode"].Value.ToString();
                string strADate = e.Row.Cells["ADate"].Value.ToString();
                string strInspectItemCode = e.Row.Cells["InspectItemCode"].Value.ToString();
                string strStackSeq = e.Row.Cells["StackSeq"].Value.ToString();
                string strGeneration = e.Row.Cells["Generation"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticD), "ProcessStaticD");
                QRPSTA.BL.STAPRC.ProcessStaticD clsStaticD = new QRPSTA.BL.STAPRC.ProcessStaticD();
                brwChannel.mfCredentials(clsStaticD);

                // ProgressBar PopUP Show
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtDetail = clsStaticD.mfReadINSProcessStaticD_Detail(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode
                                                                                , strADate, strProcessCode, m_resSys.GetString("SYS_LANG"));

                this.uGridProcessStaticDetail.DataSource = dtDetail;
                this.uGridProcessStaticDetail.DataBind();

                // ProgressBar PopUp Close 
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void frmSTA0028_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}
