﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 통계분석                                              */
/* 모듈(분류)명 : 수입검사 통계분석                                     */
/* 프로그램ID   : frmSTA0023.cs                                         */
/* 프로그램명   : X-bar / R 관리도분석                                  */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0023 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0023()
        {
            InitializeComponent();
        }

        private void frmSTA0023_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0023_Load(object sender, EventArgs e)
        {

            // SystemInfo Resource 변수
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("X-bar / R 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            this.Resize += new EventHandler(frmSTA0023_Resize);

            this.uDateSearchInspectFromDate.Focus();
            this.uDateSearchInspectFromDate.SelectAll();
        }

        void frmSTA0023_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    this.uGroupBox2.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBox1.Width;
                    this.uGroupBox3.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth - this.uGroupBox1.Width;
                }
                else
                {
                    this.uGroupBox2.Width = 616;
                    this.uGroupBox3.Width = 616;
                    this.uGroupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    this.uGroupBox3.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //Form Closing시 그리드 위치 저장
        private void frmSTA0023_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 기본값 설정
                this.uDateSearchInspectFromDate.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);
                //this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();

                setTabIndex(this.Controls);

                this.uDateSearchInspectFromDate.TabIndex = 1;
                this.uDateSearchInspectFromDate.TabStop = true;
                this.uDateSearchInspectToDate.TabIndex = 2;
                this.uDateSearchInspectToDate.TabStop = true;
                this.uComboSearchMaterial.TabIndex = 3;
                this.uComboSearchMaterial.TabStop = true;
                this.uComboSearchInspectItem.TabIndex = 4;
                this.uComboSearchInspectItem.TabStop = true;
                this.uComboSearchVendor.TabIndex = 5;
                this.uComboSearchVendor.TabStop = true;
                this.uComboSearchRev.TabIndex = 6;
                this.uComboSearchRev.TabStop = true;
                this.uComboSearchMaterialGrade.TabIndex = 7;
                this.uComboSearchMaterialGrade.TabStop = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchRev, "Rev", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialGrade, "자재등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSpecLower, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelXBar, "XBar관리도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelR, "R관리도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLCL, "LCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCL, "CL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUCL, "UCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStdDeviation, "표준편차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMax, "Max", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMin, "Min", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelOverUCL, "OverUCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderLCL, "UnderLCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOOC, "OOC", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverUSL, "OverUSL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderLSL, "UnderLSL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOOS, "OOS", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverRun, "OverRun", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderRun, "UnderRun", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRun, "RUN", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOverTrend, "OverTrend", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnderTrend, "UnderTrend", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTrend, "TREND", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCp, "Cp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpk, "Cpk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpl, "Cpl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpu, "Cpu", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPp, "Pp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpk, "Ppk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpl, "Ppl", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPpu, "Ppu", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.CHART, "X-bar 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.CHART, "R 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboGrid wCombo = new WinComboGrid();
                WinComboEditor wComboe = new WinComboEditor();

                // 검사항목 콤보박스
                wCombo.mfInitGeneralComboGrid(this.uComboSearchInspectItem, true, false, true, true, "InspectItemCode", "InspectItemName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchInspectItem.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "ItemNum", "항목순번", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemCode", "검사항목코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "InspectItemName", "검사항목명", false, 200, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "UpperSpec", "규격상한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "LowerSpec", "규격하한", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeCode", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SpecRangeName", "규격범위", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wCombo.mfSetComboGridColumn(this.uComboSearchInspectItem, 0, "SampleSize", "SampleSize", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                QRPBrowser brwChannel = new QRPBrowser();

                // 거래처 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);
                DataTable dtVendor = clsVendor.mfReadVendorPopup(m_resSys.GetString("SYS_LANG"));
                dtVendor.Columns.Add("VendorCodeName", typeof(string));
                for (int i = 0; i < dtVendor.Rows.Count; i++)
                {
                    dtVendor.Rows[i]["VendorCodename"] = dtVendor.Rows[i]["VendorCode"].ToString() + "  |  " + dtVendor.Rows[i]["VendorName"].ToString();
                }
                wComboe.mfSetComboEditor(this.uComboSearchVendor, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ""
                    , "VendorCode", "VendorCodename", dtVendor);

                // 공장 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", ""
                    , "PlantCode", "PlantName", dtPlant);

                // 자재등급 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);
                DataTable dtCom = clsCom.mfReadCommonCode("C0013", m_resSys.GetString("SYS_LANG"));
                wComboe.mfSetComboEditor(this.uComboSearchMaterialGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "ComCode", "ComCodeName", dtCom);

                this.uComboSearchMaterial.AfterEnterEditMode += new EventHandler(uComboSearchMaterial_AfterEnterEditMode);
                this.uComboSearchVendor.AfterEnterEditMode += new EventHandler(uComboSearchVendor_AfterEnterEditMode);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectTime", "시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DataRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdDev", "표준편차", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MinValue", "최소값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MaxValue", "최대값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

#endregion

        #region 툴바
        public void mfSearch()
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            DialogResult Result = new DialogResult();
            WinMessageBox msg = new WinMessageBox();

            #region 검색조건 확인

            if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedItem == null)
            {
                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000216", "M000266", Infragistics.Win.HAlign.Center);
                this.uComboSearchPlant.DropDown();
                return;
            }
            else if (Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchInspectToDate.Value)) > 0)
            {
                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000216", "M000052", Infragistics.Win.HAlign.Center);
                return;
            }
            else if (this.uComboSearchMaterial.SelectedItem == null || this.uComboSearchMaterial.Value.ToString().Equals(string.Empty))
            {
                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000216", "M000965", Infragistics.Win.HAlign.Center);
                this.uComboSearchMaterial.DropDown();
                return;
            }
            ////else if (this.uComboSearchRev.SelectedItem == null || this.uComboSearchRev.Value.ToString().Equals(string.Empty))
            ////{
            ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "M001264", "M000216", "M000965", Infragistics.Win.HAlign.Center);
            ////    this.uComboSearchRev.DropDown();
            ////    return;
            ////}
            else if (this.uComboSearchInspectItem.Value == null || this.uComboSearchInspectItem.Value == DBNull.Value || this.uComboSearchInspectItem.Value.ToString().Equals(string.Empty) ||
                this.uComboSearchInspectItem.SelectedRow == null)
            {
                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000216", "M000193", Infragistics.Win.HAlign.Center);

                this.uComboSearchInspectItem.Focus();
                this.uComboSearchInspectItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                return;
            }
            ////else if (this.uComboSearchVendor.Value.ToString().Equals(string.Empty) || this.uComboSearchVendor.SelectedItem == null)
            ////{
            ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "M001264", "M000216", "M001274", Infragistics.Win.HAlign.Center);
            ////    this.uComboSearchVendor.DropDown();
            ////    return;
            ////}
            ////else if (this.uComboSearchMaterialGrade.SelectedItem == null || this.uComboSearchMaterialGrade.Value.ToString().Equals(string.Empty))
            ////{
            ////    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                                , "M001264", "M000216", "M001413", Infragistics.Win.HAlign.Center);

            ////    this.uComboSearchMaterialGrade.DropDown();
            ////    return;
            ////}

            #endregion

            else
            {
                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 검색조건 설정

                // 이전 분기데이터 조회 하기 위한 날짜 설정
                DateTime dateInspectFromDate = Convert.ToDateTime(this.uDateSearchInspectFromDate.Value).AddDays(-1);
                DateTime dateInspectToDate = Convert.ToDateTime(this.uDateSearchInspectToDate.Value);
                DateTime datePreqFromDate = new DateTime();
                DateTime datePreqToDate = new DateTime();
                if (dateInspectToDate.Month.Equals(1) || dateInspectToDate.Month.Equals(2) || dateInspectToDate.Month.Equals(3))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 9, 30);
                    datePreqToDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 12, 31);
                }
                else if (dateInspectToDate.Month.Equals(4) || dateInspectToDate.Month.Equals(5) || dateInspectToDate.Month.Equals(6))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.AddYears(-1).Year, 12, 31);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 3, 31);
                }
                else if (dateInspectToDate.Month.Equals(7) || dateInspectToDate.Month.Equals(8) || dateInspectToDate.Month.Equals(9))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.Year, 3, 31);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 6, 30);
                }
                else if (dateInspectToDate.Month.Equals(10) || dateInspectToDate.Month.Equals(11) || dateInspectToDate.Month.Equals(12))
                {
                    datePreqFromDate = new DateTime(dateInspectToDate.Year, 6, 30);
                    datePreqToDate = new DateTime(dateInspectToDate.Year, 9, 30);
                }

                // 이전 3개월 데이터 조회하기 위한 날짜 설정
                DateTime dateFromDate = new DateTime(Convert.ToDateTime(this.uDateSearchInspectToDate.Value).AddMonths(-3).Year, Convert.ToDateTime(this.uDateSearchInspectToDate.Value).AddMonths(-3).Month, 1).AddDays(-1);   // 저번달로 부터 3개월전 첫일
                DateTime dateToDate = new DateTime(Convert.ToDateTime(this.uDateSearchInspectToDate.Value).Year, Convert.ToDateTime(this.uDateSearchInspectToDate.Value).Month, 1).AddDays(-1);                                 // 저번달 마지막일

                // 날짜를 제외한 검색조건 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uComboSearchMaterial.Value.ToString();
                string strRev = this.uComboSearchRev.Value.ToString();
                string strInspectItemCode = this.uComboSearchInspectItem.SelectedRow.GetCellValue("InspectItemCode").ToString();
                string strVendorCode = this.uComboSearchVendor.Value.ToString();
                string strMaterialGrade = this.uComboSearchMaterialGrade.Value.ToString();

                #endregion

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsStaticD = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsStaticD);

                // 바인딩될 데이터는 검색조건에 검사일로 조회
                DataTable dtBindData = clsStaticD.mfReadINSMatStatic_XBar_Xn(strPlantCode
                                                                        , strMaterialCode
                                                                        , strRev
                                                                        , strInspectItemCode
                                                                        , strVendorCode
                                                                        , strMaterialGrade
                                                                        , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                        , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                        , m_resSys.GetString("SYS_LANG"));

                // 데이터 바인딩
                this.uGrid1.SetDataBinding(dtBindData, string.Empty);

                if (dtBindData.Rows.Count > 0)
                {
                    // Data조회
                    // 이전 3개월 데이터
                    DataTable dtData = clsStaticD.mfReadINSMatStatic_XBar_Xn(strPlantCode
                                                                            , strMaterialCode
                                                                            , strRev
                                                                            , strInspectItemCode
                                                                            , strVendorCode
                                                                            , strMaterialGrade
                                                                            , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                            , dateToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                            , m_resSys.GetString("SYS_LANG"));

                    // 이전 분기 데이터
                    DataTable dtPreqData = clsStaticD.mfReadINSMatStatic_XBar_Xn(strPlantCode
                                                                            , strMaterialCode
                                                                            , strRev
                                                                            , strInspectItemCode
                                                                            , strVendorCode
                                                                            , strMaterialGrade
                                                                            , datePreqFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                            , datePreqToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                            , m_resSys.GetString("SYS_LANG"));

                    if (dtPreqData.Rows.Count.Equals(0) && dtData.Rows.Count.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001118"
                            , "M001461", Infragistics.Win.HAlign.Center);

                        ////// POPUP창 Close
                        ////this.MdiParent.Cursor = Cursors.Default;
                        ////m_ProgressPopup.mfCloseProgressPopup(this);

                        ////Clear();

                        ////return;
                    }

                    QRPSTA.STASummary structXBar = new STASummary();
                    QRPSTA.STASummary structR = new STASummary();
                    QRPSTA.STASPC clsSTAPRC = new STASPC();
                    STAControlLimit XBarCL = new STAControlLimit();
                    DataTable dtXValue = new DataTable();

                    // 변수 설정
                    double dblLowerSpec = ReturnDoubleValue(this.uTextLowerSpec.Text);
                    double dblUpperSpec = ReturnDoubleValue(this.uTextUpperSpec.Text);
                    int intSampleSize;
                    if (uTextSampleSize.Text == "" || uTextSampleSize.Text == "0")
                        intSampleSize = 5;
                    else
                        intSampleSize = Convert.ToInt32(Convert.ToDouble(this.uTextSampleSize.Text));

                    if (dtPreqData.Rows.Count > 0)
                    {
                        //이전분기 데이터가 있는 경우 이전분기 데이터로 관리한계선을 구한다.
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithR(dtPreqData.DefaultView.ToTable(false, "Mean"), dtPreqData.DefaultView.ToTable(false, "DataRange"), intSampleSize);
                    }
                    else
                    {
                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithR(dtData.DefaultView.ToTable(false, "Mean"), dtData.DefaultView.ToTable(false, "DataRange"), intSampleSize);
                    }

                    structXBar = clsSTAPRC.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartXBar, dtBindData.DefaultView.ToTable(false, "Mean"), dtBindData.DefaultView.ToTable(false, "DataRange")
                                                    , dblLowerSpec, dblUpperSpec, this.uTextSpecRangeCode.Text, true, XBarCL.XBRLCL, XBarCL.XBCL, XBarCL.XBRUCL, intSampleSize, true, true, "", ""
                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    structR = clsSTAPRC.mfDrawRChart4SubGroupMean(this.uChartR, dtBindData.DefaultView.ToTable(false, "DataRange"), true, XBarCL.RLCL, XBarCL.RCL, XBarCL.RUCL
                                                        , intSampleSize, "", "", "", m_resSys.GetString("SYS_FONTNAME")
                                                        , 50, 10, 0);

                    // 통계값 계산을 위한 조회
                    dtXValue = clsStaticD.mfReadINSMatStatic_XMR(strPlantCode
                                                                , strMaterialCode
                                                                , strRev
                                                                , strInspectItemCode
                                                                , strVendorCode
                                                                , strMaterialGrade
                                                                , dateInspectFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                , dateInspectToDate.ToString("yyyy-MM-dd 21:59:59")
                                                                , m_resSys.GetString("SYS_LANG"));

                    // 통계값 계산
                    QRPSTA.STABAS clsSTABas = new STABAS();
                    STABasic clsBasic = clsSTABas.mfCalcBasicStat(dtXValue.DefaultView.ToTable(false, "InspectValue"), dblLowerSpec, dblUpperSpec, uTextSpecRangeCode.Text);


                    this.uTextCp.Text = string.Format("{0:f5}", structXBar.CpDA);
                    this.uTextCpk.Text = string.Format("{0:f5}", structXBar.CpkDA);
                    this.uTextCpl.Text = string.Format("{0:f5}", structXBar.CplDA);
                    this.uTextCpu.Text = string.Format("{0:f5}", structXBar.CpuDA);
                    this.uTextPp.Text = string.Format("{0:f5}", clsBasic.Cp);
                    this.uTextPpk.Text = string.Format("{0:f5}", clsBasic.Cpk);
                    this.uTextPpl.Text = string.Format("{0:f5}", clsBasic.Cpl);
                    this.uTextPpu.Text = string.Format("{0:f5}", clsBasic.Cpu);

                    this.uTextXBarCL.Text = string.Format("{0:f5}", XBarCL.XBCL);
                    this.uTextXBarLCL.Text = string.Format("{0:f5}", XBarCL.XBRLCL);
                    this.uTextXBarUCL.Text = string.Format("{0:f5}", XBarCL.XBRUCL);
                    this.uTextRCL.Text = string.Format("{0:f5}", XBarCL.RCL);
                    this.uTextRLCL.Text = string.Format("{0:f5}", XBarCL.RLCL);
                    this.uTextRUCL.Text = string.Format("{0:f5}", XBarCL.RUCL);

                    this.uTextStdDev.Text = string.Format("{0:f5}", structXBar.StdDevDA);
                    this.uTextMin.Text = string.Format("{0:f5}", clsBasic.Min);
                    this.uTextMax.Text = string.Format("{0:f5}", clsBasic.Max);

                    this.uTextOverUCL.Text = structXBar.UCLUpperCount.ToString();
                    this.uTextOverUCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UCLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLCL.Text = structXBar.LCLLowerCount.ToString();
                    this.uTextUnderLCLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LCLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextOOC.Text = structXBar.CLOverCount.ToString();

                    this.uTextOverUSL.Text = structXBar.USLUpperCount.ToString();
                    this.uTextOverUSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.USLUpperCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderLSL.Text = structXBar.LSLLowerCount.ToString();
                    this.uTextUnderLSLP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.LSLLowerCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextOOS.Text = structXBar.SLOverCount.ToString();

                    this.uTextOverRun.Text = structXBar.UpwardRunCount.ToString();
                    this.uTextOverRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderRun.Text = structXBar.DownwardRunCount.ToString();
                    this.uTextUnderRunP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardRunCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextRun.Text = (structXBar.DownwardRunCount + structXBar.UpwardRunCount).ToString();

                    this.uTextOverTrend.Text = structXBar.UpwardTrendCount.ToString();
                    this.uTextOverTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.UpwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextUnderTrend.Text = structXBar.DownwardTrendCount.ToString();
                    this.uTextUnderTrendP.Text = string.Format("{0:f3}", (Convert.ToDecimal(structXBar.DownwardTrendCount) / Convert.ToDecimal(structXBar.CountDA) * 100)) + "%";
                    this.uTextTrend.Text = (structXBar.UpwardTrendCount + structXBar.DownwardTrendCount).ToString();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001099", "M001104"
                            , "M000171", Infragistics.Win.HAlign.Center);

                    Clear();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGrid1.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Methods...

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        //검사항목 콤보박스 설정
        private void InitInspectItemCombo()
        {
            try
            {
                if (this.uComboSearchPlant.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchMaterial.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchRev.SelectedIndex.Equals(-1))
                    return;
                else if (this.uComboSearchVendor.SelectedIndex.Equals(-1))
                    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uComboSearchMaterial.Value.ToString();
                string strRev = this.uComboSearchRev.Value.ToString();
                string strVendorCode = this.uComboSearchVendor.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsstd = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsstd);

                DataTable dtInspectItem = clsstd.mfReadINSMatStatic_InspectItemCombo(strPlantCode, strMaterialCode, strVendorCode, strRev, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchInspectItem.SetDataBinding(dtInspectItem, string.Empty);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                //this.uTextUpperSpec.Clear();
                //this.uTextLowerSpec.Clear();
                //this.uTextSpecRange.Clear();
                this.uTextStdDev.Clear();
                this.uTextMax.Clear();
                this.uTextMin.Clear();
                this.uTextXBarLCL.Clear();
                this.uTextXBarCL.Clear();
                this.uTextXBarUCL.Clear();
                this.uTextRLCL.Clear();
                this.uTextRCL.Clear();
                this.uTextRUCL.Clear();

                this.uTextOverUCL.Clear();
                this.uTextOverUCLP.Clear();
                this.uTextUnderLCLP.Clear();
                this.uTextUnderLCL.Clear();
                this.uTextCp.Clear();
                this.uTextOOC.Clear();
                this.uTextOverUSLP.Clear();
                this.uTextOverUSL.Clear();
                this.uTextUnderLSLP.Clear();
                this.uTextUnderLSL.Clear();
                this.uTextCpk.Clear();
                this.uTextOOS.Clear();
                this.uTextOverRunP.Clear();
                this.uTextOverRun.Clear();
                this.uTextUnderRunP.Clear();
                this.uTextUnderRun.Clear();
                this.uTextCpu.Clear();
                this.uTextRun.Clear();
                this.uTextOverTrendP.Clear();
                this.uTextOverTrend.Clear();
                this.uTextUnderTrendP.Clear();
                this.uTextUnderTrend.Clear();
                this.uTextCpl.Clear();
                this.uTextTrend.Clear();

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                ClearChart(this.uChartXBar);
                ClearChart(this.uChartR);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 차트 초기화 메소드
        private void ClearChart(Infragistics.Win.UltraWinChart.UltraChart uChart)
        {
            try
            {
                QRPSTA.STASPC clsSTASPC = new STASPC();
                clsSTASPC.mfInitControlChart(uChart);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Double 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">Double로 반환받을 값</param>
        /// <returns></returns>
        private double ReturnDoubleValue(string value)
        {
            double result = 0.0;

            if (double.TryParse(value, out result))
                return result;
            else
                return 0.0;
        }

        // Tabindex 설정 함수
        private void setTabIndex(Control.ControlCollection ctls)
        {
            try
            {
                foreach (Control ctl in ctls)
                {
                    ctl.TabIndex = 0;
                    ctl.TabStop = false;
                    if (ctl.HasChildren)
                        setTabIndex(ctl.Controls);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events...

        // 공장콤보 변경 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ////if (this.uComboSearchPlant.Value == null ||
                ////    this.uComboSearchPlant.Value == DBNull.Value ||
                ////    this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                ////    return;

                // 초기화 메소드 호출
                Clear();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMat);

                DataTable dtMat = clsMat.mfReadMASMaterial_MaterialCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchMaterial.Items.Clear();

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchMaterial, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "MaterialCode", "MaterialName", dtMat);

                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        // 자재콤보 변경시 이벤트
        private void uComboSearchMaterial_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ////if (this.uComboSearchMaterial.Value == null ||
                ////    this.uComboSearchMaterial.Value == DBNull.Value ||
                ////    this.uComboSearchMaterial.Value.ToString().Equals(string.Empty))
                ////    return;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialCode = this.uComboSearchMaterial.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAIMP.ImportStaticD), "ImportStaticD");
                QRPSTA.BL.STAIMP.ImportStaticD clsMatstd = new QRPSTA.BL.STAIMP.ImportStaticD();
                brwChannel.mfCredentials(clsMatstd);

                DataTable dtRev = clsMatstd.mfReadSTAIMP_Rev_Combo(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                this.uComboSearchRev.Items.Clear();

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchRev, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "RevCode", "RevName", dtRev);


                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchRev_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                InitInspectItemCombo();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchInspectItem_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (e.Row == null)
                    return;

                // 규격상/하한, 규격범위 텍스트 박스 설정
                this.uTextUpperSpec.Text = string.Format("{0:f5}", e.Row.Cells["UpperSpec"].Value);
                this.uTextLowerSpec.Text = string.Format("{0:f5}", e.Row.Cells["LowerSpec"].Value);
                this.uTextSpecRangeCode.Text = e.Row.Cells["SpecRangeCode"].Value.ToString();
                this.uTextSpecRange.Text = e.Row.Cells["SpecRangeName"].Value.ToString();
                this.uTextSampleSize.Text = e.Row.Cells["SampleSize"].Value.ToString();

                // 한쪽 규격만 있는경우 규격없는것은 공백처리 -2012.04.23
                if (this.uTextSpecRangeCode.Text.Equals("U"))
                    this.uTextUpperSpec.Text = string.Empty;
                else if (this.uTextSpecRangeCode.Text.Equals("L"))
                    this.uTextLowerSpec.Text = string.Empty;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        void uComboSearchVendor_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchVendor.SelectAll();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        void uComboSearchMaterial_AfterEnterEditMode(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchMaterial.SelectAll();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frm = new QRPCOM.frmErrorInfo(ex);
                frm.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion
    }
}
