﻿namespace QRPSTA.UI
{
    partial class frmSTA0025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0025));
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uTextTOTFaultQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTFaultQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTOTQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTFaultRate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTOTFaultRate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTOTQty = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxChart = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChartPchart = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uLabelCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBoxInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLCL = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxGridList = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchVendor = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchRev = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMaterialGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchMaterial = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchRev = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChart)).BeginInit();
            this.uGroupBoxChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartPchart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).BeginInit();
            this.uGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGridList)).BeginInit();
            this.uGroupBoxGridList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 244;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uTextTOTFaultQty
            // 
            this.uTextTOTFaultQty.Location = new System.Drawing.Point(116, 56);
            this.uTextTOTFaultQty.Name = "uTextTOTFaultQty";
            this.uTextTOTFaultQty.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTFaultQty.TabIndex = 3;
            this.uTextTOTFaultQty.Text = "ultraTextEditor2";
            // 
            // uLabelTOTFaultQty
            // 
            this.uLabelTOTFaultQty.Location = new System.Drawing.Point(12, 56);
            this.uLabelTOTFaultQty.Name = "uLabelTOTFaultQty";
            this.uLabelTOTFaultQty.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTFaultQty.TabIndex = 2;
            this.uLabelTOTFaultQty.Text = "총불량수";
            // 
            // uTextTOTQty
            // 
            this.uTextTOTQty.Location = new System.Drawing.Point(116, 28);
            this.uTextTOTQty.Name = "uTextTOTQty";
            this.uTextTOTQty.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTQty.TabIndex = 1;
            this.uTextTOTQty.Text = "ultraTextEditor1";
            // 
            // uLabelTOTFaultRate
            // 
            this.uLabelTOTFaultRate.Location = new System.Drawing.Point(12, 84);
            this.uLabelTOTFaultRate.Name = "uLabelTOTFaultRate";
            this.uLabelTOTFaultRate.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTFaultRate.TabIndex = 4;
            this.uLabelTOTFaultRate.Text = "총불량률(%)";
            // 
            // uTextTOTFaultRate
            // 
            this.uTextTOTFaultRate.Location = new System.Drawing.Point(116, 84);
            this.uTextTOTFaultRate.Name = "uTextTOTFaultRate";
            this.uTextTOTFaultRate.Size = new System.Drawing.Size(140, 21);
            this.uTextTOTFaultRate.TabIndex = 5;
            this.uTextTOTFaultRate.Text = "ultraTextEditor2";
            // 
            // uLabelTOTQty
            // 
            this.uLabelTOTQty.Location = new System.Drawing.Point(12, 28);
            this.uLabelTOTQty.Name = "uLabelTOTQty";
            this.uLabelTOTQty.Size = new System.Drawing.Size(100, 21);
            this.uLabelTOTQty.TabIndex = 0;
            this.uLabelTOTQty.Text = "총검사수";
            // 
            // uGroupBoxChart
            // 
            this.uGroupBoxChart.Controls.Add(this.uChartPchart);
            this.uGroupBoxChart.Location = new System.Drawing.Point(0, 510);
            this.uGroupBoxChart.Name = "uGroupBoxChart";
            this.uGroupBoxChart.Size = new System.Drawing.Size(1060, 330);
            this.uGroupBoxChart.TabIndex = 248;
            // 
            // uChartPchart
            // 
            this.uChartPchart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartPchart.Axis.PE = paintElement1;
            this.uChartPchart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartPchart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X.LineThickness = 1;
            this.uChartPchart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.X.Visible = true;
            this.uChartPchart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartPchart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartPchart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.X2.Labels.Visible = false;
            this.uChartPchart.Axis.X2.LineThickness = 1;
            this.uChartPchart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.X2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.X2.Visible = false;
            this.uChartPchart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartPchart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartPchart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y.LineThickness = 1;
            this.uChartPchart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Y.TickmarkInterval = 20;
            this.uChartPchart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Y.Visible = true;
            this.uChartPchart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartPchart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartPchart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Y2.Labels.Visible = false;
            this.uChartPchart.Axis.Y2.LineThickness = 1;
            this.uChartPchart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Y2.TickmarkInterval = 20;
            this.uChartPchart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Y2.Visible = false;
            this.uChartPchart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Z.Labels.ItemFormatString = "";
            this.uChartPchart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z.LineThickness = 1;
            this.uChartPchart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Z.Visible = false;
            this.uChartPchart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartPchart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartPchart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartPchart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartPchart.Axis.Z2.Labels.Visible = false;
            this.uChartPchart.Axis.Z2.LineThickness = 1;
            this.uChartPchart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartPchart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartPchart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartPchart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartPchart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartPchart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartPchart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartPchart.Axis.Z2.Visible = false;
            this.uChartPchart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartPchart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartPchart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartPchart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartPchart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartPchart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uChartPchart.Effects.Effects.Add(gradientEffect1);
            this.uChartPchart.Location = new System.Drawing.Point(3, 0);
            this.uChartPchart.Name = "uChartPchart";
            this.uChartPchart.Size = new System.Drawing.Size(1054, 327);
            this.uChartPchart.TabIndex = 0;
            this.uChartPchart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartPchart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uLabelCL
            // 
            this.uLabelCL.Location = new System.Drawing.Point(12, 140);
            this.uLabelCL.Name = "uLabelCL";
            this.uLabelCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelCL.TabIndex = 6;
            this.uLabelCL.Text = "CL";
            // 
            // uTextCL
            // 
            this.uTextCL.Location = new System.Drawing.Point(116, 140);
            this.uTextCL.Name = "uTextCL";
            this.uTextCL.Size = new System.Drawing.Size(140, 21);
            this.uTextCL.TabIndex = 7;
            this.uTextCL.Text = "ultraTextEditor2";
            // 
            // uGroupBoxInfo
            // 
            this.uGroupBoxInfo.Controls.Add(this.uTextUCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelUCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextLCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelLCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextCL);
            this.uGroupBoxInfo.Controls.Add(this.uLabelCL);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTFaultRate);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTFaultRate);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTFaultQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTFaultQty);
            this.uGroupBoxInfo.Controls.Add(this.uTextTOTQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelTOTQty);
            this.uGroupBoxInfo.Location = new System.Drawing.Point(800, 110);
            this.uGroupBoxInfo.Name = "uGroupBoxInfo";
            this.uGroupBoxInfo.Size = new System.Drawing.Size(260, 400);
            this.uGroupBoxInfo.TabIndex = 247;
            // 
            // uTextUCL
            // 
            this.uTextUCL.Location = new System.Drawing.Point(116, 168);
            this.uTextUCL.Name = "uTextUCL";
            this.uTextUCL.Size = new System.Drawing.Size(140, 21);
            this.uTextUCL.TabIndex = 11;
            this.uTextUCL.Text = "ultraTextEditor2";
            // 
            // uLabelUCL
            // 
            this.uLabelUCL.Location = new System.Drawing.Point(12, 168);
            this.uLabelUCL.Name = "uLabelUCL";
            this.uLabelUCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelUCL.TabIndex = 10;
            this.uLabelUCL.Text = "UCL";
            // 
            // uTextLCL
            // 
            this.uTextLCL.Location = new System.Drawing.Point(116, 112);
            this.uTextLCL.Name = "uTextLCL";
            this.uTextLCL.Size = new System.Drawing.Size(140, 21);
            this.uTextLCL.TabIndex = 9;
            this.uTextLCL.Text = "ultraTextEditor2";
            // 
            // uLabelLCL
            // 
            this.uLabelLCL.Location = new System.Drawing.Point(12, 112);
            this.uLabelLCL.Name = "uLabelLCL";
            this.uLabelLCL.Size = new System.Drawing.Size(100, 21);
            this.uLabelLCL.TabIndex = 8;
            this.uLabelLCL.Text = "LCL";
            // 
            // uGroupBoxGridList
            // 
            this.uGroupBoxGridList.Controls.Add(this.uGridList);
            this.uGroupBoxGridList.Location = new System.Drawing.Point(0, 110);
            this.uGroupBoxGridList.Name = "uGroupBoxGridList";
            this.uGroupBoxGridList.Size = new System.Drawing.Size(800, 400);
            this.uGroupBoxGridList.TabIndex = 246;
            // 
            // uGridList
            // 
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridList.DisplayLayout.Appearance = appearance7;
            this.uGridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridList.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridList.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridList.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridList.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance23.TextHAlignAsString = "Left";
            this.uGridList.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridList.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridList.Location = new System.Drawing.Point(3, 0);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(794, 397);
            this.uGridList.TabIndex = 0;
            this.uGridList.Text = "ultraGrid1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1028, -8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1052, -9);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchInspectDate.TabIndex = 12;
            this.uLabelSearchInspectDate.Text = "검색기간";
            // 
            // uDateSearchInspectFromDate
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.Appearance = appearance8;
            this.uDateSearchInspectFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectFromDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchInspectFromDate.Name = "uDateSearchInspectFromDate";
            this.uDateSearchInspectFromDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchInspectFromDate.TabIndex = 13;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchInspectToDate
            // 
            appearance9.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.Appearance = appearance9;
            this.uDateSearchInspectToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectToDate.Location = new System.Drawing.Point(232, 12);
            this.uDateSearchInspectToDate.Name = "uDateSearchInspectToDate";
            this.uDateSearchInspectToDate.Size = new System.Drawing.Size(96, 21);
            this.uDateSearchInspectToDate.TabIndex = 14;
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(344, 40);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchVendor.TabIndex = 30;
            this.uLabelSearchVendor.Text = "거래처";
            // 
            // uComboSearchVendor
            // 
            this.uComboSearchVendor.Location = new System.Drawing.Point(448, 40);
            this.uComboSearchVendor.Name = "uComboSearchVendor";
            this.uComboSearchVendor.Size = new System.Drawing.Size(356, 21);
            this.uComboSearchVendor.TabIndex = 15;
            this.uComboSearchVendor.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectItem
            // 
            this.uLabelSearchInspectItem.Location = new System.Drawing.Point(12, 40);
            this.uLabelSearchInspectItem.Name = "uLabelSearchInspectItem";
            this.uLabelSearchInspectItem.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchInspectItem.TabIndex = 283;
            this.uLabelSearchInspectItem.Text = "검사항목";
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(344, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchMaterial.TabIndex = 288;
            this.uLabelSearchMaterial.Text = "자재";
            // 
            // uLabelSearchRev
            // 
            this.uLabelSearchRev.Location = new System.Drawing.Point(812, 12);
            this.uLabelSearchRev.Name = "uLabelSearchRev";
            this.uLabelSearchRev.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchRev.TabIndex = 289;
            this.uLabelSearchRev.Text = "Rev";
            // 
            // uLabelSearchMaterialGrade
            // 
            this.uLabelSearchMaterialGrade.Location = new System.Drawing.Point(812, 40);
            this.uLabelSearchMaterialGrade.Name = "uLabelSearchMaterialGrade";
            this.uLabelSearchMaterialGrade.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchMaterialGrade.TabIndex = 292;
            this.uLabelSearchMaterialGrade.Text = "자재등급";
            // 
            // uComboSearchMaterialGrade
            // 
            this.uComboSearchMaterialGrade.Location = new System.Drawing.Point(916, 40);
            this.uComboSearchMaterialGrade.Name = "uComboSearchMaterialGrade";
            this.uComboSearchMaterialGrade.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchMaterialGrade.TabIndex = 291;
            this.uComboSearchMaterialGrade.Text = "ultraComboEditor1";
            // 
            // uComboSearchMaterial
            // 
            this.uComboSearchMaterial.Location = new System.Drawing.Point(448, 12);
            this.uComboSearchMaterial.Name = "uComboSearchMaterial";
            this.uComboSearchMaterial.Size = new System.Drawing.Size(356, 21);
            this.uComboSearchMaterial.TabIndex = 293;
            this.uComboSearchMaterial.Text = "ultraComboEditor1";
            // 
            // uComboSearchRev
            // 
            this.uComboSearchRev.Location = new System.Drawing.Point(916, 12);
            this.uComboSearchRev.Name = "uComboSearchRev";
            this.uComboSearchRev.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchRev.TabIndex = 294;
            this.uComboSearchRev.Text = "ultraComboEditor2";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchRev);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRev);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1060, 70);
            this.uGroupBoxSearchArea.TabIndex = 245;
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.CheckedListSettings.CheckStateMember = "";
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchInspectItem.DisplayLayout.Appearance = appearance15;
            this.uComboSearchInspectItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchInspectItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uComboSearchInspectItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchInspectItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellAppearance = appearance25;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowAppearance = appearance28;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchInspectItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.uComboSearchInspectItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchInspectItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchInspectItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(116, 40);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(220, 22);
            this.uComboSearchInspectItem.TabIndex = 295;
            this.uComboSearchInspectItem.Text = "ultraCombo1";
            // 
            // frmSTA0025
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxChart);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBoxInfo);
            this.Controls.Add(this.uGroupBoxGridList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0025";
            this.Text = "Form6";
            this.Load += new System.EventHandler(this.frmSTA0025_Load);
            this.Activated += new System.EventHandler(this.frmSTA0025_Activated);
            this.Resize += new System.EventHandler(this.frmSTA0025_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTOTFaultRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxChart)).EndInit();
            this.uGroupBoxChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChartPchart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).EndInit();
            this.uGroupBoxInfo.ResumeLayout(false);
            this.uGroupBoxInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGridList)).EndInit();
            this.uGroupBoxGridList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTFaultQty;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTFaultQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTQty;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTFaultRate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTOTFaultRate;
        private Infragistics.Win.Misc.UltraLabel uLabelTOTQty;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxChart;
        private Infragistics.Win.UltraWinChart.UltraChart uChartPchart;
        private Infragistics.Win.Misc.UltraLabel uLabelCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCL;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInfo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxGridList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUCL;
        private Infragistics.Win.Misc.UltraLabel uLabelUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectFromDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectToDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectItem;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRev;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGrade;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGrade;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchRev;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchInspectItem;

    }
}