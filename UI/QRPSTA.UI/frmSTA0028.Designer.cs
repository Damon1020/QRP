﻿namespace QRPSTA.UI
{
    partial class frmSTA0028
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0028));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchToLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchFromLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridProcessStaticList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGridProcessStaticDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchToLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchFromLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcessStaticList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcessStaticDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchToLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchFromLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSearchProductName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance2;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(482, 12);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductName.TabIndex = 19;
            // 
            // uTextSearchProductCode
            // 
            appearance4.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance4.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductCode.TabIndex = 18;
            this.uTextSearchProductCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchProductCode_KeyDown);
            this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchProductCode_EditorButtonClick);
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchInspectItem.TabIndex = 17;
            this.uComboSearchInspectItem.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectItem
            // 
            this.uLabelSearchInspectItem.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchInspectItem.Name = "uLabelSearchInspectItem";
            this.uLabelSearchInspectItem.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectItem.TabIndex = 16;
            this.uLabelSearchInspectItem.Text = "ultraLabel1";
            // 
            // uDateSearchInspectToDate
            // 
            this.uDateSearchInspectToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectToDate.Location = new System.Drawing.Point(808, 12);
            this.uDateSearchInspectToDate.Name = "uDateSearchInspectToDate";
            this.uDateSearchInspectToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectToDate.TabIndex = 15;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(796, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchInspectFromDate
            // 
            this.uDateSearchInspectFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectFromDate.Location = new System.Drawing.Point(696, 12);
            this.uDateSearchInspectFromDate.Name = "uDateSearchInspectFromDate";
            this.uDateSearchInspectFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectFromDate.TabIndex = 13;
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(592, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectDate.TabIndex = 12;
            this.uLabelSearchInspectDate.Text = "ultraLabel1";
            // 
            // uTextSearchToLotNo
            // 
            this.uTextSearchToLotNo.Location = new System.Drawing.Point(808, 36);
            this.uTextSearchToLotNo.Name = "uTextSearchToLotNo";
            this.uTextSearchToLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchToLotNo.TabIndex = 11;
            this.uTextSearchToLotNo.Visible = false;
            // 
            // ultraLabel1
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance6;
            this.ultraLabel1.Location = new System.Drawing.Point(796, 36);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "~";
            this.ultraLabel1.Visible = false;
            // 
            // uTextSearchFromLotNo
            // 
            this.uTextSearchFromLotNo.Location = new System.Drawing.Point(696, 36);
            this.uTextSearchFromLotNo.Name = "uTextSearchFromLotNo";
            this.uTextSearchFromLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchFromLotNo.TabIndex = 9;
            this.uTextSearchFromLotNo.Visible = false;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(592, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 8;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            this.uLabelSearchLotNo.Visible = false;
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProduct.TabIndex = 6;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            // 
            // uTextSearchCustomerName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance3;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(482, 36);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerName.TabIndex = 5;
            // 
            // uTextSearchCustomerCode
            // 
            appearance19.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance19.TextHAlignAsString = "Center";
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerCode.TabIndex = 3;
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 2;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridProcessStaticList
            // 
            this.uGridProcessStaticList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcessStaticList.DisplayLayout.Appearance = appearance7;
            this.uGridProcessStaticList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcessStaticList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticList.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcessStaticList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGridProcessStaticList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcessStaticList.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGridProcessStaticList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcessStaticList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcessStaticList.DisplayLayout.Override.ActiveCellAppearance = appearance11;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcessStaticList.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridProcessStaticList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcessStaticList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticList.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            appearance14.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcessStaticList.DisplayLayout.Override.CellAppearance = appearance14;
            this.uGridProcessStaticList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcessStaticList.DisplayLayout.Override.CellPadding = 0;
            appearance15.BackColor = System.Drawing.SystemColors.Control;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticList.DisplayLayout.Override.GroupByRowAppearance = appearance15;
            appearance16.TextHAlignAsString = "Left";
            this.uGridProcessStaticList.DisplayLayout.Override.HeaderAppearance = appearance16;
            this.uGridProcessStaticList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcessStaticList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcessStaticList.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridProcessStaticList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcessStaticList.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGridProcessStaticList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcessStaticList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcessStaticList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcessStaticList.Location = new System.Drawing.Point(0, 100);
            this.uGridProcessStaticList.Name = "uGridProcessStaticList";
            this.uGridProcessStaticList.Size = new System.Drawing.Size(1060, 720);
            this.uGridProcessStaticList.TabIndex = 2;
            this.uGridProcessStaticList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridProcessStaticList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridProcessStaticDetail);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGridProcessStaticDetail
            // 
            this.uGridProcessStaticDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcessStaticDetail.DisplayLayout.Appearance = appearance20;
            this.uGridProcessStaticDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcessStaticDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticDetail.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcessStaticDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGridProcessStaticDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcessStaticDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGridProcessStaticDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcessStaticDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcessStaticDetail.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcessStaticDetail.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridProcessStaticDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcessStaticDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticDetail.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcessStaticDetail.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGridProcessStaticDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcessStaticDetail.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcessStaticDetail.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGridProcessStaticDetail.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGridProcessStaticDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcessStaticDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcessStaticDetail.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGridProcessStaticDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcessStaticDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGridProcessStaticDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcessStaticDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcessStaticDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcessStaticDetail.Location = new System.Drawing.Point(12, 12);
            this.uGridProcessStaticDetail.Name = "uGridProcessStaticDetail";
            this.uGridProcessStaticDetail.Size = new System.Drawing.Size(1036, 632);
            this.uGridProcessStaticDetail.TabIndex = 0;
            this.uGridProcessStaticDetail.Text = "ultraGrid1";
            // 
            // frmSTA0028
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridProcessStaticList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0028";
            this.Load += new System.EventHandler(this.frmSTA0028_Load);
            this.Activated += new System.EventHandler(this.frmSTA0028_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0028_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchToLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchFromLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcessStaticList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcessStaticDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchToLotNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchFromLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectItem;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcessStaticList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcessStaticDetail;
    }
}