﻿namespace QRPSTA.UI
{
    partial class frmSTA0069
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0069));
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            this.uGridCCSList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateSearchReqDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchReqDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcessOperationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcessOperationType = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uChart = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCCSList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridCCSList
            // 
            this.uGridCCSList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCCSList.DisplayLayout.Appearance = appearance14;
            this.uGridCCSList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCCSList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCCSList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCCSList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridCCSList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCCSList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridCCSList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCCSList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCCSList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCCSList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridCCSList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCCSList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCCSList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCCSList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridCCSList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCCSList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCCSList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridCCSList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridCCSList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCCSList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridCCSList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridCCSList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCCSList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridCCSList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCCSList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCCSList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCCSList.Location = new System.Drawing.Point(4, 113);
            this.uGridCCSList.Name = "uGridCCSList";
            this.uGridCCSList.Size = new System.Drawing.Size(1048, 423);
            this.uGridCCSList.TabIndex = 313;
            this.uGridCCSList.Text = "ultraGrid1";
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(684, 12);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchProductActionType.TabIndex = 317;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProductActionType
            // 
            this.uLabelSearchProductActionType.Location = new System.Drawing.Point(580, 12);
            this.uLabelSearchProductActionType.Name = "uLabelSearchProductActionType";
            this.uLabelSearchProductActionType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProductActionType.TabIndex = 316;
            this.uLabelSearchProductActionType.Text = "제품구분";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(444, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchCustomer.TabIndex = 315;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uDateSearchReqDateTo
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchReqDateTo.Appearance = appearance17;
            this.uDateSearchReqDateTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchReqDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqDateTo.Location = new System.Drawing.Point(228, 12);
            this.uDateSearchReqDateTo.Name = "uDateSearchReqDateTo";
            this.uDateSearchReqDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchReqDateTo.TabIndex = 312;
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(340, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 314;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProcessCode);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProductActionType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchCustomer);
            this.ultraGroupBox1.Controls.Add(this.uDateSearchReqDateTo);
            this.ultraGroupBox1.Controls.Add(this.uDateSearchReqDateFrom);
            this.ultraGroupBox1.Controls.Add(this.ultraLabel2);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchReqDate);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProcess);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchDetailProcessOperationType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchDetailProcessOperationType);
            this.ultraGroupBox1.Location = new System.Drawing.Point(4, 45);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1060, 68);
            this.ultraGroupBox1.TabIndex = 314;
            // 
            // uDateSearchReqDateFrom
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchReqDateFrom.Appearance = appearance27;
            this.uDateSearchReqDateFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchReqDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqDateFrom.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchReqDateFrom.Name = "uDateSearchReqDateFrom";
            this.uDateSearchReqDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchReqDateFrom.TabIndex = 313;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(216, 20);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel2.TabIndex = 311;
            this.ultraLabel2.Text = "~";
            // 
            // uLabelSearchReqDate
            // 
            this.uLabelSearchReqDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchReqDate.Name = "uLabelSearchReqDate";
            this.uLabelSearchReqDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchReqDate.TabIndex = 310;
            this.uLabelSearchReqDate.Text = "의뢰일";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(580, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 334;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uComboSearchDetailProcessOperationType
            // 
            appearance2.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Appearance = appearance2;
            this.uComboSearchDetailProcessOperationType.BackColor = System.Drawing.Color.White;
            this.uComboSearchDetailProcessOperationType.Location = new System.Drawing.Point(444, 36);
            this.uComboSearchDetailProcessOperationType.Name = "uComboSearchDetailProcessOperationType";
            this.uComboSearchDetailProcessOperationType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchDetailProcessOperationType.TabIndex = 327;
            this.uComboSearchDetailProcessOperationType.Text = "ultraComboEditor1";
            this.uComboSearchDetailProcessOperationType.ValueChanged += new System.EventHandler(this.uComboSearchDetailProcessOperationType_ValueChanged);
            // 
            // uLabelSearchDetailProcessOperationType
            // 
            this.uLabelSearchDetailProcessOperationType.Location = new System.Drawing.Point(340, 36);
            this.uLabelSearchDetailProcessOperationType.Name = "uLabelSearchDetailProcessOperationType";
            this.uLabelSearchDetailProcessOperationType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDetailProcessOperationType.TabIndex = 326;
            this.uLabelSearchDetailProcessOperationType.Text = "공정구분";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 312;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uChart
            // 
            this.uChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChart.Axis.PE = paintElement1;
            this.uChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.LineThickness = 1;
            this.uChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MajorGridLines.Visible = true;
            this.uChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MinorGridLines.Visible = false;
            this.uChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X.Visible = true;
            this.uChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.Visible = false;
            this.uChart.Axis.X2.LineThickness = 1;
            this.uChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MajorGridLines.Visible = true;
            this.uChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MinorGridLines.Visible = false;
            this.uChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X2.Visible = false;
            this.uChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.LineThickness = 1;
            this.uChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MajorGridLines.Visible = true;
            this.uChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MinorGridLines.Visible = false;
            this.uChart.Axis.Y.TickmarkInterval = 50;
            this.uChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y.Visible = true;
            this.uChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.Visible = false;
            this.uChart.Axis.Y2.LineThickness = 1;
            this.uChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChart.Axis.Y2.TickmarkInterval = 50;
            this.uChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y2.Visible = false;
            this.uChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z.Labels.ItemFormatString = "";
            this.uChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.LineThickness = 1;
            this.uChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MajorGridLines.Visible = true;
            this.uChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MinorGridLines.Visible = false;
            this.uChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z.Visible = false;
            this.uChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.Visible = false;
            this.uChart.Axis.Z2.LineThickness = 1;
            this.uChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z2.Visible = false;
            this.uChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChart.Effects.Effects.Add(gradientEffect1);
            this.uChart.Location = new System.Drawing.Point(4, 541);
            this.uChart.Name = "uChart";
            this.uChart.Size = new System.Drawing.Size(1048, 287);
            this.uChart.TabIndex = 315;
            this.uChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcessCode.DisplayLayout.Appearance = appearance92;
            this.uComboSearchProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.Appearance = appearance93;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance94;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance95.BackColor2 = System.Drawing.SystemColors.Control;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance95;
            this.uComboSearchProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance96.BackColor = System.Drawing.SystemColors.Window;
            appearance96.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance96;
            appearance97.BackColor = System.Drawing.SystemColors.Highlight;
            appearance97.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance97;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance98;
            appearance99.BorderColor = System.Drawing.Color.Silver;
            appearance99.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellAppearance = appearance99;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance100.BackColor = System.Drawing.SystemColors.Control;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance100;
            appearance101.TextHAlignAsString = "Left";
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderAppearance = appearance101;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowAppearance = appearance102;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance103;
            this.uComboSearchProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(684, 36);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(120, 22);
            this.uComboSearchProcessCode.TabIndex = 335;
            this.uComboSearchProcessCode.Text = "ultraCombo1";
            // 
            // frmSTA0069
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCCSList);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uChart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0069";
            this.Text = "frmSTA0069";
            this.Load += new System.EventHandler(this.frmSTA0069_Load);
            this.Activated += new System.EventHandler(this.frmSTA0069_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0069_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCCSList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCCSList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductActionType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqDateTo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcessOperationType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcessOperationType;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinChart.UltraChart uChart;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcessCode;
    }
}