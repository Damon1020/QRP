﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPSTA.UI
{
    public partial class frmSTA0076 : Form, QRPCOM.QRPGLO.IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        public frmSTA0076()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 폼 리사이즈
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0076_Resize(object sender, EventArgs e)
        {
            if (this.Width > 1070)
            {
                uGrid.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth + 15;

            }
            else
            {
                uGrid.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            }
        }

        /// <summary>
        /// 화면 닫음
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSTA0076_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        
        private void frmSTA0076_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            //Title 설정
            titleArea.mfSetLabelText("공정 품질실적", m_SysRes.GetString("SYS_FONTNAME"), 12);

            //초기화 메소드
            SetToolAuth();
            InitComboBox();
            InitYear();
            InitLabel();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmSTA0076_Activated(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //툴바 활성화
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        /// <summary>
        /// 사용자 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //사용자 프로그램 권한정보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));
                
                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region 초기화 메소드
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                
                // BL 연결
                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                string strLang = m_resSys.GetString("SYS_LANG");
                System.Data.DataTable dtPlant = clsPlant.mfReadPlantForCombo(strLang);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "", "PlantCode", "PlantName", dtPlant);


                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                // 월 콤보박스
                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    arrValue.Add(i.ToString("D2"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

                //고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCust = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCust);

                System.Data.DataTable dtCustomer = clsCust.mfReadCustomer_Combo(strLang);

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "", "CustomerCode", "CustomerName", dtCustomer);

               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Lable 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchYear, "년", m_resSys.GetString("SYS_LANG"), true, true);
                lbl.mfSetLabel(this.uLabelSearchProductDivide, "제품구분", m_resSys.GetString("SYS_LANG"), true, false);
                lbl.mfSetLabel(this.uLabelSearchCutomer, "고객사", m_resSys.GetString("SYS_LANG"), true, false);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                int intBandIndex = 0;

                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                       , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                       , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";
                if (strLang.Equals("KOR"))
                    strName = "당해 당월 실적,분기별 실적,반기별 실적,Total실적";
                else if (strLang.Equals("CHN"))
                    strName = "当年 当月 实绩,季度别实绩,半年别实绩,Total 实绩";
                else
                    strName = "당해 당월 실적,분기별 실적,반기별 실적,Total실적";

                string [] strGroups = strName.Split(',');

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "MonthlyResult", strGroups[0], 4, 0, 10, 2, false);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
                int i = 0;
                for (i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
                }

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "MTotal", "월간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i, 0, 1, 1, uGroupMonthly);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "QuarterResult", strGroups[1], 15 + i, 0, 4, 2, false);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "HarfResult", strGroups[2], 19 + i, 0, 2, 2, false);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "TotalResult", strGroups[3], 22 + i, 0, 1, 2, false);

                //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "MTotal", "월간Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                //        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Total", "연간Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);



                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.CellAppearance.ForeColor = Color.Black;
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid.DisplayLayout.Bands[0].Groups["MonthlyResult"].CellAppearance.ForeColor = Color.Black;
                this.uGrid.DisplayLayout.Bands[0].Groups["QuarterResult"].CellAppearance.ForeColor = Color.Black;
                this.uGrid.DisplayLayout.Bands[0].Groups["HarfResult"].CellAppearance.ForeColor = Color.Black;
                this.uGrid.DisplayLayout.Bands[0].Groups["TotalResult"].CellAppearance.ForeColor = Color.Black;

                this.uGrid.DisplayLayout.UseFixedHeaders = true;
                this.uGrid.DisplayLayout.Bands[0].Columns["PRODUCTACTIONTYPE"].Header.Fixed = true;
                this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        /// <summary>
        /// 검색툴바 연도 초기화
        /// </summary>
        private void InitYear()
        {
            try
            {  
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        #endregion

        #region IToolBar 메소드
        public void mfCreate()
        {

        }
        public void mfDelete()
        {

        }
        public void mfPrint()
        {

        }
        public void mfSave()
        {

        }
        public void mfExcel()
        {
            try
            {
                if (this.uGrid.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid);
                }
                if (this.uGrid.Rows.Count.Equals(0))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000799", "M000331", "M000800", Infragistics.Win.HAlign.Center);
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                #region 필수확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if(this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000881", "M000813", Infragistics.Win.HAlign.Center);

                    this.uTextSearchYear.Focus();
                    return;
                }
                if (this.uComboSearchMonth.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000881", "M000363", Infragistics.Win.HAlign.Center);

                    this.uComboSearchMonth.DropDown();
                    return;
                }
                #endregion
                

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                SearchProcQCData();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 공정 품질실적 조회
        /// </summary>
        private void SearchProcQCData()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                //검색 파라미터
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uTextSearchYear.Text;
                string strMonth = this.uComboSearchMonth.Value.ToString();
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strCutomerCode = this.uComboSearchCustomer.Value.ToString();

                //DataTable dtRtn = clsReport.mfReadINSProcInspect_frmSTA0076(strPlantCode, strYear, strMonth, strCutomerCode, strProductActionType);
                DataTable dtRtn1 = clsReport.mfReadINSProcInspect_frmSTA0076_D1_PSTS(strPlantCode, strYear, strMonth, strCutomerCode, strProductActionType);
                DataTable dtRtn2 = clsReport.mfReadINSProcInspect_frmSTA0076_D2_PSTS(strPlantCode, strYear, strMonth, strCutomerCode, strProductActionType);
                dtRtn1.TableName = "dtRtn1";
                dtRtn2.TableName = "dtRtn2";

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";
                if (strLang.Equals("KOR"))
                    strName = "검사Lot,불량Lot,시료수,불량수,실적(ppm)";
                else if (strLang.Equals("CHN"))
                    strName = "检查Lot,不良Lot,样品数,不良数,实绩(ppm)";
                else if (strLang.Equals("ENG"))
                    strName = "검사Lot,불량Lot,시료수,불량수,실적(ppm)";

                string[] strGubun = strName.Split(',');

                int intMonthDay = 31;

                DataTable dtProcResultGrid = new DataTable("ProcResult");
                DataColumn[] dcProcResult = new DataColumn[3];
                dtProcResultGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcResultGrid.Columns.Add("PRODUCTACTIONTYPE", typeof(string));
                dtProcResultGrid.Columns.Add("Gubun", typeof(string));
                dtProcResultGrid.Columns.Add("D01", typeof(string));
                dtProcResultGrid.Columns.Add("D02", typeof(string));
                dtProcResultGrid.Columns.Add("D03", typeof(string));
                dtProcResultGrid.Columns.Add("D04", typeof(string));
                dtProcResultGrid.Columns.Add("D05", typeof(string));
                dtProcResultGrid.Columns.Add("D06", typeof(string));
                dtProcResultGrid.Columns.Add("D07", typeof(string));
                dtProcResultGrid.Columns.Add("D08", typeof(string));
                dtProcResultGrid.Columns.Add("D09", typeof(string));
                dtProcResultGrid.Columns.Add("D10", typeof(string));
                for (int i = 11; i <= intMonthDay; i++)
                {
                    dtProcResultGrid.Columns.Add("D" + i.ToString(), typeof(string));
                }
                dtProcResultGrid.Columns.Add("MTotal", typeof(string));
                dtProcResultGrid.Columns.Add("Q1", typeof(string));
                dtProcResultGrid.Columns.Add("Q2", typeof(string));
                dtProcResultGrid.Columns.Add("Q3", typeof(string));
                dtProcResultGrid.Columns.Add("Q4", typeof(string));
                dtProcResultGrid.Columns.Add("FirHalf", typeof(string));
                dtProcResultGrid.Columns.Add("SecHalf", typeof(string));
                dtProcResultGrid.Columns.Add("Total", typeof(string));

                dcProcResult[0] = dtProcResultGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcProcResult[1] = dtProcResultGrid.Columns["PRODUCTACTIONTYPE"];
                dcProcResult[2] = dtProcResultGrid.Columns["Gubun"];
                dtProcResultGrid.PrimaryKey = dcProcResult;



                for (int i = 0; i < dtRtn1.Rows.Count; i++)
                {
                    //LotCount
                    DataRow Lotrow = dtProcResultGrid.NewRow();
                    Lotrow["DETAILPROCESSOPERATIONTYPE"] = dtRtn1.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    Lotrow["PRODUCTACTIONTYPE"] = dtRtn1.Rows[i]["PRODUCTACTIONTYPE"];
                    Lotrow["Gubun"] = strGubun[0]; //검사Lot
                    Lotrow["D01"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D01LotCount"]);
                    Lotrow["D02"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D02LotCount"]);
                    Lotrow["D03"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D03LotCount"]);
                    Lotrow["D04"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D04LotCount"]);
                    Lotrow["D05"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D05LotCount"]);
                    Lotrow["D06"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D06LotCount"]);
                    Lotrow["D07"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D07LotCount"]);
                    Lotrow["D08"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D08LotCount"]);
                    Lotrow["D09"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D09LotCount"]);
                    Lotrow["D10"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D10LotCount"]);
                    Lotrow["MTotal"] = string.Format("{0:n0}", dtRtn1.Rows[i]["MTLotCount"]);

                    Lotrow["Q1"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q01LotCount"]);
                    Lotrow["Q2"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q02LotCount"]);
                    Lotrow["Q3"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q03LotCount"]);
                    Lotrow["Q4"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q04LotCount"]);
                    Lotrow["FirHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["FirHalfLotCount"]);
                    Lotrow["SecHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["SecHalfLotCount"]);
                    Lotrow["Total"] = string.Format("{0:n0}", dtRtn2.Rows[i]["QTLotCount"]);

                    //FailLot
                    DataRow FailLotRow = dtProcResultGrid.NewRow();
                    FailLotRow["DETAILPROCESSOPERATIONTYPE"] = dtRtn1.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    FailLotRow["PRODUCTACTIONTYPE"] = dtRtn1.Rows[i]["PRODUCTACTIONTYPE"];
                    FailLotRow["Gubun"] = strGubun[1];//"불량Lot";

                    FailLotRow["D01"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D01FailLotCount"]);
                    FailLotRow["D02"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D02FailLotCount"]);
                    FailLotRow["D03"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D03FailLotCount"]);
                    FailLotRow["D04"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D04FailLotCount"]);
                    FailLotRow["D05"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D05FailLotCount"]);
                    FailLotRow["D06"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D06FailLotCount"]);
                    FailLotRow["D07"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D07FailLotCount"]);
                    FailLotRow["D08"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D08FailLotCount"]);
                    FailLotRow["D09"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D09FailLotCount"]);
                    FailLotRow["D10"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D10FailLotCount"]);
                    FailLotRow["MTotal"] = string.Format("{0:n0}", dtRtn1.Rows[i]["MTFailLotCount"]);

                    FailLotRow["Q1"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q01FailLotCount"]);
                    FailLotRow["Q2"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q02FailLotCount"]);
                    FailLotRow["Q3"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q03FailLotCount"]);
                    FailLotRow["Q4"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q04FailLotCount"]);
                    FailLotRow["FirHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["FirHalfFailLotCount"]);
                    FailLotRow["SecHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["SecHalfFailLotCount"]);
                    FailLotRow["Total"] = string.Format("{0:n0}", dtRtn2.Rows[i]["QTFailLotCount"]);

                    DataRow SampleSizeRow = dtProcResultGrid.NewRow();
                    SampleSizeRow["DETAILPROCESSOPERATIONTYPE"] = dtRtn1.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    SampleSizeRow["PRODUCTACTIONTYPE"] = dtRtn1.Rows[i]["PRODUCTACTIONTYPE"];
                    SampleSizeRow["Gubun"] = strGubun[2];//"시료수";

                    SampleSizeRow["D01"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D01SampleSize"]);
                    SampleSizeRow["D02"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D02SampleSize"]);
                    SampleSizeRow["D03"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D03SampleSize"]);
                    SampleSizeRow["D04"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D04SampleSize"]);
                    SampleSizeRow["D05"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D05SampleSize"]);
                    SampleSizeRow["D06"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D06SampleSize"]);
                    SampleSizeRow["D07"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D07SampleSize"]);
                    SampleSizeRow["D08"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D08SampleSize"]);
                    SampleSizeRow["D09"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D09SampleSize"]);
                    SampleSizeRow["D10"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D10SampleSize"]);
                    SampleSizeRow["MTotal"] = string.Format("{0:n0}", dtRtn1.Rows[i]["MTSampleSize"]);

                    SampleSizeRow["Q1"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q01SampleSize"]);
                    SampleSizeRow["Q2"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q02SampleSize"]);
                    SampleSizeRow["Q3"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q03SampleSize"]);
                    SampleSizeRow["Q4"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q04SampleSize"]);

                    SampleSizeRow["FirHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["FirHalfSampleSize"]);
                    SampleSizeRow["SecHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["SecHalfSampleSize"]);
                    //SampleSizeRow["Total"] = dtRtn.Rows[i]["TSampleSize"];
                    SampleSizeRow["Total"] = string.Format("{0:n0}", dtRtn2.Rows[i]["QTSampleSize"]);

                    DataRow FaultQtyRow = dtProcResultGrid.NewRow();
                    FaultQtyRow["DETAILPROCESSOPERATIONTYPE"] = dtRtn1.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    FaultQtyRow["PRODUCTACTIONTYPE"] = dtRtn1.Rows[i]["PRODUCTACTIONTYPE"];
                    FaultQtyRow["Gubun"] = strGubun[3];// "불량수";

                    FaultQtyRow["D01"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D01FaultQty"]);
                    FaultQtyRow["D02"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D02FaultQty"]);
                    FaultQtyRow["D03"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D03FaultQty"]);
                    FaultQtyRow["D04"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D04FaultQty"]);
                    FaultQtyRow["D05"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D05FaultQty"]);
                    FaultQtyRow["D06"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D06FaultQty"]);
                    FaultQtyRow["D07"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D07FaultQty"]);
                    FaultQtyRow["D08"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D08FaultQty"]);
                    FaultQtyRow["D09"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D09FaultQty"]);
                    FaultQtyRow["D10"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D10FaultQty"]);
                    FaultQtyRow["MTotal"] = string.Format("{0:n0}", dtRtn1.Rows[i]["MTFaultQty"]);

                    FaultQtyRow["Q1"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q01FaultQty"]);
                    FaultQtyRow["Q2"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q02FaultQty"]);
                    FaultQtyRow["Q3"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q03FaultQty"]);
                    FaultQtyRow["Q4"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q04FaultQty"]);

                    FaultQtyRow["FirHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["FirHalfFaultQty"]);
                    FaultQtyRow["SecHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["SecHalfFaultQty"]);
                    //FaultQtyRow["Total"] = dtRtn.Rows[i]["TFaultQty"];
                    FaultQtyRow["Total"] = string.Format("{0:n0}", dtRtn2.Rows[i]["QTFaultRate"]);

                    DataRow FaultRateRow = dtProcResultGrid.NewRow();

                    FaultRateRow["DETAILPROCESSOPERATIONTYPE"] = dtRtn1.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    FaultRateRow["PRODUCTACTIONTYPE"] = dtRtn1.Rows[i]["PRODUCTACTIONTYPE"];
                    FaultRateRow["Gubun"] = strGubun[4];//"실적(ppm)";

                    FaultRateRow["D01"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D01FaultRate"]);
                    FaultRateRow["D02"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D02FaultRate"]);
                    FaultRateRow["D03"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D03FaultRate"]);
                    FaultRateRow["D04"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D04FaultRate"]);
                    FaultRateRow["D05"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D05FaultRate"]);
                    FaultRateRow["D06"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D06FaultRate"]);
                    FaultRateRow["D07"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D07FaultRate"]);
                    FaultRateRow["D08"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D08FaultRate"]);
                    FaultRateRow["D09"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D09FaultRate"]);
                    FaultRateRow["D10"] = string.Format("{0:n0}", dtRtn1.Rows[i]["D10FaultRate"]);
                    FaultRateRow["MTotal"] = string.Format("{0:n0}", dtRtn1.Rows[i]["MTFaultRate"]);

                    FaultRateRow["Q1"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q01FaultRate"]);
                    FaultRateRow["Q2"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q02FaultRate"]);
                    FaultRateRow["Q3"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q03FaultRate"]);
                    FaultRateRow["Q4"] = string.Format("{0:n0}", dtRtn2.Rows[i]["Q04FaultRate"]);

                    FaultRateRow["FirHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["FirHalfFaultRate"]);
                    FaultRateRow["SecHalf"] = string.Format("{0:n0}", dtRtn2.Rows[i]["SecHalfFaultRate"]);
                    FaultRateRow["Total"] = string.Format("{0:n0}", dtRtn2.Rows[i]["QTFaultRate"]);

                    for (int j = 11; j <= intMonthDay; j++)
                    {
                        Lotrow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn1.Rows[i]["D" + j.ToString() + "LotCount"]);
                        FailLotRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn1.Rows[i]["D" + j.ToString() + "FailLotCount"]);
                        SampleSizeRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn1.Rows[i]["D" + j.ToString() + "SampleSize"]);
                        FaultQtyRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn1.Rows[i]["D" + j.ToString() + "FaultQty"]);
                        FaultRateRow["D" + j.ToString()] = string.Format("{0:n0}", dtRtn1.Rows[i]["D" + j.ToString() + "FaultRate"]);
                    }

                    dtProcResultGrid.Rows.Add(Lotrow);
                    dtProcResultGrid.Rows.Add(FailLotRow);
                    dtProcResultGrid.Rows.Add(SampleSizeRow);
                    dtProcResultGrid.Rows.Add(FaultQtyRow);
                    dtProcResultGrid.Rows.Add(FaultRateRow);
                }

                //this.uGrid.DataSource = null;
                //this.uGrid.ResetDisplayLayout();
                //this.uGrid.Layouts.Clear();

                int intDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));
                if (intDay == 30)
                    this.uGrid.DisplayLayout.Bands[0].Columns["D31"].Hidden = true;
                else
                    this.uGrid.DisplayLayout.Bands[0].Columns["D31"].Hidden = false;

                if (this.uGrid.Rows.Count > 0)
                {
                    this.uGrid.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGrid.Rows.All);
                    this.uGrid.DeleteSelectedRows(false);
                }

                uGrid.DataSource = dtProcResultGrid;
                uGrid.DataBind();

                //색변경
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells["Gubun"].Text == strGubun[4])//"실적(ppm)")
                    {
                        for (int j = 1; j < uGrid.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGrid.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                            this.uGrid.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    this.uGrid.Rows[i].Cells["MTotal"].Appearance.BackColor = Color.MistyRose;
                    this.uGrid.Rows[i].Cells["Total"].Appearance.BackColor = Color.MistyRose;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 공장콤보 변경시 공정타입 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();
                
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProductActionType.Items.Clear();

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                System.Data.DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "", "ActionTypeCode", "ActionTypeName", dtProductActionType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        
        /// <summary>
        /// 레이아웃 초기화(입력값 정수로)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void uGrid_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        //{
        //    try
        //    {
        //        //e.Layout.Bands[0].Columns["D01"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D01"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D01"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D02"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D02"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D02"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D03"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D03"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D03"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D04"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D04"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D04"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D05"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D05"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D05"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D06"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D06"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D06"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D07"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D07"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D07"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D08"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D08"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D08"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D09"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D09"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D09"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["D10"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["D10"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["D10"].CellAppearance.ForeColor = Color.Black;
        //        for (int i = 11; i < 32; i++)
        //        {
        //        //    e.Layout.Bands[0].Columns["D" + i].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //    e.Layout.Bands[0].Columns["D" + i].MaskInput = "nnn,nnn,nnn";
        //            e.Layout.Bands[0].Columns["D" + i].CellAppearance.ForeColor = Color.Black;
        //        }
        //        //e.Layout.Bands[0].Columns["Q1"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["Q1"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["Q1"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["Q2"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["Q2"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["Q2"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["Q3"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["Q3"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["Q3"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["Q4"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["Q4"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["Q4"].CellAppearance.ForeColor = Color.Black;

        //        //e.Layout.Bands[0].Columns["FirHalf"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["FirHalf"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["FirHalf"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["SecHalf"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["SecHalf"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["SecHalf"].CellAppearance.ForeColor = Color.Black;
        //        //e.Layout.Bands[0].Columns["Total"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
        //        //e.Layout.Bands[0].Columns["Total"].MaskInput = "nnn,nnn,nnn";
        //        e.Layout.Bands[0].Columns["Total"].CellAppearance.ForeColor = Color.Black;

        //        e.Layout.Bands[0].Columns["MTotal"].CellAppearance.ForeColor = Color.Black;

        //        e.Layout.UseFixedHeaders = true;
        //        e.Layout.Bands[0].Columns["PRODUCTACTIONTYPE"].Header.Fixed = true;

                

        //        this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
        //    }
        //    catch (Exception ex)
        //    { }
        //    finally
        //    { }
        //}




        //private void ChangeGridColumn(int intBandIndex)
        //{
        //    try
        //    {
        //        string strYear = this.uTextSearchYear.Text;
        //        string strPastYear = Convert.ToString(Convert.ToInt32(strYear) - 1);
        //        string strMonth = this.uComboSearchMonth.Value.ToString();
        //        int intMontDay = DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));
        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinGrid wGrid = new WinGrid();

        //        wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
        //               , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //               , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
        //               , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
        //               , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

        //        //날짜

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //               , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
        //               , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

        //        //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "PRODUCTACTIONTYPE", "제품 구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //        //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
        //        //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //        //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
        //        //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        //wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
        //        //        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //        //        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupMonthly = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "MonthlyResult", strYear + "년" + strMonth + "월 실적", 4, 0, 10, 2, false);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, uGroupMonthly);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, uGroupMonthly);
        //        int i = 0;
        //        for (i = 11; i < 32; i++)
        //        {
        //            if (i <= intMontDay)
        //            {
        //                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
        //                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
        //            }
        //            else
        //            {
        //                wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 20
        //                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i - 1, 0, 1, 1, uGroupMonthly);
        //            }
        //        }

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "MTotal", strMonth + "월 Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", i, 0, 1, 1, uGroupMonthly);

        //        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQuarter = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "QuarterResult", strYear + "년 분기별 실적", 15 + i, 0, 4, 2, false);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q1", "1Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //                 , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                 , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupQuarter);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q2", "2Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupQuarter);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q3", "3Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQuarter);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Q4", "4Q", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 1, uGroupQuarter);

        //        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHarf = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "HarfResult", strYear + "년 반기별 실적", 19 + i, 0, 2, 2, false);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "FirHalf", "전반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupHarf);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "SecHalf", "후반기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupHarf);

        //        Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupTotal = wGrid.mfSetGridGroup(this.uGrid, intBandIndex, "TotalResult", "Total실적", 22 + i, 0, 1, 2, false);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "MTotal", strMonth + "월 Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGroupTotal);

        //        wGrid.mfSetGridColumn(this.uGrid, intBandIndex, "Total", strYear + "년 Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
        //                , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGroupTotal);

        //        this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
        //        this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}
    }
}