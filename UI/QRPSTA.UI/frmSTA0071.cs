﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0071.cs                                         */
/* 프로그램명   : 월별품질 종합현황                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-02                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.Office.Interop.Excel;

namespace QRPSTA.UI
{
    public partial class frmSTA0071 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0071()
        {
            InitializeComponent();
        }

        private void frmSTA0071_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0071_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("월별품질 종합현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitEtc();
            InitGrid();
            

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        private void frmSTA0071_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 메소드
        // Label 초기화
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 콤보박스 초기화 
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                System.Data.DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);

                // 고객사
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                System.Data.DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "", "CustomerCode", "CustomerName", dtCustomer);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화
        private void InitEtc()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Grid 초기화
        private void InitGrid()
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                
                int intBandIndex = 0;

                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Package", "Package", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); 
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M01", "1월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M02", "2월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M03", "3월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M04", "4월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M05", "5월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M06", "6월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M07", "7월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M08", "8월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M09", "9월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M10", "10월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M11", "11월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "M12", "12월", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");        
            }
            catch (System.Exception ex)
            {
                    	
            }
            finally
            {
            }
        }
        #endregion

        #region IToolbar
        public void mfCreate()
        {

        }

        public void mfDelete()
        {

        }

        //public void mfExcel()
        //{
        //    try
        //    {
        //        #region 기존소스 주석처리
        //        /*
        //        int intLastRow = 4;

        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinMessageBox msg = new WinMessageBox();

        //        // 저장여부를 묻는다
        //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                        "확인창", "Excel변환확인", "품질일보 Excel로 변환합니다. 조회조건이 다소 변경될 수 있습니다. 변환하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
        //        {
        //            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        //            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excel.Workbooks.Add(true);
        //            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Worksheets.get_Item(1);
        //            Microsoft.Office.Interop.Excel.Range range;

        //            #region Sub-Strate 에 대한 월별품질현황 처리
        //            //Sub-Strate로 조회한 후 엑셀로 변환한다.

        //            //--전체 Sheet적용 속성--//
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlToRight);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlDown);
        //            range = excelWorksheet.get_Range("A1", range);
        //            range.Font.Size = 9;

        //            //--제목--//
        //            excel.Cells[2, 3] = "● Monthly Quality Trend_Sub-Strate PKG";
        //            range = excelWorksheet.get_Range("C2", "C2");
        //            range.Font.Size = 12;
        //            range.Font.Bold = true;

        //            #region Header 제목
        //            excel.Cells[3, 3] = "공정";
        //            range = excelWorksheet.get_Range("C3", "D4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);
                    
        //            excel.Cells[3, 5] = "1월";
        //            range = excelWorksheet.get_Range("E3", "E4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 6] = "2월";
        //            range = excelWorksheet.get_Range("F3", "F4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 7] = "3월";
        //            range = excelWorksheet.get_Range("G3", "G4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 8] = "4월";
        //            range = excelWorksheet.get_Range("H3", "H4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 9] = "5월";
        //            range = excelWorksheet.get_Range("I3", "I4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 10] = "6월";
        //            range = excelWorksheet.get_Range("J3", "J4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 11] = "7월";
        //            range = excelWorksheet.get_Range("K3", "K4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 12] = "8월";
        //            range = excelWorksheet.get_Range("L3", "L4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 13] = "9월";
        //            range = excelWorksheet.get_Range("M3", "M4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 14] = "10월";
        //            range = excelWorksheet.get_Range("N3", "N4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 15] = "11월";
        //            range = excelWorksheet.get_Range("O3", "O4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);

        //            excel.Cells[3, 16] = "12월";
        //            range = excelWorksheet.get_Range("P3", "P4");
        //            range.MergeCells = true;
        //            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightYellow);
        //            #endregion

        //            //--Data --//
        //            for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //            {                        
        //                if ((i + 1) % 3 == 1)
        //                    excel.Cells[5 + i, 3] = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;

        //                excel.Cells[5 + i, 4] = this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text;
        //                excel.Cells[5 + i, 5] = this.uGridProcTypeList.Rows[i].Cells["M01"].Text;
        //                excel.Cells[5 + i, 6] = this.uGridProcTypeList.Rows[i].Cells["M02"].Text;
        //                excel.Cells[5 + i, 7] = this.uGridProcTypeList.Rows[i].Cells["M03"].Text;
        //                excel.Cells[5 + i, 8] = this.uGridProcTypeList.Rows[i].Cells["M04"].Text;
        //                excel.Cells[5 + i, 9] = this.uGridProcTypeList.Rows[i].Cells["M05"].Text;
        //                excel.Cells[5 + i, 10] = this.uGridProcTypeList.Rows[i].Cells["M06"].Text;
        //                excel.Cells[5 + i, 11] = this.uGridProcTypeList.Rows[i].Cells["M07"].Text;
        //                excel.Cells[5 + i, 12] = this.uGridProcTypeList.Rows[i].Cells["M08"].Text;
        //                excel.Cells[5 + i, 13] = this.uGridProcTypeList.Rows[i].Cells["M09"].Text;
        //                excel.Cells[5 + i, 14] = this.uGridProcTypeList.Rows[i].Cells["M10"].Text;
        //                excel.Cells[5 + i, 15] = this.uGridProcTypeList.Rows[i].Cells["M11"].Text;
        //                excel.Cells[5 + i, 16] = this.uGridProcTypeList.Rows[i].Cells["M12"].Text;

        //                //공정Type Merge 처리
        //                if ((i + 1) % 3 == 0)
        //                {
        //                    range = excelWorksheet.get_Range("C" + (3 + i).ToString(), "C" + (5 + i).ToString());
        //                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                    range.MergeCells = true;

        //                    range = excelWorksheet.get_Range("D" + (5 + i).ToString(), "P" + (5 + i).ToString());
        //                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightCyan);    
        //                }
        //                intLastRow = i;
        //            }

        //            //--Line그리기--//
        //            //행별로 선그리기
        //            range = excelWorksheet.get_Range("C3", "P" + (4 + this.uGridProcTypeList.Rows.Count).ToString());
        //            range.Borders.LineStyle = BorderStyle.FixedSingle;

        //            //--컬럼 폭 조절--//
        //            //((Range)excelWorksheet.Columns["A:A"]).ColumnWidth = 8.8;
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range.ColumnWidth = 0.85;
        //            range = excelWorksheet.get_Range("B1", Type.Missing);
        //            range.ColumnWidth = 0.38;
        //            range = excelWorksheet.get_Range("C1", Type.Missing);
        //            range.ColumnWidth = 5.63;
        //            range = excelWorksheet.get_Range("D1", Type.Missing);
        //            range.ColumnWidth = 5.38;
        //            range = excelWorksheet.get_Range("E1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("F1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("G1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("H1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("I1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("J1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("K1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("L1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("M1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("N1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("O1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("P1", Type.Missing);
        //            range.ColumnWidth = 8.88;
        //            range = excelWorksheet.get_Range("Q1", Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range("R1", Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range("S1", Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range("T1", Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range("U1", Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range("V1", Type.Missing);
        //            range.ColumnWidth = 2.5;

        //            //--전체 Chart그리기--//
        //            Microsoft.Office.Interop.Excel.ChartObjects chartObjects = (Microsoft.Office.Interop.Excel.ChartObjects)excelWorksheet.ChartObjects(Type.Missing);
        //            Microsoft.Office.Interop.Excel.ChartObject dchartObject = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(15, (intLastRow+6)*11.25, 270, 250);
        //            Microsoft.Office.Interop.Excel.Chart chart = dchartObject.Chart;
                    
        //            //범례지정
        //            Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)chart.SeriesCollection(Type.Missing);
        //            for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //            {
        //                if ((i + 1) % 3 == 0)
        //                {
        //                    Microsoft.Office.Interop.Excel.Series ser1 = seriesCollection.NewSeries();
        //                    ser1.Values = excelWorksheet.get_Range("E" + (5 + i).ToString(), "P" + (5 + i).ToString());
        //                    ser1.XValues = excelWorksheet.get_Range("E3", "P3");
        //                    ser1.Name = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                }
        //            }
        //            chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //            chart.Legend.Font.Size = 9;
        //            chart.Legend.Font.Name = "맑은 고딕";
        //            chart.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //            //Chart유형 지정
        //            Microsoft.Office.Interop.Excel.Chart chartPage = chart;
        //            chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;

        //            //Chart Style 지정
        //            Microsoft.Office.Interop.Excel.Axis x = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //            Microsoft.Office.Interop.Excel.Axis y = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //            x.TickLabels.Font.Size = 7;
        //            x.TickLabels.Font.Bold = false;
        //            x.TickLabels.Font.Name = "맑은 고딕";
        //            y.TickLabels.Font.Size = 7;
        //            y.TickLabels.Font.Bold = false;
        //            y.TickLabels.Font.Name = "맑은 고딕";

        //            //--공정Type별 Chart그리기--//
        //            for (int i = 1; i < uTabChartInfo.Tabs.Count; i++)
        //            {
        //                for (int j = 0; j < uGridProcTypeList.Rows.Count; j++)
        //                {
        //                    if (uTabChartInfo.Tabs[i].Key == uGridProcTypeList.Rows[j].Cells["DETAILPROCESSOPERATIONTYPE"].Text)
        //                    {
        //                        double dblLeft = 15 + 270 * (i % 4);
        //                        double dblTop = (intLastRow + 6) * 11.25 + 250 * (i / 4);

        //                        Microsoft.Office.Interop.Excel.ChartObject dchartObjectPT = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 270, 250);
        //                        Microsoft.Office.Interop.Excel.Chart chartPT = dchartObjectPT.Chart;
                                
        //                        //범례지정
        //                        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionPT = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPT.SeriesCollection(Type.Missing);                                
        //                        Microsoft.Office.Interop.Excel.Series ser2 = seriesCollectionPT.NewSeries();
        //                        ser2.Values = excelWorksheet.get_Range("E" + (7 + j).ToString(), "P" + (7 + j).ToString());
        //                        ser2.XValues = excelWorksheet.get_Range("E3", "P3");
        //                        ser2.Name = uTabChartInfo.Tabs[i].Key;
        //                        chartPT.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                        chartPT.Legend.Font.Size = 9;
        //                        chartPT.Legend.Font.Name = "맑은 고딕";
        //                        chartPT.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                        //Chart유형 지정
        //                        Microsoft.Office.Interop.Excel.Chart chartPagePT = chartPT;
        //                        chartPagePT.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;

        //                        //Chart Style 지정
        //                        x = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                        y = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                        x.TickLabels.Font.Size = 7;
        //                        x.TickLabels.Font.Bold = false;
        //                        x.TickLabels.Font.Name = "맑은 고딕";
        //                        y.TickLabels.Font.Size = 7;
        //                        y.TickLabels.Font.Bold = false;
        //                        chartPT.HasTitle = false;
        //                        y.TickLabels.Font.Name = "맑은 고딕";
                                
        //                        break;
        //                    }
        //                }
        //            }
        //            #endregion

        //            //Lead-Frame에 대한 월별품질현황 처리

        //            excelWorksheet.Application.ActiveWindow.Zoom = 75;

        //            string strExePath = System.Windows.Forms.Application.ExecutablePath;
        //            int intPos = strExePath.LastIndexOf(@"\");
        //            strExePath = strExePath.Substring(0, intPos + 1) + "QRPKPI\\";
                    
        //            //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
        //            if (!System.IO.Directory.Exists(strExePath))
        //                System.IO.Directory.CreateDirectory(strExePath);

        //            excelWorkbook.SaveAs(strExePath + uTextSearchYear.Text + "_Monthly Quality Trend.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, 
        //                                 Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //            excel.Visible = true;
        //            //excelWorkbook.Close(true, Type.Missing, Type.Missing);
        //            //app.Quit();

        //            releaseObject(excel);
                 
        //        }
        //        */
        //        #endregion

        //        #region Var
        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinMessageBox msg = new WinMessageBox();

        //        int intStartRow = 2;        // 시작행
        //        int intStartCol = 3;        // 시작컬럼
        //        int intMonths = 12;      // 달수
        //        int intChartHeight = 17;    // 차트 하나가 차지하는 줄의 수
        //        int intGridRowCount = 0;    // 그리드 줄 수
        //        double dblheight = 5.6;     // 차트 위치 결정하기 위한 변수( 표와 차트사이의 공백높이)
        //        #endregion

        //        // MessageBox 출력
        //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                        "M001264", "M000048", "M001215", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
        //        {
        //            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        //            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excel.Workbooks.Add(true);
        //            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Worksheets.get_Item(1);
        //            Microsoft.Office.Interop.Excel.Range range;

        //            #region 속성 설정
        //            excelWorksheet.Name = "Monthly Quality Trend";
        //            //--전체 Sheet적용 속성--//
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlToRight);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlDown);
        //            range = excelWorksheet.get_Range("A1", range);
        //            range.Font.Size = 9;
        //            range.RowHeight = 11.25;

        //            //--컬럼 폭 조절--//
        //            //((Range)excelWorksheet.Columns["A:A"]).ColumnWidth = 8.8;
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range.ColumnWidth = 0.85;
        //            range = excelWorksheet.get_Range("B1", Type.Missing);
        //            range.ColumnWidth = 0.38;
        //            range = excelWorksheet.get_Range("C1", Type.Missing);
        //            range.ColumnWidth = 5.63;
        //            range = excelWorksheet.get_Range("D1", Type.Missing);
        //            range.ColumnWidth = 5.38;
        //            // 일자부분
        //            for (int i = 0; i < intMonths; i++)
        //            {
        //                range = excelWorksheet.get_Range(CellAddress(1, i + 5), Type.Missing);
        //                range.ColumnWidth = 8.88;
        //            }
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonths + 5), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonths + 6), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonths + 7), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonths + 8), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonths + 9), Type.Missing);
        //            range.ColumnWidth = 2.5;
        //            #endregion

        //            #region Sub-Strate 에 대한 월별품질현황 처리
        //            //Sub-Strate로 조회한 후 엑셀로 변환한다.
        //            if (!this.uComboSearchProductActionType.Value.ToString().Equals("Sub"))
        //            {
        //                this.uComboSearchProductActionType.Value = "Sub";
        //                //mfSearch();
        //            }

        //            if (this.uGridProcTypeList.Rows.Count > 0)
        //            {
        //                //--제목--//
        //                excel.Cells[intStartRow, intStartCol] = "● Monthly Quality Trend_Sub-Strate Pro Type";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow, intStartCol), CellAddress(intStartRow, intStartCol));
        //                range.Font.Size = 12;
        //                range.Font.Bold = true;
        //                range.RowHeight = 15.75;

        //                #region Header
        //                excel.Cells[intStartRow + 1, intStartCol] = "공정";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol), CellAddress(intStartRow + 3, intStartCol + 1));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                for (int i = 1; i <= intMonths; i++)
        //                {
        //                    excel.Cells[intStartRow + 2, intStartCol + 1 + i] = i.ToString() + "월";
        //                    range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 1 + i), CellAddress(intStartRow + 3, intStartCol + 1 + i));
        //                    range.MergeCells = true;
        //                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //                }
        //                #endregion

        //                #region Write Data in Cell
        //                //--Data --//
        //                for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 4 == 1)
        //                        excel.Cells[intStartRow + 4 + i, intStartCol] = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;

        //                    excel.Cells[intStartRow + 4 + i, intStartCol + 1] = this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text;

        //                    for (int j = 1; j <= intMonths; j++)
        //                    {
        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 1 + j] = this.uGridProcTypeList.Rows[i].Cells["M" + j.ToString("D2")].Text;
        //                    }

        //                    //공정Type Merge 처리
        //                    if ((i + 1) % 4 == 0)
        //                    {
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 1 + i), intStartCol), CellAddress((intStartRow + 4 + i), intStartCol));
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;

        //                        // 불량율영역 배경색 지정
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 4 + i), intStartCol + 1), CellAddress((intStartRow + 4 + i), intStartCol + 1 + intMonths));
        //                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(204, 255, 255));
        //                    }
        //                }
        //                intGridRowCount = this.uGridProcTypeList.Rows.Count;
        //                #endregion

        //                //--Line그리기--//
        //                //행별로 선그리기
        //                range = excelWorksheet.get_Range(CellAddress((intStartRow + 1), intStartCol), CellAddress(intStartRow + 3 + intGridRowCount, intStartCol + 1 + intMonths));
        //                range.Borders.LineStyle = BorderStyle.FixedSingle;

        //                #region 取消图表
        //                /*
        //                #region Chart
        //                //--전체 Chart그리기--//
        //                double dblLeft = 15;
        //                double dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + dblheight + 15.75;
        //                Microsoft.Office.Interop.Excel.ChartObjects chartObjects = (Microsoft.Office.Interop.Excel.ChartObjects)excelWorksheet.ChartObjects(Type.Missing);
        //                Microsoft.Office.Interop.Excel.ChartObject dchartObject = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 213, 180);
        //                Microsoft.Office.Interop.Excel.Chart chart = dchartObject.Chart;

        //                //범례지정
        //                Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)chart.SeriesCollection(Type.Missing);
        //                for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 3 == 0)
        //                    {
        //                        Microsoft.Office.Interop.Excel.Series ser1 = seriesCollection.NewSeries();
        //                        ser1.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 3 + i, intStartCol + 2), CellAddress(intStartRow + 3 + i, intStartCol + 2 + intMonths));
        //                        ser1.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2), CellAddress(intStartRow + 1, intStartCol + 1 + intMonths));
        //                        ser1.Name = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                    }
        //                }
        //                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                chart.Legend.Font.Size = 9;
        //                chart.Legend.Font.Name = "맑은 고딕";
        //                chart.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                //Chart유형 지정
        //                Microsoft.Office.Interop.Excel.Chart chartPage = chart;
        //                chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;

        //                // 그래프에 값나오게 하는 구문 => 지져분해서 주석처리
        //                //chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLineStacked;
        //                //chartPage.ApplyDataLabels(XlDataLabelsType.xlDataLabelsShowValue, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

        //                //Chart Style 지정
        //                Microsoft.Office.Interop.Excel.Axis x = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                Microsoft.Office.Interop.Excel.Axis y = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                x.TickLabels.Font.Size = 7;
        //                x.TickLabels.Font.Bold = false;
        //                x.TickLabels.Font.Name = "맑은 고딕";
        //                y.TickLabels.Font.Size = 7;
        //                y.TickLabels.Font.Bold = false;
        //                y.TickLabels.Font.Name = "맑은 고딕";

        //                // Chart 내부색 지정
        //                chart.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                // Chart 외부색 지정
        //                x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                //--공정Type별 Chart그리기--//
        //                for (int i = 1; i < uTabChartInfo.Tabs.Count; i++)
        //                {
        //                    for (int j = 0; j < uGridProcTypeList.Rows.Count; j++)
        //                    {
        //                        if (uTabChartInfo.Tabs[i].Key == uGridProcTypeList.Rows[j].Cells["DETAILPROCESSOPERATIONTYPE"].Text)
        //                        {
        //                            dblLeft = 15 + 213 * (i % 4);
        //                            dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + 180 * (i / 4) + dblheight + 15.75;

        //                            Microsoft.Office.Interop.Excel.ChartObject dchartObjectPT = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 213, 180);
        //                            Microsoft.Office.Interop.Excel.Chart chartPT = dchartObjectPT.Chart;

        //                            //범례지정
        //                            Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionPT = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPT.SeriesCollection(Type.Missing);
        //                            Microsoft.Office.Interop.Excel.Series ser2 = seriesCollectionPT.NewSeries();
        //                            ser2.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 5 + j, intStartCol + 2), CellAddress(intStartRow + 5 + j, intStartCol + 1 + intMonths));
        //                            ser2.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2), CellAddress(intStartRow + 1, intStartCol + 1 + intMonths));
        //                            ser2.Name = uTabChartInfo.Tabs[i].Key;
        //                            chartPT.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                            chartPT.Legend.Font.Size = 9;
        //                            chartPT.Legend.Font.Name = "맑은 고딕";
        //                            chartPT.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                            //Chart유형 지정
        //                            Microsoft.Office.Interop.Excel.Chart chartPagePT = chartPT;
        //                            chartPagePT.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLineStacked;
        //                            // Chart Title 사용하지 않함
        //                            chartPagePT.ChartTitle.Delete();

        //                            //Chart Style 지정
        //                            x = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            y = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            x.TickLabels.Font.Size = 7;
        //                            x.TickLabels.Font.Bold = false;
        //                            x.TickLabels.Font.Name = "맑은 고딕";
        //                            y.TickLabels.Font.Size = 7;
        //                            y.TickLabels.Font.Bold = false;
        //                            y.TickLabels.Font.Name = "맑은 고딕";

        //                            // Chart 내부색 지정
        //                            chartPT.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                            // Chart 외부색 지정
        //                            x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                            y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                            break;
        //                        }
        //                    }
        //                }
        //                #endregion

        //                */
        //                #endregion
        //            }
        //            #endregion

        //            // Lead-Frame 일별 품질일보 시작점 설정
        //            // 그리드줄수 + 차트가 차지하는 줄수 + Sub차트 헤더줄수(2)
        //            intStartRow += intGridRowCount + (((intGridRowCount + 2) / 12) + 1) * intChartHeight + 2;

        //            #region Lead-Frame에 대한 월별품질현황 처리
        //            // Lead-Frame으로 조회
        //            if (!this.uComboSearchProductActionType.Value.ToString().Equals("Lead Frame"))
        //            {
        //                this.uComboSearchProductActionType.Value = "Lead Frame";
        //                //mfSearch();
        //            }

        //            if (this.uGridPackageList.Rows.Count > 0)
        //            {
        //                //--제목--//
        //                excel.Cells[intStartRow, intStartCol] = "● Monthly Quality Trend_Lead-Frame PKG";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow, intStartCol), CellAddress(intStartRow, intStartCol));
        //                range.Font.Size = 12;
        //                range.Font.Bold = true;
        //                range.RowHeight = 15.75;

        //                #region Header
        //                excel.Cells[intStartRow + 1, intStartCol] = "공정";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol), CellAddress(intStartRow + 3, intStartCol + 2));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                for (int i = 1; i <= intMonths; i++)
        //                {
        //                    excel.Cells[intStartRow + 2, intStartCol + 2 + i] = i.ToString() + "월";
        //                    range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2 + i), CellAddress(intStartRow + 3, intStartCol + 2 + i));
        //                    range.MergeCells = true;
        //                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //                }
        //                #endregion

        //                #region Write Data in Cell
        //                //--Data --//
        //                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 4 == 1)
        //                        excel.Cells[intStartRow + 4 + i, intStartCol] = this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;

        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 1] = this.uGridPackageList.Rows[i].Cells["Package"].Text;

        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 2] = this.uGridPackageList.Rows[i].Cells["Gubun"].Text;

        //                    for (int j = 1; j <= intMonths; j++)
        //                    {
        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 2 + j] = this.uGridPackageList.Rows[i].Cells["M" + j.ToString("D2")].Text;
        //                    }

        //                    //공정Type Merge 처리
        //                    if ((i + 1) % 4 == 0)
        //                    {
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 1 + i), intStartCol), CellAddress((intStartRow + 4 + i), intStartCol));
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;

        //                        // 불량율영역 배경색 지정
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 4 + i), intStartCol + 1), CellAddress((intStartRow + 4 + i), intStartCol + 2 + intMonths));
        //                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(204, 255, 255));
        //                    }
        //                }
        //                intGridRowCount = this.uGridPackageList.Rows.Count;
        //                #endregion

        //                //--Line그리기--//
        //                //행별로 선그리기
        //                range = excelWorksheet.get_Range(CellAddress((intStartRow + 1), intStartCol), CellAddress(intStartRow + 3 + intGridRowCount, intStartCol + 2 + intMonths));
        //                range.Borders.LineStyle = BorderStyle.FixedSingle;

        //                #region 取消图表
        //                /*
        //                #region Chart
        //                //--전체 Chart그리기--//
        //                double dblLeft = 15;
        //                double dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + dblheight * 2 + 15.75;
        //                Microsoft.Office.Interop.Excel.ChartObjects chartObjects = (Microsoft.Office.Interop.Excel.ChartObjects)excelWorksheet.ChartObjects(Type.Missing);
        //                Microsoft.Office.Interop.Excel.ChartObject dchartObject = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 213, 180);
        //                Microsoft.Office.Interop.Excel.Chart chart = dchartObject.Chart;

        //                //범례지정
        //                Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)chart.SeriesCollection(Type.Missing);
        //                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 3 == 0)
        //                    {
        //                        Microsoft.Office.Interop.Excel.Series ser1 = seriesCollection.NewSeries();
        //                        ser1.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 3 + i, intStartCol + 2), CellAddress(intStartRow + 3 + i, intStartCol + 2 + intMonths));
        //                        ser1.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2), CellAddress(intStartRow + 1, intStartCol + 1 + intMonths));
        //                        ser1.Name = this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                    }
        //                }
        //                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                chart.Legend.Font.Size = 9;
        //                chart.Legend.Font.Name = "맑은 고딕";
        //                chart.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                //Chart유형 지정
        //                Microsoft.Office.Interop.Excel.Chart chartPage = chart;
        //                chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                // Chart Title 사용하지 않함
        //                //chartPage.ChartTitle.Delete();

        //                //Chart Style 지정
        //                Microsoft.Office.Interop.Excel.Axis x = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                Microsoft.Office.Interop.Excel.Axis y = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                x.TickLabels.Font.Size = 7;
        //                x.TickLabels.Font.Bold = false;
        //                x.TickLabels.Font.Name = "맑은 고딕";
        //                y.TickLabels.Font.Size = 7;
        //                y.TickLabels.Font.Bold = false;
        //                y.TickLabels.Font.Name = "맑은 고딕";

        //                // Chart 내부색 지정
        //                chart.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                // Chart 외부색 지정
        //                x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                //--공정Type별 Chart그리기--//
        //                for (int i = 1; i < uTabChartInfo.Tabs.Count; i++)
        //                {
        //                    for (int j = 0; j < uGridPackageList.Rows.Count; j++)
        //                    {
        //                        if (uTabChartInfo.Tabs[i].Key == uGridPackageList.Rows[j].Cells["DETAILPROCESSOPERATIONTYPE"].Text)
        //                        {
        //                            dblLeft = 15 + 213 * (i % 4);
        //                            dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + 180 * (i / 4) + dblheight * 2 + 15.75;

        //                            Microsoft.Office.Interop.Excel.ChartObject dchartObjectPT = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 213, 180);
        //                            Microsoft.Office.Interop.Excel.Chart chartPT = dchartObjectPT.Chart;

        //                            //범례지정
        //                            Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionPT = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPT.SeriesCollection(Type.Missing);
        //                            Microsoft.Office.Interop.Excel.Series ser2 = seriesCollectionPT.NewSeries();
        //                            ser2.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 5 + j, intStartCol + 2), CellAddress(intStartRow + 5 + j, intStartCol + 1 + intMonths));
        //                            ser2.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2), CellAddress(intStartRow + 1, intStartCol + 1 + intMonths));
        //                            ser2.Name = uTabChartInfo.Tabs[i].Key;
        //                            chartPT.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                            chartPT.Legend.Font.Size = 9;
        //                            chartPT.Legend.Font.Name = "맑은 고딕";
        //                            chartPT.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                            //Chart유형 지정
        //                            Microsoft.Office.Interop.Excel.Chart chartPagePT = chartPT;
        //                            chartPagePT.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                            // Chart Title 사용하지 않함
        //                            chartPagePT.ChartTitle.Delete();

        //                            //Chart Style 지정
        //                            x = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            y = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            x.TickLabels.Font.Size = 7;
        //                            x.TickLabels.Font.Bold = false;
        //                            x.TickLabels.Font.Name = "맑은 고딕";
        //                            y.TickLabels.Font.Size = 7;
        //                            y.TickLabels.Font.Bold = false;
        //                            y.TickLabels.Font.Name = "맑은 고딕";

        //                            // Chart 내부색 지정
        //                            chartPT.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                            // Chart 외부색 지정
        //                            x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                            y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                            break;
        //                        }
        //                    }
        //                }
        //                #endregion

        //                */
        //                #endregion
        //            }
        //            #endregion

        //            // 테두리선 그리기
        //            // 마지막줄수를 구한다
        //            intStartRow += intGridRowCount + (((intGridRowCount + 2) / 12) + 1) * intChartHeight + 2;
        //            range = excelWorksheet.get_Range(CellAddress(2, 2), CellAddress(intStartRow, intStartCol + intMonths + 5));
        //            range.BorderAround(Type.Missing, XlBorderWeight.xlThick, XlColorIndex.xlColorIndexAutomatic, Type.Missing);

        //            #region 엑셀파일 저장
        //            excelWorksheet.Application.ActiveWindow.Zoom = 65;

        //            string strExePath = System.Windows.Forms.Application.ExecutablePath;
        //            int intPos = strExePath.LastIndexOf(@"\");
        //            strExePath = strExePath.Substring(0, intPos + 1) + "QRPKPI\\";

        //            //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
        //            if (!System.IO.Directory.Exists(strExePath))
        //                System.IO.Directory.CreateDirectory(strExePath);

        //            excelWorkbook.SaveAs(strExePath + uTextSearchYear.Text + "_Monthly Quality Trend.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //                                 Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //            excel.Visible = true;
        //            //excelWorkbook.Close(true, Type.Missing, Type.Missing);
        //            //app.Quit();

        //            // 가비지 콜렉션
        //            releaseObject(excel);
        //            #endregion
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}

        public void mfExcel()
        {
            try
            {
                WinGrid wGrid = new WinGrid();

                if ( this.uTabInfo.ActiveTab.Key == "Process" && this.uGridProcTypeList.Rows.Count > 0 )
                {
                    
                    wGrid.mfDownLoadGridToExcel(this.uGridProcTypeList);
                }
                else if (this.uTabInfo.ActiveTab.Key == "Package" && this.uGridPackageList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridPackageList);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfSave()
        {

        }

        public void mfSearch()
        {
            try
            {   
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextSearchYear.Text.Length < 4)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001107", Infragistics.Win.HAlign.Center);
                    this.uTextSearchYear.Focus();
                    return;
                }

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //조회
                System.Data.DataTable dtProcType = new System.Data.DataTable();
                System.Data.DataTable dtProcTypePackage = new System.Data.DataTable();
                ////dtProcType = clsReport.mfReadINSProcInspect_frmSTA0071_D1(strPlantCode, this.uTextSearchYear.Text, strProductActionType);
                ////dtProcTypePackage = clsReport.mfReadINSProcInspect_frmSTA0071_D2(strPlantCode, this.uTextSearchYear.Text, strProductActionType);
                // PSTS
                dtProcType = clsReport.mfReadINSProcInspect_frmSTA0071_D1_PSTS(strPlantCode, this.uTextSearchYear.Text, strProductActionType, strCustomerCode);
                dtProcTypePackage = clsReport.mfReadINSProcInspect_frmSTA0071_D2_PSTS(strPlantCode, this.uTextSearchYear.Text, strProductActionType, strCustomerCode);

                dtProcType.TableName = "ProcType";
                dtProcTypePackage.TableName = "ProcTypePackage";

                #region Grid에 뿌려줄 DataTable 형태로 조정
                System.Data.DataTable dtProcTypeGrid = new System.Data.DataTable("ProcType");
                DataColumn[] dcPKProcTypeGrid = new DataColumn[2];
                dtProcTypeGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcTypeGrid.Columns.Add("Gubun", typeof(string));
                dtProcTypeGrid.Columns.Add("M01", typeof(double));
                dtProcTypeGrid.Columns.Add("M02", typeof(double));
                dtProcTypeGrid.Columns.Add("M03", typeof(double));
                dtProcTypeGrid.Columns.Add("M04", typeof(double));
                dtProcTypeGrid.Columns.Add("M05", typeof(double));
                dtProcTypeGrid.Columns.Add("M06", typeof(double));
                dtProcTypeGrid.Columns.Add("M07", typeof(double));
                dtProcTypeGrid.Columns.Add("M08", typeof(double));
                dtProcTypeGrid.Columns.Add("M09", typeof(double));
                dtProcTypeGrid.Columns.Add("M10", typeof(double));
                dtProcTypeGrid.Columns.Add("M11", typeof(double));
                dtProcTypeGrid.Columns.Add("M12", typeof(double));
                dtProcTypeGrid.Columns.Add("Total", typeof(double));
                dcPKProcTypeGrid[0] = dtProcTypeGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcPKProcTypeGrid[1] = dtProcTypeGrid.Columns["Gubun"];
                dtProcTypeGrid.PrimaryKey = dcPKProcTypeGrid;

                System.Data.DataTable dtProcTypePackageGrid = new System.Data.DataTable("ProcTypePackage");
                DataColumn[] dcPKProcTypePackageGrid = new DataColumn[3];
                dtProcTypePackageGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcTypePackageGrid.Columns.Add("Package", typeof(string));
                dtProcTypePackageGrid.Columns.Add("Gubun", typeof(string));
                dtProcTypePackageGrid.Columns.Add("M01", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M02", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M03", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M04", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M05", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M06", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M07", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M08", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M09", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M10", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M11", typeof(double));
                dtProcTypePackageGrid.Columns.Add("M12", typeof(double));
                dtProcTypePackageGrid.Columns.Add("Total", typeof(double));
                dcPKProcTypePackageGrid[0] = dtProcTypePackageGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcPKProcTypePackageGrid[1] = dtProcTypePackageGrid.Columns["Package"];
                dcPKProcTypePackageGrid[2] = dtProcTypePackageGrid.Columns["Gubun"];
                dtProcTypePackageGrid.PrimaryKey = dcPKProcTypePackageGrid;

                string strLang = m_resSys.GetString("SYS_LANG");
                string[] strGubun = new string[3];
                strGubun[0] = msg.GetMessge_Text("M000772", strLang); //시료수
                strGubun[1] = msg.GetMessge_Text("M000607", strLang); //불량수
                strGubun[2] = msg.GetMessge_Text("M000603", strLang); //불량률
                
                for (int i = 0; i < dtProcType.Rows.Count; i++)
                {                    
                    //infragistics.Win.UltraWinGrid.UltraGridRow row = this.uGridMonthList.DisplayLayout.Bands[0].AddNew();
                    DataRow row = dtProcTypeGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["GUBUN"] =strGubun[0];
                    row["M01"] = string.Format("{0:n0}", dtProcType.Rows[i]["M01SampleSize"]);
                    row["M02"] = string.Format("{0:n0}", dtProcType.Rows[i]["M02SampleSize"]);
                    row["M03"] = string.Format("{0:n0}", dtProcType.Rows[i]["M03SampleSize"]);
                    row["M04"] = string.Format("{0:n0}", dtProcType.Rows[i]["M04SampleSize"]);
                    row["M05"] = string.Format("{0:n0}", dtProcType.Rows[i]["M05SampleSize"]);
                    row["M06"] = string.Format("{0:n0}", dtProcType.Rows[i]["M06SampleSize"]);
                    row["M07"] = string.Format("{0:n0}", dtProcType.Rows[i]["M07SampleSize"]);
                    row["M08"] = string.Format("{0:n0}", dtProcType.Rows[i]["M08SampleSize"]);
                    row["M09"] = string.Format("{0:n0}", dtProcType.Rows[i]["M09SampleSize"]);
                    row["M10"] = string.Format("{0:n0}", dtProcType.Rows[i]["M10SampleSize"]);
                    row["M11"] = string.Format("{0:n0}", dtProcType.Rows[i]["M11SampleSize"]);
                    row["M12"] = string.Format("{0:n0}", dtProcType.Rows[i]["M12SampleSize"]);
                    row["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TSampleSize"]);
                    dtProcTypeGrid.Rows.Add(row);

                    row = dtProcTypeGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["GUBUN"] = strGubun[1];
                    row["M01"] = string.Format("{0:n0}", dtProcType.Rows[i]["M01FaultQty"]);
                    row["M02"] = string.Format("{0:n0}", dtProcType.Rows[i]["M02FaultQty"]);
                    row["M03"] = string.Format("{0:n0}", dtProcType.Rows[i]["M03FaultQty"]);
                    row["M04"] = string.Format("{0:n0}", dtProcType.Rows[i]["M04FaultQty"]);
                    row["M05"] = string.Format("{0:n0}", dtProcType.Rows[i]["M05FaultQty"]);
                    row["M06"] = string.Format("{0:n0}", dtProcType.Rows[i]["M06FaultQty"]);
                    row["M07"] = string.Format("{0:n0}", dtProcType.Rows[i]["M07FaultQty"]);
                    row["M08"] = string.Format("{0:n0}", dtProcType.Rows[i]["M08FaultQty"]);
                    row["M09"] = string.Format("{0:n0}", dtProcType.Rows[i]["M09FaultQty"]);
                    row["M10"] = string.Format("{0:n0}", dtProcType.Rows[i]["M10FaultQty"]);
                    row["M11"] = string.Format("{0:n0}", dtProcType.Rows[i]["M11FaultQty"]);
                    row["M12"] = string.Format("{0:n0}", dtProcType.Rows[i]["M12FaultQty"]);
                    row["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TFaultQty"]);
                    dtProcTypeGrid.Rows.Add(row);

                    row = dtProcTypeGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["GUBUN"] = strGubun[2];
                    row["M01"] = string.Format("{0:n0}", dtProcType.Rows[i]["M01FaultRate"]);
                    row["M02"] = string.Format("{0:n0}", dtProcType.Rows[i]["M02FaultRate"]);
                    row["M03"] = string.Format("{0:n0}", dtProcType.Rows[i]["M03FaultRate"]);
                    row["M04"] = string.Format("{0:n0}", dtProcType.Rows[i]["M04FaultRate"]);
                    row["M05"] = string.Format("{0:n0}", dtProcType.Rows[i]["M05FaultRate"]);
                    row["M06"] = string.Format("{0:n0}", dtProcType.Rows[i]["M06FaultRate"]);
                    row["M07"] = string.Format("{0:n0}", dtProcType.Rows[i]["M07FaultRate"]);
                    row["M08"] = string.Format("{0:n0}", dtProcType.Rows[i]["M08FaultRate"]);
                    row["M09"] = string.Format("{0:n0}", dtProcType.Rows[i]["M09FaultRate"]);
                    row["M10"] = string.Format("{0:n0}", dtProcType.Rows[i]["M10FaultRate"]);
                    row["M11"] = string.Format("{0:n0}", dtProcType.Rows[i]["M11FaultRate"]);
                    row["M12"] = string.Format("{0:n0}", dtProcType.Rows[i]["M12FaultRate"]);
                    row["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TFaultRate"]);
                    dtProcTypeGrid.Rows.Add(row);

                    row = dtProcTypeGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["GUBUN"] = "Target";
                    row["M01"] = string.Format("{0:n0}", dtProcType.Rows[i]["M01Target"]);
                    row["M02"] = string.Format("{0:n0}", dtProcType.Rows[i]["M02Target"]);
                    row["M03"] = string.Format("{0:n0}", dtProcType.Rows[i]["M03Target"]);
                    row["M04"] = string.Format("{0:n0}", dtProcType.Rows[i]["M04Target"]);
                    row["M05"] = string.Format("{0:n0}", dtProcType.Rows[i]["M05Target"]);
                    row["M06"] = string.Format("{0:n0}", dtProcType.Rows[i]["M06Target"]);
                    row["M07"] = string.Format("{0:n0}", dtProcType.Rows[i]["M07Target"]);
                    row["M08"] = string.Format("{0:n0}", dtProcType.Rows[i]["M08Target"]);
                    row["M09"] = string.Format("{0:n0}", dtProcType.Rows[i]["M09Target"]);
                    row["M10"] = string.Format("{0:n0}", dtProcType.Rows[i]["M10Target"]);
                    row["M11"] = string.Format("{0:n0}", dtProcType.Rows[i]["M11Target"]);
                    row["M12"] = string.Format("{0:n0}", dtProcType.Rows[i]["M12Target"]);
                    row["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TotalTarget"]);
                    dtProcTypeGrid.Rows.Add(row);
                }

                for (int i = 0; i < dtProcTypePackage.Rows.Count; i++)
                {
                    //infragistics.Win.UltraWinGrid.UltraGridRow row = this.uGridMonthList.DisplayLayout.Bands[0].AddNew();
                    DataRow row = dtProcTypePackageGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row["GUBUN"] = strGubun[0];
                    row["M01"] = dtProcTypePackage.Rows[i]["M01SampleSize"];
                    row["M02"] = dtProcTypePackage.Rows[i]["M02SampleSize"];
                    row["M03"] = dtProcTypePackage.Rows[i]["M03SampleSize"];
                    row["M04"] = dtProcTypePackage.Rows[i]["M04SampleSize"];
                    row["M05"] = dtProcTypePackage.Rows[i]["M05SampleSize"];
                    row["M06"] = dtProcTypePackage.Rows[i]["M06SampleSize"];
                    row["M07"] = dtProcTypePackage.Rows[i]["M07SampleSize"];
                    row["M08"] = dtProcTypePackage.Rows[i]["M08SampleSize"];
                    row["M09"] = dtProcTypePackage.Rows[i]["M09SampleSize"];
                    row["M10"] = dtProcTypePackage.Rows[i]["M10SampleSize"];
                    row["M11"] = dtProcTypePackage.Rows[i]["M11SampleSize"];
                    row["M12"] = dtProcTypePackage.Rows[i]["M12SampleSize"];
                    row["Total"] = dtProcTypePackage.Rows[i]["TSampleSize"];
                    dtProcTypePackageGrid.Rows.Add(row);

                    row = dtProcTypePackageGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row["GUBUN"] = strGubun[1];
                    row["M01"] = dtProcTypePackage.Rows[i]["M01FaultQty"];
                    row["M02"] = dtProcTypePackage.Rows[i]["M02FaultQty"];
                    row["M03"] = dtProcTypePackage.Rows[i]["M03FaultQty"];
                    row["M04"] = dtProcTypePackage.Rows[i]["M04FaultQty"];
                    row["M05"] = dtProcTypePackage.Rows[i]["M05FaultQty"];
                    row["M06"] = dtProcTypePackage.Rows[i]["M06FaultQty"];
                    row["M07"] = dtProcTypePackage.Rows[i]["M07FaultQty"];
                    row["M08"] = dtProcTypePackage.Rows[i]["M08FaultQty"];
                    row["M09"] = dtProcTypePackage.Rows[i]["M09FaultQty"];
                    row["M10"] = dtProcTypePackage.Rows[i]["M10FaultQty"];
                    row["M11"] = dtProcTypePackage.Rows[i]["M11FaultQty"];
                    row["M12"] = dtProcTypePackage.Rows[i]["M12FaultQty"];
                    row["Total"] = dtProcTypePackage.Rows[i]["TFaultQty"];
                    dtProcTypePackageGrid.Rows.Add(row);

                    row = dtProcTypePackageGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row["GUBUN"] = strGubun[2];
                    row["M01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M01FaultRate"]);
                    row["M02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M02FaultRate"]);
                    row["M03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M03FaultRate"]);
                    row["M04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M04FaultRate"]);
                    row["M05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M05FaultRate"]);
                    row["M06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M06FaultRate"]);
                    row["M07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M07FaultRate"]);
                    row["M08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M08FaultRate"]);
                    row["M09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M09FaultRate"]);
                    row["M10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M10FaultRate"]);
                    row["M11"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M11FaultRate"]);
                    row["M12"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M12FaultRate"]);
                    row["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TFaultRate"]);
                    dtProcTypePackageGrid.Rows.Add(row);

                    row = dtProcTypePackageGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row["GUBUN"] = "Target";
                    row["M01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M01Target"]);
                    row["M02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M02Target"]);
                    row["M03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M03Target"]);
                    row["M04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M04Target"]);
                    row["M05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M05Target"]);
                    row["M06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M06Target"]);
                    row["M07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M07Target"]);
                    row["M08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M08Target"]);
                    row["M09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M09Target"]);
                    row["M10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M10Target"]);
                    row["M11"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M11Target"]);
                    row["M12"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["M12Target"]);
                    row["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TotalTarget"]);

                    dtProcTypePackageGrid.Rows.Add(row);
                }

                uGridProcTypeList.DataSource = dtProcTypeGrid;
                uGridProcTypeList.DataBind();

                uGridPackageList.DataSource = dtProcTypePackageGrid;
                uGridPackageList.DataBind();
                #endregion

                #region 색깔 변경
                for (int i = 0; i < uGridProcTypeList.Rows.Count; i++)
                {
                    if (uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strGubun[2])
                    {
                        for (int j = 1; j < uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridProcTypeList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridProcTypeList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    if (uGridProcTypeList.Rows[i].Cells["Gubun"].Text == "Target")
                    {
                        for (int j = 1; j < uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridProcTypeList.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                        }
                    }
                    this.uGridProcTypeList.Rows[i].Cells[uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.MistyRose;
                    this.uGridProcTypeList.Rows[i].Cells[uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                }

                for (int i = 0; i < uGridPackageList.Rows.Count; i++)
                {
                    if (uGridPackageList.Rows[i].Cells["Gubun"].Text == strGubun[2])
                    {
                        for (int j = 2; j < uGridPackageList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridPackageList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridPackageList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    if (uGridPackageList.Rows[i].Cells["Gubun"].Text == "Target")
                    {
                        for (int j = 2; j < uGridPackageList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridPackageList.Rows[i].Cells[j].Appearance.BackColor = Color.Lavender;
                        }
                    }
                    this.uGridPackageList.Rows[i].Cells[uGridPackageList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.MistyRose;
                    this.uGridPackageList.Rows[i].Cells[uGridPackageList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                }
                #endregion

                #region ChartTab 설정
                uTabChartInfo.Tabs.Clear();
                uTabChartInfo.Tabs.Add("ALL", msg.GetMessge_Text("M001498", strLang));
                for (int i = 0; i < dtProcType.Rows.Count; i++)
                {
                    uTabChartInfo.Tabs.Add(dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"].ToString(), dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"].ToString());
                }
                DrawChart();
                #endregion

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void DrawChart()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strLang = m_resSys.GetString("SYS_LANG");
                string[] strGubun = new string[2];
                strGubun[0] = msg.GetMessge_Text("M001499", strLang); //월
                strGubun[1] = msg.GetMessge_Text("M000603", strLang); //불량률

                System.Data.DataTable dtProcChart = new System.Data.DataTable("ProcChart");
                dtProcChart.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcChart.Columns.Add("1" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("2" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("3" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("4" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("5" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("6" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("7" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("8" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("9" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("10" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("11" + strGubun[0], typeof(double));
                dtProcChart.Columns.Add("12" + strGubun[0], typeof(double));

                //전체Tab을 선택한 경우 
                if (uTabChartInfo.SelectedTab.Key == "ALL")
                {
                    for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
                    {
                        if (this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strGubun[1])
                        {
                            DataRow row = dtProcChart.NewRow();
                            row["DETAILPROCESSOPERATIONTYPE"] = uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
                            row["1" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M01"].Text);
                            row["2" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M02"].Text);
                            row["3" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M03"].Text);
                            row["4" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M04"].Text);
                            row["5" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M05"].Text);
                            row["6" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M06"].Text);
                            row["7" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M07"].Text);
                            row["8" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M08"].Text);
                            row["9" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M09"].Text);
                            row["10" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M10"].Text);
                            row["11" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M11"].Text);
                            row["12" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M12"].Text);
                            dtProcChart.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    string strProcType = uTabChartInfo.SelectedTab.Key;
                    for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
                    {
                        if (this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text == strProcType && this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strGubun[1])
                        {
                            DataRow row = dtProcChart.NewRow();
                            row["DETAILPROCESSOPERATIONTYPE"] = uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
                            row["1" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M01"].Text);
                            row["2" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M02"].Text);
                            row["3" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M03"].Text);
                            row["4" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M04"].Text);
                            row["5" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M05"].Text);
                            row["6" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M06"].Text);
                            row["7" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M07"].Text);
                            row["8" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M08"].Text);
                            row["9" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M09"].Text);
                            row["10" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M10"].Text);
                            row["11" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M11"].Text);
                            row["12" + strGubun[0]] = Convert.ToDouble(uGridProcTypeList.Rows[i].Cells["M12"].Text);
                            dtProcChart.Rows.Add(row);
                        }
                    }
                }
                #region Chart그리기
                //챠트 유형 지정
                uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                uChart.AreaChart.NullHandling = Infragistics.UltraChart.Shared.Styles.NullHandling.DontPlot;

                //Line챠트의 Line 속성 지정
                Infragistics.UltraChart.Resources.Appearance.LineAppearance ValueLineApp = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                ValueLineApp.Thickness = 4;
                ValueLineApp.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                ValueLineApp.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.Square;
                //ValueLineApp.IconAppearance.PE.Fill = System.Drawing.Color.Black;
                ValueLineApp.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                ValueLineApp.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Auto;
                uChart.LineChart.LineAppearances.Add(ValueLineApp);

                //범례 유형 지정
                uChart.Legend.Visible = true;
                uChart.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Right;
                uChart.Legend.Margins.Left = 0;
                uChart.Legend.Margins.Right = 0;
                uChart.Legend.Margins.Top = 0;
                uChart.Legend.Margins.Bottom = 0;
                uChart.Legend.SpanPercentage = 10;

                
                uChart.TitleTop.Text = "";
                uChart.TitleTop.HorizontalAlign = StringAlignment.Center;
                uChart.TitleTop.VerticalAlign = StringAlignment.Center;
                uChart.TitleTop.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 12, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point);
                uChart.TitleTop.Margins.Left = 0;
                uChart.TitleTop.Margins.Right = 0;
                uChart.TitleTop.Margins.Top = 0;
                uChart.TitleTop.Margins.Bottom = 0;
                
                //X축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisX = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisX.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                axisX.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                axisX.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;   // 라인으로 그리는 경우 ContinuousData, 막대로 그리는 경우 GroupBySeries
                axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
                axisX.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
                axisX.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisX.Extent = 1;
                uChart.TitleBottom.Text = strGubun[0];
                uChart.TitleBottom.HorizontalAlign = StringAlignment.Center;
                uChart.TitleBottom.VerticalAlign = StringAlignment.Center;
                uChart.TitleBottom.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                uChart.TitleBottom.Margins.Left = 0;
                uChart.TitleBottom.Margins.Right = 0;
                uChart.TitleBottom.Margins.Top = 1;
                uChart.TitleBottom.Margins.Bottom = 1;
                uChart.TitleBottom.Visible = true;

                //Y축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisY = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisY.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                axisY.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                axisY.Labels.ItemFormatString = "<DATA_VALUE:0.#>";
                axisY.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisY.Extent = 5;
                uChart.TitleLeft.Text = strGubun[1]+"(PPM)";
                uChart.TitleLeft.HorizontalAlign = StringAlignment.Center;
                uChart.TitleLeft.VerticalAlign = StringAlignment.Center;
                uChart.TitleLeft.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                uChart.TitleLeft.Margins.Left = 5;
                uChart.TitleLeft.Margins.Right = 5;
                uChart.TitleLeft.Margins.Top = 0;
                uChart.TitleLeft.Margins.Bottom = 0;
                uChart.TitleLeft.Visible = true;
                uChart.LineChart.HighLightLines = true;
                //uChart.LineChart
                if (dtProcChart.Rows.Count > 0)
                {
                    uChart.DataSource = dtProcChart;
                    uChart.DataBind();
                }
                #endregion

            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }


        private void uGridProcTypeList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //공정Type을 더블클릭하면 Package별 Tab으로 이동하여 선택한 공정Type으로 Filtering하여 Package별로 조회하도록 한다.
                if (e.Cell.Column.Key == "DETAILPROCESSOPERATIONTYPE")
                {
                    uTabInfo.Tabs[1].Selected = true;
                    string strProcType = e.Cell.Value.ToString();

                    for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                    {
                        if (this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text != strProcType)
                            this.uGridPackageList.Rows[i].Hidden = true;
                        else
                            this.uGridPackageList.Rows[i].Hidden = false;
                    }

                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uTabChartInfo_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            DrawChart();
        }

        #region 엑셀관련 함수
        /// <summary>
        /// 지정된 Column Key를 Column 순번으로 변환 합니다.
        /// </summary>
        /// <param name="columnKey">Column Key</param>
        /// <returns>1부터 시작하는 Column key의 순번.</returns>
        private static int ConvertColumnKeyToIndex(string columnKey)
        {
            int index = 0;

            int x = columnKey.Length - 1;

            for (int i = 0; i < columnKey.Length; i++)
            {
                index += (Convert.ToInt32(columnKey[i]) - 64) * Convert.ToInt32(Math.Pow(26d, Convert.ToDouble(x--)));
            }

            return index;
        }

        /// <summary>
        /// 지정된 컬럼 순번을 Column key로 변환 합니다..
        /// </summary>
        /// <param name="columnIndex">1부터 시작하는 Column 순번.</param>
        /// <returns>Column Key</returns>
        private static string ConvertColumnIndexToKey(int columnIndex)
        {
            string key = string.Empty;

            for (int i = Convert.ToInt32(Math.Log(Convert.ToDouble(25d * (Convert.ToDouble(columnIndex) + 1d))) / Math.Log(26d)) - 1; i >= 0; i--)
            {
                int x = Convert.ToInt32(Math.Pow(26d, i + 1d) - 1d) / 25 - 1;
                if (columnIndex > x)
                {
                    key += (char)(((columnIndex - x - 1) / Convert.ToInt32(Math.Pow(26d, i))) % 26d + 65d);
                }
            }

            return key;
        }

        /// <summary>
        /// 새로운 CellAddress 개체 인스턴스를 Cell의 열과 행을 사용해 초기화 합니다.
        /// </summary>
        /// <param name="columnIndex">
        /// 1부터 시작하는 Column 순번.
        /// </param>
        /// <param name="rowIndex">
        /// 1부터 시작하는 Row 순번.
        /// </param>
        public string CellAddress(int intRowIndex, int intColumnIndex)
        {
            string strAddress = string.Empty;

            string strColumnKey = ConvertColumnIndexToKey(intColumnIndex);
            string strRowIndex = intRowIndex.ToString();
            strAddress = string.Format("{0}{1}", strColumnKey, strRowIndex);

            return strAddress;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null; MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion
    }
}
