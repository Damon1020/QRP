﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질종합현황                                          */
/* 프로그램ID   : frmSTA0072.cs                                         */
/* 프로그램명   : 일별품질 종합현황                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-02                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.Office.Interop.Excel;

namespace QRPSTA.UI
{
    public partial class frmSTA0072 : Form, QRPCOM.QRPGLO.IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0072()
        {
            InitializeComponent();
        }

        private void frmSTA0072_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("일별품질 종합현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitEtc();
            InitComboBox();

            QRPSTA.STASPC clsSTASPC = new STASPC();
            clsSTASPC.mfInitControlChart(uChart);
        }

        private void frmSTA0072_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmSTA0072_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                System.Data.DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchYear, "년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMonth, "월", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // BL 연결
                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                System.Data.DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);


                System.Collections.ArrayList arrKey = new System.Collections.ArrayList();
                System.Collections.ArrayList arrValue = new System.Collections.ArrayList();

                // 월 콤보박스
                for (int i = 1; i <= 12; i++)
                {
                    arrKey.Add(i.ToString("D2"));
                    if(m_resSys.GetString("SYS_LANG").Equals("CHN"))
                        arrValue.Add(i.ToString("D2") + "月");
                    else
                        arrValue.Add(i.ToString("D2") + "월");
                }

                wCombo.mfSetComboEditor(this.uComboSearchMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , DateTime.Now.Month.ToString("D2"), arrKey, arrValue);

                // 고객사
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                System.Data.DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "", "CustomerCode", "CustomerName", dtCustomer);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 기타 컨트롤 초기화

        private void InitEtc()
        {
            try
            {
                this.uTextSearchYear.Text = DateTime.Now.ToString("yyyy");
                this.uTextSearchYear.MaxLength = 4;

                this.uComboSearchPlantCode.Hide();
                this.uLabelSearchPlant.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                int intBandIndex = 0;

                #region 공정 Type별 그리드

                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                for (int i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "D"+i.ToString(), i.ToString()+"일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }

                wGrid.mfSetGridColumn(this.uGridProcTypeList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridProcTypeList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcTypeList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region Package 별 그리드

                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Package", "Package", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                for (int i = 11; i < 32; i++)
                {
                    wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }

                wGrid.mfSetGridColumn(this.uGridPackageList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridPackageList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPackageList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region IToolbar 멤버
        public void mfCreate()
        {
            
        }

        public void mfDelete()
        {
            
        }

        public void mfPrint()
        {
            
        }

        public void mfSave()
        {
            
        }

        //public void mfExcel()
        //{
        //    try
        //    {
        //        #region Var
        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinMessageBox msg = new WinMessageBox();

        //        int intStartRow = 2;        // 시작행

        //        int intStartCol = 3;        // 시작컬럼
        //        int intMonthDays = DateTime.DaysInMonth(Convert.ToInt32(this.uTextSearchYear.Text), Convert.ToInt32(this.uComboSearchMonth.Value)); // 달의 일수
        //        int intChartHeight = 17;    // 차트 하나가 차지하는 줄의 수

        //        int intGridRowCount = 0;    // 그리드 줄 수

        //        double dblheight = 5.6;     // 차트 위치 결정하기 위한 변수( 표와 차트사이의 공백높이)
        //        #endregion

        //        // MessageBox 출력
        //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                        "M001264", "M000048", "M001215", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
        //        {
        //            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        //            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excel.Workbooks.Add(true);
        //            Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Worksheets.get_Item(1);
        //            Microsoft.Office.Interop.Excel.Range range;

        //            #region 속성 설정
        //            excelWorksheet.Name = "Daily Quality Trend";
        //            //--전체 Sheet적용 속성--//
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlToRight);
        //            range = range.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlDown);
        //            range = excelWorksheet.get_Range("A1", range);
        //            range.Font.Size = 9;
        //            range.RowHeight = 11.25;

        //            //--컬럼 폭 조절--//
        //            //((Range)excelWorksheet.Columns["A:A"]).ColumnWidth = 8.8;
        //            range = excelWorksheet.get_Range("A1", Type.Missing);
        //            range.ColumnWidth = 0.85;
        //            range = excelWorksheet.get_Range("B1", Type.Missing);
        //            range.ColumnWidth = 0.38;
        //            range = excelWorksheet.get_Range("C1", Type.Missing);
        //            range.ColumnWidth = 5.63;
        //            range = excelWorksheet.get_Range("D1", Type.Missing);
        //            range.ColumnWidth = 5.38;
        //            // 일자부분

        //            for (int i = 0; i < intMonthDays; i++)
        //            {
        //                range = excelWorksheet.get_Range(CellAddress(1, i + 5), Type.Missing);
        //                range.ColumnWidth = 8.25;
        //            }
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonthDays + 5), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonthDays + 6), Type.Missing);
        //            range.ColumnWidth = 7.75;
        //            range = excelWorksheet.get_Range(CellAddress(1, intMonthDays + 7), Type.Missing);
        //            range.ColumnWidth = 2.5;
        //            #endregion

        //            #region Sub-Strate 에 대한 일별품질현황 처리
        //            //Sub-Strate로 조회한 후 엑셀로 변환한다.
        //            if (!this.uComboSearchProductActionType.Value.ToString().Equals("Sub"))
        //            {
        //                this.uComboSearchProductActionType.Value = "Sub";
        //                //mfSearch();
        //            }

        //            if (this.uGridProcTypeList.Rows.Count > 0)
        //            {
        //                //--제목--//
        //                excel.Cells[intStartRow, intStartCol] = "● Daily Quality Trend_Sub-Strate Pro Type";  //2,3
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow, intStartCol), CellAddress(intStartRow, intStartCol));
        //                range.Font.Size = 12;
        //                range.Font.Bold = true;
        //                range.RowHeight = 15.75;

        //                #region Header
        //                excel.Cells[intStartRow + 1, intStartCol] = "공정";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol), CellAddress(intStartRow + 3, intStartCol + 1));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                excel.Cells[intStartRow + 1, intStartCol + 2] = this.uComboSearchMonth.Value.ToString() + "월";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 2), CellAddress(intStartRow + 1, intStartCol + 1 + intMonthDays));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                for (int i = 1; i <= intMonthDays; i++)
        //                {
        //                    excel.Cells[intStartRow + 2, intStartCol + 1 + i] = i.ToString() + "일";
        //                    range = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 1 + i), CellAddress(intStartRow + 2, intStartCol + 1 + i));
        //                    range.MergeCells = false;
        //                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
        //                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //                }
        //                #endregion

        //                #region Write Data in Cell
        //                //--Data --//
        //                for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 4 == 1)
        //                        excel.Cells[intStartRow + 4 + i, intStartCol] = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;

        //                    excel.Cells[intStartRow + 4 + i, intStartCol + 1] = this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text;

        //                    for (int j = 1; j <= intMonthDays; j++)
        //                    {
        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 1 + j] = this.uGridProcTypeList.Rows[i].Cells["D" + j.ToString("D2")].Text;
        //                    }

        //                    //공정Type Merge 처리
        //                    if ((i + 1) % 4 == 0)
        //                    {
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 1 + i), intStartCol), CellAddress((intStartRow + 4 + i), intStartCol));
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;

        //                        // 불량율영역 배경색 지정

        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 4 + i), intStartCol + 1), CellAddress((intStartRow + 4 + i), intStartCol + 1 + intMonthDays));
        //                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(204, 255, 255));
        //                    }
        //                }
        //                intGridRowCount = this.uGridProcTypeList.Rows.Count;
        //                #endregion

        //                //--Line그리기--//
        //                //행별로 선그리기
        //                range = excelWorksheet.get_Range(CellAddress((intStartRow + 1), intStartCol), CellAddress(intStartRow + 2 + intGridRowCount + 1, intStartCol + 1 + intMonthDays));
        //                range.Borders.LineStyle = BorderStyle.FixedSingle;

        //                #region 取消图表
        //                /*

        //                #region Chart
        //                //--전체 Chart그리기--//
        //                double dblLeft = 15;
        //                double dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + dblheight + 15.75;
        //                Microsoft.Office.Interop.Excel.ChartObjects chartObjects = (Microsoft.Office.Interop.Excel.ChartObjects)excelWorksheet.ChartObjects(Type.Missing);
        //                Microsoft.Office.Interop.Excel.ChartObject dchartObject = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 320, 180);
        //                Microsoft.Office.Interop.Excel.Chart chart = dchartObject.Chart;

        //                //범례지정

        //                Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)chart.SeriesCollection(Type.Missing);
        //                for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 3 == 0)
        //                    {
        //                        Microsoft.Office.Interop.Excel.Series ser1 = seriesCollection.NewSeries();
        //                        ser1.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 3 + i, intStartCol + 2), CellAddress(intStartRow + 3 + i, intStartCol + 2 + intMonthDays));
        //                        ser1.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 2), CellAddress(intStartRow + 2, intStartCol + 1 + intMonthDays));
        //                        ser1.Name = this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                    }
        //                }
        //                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                chart.Legend.Font.Size = 9;
        //                chart.Legend.Font.Name = "맑은 고딕";
        //                chart.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                //Chart유형 지정

        //                Microsoft.Office.Interop.Excel.Chart chartPage = chart;
        //                chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                // Chart Title 사용하지 않함
        //                //chartPage.ChartTitle.Delete();

        //                //Chart Style 지정

        //                Microsoft.Office.Interop.Excel.Axis x = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                Microsoft.Office.Interop.Excel.Axis y = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                x.TickLabels.Font.Size = 7;
        //                x.TickLabels.Font.Bold = false;
        //                x.TickLabels.Font.Name = "맑은 고딕";
        //                y.TickLabels.Font.Size = 7;
        //                y.TickLabels.Font.Bold = false;
        //                y.TickLabels.Font.Name = "맑은 고딕";

        //                // Chart 내부색 지정

        //                chart.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                // Chart 외부색 지정

        //                x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                //--공정Type별 Chart그리기--//
        //                for (int i = 1; i < uTabChartInfo.Tabs.Count; i++)
        //                {
        //                    for (int j = 0; j < uGridProcTypeList.Rows.Count; j++)
        //                    {
        //                        if (uTabChartInfo.Tabs[i].Key == uGridProcTypeList.Rows[j].Cells["DETAILPROCESSOPERATIONTYPE"].Text)
        //                        {
        //                            dblLeft = 15 + 320 * (i % 4);
        //                            dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + 180 * (i / 4) + dblheight + 15.75;

        //                            Microsoft.Office.Interop.Excel.ChartObject dchartObjectPT = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 320, 180);
        //                            Microsoft.Office.Interop.Excel.Chart chartPT = dchartObjectPT.Chart;

        //                            //범례지정

        //                            Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionPT = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPT.SeriesCollection(Type.Missing);
        //                            Microsoft.Office.Interop.Excel.Series ser2 = seriesCollectionPT.NewSeries();
        //                            ser2.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 5 + j, intStartCol + 2), CellAddress(intStartRow + 5 + j, intStartCol + 1 + intMonthDays));
        //                            ser2.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 2), CellAddress(intStartRow + 2, intStartCol + 1 + intMonthDays));
        //                            ser2.Name = uTabChartInfo.Tabs[i].Key;
        //                            chartPT.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                            chartPT.Legend.Font.Size = 9;
        //                            chartPT.Legend.Font.Name = "맑은 고딕";
        //                            chartPT.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                            //Chart유형 지정

        //                            Microsoft.Office.Interop.Excel.Chart chartPagePT = chartPT;
        //                            chartPagePT.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                            // Chart Title 사용하지 않함
        //                            chartPagePT.ChartTitle.Delete();

        //                            //Chart Style 지정

        //                            x = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            y = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            x.TickLabels.Font.Size = 7;
        //                            x.TickLabels.Font.Bold = false;
        //                            x.TickLabels.Font.Name = "맑은 고딕";
        //                            y.TickLabels.Font.Size = 7;
        //                            y.TickLabels.Font.Bold = false;
        //                            y.TickLabels.Font.Name = "맑은 고딕";

        //                            // Chart 내부색 지정

        //                            chartPT.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                            // Chart 외부색 지정

        //                            x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                            y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                            break;
        //                        }
        //                    }
        //                }
        //                #endregion

        //                */
        //                #endregion
        //            }
        //            #endregion

        //            // Lead-Frame 일별 품질일보 시작점 설정
        //            // 그리드줄수 + 차트가 차지하는 줄수 + Sub차트 헤더줄수(2)
        //            intStartRow += intGridRowCount + (((intGridRowCount + 2) / 12) + 1) * intChartHeight + 2;

        //            #region Lead-Frame에 대한 일별품질현황 처리
        //            // Lead-Frame으로 조회
        //            if (!this.uComboSearchProductActionType.Value.ToString().Equals("Lead Frame"))
        //            {
        //                this.uComboSearchProductActionType.Value = "Lead Frame";
        //                //mfSearch();
        //            }

        //            if (this.uGridPackageList.Rows.Count > 0)
        //            {
        //                //--제목--//
        //                excel.Cells[intStartRow, intStartCol] = "● Daily Quality Trend_Lead-Frame PKG";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow, intStartCol), CellAddress(intStartRow, intStartCol));
        //                range.Font.Size = 12;
        //                range.Font.Bold = true;
        //                range.RowHeight = 15.75;

        //                #region Header
        //                excel.Cells[intStartRow + 1, intStartCol] = "공정";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol), CellAddress(intStartRow + 2, intStartCol + 2));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                excel.Cells[intStartRow + 1, intStartCol + 3] = this.uComboSearchMonth.Value.ToString() + "월";
        //                range = excelWorksheet.get_Range(CellAddress(intStartRow + 1, intStartCol + 3), CellAddress(intStartRow + 1, intStartCol + 2 + intMonthDays));
        //                range.MergeCells = true;
        //                range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));

        //                for (int i = 1; i <= intMonthDays; i++)
        //                {
        //                    excel.Cells[intStartRow + 2, intStartCol + 2 + i] = i.ToString() + "일";
        //                    range = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 2 + i), CellAddress(intStartRow + 2, intStartCol + 2 + i));
        //                    range.MergeCells = false;
        //                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
        //                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 255, 153));
        //                }
        //                #endregion

        //                #region Write Data in Cell
        //                //--Data --//
        //                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 4 == 1)
        //                        excel.Cells[intStartRow + 4 + i, intStartCol] = this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;

        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 1] = this.uGridPackageList.Rows[i].Cells["Package"].Text;

        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 2] = this.uGridPackageList.Rows[i].Cells["Gubun"].Text;

        //                    for (int j = 1; j <= intMonthDays; j++)
        //                    {
        //                        excel.Cells[intStartRow + 4 + i, intStartCol + 2 + j] = this.uGridPackageList.Rows[i].Cells["D" + j.ToString("D2")].Text;
        //                    }

        //                    //공정Type Merge 처리
        //                    if ((i + 1) % 4 == 0)
        //                    {
        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 1 + i), intStartCol), CellAddress((intStartRow + 4 + i), intStartCol));
        //                        range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //                        range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
        //                        range.MergeCells = true;

        //                        // 불량율영역 배경색 지정

        //                        range = excelWorksheet.get_Range(CellAddress((intStartRow + 4 + i), intStartCol + 2), CellAddress((intStartRow + 4 + i), intStartCol + 2 + intMonthDays));
        //                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(204, 255, 255));
        //                    }
        //                }
        //                intGridRowCount = this.uGridPackageList.Rows.Count;
        //                #endregion

        //                //--Line그리기--//
        //                //행별로 선그리기
        //                range = excelWorksheet.get_Range(CellAddress((intStartRow + 1), intStartCol), CellAddress(intStartRow + 2 + intGridRowCount + 1, intStartCol + 2 + intMonthDays));
        //                range.Borders.LineStyle = BorderStyle.FixedSingle;

        //                #region 取消掉图标
        //                /*

        //                #region Chart
        //                //--전체 Chart그리기--//
        //                double dblLeft = 15;
        //                double dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + dblheight * 2 + 15.75;
        //                Microsoft.Office.Interop.Excel.ChartObjects chartObjects = (Microsoft.Office.Interop.Excel.ChartObjects)excelWorksheet.ChartObjects(Type.Missing);
        //                Microsoft.Office.Interop.Excel.ChartObject dchartObject = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 320, 180);
        //                Microsoft.Office.Interop.Excel.Chart chart = dchartObject.Chart;

        //                //범례지정

        //                Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)chart.SeriesCollection(Type.Missing);
        //                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
        //                {
        //                    if ((i + 1) % 3 == 0)
        //                    {
        //                        Microsoft.Office.Interop.Excel.Series ser1 = seriesCollection.NewSeries();
        //                        ser1.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 3 + i, intStartCol + 3), CellAddress(intStartRow + 3 + i, intStartCol + 3 + intMonthDays));
        //                        ser1.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 3), CellAddress(intStartRow + 2, intStartCol + 2 + intMonthDays));
        //                        ser1.Name = this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
        //                    }
        //                }
        //                chart.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                chart.Legend.Font.Size = 9;
        //                chart.Legend.Font.Name = "맑은 고딕";
        //                chart.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                //Chart유형 지정

        //                Microsoft.Office.Interop.Excel.Chart chartPage = chart;
        //                chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                // Chart Title 사용하지 않함
        //                //chartPage.ChartTitle.Delete();

        //                //Chart Style 지정

        //                Microsoft.Office.Interop.Excel.Axis x = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                Microsoft.Office.Interop.Excel.Axis y = (Microsoft.Office.Interop.Excel.Axis)chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                x.TickLabels.Font.Size = 7;
        //                x.TickLabels.Font.Bold = false;
        //                x.TickLabels.Font.Name = "맑은 고딕";
        //                y.TickLabels.Font.Size = 7;
        //                y.TickLabels.Font.Bold = false;
        //                y.TickLabels.Font.Name = "맑은 고딕";

        //                // Chart 내부색 지정

        //                chart.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                // Chart 외부색 지정

        //                x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                //--공정Type별 Chart그리기--//
        //                for (int i = 1; i < uTabChartInfo.Tabs.Count; i++)
        //                {
        //                    for (int j = 0; j < uGridPackageList.Rows.Count; j++)
        //                    {
        //                        if (uTabChartInfo.Tabs[i].Key == uGridPackageList.Rows[j].Cells["DETAILPROCESSOPERATIONTYPE"].Text)
        //                        {
        //                            dblLeft = 15 + 320 * (i % 4);
        //                            dblTop = (intGridRowCount + intStartRow + 1) * 11.25 + 180 * (i / 4) + dblheight * 2 + 15.75;

        //                            Microsoft.Office.Interop.Excel.ChartObject dchartObjectPT = (Microsoft.Office.Interop.Excel.ChartObject)chartObjects.Add(dblLeft, dblTop, 320, 180);
        //                            Microsoft.Office.Interop.Excel.Chart chartPT = dchartObjectPT.Chart;

        //                            //범례지정

        //                            Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionPT = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPT.SeriesCollection(Type.Missing);
        //                            Microsoft.Office.Interop.Excel.Series ser2 = seriesCollectionPT.NewSeries();
        //                            ser2.Values = excelWorksheet.get_Range(CellAddress(intStartRow + 5 + j, intStartCol + 3), CellAddress(intStartRow + 5 + j, intStartCol + 2 + intMonthDays));
        //                            ser2.XValues = excelWorksheet.get_Range(CellAddress(intStartRow + 2, intStartCol + 3), CellAddress(intStartRow + 2, intStartCol + 2 + intMonthDays));
        //                            ser2.Name = uTabChartInfo.Tabs[i].Key;
        //                            chartPT.Legend.Position = XlLegendPosition.xlLegendPositionTop;
        //                            chartPT.Legend.Font.Size = 9;
        //                            chartPT.Legend.Font.Name = "맑은 고딕";
        //                            chartPT.Legend.Border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlLineStyleNone;

        //                            //Chart유형 지정

        //                            Microsoft.Office.Interop.Excel.Chart chartPagePT = chartPT;
        //                            chartPagePT.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //                            // Chart Title 사용하지 않함
        //                            chartPagePT.ChartTitle.Delete();

        //                            //Chart Style 지정

        //                            x = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            y = (Microsoft.Office.Interop.Excel.Axis)chartPT.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        //                            x.TickLabels.Font.Size = 7;
        //                            x.TickLabels.Font.Bold = false;
        //                            x.TickLabels.Font.Name = "맑은 고딕";
        //                            y.TickLabels.Font.Size = 7;
        //                            y.TickLabels.Font.Bold = false;
        //                            y.TickLabels.Font.Name = "맑은 고딕";

        //                            // Chart 내부색 지정

        //                            chartPT.PlotArea.Interior.Color = ColorTranslator.ToWin32(Color.White);
        //                            // Chart 외부색 지정

        //                            x.Border.Color = ColorTranslator.ToWin32(Color.Black);
        //                            y.Border.Color = ColorTranslator.ToWin32(Color.Black);

        //                            break;
        //                        }
        //                    }
        //                }
        //                #endregion

        //                */
        //                #endregion
        //            }
        //            #endregion

        //            // 테두리선 그리기

        //            // 마지막줄수를 구한다

        //            intStartRow += intGridRowCount + (((intGridRowCount + 2) / 12) + 1) * intChartHeight + 2;
        //            range = excelWorksheet.get_Range(CellAddress(2, 2), CellAddress(intStartRow, intStartCol + intMonthDays + 3));
        //            range.BorderAround(Type.Missing, XlBorderWeight.xlThick, XlColorIndex.xlColorIndexAutomatic, Type.Missing);

        //            #region 엑셀파일 저장

        //            excelWorksheet.Application.ActiveWindow.Zoom = 65;

        //            string strExePath = System.Windows.Forms.Application.ExecutablePath;
        //            int intPos = strExePath.LastIndexOf(@"\");
        //            strExePath = strExePath.Substring(0, intPos + 1) + "QRPKPI\\";

        //            //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
        //            if (!System.IO.Directory.Exists(strExePath))
        //                System.IO.Directory.CreateDirectory(strExePath);

        //            excelWorkbook.SaveAs(strExePath + uTextSearchYear.Text + "_" + this.uComboSearchMonth.Value.ToString() + "_Daily Quality Trend.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing,
        //                                 Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
        //                                 Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //            excel.Visible = true;
        //            //excelWorkbook.Close(true, Type.Missing, Type.Missing);
        //            //app.Quit();

        //            // 가비지 콜렉션

        //            releaseObject(excel);
        //            #endregion
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {

        //    }
        //}

        public void mfExcel()
        {
            try
            {
                WinGrid wGrid = new WinGrid();

                if (this.uTabInfo.ActiveTab.Key == "Process" && this.uGridProcTypeList.Rows.Count > 0)
                {

                    wGrid.mfDownLoadGridToExcel(this.uGridProcTypeList);
                }
                else if (this.uTabInfo.ActiveTab.Key == "Package" && this.uGridPackageList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridPackageList);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 필수조건 확인
                if (string.IsNullOrEmpty(this.uComboSearchPlantCode.Value.ToString()))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);
                    
                    this.uComboSearchPlantCode.DropDown();
                    return;
                }
                else if (this.uTextSearchYear.Text.Length < 4)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001107", Infragistics.Win.HAlign.Center);
                    
                    this.uTextSearchYear.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(this.uComboSearchMonth.Value.ToString()))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000363", Infragistics.Win.HAlign.Center);
                    
                    this.uComboSearchMonth.DropDown();
                    return;
                }

                // 매개변수

                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                string strYear = this.uTextSearchYear.Text;
                string strMonth = this.uComboSearchMonth.Value.ToString();
                string strProductActionType = this.uComboSearchProductActionType.Value.ToString();
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.INSProcessReport), "INSProcessReport");
                QRPSTA.BL.STAPRC.INSProcessReport clsReport = new QRPSTA.BL.STAPRC.INSProcessReport();
                brwChannel.mfCredentials(clsReport);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 그리드 컬럼 변경 메소드 호출
                //ChangeGridColumn(0);

                int intMontDay = DateTime.DaysInMonth(Convert.ToInt32(this.uTextSearchYear.Text), Convert.ToInt32(this.uComboSearchMonth.Value));
                if (intMontDay == 31)
                {
                    this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["D31"].Hidden = false;
                    this.uGridPackageList.DisplayLayout.Bands[0].Columns["D31"].Hidden = false;
                }
                else
                {
                    this.uGridProcTypeList.DisplayLayout.Bands[0].Columns["D31"].Hidden = true;
                    this.uGridPackageList.DisplayLayout.Bands[0].Columns["D31"].Hidden = true;
                }

                //조회
                System.Data.DataTable dtProcType = new System.Data.DataTable();
                System.Data.DataTable dtProcTypePackage = new System.Data.DataTable();

                ////dtProcType = clsReport.mfReadINSProcInspect_frmSTA0072_D1(strPlantCode, strYear, strMonth, strProductActionType);
                ////dtProcTypePackage = clsReport.mfReadINSProcInspect_frmSTA0072_D2(strPlantCode, strYear, strMonth, strProductActionType);
                // PSTS
                dtProcType = clsReport.mfReadINSProcInspect_frmSTA0072_D1_PSTS(strPlantCode, strYear, strMonth, strProductActionType, strCustomerCode);
                dtProcTypePackage = clsReport.mfReadINSProcInspect_frmSTA0072_D2_PSTS(strPlantCode, strYear, strMonth, strProductActionType, strCustomerCode);

                dtProcType.TableName = "ProcType";
                dtProcTypePackage.TableName = "ProcTypePackage";

                #region Grid에 뿌려줄 DataTable 형태로 조정
                int intMohtDay = 31;// DateTime.DaysInMonth(Convert.ToInt32(strYear), Convert.ToInt32(strMonth));

                System.Data.DataTable dtProcTypeGrid = new System.Data.DataTable("ProcType");
                DataColumn[] dcPKProcTypeGrid = new DataColumn[2];
                dtProcTypeGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcTypeGrid.Columns.Add("Gubun", typeof(string));
                dtProcTypeGrid.Columns.Add("D01", typeof(double));
                dtProcTypeGrid.Columns.Add("D02", typeof(double));
                dtProcTypeGrid.Columns.Add("D03", typeof(double));
                dtProcTypeGrid.Columns.Add("D04", typeof(double));
                dtProcTypeGrid.Columns.Add("D05", typeof(double));
                dtProcTypeGrid.Columns.Add("D06", typeof(double));
                dtProcTypeGrid.Columns.Add("D07", typeof(double));
                dtProcTypeGrid.Columns.Add("D08", typeof(double));
                dtProcTypeGrid.Columns.Add("D09", typeof(double));
                dtProcTypeGrid.Columns.Add("D10", typeof(double));
                for (int i = 11; i <= intMohtDay; i++)
                {
                    dtProcTypeGrid.Columns.Add("D" + i.ToString(), typeof(double));
                }
                dtProcTypeGrid.Columns.Add("Total", typeof(double));
                dcPKProcTypeGrid[0] = dtProcTypeGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcPKProcTypeGrid[1] = dtProcTypeGrid.Columns["Gubun"];
                dtProcTypeGrid.PrimaryKey = dcPKProcTypeGrid;

                System.Data.DataTable dtProcTypePackageGrid = new System.Data.DataTable("ProcTypePackage");
                DataColumn[] dcPKProcTypePackageGrid = new DataColumn[3];
                dtProcTypePackageGrid.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                dtProcTypePackageGrid.Columns.Add("Package", typeof(string));
                dtProcTypePackageGrid.Columns.Add("Gubun", typeof(string));
                dtProcTypePackageGrid.Columns.Add("D01", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D02", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D03", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D04", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D05", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D06", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D07", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D08", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D09", typeof(double));
                dtProcTypePackageGrid.Columns.Add("D10", typeof(double));
                for (int i = 11; i <= intMohtDay; i++)
                {
                    dtProcTypePackageGrid.Columns.Add("D" + i.ToString(), typeof(double));
                }
                dtProcTypePackageGrid.Columns.Add("Total", typeof(double));
                dcPKProcTypePackageGrid[0] = dtProcTypePackageGrid.Columns["DETAILPROCESSOPERATIONTYPE"];
                dcPKProcTypePackageGrid[1] = dtProcTypePackageGrid.Columns["Package"];
                dcPKProcTypePackageGrid[2] = dtProcTypePackageGrid.Columns["Gubun"];
                dtProcTypePackageGrid.PrimaryKey = dcPKProcTypePackageGrid;

                //////////////////////////////////////////////////
                string strLang = m_resSys.GetString("SYS_LANG");
                string[] strGubun = new string[3];
                strGubun[0] = msg.GetMessge_Text("M000772", strLang); //시료수
                strGubun[1] = msg.GetMessge_Text("M000607", strLang); //불량수
                strGubun[2] = msg.GetMessge_Text("M000603", strLang); //불량률
                /////////////////////////////////////////////////

                for (int i = 0; i < dtProcType.Rows.Count; i++)
                {
                    //infragistics.Win.UltraWinGrid.UltraGridRow row = this.uGridMonthList.DisplayLayout.Bands[0].AddNew();
                    DataRow row = dtProcTypeGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["GUBUN"] = strGubun[0];
                    row["D01"] = string.Format("{0:n0}", dtProcType.Rows[i]["D01SampleSize"]);
                    row["D02"] = string.Format("{0:n0}", dtProcType.Rows[i]["D02SampleSize"]);
                    row["D03"] = string.Format("{0:n0}", dtProcType.Rows[i]["D03SampleSize"]);
                    row["D04"] = string.Format("{0:n0}", dtProcType.Rows[i]["D04SampleSize"]);
                    row["D05"] = string.Format("{0:n0}", dtProcType.Rows[i]["D05SampleSize"]);
                    row["D06"] = string.Format("{0:n0}", dtProcType.Rows[i]["D06SampleSize"]);
                    row["D07"] = string.Format("{0:n0}", dtProcType.Rows[i]["D07SampleSize"]);
                    row["D08"] = string.Format("{0:n0}", dtProcType.Rows[i]["D08SampleSize"]);
                    row["D09"] = string.Format("{0:n0}", dtProcType.Rows[i]["D09SampleSize"]);
                    row["D10"] = string.Format("{0:n0}", dtProcType.Rows[i]["D10SampleSize"]);
                    //for (int j = 11; j < 32; j++)
                    //{
                    //    row["D"+j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D"+j.ToString()+"SampleSize"]);
                    //}
                    row["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TSampleSize"]);
                    //dtProcTypeGrid.Rows.Add(row);

                    DataRow row1 = dtProcTypeGrid.NewRow();
                    row1["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row1["GUBUN"] = strGubun[1];
                    row1["D01"] = string.Format("{0:n0}", dtProcType.Rows[i]["D01FaultQty"]);
                    row1["D02"] = string.Format("{0:n0}", dtProcType.Rows[i]["D02FaultQty"]);
                    row1["D03"] = string.Format("{0:n0}", dtProcType.Rows[i]["D03FaultQty"]);
                    row1["D04"] = string.Format("{0:n0}", dtProcType.Rows[i]["D04FaultQty"]);
                    row1["D05"] = string.Format("{0:n0}", dtProcType.Rows[i]["D05FaultQty"]);
                    row1["D06"] = string.Format("{0:n0}", dtProcType.Rows[i]["D06FaultQty"]);
                    row1["D07"] = string.Format("{0:n0}", dtProcType.Rows[i]["D07FaultQty"]);
                    row1["D08"] = string.Format("{0:n0}", dtProcType.Rows[i]["D08FaultQty"]);
                    row1["D09"] = string.Format("{0:n0}", dtProcType.Rows[i]["D09FaultQty"]);
                    row1["D10"] = string.Format("{0:n0}", dtProcType.Rows[i]["D10FaultQty"]);
                    //for (int j = 11; j < 32; j++)
                    //{
                    //    row1["D" + j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D" + j.ToString() + "FaultQty"]);
                    //}
                    row1["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TFaultQty"]);
                    //dtProcTypeGrid.Rows.Add(row1);

                    DataRow row2 = dtProcTypeGrid.NewRow();
                    row2["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row2["GUBUN"] = strGubun[2];
                    row2["D01"] = string.Format("{0:n0}", dtProcType.Rows[i]["D01FaultRate"]);
                    row2["D02"] = string.Format("{0:n0}", dtProcType.Rows[i]["D02FaultRate"]);
                    row2["D03"] = string.Format("{0:n0}", dtProcType.Rows[i]["D03FaultRate"]);
                    row2["D04"] = string.Format("{0:n0}", dtProcType.Rows[i]["D04FaultRate"]);
                    row2["D05"] = string.Format("{0:n0}", dtProcType.Rows[i]["D05FaultRate"]);
                    row2["D06"] = string.Format("{0:n0}", dtProcType.Rows[i]["D06FaultRate"]);
                    row2["D07"] = string.Format("{0:n0}", dtProcType.Rows[i]["D07FaultRate"]);
                    row2["D08"] = string.Format("{0:n0}", dtProcType.Rows[i]["D08FaultRate"]);
                    row2["D09"] = string.Format("{0:n0}", dtProcType.Rows[i]["D09FaultRate"]);
                    row2["D10"] = string.Format("{0:n0}", dtProcType.Rows[i]["D10FaultRate"]);
                    row2["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TFaultRate"]);

                    DataRow row3 = dtProcTypeGrid.NewRow();
                    row3["DETAILPROCESSOPERATIONTYPE"] = dtProcType.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row3["GUBUN"] = "Target";
                    row3["D01"] = string.Format("{0:n0}", dtProcType.Rows[i]["D01Target"]);
                    row3["D02"] = string.Format("{0:n0}", dtProcType.Rows[i]["D02Target"]);
                    row3["D03"] = string.Format("{0:n0}", dtProcType.Rows[i]["D03Target"]);
                    row3["D04"] = string.Format("{0:n0}", dtProcType.Rows[i]["D04Target"]);
                    row3["D05"] = string.Format("{0:n0}", dtProcType.Rows[i]["D05Target"]);
                    row3["D06"] = string.Format("{0:n0}", dtProcType.Rows[i]["D06Target"]);
                    row3["D07"] = string.Format("{0:n0}", dtProcType.Rows[i]["D07Target"]);
                    row3["D08"] = string.Format("{0:n0}", dtProcType.Rows[i]["D08Target"]);
                    row3["D09"] = string.Format("{0:n0}", dtProcType.Rows[i]["D09Target"]);
                    row3["D10"] = string.Format("{0:n0}", dtProcType.Rows[i]["D10Target"]);
                    row3["Total"] = string.Format("{0:n0}", dtProcType.Rows[i]["TotalTarget"]);

                    for (int j = 11; j <= intMohtDay; j++)
                    {
                        row["D" + j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D" + j.ToString() + "SampleSize"]);
                        row1["D" + j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D" + j.ToString() + "FaultQty"]);
                        row2["D" + j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D" + j.ToString() + "FaultRate"]);
                        row3["D" + j.ToString()] = string.Format("{0:n0}", dtProcType.Rows[i]["D" + j.ToString() + "Target"]);
                    }
                    
                    //dtProcTypeGrid.Rows.Add(row2);

                    dtProcTypeGrid.Rows.Add(row);
                    dtProcTypeGrid.Rows.Add(row1);
                    dtProcTypeGrid.Rows.Add(row2);
                    dtProcTypeGrid.Rows.Add(row3);
                }

                for (int i = 0; i < dtProcTypePackage.Rows.Count; i++)
                {
                    //infragistics.Win.UltraWinGrid.UltraGridRow row = this.uGridMonthList.DisplayLayout.Bands[0].AddNew();
                    DataRow row = dtProcTypePackageGrid.NewRow();
                    row["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row["GUBUN"] = strGubun[0];
                    row["D01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D01SampleSize"]);
                    row["D02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D02SampleSize"]);
                    row["D03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D03SampleSize"]);
                    row["D04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D04SampleSize"]);
                    row["D05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D05SampleSize"]);
                    row["D06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D06SampleSize"]);
                    row["D07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D07SampleSize"]);
                    row["D08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D08SampleSize"]);
                    row["D09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D09SampleSize"]);
                    row["D10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D10SampleSize"]);
                    //for (int j = 11; j < 32; j++)
                    //{
                    //    row["D"+j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D"+j.ToString()+"SampleSize"]);
                    //}
                    row["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TSampleSize"]);
                    //dtProcTypePackage.Rows.Add(row);

                    DataRow row1 = dtProcTypePackageGrid.NewRow();
                    row1["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row1["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row1["GUBUN"] = strGubun[1];
                    row1["D01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D01FaultQty"]);
                    row1["D02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D02FaultQty"]);
                    row1["D03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D03FaultQty"]);
                    row1["D04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D04FaultQty"]);
                    row1["D05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D05FaultQty"]);
                    row1["D06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D06FaultQty"]);
                    row1["D07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D07FaultQty"]);
                    row1["D08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D08FaultQty"]);
                    row1["D09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D09FaultQty"]);
                    row1["D10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D10FaultQty"]);
                    //for (int j = 11; j < 32; j++)
                    //{
                    //    row1["D" + j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D" + j.ToString() + "FaultQty"]);
                    //}
                    row1["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TFaultQty"]);
                    //dtProcTypePackage.Rows.Add(row1);

                    DataRow row2 = dtProcTypePackageGrid.NewRow();
                    row2["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row2["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row2["GUBUN"] = strGubun[2];
                    row2["D01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D01FaultRate"]);
                    row2["D02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D02FaultRate"]);
                    row2["D03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D03FaultRate"]);
                    row2["D04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D04FaultRate"]);
                    row2["D05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D05FaultRate"]);
                    row2["D06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D06FaultRate"]);
                    row2["D07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D07FaultRate"]);
                    row2["D08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D08FaultRate"]);
                    row2["D09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D09FaultRate"]);
                    row2["D10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D10FaultRate"]);
                    row2["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TFaultRate"]);

                    DataRow row3 = dtProcTypePackageGrid.NewRow();
                    row3["DETAILPROCESSOPERATIONTYPE"] = dtProcTypePackage.Rows[i]["DETAILPROCESSOPERATIONTYPE"];
                    row3["Package"] = dtProcTypePackage.Rows[i]["Package"];
                    row3["GUBUN"] = "Target";
                    row3["D01"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D01Target"]);
                    row3["D02"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D02Target"]);
                    row3["D03"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D03Target"]);
                    row3["D04"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D04Target"]);
                    row3["D05"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D05Target"]);
                    row3["D06"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D06Target"]);
                    row3["D07"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D07Target"]);
                    row3["D08"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D08Target"]);
                    row3["D09"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D09Target"]);
                    row3["D10"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D10Target"]);
                    row3["Total"] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["TotalTarget"]);

                    for (int j = 11; j <= intMohtDay; j++)
                    {
                        row["D" + j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D" + j.ToString() + "SampleSize"]);
                        row1["D" + j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D" + j.ToString() + "FaultQty"]);
                        row2["D" + j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D" + j.ToString() + "FaultRate"]);
                        row3["D" + j.ToString()] = string.Format("{0:n0}", dtProcTypePackage.Rows[i]["D" + j.ToString() + "Target"]);
                    }
                    
                    //dtProcTypePackage.Rows.Add(row2);

                    dtProcTypePackageGrid.Rows.Add(row);
                    dtProcTypePackageGrid.Rows.Add(row1);
                    dtProcTypePackageGrid.Rows.Add(row2);
                    dtProcTypePackageGrid.Rows.Add(row3);
                }

                uGridProcTypeList.DataSource = dtProcTypeGrid;
                uGridProcTypeList.DataBind();

                uGridPackageList.DataSource = dtProcTypePackageGrid;
                uGridPackageList.DataBind();
                #endregion

                #region 색깔 변경

                for (int i = 0; i < uGridProcTypeList.Rows.Count; i++)
                {
                    if (uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strGubun[2])
                    {
                        for (int j = 1; j < uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridProcTypeList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridProcTypeList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    this.uGridProcTypeList.Rows[i].Cells[uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.MistyRose;
                    this.uGridProcTypeList.Rows[i].Cells[uGridProcTypeList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                }

                for (int i = 0; i < uGridPackageList.Rows.Count; i++)
                {
                    if (uGridPackageList.Rows[i].Cells["Gubun"].Text == strGubun[2])
                    {
                        for (int j = 2; j < uGridPackageList.DisplayLayout.Bands[0].Columns.Count; j++)
                        {
                            this.uGridPackageList.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                            this.uGridPackageList.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                    this.uGridPackageList.Rows[i].Cells[uGridPackageList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.BackColor = Color.MistyRose;
                    this.uGridPackageList.Rows[i].Cells[uGridPackageList.DisplayLayout.Bands[0].Columns.Count - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                }
                #endregion

                #region ChartTab 설정
                uTabChartInfo.Tabs.Clear();
                uTabChartInfo.Tabs.Add("ALL", msg.GetMessge_Text("M001498",strLang)); //전체
                if (dtProcType.Rows.Count > 0)
                {
                    System.Data.DataTable dtDistinct = dtProcType.DefaultView.ToTable(true, "DETAILPROCESSOPERATIONTYPE");
                    for (int i = 0; i < dtDistinct.Rows.Count; i++)
                    {
                        uTabChartInfo.Tabs.Add(dtDistinct.Rows[i]["DETAILPROCESSOPERATIONTYPE"].ToString(), dtDistinct.Rows[i]["DETAILPROCESSOPERATIONTYPE"].ToString());
                    }
                }
                DrawChart();
                #endregion

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 차트 그리는 메소드

        private void DrawChart()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                int intMohtDay = DateTime.DaysInMonth(Convert.ToInt32(this.uTextSearchYear.Text), Convert.ToInt32(this.uComboSearchMonth.Value));
                /////////////////////////////////////////////////////////////////////
                string strLang = m_resSys.GetString("SYS_LANG");
                string strDay = msg.GetMessge_Text("M001500",strLang);    //일
                string strFault = msg.GetMessge_Text("M000603", strLang); //불량률
                ///////////////////////////////////////////////////////////////////// 2012-08-29 권종구
                System.Data.DataTable dtProcChart = new System.Data.DataTable("ProcChart");
                dtProcChart.Columns.Add("DETAILPROCESSOPERATIONTYPE", typeof(string));
                for (int i = 1; i <= intMohtDay; i++)
                {
                    dtProcChart.Columns.Add(i.ToString() + strDay, typeof(double));
                }

                //전체Tab을 선택한 경우 
                if (uTabChartInfo.SelectedTab.Key == "ALL")
                {
                    for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
                    {
                        if (this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strFault)
                        {
                            DataRow row = dtProcChart.NewRow();
                            row["DETAILPROCESSOPERATIONTYPE"] = uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
                            row["1" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D01"].Text);
                            row["2" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D02"].Text);
                            row["3" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D03"].Text);
                            row["4" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D04"].Text);
                            row["5" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D05"].Text);
                            row["6" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D06"].Text);
                            row["7" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D07"].Text);
                            row["8" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D08"].Text);
                            row["9" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D09"].Text);
                            for (int j = 10; j <= intMohtDay; j++)
                            {
                                row[j.ToString() + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D" + j.ToString()].Text);
                            }
                            dtProcChart.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    string strProcType = uTabChartInfo.SelectedTab.Key;
                    for (int i = 0; i < this.uGridProcTypeList.Rows.Count; i++)
                    {
                        if (this.uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text == strProcType && this.uGridProcTypeList.Rows[i].Cells["Gubun"].Text == strFault)
                        {
                            DataRow row = dtProcChart.NewRow();
                            row["DETAILPROCESSOPERATIONTYPE"] = uGridProcTypeList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text;
                            row["1" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D01"].Text);
                            row["2" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D02"].Text);
                            row["3" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D03"].Text);
                            row["4" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D04"].Text);
                            row["5" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D05"].Text);
                            row["6" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D06"].Text);
                            row["7" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D07"].Text);
                            row["8" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D08"].Text);
                            row["9" + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D09"].Text);
                            for (int j = 10; j <= intMohtDay; j++)
                            {
                                row[j.ToString() + strDay] = Convert.ToDecimal(uGridProcTypeList.Rows[i].Cells["D" + j.ToString()].Text);
                            }
                            dtProcChart.Rows.Add(row);
                        }
                    }
                }

                #region Chart그리기

                //챠트 유형 지정

                uChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                uChart.AreaChart.NullHandling = Infragistics.UltraChart.Shared.Styles.NullHandling.DontPlot;

                //Line챠트의 Line 속성 지정

                Infragistics.UltraChart.Resources.Appearance.LineAppearance ValueLineApp = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                ValueLineApp.Thickness = 4;
                ValueLineApp.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                ValueLineApp.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.Square;
                //ValueLineApp.IconAppearance.PE.Fill = System.Drawing.Color.Black;
                ValueLineApp.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                ValueLineApp.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Auto;
                uChart.LineChart.LineAppearances.Add(ValueLineApp);

                //범례 유형 지정

                uChart.Legend.Visible = true;
                uChart.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Right;
                uChart.Legend.Margins.Left = 0;
                uChart.Legend.Margins.Right = 0;
                uChart.Legend.Margins.Top = 0;
                uChart.Legend.Margins.Bottom = 0;
                uChart.Legend.SpanPercentage = 10;


                uChart.TitleTop.Text = "";
                uChart.TitleTop.HorizontalAlign = StringAlignment.Center;
                uChart.TitleTop.VerticalAlign = StringAlignment.Center;
                uChart.TitleTop.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 12, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point);
                uChart.TitleTop.Margins.Left = 0;
                uChart.TitleTop.Margins.Right = 0;
                uChart.TitleTop.Margins.Top = 0;
                uChart.TitleTop.Margins.Bottom = 0;

                //X축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisX = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisX.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                axisX.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                axisX.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;   // 라인으로 그리는 경우 ContinuousData, 막대로 그리는 경우 GroupBySeries
                axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
                axisX.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
                axisX.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisX.Extent = 1;
                uChart.TitleBottom.Text = strDay;
                uChart.TitleBottom.HorizontalAlign = StringAlignment.Center;
                uChart.TitleBottom.VerticalAlign = StringAlignment.Center;
                uChart.TitleBottom.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                uChart.TitleBottom.Margins.Left = 0;
                uChart.TitleBottom.Margins.Right = 0;
                uChart.TitleBottom.Margins.Top = 1;
                uChart.TitleBottom.Margins.Bottom = 1;
                uChart.TitleBottom.Visible = true;

                //Y축 제목
                Infragistics.UltraChart.Resources.Appearance.AxisItem axisY = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                axisY.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                axisY.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                axisY.Labels.ItemFormatString = "<DATA_VALUE:0.#>";
                axisY.Labels.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 8);
                axisY.Extent = 5;
                uChart.TitleLeft.Text = strFault + "(PPM)";
                uChart.TitleLeft.HorizontalAlign = StringAlignment.Center;
                uChart.TitleLeft.VerticalAlign = StringAlignment.Center;
                uChart.TitleLeft.Font = new System.Drawing.Font(m_resSys.GetString("SYS_FONTNAME"), 10, FontStyle.Bold, GraphicsUnit.Point);
                uChart.TitleLeft.Margins.Left = 5;
                uChart.TitleLeft.Margins.Right = 5;
                uChart.TitleLeft.Margins.Top = 0;
                uChart.TitleLeft.Margins.Bottom = 0;
                uChart.TitleLeft.Visible = true;
                uChart.LineChart.HighLightLines = true;

                Color[] colors = new Color[11];
                Random ran = new Random();
                for (int i = 0; i < 11; i++)
                {
                    int r = (int)(ran.NextDouble() * 255);
                    int g = (int)(ran.NextDouble() * 255);
                    int b = (int)(ran.NextDouble() * 255);
                    colors[i] = Color.FromArgb(r, g, b);

                }

                this.uChart.ColorModel.CustomPalette = colors;

                //uChart.LineChart
                if (dtProcChart.Rows.Count > 0)
                {
                    uChart.DataSource = dtProcChart;
                    uChart.DataBind();
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                int intMontDay = DateTime.DaysInMonth(Convert.ToInt32(this.uTextSearchYear.Text), Convert.ToInt32(this.uComboSearchMonth.Value));
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 공정 Type별 그리드

                wGrid.mfInitGeneralGrid(this.uGridProcTypeList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                for (int i = 11; i <= 32; i++)
                {
                    if (i <= intMontDay)
                    {
                        wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                    }
                    else
                    {
                        wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 20
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                    }
                }

                wGrid.mfChangeGridColumnStyle(this.uGridProcTypeList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridProcTypeList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcTypeList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region Package 별 그리드

                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Package", "Package", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D01", "1일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D02", "2일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D03", "3일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D04", "4일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D05", "5일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D06", "6일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D07", "7일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D08", "8일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D09", "9일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D10", "10일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                for (int i = 11; i <= 32; i++)
                {
                    if (i <= intMontDay)
                    {
                        wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                    }
                    else
                    {
                        wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "D" + i.ToString(), i.ToString() + "일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 20
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                    }
                }

                wGrid.mfChangeGridColumnStyle(this.uGridPackageList, intBandIndex, "Total", "Total", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 20
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridPackageList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPackageList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 엑셀관련 함수
        /// <summary>
        /// 지정된 Column Key를 Column 순번으로 변환 합니다.
        /// </summary>
        /// <param name="columnKey">Column Key</param>
        /// <returns>1부터 시작하는 Column key의 순번.</returns>
        private static int ConvertColumnKeyToIndex(string columnKey)
        {
            int index = 0;

            int x = columnKey.Length - 1;

            for (int i = 0; i < columnKey.Length; i++)
            {
                index += (Convert.ToInt32(columnKey[i]) - 64) * Convert.ToInt32(Math.Pow(26d, Convert.ToDouble(x--)));
            }

            return index;
        }

        /// <summary>
        /// 지정된 컬럼 순번을 Column key로 변환 합니다..
        /// </summary>
        /// <param name="columnIndex">1부터 시작하는 Column 순번.</param>
        /// <returns>Column Key</returns>
        private static string ConvertColumnIndexToKey(int columnIndex)
        {
            string key = string.Empty;

            for (int i = Convert.ToInt32(Math.Log(Convert.ToDouble(25d * (Convert.ToDouble(columnIndex) + 1d))) / Math.Log(26d)) - 1; i >= 0; i--)
            {
                int x = Convert.ToInt32(Math.Pow(26d, i + 1d) - 1d) / 25 - 1;
                if (columnIndex > x)
                {
                    key += (char)(((columnIndex - x - 1) / Convert.ToInt32(Math.Pow(26d, i))) % 26d + 65d);
                }
            }

            return key;
        }

        /// <summary>
        /// 새로운 CellAddress 개체 인스턴스를 Cell의 열과 행을 사용해 초기화 합니다.
        /// </summary>
        /// <param name="columnIndex">
        /// 1부터 시작하는 Column 순번.
        /// </param>
        /// <param name="rowIndex">
        /// 1부터 시작하는 Row 순번.
        /// </param>
        public string CellAddress(int intRowIndex, int intColumnIndex)
        {
            string strAddress = string.Empty;

            string strColumnKey = ConvertColumnIndexToKey(intColumnIndex);
            string strRowIndex = intRowIndex.ToString();
            strAddress = string.Format("{0}{1}", strColumnKey, strRowIndex);

            return strAddress;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null; MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion

        #region 이벤트

        // 공장콤보값에 따라 제품구분 콤보박스 설정하는 이벤트

        private void uComboSearchPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();

                //검색조건 - 제품구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                System.Data.DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "ActionTypeCode", "ActionTypeName", dtProductActionType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 년도 텍스트박스 숫자만 입력받도록 하는 이벤트

        private void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTabChartInfo_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            DrawChart();
        }

        private void uGridProcTypeList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //공정Type을 더블클릭하면 Package별 Tab으로 이동하여 선택한 공정Type으로 Filtering하여 Package별로 조회하도록 한다.
                if (e.Cell.Column.Key == "DETAILPROCESSOPERATIONTYPE")
                {
                    uTabInfo.Tabs[1].Selected = true;
                    string strProcType = e.Cell.Value.ToString();

                    for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                    {
                        if (this.uGridPackageList.Rows[i].Cells["DETAILPROCESSOPERATIONTYPE"].Text != strProcType)
                            this.uGridPackageList.Rows[i].Hidden = true;
                        else
                            this.uGridPackageList.Rows[i].Hidden = false;
                    }

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
    }
}
