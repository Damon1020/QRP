﻿namespace QRPSTA.UI
{
    partial class frmSTA0065
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0065));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextMin = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMin = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMax = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdDev = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdDeviation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMean = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAverage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecRange = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLowerSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecLower = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUpperSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecUpper = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChartHistogram = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uTextCpu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCpl = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCpk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpu = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCpl = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCpk = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCp = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchGeneration = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchStackSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchInspectItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uLabelSearchInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackageGroup5 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackageGroup5 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uComboSearchProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartHistogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGeneration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStackSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackageGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Controls.Add(this.uTextMin);
            this.uGroupBox1.Controls.Add(this.uLabelMin);
            this.uGroupBox1.Controls.Add(this.uTextMax);
            this.uGroupBox1.Controls.Add(this.uLabelMax);
            this.uGroupBox1.Controls.Add(this.uTextStdDev);
            this.uGroupBox1.Controls.Add(this.uLabelStdDeviation);
            this.uGroupBox1.Controls.Add(this.uTextMean);
            this.uGroupBox1.Controls.Add(this.uLabelAverage);
            this.uGroupBox1.Controls.Add(this.uTextSpecRange);
            this.uGroupBox1.Controls.Add(this.uLabelSpecRange);
            this.uGroupBox1.Controls.Add(this.uTextLowerSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpecLower);
            this.uGroupBox1.Controls.Add(this.uTextUpperSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpecUpper);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 132);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(440, 708);
            this.uGroupBox1.TabIndex = 12;
            // 
            // uGrid1
            // 
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance19;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance29;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 128);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(420, 576);
            this.uGrid1.TabIndex = 22;
            this.uGrid1.Text = "ultraGrid1";
            // 
            // uTextMin
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            appearance32.TextHAlignAsString = "Right";
            this.uTextMin.Appearance = appearance32;
            this.uTextMin.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMin.Location = new System.Drawing.Point(116, 100);
            this.uTextMin.Name = "uTextMin";
            this.uTextMin.ReadOnly = true;
            this.uTextMin.Size = new System.Drawing.Size(100, 21);
            this.uTextMin.TabIndex = 19;
            // 
            // uLabelMin
            // 
            this.uLabelMin.Location = new System.Drawing.Point(12, 100);
            this.uLabelMin.Name = "uLabelMin";
            this.uLabelMin.Size = new System.Drawing.Size(100, 20);
            this.uLabelMin.TabIndex = 18;
            this.uLabelMin.Text = "ultraLabel1";
            // 
            // uTextMax
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            appearance13.TextHAlignAsString = "Right";
            this.uTextMax.Appearance = appearance13;
            this.uTextMax.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMax.Location = new System.Drawing.Point(332, 100);
            this.uTextMax.Name = "uTextMax";
            this.uTextMax.ReadOnly = true;
            this.uTextMax.Size = new System.Drawing.Size(100, 21);
            this.uTextMax.TabIndex = 17;
            // 
            // uLabelMax
            // 
            this.uLabelMax.Location = new System.Drawing.Point(228, 100);
            this.uLabelMax.Name = "uLabelMax";
            this.uLabelMax.Size = new System.Drawing.Size(100, 20);
            this.uLabelMax.TabIndex = 16;
            this.uLabelMax.Text = "ultraLabel1";
            // 
            // uTextStdDev
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            appearance33.TextHAlignAsString = "Right";
            this.uTextStdDev.Appearance = appearance33;
            this.uTextStdDev.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDev.Location = new System.Drawing.Point(332, 76);
            this.uTextStdDev.Name = "uTextStdDev";
            this.uTextStdDev.ReadOnly = true;
            this.uTextStdDev.Size = new System.Drawing.Size(100, 21);
            this.uTextStdDev.TabIndex = 13;
            // 
            // uLabelStdDeviation
            // 
            this.uLabelStdDeviation.Location = new System.Drawing.Point(228, 76);
            this.uLabelStdDeviation.Name = "uLabelStdDeviation";
            this.uLabelStdDeviation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStdDeviation.TabIndex = 12;
            this.uLabelStdDeviation.Text = "ultraLabel1";
            // 
            // uTextMean
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            appearance16.TextHAlignAsString = "Right";
            this.uTextMean.Appearance = appearance16;
            this.uTextMean.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMean.Location = new System.Drawing.Point(116, 76);
            this.uTextMean.Name = "uTextMean";
            this.uTextMean.ReadOnly = true;
            this.uTextMean.Size = new System.Drawing.Size(100, 21);
            this.uTextMean.TabIndex = 11;
            // 
            // uLabelAverage
            // 
            this.uLabelAverage.Location = new System.Drawing.Point(12, 76);
            this.uLabelAverage.Name = "uLabelAverage";
            this.uLabelAverage.Size = new System.Drawing.Size(100, 20);
            this.uLabelAverage.TabIndex = 10;
            this.uLabelAverage.Text = "ultraLabel1";
            // 
            // uTextSpecRange
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Appearance = appearance17;
            this.uTextSpecRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Location = new System.Drawing.Point(332, 52);
            this.uTextSpecRange.Name = "uTextSpecRange";
            this.uTextSpecRange.ReadOnly = true;
            this.uTextSpecRange.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecRange.TabIndex = 9;
            // 
            // uLabelSpecRange
            // 
            this.uLabelSpecRange.Location = new System.Drawing.Point(228, 52);
            this.uLabelSpecRange.Name = "uLabelSpecRange";
            this.uLabelSpecRange.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecRange.TabIndex = 8;
            this.uLabelSpecRange.Text = "ultraLabel1";
            // 
            // uTextLowerSpec
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            appearance18.TextHAlignAsString = "Right";
            this.uTextLowerSpec.Appearance = appearance18;
            this.uTextLowerSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLowerSpec.Location = new System.Drawing.Point(116, 52);
            this.uTextLowerSpec.Name = "uTextLowerSpec";
            this.uTextLowerSpec.ReadOnly = true;
            this.uTextLowerSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextLowerSpec.TabIndex = 7;
            // 
            // uLabelSpecLower
            // 
            this.uLabelSpecLower.Location = new System.Drawing.Point(12, 52);
            this.uLabelSpecLower.Name = "uLabelSpecLower";
            this.uLabelSpecLower.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecLower.TabIndex = 6;
            this.uLabelSpecLower.Text = "ultraLabel1";
            // 
            // uTextUpperSpec
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            appearance34.TextHAlignAsString = "Right";
            this.uTextUpperSpec.Appearance = appearance34;
            this.uTextUpperSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperSpec.Location = new System.Drawing.Point(332, 28);
            this.uTextUpperSpec.Name = "uTextUpperSpec";
            this.uTextUpperSpec.ReadOnly = true;
            this.uTextUpperSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextUpperSpec.TabIndex = 5;
            // 
            // uLabelSpecUpper
            // 
            this.uLabelSpecUpper.Location = new System.Drawing.Point(228, 28);
            this.uLabelSpecUpper.Name = "uLabelSpecUpper";
            this.uLabelSpecUpper.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecUpper.TabIndex = 4;
            this.uLabelSpecUpper.Text = "ultraLabel1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uChartHistogram);
            this.uGroupBox2.Controls.Add(this.uTextCpu);
            this.uGroupBox2.Controls.Add(this.uTextCpl);
            this.uGroupBox2.Controls.Add(this.uTextCpk);
            this.uGroupBox2.Controls.Add(this.uTextCp);
            this.uGroupBox2.Controls.Add(this.uLabelCpu);
            this.uGroupBox2.Controls.Add(this.uLabelCpl);
            this.uGroupBox2.Controls.Add(this.uLabelCpk);
            this.uGroupBox2.Controls.Add(this.uLabelCp);
            this.uGroupBox2.Location = new System.Drawing.Point(440, 132);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(620, 712);
            this.uGroupBox2.TabIndex = 13;
            // 
            // uChartHistogram
            // 
            this.uChartHistogram.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartHistogram.Axis.PE = paintElement1;
            this.uChartHistogram.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartHistogram.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.LineThickness = 1;
            this.uChartHistogram.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.X.Visible = true;
            this.uChartHistogram.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartHistogram.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartHistogram.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.Visible = false;
            this.uChartHistogram.Axis.X2.LineThickness = 1;
            this.uChartHistogram.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.X2.Visible = false;
            this.uChartHistogram.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartHistogram.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartHistogram.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.LineThickness = 1;
            this.uChartHistogram.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Y.TickmarkInterval = 40;
            this.uChartHistogram.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Y.Visible = true;
            this.uChartHistogram.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartHistogram.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.Visible = false;
            this.uChartHistogram.Axis.Y2.LineThickness = 1;
            this.uChartHistogram.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Y2.TickmarkInterval = 40;
            this.uChartHistogram.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Y2.Visible = false;
            this.uChartHistogram.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Z.Labels.ItemFormatString = "";
            this.uChartHistogram.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.LineThickness = 1;
            this.uChartHistogram.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Z.Visible = false;
            this.uChartHistogram.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartHistogram.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.Visible = false;
            this.uChartHistogram.Axis.Z2.LineThickness = 1;
            this.uChartHistogram.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Z2.Visible = false;
            this.uChartHistogram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartHistogram.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartHistogram.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartHistogram.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartHistogram.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartHistogram.Effects.Effects.Add(gradientEffect1);
            this.uChartHistogram.Location = new System.Drawing.Point(12, 28);
            this.uChartHistogram.Name = "uChartHistogram";
            this.uChartHistogram.Size = new System.Drawing.Size(600, 648);
            this.uChartHistogram.TabIndex = 18;
            this.uChartHistogram.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uTextCpu
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            appearance11.TextHAlignAsString = "Right";
            this.uTextCpu.Appearance = appearance11;
            this.uTextCpu.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpu.Location = new System.Drawing.Point(540, 684);
            this.uTextCpu.Name = "uTextCpu";
            this.uTextCpu.ReadOnly = true;
            this.uTextCpu.Size = new System.Drawing.Size(70, 21);
            this.uTextCpu.TabIndex = 17;
            // 
            // uTextCpl
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            appearance57.TextHAlignAsString = "Right";
            this.uTextCpl.Appearance = appearance57;
            this.uTextCpl.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpl.Location = new System.Drawing.Point(388, 684);
            this.uTextCpl.Name = "uTextCpl";
            this.uTextCpl.ReadOnly = true;
            this.uTextCpl.Size = new System.Drawing.Size(70, 21);
            this.uTextCpl.TabIndex = 16;
            // 
            // uTextCpk
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            appearance59.TextHAlignAsString = "Right";
            this.uTextCpk.Appearance = appearance59;
            this.uTextCpk.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpk.Location = new System.Drawing.Point(236, 684);
            this.uTextCpk.Name = "uTextCpk";
            this.uTextCpk.ReadOnly = true;
            this.uTextCpk.Size = new System.Drawing.Size(70, 21);
            this.uTextCpk.TabIndex = 15;
            // 
            // uTextCp
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            appearance60.TextHAlignAsString = "Right";
            this.uTextCp.Appearance = appearance60;
            this.uTextCp.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCp.Location = new System.Drawing.Point(84, 684);
            this.uTextCp.Name = "uTextCp";
            this.uTextCp.ReadOnly = true;
            this.uTextCp.Size = new System.Drawing.Size(70, 21);
            this.uTextCp.TabIndex = 14;
            // 
            // uLabelCpu
            // 
            this.uLabelCpu.Location = new System.Drawing.Point(468, 684);
            this.uLabelCpu.Name = "uLabelCpu";
            this.uLabelCpu.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpu.TabIndex = 12;
            this.uLabelCpu.Text = "ultraLabel4";
            // 
            // uLabelCpl
            // 
            this.uLabelCpl.Location = new System.Drawing.Point(316, 684);
            this.uLabelCpl.Name = "uLabelCpl";
            this.uLabelCpl.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpl.TabIndex = 10;
            this.uLabelCpl.Text = "ultraLabel5";
            // 
            // uLabelCpk
            // 
            this.uLabelCpk.Location = new System.Drawing.Point(164, 684);
            this.uLabelCpk.Name = "uLabelCpk";
            this.uLabelCpk.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpk.TabIndex = 8;
            this.uLabelCpk.Text = "ultraLabel2";
            // 
            // uLabelCp
            // 
            this.uLabelCp.Location = new System.Drawing.Point(12, 684);
            this.uLabelCp.Name = "uLabelCp";
            this.uLabelCp.Size = new System.Drawing.Size(65, 20);
            this.uLabelCp.TabIndex = 6;
            this.uLabelCp.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 10;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchGeneration);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStackSeq);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectItem);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackageGroup5);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackageGroup5);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 88);
            this.uGroupBoxSearchArea.TabIndex = 16;
            // 
            // uTextSearchEquipName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Appearance = appearance14;
            this.uTextSearchEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchEquipName.Location = new System.Drawing.Point(652, 60);
            this.uTextSearchEquipName.Name = "uTextSearchEquipName";
            this.uTextSearchEquipName.ReadOnly = true;
            this.uTextSearchEquipName.Size = new System.Drawing.Size(132, 21);
            this.uTextSearchEquipName.TabIndex = 289;
            // 
            // uTextSearchEquipCode
            // 
            appearance15.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(548, 60);
            this.uTextSearchEquipCode.MaxLength = 20;
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchEquipCode.TabIndex = 290;
            this.uTextSearchEquipCode.ValueChanged += new System.EventHandler(this.uTextSearchEquipCode_ValueChanged);
            this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextEquipCode_EditorButtonClick);
            // 
            // uLabelSearchEquipCode
            // 
            this.uLabelSearchEquipCode.Location = new System.Drawing.Point(456, 60);
            this.uLabelSearchEquipCode.Name = "uLabelSearchEquipCode";
            this.uLabelSearchEquipCode.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchEquipCode.TabIndex = 288;
            this.uLabelSearchEquipCode.Text = "설비";
            // 
            // uTextSearchGeneration
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            appearance58.TextHAlignAsString = "Right";
            this.uTextSearchGeneration.Appearance = appearance58;
            this.uTextSearchGeneration.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchGeneration.Location = new System.Drawing.Point(756, 36);
            this.uTextSearchGeneration.Name = "uTextSearchGeneration";
            this.uTextSearchGeneration.ReadOnly = true;
            this.uTextSearchGeneration.Size = new System.Drawing.Size(28, 21);
            this.uTextSearchGeneration.TabIndex = 287;
            // 
            // uTextSearchStackSeq
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            appearance2.TextHAlignAsString = "Right";
            this.uTextSearchStackSeq.Appearance = appearance2;
            this.uTextSearchStackSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchStackSeq.Location = new System.Drawing.Point(724, 36);
            this.uTextSearchStackSeq.Name = "uTextSearchStackSeq";
            this.uTextSearchStackSeq.ReadOnly = true;
            this.uTextSearchStackSeq.Size = new System.Drawing.Size(28, 21);
            this.uTextSearchStackSeq.TabIndex = 286;
            // 
            // uComboSearchInspectItem
            // 
            this.uComboSearchInspectItem.CheckedListSettings.CheckStateMember = "";
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchInspectItem.DisplayLayout.Appearance = appearance69;
            this.uComboSearchInspectItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchInspectItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance54;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchInspectItem.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uComboSearchInspectItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchInspectItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            appearance72.BackColor = System.Drawing.SystemColors.Highlight;
            appearance72.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchInspectItem.DisplayLayout.Override.ActiveRowAppearance = appearance72;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchInspectItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            appearance70.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellAppearance = appearance70;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchInspectItem.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.SystemColors.Control;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchInspectItem.DisplayLayout.Override.GroupByRowAppearance = appearance74;
            appearance76.TextHAlignAsString = "Left";
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderAppearance = appearance76;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchInspectItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowAppearance = appearance75;
            this.uComboSearchInspectItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchInspectItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance73;
            this.uComboSearchInspectItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchInspectItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchInspectItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchInspectItem.Location = new System.Drawing.Point(548, 36);
            this.uComboSearchInspectItem.Name = "uComboSearchInspectItem";
            this.uComboSearchInspectItem.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchInspectItem.Size = new System.Drawing.Size(172, 22);
            this.uComboSearchInspectItem.TabIndex = 285;
            this.uComboSearchInspectItem.Text = "ultraCombo1";
            this.uComboSearchInspectItem.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboInspectItem_RowSelected);
            // 
            // uLabelSearchInspectItem
            // 
            this.uLabelSearchInspectItem.Location = new System.Drawing.Point(456, 36);
            this.uLabelSearchInspectItem.Name = "uLabelSearchInspectItem";
            this.uLabelSearchInspectItem.Size = new System.Drawing.Size(88, 20);
            this.uLabelSearchInspectItem.TabIndex = 283;
            this.uLabelSearchInspectItem.Text = "검사항목";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(890, 36);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(174, 21);
            this.uComboSearchPackage.TabIndex = 282;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(808, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(79, 20);
            this.uLabelSearchPackage.TabIndex = 281;
            this.uLabelSearchPackage.Text = "Package";
            // 
            // uComboSearchPackageGroup5
            // 
            this.uComboSearchPackageGroup5.Location = new System.Drawing.Point(920, 9);
            this.uComboSearchPackageGroup5.Name = "uComboSearchPackageGroup5";
            this.uComboSearchPackageGroup5.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackageGroup5.TabIndex = 280;
            this.uComboSearchPackageGroup5.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackageGroup5
            // 
            this.uLabelSearchPackageGroup5.Location = new System.Drawing.Point(808, 9);
            this.uLabelSearchPackageGroup5.Name = "uLabelSearchPackageGroup5";
            this.uLabelSearchPackageGroup5.Size = new System.Drawing.Size(108, 20);
            this.uLabelSearchPackageGroup5.TabIndex = 279;
            this.uLabelSearchPackageGroup5.Text = "PackageGroup";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(636, 8);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchCustomer.TabIndex = 31;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(564, 8);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(66, 20);
            this.uLabelSearchCustomer.TabIndex = 30;
            this.uLabelSearchCustomer.Text = "고객";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.CheckedListSettings.CheckStateMember = "";
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcess.DisplayLayout.Appearance = appearance78;
            this.uComboSearchProcess.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcess.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance79.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.Appearance = appearance79;
            appearance80.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.BandLabelAppearance = appearance80;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance81.BackColor2 = System.Drawing.SystemColors.Control;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcess.DisplayLayout.GroupByBox.PromptAppearance = appearance81;
            this.uComboSearchProcess.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcess.DisplayLayout.MaxRowScrollRegions = 1;
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcess.DisplayLayout.Override.ActiveCellAppearance = appearance82;
            appearance83.BackColor = System.Drawing.SystemColors.Highlight;
            appearance83.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcess.DisplayLayout.Override.ActiveRowAppearance = appearance83;
            this.uComboSearchProcess.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcess.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.Override.CardAreaAppearance = appearance84;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            appearance85.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcess.DisplayLayout.Override.CellAppearance = appearance85;
            this.uComboSearchProcess.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcess.DisplayLayout.Override.CellPadding = 0;
            appearance86.BackColor = System.Drawing.SystemColors.Control;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcess.DisplayLayout.Override.GroupByRowAppearance = appearance86;
            appearance87.TextHAlignAsString = "Left";
            this.uComboSearchProcess.DisplayLayout.Override.HeaderAppearance = appearance87;
            this.uComboSearchProcess.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcess.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcess.DisplayLayout.Override.RowAppearance = appearance88;
            this.uComboSearchProcess.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance89.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcess.DisplayLayout.Override.TemplateAddRowAppearance = appearance89;
            this.uComboSearchProcess.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcess.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcess.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcess.Location = new System.Drawing.Point(186, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcess.Size = new System.Drawing.Size(138, 22);
            this.uComboSearchProcess.TabIndex = 27;
            this.uComboSearchProcess.Text = "ultraCombo1";
            this.uComboSearchProcess.ValueChanged += new System.EventHandler(this.uComboSearchProcess_ValueChanged);
            // 
            // uComboSearchProcessGroup
            // 
            this.uComboSearchProcessGroup.Location = new System.Drawing.Point(82, 36);
            this.uComboSearchProcessGroup.Name = "uComboSearchProcessGroup";
            this.uComboSearchProcessGroup.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchProcessGroup.TabIndex = 26;
            this.uComboSearchProcessGroup.Text = "ultraComboEditor1";
            this.uComboSearchProcessGroup.ValueChanged += new System.EventHandler(this.uComboSearchProcessGroup_ValueChanged);
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(66, 20);
            this.uLabelSearchProcess.TabIndex = 25;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uDateSearchInspectToDate
            // 
            appearance9.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.Appearance = appearance9;
            this.uDateSearchInspectToDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectToDate.Location = new System.Drawing.Point(436, 9);
            this.uDateSearchInspectToDate.Name = "uDateSearchInspectToDate";
            this.uDateSearchInspectToDate.Size = new System.Drawing.Size(90, 21);
            this.uDateSearchInspectToDate.TabIndex = 15;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(422, 10);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchInspectFromDate
            // 
            appearance8.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.Appearance = appearance8;
            this.uDateSearchInspectFromDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchInspectFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectFromDate.Location = new System.Drawing.Point(328, 9);
            this.uDateSearchInspectFromDate.Name = "uDateSearchInspectFromDate";
            this.uDateSearchInspectFromDate.Size = new System.Drawing.Size(92, 21);
            this.uDateSearchInspectFromDate.TabIndex = 13;
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(256, 9);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(68, 20);
            this.uLabelSearchInspectDate.TabIndex = 12;
            this.uLabelSearchInspectDate.Text = "검색기간";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(84, 9);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 9);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(68, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // frmSTA0065
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0065";
            this.Load += new System.EventHandler(this.frmSTA0065_Load);
            this.Activated += new System.EventHandler(this.frmSTA0065_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0065_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartHistogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGeneration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStackSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackageGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMin;
        private Infragistics.Win.Misc.UltraLabel uLabelMin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMax;
        private Infragistics.Win.Misc.UltraLabel uLabelMax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdDev;
        private Infragistics.Win.Misc.UltraLabel uLabelStdDeviation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMean;
        private Infragistics.Win.Misc.UltraLabel uLabelAverage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRange;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecRange;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLowerSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecUpper;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCp;
        private Infragistics.Win.Misc.UltraLabel uLabelCpu;
        private Infragistics.Win.Misc.UltraLabel uLabelCpl;
        private Infragistics.Win.Misc.UltraLabel uLabelCpk;
        private Infragistics.Win.Misc.UltraLabel uLabelCp;
        private Infragistics.Win.UltraWinChart.UltraChart uChartHistogram;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchGeneration;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStackSeq;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchInspectItem;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackageGroup5;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackageGroup5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}