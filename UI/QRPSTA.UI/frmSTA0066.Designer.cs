﻿namespace QRPSTA.UI
{
    partial class frmSTA0066
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0066));
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextCpu = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCpl = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCpk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpu = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCpl = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCpk = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCp = new Infragistics.Win.Misc.UltraLabel();
            this.uChartHistogram = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboInspectItem = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextMin = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMin = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMax = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdDev = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdDeviation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMean = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAverage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecRange = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLowerSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecLower = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUpperSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecUpper = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uLabelSearchSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchSpecNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartHistogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchSpecNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uTextCpu);
            this.uGroupBox2.Controls.Add(this.uTextCpl);
            this.uGroupBox2.Controls.Add(this.uTextCpk);
            this.uGroupBox2.Controls.Add(this.uTextCp);
            this.uGroupBox2.Controls.Add(this.uLabelCpu);
            this.uGroupBox2.Controls.Add(this.uLabelCpl);
            this.uGroupBox2.Controls.Add(this.uLabelCpk);
            this.uGroupBox2.Controls.Add(this.uLabelCp);
            this.uGroupBox2.Controls.Add(this.uChartHistogram);
            this.uGroupBox2.Location = new System.Drawing.Point(440, 100);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(620, 742);
            this.uGroupBox2.TabIndex = 17;
            // 
            // uTextCpu
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            appearance11.TextHAlignAsString = "Right";
            this.uTextCpu.Appearance = appearance11;
            this.uTextCpu.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpu.Location = new System.Drawing.Point(540, 712);
            this.uTextCpu.Name = "uTextCpu";
            this.uTextCpu.ReadOnly = true;
            this.uTextCpu.Size = new System.Drawing.Size(70, 21);
            this.uTextCpu.TabIndex = 17;
            // 
            // uTextCpl
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            appearance12.TextHAlignAsString = "Right";
            this.uTextCpl.Appearance = appearance12;
            this.uTextCpl.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpl.Location = new System.Drawing.Point(388, 712);
            this.uTextCpl.Name = "uTextCpl";
            this.uTextCpl.ReadOnly = true;
            this.uTextCpl.Size = new System.Drawing.Size(70, 21);
            this.uTextCpl.TabIndex = 16;
            // 
            // uTextCpk
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            appearance32.TextHAlignAsString = "Right";
            this.uTextCpk.Appearance = appearance32;
            this.uTextCpk.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpk.Location = new System.Drawing.Point(236, 712);
            this.uTextCpk.Name = "uTextCpk";
            this.uTextCpk.ReadOnly = true;
            this.uTextCpk.Size = new System.Drawing.Size(70, 21);
            this.uTextCpk.TabIndex = 15;
            // 
            // uTextCp
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            appearance33.TextHAlignAsString = "Right";
            this.uTextCp.Appearance = appearance33;
            this.uTextCp.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCp.Location = new System.Drawing.Point(84, 712);
            this.uTextCp.Name = "uTextCp";
            this.uTextCp.ReadOnly = true;
            this.uTextCp.Size = new System.Drawing.Size(70, 21);
            this.uTextCp.TabIndex = 14;
            // 
            // uLabelCpu
            // 
            this.uLabelCpu.Location = new System.Drawing.Point(468, 712);
            this.uLabelCpu.Name = "uLabelCpu";
            this.uLabelCpu.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpu.TabIndex = 12;
            this.uLabelCpu.Text = "ultraLabel4";
            // 
            // uLabelCpl
            // 
            this.uLabelCpl.Location = new System.Drawing.Point(316, 712);
            this.uLabelCpl.Name = "uLabelCpl";
            this.uLabelCpl.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpl.TabIndex = 10;
            this.uLabelCpl.Text = "ultraLabel5";
            // 
            // uLabelCpk
            // 
            this.uLabelCpk.Location = new System.Drawing.Point(164, 712);
            this.uLabelCpk.Name = "uLabelCpk";
            this.uLabelCpk.Size = new System.Drawing.Size(65, 20);
            this.uLabelCpk.TabIndex = 8;
            this.uLabelCpk.Text = "ultraLabel2";
            // 
            // uLabelCp
            // 
            this.uLabelCp.Location = new System.Drawing.Point(12, 712);
            this.uLabelCp.Name = "uLabelCp";
            this.uLabelCp.Size = new System.Drawing.Size(65, 20);
            this.uLabelCp.TabIndex = 6;
            this.uLabelCp.Text = "ultraLabel1";
            // 
            // uChartHistogram
            // 
            this.uChartHistogram.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartHistogram.Axis.PE = paintElement1;
            this.uChartHistogram.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartHistogram.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X.LineThickness = 1;
            this.uChartHistogram.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.X.Visible = true;
            this.uChartHistogram.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartHistogram.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartHistogram.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.X2.Labels.Visible = false;
            this.uChartHistogram.Axis.X2.LineThickness = 1;
            this.uChartHistogram.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.X2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.X2.Visible = false;
            this.uChartHistogram.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartHistogram.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartHistogram.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y.LineThickness = 1;
            this.uChartHistogram.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Y.TickmarkInterval = 40;
            this.uChartHistogram.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Y.Visible = true;
            this.uChartHistogram.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartHistogram.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartHistogram.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Y2.Labels.Visible = false;
            this.uChartHistogram.Axis.Y2.LineThickness = 1;
            this.uChartHistogram.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Y2.TickmarkInterval = 40;
            this.uChartHistogram.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Y2.Visible = false;
            this.uChartHistogram.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Z.Labels.ItemFormatString = "";
            this.uChartHistogram.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z.LineThickness = 1;
            this.uChartHistogram.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Z.Visible = false;
            this.uChartHistogram.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartHistogram.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartHistogram.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartHistogram.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartHistogram.Axis.Z2.Labels.Visible = false;
            this.uChartHistogram.Axis.Z2.LineThickness = 1;
            this.uChartHistogram.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartHistogram.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartHistogram.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartHistogram.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartHistogram.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartHistogram.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartHistogram.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartHistogram.Axis.Z2.Visible = false;
            this.uChartHistogram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartHistogram.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartHistogram.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartHistogram.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartHistogram.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartHistogram.Effects.Effects.Add(gradientEffect1);
            this.uChartHistogram.Location = new System.Drawing.Point(12, 28);
            this.uChartHistogram.Name = "uChartHistogram";
            this.uChartHistogram.Size = new System.Drawing.Size(600, 676);
            this.uChartHistogram.TabIndex = 0;
            this.uChartHistogram.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartHistogram.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uComboInspectItem);
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Controls.Add(this.uTextMin);
            this.uGroupBox1.Controls.Add(this.uLabelMin);
            this.uGroupBox1.Controls.Add(this.uTextMax);
            this.uGroupBox1.Controls.Add(this.uLabelMax);
            this.uGroupBox1.Controls.Add(this.uTextStdDev);
            this.uGroupBox1.Controls.Add(this.uLabelStdDeviation);
            this.uGroupBox1.Controls.Add(this.uTextMean);
            this.uGroupBox1.Controls.Add(this.uLabelAverage);
            this.uGroupBox1.Controls.Add(this.uTextSpecRange);
            this.uGroupBox1.Controls.Add(this.uLabelSpecRange);
            this.uGroupBox1.Controls.Add(this.uTextLowerSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpecLower);
            this.uGroupBox1.Controls.Add(this.uTextUpperSpec);
            this.uGroupBox1.Controls.Add(this.uLabelSpecUpper);
            this.uGroupBox1.Controls.Add(this.uLabelInspectItem);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 100);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(440, 742);
            this.uGroupBox1.TabIndex = 16;
            // 
            // uComboInspectItem
            // 
            this.uComboInspectItem.CheckedListSettings.CheckStateMember = "";
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboInspectItem.DisplayLayout.Appearance = appearance36;
            this.uComboInspectItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboInspectItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance42.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboInspectItem.DisplayLayout.GroupByBox.Appearance = appearance42;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboInspectItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance48;
            this.uComboInspectItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance55.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance55.BackColor2 = System.Drawing.SystemColors.Control;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboInspectItem.DisplayLayout.GroupByBox.PromptAppearance = appearance55;
            this.uComboInspectItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboInspectItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboInspectItem.DisplayLayout.Override.ActiveCellAppearance = appearance61;
            appearance62.BackColor = System.Drawing.SystemColors.Highlight;
            appearance62.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboInspectItem.DisplayLayout.Override.ActiveRowAppearance = appearance62;
            this.uComboInspectItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboInspectItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            this.uComboInspectItem.DisplayLayout.Override.CardAreaAppearance = appearance63;
            appearance64.BorderColor = System.Drawing.Color.Silver;
            appearance64.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboInspectItem.DisplayLayout.Override.CellAppearance = appearance64;
            this.uComboInspectItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboInspectItem.DisplayLayout.Override.CellPadding = 0;
            appearance65.BackColor = System.Drawing.SystemColors.Control;
            appearance65.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance65.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance65.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboInspectItem.DisplayLayout.Override.GroupByRowAppearance = appearance65;
            appearance66.TextHAlignAsString = "Left";
            this.uComboInspectItem.DisplayLayout.Override.HeaderAppearance = appearance66;
            this.uComboInspectItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboInspectItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            appearance67.BorderColor = System.Drawing.Color.Silver;
            this.uComboInspectItem.DisplayLayout.Override.RowAppearance = appearance67;
            this.uComboInspectItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance68.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboInspectItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance68;
            this.uComboInspectItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboInspectItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboInspectItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboInspectItem.Location = new System.Drawing.Point(116, 28);
            this.uComboInspectItem.Name = "uComboInspectItem";
            this.uComboInspectItem.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboInspectItem.Size = new System.Drawing.Size(100, 22);
            this.uComboInspectItem.TabIndex = 63;
            this.uComboInspectItem.Text = "ultraCombo1";
            this.uComboInspectItem.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboInspectItem_RowSelected);
            this.uComboInspectItem.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboInspectItem_BeforeDropDown);
            // 
            // uGrid1
            // 
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance19;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance29;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 128);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(420, 604);
            this.uGrid1.TabIndex = 22;
            this.uGrid1.Text = "ultraGrid1";
            // 
            // uTextMin
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            appearance34.TextHAlignAsString = "Right";
            this.uTextMin.Appearance = appearance34;
            this.uTextMin.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMin.Location = new System.Drawing.Point(116, 100);
            this.uTextMin.Name = "uTextMin";
            this.uTextMin.ReadOnly = true;
            this.uTextMin.Size = new System.Drawing.Size(100, 21);
            this.uTextMin.TabIndex = 19;
            // 
            // uLabelMin
            // 
            this.uLabelMin.Location = new System.Drawing.Point(12, 100);
            this.uLabelMin.Name = "uLabelMin";
            this.uLabelMin.Size = new System.Drawing.Size(100, 20);
            this.uLabelMin.TabIndex = 18;
            this.uLabelMin.Text = "ultraLabel1";
            // 
            // uTextMax
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            appearance13.TextHAlignAsString = "Right";
            this.uTextMax.Appearance = appearance13;
            this.uTextMax.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMax.Location = new System.Drawing.Point(332, 100);
            this.uTextMax.Name = "uTextMax";
            this.uTextMax.ReadOnly = true;
            this.uTextMax.Size = new System.Drawing.Size(100, 21);
            this.uTextMax.TabIndex = 17;
            // 
            // uLabelMax
            // 
            this.uLabelMax.Location = new System.Drawing.Point(228, 100);
            this.uLabelMax.Name = "uLabelMax";
            this.uLabelMax.Size = new System.Drawing.Size(100, 20);
            this.uLabelMax.TabIndex = 16;
            this.uLabelMax.Text = "ultraLabel1";
            // 
            // uTextStdDev
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            appearance15.TextHAlignAsString = "Right";
            this.uTextStdDev.Appearance = appearance15;
            this.uTextStdDev.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDev.Location = new System.Drawing.Point(332, 76);
            this.uTextStdDev.Name = "uTextStdDev";
            this.uTextStdDev.ReadOnly = true;
            this.uTextStdDev.Size = new System.Drawing.Size(100, 21);
            this.uTextStdDev.TabIndex = 13;
            // 
            // uLabelStdDeviation
            // 
            this.uLabelStdDeviation.Location = new System.Drawing.Point(228, 76);
            this.uLabelStdDeviation.Name = "uLabelStdDeviation";
            this.uLabelStdDeviation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStdDeviation.TabIndex = 12;
            this.uLabelStdDeviation.Text = "ultraLabel1";
            // 
            // uTextMean
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            appearance35.TextHAlignAsString = "Right";
            this.uTextMean.Appearance = appearance35;
            this.uTextMean.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMean.Location = new System.Drawing.Point(116, 76);
            this.uTextMean.Name = "uTextMean";
            this.uTextMean.ReadOnly = true;
            this.uTextMean.Size = new System.Drawing.Size(100, 21);
            this.uTextMean.TabIndex = 11;
            // 
            // uLabelAverage
            // 
            this.uLabelAverage.Location = new System.Drawing.Point(12, 76);
            this.uLabelAverage.Name = "uLabelAverage";
            this.uLabelAverage.Size = new System.Drawing.Size(100, 20);
            this.uLabelAverage.TabIndex = 10;
            this.uLabelAverage.Text = "ultraLabel1";
            // 
            // uTextSpecRange
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Appearance = appearance37;
            this.uTextSpecRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Location = new System.Drawing.Point(332, 52);
            this.uTextSpecRange.Name = "uTextSpecRange";
            this.uTextSpecRange.ReadOnly = true;
            this.uTextSpecRange.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecRange.TabIndex = 9;
            // 
            // uLabelSpecRange
            // 
            this.uLabelSpecRange.Location = new System.Drawing.Point(228, 52);
            this.uLabelSpecRange.Name = "uLabelSpecRange";
            this.uLabelSpecRange.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecRange.TabIndex = 8;
            this.uLabelSpecRange.Text = "ultraLabel1";
            // 
            // uTextLowerSpec
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            appearance18.TextHAlignAsString = "Right";
            this.uTextLowerSpec.Appearance = appearance18;
            this.uTextLowerSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLowerSpec.Location = new System.Drawing.Point(116, 52);
            this.uTextLowerSpec.Name = "uTextLowerSpec";
            this.uTextLowerSpec.ReadOnly = true;
            this.uTextLowerSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextLowerSpec.TabIndex = 7;
            // 
            // uLabelSpecLower
            // 
            this.uLabelSpecLower.Location = new System.Drawing.Point(12, 52);
            this.uLabelSpecLower.Name = "uLabelSpecLower";
            this.uLabelSpecLower.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecLower.TabIndex = 6;
            this.uLabelSpecLower.Text = "ultraLabel1";
            // 
            // uTextUpperSpec
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            appearance58.TextHAlignAsString = "Right";
            this.uTextUpperSpec.Appearance = appearance58;
            this.uTextUpperSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperSpec.Location = new System.Drawing.Point(332, 28);
            this.uTextUpperSpec.Name = "uTextUpperSpec";
            this.uTextUpperSpec.ReadOnly = true;
            this.uTextUpperSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextUpperSpec.TabIndex = 5;
            // 
            // uLabelSpecUpper
            // 
            this.uLabelSpecUpper.Location = new System.Drawing.Point(228, 28);
            this.uLabelSpecUpper.Name = "uLabelSpecUpper";
            this.uLabelSpecUpper.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecUpper.TabIndex = 4;
            this.uLabelSpecUpper.Text = "ultraLabel1";
            // 
            // uLabelInspectItem
            // 
            this.uLabelInspectItem.Location = new System.Drawing.Point(12, 28);
            this.uLabelInspectItem.Name = "uLabelInspectItem";
            this.uLabelInspectItem.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectItem.TabIndex = 2;
            this.uLabelInspectItem.Text = "ultraLabel1";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchSpecNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateInspectDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateInspectDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 15;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 14;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uLabelSearchSpec
            // 
            this.uLabelSearchSpec.Location = new System.Drawing.Point(376, 32);
            this.uLabelSearchSpec.Name = "uLabelSearchSpec";
            this.uLabelSearchSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchSpec.TabIndex = 250;
            this.uLabelSearchSpec.Text = "2";
            // 
            // uTextSearchSpecNum
            // 
            appearance57.BackColor = System.Drawing.Color.White;
            this.uTextSearchSpecNum.Appearance = appearance57;
            this.uTextSearchSpecNum.BackColor = System.Drawing.Color.White;
            this.uTextSearchSpecNum.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchSpecNum.Location = new System.Drawing.Point(492, 32);
            this.uTextSearchSpecNum.Name = "uTextSearchSpecNum";
            this.uTextSearchSpecNum.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchSpecNum.TabIndex = 249;
            // 
            // uDateInspectDateTo
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDateTo.Appearance = appearance17;
            this.uDateInspectDateTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateInspectDateTo.Location = new System.Drawing.Point(604, 8);
            this.uDateInspectDateTo.Name = "uDateInspectDateTo";
            this.uDateInspectDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDateTo.TabIndex = 247;
            // 
            // uDateInspectDateFrom
            // 
            appearance54.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDateFrom.Appearance = appearance54;
            this.uDateInspectDateFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateInspectDateFrom.Location = new System.Drawing.Point(492, 8);
            this.uDateInspectDateFrom.Name = "uDateInspectDateFrom";
            this.uDateInspectDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDateFrom.TabIndex = 248;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(592, 12);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel2.TabIndex = 246;
            this.ultraLabel2.Text = "~";
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(712, 32);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchVendor.TabIndex = 245;
            this.uLabelSearchVendor.Text = "3";
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(16, 32);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchMaterialCode.TabIndex = 244;
            this.uLabelSearchMaterialCode.Text = "2";
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(376, 8);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectDate.TabIndex = 243;
            this.uLabelInspectDate.Text = "1";
            // 
            // uTextSearchVendorName
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance46;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(944, 32);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorName.TabIndex = 241;
            // 
            // uTextSearchMaterialName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance16;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(248, 32);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialName.TabIndex = 242;
            // 
            // uTextSearchVendorCode
            // 
            appearance14.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance14;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(828, 32);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 239;
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uTextSearchMaterialCode
            // 
            appearance59.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchMaterialCode.Appearance = appearance59;
            this.uTextSearchMaterialCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance60.Image = global::QRPSTA.UI.Properties.Resources.btn_Zoom;
            appearance60.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance60;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(132, 32);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 240;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick_1);
            // 
            // uComboSearchPlant
            // 
            appearance81.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.Appearance = appearance81;
            this.uComboSearchPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboSearchPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSearchPlant.Location = new System.Drawing.Point(132, 8);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 238;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(16, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 237;
            // 
            // frmSTA0066
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox2);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0066";
            this.Load += new System.EventHandler(this.frmSTA0066_Load);
            this.Activated += new System.EventHandler(this.frmSTA0066_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0066_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartHistogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchSpecNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpu;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCp;
        private Infragistics.Win.Misc.UltraLabel uLabelCpu;
        private Infragistics.Win.Misc.UltraLabel uLabelCpl;
        private Infragistics.Win.Misc.UltraLabel uLabelCpk;
        private Infragistics.Win.Misc.UltraLabel uLabelCp;
        private Infragistics.Win.UltraWinChart.UltraChart uChartHistogram;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboInspectItem;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMin;
        private Infragistics.Win.Misc.UltraLabel uLabelMin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMax;
        private Infragistics.Win.Misc.UltraLabel uLabelMax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdDev;
        private Infragistics.Win.Misc.UltraLabel uLabelStdDeviation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMean;
        private Infragistics.Win.Misc.UltraLabel uLabelAverage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRange;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecRange;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLowerSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecLower;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecUpper;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItem;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchSpecNum;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}