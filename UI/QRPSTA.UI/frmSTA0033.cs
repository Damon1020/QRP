﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0033.cs                                         */
/* 프로그램명   : c(결점수) 관리도 분석                                */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0033 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0033()
        {
            InitializeComponent();
        }

        private void frmSTA0033_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmSTA0033_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("c(결점수) 관리도 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사항목 및 측정값", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.CHART, "C 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSpecUpper, "규격상한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecLower, "규격하한", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecRange, "규격범위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAverage, "평균", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSampleAmount, "시료수", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 검사항목
                DataTable dt = new DataTable();
                wCombo.mfSetComboEditor(this.uComboInspectItem, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "", "", dtPlant);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "측정일", "측정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "측정시간", "측정시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Time, "", "hh:mm:ss", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "결점수", "결점수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;  
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion
    }
}
