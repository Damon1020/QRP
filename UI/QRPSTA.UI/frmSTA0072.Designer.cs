﻿namespace QRPSTA.UI
{
    partial class frmSTA0072
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSTA0072));
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridProcTypeList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridPackageList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uTabChartInfo = new Infragistics.Win.UltraWinTabControl.UltraTabStripControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uChart = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uTabInfo = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcTypeList)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabChartInfo)).BeginInit();
            this.uTabChartInfo.SuspendLayout();
            this.ultraTabSharedControlsPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabInfo)).BeginInit();
            this.uTabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridProcTypeList);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(908, 450);
            // 
            // uGridProcTypeList
            // 
            this.uGridProcTypeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcTypeList.DisplayLayout.Appearance = appearance2;
            this.uGridProcTypeList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcTypeList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcTypeList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcTypeList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridProcTypeList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcTypeList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridProcTypeList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcTypeList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcTypeList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcTypeList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridProcTypeList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcTypeList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcTypeList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcTypeList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridProcTypeList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcTypeList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcTypeList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridProcTypeList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridProcTypeList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcTypeList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcTypeList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridProcTypeList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcTypeList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridProcTypeList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcTypeList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcTypeList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcTypeList.Location = new System.Drawing.Point(6, 5);
            this.uGridProcTypeList.Name = "uGridProcTypeList";
            this.uGridProcTypeList.Size = new System.Drawing.Size(897, 439);
            this.uGridProcTypeList.TabIndex = 319;
            this.uGridProcTypeList.Text = "uGridMonth";
            this.uGridProcTypeList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridProcTypeList_DoubleClickCell);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridPackageList);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(908, 450);
            // 
            // uGridPackageList
            // 
            this.uGridPackageList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPackageList.DisplayLayout.Appearance = appearance14;
            this.uGridPackageList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPackageList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridPackageList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridPackageList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPackageList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPackageList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPackageList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPackageList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridPackageList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPackageList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridPackageList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridPackageList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPackageList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridPackageList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridPackageList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPackageList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridPackageList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPackageList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPackageList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPackageList.Location = new System.Drawing.Point(5, 5);
            this.uGridPackageList.Name = "uGridPackageList";
            this.uGridPackageList.Size = new System.Drawing.Size(897, 439);
            this.uGridPackageList.TabIndex = 320;
            this.uGridPackageList.Text = "uGridMonth";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance27;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMonth);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlantCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(624, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(113, 21);
            this.uComboSearchCustomer.TabIndex = 326;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(535, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 325;
            this.uLabelSearchCustomer.Text = "고객사";
            // 
            // uComboSearchProductActionType
            // 
            this.uComboSearchProductActionType.Location = new System.Drawing.Point(415, 12);
            this.uComboSearchProductActionType.Name = "uComboSearchProductActionType";
            this.uComboSearchProductActionType.Size = new System.Drawing.Size(113, 21);
            this.uComboSearchProductActionType.TabIndex = 324;
            this.uComboSearchProductActionType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProductActionType
            // 
            this.uLabelSearchProductActionType.Location = new System.Drawing.Point(326, 12);
            this.uLabelSearchProductActionType.Name = "uLabelSearchProductActionType";
            this.uLabelSearchProductActionType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchProductActionType.TabIndex = 323;
            this.uLabelSearchProductActionType.Text = "제품구분";
            // 
            // uComboSearchMonth
            // 
            this.uComboSearchMonth.Location = new System.Drawing.Point(257, 12);
            this.uComboSearchMonth.Name = "uComboSearchMonth";
            this.uComboSearchMonth.Size = new System.Drawing.Size(62, 21);
            this.uComboSearchMonth.TabIndex = 322;
            this.uComboSearchMonth.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMonth
            // 
            this.uLabelSearchMonth.Location = new System.Drawing.Point(168, 12);
            this.uLabelSearchMonth.Name = "uLabelSearchMonth";
            this.uLabelSearchMonth.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchMonth.TabIndex = 321;
            this.uLabelSearchMonth.Text = "ultraLabel1";
            // 
            // uTextSearchYear
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Appearance = appearance1;
            this.uTextSearchYear.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSearchYear.Location = new System.Drawing.Point(99, 12);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(62, 21);
            this.uTextSearchYear.TabIndex = 320;
            this.uTextSearchYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextSearchYear_KeyPress);
            // 
            // uLabelSearchYear
            // 
            appearance17.BackColor = System.Drawing.Color.LightCyan;
            this.uLabelSearchYear.Appearance = appearance17;
            this.uLabelSearchYear.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchYear.TabIndex = 319;
            this.uLabelSearchYear.Text = "년도";
            // 
            // uComboSearchPlantCode
            // 
            this.uComboSearchPlantCode.Location = new System.Drawing.Point(785, 0);
            this.uComboSearchPlantCode.Name = "uComboSearchPlantCode";
            this.uComboSearchPlantCode.Size = new System.Drawing.Size(129, 21);
            this.uComboSearchPlantCode.TabIndex = 1;
            this.uComboSearchPlantCode.Text = "ultraComboEditor1";
            this.uComboSearchPlantCode.ValueChanged += new System.EventHandler(this.uComboSearchPlantCode_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(696, 0);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uTabChartInfo
            // 
            this.uTabChartInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabChartInfo.Controls.Add(this.ultraTabSharedControlsPage2);
            this.uTabChartInfo.Location = new System.Drawing.Point(0, 80);
            this.uTabChartInfo.Name = "uTabChartInfo";
            this.uTabChartInfo.SharedControls.AddRange(new System.Windows.Forms.Control[] {
            this.uChart});
            this.uTabChartInfo.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.uTabChartInfo.Size = new System.Drawing.Size(912, 288);
            this.uTabChartInfo.TabIndex = 323;
            this.uTabChartInfo.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uTabChartInfo_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Controls.Add(this.uChart);
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(908, 265);
            // 
            // uChart
            // 
            this.uChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChart.Axis.PE = paintElement1;
            this.uChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X.LineThickness = 1;
            this.uChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MajorGridLines.Visible = true;
            this.uChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X.MinorGridLines.Visible = false;
            this.uChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X.Visible = true;
            this.uChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.X2.Labels.Visible = false;
            this.uChart.Axis.X2.LineThickness = 1;
            this.uChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MajorGridLines.Visible = true;
            this.uChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.X2.MinorGridLines.Visible = false;
            this.uChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.X2.Visible = false;
            this.uChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y.LineThickness = 1;
            this.uChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MajorGridLines.Visible = true;
            this.uChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y.MinorGridLines.Visible = false;
            this.uChart.Axis.Y.TickmarkInterval = 20;
            this.uChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y.Visible = true;
            this.uChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Y2.Labels.Visible = false;
            this.uChart.Axis.Y2.LineThickness = 1;
            this.uChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MajorGridLines.Visible = true;
            this.uChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Y2.MinorGridLines.Visible = false;
            this.uChart.Axis.Y2.TickmarkInterval = 20;
            this.uChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Y2.Visible = false;
            this.uChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z.Labels.ItemFormatString = "";
            this.uChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z.LineThickness = 1;
            this.uChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MajorGridLines.Visible = true;
            this.uChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z.MinorGridLines.Visible = false;
            this.uChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z.Visible = false;
            this.uChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChart.Axis.Z2.Labels.ItemFormatString = "";
            this.uChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChart.Axis.Z2.Labels.Visible = false;
            this.uChart.Axis.Z2.LineThickness = 1;
            this.uChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MajorGridLines.Visible = true;
            this.uChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChart.Axis.Z2.MinorGridLines.Visible = false;
            this.uChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChart.Axis.Z2.Visible = false;
            this.uChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChart.ColorModel.AlphaLevel = ((byte)(150));
            this.uChart.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChart.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChart.Effects.Effects.Add(gradientEffect1);
            this.uChart.Location = new System.Drawing.Point(7, 6);
            this.uChart.Name = "uChart";
            this.uChart.Size = new System.Drawing.Size(894, 254);
            this.uChart.TabIndex = 320;
            this.uChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uTabInfo
            // 
            this.uTabInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabInfo.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabInfo.Controls.Add(this.ultraTabPageControl1);
            this.uTabInfo.Controls.Add(this.ultraTabPageControl2);
            this.uTabInfo.Location = new System.Drawing.Point(0, 368);
            this.uTabInfo.Name = "uTabInfo";
            this.uTabInfo.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabInfo.Size = new System.Drawing.Size(912, 476);
            this.uTabInfo.TabIndex = 322;
            ultraTab3.Key = "Process";
            ultraTab3.TabPage = this.ultraTabPageControl1;
            ultraTab3.Text = "공정Type별";
            ultraTab4.Key = "Package";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "Package별";
            this.uTabInfo.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(908, 450);
            // 
            // frmSTA0072
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uTabChartInfo);
            this.Controls.Add(this.uTabInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSTA0072";
            this.Load += new System.EventHandler(this.frmSTA0072_Load);
            this.Activated += new System.EventHandler(this.frmSTA0072_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSTA0072_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcTypeList)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabChartInfo)).EndInit();
            this.uTabChartInfo.ResumeLayout(false);
            this.ultraTabSharedControlsPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabInfo)).EndInit();
            this.uTabInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMonth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProductActionType;
        private Infragistics.Win.UltraWinTabControl.UltraTabStripControl uTabChartInfo;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinChart.UltraChart uChart;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabInfo;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcTypeList;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPackageList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
    }
}