﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사통계분석                                      */
/* 프로그램ID   : frmSTA0062.cs                                         */
/* 프로그램명   : 월별 관리한계선 분석                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPSTA.UI
{
    public partial class frmSTA0062 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmSTA0062()
        {
            InitializeComponent();
        }

        private void frmSTA0062_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 설정함수 호출
            this.titleArea.mfSetLabelText("월별 관리한계선 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 컨트롤 초기화
            InitLabel();
            InitComboBox();
            InitGrid();
        }

        private void frmSTA0062_Activated(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchYear, "조회년도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Group 설정
                this.uGrid1.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;

                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_1 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_1", "1월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_2 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_2", "2월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_3 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_3", "3월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_4 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_4", "4월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_5 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_5", "5월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_6 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_6", "6월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_7 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_7", "7월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_8 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_8", "8월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_9 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_9", "9월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_10 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_10", "10월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_11 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_11", "11월", false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup Month_12 = wGrid.mfSetGridGroup(this.uGrid1, 0, "Month_12", "12월", false);

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "고객사", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "제품코드", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "제품명", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "검사항목", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL1", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 1, Month_1);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL1", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 1, Month_1);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL2", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 1, Month_2);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL2", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, Month_2);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL3", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, Month_3);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL3", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, Month_3);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL4", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 10, 0, 1, 1, Month_4);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL4", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 1, 1, Month_4);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL5", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 12, 0, 1, 1, Month_5);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL5", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 13, 0, 1, 1, Month_5);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL6", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 14, 0, 1, 1, Month_6);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL6", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 15, 0, 1, 1, Month_6);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL7", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 16, 0, 1, 1, Month_7);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL7", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 17, 0, 1, 1, Month_7);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL8", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 18, 0, 1, 1, Month_8);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL8", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 19, 0, 1, 1, Month_8);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL9", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 20, 0, 1, 1, Month_9);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL9", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 21, 0, 1, 1, Month_9);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL10", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 22, 0, 1, 1, Month_10);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL10", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 23, 0, 1, 1, Month_10);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL11", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 24, 0, 1, 1, Month_11);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL11", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 25, 0, 1, 1, Month_11);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LCL12", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 26, 0, 1, 1, Month_12);

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UCL12", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 27, 0, 1, 1, Month_12);

                // set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion
    }
}
