﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0011.cs                                         */
/* 프로그램명   : 사용자정보                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0011 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strUserID;
        private string strUserName;
        private string strDeptCode;
        private string strDeptName;
        private string strPosition;
        private string strTelNum;
        private string strHpNum;
        private string strEMail;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string UserID
        {
            get { return strUserID; }
            set { strUserID = value; }
        }

        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }

        public string DeptCode
        {
            get { return strDeptCode; }
            set { strDeptCode = value; }
        }

        public string DeptName
        {
            get { return strDeptName; }
            set { strDeptName = value; }
        }

        public string Position
        {
            get { return strPosition; }
            set { strPosition = value; }
        }
        
        public string TelNum
        {
            get { return strTelNum; }
            set { strTelNum = value; }
        }

        public string HpNum
        {
            get { return strHpNum; }
            set { strHpNum = value; }
        }

        public string EMail
        {
            get { return strEMail; }
            set { strEMail = value; }
        }

        public frmCOM0011()
        {
            InitializeComponent();
        }

        private void frmCOM0011_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strUserID = "";
            strUserName = "";
            strDeptCode = "";
            strDeptName = "";
            strPosition = "";
            strTelNum = "";
            strHpNum = "";
            strEMail = "";
            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            if (PlantCode != null)
            {
                if (!PlantCode.Equals(string.Empty))
                {
                    uComboSearchPlant.Value = PlantCode;
                }
            }

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelUserID, "사용자ID", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelUserName, "사용자명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchDept, "부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;

                //부서정보BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                //부서정보 가져오기
                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode,m_resSys.GetString("SYS_LANG"));
                clsDept.Dispose();

                //Combo 부서정보 추가
                combo.mfSetComboEditor(this.uComboSearchDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "DeptCode", "DeptName", dtDept);

                dt.Dispose();
                dtDept.Dispose();

            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridUser, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridUser, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "TelNum", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "HpNum", "핸드폰번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridUser, 0, "EMail", "이메일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //폰트설정
                uGridUser.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridUser.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsusr = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsusr);

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                string strFactoryCode = this.uComboSearchPlant.Value.ToString();
                string strDeptCode = this.uComboSearchDept.Value.ToString();

                //DataTable dtusr = clsusr.mfReadSYSUser_Like(strPlantCode, this.uTextUserID.Text,this.uTextUserName.Text,m_resSys.GetString("SYS_LANG"));
                //부서별사용자정보 조회
                DataTable dtusr = clsusr.mfReadSYSUser_DeptLike(strFactoryCode, strDeptCode, this.uTextUserID.Text, this.uTextUserName.Text, m_resSys.GetString("SYS_LANG"));
                this.uGridUser.DataSource = dtusr;
                this.uGridUser.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtusr.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridUser, 0);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridUser_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strUserID = e.Row.Cells["UserID"].Text.ToString();
                strUserName = e.Row.Cells["UserName"].Text.ToString();
                strDeptCode = e.Row.Cells["DeptCode"].Text.ToString();
                strDeptName = e.Row.Cells["DeptName"].Text.ToString();
                strPosition = e.Row.Cells["Position"].Text.ToString();
                strTelNum = e.Row.Cells["TelNum"].Text.ToString();
                strHpNum = e.Row.Cells["HpNum"].Text.ToString();
                strEMail = e.Row.Cells["EMail"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridUser.ActiveRow == null) return;

                if (this.uGridUser.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridUser.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridUser.ActiveRow.Cells["PlantName"].Text.ToString();
                    strUserID = this.uGridUser.ActiveRow.Cells["UserID"].Text.ToString();
                    strUserName = this.uGridUser.ActiveRow.Cells["UserName"].Text.ToString();
                    strDeptCode = this.uGridUser.ActiveRow.Cells["DeptCode"].Text.ToString();
                    strDeptName = this.uGridUser.ActiveRow.Cells["DeptName"].Text.ToString();
                    strPosition = this.uGridUser.ActiveRow.Cells["Position"].Text.ToString();
                    strTelNum = this.uGridUser.ActiveRow.Cells["TelNum"].Text.ToString();
                    strHpNum = this.uGridUser.ActiveRow.Cells["HpNum"].Text.ToString();
                    strEMail = this.uGridUser.ActiveRow.Cells["EMail"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uTextUser_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData.Equals(Keys.Enter))
                Search();
        }
    }
}
