﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0001.cs                                         */
/* 프로그램명   : 자재정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0001 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //자재정보 속성
        private string strPlantCode;
        private string strPlantName;
        private string strMaterialCode;
        private string strMaterialName;
        private string strMaterialGroupCode;
        private string strMaterialGroupName;
        private string strMaterialTypeCode;
        private string strMaterialTypeName;
        private string strConsumableTypeCode;
        private string strConsumableTypeName;
        private string strMaterialType;
        private string strSpec;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string MaterialCode
        {
            get { return strMaterialCode; }
            set { strMaterialCode = value; }
        }

        public string MaterialName
        {
            get { return strMaterialName; }
            set { strMaterialName = value; }
        }

        public string MaterialGroupCode
        {
            get { return strMaterialGroupCode; }
            set { strMaterialGroupCode = value; }
        }

        public string MaterialGroupName
        {
            get { return strMaterialGroupName; }
            set { strMaterialGroupName = value; }
        }

        public string MaterialTypeCode
        {
            get { return strMaterialTypeCode; }
            set { strMaterialTypeCode = value; }
        }

        public string MaterialTypeName
        {
            get { return strMaterialTypeName; }
            set { strMaterialTypeName = value; }
        }

        public string ConsumableTypeCode
        {
            get { return strConsumableTypeCode; }
            set { strConsumableTypeCode = value; }
        }


        public string ConsumableTypeName
        {
            get { return strConsumableTypeName; }
            set { strConsumableTypeName = value; }
        }

        public string MaterialType
        {
            get { return strMaterialType; }
            set { strMaterialType = value; }
        }

        public string Spec
        {
            get { return strSpec; }
            set { strSpec = value; }
        }

        public frmCOM0001()
        {
            InitializeComponent();
        }

        private void frmCOM0001_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strMaterialCode = "";
            strMaterialName = "";
            strMaterialGroupCode = "";
            strMaterialGroupName = "";
            strMaterialTypeCode = "";
            strMaterialTypeName = "";
            strConsumableTypeCode = "";
            strConsumableTypeName = "";
            strSpec = "";
            strMaterialType = "";

            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);

        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);

                uComboSearchPlant.Enabled = false;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridMaterial, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                
                grd.mfSetGridColumn(this.uGridMaterial, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialCode", "자재코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialName", "자재명", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialGroupCode", "자재그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialGroupName", "자재그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialTypeCode", "자재유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialTypeName", "자재유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "MaterialType", "부품유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMaterial, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 150, false, true, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridMaterial.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMaterial.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion


        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                this.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material material = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(material);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialGroupCode = ""; // this.uComboSearchMaterialGroup.Value.ToString();
                string strMaterialTypeCode = ""; // this.uComboSearchMaterialType.Value.ToString();

                //DataTable dtMaterial = material.mfReadMASMaterialPopup(strPlantCode, strMaterialGroupCode, strMaterialTypeCode, "", m_resSys.GetString("SYS_LANG"));
                DataTable dtMaterial = material.mfReadMASMaterialPopup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uGridMaterial.DataSource = dtMaterial;
                this.uGridMaterial.DataBind();

                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtMaterial.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMaterial, 0);
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();    
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridMaterial_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strMaterialGroupCode = ""; // e.Row.Cells["MaterialGroupCode"].Text.ToString();
                strMaterialGroupName = ""; //e.Row.Cells["MaterialGroupName"].Text.ToString();
                strMaterialTypeCode = ""; //e.Row.Cells["MaterialTypeCode"].Text.ToString();
                strMaterialTypeName = ""; //e.Row.Cells["MaterialTypeName"].Text.ToString();

                strConsumableTypeCode = e.Row.Cells["ConsumableTypeCode"].Text.ToString();
                strConsumableTypeName = ""; //e.Row.Cells["ConsumableTypeName"].Text.ToString();

                strMaterialCode = e.Row.Cells["MaterialCode"].Text.ToString();
                strMaterialName = e.Row.Cells["MaterialName"].Text.ToString();

                strConsumableTypeCode = e.Row.Cells["ConsumableTypeCode"].Text.ToString();
                strMaterialType = e.Row.Cells["MaterialType"].Text.ToString();

                strSpec = ""; // e.Row.Cells["Spec"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridMaterial.ActiveRow == null) return;

                if (this.uGridMaterial.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridMaterial.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridMaterial.ActiveRow.Cells["PlantName"].Text.ToString();
                    strMaterialGroupCode = this.uGridMaterial.ActiveRow.Cells["MaterialGroupCode"].Text.ToString();
                    strMaterialGroupName = this.uGridMaterial.ActiveRow.Cells["MaterialGroupName"].Text.ToString();
                    strMaterialTypeCode = this.uGridMaterial.ActiveRow.Cells["MaterialTypeCode"].Text.ToString();
                    strMaterialTypeName = this.uGridMaterial.ActiveRow.Cells["MaterialTypeName"].Text.ToString();

                    strConsumableTypeCode = this.uGridMaterial.ActiveRow.Cells["ConsumableTypeCode"].Text.ToString();
                    strConsumableTypeName = this.uGridMaterial.ActiveRow.Cells["ConsumableTypeName"].Text.ToString();

                    strMaterialCode = this.uGridMaterial.ActiveRow.Cells["MaterialCode"].Text.ToString();
                    strMaterialName = this.uGridMaterial.ActiveRow.Cells["MaterialName"].Text.ToString();
                    strSpec = this.uGridMaterial.ActiveRow.Cells["Spec"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
            
        }


    }
}
