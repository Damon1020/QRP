﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0002.cs                                         */
/* 프로그램명   : 제품정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0002 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //자재정보 속성
        private string strPlantCode;
        private string strPlantName;
        private string strProductCode;
        private string strProductName;
        private string strCustomerCode;
        private string strCustomerName;
        private string strMaterialGroupCode;
        private string strMaterialGroupName;
        private string strMaterialTypeCode;
        private string strMaterialTypeName;

        private string strPackageGroupCode;        
        //private string strPackageGroupName;        
        //private string strPackageTypeCode;        
        //private string strPackageTypeName;        
        //private string strFamilyCode;
        //private string strFamilyName;
        //private string strSpec;

        private string strPackage;
        private string strProcessFlowName;
        private string strDensity;
        private string strStackKind;
        private string strStackSeq;
        private string strGeneration;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string ProductCode
        {
            get { return strProductCode; }
            set { strProductCode = value; }
        }

        public string ProductName
        {
            get { return strProductName; }
            set { strProductName = value; }
        }

        public string CustomerCode
        {
            get { return strCustomerCode; }
            set { strCustomerCode = value; }
        }

        public string CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        public string MaterialGroupCode
        {
            get { return strMaterialGroupCode; }
            set { strMaterialGroupCode = value; }
        }

        public string MaterialGroupName
        {
            get { return strMaterialGroupName; }
            set { strMaterialGroupName = value; }
        }

        public string MaterialTypeCode
        {
            get { return strMaterialTypeCode; }
            set { strMaterialTypeCode = value; }
        }

        public string MaterialTypeName
        {
            get { return strMaterialTypeName; }
            set { strMaterialTypeName = value; }
        }


        public string PackageGroupCode
        {
            get { return strPackageGroupCode; }
            set { strPackageGroupCode = value; }
        }

        public string Package
        {
            get { return strPackage; }
            set { strPackage = value; }
        }

        public string ProcessFlowName
        {
            get { return strProcessFlowName; }
            set { strProcessFlowName = value; }
        }

        public string Density
        {
            get { return strDensity; }
            set { strDensity = value; }
        }

        public string StackKind
        {
            get { return strStackKind; }
            set { strStackKind = value; }
        }

        public string StackSeq
        {
            get { return strStackSeq; }
            set { strStackSeq = value; }
        }

        public string Generation
        {
            get { return strGeneration; }
            set { strGeneration = value; }
        }

        //public string PackageGroupName
        //{
        //    get { return strPackageGroupName; }
        //    set { strPackageGroupName = value; }
        //}

        //public string PackageTypeCode
        //{
        //    get { return strPackageTypeCode; }
        //    set { strPackageTypeCode = value; }
        //}

        //public string PackageTypeName
        //{
        //    get { return strPackageTypeName; }
        //    set { strPackageTypeName = value; }
        //}

        //public string FamilyCode
        //{
        //    get { return strFamilyCode; }
        //    set { strFamilyCode = value; }
        //}

        //public string FamilyName
        //{
        //    get { return strFamilyName; }
        //    set { strFamilyName = value; }
        //}


        //public string Spec
        //{
        //    get { return strSpec; }
        //    set { strSpec = value; }
        //}

        public frmCOM0002()
        {
            InitializeComponent();
        }

        private void frmCOM0002_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strProductCode = "";
            strProductName = "";
            strCustomerCode = "";
            strCustomerName = "";
            strMaterialGroupCode = "";
            strMaterialGroupName = "";
            strMaterialTypeCode = "";
            strMaterialTypeName = "";
            strPackageGroupCode = "";
            //strPackageGroupName = "";
            //strPackageTypeCode = "";
            //strPackageTypeName = "";
            //strFamilyCode = "";
            //strFamilyName = "";
            //strSpec = "";

            strPackage = "";
            strProcessFlowName = "";
            strDensity = "";
            strStackKind = "";
            strStackSeq = "";
            strGeneration = "";

            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            if (strPlantCode == null)
            {
                strPlantCode = "";
            }

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;
                //if (strPlantCode != null)
                //{
                //    if (!PlantCode.Equals(string.Empty))
                //    {
                //        for (int i = 0; i < uComboSearchPlant.Items.Count; i++)
                //        {
                //            uComboSearchPlant.SelectedIndex = i;
                //            if (uComboSearchPlant.Value.ToString().Equals(PlantCode))
                //            {
                //                uComboSearchPlant.SelectedIndex = i;
                //                break;
                //            }
                //        }
                //    }
                //}           
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridProduct, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridProduct, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                
                
                ////grd.mfSetGridColumn(this.uGridProduct, 0, "MaterialGroupCode", "제품그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridProduct, 0, "MaterialGroupName", "제품그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridProduct, 0, "MaterialTypeCode", "제품유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridProduct, 0, "MaterialTypeName", "제품유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductCode", "제품코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductName", "제품명", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PackageGroupCode", "PackageGroup", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "CustomerCode", "고객코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridProduct, 0, "CustomerName", "고객명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                //    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProcessFlowName", "공정흐름", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "Density", "Density", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "STACKKIND", "스택구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "STACKSEQ", "스택Seq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "GENERATION", "세대", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridProduct.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridProduct.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                this.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product product = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(product);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMaterialGroupCode = ""; // this.uComboSearchMaterialGroup.Value.ToString();
                string strMaterialTypeCode = ""; // this.uComboSearchMaterialType.Value.ToString();

                //DataTable dtMaterial = product.mfReadMASProductPopup(PlantCode, strMaterialGroupCode, strMaterialTypeCode, "", "", "", m_resSys.GetString("SYS_LANG"));
                DataTable dtMaterial = product.mfReadMASProductPopup(PlantCode, m_resSys.GetString("SYS_LANG"));

                this.uGridProduct.DataSource = dtMaterial;
                this.uGridProduct.DataBind();

                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtMaterial.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridProduct, 0);
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridProduct_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();

                strMaterialGroupCode = ""; // e.Row.Cells["MaterialGroupCode"].Text.ToString();
                strMaterialGroupName = ""; //e.Row.Cells["MaterialGroupName"].Text.ToString();
                strMaterialTypeCode = ""; //e.Row.Cells["MaterialTypeCode"].Text.ToString();
                strMaterialTypeName = ""; //e.Row.Cells["MaterialTypeName"].Text.ToString();

                strProductCode = e.Row.Cells["ProductCode"].Text.ToString();
                strProductName = e.Row.Cells["ProductName"].Text.ToString();
                //strSpec = e.Row.Cells["Spec"].Text.ToString();
                strCustomerCode = e.Row.Cells["CustomerCode"].Text.ToString();
                strCustomerName = ""; // e.Row.Cells["CustomerName"].Text.ToString();

                strPackageGroupCode = e.Row.Cells["PackageGroupCode"].Text.ToString();
                //strPackageGroupName = e.Row.Cells["PackageGroupName"].Text.ToString();
                //strPackageTypeCode = e.Row.Cells["PackageTypeCode"].Text.ToString();
                //strPackageTypeName = e.Row.Cells["PackageTypeName"].Text.ToString();
                //strFamilyCode = e.Row.Cells["FamilyCode"].Text.ToString();
                //strFamilyName = e.Row.Cells["FamilyName"].Text.ToString();
                strPackage = e.Row.Cells["Package"].Text.ToString();
                strProcessFlowName = e.Row.Cells["ProcessFlowName"].Text.ToString();
                strDensity = e.Row.Cells["Density"].Text.ToString();
                strStackKind = e.Row.Cells["StackKind"].Text.ToString();
                strStackSeq = e.Row.Cells["StackSeq"].Text.ToString();
                strGeneration = e.Row.Cells["Generation"].Text.ToString();

                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridProduct.ActiveRow == null) return;

                if (this.uGridProduct.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridProduct.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridProduct.ActiveRow.Cells["PlantName"].Text.ToString();

                    strMaterialGroupCode = ""; //this.uGridProduct.ActiveRow.Cells["MaterialGroupCode"].Text.ToString();
                    strMaterialGroupName = ""; //this.uGridProduct.ActiveRow.Cells["MaterialGroupName"].Text.ToString();
                    strMaterialTypeCode = ""; //this.uGridProduct.ActiveRow.Cells["MaterialTypeCode"].Text.ToString();
                    strMaterialTypeName = ""; //this.uGridProduct.ActiveRow.Cells["MaterialTypeName"].Text.ToString();

                    strProductCode = this.uGridProduct.ActiveRow.Cells["ProductCode"].Text.ToString();
                    strProductName = this.uGridProduct.ActiveRow.Cells["ProductName"].Text.ToString();
                    //strSpec = this.uGridProduct.ActiveRow.Cells["Spec"].Text.ToString();
                    strCustomerCode = this.uGridProduct.ActiveRow.Cells["CustomerCode"].Text.ToString();
                    strCustomerName = ""; // this.uGridProduct.ActiveRow.Cells["CustomerName"].Text.ToString();

                    strPackageGroupCode = this.uGridProduct.ActiveRow.Cells["PackageGroupCode"].Text.ToString();
                    //strPackageGroupName = this.uGridProduct.ActiveRow.Cells["PackageGroupName"].Text.ToString();
                    //strPackageTypeCode = this.uGridProduct.ActiveRow.Cells["PackageTypeCode"].Text.ToString();
                    //strPackageTypeName = this.uGridProduct.ActiveRow.Cells["PackageTypeName"].Text.ToString();
                    //strFamilyCode = this.uGridProduct.ActiveRow.Cells["FamilyCode"].Text.ToString();
                    //strFamilyName = this.uGridProduct.ActiveRow.Cells["FamilyName"].Text.ToString();

                    strPackage = this.uGridProduct.ActiveRow.Cells["Package"].Text.ToString();
                    strProcessFlowName = this.uGridProduct.ActiveRow.Cells["ProcessFlowName"].Text.ToString();
                    strDensity = this.uGridProduct.ActiveRow.Cells["Density"].Text.ToString();
                    strStackKind = this.uGridProduct.ActiveRow.Cells["StackKind"].Text.ToString();
                    strStackSeq = this.uGridProduct.ActiveRow.Cells["StackSeq"].Text.ToString();
                    strGeneration = this.uGridProduct.ActiveRow.Cells["Generation"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }

        }
    }
}
