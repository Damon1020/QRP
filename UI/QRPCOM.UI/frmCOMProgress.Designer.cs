﻿namespace QRPCOM.UI
{
    partial class frmCOMProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.uLabelTime = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTimeTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFunction = new Infragistics.Win.Misc.UltraLabel();
            this.timerProgress = new System.Windows.Forms.Timer(this.components);
            this.uActivityIndicator = new Infragistics.Win.UltraActivityIndicator.UltraActivityIndicator();
            this.SuspendLayout();
            // 
            // uLabelTime
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextHAlignAsString = "Center";
            this.uLabelTime.Appearance = appearance1;
            this.uLabelTime.AutoSize = true;
            this.uLabelTime.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTime.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTime.Location = new System.Drawing.Point(327, 180);
            this.uLabelTime.Name = "uLabelTime";
            this.uLabelTime.Size = new System.Drawing.Size(0, 0);
            this.uLabelTime.TabIndex = 8;
            // 
            // uLabelTimeTitle
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackColor2 = System.Drawing.Color.Transparent;
            appearance3.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance3.FontData.BoldAsString = "True";
            appearance3.TextHAlignAsString = "Center";
            this.uLabelTimeTitle.Appearance = appearance3;
            this.uLabelTimeTitle.AutoSize = true;
            this.uLabelTimeTitle.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTimeTitle.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTimeTitle.Location = new System.Drawing.Point(264, 180);
            this.uLabelTimeTitle.Name = "uLabelTimeTitle";
            this.uLabelTimeTitle.Size = new System.Drawing.Size(63, 16);
            this.uLabelTimeTitle.TabIndex = 7;
            this.uLabelTimeTitle.Text = "경과시간 : ";
            // 
            // uLabelFunction
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.TextHAlignAsString = "Center";
            this.uLabelFunction.Appearance = appearance2;
            this.uLabelFunction.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelFunction.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelFunction.Location = new System.Drawing.Point(216, 136);
            this.uLabelFunction.Name = "uLabelFunction";
            this.uLabelFunction.Size = new System.Drawing.Size(81, 16);
            this.uLabelFunction.TabIndex = 6;
            // 
            // timerProgress
            // 
            this.timerProgress.Tick += new System.EventHandler(this.timerProgress_Tick);
            // 
            // uActivityIndicator
            // 
            this.uActivityIndicator.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uActivityIndicator.CausesValidation = true;
            this.uActivityIndicator.Location = new System.Drawing.Point(112, 132);
            this.uActivityIndicator.Name = "uActivityIndicator";
            this.uActivityIndicator.Size = new System.Drawing.Size(276, 24);
            this.uActivityIndicator.TabIndex = 5;
            this.uActivityIndicator.TabStop = true;
            // 
            // frmCOMProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::QRPCOM.UI.Properties.Resources.progress;
            this.ClientSize = new System.Drawing.Size(400, 201);
            this.ControlBox = false;
            this.Controls.Add(this.uLabelTime);
            this.Controls.Add(this.uLabelTimeTitle);
            this.Controls.Add(this.uLabelFunction);
            this.Controls.Add(this.uActivityIndicator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmCOMProgress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmCOMProgress_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCOMProgress_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel uLabelTime;
        private Infragistics.Win.Misc.UltraLabel uLabelTimeTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelFunction;
        private System.Windows.Forms.Timer timerProgress;
        private Infragistics.Win.UltraActivityIndicator.UltraActivityIndicator uActivityIndicator;
    }
}