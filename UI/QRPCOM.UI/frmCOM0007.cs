﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0007.cs                                         */
/* 프로그램명   : 금형치공구정보                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;


namespace QRPCOM.UI
{
    public partial class frmCOM0007 : Form
    {
        #region 전역변수
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strDurableMatCode;
        private string strDurableMatName;
        private string strDurableMatTypeCode;
        private string strDurableMatTypeName;
        private string strSpec;
        private string strPackage;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string DurableMatCode
        {
            get { return strDurableMatCode; }
            set { strDurableMatCode = value; }
        }

        public string DurableMatName
        {
            get { return strDurableMatName; }
            set { strDurableMatName = value; }
        }

        public string DurableMatTypeCode
        {
            get { return strDurableMatTypeCode; }
            set { strDurableMatTypeCode = value; }
        }

        public string DurableMatTypeName
        {
            get { return strDurableMatTypeName; }
            set { strDurableMatTypeName = value; }
        }

        public string Spec
        {
            get { return strSpec; }
            set { strSpec = value; }
        }
        public string Package
        {
            get { return strPackage; }
            set { strPackage = value; }
        }

        #endregion

        public frmCOM0007()
        {
            InitializeComponent();
        }

        private void frmCOM0007_Load(object sender, EventArgs e)
        {

            //strPlantCode = "";
            //strPlantName = "";
            strDurableMatCode = "";
            strDurableMatName = "";
            strDurableMatTypeCode = "";
            strDurableMatTypeName = "";
            strSpec = "";
            strPackage = "";
            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;


                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                //QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                //brwChannel.mfCredentials(clsProduct);

                //DataTable dtPakage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //combo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "Package", "ComboName", dtPakage);

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridDurableMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridDurableMat, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatTypeCode", "금형치공구유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatTypeName", "금형치공구유형명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "Spec", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridDurableMat, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //폰트설정
                this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }

        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                
                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strDurableName = this.uComboDurableMatName.Value.ToString();

                //DataTable dtDurableMat = clsDurableMat.mfReadDurableMat_Package(strPlantCode, "",strPackage,strDurableName ,m_resSys.GetString("SYS_LANG"));
                DataTable dtDurableMat = clsDurableMat.mfReadDurableMat_PSTS(strPlantCode, strPackage, strDurableName, "",m_resSys.GetString("SYS_LANG"));

                this.uGridDurableMat.DataSource = dtDurableMat;
                this.uGridDurableMat.DataBind();
                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtDurableMat.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableMat, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Event
        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridDurableMat.ActiveRow == null) return;

                if (this.uGridDurableMat.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridDurableMat.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridDurableMat.ActiveRow.Cells["PlantName"].Text.ToString();
                    strDurableMatCode = this.uGridDurableMat.ActiveRow.Cells["DurableMatCode"].Text.ToString();
                    strDurableMatName = this.uGridDurableMat.ActiveRow.Cells["DurableMatName"].Text.ToString();
                    strDurableMatTypeCode = this.uGridDurableMat.ActiveRow.Cells["DurableMatTypeCode"].Text.ToString();
                    strDurableMatTypeName = this.uGridDurableMat.ActiveRow.Cells["DurableMatTypeName"].Text.ToString();
                    strSpec = this.uGridDurableMat.ActiveRow.Cells["Spec"].Text.ToString();
                    strPackage = this.uGridDurableMat.ActiveRow.Cells["Package"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
            
        }

        private void uGridDurableMat_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strDurableMatCode = e.Row.Cells["DurableMatCode"].Text.ToString();
                strDurableMatName = e.Row.Cells["DurableMatName"].Text.ToString();
                strDurableMatTypeCode = e.Row.Cells["DurableMatTypeCode"].Text.ToString();
                strDurableMatTypeName = e.Row.Cells["DurableMatTypeName"].Text.ToString();
                strSpec = e.Row.Cells["Spec"].Text.ToString();
                strPackage = e.Row.Cells["Package"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
        #endregion

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboSearchPackage.Items.Count > 0)
                    this.uComboSearchPackage.Items.Clear();

                if (this.uComboDurableMatName.Items.Count > 0)
                    this.uComboDurableMatName.Items.Clear();

                QRPBrowser brwChannel = new QRPBrowser();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                //제품정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);


                //패키지콤보매서드 호출
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "Package", "ComboName", dtPackage);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtDurableMatName = clsDurableMat.mfReadDurableMatNameCombo(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                if (this.uComboDurableMatName.Items.Count > 0)
                    this.uComboDurableMatName.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                   , "", "", "전체", "DurableMat", "DurableMatName", dtDurableMatName);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
