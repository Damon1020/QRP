﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0004.cs                                         */
/* 프로그램명   : 거래처정보                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0004 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strVendorCode;
        private string strVendorName;
        private string strAddress;
        private string strRegNo;
        private string strBossName;
        private string strTel;
        private string strFax;

        public string CustomerCode
        {
            get { return strVendorCode; }
            set { strVendorCode = value; }
        }

        public string CustomerName
        {
            get { return strVendorName; }
            set { strVendorName = value; }
        }

        public string RegNo
        {
            get { return strRegNo; }
            set { strRegNo = value; }
        }

        public string BossName
        {
            get { return strBossName; }
            set { strBossName = value; }
        }

        public string Tel
        {
            get { return strTel; }
            set { strTel = value; }
        }

        public string Fax
        {
            get { return strFax; }
            set { strFax = value; }
        }

        public string Address
        {
            get { return strAddress; }
            set { strAddress = value; }
        }

        public frmCOM0004()
        {
            InitializeComponent();
        }

        private void frmCOM0004_Load(object sender, EventArgs e)
        {
            strVendorCode = "";
            strVendorName = "";
            strRegNo = "";
            strBossName = "";
            strTel = "";
            strFax = "";
            strAddress = "";
            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinLabel label = new WinLabel();

                //label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //WinComboEditor combo = new WinComboEditor();

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //brwChannel.mfCredentials(clsPlant);

                //DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //// 공장 콤보박스
                //combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "PlantCode", "PlantName", dt);
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridVendor, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--고객정보
                grd.mfSetGridColumn(this.uGridVendor, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridVendor, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridVendor, 0, "BossName", "대표자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridVendor, 0, "Tel", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridVendor, 0, "Fax", "팩스번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridVendor, 0, "Address", "주소", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridVendor.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridVendor.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsvendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsvendor);

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtvendor = clsvendor.mfReadVendorPopup(m_resSys.GetString("SYS_LANG"));
                this.uGridVendor.DataSource = dtvendor;
                this.uGridVendor.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtvendor.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridVendor, 0);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridVendor_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strVendorCode = e.Row.Cells["VendorCode"].Text.ToString();
                strVendorName = e.Row.Cells["VendorName"].Text.ToString();
                strTel = e.Row.Cells["Tel"].Text.ToString();
                strFax = e.Row.Cells["Fax"].Text.ToString();
                strAddress = e.Row.Cells["Address"].Text.ToString();
                strBossName = e.Row.Cells["BossName"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridVendor.ActiveRow == null) return;

                if (this.uGridVendor.ActiveRow.Index >= 0)
                {
                    strVendorCode = this.uGridVendor.ActiveRow.Cells["VendorCode"].Text.ToString();
                    strVendorName = this.uGridVendor.ActiveRow.Cells["VendorName"].Text.ToString();
                    strTel = this.uGridVendor.ActiveRow.Cells["Tel"].Text.ToString();
                    strFax = this.uGridVendor.ActiveRow.Cells["Fax"].Text.ToString();
                    strAddress = this.uGridVendor.ActiveRow.Cells["Address"].Text.ToString();
                    strBossName = this.uGridVendor.ActiveRow.Cells["BossName"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
    }
}
