﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0005.cs                                         */
/* 프로그램명   : 설비정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : 2011-11-10 : 위치필드를 가장 왼쪽으로 수정 (정결)     */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0005 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strEquipCode;
        private string strEquipName;
        private string strAreaCode;
        private string strAreaName;
        private string strStationCode;
        private string strStationName;
        private string strEquipLocCode;
        private string strEquipLocName;
        private string strEquipProcGubunCode;
        private string strEquipProcGubunName;
        private string strEquipTypeCode;
        private string strEquipTypeName;
        private string strEquipGroupCode;
        private string strEquipGroupName;
        private string strSuperEquipCode;
        private string strModelName;
        private string strSerialNo;
        private string strGRDate;
        private string strMakeYear;
        private string strEquipLevelCode;
        private string strVendorCode;
        private string strVendorName;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }
        
        public string EquipCode
        {
            get { return strEquipCode; }
            set { strEquipCode = value; }
        }
        
        public string EquipName
        {
            get { return strEquipName; }
            set { strEquipName = value; }
        }
        
        public string AreaCode
        {
            get { return strAreaCode; }
            set { strAreaCode = value; }
        }
        
        public string AreaName
        {
            get { return strAreaName; }
            set { strAreaName = value; }
        }
        
        public string StationCode
        {
            get { return strStationCode; }
            set { strStationCode = value; }
        }
        
        public string StationName
        {
            get { return strStationName; }
            set { strStationName = value; }
        }
        
        public string EquippLocCode
        {
            get { return strEquipLocCode; }
            set { strEquipLocCode = value; }
        }
        
        public string EquipLocName
        {
            get { return strEquipLocName; }
            set { strEquipLocName = value; }
        }
        
        public string EquipProcGubunCode
        {
            get { return strEquipProcGubunCode; }
            set { strEquipProcGubunCode = value; }
        }
        
        public string EquipProcGubunName
        {
            get { return strEquipProcGubunName; }
            set { strEquipProcGubunName = value; }
        }
        
        public string EquipTypeCode
        {
            get { return strEquipTypeCode; }
            set { strEquipTypeCode = value; }
        }
        
        public string EquipTypeName
        {
            get { return strEquipTypeName; }
            set { strEquipTypeName = value; }
        }
        
        public string EquipGroupCode
        {
            get { return strEquipGroupCode; }
            set { strEquipGroupCode = value; }
        }
        
        public string EquipGroupName
        {
            get { return strEquipGroupName; }
            set { strEquipGroupName = value; }
        }
        
        public string SuperEquipCode
        {
            get { return strSuperEquipCode; }
            set { strSuperEquipCode = value; }
        }
        
        public string ModelName
        {
            get { return strModelName; }
            set { strModelName = value; }
        }
        
        public string SerialNo
        {
            get { return strSerialNo; }
            set { strSerialNo = value; }
        }
        
        public string GRDate
        {
            get { return strGRDate; }
            set { strGRDate = value; }
        }
        
        public string MakeYear
        {
            get { return strMakeYear; }
            set { strMakeYear = value; }
        }
        
        public string EquipLevelCode
        {
            get { return strEquipLevelCode; }
            set { strEquipLevelCode = value; }
        }
        
        public string VendorCode
        {
            get { return strVendorCode; }
            set { strVendorCode = value; }
        }
        
        public string VendorName
        {
            get { return strVendorName; }
            set { strVendorName = value; }
        }

        public frmCOM0005()
        {
            InitializeComponent();
        }

        private void frmCOM0005_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strEquipCode = "";
            strEquipName = "";
            strAreaCode = "";
            strAreaName = "";
            strStationCode = "";
            strStationName = "";
            strEquipLevelCode = "";
            strEquipLocName = "";
            strEquipProcGubunCode = "";
            strEquipProcGubunName = "";
            strEquipTypeCode = "";
            strEquipTypeName = "";
            strEquipGroupCode = "";
            strEquipGroupName = "";
            strSuperEquipCode = "";
            strModelName = "";
            strSerialNo = "";
            strGRDate = "";
            strMakeYear = "";
            strEquipLevelCode = "";
            strVendorCode = "";
            strVendorName = "";

            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            if (strPlantCode == null)
            {
                strPlantCode = "";
            }
            
            this.Text = "설비정보 검색창";

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);

                this.uLabelEquipProcGubun.Visible = false;
                this.uComboEquipProcGubun.Visible = false;

                //this.uLabelEquipLocation.Visible = false;
                //this.uComboEquipLocation.Visible = false;

                //this.uLabelEquipType.Visible = false;
                //this.uComboEquipType.Visible = false;

                this.uLabelArea.Visible = false;
                this.uComboArea.Visible = false;

            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboPlant.Enabled = false;
                //if (strPlantCode != null)
                //{
                //    if (!PlantCode.Equals(string.Empty))
                //    {
                //        for (int i = 0; i < uComboSearchPlant.Items.Count; i++)
                //        {
                //            uComboSearchPlant.SelectedIndex = i;
                //            if (uComboSearchPlant.Value.ToString().Equals(PlantCode))
                //            {
                //                uComboSearchPlant.SelectedIndex = i;
                //                break;
                //            }
                //        }
                //    }
                //}
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridEquip, 0, "PlantCode", "공장코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "AreaCode", "Area코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupCode", "설비그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "StationCode", "Station코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipTypeCode", "설비유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipTypeName", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipProcGubunCode", "설비공정구분코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "SuperEquipCode", "Super설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "VendorCode", "Vendor코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "VendorName", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GRDate", "STS입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "MakeYear", "제작년도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLevelCode", "설비등급", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "ModelName", "모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                //폰트설정
                uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //검색조건
                string strFactoryCode = uComboPlant.Value.ToString();
                //string strAreaCode = uComboArea.Value.ToString();
                string strSta = uComboStation.Value.ToString();
                string strLoc = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboSearchProcess.Value.ToString();
                string strEquipLargeType = this.uComboSearchLargeType.Value.ToString();
                string strGroup = this.uComboSearchEquipGroup.Value.ToString();

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip equip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(equip);

                //매서드호출
                DataTable dtEquip = equip.mfReadEquip_POPUP(strFactoryCode, strSta, strLoc, strProcessGroup, strEquipLargeType, strGroup,m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridEquip.DataSource = dtEquip; 
                this.uGridEquip.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();

                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtEquip.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                { 
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquip, 0);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uGridEquip_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strEquipCode = e.Row.Cells["EquipCode"].Text.ToString();
                strEquipName = e.Row.Cells["EquipName"].Text.ToString();
                strAreaCode = e.Row.Cells["AreaCode"].Text.ToString();
                strAreaName = e.Row.Cells["AreaName"].Text.ToString();
                strStationCode = e.Row.Cells["StationCode"].Text.ToString();
                strStationName = e.Row.Cells["StationName"].Text.ToString();
                strEquipLocCode = e.Row.Cells["EquipLocCode"].Text.ToString();
                strEquipLocName = e.Row.Cells["EquipLocName"].Text.ToString();
                strEquipProcGubunCode = e.Row.Cells["EquipProcGubunCode"].Text.ToString();
                strEquipProcGubunName = e.Row.Cells["EquipProcGubunName"].Text.ToString();
                strEquipTypeCode = e.Row.Cells["EquipTypeCode"].Text.ToString();
                strEquipTypeName = e.Row.Cells["EquipTypeName"].Text.ToString();
                strEquipGroupCode = e.Row.Cells["EquipGroupCode"].Text.ToString();
                strEquipGroupName = e.Row.Cells["EquipGroupName"].Text.ToString();
                strSuperEquipCode = e.Row.Cells["SuperEquipCode"].Text.ToString();
                strModelName = e.Row.Cells["ModelName"].Text.ToString();
                strMakeYear = e.Row.Cells["MakeYear"].Text.ToString();
                strEquipLevelCode = e.Row.Cells["EquipLevelCode"].Text.ToString();
                strVendorCode = e.Row.Cells["VendorCode"].Text.ToString();
                strVendorName = e.Row.Cells["VendorName"].Text.ToString();
                strSerialNo = e.Row.Cells["SerialNo"].Text.ToString();
                strGRDate = e.Row.Cells["GRDate"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridEquip.ActiveRow == null) return;

                if (this.uGridEquip.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridEquip.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridEquip.ActiveRow.Cells["PlantName"].Text.ToString();
                    strEquipCode = this.uGridEquip.ActiveRow.Cells["EquipCode"].Text.ToString();
                    strEquipName = this.uGridEquip.ActiveRow.Cells["EquipName"].Text.ToString();
                    strAreaCode = this.uGridEquip.ActiveRow.Cells["AreaCode"].Text.ToString();
                    strAreaName = this.uGridEquip.ActiveRow.Cells["AreaName"].Text.ToString();
                    strStationCode = this.uGridEquip.ActiveRow.Cells["StationCode"].Text.ToString();
                    strStationName = this.uGridEquip.ActiveRow.Cells["StationName"].Text.ToString();
                    strEquipLocCode = this.uGridEquip.ActiveRow.Cells["EquipLocCode"].Text.ToString();
                    strEquipLocName = this.uGridEquip.ActiveRow.Cells["EquipLocName"].Text.ToString();
                    strEquipProcGubunCode = this.uGridEquip.ActiveRow.Cells["EquipProcGubunCode"].Text.ToString();
                    strEquipProcGubunName = this.uGridEquip.ActiveRow.Cells["EquipProcGubunName"].Text.ToString();
                    strEquipTypeCode = this.uGridEquip.ActiveRow.Cells["EquipTypeCode"].Text.ToString();
                    strEquipTypeName = this.uGridEquip.ActiveRow.Cells["EquipTypeName"].Text.ToString();
                    strEquipGroupCode = this.uGridEquip.ActiveRow.Cells["EquipGroupCode"].Text.ToString();
                    strEquipGroupName = this.uGridEquip.ActiveRow.Cells["EquipGroupName"].Text.ToString();
                    strSuperEquipCode = this.uGridEquip.ActiveRow.Cells["SuperEquipCode"].Text.ToString();
                    strModelName = this.uGridEquip.ActiveRow.Cells["ModelName"].Text.ToString();
                    strMakeYear = this.uGridEquip.ActiveRow.Cells["MakeYear"].Text.ToString();
                    strEquipLevelCode = this.uGridEquip.ActiveRow.Cells["EquipLevelCode"].Text.ToString();
                    strVendorCode = this.uGridEquip.ActiveRow.Cells["VendorCode"].Text.ToString();
                    strVendorName = this.uGridEquip.ActiveRow.Cells["VendorName"].Text.ToString();
                    strSerialNo = this.uGridEquip.ActiveRow.Cells["SerialNo"].Text.ToString();
                    strGRDate = this.uGridEquip.ActiveRow.Cells["GRDate"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Combo

        //공장선택시 Area Station콤보 변경
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlantCode = uComboPlant.Value.ToString();

                string strPlant = this.uComboPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }
                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor com = new WinComboEditor();

                    //리스트클리어
                    uComboArea.Items.Clear();

                    uComboStation.Items.Clear();

                    //Area
                    //bl호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    brwChannel.mfCredentials(clsArea);
                    //매서드호출
                    DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //ComboBox에 추가
                    com.mfSetComboEditor(this.uComboArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "AreaCode", "AreaName", dtArea);

                    //Station
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    //ComboBox에 추가
                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    com.mfSetComboEditor(this.uComboStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "StationCode", "StationName", dtStation);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //Station이 변경시 위치,설비대분류,설비중분류,설비그룹이바뀜
        private void uComboStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchEquipLoc.Items.Clear();

                //설비위치콤보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsEquipLocation);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                DataTable dtEquipLoc = clsEquipLocation.mfReadLocation_Combo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                WinComboEditor com = new WinComboEditor();

                com.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false, "",
                                      true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLocCode", "EquipLocName", dtEquipLoc);


                mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), "", "", "", 3);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //위치가 변경시 설비대분류,설비중분류,설비그룹이변경
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboSearchProcess.Value.ToString(), "", 3);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비대분류가 변경시 설비중분류,설비그룹이변경
        private void uComboSearchProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                if (!this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                {
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboSearchProcess.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비중분류가 변경시 설비그룹이변경됨
        private void uComboSearchLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uComboSearchLargeType.Value.ToString().Equals(string.Empty))
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboSearchProcess.Value.ToString(), this.uComboSearchLargeType.Value.ToString(), 1);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");

                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboSearchProcess.Items.Clear();
                    this.uComboSearchLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
