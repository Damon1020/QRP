﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0009.cs                                         */
/* 프로그램명   : 검사항목정보                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0009 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strInspectGroupCode;
        private string strInspectGroupName;
        private string strInspectTypeCode;
        private string strInspectTypeName;
        private string strInspectItemCode;
        private string strInspectItemName;
        private string strInspectCondition;
        private string strInspectMethod;
        private string strDataTypeCode;
        private string strDataTypeName;
        private int intUseDecimalPoint;
        private string strUnitCode;
        private string strUnitName;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        
        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }
        
        public string InspectGroupCode
        {
            get { return strInspectGroupCode; }
            set { strInspectGroupCode = value; }
        }
        
        public string InspectGroupName
        {
            get { return strInspectGroupName; }
            set { strInspectGroupName = value; }
        }
        
        public string InspectTypeCode
        {
            get { return strInspectTypeCode; }
            set { strInspectTypeCode = value; }
        }
        
        public string InspectTypeName
        {
            get { return strInspectTypeName; }
            set { strInspectTypeName = value; }
        }
        
        public string InspectItemCode
        {
            get { return strInspectItemCode; }
            set { strInspectItemCode = value; }
        }
        
        public string InspectItemName
        {
            get { return strInspectItemName; }
            set { strInspectItemName = value; }
        }
        
        public string InspectCondition
        {
            get { return strInspectCondition; }
            set { strInspectCondition = value; }
        }
        
        public string InspectMethod
        {
            get { return strInspectMethod; }
            set { strInspectMethod = value; }
        }
        
        public string DataTypeCode
        {
            get { return strDataTypeCode; }
            set { strDataTypeCode = value; }
        }
        
        public string DataTypeName
        {
            get { return strDataTypeName; }
            set { strDataTypeName = value; }
        }
        
        public int UseDecimalPoint
        {
            get { return intUseDecimalPoint; }
            set { intUseDecimalPoint = value; }
        }
        
        public string UnitCode
        {
            get { return strUnitCode; }
            set { strUnitCode = value; }
        }
        
        public string UnitName
        {
            get { return strUnitName; }
            set { strUnitName = value; }
        }

        public frmCOM0009()
        {
            InitializeComponent();
        }

        private void frmCOM0009_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strInspectGroupCode = "";
            strInspectGroupName = "";
            strInspectTypeCode = "";
            strInspectTypeName = "";
            strInspectItemCode = "";
            strInspectItemName = "";
            strInspectCondition = "";
            strInspectMethod = "";
            strDataTypeCode = "";
            strDataTypeName = "";
            intUseDecimalPoint = 0;
            strUnitCode = "";
            strUnitName = "";
            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridInspectItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼추가
                grd.mfSetGridColumn(this.uGridInspectItem, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectItemName", "검사항목명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "InspectMethod", "검사방법", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "DataTypeCode", "데이타유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 2, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "DataTypeName", "데이타유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 2, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "UseDecimalPoint", "허용소수점자리수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 10, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItem, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //폰트설정
                uGridInspectItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInspectItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem clsinspectitem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(clsinspectitem);

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dtinspectitem = clsinspectitem.mfReadMASInspectItemPopup(strPlantCode, "", "", m_resSys.GetString("SYS_LANG"));
                this.uGridInspectItem.DataSource = dtinspectitem;
                this.uGridInspectItem.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtinspectitem.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInspectItem, 0);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridInspectItem_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strInspectGroupCode = e.Row.Cells["InspectGroupCode"].Text.ToString();
                strInspectGroupName = e.Row.Cells["InspectGroupName"].Text.ToString();
                strInspectTypeCode = e.Row.Cells["InspectTypeCode"].Text.ToString();
                strInspectTypeName = e.Row.Cells["InspectTypeName"].Text.ToString();
                strInspectItemCode = e.Row.Cells["InspectItemCode"].Text.ToString();
                strInspectItemName = e.Row.Cells["InspectItemName"].Text.ToString();
                strInspectCondition = e.Row.Cells["InspectCondition"].Text.ToString();
                strInspectMethod = e.Row.Cells["InspectMethod"].Text.ToString();
                strDataTypeCode = e.Row.Cells["DataTypeCode"].Text.ToString();
                strDataTypeName = e.Row.Cells["DataTypeName"].Text.ToString();
                intUseDecimalPoint = Convert.ToInt32(e.Row.Cells["UseDecimalPoint"].Text.ToString());
                strUnitCode = e.Row.Cells["UnitCode"].Text.ToString();
                strUnitName = e.Row.Cells["UnitName"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridInspectItem.ActiveRow == null) return;

                if (this.uGridInspectItem.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridInspectItem.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridInspectItem.ActiveRow.Cells["PlantName"].Text.ToString();
                    strInspectGroupCode = this.uGridInspectItem.ActiveRow.Cells["InspectGroupCode"].Text.ToString();
                    strInspectGroupName = this.uGridInspectItem.ActiveRow.Cells["InspectGroupName"].Text.ToString();
                    strInspectTypeCode = this.uGridInspectItem.ActiveRow.Cells["InspectTypeCode"].Text.ToString();
                    strInspectTypeName = this.uGridInspectItem.ActiveRow.Cells["InspectTypeName"].Text.ToString();
                    strInspectItemCode = this.uGridInspectItem.ActiveRow.Cells["InspectItemCode"].Text.ToString();
                    strInspectItemName = this.uGridInspectItem.ActiveRow.Cells["InspectItemName"].Text.ToString();
                    strInspectCondition = this.uGridInspectItem.ActiveRow.Cells["InspectCondition"].Text.ToString();
                    strInspectMethod = this.uGridInspectItem.ActiveRow.Cells["InspectMethod"].Text.ToString();
                    strDataTypeCode = this.uGridInspectItem.ActiveRow.Cells["DataTypeCode"].Text.ToString();
                    strDataTypeName = this.uGridInspectItem.ActiveRow.Cells["DataTypeName"].Text.ToString();
                    intUseDecimalPoint = Convert.ToInt32(this.uGridInspectItem.ActiveRow.Cells["UseDecimalPoint"].Text.ToString());
                    strUnitCode = this.uGridInspectItem.ActiveRow.Cells["UnitCode"].Text.ToString();
                    strUnitName = this.uGridInspectItem.ActiveRow.Cells["UnitName"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
    }
}
