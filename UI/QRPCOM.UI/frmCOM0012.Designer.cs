﻿namespace QRPCOM.UI
{
    partial class frmCOM0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCOM0012));
            this.uGroupBoxForm = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonSendRA = new Infragistics.Win.Misc.UltraButton();
            this.uComboSaveTerm = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboDocClassName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonSendSI = new Infragistics.Win.Misc.UltraButton();
            this.rdDept = new System.Windows.Forms.RadioButton();
            this.rdPerson = new System.Windows.Forms.RadioButton();
            this.uButtonCc = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSendSP = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSendSS = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSendSA = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxCcLine = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonCcLineDel = new Infragistics.Win.Misc.UltraButton();
            this.uGridCcLine = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSendLine = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonSendLineDel = new Infragistics.Win.Misc.UltraButton();
            this.uGridSendLine = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxResult = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearch = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboDeptName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonRefresh = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uCheckDept = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelDeptSearch = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDept = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUserName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonCancel = new Infragistics.Win.Misc.UltraButton();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uComboCompany = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxComment = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxForm)).BeginInit();
            this.uGroupBoxForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSaveTerm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDocClassName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCcLine)).BeginInit();
            this.uGroupBoxCcLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCcLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSendLine)).BeginInit();
            this.uGroupBoxSendLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSendLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).BeginInit();
            this.uGroupBoxResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearch)).BeginInit();
            this.uGroupBoxSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxComment)).BeginInit();
            this.uGroupBoxComment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxForm
            // 
            this.uGroupBoxForm.Controls.Add(this.uGroupBoxComment);
            this.uGroupBoxForm.Controls.Add(this.uButtonSendRA);
            this.uGroupBoxForm.Controls.Add(this.uComboSaveTerm);
            this.uGroupBoxForm.Controls.Add(this.uComboDocClassName);
            this.uGroupBoxForm.Controls.Add(this.uButtonSendSI);
            this.uGroupBoxForm.Controls.Add(this.rdDept);
            this.uGroupBoxForm.Controls.Add(this.rdPerson);
            this.uGroupBoxForm.Controls.Add(this.uButtonCc);
            this.uGroupBoxForm.Controls.Add(this.uButtonSendSP);
            this.uGroupBoxForm.Controls.Add(this.uButtonSendSS);
            this.uGroupBoxForm.Controls.Add(this.uButtonSendSA);
            this.uGroupBoxForm.Controls.Add(this.uGroupBoxCcLine);
            this.uGroupBoxForm.Controls.Add(this.uGroupBoxSendLine);
            this.uGroupBoxForm.Controls.Add(this.uGroupBoxResult);
            this.uGroupBoxForm.Controls.Add(this.uGroupBoxSearch);
            this.uGroupBoxForm.Controls.Add(this.uButtonCancel);
            this.uGroupBoxForm.Controls.Add(this.uButtonOK);
            this.uGroupBoxForm.Controls.Add(this.uComboCompany);
            this.uGroupBoxForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBoxForm.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxForm.Name = "uGroupBoxForm";
            this.uGroupBoxForm.Size = new System.Drawing.Size(876, 634);
            this.uGroupBoxForm.TabIndex = 0;
            this.uGroupBoxForm.Text = "uGroupBoxForm";
            // 
            // uButtonSendRA
            // 
            this.uButtonSendRA.Location = new System.Drawing.Point(400, 208);
            this.uButtonSendRA.Name = "uButtonSendRA";
            this.uButtonSendRA.Size = new System.Drawing.Size(84, 28);
            this.uButtonSendRA.TabIndex = 16;
            this.uButtonSendRA.Text = "ultraButton1";
            this.uButtonSendRA.Click += new System.EventHandler(this.uButtonSendRA_Click);
            // 
            // uComboSaveTerm
            // 
            this.uComboSaveTerm.Location = new System.Drawing.Point(468, 0);
            this.uComboSaveTerm.Name = "uComboSaveTerm";
            this.uComboSaveTerm.Size = new System.Drawing.Size(152, 21);
            this.uComboSaveTerm.TabIndex = 15;
            this.uComboSaveTerm.Text = "년";
            // 
            // uComboDocClassName
            // 
            this.uComboDocClassName.Location = new System.Drawing.Point(312, 0);
            this.uComboDocClassName.Name = "uComboDocClassName";
            this.uComboDocClassName.Size = new System.Drawing.Size(152, 21);
            this.uComboDocClassName.TabIndex = 14;
            this.uComboDocClassName.Text = "구분";
            // 
            // uButtonSendSI
            // 
            this.uButtonSendSI.Location = new System.Drawing.Point(400, 64);
            this.uButtonSendSI.Name = "uButtonSendSI";
            this.uButtonSendSI.Size = new System.Drawing.Size(84, 28);
            this.uButtonSendSI.TabIndex = 13;
            this.uButtonSendSI.Text = "ultraButton1";
            this.uButtonSendSI.Click += new System.EventHandler(this.uButtonSendSI_Click);
            // 
            // rdDept
            // 
            this.rdDept.AutoSize = true;
            this.rdDept.Location = new System.Drawing.Point(404, 368);
            this.rdDept.Name = "rdDept";
            this.rdDept.Size = new System.Drawing.Size(47, 16);
            this.rdDept.TabIndex = 12;
            this.rdDept.TabStop = true;
            this.rdDept.Text = "부서";
            this.rdDept.UseVisualStyleBackColor = true;
            // 
            // rdPerson
            // 
            this.rdPerson.AutoSize = true;
            this.rdPerson.Location = new System.Drawing.Point(404, 340);
            this.rdPerson.Name = "rdPerson";
            this.rdPerson.Size = new System.Drawing.Size(47, 16);
            this.rdPerson.TabIndex = 11;
            this.rdPerson.TabStop = true;
            this.rdPerson.Text = "개인";
            this.rdPerson.UseVisualStyleBackColor = true;
            // 
            // uButtonCc
            // 
            this.uButtonCc.Location = new System.Drawing.Point(400, 300);
            this.uButtonCc.Name = "uButtonCc";
            this.uButtonCc.Size = new System.Drawing.Size(84, 28);
            this.uButtonCc.TabIndex = 10;
            this.uButtonCc.Text = "ultraButton1";
            this.uButtonCc.Click += new System.EventHandler(this.uButtonCc_Click);
            // 
            // uButtonSendSP
            // 
            this.uButtonSendSP.Location = new System.Drawing.Point(400, 172);
            this.uButtonSendSP.Name = "uButtonSendSP";
            this.uButtonSendSP.Size = new System.Drawing.Size(84, 28);
            this.uButtonSendSP.TabIndex = 9;
            this.uButtonSendSP.Text = "ultraButton1";
            this.uButtonSendSP.Click += new System.EventHandler(this.uButtonSendSP_Click);
            // 
            // uButtonSendSS
            // 
            this.uButtonSendSS.Location = new System.Drawing.Point(400, 136);
            this.uButtonSendSS.Name = "uButtonSendSS";
            this.uButtonSendSS.Size = new System.Drawing.Size(84, 28);
            this.uButtonSendSS.TabIndex = 8;
            this.uButtonSendSS.Text = "ultraButton1";
            this.uButtonSendSS.Click += new System.EventHandler(this.uButtonSendSS_Click);
            // 
            // uButtonSendSA
            // 
            this.uButtonSendSA.Location = new System.Drawing.Point(400, 100);
            this.uButtonSendSA.Name = "uButtonSendSA";
            this.uButtonSendSA.Size = new System.Drawing.Size(84, 28);
            this.uButtonSendSA.TabIndex = 7;
            this.uButtonSendSA.Text = "ultraButton1";
            this.uButtonSendSA.Click += new System.EventHandler(this.uButtonSendSA_Click);
            // 
            // uGroupBoxCcLine
            // 
            this.uGroupBoxCcLine.Controls.Add(this.uButtonCcLineDel);
            this.uGroupBoxCcLine.Controls.Add(this.uGridCcLine);
            this.uGroupBoxCcLine.Location = new System.Drawing.Point(488, 270);
            this.uGroupBoxCcLine.Name = "uGroupBoxCcLine";
            this.uGroupBoxCcLine.Size = new System.Drawing.Size(376, 232);
            this.uGroupBoxCcLine.TabIndex = 6;
            this.uGroupBoxCcLine.Text = "ultraGroupBox2";
            // 
            // uButtonCcLineDel
            // 
            this.uButtonCcLineDel.Location = new System.Drawing.Point(8, 29);
            this.uButtonCcLineDel.Name = "uButtonCcLineDel";
            this.uButtonCcLineDel.Size = new System.Drawing.Size(84, 27);
            this.uButtonCcLineDel.TabIndex = 2;
            this.uButtonCcLineDel.Text = "ultraButton1";
            this.uButtonCcLineDel.Click += new System.EventHandler(this.uButtonCcLineDel_Click);
            // 
            // uGridCcLine
            // 
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCcLine.DisplayLayout.Appearance = appearance28;
            this.uGridCcLine.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCcLine.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCcLine.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCcLine.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridCcLine.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCcLine.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridCcLine.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCcLine.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCcLine.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCcLine.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGridCcLine.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCcLine.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCcLine.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCcLine.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridCcLine.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCcLine.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCcLine.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance35.TextHAlignAsString = "Left";
            this.uGridCcLine.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.uGridCcLine.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCcLine.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridCcLine.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridCcLine.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCcLine.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridCcLine.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCcLine.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCcLine.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCcLine.Location = new System.Drawing.Point(4, 57);
            this.uGridCcLine.Name = "uGridCcLine";
            this.uGridCcLine.Size = new System.Drawing.Size(368, 168);
            this.uGridCcLine.TabIndex = 0;
            this.uGridCcLine.Text = "ultraGrid2";
            this.uGridCcLine.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange;
            // 
            // uGroupBoxSendLine
            // 
            this.uGroupBoxSendLine.Controls.Add(this.uButtonSendLineDel);
            this.uGroupBoxSendLine.Controls.Add(this.uGridSendLine);
            this.uGroupBoxSendLine.Location = new System.Drawing.Point(488, 23);
            this.uGroupBoxSendLine.Name = "uGroupBoxSendLine";
            this.uGroupBoxSendLine.Size = new System.Drawing.Size(376, 240);
            this.uGroupBoxSendLine.TabIndex = 5;
            this.uGroupBoxSendLine.Text = "ultraGroupBox1";
            // 
            // uButtonSendLineDel
            // 
            this.uButtonSendLineDel.Location = new System.Drawing.Point(8, 29);
            this.uButtonSendLineDel.Name = "uButtonSendLineDel";
            this.uButtonSendLineDel.Size = new System.Drawing.Size(84, 27);
            this.uButtonSendLineDel.TabIndex = 2;
            this.uButtonSendLineDel.Text = "ultraButton1";
            this.uButtonSendLineDel.Click += new System.EventHandler(this.uButtonSendLineDel_Click);
            // 
            // uGridSendLine
            // 
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSendLine.DisplayLayout.Appearance = appearance16;
            this.uGridSendLine.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSendLine.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSendLine.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSendLine.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridSendLine.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSendLine.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGridSendLine.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSendLine.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSendLine.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSendLine.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridSendLine.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSendLine.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSendLine.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSendLine.DisplayLayout.Override.CellAppearance = appearance17;
            this.uGridSendLine.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSendLine.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSendLine.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance23.TextHAlignAsString = "Left";
            this.uGridSendLine.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridSendLine.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSendLine.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGridSendLine.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGridSendLine.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSendLine.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.uGridSendLine.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSendLine.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSendLine.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSendLine.Location = new System.Drawing.Point(4, 57);
            this.uGridSendLine.Name = "uGridSendLine";
            this.uGridSendLine.Size = new System.Drawing.Size(368, 176);
            this.uGridSendLine.TabIndex = 0;
            this.uGridSendLine.Text = "ultraGrid1";
            this.uGridSendLine.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange;
            // 
            // uGroupBoxResult
            // 
            this.uGroupBoxResult.Controls.Add(this.uGridResult);
            this.uGroupBoxResult.Location = new System.Drawing.Point(8, 180);
            this.uGroupBoxResult.Name = "uGroupBoxResult";
            this.uGroupBoxResult.Size = new System.Drawing.Size(388, 288);
            this.uGroupBoxResult.TabIndex = 4;
            this.uGroupBoxResult.Text = "ultraGroupBox2";
            // 
            // uGridResult
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridResult.DisplayLayout.Appearance = appearance4;
            this.uGridResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridResult.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridResult.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridResult.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridResult.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridResult.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridResult.DisplayLayout.Override.CellAppearance = appearance5;
            this.uGridResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridResult.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridResult.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridResult.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridResult.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridResult.Location = new System.Drawing.Point(4, 24);
            this.uGridResult.Name = "uGridResult";
            this.uGridResult.Size = new System.Drawing.Size(376, 260);
            this.uGridResult.TabIndex = 0;
            this.uGridResult.Text = "ultraGrid1";
            // 
            // uGroupBoxSearch
            // 
            this.uGroupBoxSearch.Controls.Add(this.uComboDeptName);
            this.uGroupBoxSearch.Controls.Add(this.uButtonRefresh);
            this.uGroupBoxSearch.Controls.Add(this.uButtonSearch);
            this.uGroupBoxSearch.Controls.Add(this.uCheckDept);
            this.uGroupBoxSearch.Controls.Add(this.uLabelDeptSearch);
            this.uGroupBoxSearch.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearch.Controls.Add(this.uLabelDept);
            this.uGroupBoxSearch.Controls.Add(this.uLabel);
            this.uGroupBoxSearch.Controls.Add(this.uLabelUserName);
            this.uGroupBoxSearch.Controls.Add(this.ultraTextEditor4);
            this.uGroupBoxSearch.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearch.Controls.Add(this.uTextUserName);
            this.uGroupBoxSearch.Location = new System.Drawing.Point(8, 24);
            this.uGroupBoxSearch.Name = "uGroupBoxSearch";
            this.uGroupBoxSearch.Size = new System.Drawing.Size(388, 152);
            this.uGroupBoxSearch.TabIndex = 3;
            this.uGroupBoxSearch.Text = "ultraGroupBox1";
            // 
            // uComboDeptName
            // 
            this.uComboDeptName.Location = new System.Drawing.Point(278, 36);
            this.uComboDeptName.Name = "uComboDeptName";
            this.uComboDeptName.Size = new System.Drawing.Size(100, 21);
            this.uComboDeptName.TabIndex = 24;
            this.uComboDeptName.Text = "ultraComboEditor1";
            // 
            // uButtonRefresh
            // 
            this.uButtonRefresh.Location = new System.Drawing.Point(198, 116);
            this.uButtonRefresh.Name = "uButtonRefresh";
            this.uButtonRefresh.Size = new System.Drawing.Size(84, 28);
            this.uButtonRefresh.TabIndex = 23;
            this.uButtonRefresh.Text = "ultraButton2";
            this.uButtonRefresh.Visible = false;
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(106, 116);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(84, 28);
            this.uButtonSearch.TabIndex = 22;
            this.uButtonSearch.Text = "ultraButton1";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uCheckDept
            // 
            this.uCheckDept.Location = new System.Drawing.Point(86, 84);
            this.uCheckDept.Name = "uCheckDept";
            this.uCheckDept.Size = new System.Drawing.Size(292, 20);
            this.uCheckDept.TabIndex = 21;
            this.uCheckDept.Text = "부서명 검색";
            // 
            // uLabelDeptSearch
            // 
            this.uLabelDeptSearch.Location = new System.Drawing.Point(10, 84);
            this.uLabelDeptSearch.Name = "uLabelDeptSearch";
            this.uLabelDeptSearch.Size = new System.Drawing.Size(72, 20);
            this.uLabelDeptSearch.TabIndex = 20;
            this.uLabelDeptSearch.Text = "ultraLabel5";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(202, 60);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(72, 20);
            this.ultraLabel3.TabIndex = 19;
            this.ultraLabel3.Text = "ultraLabel3";
            // 
            // uLabelDept
            // 
            this.uLabelDept.Location = new System.Drawing.Point(202, 36);
            this.uLabelDept.Name = "uLabelDept";
            this.uLabelDept.Size = new System.Drawing.Size(72, 20);
            this.uLabelDept.TabIndex = 18;
            this.uLabelDept.Text = "ultraLabel4";
            // 
            // uLabel
            // 
            this.uLabel.Location = new System.Drawing.Point(10, 60);
            this.uLabel.Name = "uLabel";
            this.uLabel.Size = new System.Drawing.Size(72, 20);
            this.uLabel.TabIndex = 17;
            this.uLabel.Text = "ultraLabel2";
            // 
            // uLabelUserName
            // 
            this.uLabelUserName.Location = new System.Drawing.Point(10, 36);
            this.uLabelUserName.Name = "uLabelUserName";
            this.uLabelUserName.Size = new System.Drawing.Size(72, 20);
            this.uLabelUserName.TabIndex = 16;
            this.uLabelUserName.Text = "ultraLabel1";
            // 
            // ultraTextEditor4
            // 
            this.ultraTextEditor4.Location = new System.Drawing.Point(278, 60);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 15;
            this.ultraTextEditor4.Text = "ultraTextEditor4";
            // 
            // ultraTextEditor2
            // 
            this.ultraTextEditor2.Location = new System.Drawing.Point(86, 60);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 13;
            this.ultraTextEditor2.Text = "ultraTextEditor2";
            // 
            // uTextUserName
            // 
            this.uTextUserName.Location = new System.Drawing.Point(86, 36);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 12;
            this.uTextUserName.Text = "ultraTextEditor1";
            // 
            // uButtonCancel
            // 
            this.uButtonCancel.Location = new System.Drawing.Point(388, 476);
            this.uButtonCancel.Name = "uButtonCancel";
            this.uButtonCancel.Size = new System.Drawing.Size(84, 28);
            this.uButtonCancel.TabIndex = 2;
            this.uButtonCancel.Text = "ultraButton2";
            this.uButtonCancel.Click += new System.EventHandler(this.uButtonCancel_Click);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(296, 476);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(84, 28);
            this.uButtonOK.TabIndex = 1;
            this.uButtonOK.Text = "ultraButton1";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uComboCompany
            // 
            this.uComboCompany.Location = new System.Drawing.Point(156, 0);
            this.uComboCompany.Name = "uComboCompany";
            this.uComboCompany.Size = new System.Drawing.Size(152, 21);
            this.uComboCompany.TabIndex = 0;
            this.uComboCompany.Text = "회사";
            this.uComboCompany.Visible = false;
            // 
            // uGroupBoxComment
            // 
            this.uGroupBoxComment.Controls.Add(this.uTextComment);
            this.uGroupBoxComment.Location = new System.Drawing.Point(8, 512);
            this.uGroupBoxComment.Name = "uGroupBoxComment";
            this.uGroupBoxComment.Size = new System.Drawing.Size(856, 110);
            this.uGroupBoxComment.TabIndex = 17;
            this.uGroupBoxComment.Text = "ultraGroupBox1";
            // 
            // uTextComment
            // 
            this.uTextComment.Location = new System.Drawing.Point(5, 32);
            this.uTextComment.Multiline = true;
            this.uTextComment.Name = "uTextComment";
            this.uTextComment.Size = new System.Drawing.Size(844, 68);
            this.uTextComment.TabIndex = 0;
            this.uTextComment.Text = "ultraTextEditor1";
            // 
            // frmCOM0012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(876, 634);
            this.Controls.Add(this.uGroupBoxForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCOM0012";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "결재선관리";
            this.Load += new System.EventHandler(this.frmCOM0012_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxForm)).EndInit();
            this.uGroupBoxForm.ResumeLayout(false);
            this.uGroupBoxForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSaveTerm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDocClassName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCcLine)).EndInit();
            this.uGroupBoxCcLine.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCcLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSendLine)).EndInit();
            this.uGroupBoxSendLine.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSendLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxResult)).EndInit();
            this.uGroupBoxResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearch)).EndInit();
            this.uGroupBoxSearch.ResumeLayout(false);
            this.uGroupBoxSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxComment)).EndInit();
            this.uGroupBoxComment.ResumeLayout(false);
            this.uGroupBoxComment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxForm;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCompany;
        private Infragistics.Win.Misc.UltraButton uButtonCancel;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearch;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxResult;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridResult;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxCcLine;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSendLine;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCcLine;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSendLine;
        private Infragistics.Win.Misc.UltraButton uButtonCc;
        private Infragistics.Win.Misc.UltraButton uButtonSendSP;
        private Infragistics.Win.Misc.UltraButton uButtonSendSS;
        private Infragistics.Win.Misc.UltraButton uButtonSendSA;
        private System.Windows.Forms.RadioButton rdDept;
        private System.Windows.Forms.RadioButton rdPerson;
        private Infragistics.Win.Misc.UltraButton uButtonSendSI;
        private Infragistics.Win.Misc.UltraButton uButtonCcLineDel;
        private Infragistics.Win.Misc.UltraButton uButtonSendLineDel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSaveTerm;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDocClassName;
        private Infragistics.Win.Misc.UltraButton uButtonRefresh;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDept;
        private Infragistics.Win.Misc.UltraLabel uLabelDeptSearch;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabelDept;
        private Infragistics.Win.Misc.UltraLabel uLabel;
        private Infragistics.Win.Misc.UltraLabel uLabelUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.Misc.UltraButton uButtonSendRA;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDeptName;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxComment;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextComment;
    }
}