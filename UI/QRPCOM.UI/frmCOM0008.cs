﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0008.cs                                         */
/* 프로그램명   : SparePart정보                                         */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0008 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strSparePartCode;
        private string strSparePartName;
        private string strSpec;
        private string strUnitCode;
        private string strUnitName;
        private string strMaker;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string SparePartCode
        {
            get { return strSparePartCode; }
            set { strSparePartCode = value; }
        }

        public string SparePartName
        {
            get { return strSparePartName; }
            set { strSparePartName = value; }
        }

        public string Spec
        {
            get { return strSpec; }
            set { strSpec = value; }
        }

        public string Maker
        {
            get { return strMaker; }
            set { strMaker = value; }
        }

        public string UnitCode
        {
            get { return strUnitCode; }
            set { strUnitCode = value; }
        }

        public string UnitName
        {
            get { return strUnitName; }
            set { strUnitName = value; }
        }

        public frmCOM0008()
        {
            InitializeComponent();
        }

        private void frmCOM0008_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strSparePartCode = "";
            strSparePartName = "";

            //--컨트롤초기화
            InitButton();
            InitLabel();
            InitCombo();
            InitGrid();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridSparePart, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridSparePart, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //폰트설정
                this.uGridSparePart.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSparePart.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }

        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                QRPMAS.BL.MASEQU.SparePart clsSparePart = new QRPMAS.BL.MASEQU.SparePart();
                brwChannel.mfCredentials(clsSparePart);

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dtSparePart = clsSparePart.mfReadMASSparePartPOP(strPlantCode, m_resSys.GetString("SYS_LANG"));
                this.uGridSparePart.DataSource = dtSparePart;
                this.uGridSparePart.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtSparePart.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSparePart, 0);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridSparePart.ActiveRow == null) return;

                if (this.uGridSparePart.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridSparePart.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridSparePart.ActiveRow.Cells["PlantName"].Text.ToString();
                    strSparePartCode = this.uGridSparePart.ActiveRow.Cells["SparePartCode"].Text.ToString();
                    strSparePartName = this.uGridSparePart.ActiveRow.Cells["SparePartName"].Text.ToString();
                    strSpec = this.uGridSparePart.ActiveRow.Cells["Spec"].Text.ToString();
                    strMaker = this.uGridSparePart.ActiveRow.Cells["Maker"].Text.ToString();
                    strUnitCode = this.uGridSparePart.ActiveRow.Cells["UnitCode"].Text.ToString();
                    strUnitName = this.uGridSparePart.ActiveRow.Cells["UnitName"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridSparePart_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strSparePartCode = e.Row.Cells["SparePartCode"].Text.ToString();
                strSparePartName = e.Row.Cells["SparePartName"].Text.ToString();
                strSpec = e.Row.Cells["Spec"].Text.ToString();
                strMaker = e.Row.Cells["Maker"].Text.ToString();
                strUnitCode = e.Row.Cells["UnitCode"].Text.ToString();
                strUnitName = e.Row.Cells["UnitName"].Text.ToString();
                this.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }


    }
}
