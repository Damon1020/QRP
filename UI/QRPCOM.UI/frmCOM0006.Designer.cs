﻿namespace QRPCOM.UI
{
    partial class frmCOM0006
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uGridMeasureTool = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMeasureTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uButtonOK
            // 
            this.uButtonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uButtonOK.Location = new System.Drawing.Point(447, 385);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 8;
            this.uButtonOK.Text = "확인";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uButtonClose.Location = new System.Drawing.Point(539, 385);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 7;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(540, 6);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 2;
            this.uButtonSearch.Text = "검색";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uGridMeasureTool
            // 
            this.uGridMeasureTool.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMeasureTool.DisplayLayout.Appearance = appearance5;
            this.uGridMeasureTool.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMeasureTool.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureTool.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMeasureTool.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridMeasureTool.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMeasureTool.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridMeasureTool.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMeasureTool.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMeasureTool.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMeasureTool.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridMeasureTool.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMeasureTool.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureTool.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMeasureTool.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridMeasureTool.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMeasureTool.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureTool.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridMeasureTool.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridMeasureTool.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMeasureTool.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridMeasureTool.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridMeasureTool.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMeasureTool.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridMeasureTool.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMeasureTool.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMeasureTool.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMeasureTool.Location = new System.Drawing.Point(-1, 41);
            this.uGridMeasureTool.Name = "uGridMeasureTool";
            this.uGridMeasureTool.Size = new System.Drawing.Size(636, 340);
            this.uGridMeasureTool.TabIndex = 6;
            this.uGridMeasureTool.Text = "ultraGrid1";
            this.uGridMeasureTool.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridMeasureTool_DoubleClickRow);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraGroupBox1.Appearance = appearance1;
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox1.Controls.Add(this.uButtonSearch);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchPlant);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchPlant);
            this.ultraGroupBox1.Location = new System.Drawing.Point(-1, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(635, 40);
            this.ultraGroupBox1.TabIndex = 5;
            // 
            // frmCOM0006
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(634, 414);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uGridMeasureTool);
            this.Controls.Add(this.ultraGroupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCOM0006";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "계측기정보 검색창";
            this.Load += new System.EventHandler(this.frmCOM0006_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMeasureTool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMeasureTool;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
    }
}