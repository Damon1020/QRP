﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0011.cs                                         */
/* 프로그램명   : 사용자정보                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

namespace QRPCOM.UI
{
    public partial class frmCOMProgress : Form
    {
        private System.Threading.Thread threadPopup = null;         //팝업창 처리 Thread
        private string m_strFunctionName;                           //팝업창 처리기능명

        private string m_strProgressName;
        private DateTime m_datStartDate;

        public string ProgressName
        {
            set { m_strProgressName = value; }
        }

        public frmCOMProgress()
        {
            InitializeComponent();
        }

        private void frmCOMProgress_Load(object sender, EventArgs e)
        {
            uLabelFunction.Text = m_strProgressName;
            uLabelFunction.AutoSize = true;

            uActivityIndicator.AnimationEnabled = true;
            uActivityIndicator.MarqueeAnimationStyle = Infragistics.Win.UltraActivityIndicator.MarqueeAnimationStyle.BounceBack;
            uActivityIndicator.AnimationSpeed = 30;
            uActivityIndicator.MarqueeMarkerWidth = 100;
            uActivityIndicator.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            uActivityIndicator.ViewStyle = Infragistics.Win.UltraActivityIndicator.ActivityIndicatorViewStyle.Aero;

            m_datStartDate = DateTime.Now;
            timerProgress.Interval = 1000;
            timerProgress.Start();

            this.Cursor = Cursors.WaitCursor;
        }

        private void frmCOMProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerProgress.Stop();
            this.Cursor = Cursors.Default;
        }

        private void timerProgress_Tick(object sender, EventArgs e)
        {
            DateTime datCurDate = DateTime.Now;
            TimeSpan timSpendTime = DateTime.Now.Subtract(m_datStartDate);

            string strSpendDate = "";
            if (timSpendTime.Hours > 0)
                strSpendDate = timSpendTime.Hours.ToString() + "시";
            if (timSpendTime.Minutes > 0)
                strSpendDate = strSpendDate + timSpendTime.Minutes.ToString() + "분";
            if (timSpendTime.Seconds > 0)
                strSpendDate = strSpendDate + timSpendTime.Seconds.ToString() + "초";

            uLabelTime.Text = strSpendDate;
        }

        /// <summary>
        /// 팝업창을 처리하기위한 Thread 생성
        /// </summary>
        /// <returns></returns>
        public Thread mfStartThread()
        {
            threadPopup = new System.Threading.Thread((OpenPopup)); //Thread생성
            return threadPopup;
        }

        /// <summary>
        /// 진행 팝업창 열기
        /// </summary>
        /// <param name="frmForm"></param>
        /// <param name="strFunctioName"></param>
        public void mfOpenProgressPopup(Form frmForm, string strFunctioName)
        {
            try
            {
                //m_frmForm = frmForm;
                m_strFunctionName = strFunctioName;
                //m_frmForm.Cursor = Cursors.WaitCursor;
                threadPopup.Start();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 진행 팝업창 열기
        /// </summary>
        private void OpenPopup()
        {
            this.ProgressName = m_strFunctionName;
            this.StartPosition = FormStartPosition.CenterScreen;
            //frmProgressPopup.MdiParent = m_frmForm; // m_mdiform;
            //frmProgressPopup.Left = Convert.ToInt32((m_frmForm.Width - frmProgressPopup.Width) / 2);
            //frmProgressPopup.Top = Convert.ToInt32((m_frmForm.Height - frmProgressPopup.Height) / 2);
            //frmProgressPopup.Show();
            //this.Show();
            this.ShowDialog();
            //this.Refresh();
        }

        /// <summary>
        /// Thread 종료 및 진행 팝업창 닫기
        /// </summary>
        public void mfCloseProgressPopup(Form MdiForm)
        {
            try
            {
                threadPopup.Abort();
                //m_frmForm.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }
    }
}
