﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0006.cs                                         */
/* 프로그램명   : 계측기정보                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPCOM.UI
{
    public partial class frmCOM0006 : Form
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strMTTypeCode;
        private string strMTTypeName;
        private string strMeasureToolCode;
        private string strMeasureToolName;
        private string strSpec;
        private string strModelName;
        private string strMakerCompany;
        private string strMakerNo;
        private string strAcquireDate;
        private string strSerialNo;
        private string strDiscardDate;
        private string strAcquireDeptCode;
        private string strAcquireDeptName;
        private string strLastInspectDate;
        private string strInspectNextDate;

        



        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }
        
        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }
        
        public string MTTypeCode
        {
            get { return strMTTypeCode; }
            set { strMTTypeCode = value; }
        }
        
        public string MTTypeName
        {
            get { return strMTTypeName; }
            set { strMTTypeName = value; }
        }
        
        public string MeasureToolCode
        {
            get { return strMeasureToolCode; }
            set { strMeasureToolCode = value; }
        }
        
        public string MeasureToolName
        {
            get { return strMeasureToolName; }
            set { strMeasureToolName = value; }
        }
        
        public string Spec
        {
            get { return strSpec; }
            set { strSpec = value; }
        }
        
        public string ModelName
        {
            get { return strModelName; }
            set { strModelName = value; }
        }
        
        public string MakerCompany
        {
            get { return strMakerCompany; }
            set { strMakerCompany = value; }
        }
        
        public string MakerNo
        {
            get { return strMakerNo; }
            set { strMakerNo = value; }
        }
        
        public string AcquireDate
        {
            get { return strAcquireDate; }
            set { strAcquireDate = value; }
        }
        
        public string SerialNo
        {
            get { return strSerialNo; }
            set { strSerialNo = value; }
        }
        
        public string DiscardDate
        {
            get { return strDiscardDate; }
            set { strDiscardDate = value; }
        }
        public string AcquireDeptCode
        {
            get { return strAcquireDeptCode; }
            set { strAcquireDeptCode = value; }
        }
        public string AcquireDeptName
        {
            get { return strAcquireDeptName; }
            set { strAcquireDeptName = value; }
        }
        public string InspectNextDate
        {
            get { return strInspectNextDate; }
            set { strInspectNextDate = value; }
        }

        public string LastInspectDate
        {
            get { return strLastInspectDate; }
            set { strLastInspectDate = value; }
        }


        public frmCOM0006()
        {
            InitializeComponent();
        }

        private void frmCOM0006_Load(object sender, EventArgs e)
        {
            //strPlantCode = "";
            //strPlantName = "";
            strMTTypeCode = "";
            strMTTypeName = "";
            strMeasureToolCode = "";
            strMeasureToolName = "";
            strSpec = "";
            strModelName = "";
            strMakerCompany = "";
            strMakerNo = "";
            strAcquireDate = "";
            strSerialNo = "";
            strDiscardDate = "";
            strInspectNextDate = "";
            strLastInspectDate = "";
            strAcquireDeptCode = "";
            strAcquireDeptName = "";

            // 초기화 Method 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            this.Icon = Properties.Resources.qrpi;

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , strPlantCode, "", "전체", "PlantCode", "PlantName", dt);
                uComboSearchPlant.Enabled = false;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridMeasureTool, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MTTypeCode", "계측기분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MTTypeName", "계측기분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MeasureToolCode", "계측기코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MeasureToolName", "계측기명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "ModelName", "모델명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MakerCompany", "제작회사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");
                
                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "MakerNo", "제작번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "AcquireDate", "취득일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "AcquireDeptCode", "취득부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "AcquireDeptName", "취득부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "InspectPeriod", "검교정주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "DiscardDate", "폐기일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "InspectNextDate", "검교정예정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureTool, 0, "LastInspectDate", "최근교정일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                

                //폰트설정
                uGridMeasureTool.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMeasureTool.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch
            {
            }
            finally
            {
            }

        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                QRPMAS.BL.MASQUA.MeasureTool clsemt = new QRPMAS.BL.MASQUA.MeasureTool();
                brwChannel.mfCredentials(clsemt);

                // PrograssBar 생성
                this.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dtmt = clsemt.mfReadMASMeasureToolPopup(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                this.uGridMeasureTool.DataSource = dtmt;
                this.uGridMeasureTool.DataBind();

                // PrograssBar 종료
                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();
                if (dtmt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMeasureTool, 0);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridMeasureTool_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                strPlantName = e.Row.Cells["PlantName"].Text.ToString();
                strMTTypeCode = e.Row.Cells["MTTypeCode"].Text.ToString();
                strMTTypeName = e.Row.Cells["MTTypeName"].Text.ToString();
                strMeasureToolCode = e.Row.Cells["MeasureToolCode"].Text.ToString();
                strMeasureToolName = e.Row.Cells["MeasureToolName"].Text.ToString();
                strSpec = e.Row.Cells["Spec"].Text.ToString();
                strModelName = e.Row.Cells["ModelName"].Text.ToString();
                strMakerCompany = e.Row.Cells["MakerCompany"].Text.ToString();
                strMakerNo = e.Row.Cells["MakerNo"].Text.ToString();
                strAcquireDate = e.Row.Cells["AcquireDate"].Text.ToString();
                strAcquireDeptCode = e.Row.Cells["AcquireDeptCode"].Text.ToString();
                strAcquireDeptName = e.Row.Cells["AcquireDeptName"].Text.ToString();
                strSerialNo = e.Row.Cells["SerialNo"].Text.ToString();
                strDiscardDate = e.Row.Cells["DiscardDate"].Text.ToString();
                strInspectNextDate = e.Row.Cells["InspectNextDate"].Text.ToString();
                strLastInspectDate = e.Row.Cells["LastInspectDate"].Text.ToString();
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridMeasureTool.ActiveRow == null) return;

                if (this.uGridMeasureTool.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGridMeasureTool.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGridMeasureTool.ActiveRow.Cells["PlantName"].Text.ToString();
                    strMTTypeCode = this.uGridMeasureTool.ActiveRow.Cells["MTTypeCode"].Text.ToString();
                    strMTTypeName = this.uGridMeasureTool.ActiveRow.Cells["MTTypeName"].Text.ToString();
                    strMeasureToolCode = this.uGridMeasureTool.ActiveRow.Cells["MeasureToolCode"].Text.ToString();
                    strMeasureToolName = this.uGridMeasureTool.ActiveRow.Cells["MeasureToolName"].Text.ToString();
                    strSpec = this.uGridMeasureTool.ActiveRow.Cells["Spec"].Text.ToString();
                    strModelName = this.uGridMeasureTool.ActiveRow.Cells["ModelName"].Text.ToString();
                    strMakerCompany = this.uGridMeasureTool.ActiveRow.Cells["MakerCompany"].Text.ToString();
                    strMakerNo = this.uGridMeasureTool.ActiveRow.Cells["MakerNo"].Text.ToString();
                    strAcquireDate = this.uGridMeasureTool.ActiveRow.Cells["AcquireDate"].Text.ToString();
                    strAcquireDeptCode = this.uGridMeasureTool.ActiveRow.Cells["AcquireDeptCode"].Text.ToString();
                    strAcquireDeptName = this.uGridMeasureTool.ActiveRow.Cells["AcquireDeptName"].Text.ToString();
                    strSerialNo = this.uGridMeasureTool.ActiveRow.Cells["SerialNo"].Text.ToString();
                    strDiscardDate = this.uGridMeasureTool.ActiveRow.Cells["DiscardDate"].Text.ToString();
                    strLastInspectDate = this.uGridMeasureTool.ActiveRow.Cells["LastInspectDate"].Text.ToString();
                    strInspectNextDate = this.uGridMeasureTool.ActiveRow.Cells["InspectNextDate"].Text.ToString();
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
    }
}
