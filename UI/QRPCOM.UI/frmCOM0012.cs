﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통UI                                                */
/* 모듈(분류)명 : 공통POPUP                                             */
/* 프로그램ID   : frmCOM0012.cs                                         */
/* 프로그램명   : 결재선                                                */
/* 작성자       : 이종민                                                */
/* 작성일자     : 2011-11-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Data;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.Resources;
using System.Threading;

namespace QRPCOM.UI
{
    public partial class frmCOM0012 : Form
    {
        private static QRPGlobal SysRes = new QRPGlobal();
        private ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        private string strComment;

        public DataTable dtSendLine { get; set; }
        public DataTable dtCcLine { get; set; }
        public DataTable dtFormInfo { get; set; }
        private string CD_USERKEY { get; set; }
        private string USERID { get; set; }
        private string USERNAME { get; set; }

        public string ApprovalUserID { get; set; }
        public string ApprovalUserName { get; set; }

        public string WriteID { get; set; }
        public string WriteName { get; set; }

        public string Comment
        {
            get { return strComment; }
            set { strComment = value; }
        }

        public frmCOM0012()
        {
            InitializeComponent();
            InitDataTable();
            InitGroupBox();
            InitComboBox();
            InitButton();
            InitGrid();
            InitTextBox();
            InitLabel();


            uTextUserName.Text = m_resSys.GetString("SYS_USERNAME");
            ApprovalUserID = string.Empty;
            ApprovalUserName = string.Empty;
            WriteID = string.Empty;
            WriteName = string.Empty;

            uTextUserName.KeyUp += new KeyEventHandler(TextBoxKeyEvent);
            ultraTextEditor2.KeyUp += new KeyEventHandler(TextBoxKeyEvent);
            ultraTextEditor4.KeyUp += new KeyEventHandler(TextBoxKeyEvent);

        }

        private void frmCOM0012_Load(object sender, EventArgs e)
        {
            Find_CD_USERKEY();
            Find_RA_CD_USERKEY();
            //Find_RI_CD_USERKEY();
            ReOrder();
            uTextUserName.Focus();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region Util
        //결재선을 SA-> SS -> SP -> RA 순서대로 정렬
        private void ReOrder()
        {
            WinGrid wGrid = new WinGrid();
            try
            {
                DataTable SendLine = new DataTable();
                SendLine.Columns.Add("Seq");
                SendLine.Columns.Add("CD_USERKEY");
                SendLine.Columns.Add("UserName");
                SendLine.Columns.Add("UserID");
                SendLine.Columns.Add("CD_KIND");
                SendLine.Columns.Add("CD_COMPANY");
                SendLine.Columns.Add("DeptName");

                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    DataRow dr = SendLine.NewRow();
                    dr["Seq"] = uGridSendLine.Rows[i].Cells["Seq"].Value.ToString();
                    dr["CD_USERKEY"] = uGridSendLine.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                    dr["UserName"] = uGridSendLine.Rows[i].Cells["UserName"].Value.ToString();
                    dr["UserID"] = uGridSendLine.Rows[i].Cells["UserID"].Value.ToString();
                    dr["CD_KIND"] = uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString();
                    dr["CD_COMPANY"] = uGridSendLine.Rows[i].Cells["CD_COMPANY"].Value.ToString();
                    dr["DeptName"] = uGridSendLine.Rows[i].Cells["DeptName"].Value.ToString();
                    SendLine.Rows.Add(dr);
                }

                DataRow[] drSI = SendLine.Select("CD_KIND = 'SI'", "Seq");
                DataRow[] drSA = SendLine.Select("CD_KIND = 'SA'", "Seq");
                DataRow[] drSS = SendLine.Select("CD_KIND = 'SS'", "Seq");
                DataRow[] drSP = SendLine.Select("CD_KIND = 'SP'", "Seq");
                DataRow[] drRA = SendLine.Select("CD_KIND = 'RI'", "Seq");

                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    uGridSendLine.Rows[i].Delete(false);
                    i -= 1;
                }

                foreach (DataRow dr in drSI)
                {
                    //wGrid.mfAddRowGrid(uGridSendLine, 0);
                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "01";
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = dr["CD_USERKEY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = dr["UserName"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = dr["CD_KIND"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = dr["UserID"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_COMPANY"].Value = dr["CD_COMPANY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = dr["DeptName"].ToString();
                }
                foreach (DataRow dr in drSA)
                {
                    //wGrid.mfAddRowGrid(uGridSendLine, 0);
                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = dr["Seq"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = dr["CD_USERKEY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = dr["UserName"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = dr["CD_KIND"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = dr["UserID"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_COMPANY"].Value = dr["CD_COMPANY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = dr["DeptName"].ToString();
                }
                foreach (DataRow dr in drSS)
                {
                    //wGrid.mfAddRowGrid(uGridSendLine, 0);
                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = dr["Seq"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = dr["CD_USERKEY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = dr["UserName"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = dr["CD_KIND"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = dr["UserID"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_COMPANY"].Value = dr["CD_COMPANY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = dr["DeptName"].ToString();
                }
                foreach (DataRow dr in drSP)
                {
                    //wGrid.mfAddRowGrid(uGridSendLine, 0);
                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = dr["Seq"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = dr["CD_USERKEY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = dr["UserName"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = dr["CD_KIND"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = dr["UserID"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_COMPANY"].Value = dr["CD_COMPANY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = dr["DeptName"].ToString();
                }
                foreach (DataRow dr in drRA)
                {
                    //wGrid.mfAddRowGrid(uGridSendLine, 0);
                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = dr["Seq"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = dr["CD_USERKEY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = dr["UserName"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = dr["CD_KIND"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = dr["UserID"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_COMPANY"].Value = dr["CD_COMPANY"].ToString();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = dr["DeptName"].ToString();
                }

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    uGridResult.Rows[i].Cells["Check"].Value = false;
                }

            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                uGridSendLine.DisplayLayout.Bands[0].AddNew();
                uTextUserName.Focus();
            }
        }
        private void Find_CD_USERKEY()
        {
            try
            {
                if (WriteID.Equals(string.Empty))
                    return;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPGRW.BL.GRWUSR.GRWUser), "GRWUser");
                QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadGRWUserD("250", WriteID);

                if (dtUser.Rows.Count.Equals(0))
                {
                    CD_USERKEY = string.Empty;
                    uButtonSendSI.Visible = true;
                }
                else
                {
                    CD_USERKEY = dtUser.Rows[0]["CD_USERKEY"].ToString();
                    USERID = WriteID;
                    USERNAME = WriteName;
                    uButtonSendSI.Visible = false;

                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "01";
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = "SI";
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = CD_USERKEY;
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = USERID;
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = USERNAME;
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                uGridSendLine.DisplayLayout.Bands[0].AddNew();
            }
        }
        private void Find_RA_CD_USERKEY()
        {
            try
            {
                if (ApprovalUserID.Equals(string.Empty))
                    return;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPGRW.BL.GRWUSR.GRWUser), "GRWUser");
                QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadGRWUserD("250", ApprovalUserID);

                if (!dtUser.Rows.Count.Equals(0))
                {
                    string strCD_USERKEY = dtUser.Rows[0]["CD_USERKEY"].ToString();
                    string strUSERID = ApprovalUserID;
                    string strUSERNAME = ApprovalUserName;
                    //uButtonSendSI.Visible = false;

                    uGridSendLine.DisplayLayout.Bands[0].AddNew();
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "05.01";
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = "RI";
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = strCD_USERKEY;
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = strUSERID;
                    uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = strUSERNAME;
                }

            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                uGridSendLine.DisplayLayout.Bands[0].AddNew();
            }
        }
        //////담당자 선택
        ////private void Find_RI_CD_USERKEY()
        ////{
        ////    try
        ////    {
        ////        if (RIUserID.Equals(string.Empty))
        ////            return;

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPGRW.BL.GRWUSR.GRWUser), "GRWUser");
        ////        QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
        ////        brwChannel.mfCredentials(clsUser);

        ////        DataTable dtUser = clsUser.mfReadGRWUserD("250", RIUserID);

        ////        if (!dtUser.Rows.Count.Equals(0))
        ////        {
        ////            string strCD_USERKEY = dtUser.Rows[0]["CD_USERKEY"].ToString();
        ////            string strUSERID = RIUserID;
        ////            string strUSERNAME = RIUserName;

        ////            uGridSendLine.DisplayLayout.Bands[0].AddNew();
        ////            uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "05.01";
        ////            uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = "RI";
        ////            uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = strCD_USERKEY;
        ////            uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = strUSERID;
        ////            uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = strUSERNAME;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        throw (ex);
        ////    }
        ////    finally
        ////    {
        ////        uGridSendLine.DisplayLayout.Bands[0].AddNew();
        ////    }
        ////}
        private Boolean CheckGridRow()
        {
            try
            {
                int cnt = 0;
                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        cnt += 1;
                        break;
                    }
                }

                if (!cnt.Equals(0))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region 초기화
        private void InitDataTable()
        {
            try
            {
                dtSendLine = new DataTable();
                dtSendLine.Columns.Add("Seq");
                dtSendLine.Columns.Add("CD_USERKEY");
                dtSendLine.Columns.Add("UserName");
                dtSendLine.Columns.Add("UserID");
                dtSendLine.Columns.Add("CD_KIND");
                dtSendLine.Columns.Add("CD_COMPANY");
                dtSendLine.Columns.Add("DeptName");

                dtCcLine = new DataTable();
                dtCcLine.Columns.Add("CD_USERKEY");
                dtCcLine.Columns.Add("UserName");
                dtCcLine.Columns.Add("UserID");
                dtCcLine.Columns.Add("CD_KIND");
                dtCcLine.Columns.Add("CD_COMPANY");
                dtCcLine.Columns.Add("DeptName");

                dtFormInfo = new DataTable();
                dtFormInfo.Columns.Add("UserType");
                dtFormInfo.Columns.Add("SaveTerm");
                dtFormInfo.Columns.Add("DocLevel");
                dtFormInfo.Columns.Add("DocClassId");
                dtFormInfo.Columns.Add("DocClassName");
                dtFormInfo.Columns.Add("DocNo");
                dtFormInfo.Columns.Add("EntName");
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }
        private void InitGroupBox()
        {
            QRPCOM.QRPUI.WinGroupBox wGroup = new QRPCOM.QRPUI.WinGroupBox();
            try
            {
                wGroup.mfSetGroupBox(this.uGroupBoxForm, GroupBoxType.INFO, "결재선관리", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxSearch, GroupBoxType.INFO, "검색", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxResult, GroupBoxType.INFO, "검색결과", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxSendLine, GroupBoxType.INFO, "결재목록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxCcLine, GroupBoxType.INFO, "통보목록", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroup.mfSetGroupBox(this.uGroupBoxComment, GroupBoxType.INFO, "Comment", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default,
                    Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
            }
            catch
            {
            }
            finally
            {
            }
        }
        private void InitComboBox()
        {
            WinComboEditor wCombo = new WinComboEditor();
            try
            {
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPGRW.BL.GRWUSR.GRWUser), "GRWUser");
                //QRPGRW.BL.GRWUSR.GRWUser clsCompany = new QRPGRW.BL.GRWUSR.GRWUser();
                //brwChannel.mfCredentials(clsCompany);

                //DataTable dtCompany = clsCompany.mfReadCompany(m_resSys.GetString("SYS_LANG"));

                //wCombo.mfSetComboEditor(this.uComboCompany, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                //    , "PlantCode", "PlantName", dtCompany);

                DataTable dt = new DataTable();
                dt.Columns.Add("DocClassID");
                dt.Columns.Add("DocClassName");

                DataRow dr = dt.NewRow();
                dr["DocClassID"] = "일반문서";
                dr["DocClassName"] = "일반문서";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["DocClassID"] = "보안문서";
                dr["DocClassName"] = "보안문서";
                dt.Rows.Add(dr);

                wCombo.mfSetComboEditor(this.uComboDocClassName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "일반문서", "", ""
                    , "DocClassID", "DocClassName", dt);

                //문서보존 기간 : Default / 5(5년)
                //         1(1년), 3(3년), 5(5년), 10(10년), 99(영구

                dt = new DataTable();
                dt.Columns.Add("SaveTerm");
                dt.Columns.Add("SaveTermName");

                dr = dt.NewRow();
                dr["SaveTerm"] = "1";
                dr["SaveTermName"] = "1년";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["SaveTerm"] = "3";
                dr["SaveTermName"] = "3년";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["SaveTerm"] = "5";
                dr["SaveTermName"] = "5년";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["SaveTerm"] = "10";
                dr["SaveTermName"] = "10년";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["SaveTerm"] = "99";
                dr["SaveTermName"] = "영구";
                dt.Rows.Add(dr);

                wCombo.mfSetComboEditor(this.uComboSaveTerm, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "5", "", ""
                    , "SaveTerm", "SaveTermName", dt);


                //부서 콤보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboDeptName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);
            }
            catch
            {
            }
            finally
            {

            }
        }
        private void InitButton()
        {
            WinButton wButton = new WinButton();
            try
            {
                wButton.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonCancel, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

                wButton.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                wButton.mfSetButton(this.uButtonRefresh, "새로고침", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonSendSI, "기안자", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonSendSA, "일반결재", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonSendSS, "순차합의", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonSendSP, "병렬합의", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonSendRA, "담당자", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonCc, "통보", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonSendLineDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonCcLineDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch
            {
            }
            finally
            {

            }
        }
        private void InitGrid()
        {
            WinGrid wGrid = new WinGrid();
            try
            {
                #region 조회결과
                wGrid.mfInitGeneralGrid(this.uGridResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridResult, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridResult, 0, "CD_USERKEY", "CD_USERKEY", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridResult, 0, "UserName", "이름", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridResult, 0, "DeptCode", "DeptCode", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridResult, 0, "UserID", "UserID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridResult, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                uGridResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                uGridResult.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region 결재선
                wGrid.mfInitGeneralGrid(this.uGridSendLine, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "CD_USERKEY", "CD_USERKEY", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "UserName", "이름", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "UserID", "UserID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "CD_KIND", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "CD_COMPANY", "CD_COMPANY", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSendLine, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                //wGrid.mfAddRowGrid(uGridSendLine, 0);
                uGridSendLine.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSendLine.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                //uGridSendLine.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                DataTable dtSendLineC = new DataTable();
                dtSendLineC.Columns.Add("CD_KIND");
                dtSendLineC.Columns.Add("TYPE");

                DataRow dr = dtSendLineC.NewRow();
                dr["CD_KIND"] = "SI";
                dr["TYPE"] = "기안자";
                dtSendLineC.Rows.Add(dr);

                dr = dtSendLineC.NewRow();
                dr["CD_KIND"] = "SA";
                dr["TYPE"] = "결재자";
                dtSendLineC.Rows.Add(dr);

                dr = dtSendLineC.NewRow();
                dr["CD_KIND"] = "SS";
                dr["TYPE"] = "순차합의";
                dtSendLineC.Rows.Add(dr);

                dr = dtSendLineC.NewRow();
                dr["CD_KIND"] = "SP";
                dr["TYPE"] = "병렬합의";
                dtSendLineC.Rows.Add(dr);

                dr = dtSendLineC.NewRow();
                dr["CD_KIND"] = "RI";
                dr["TYPE"] = "담당자";
                dtSendLineC.Rows.Add(dr);

                wGrid.mfSetGridColumnValueList(uGridSendLine, 0, "CD_KIND", Infragistics.Win.ValueListDisplayStyle.DisplayText, "선택", "", dtSendLineC);
                uGridSendLine.DataSource = dtSendLine;
                uGridSendLine.DataBind();
                #endregion

                #region 통보
                wGrid.mfInitGeneralGrid(this.uGridCcLine, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "CD_USERKEY", "CD_USERKEY", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "UserName", "이름", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "UserID", "UserID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "CD_KIND", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "CD_COMPANY", "CD_COMPANY", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCcLine, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                uGridCcLine.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridCcLine.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                //uGridCcLine.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                //wGrid.mfAddRowGrid(uGridCcLine, 0);
                DataTable dtCcLineC = new DataTable();
                dtCcLineC.Columns.Add("CD_KIND");
                dtCcLineC.Columns.Add("TYPE");

                dr = dtCcLineC.NewRow();
                dr["CD_KIND"] = "CP";
                dr["TYPE"] = "개인통보";
                dtCcLineC.Rows.Add(dr);

                dr = dtCcLineC.NewRow();
                dr["CD_KIND"] = "CD";
                dr["TYPE"] = "부서통보";
                dtCcLineC.Rows.Add(dr);

                wGrid.mfSetGridColumnValueList(uGridCcLine, 0, "CD_KIND", Infragistics.Win.ValueListDisplayStyle.DisplayText, "선택", "", dtCcLineC);
                uGridCcLine.DataSource = dtCcLine;
                uGridCcLine.DataBind();
                #endregion

                wGrid.mfAddRowGrid(uGridSendLine, 0);
                wGrid.mfAddRowGrid(uGridCcLine, 0);
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }
        private void InitTextBox()
        {
            try
            {
                uTextUserName.Text = string.Empty;
                ultraTextEditor2.Text = string.Empty;
                ultraTextEditor4.Text = string.Empty;
                this.uTextComment.Text = string.Empty;
                this.uTextComment.Scrollbars = ScrollBars.Vertical;
                this.uTextComment.SelectionStart = uTextComment.Text.Length;
                this.uTextComment.ScrollToCaret();

                rdPerson.Checked = true;
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }
        private void InitLabel()
        {
            try
            {
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(uLabelUserName, "이름", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelDept, "부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabel, "직위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(ultraLabel3, "직책", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(uLabelDeptSearch, "부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }
        #endregion

        #region 버튼 이벤트

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                string strUserId = string.Empty;
                string strSeq = string.Empty;
                string strUserName = string.Empty;
                string strDeptName = string.Empty;
                WinMessageBox msg = new WinMessageBox();

                //////////if (this.uGridCcLine.Rows.Count > 8)
                //////////{
                //////////    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //////////        , "필수확인", "필수확인", "통보목록은 최대 8명 까지 가능합니다.", Infragistics.Win.HAlign.Right);
                //////////    return;
                //////////}

                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SI"))
                    {
                        CD_USERKEY = uGridSendLine.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        strUserId = uGridSendLine.Rows[i].Cells["UserID"].Value.ToString();
                        strSeq = uGridSendLine.Rows[i].Cells["Seq"].Value.ToString();
                        strUserName = uGridSendLine.Rows[i].Cells["UserName"].Value.ToString();
                        strDeptName = uGridSendLine.Rows[i].Cells["DeptName"].Value.ToString();
                    }
                }

                #region 필수확인
                if (CD_USERKEY == null || CD_USERKEY.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001235", "M001279", Infragistics.Win.HAlign.Right);
                    return;
                }

                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SA"))
                    {
                        cnt += 1;
                        break;
                    }
                }
                if (cnt.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001235", "M001324", Infragistics.Win.HAlign.Right);
                    return;
                }
                #endregion

                #region Create DataTable
                DataRow dr;

                dr = dtSendLine.NewRow();
                dr["CD_KIND"] = "SI";
                dr["CD_COMPANY"] = "250";
                dr["CD_USERKEY"] = CD_USERKEY;
                dr["UserName"] = strUserName;
                dr["UserID"] = strUserId;
                dr["Seq"] = strSeq;
                dr["DeptName"] = strDeptName;
                dtSendLine.Rows.Add(dr);

                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (!uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SI"))
                    {
                        if (!uGridSendLine.Rows[i].Cells["CD_KIND"].Value.Equals(string.Empty)
                            && !uGridSendLine.Rows[i].Cells["CD_USERKEY"].Value.Equals(string.Empty))
                        {
                            dr = dtSendLine.NewRow();
                            dr["CD_KIND"] = uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString();
                            dr["CD_COMPANY"] = "250";
                            dr["CD_USERKEY"] = uGridSendLine.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                            dr["UserName"] = uGridSendLine.Rows[i].Cells["UserName"].Value.ToString();
                            dr["Seq"] = uGridSendLine.Rows[i].Cells["Seq"].Value.ToString();
                            dr["UserID"] = uGridSendLine.Rows[i].Cells["UserID"].Value.ToString();
                            dr["DeptName"] = uGridSendLine.Rows[i].Cells["DeptName"].Value.ToString();
                            dtSendLine.Rows.Add(dr);
                        }
                    }
                }
                for (int i = 0; i < uGridCcLine.Rows.Count; i++)
                {
                    if (!uGridCcLine.Rows[i].Cells["CD_KIND"].Value.Equals(string.Empty)
                        && !uGridCcLine.Rows[i].Cells["CD_USERKEY"].Value.Equals(string.Empty))
                    {
                        dr = dtCcLine.NewRow();
                        dr["CD_KIND"] = uGridCcLine.Rows[i].Cells["CD_KIND"].Value.ToString();
                        dr["CD_COMPANY"] = "250";
                        dr["CD_USERKEY"] = uGridCcLine.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridCcLine.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridCcLine.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridCcLine.Rows[i].Cells["DeptName"].Value.ToString();
                        dtCcLine.Rows.Add(dr);
                    }
                }

                dr = dtFormInfo.NewRow();
                dr["DocClassName"] = uComboDocClassName.Value.ToString();
                dr["SaveTerm"] = uComboSaveTerm.Value.ToString();
                dr["UserType"] = "U";
                dr["DocLevel"] = string.Empty;
                dr["DocClassId"] = string.Empty;
                dr["DocNo"] = string.Empty;
                dr["EntName"] = string.Empty;

                dtFormInfo.Rows.Add(dr);
                #endregion

                strComment = this.uTextComment.Text;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }

        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            try
            {
                InitDataTable();
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                this.Close();
            }
        }

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox wMsg = new WinMessageBox();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPGRW.BL.GRWUSR.GRWUser), "GRWUser");
                QRPGRW.BL.GRWUSR.GRWUser clsUser = new QRPGRW.BL.GRWUSR.GRWUser();
                brwChannel.mfCredentials(clsUser);

                string strUserName = uTextUserName.Text.ToString();
                string strDeptName = string.Empty;
                if (uCheckDept.Checked)
                    //strDeptName = uTextDeptName.Text.ToString();
                    strDeptName = uComboDeptName.Text.ToString();

                DataTable dtUser = clsUser.mfReadGRWUser("250", strUserName, strDeptName);

                uGridResult.DataSource = dtUser;
                uGridResult.DataBind();

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.Cursor = Cursors.Default;

                if (dtUser.Rows.Count.Equals(0))
                {
                    wMsg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }

        private void uButtonRefresh_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }

        //그리드에 결재자 추가
        private void uButtonSendSA_Click(object sender, EventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            uGridSendLine.Focus();
            try
            {
                if (!CheckGridRow())
                    return;

                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();
                        dr["CD_KIND"] = "SA";
                        dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();
                        newRow.Rows.Add(dr);
                    }
                }

                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SA"))
                        cnt += 1;
                }
                cnt += 1;
                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    bool check = false;

                    for (int j = 0; j < uGridSendLine.Rows.Count; j++)
                    {
                        if (uGridSendLine.Rows[j].Cells["UserID"].Value.Equals(newRow.Rows[i]["UserID"].ToString()))
                        {
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        //wGrid.mfAddRowGrid(uGridSendLine, 0);
                        uGridSendLine.DisplayLayout.Bands[0].AddNew();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "02." + (cnt + i).ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                ReOrder();
            }
        }

        //그리드에 순차합의 추가
        private void uButtonSendSS_Click(object sender, EventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            uGridSendLine.Focus();
            try
            {
                if (!CheckGridRow())
                    return;

                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();
                        dr["CD_KIND"] = "SS";
                        dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();
                        newRow.Rows.Add(dr);
                    }
                }

                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SS"))
                        cnt += 1;
                }
                cnt += 1;
                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    bool check = false;

                    for (int j = 0; j < uGridSendLine.Rows.Count; j++)
                    {
                        if (uGridSendLine.Rows[j].Cells["UserID"].Value.Equals(newRow.Rows[i]["UserID"].ToString()))
                        {
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        //wGrid.mfAddRowGrid(uGridSendLine, 0);
                        uGridSendLine.DisplayLayout.Bands[0].AddNew();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "03." + (cnt + i).ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                ReOrder();
            }
        }

        //그리드에 병렬합의 추가
        private void uButtonSendSP_Click(object sender, EventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            uGridSendLine.Focus();
            try
            {
                if (!CheckGridRow())
                    return;
                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();
                        dr["CD_KIND"] = "SP";
                        dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();
                        newRow.Rows.Add(dr);
                    }
                }

                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SP"))
                        cnt += 1;
                }
                cnt += 1;
                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    bool check = false;

                    for (int j = 0; j < uGridSendLine.Rows.Count; j++)
                    {
                        if (uGridSendLine.Rows[j].Cells["UserID"].Value.Equals(newRow.Rows[i]["UserID"].ToString()))
                        {
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        //wGrid.mfAddRowGrid(uGridSendLine, 0);
                        uGridSendLine.DisplayLayout.Bands[0].AddNew();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "04." + (cnt + i).ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                ReOrder();
            }
        }

        //그리드에 통보 추가
        private void uButtonCc_Click(object sender, EventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            uGridCcLine.Focus();

            try
            {
                if (!CheckGridRow())
                    return;

                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();

                        if (rdPerson.Checked)
                        {
                            dr["CD_KIND"] = "CP";
                            dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                            dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        }
                        else
                        {
                            dr["CD_KIND"] = "CD";
                            dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["DeptCode"].Value.ToString();
                            dr["UserID"] = uGridResult.Rows[i].Cells["DeptCode"].Value.ToString();
                        }

                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();

                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();

                        newRow.Rows.Add(dr);
                    }
                }

                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    //wGrid.mfAddRowGrid(uGridCcLine, 0);
                    uGridCcLine.DisplayLayout.Bands[0].AddNew();
                    uGridCcLine.Rows[uGridCcLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                    uGridCcLine.Rows[uGridCcLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                    uGridCcLine.Rows[uGridCcLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                    uGridCcLine.Rows[uGridCcLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                    uGridCcLine.Rows[uGridCcLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                }

            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
            }
        }

        //그리드에 기안자 추가
        private void uButtonSendSI_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            WinGrid wGrid = new WinGrid();
            uGridSendLine.Focus();

            try
            {
                if (!CheckGridRow())
                    return;

                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();
                        dr["CD_KIND"] = "SI";
                        dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();
                        newRow.Rows.Add(dr);
                    }
                }
                if (newRow.Rows.Count > 1)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001278", Infragistics.Win.HAlign.Right);
                    return;
                }

                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SI"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001228", "M001278", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    bool check = false;

                    for (int j = 0; j < uGridSendLine.Rows.Count; j++)
                    {
                        if (uGridSendLine.Rows[j].Cells["UserID"].Value.Equals(newRow.Rows[i]["UserID"].ToString()))
                        {
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        //wGrid.mfAddRowGrid(uGridSendLine, 0);
                        uGridSendLine.DisplayLayout.Bands[0].AddNew();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "01.01";
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                ReOrder();
            }
        }

        //행삭제
        private void uButtonSendLineDel_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridSendLine.Rows[i].Cells["Check"].Value))
                    {
                        uGridSendLine.Rows[i].Delete(false);
                        i -= 1;
                    }
                }
                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("SI"))
                    {
                        cnt += 1;
                        break;
                    }
                }

                if (cnt.Equals(0))
                {
                    uButtonSendSI.Visible = true;
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }

        //행삭제
        private void uButtonCcLineDel_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < uGridCcLine.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridCcLine.Rows[i].Cells["Check"].Value))
                    {
                        uGridCcLine.Rows[i].Delete(false);
                        i -= 1;
                    }
                }

            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {

            }
        }

        // 그리드에 담당자 추가
        private void uButtonSendRA_Click(object sender, EventArgs e)
        {
            WinGrid wGrid = new WinGrid();
            uGridSendLine.Focus();
            try
            {
                if (!CheckGridRow())
                    return;

                DataTable newRow = new DataTable();
                newRow.Columns.Add("CD_KIND");
                newRow.Columns.Add("CD_USERKEY");
                newRow.Columns.Add("UserName");
                newRow.Columns.Add("UserID");
                newRow.Columns.Add("DeptName");

                for (int i = 0; i < uGridResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridResult.Rows[i].Cells["Check"].Value))
                    {
                        DataRow dr = newRow.NewRow();
                        dr["CD_KIND"] = "RI";
                        dr["CD_USERKEY"] = uGridResult.Rows[i].Cells["CD_USERKEY"].Value.ToString();
                        dr["UserName"] = uGridResult.Rows[i].Cells["UserName"].Value.ToString();
                        dr["UserID"] = uGridResult.Rows[i].Cells["UserID"].Value.ToString();
                        dr["DeptName"] = uGridResult.Rows[i].Cells["DeptName"].Value.ToString();
                        newRow.Rows.Add(dr);
                    }
                }

                int cnt = 0;
                for (int i = 0; i < uGridSendLine.Rows.Count; i++)
                {
                    if (uGridSendLine.Rows[i].Cells["CD_KIND"].Value.ToString().Equals("RA"))
                        cnt += 1;
                }
                cnt += 1;
                for (int i = 0; i < newRow.Rows.Count; i++)
                {
                    bool check = false;

                    for (int j = 0; j < uGridSendLine.Rows.Count; j++)
                    {
                        if (uGridSendLine.Rows[j].Cells["UserID"].Value.Equals(newRow.Rows[i]["UserID"].ToString()))
                        {
                            check = true;
                            break;
                        }
                    }

                    if (!check)
                    {
                        //wGrid.mfAddRowGrid(uGridSendLine, 0);
                        uGridSendLine.DisplayLayout.Bands[0].AddNew();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_KIND"].Value = newRow.Rows[i]["CD_KIND"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["CD_USERKEY"].Value = newRow.Rows[i]["CD_USERKEY"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserName"].Value = newRow.Rows[i]["UserName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["UserID"].Value = newRow.Rows[i]["UserID"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["DeptName"].Value = newRow.Rows[i]["DeptName"].ToString();
                        uGridSendLine.Rows[uGridSendLine.Rows.Count - 1].Cells["Seq"].Value = "05." + (cnt + i).ToString("00");
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            finally
            {
                ReOrder();
            }
        }

        private void TextBoxKeyEvent(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                uButtonSearch.PerformClick();
            }
        }
        #endregion


    }
}


