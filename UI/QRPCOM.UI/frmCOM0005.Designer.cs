﻿namespace QRPCOM.UI
{
    partial class frmCOM0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uButtonOK
            // 
            this.uButtonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uButtonOK.Location = new System.Drawing.Point(460, 448);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 8;
            this.uButtonOK.Text = "확인";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uButtonClose.Location = new System.Drawing.Point(552, 448);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 7;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(536, 24);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 2;
            this.uButtonSearch.Text = "검색";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uGridEquip
            // 
            this.uGridEquip.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquip.DisplayLayout.Appearance = appearance1;
            this.uGridEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquip.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquip.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquip.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquip.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridEquip.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquip.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquip.Location = new System.Drawing.Point(0, 75);
            this.uGridEquip.Name = "uGridEquip";
            this.uGridEquip.Size = new System.Drawing.Size(637, 373);
            this.uGridEquip.TabIndex = 6;
            this.uGridEquip.Text = "ultraGrid1";
            this.uGridEquip.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridEquip_DoubleClickRow);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraGroupBox1.Appearance = appearance13;
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox1.Controls.Add(this.uComboSearchEquipGroup);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchLargeType);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchProcess);
            this.ultraGroupBox1.Controls.Add(this.uComboSearchEquipLoc);
            this.ultraGroupBox1.Controls.Add(this.uComboStation);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchEquipGroup);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchEquipLargeType);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchProcessGroup);
            this.ultraGroupBox1.Controls.Add(this.uLabelSearchEquipLoc);
            this.ultraGroupBox1.Controls.Add(this.uLabelStation);
            this.ultraGroupBox1.Controls.Add(this.uComboPlant);
            this.ultraGroupBox1.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox1.Controls.Add(this.uButtonSearch);
            this.ultraGroupBox1.Location = new System.Drawing.Point(-1, 1);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(645, 75);
            this.ultraGroupBox1.TabIndex = 5;
            // 
            // uComboArea
            // 
            this.uComboArea.Location = new System.Drawing.Point(352, 448);
            this.uComboArea.Name = "uComboArea";
            this.uComboArea.Size = new System.Drawing.Size(132, 21);
            this.uComboArea.TabIndex = 30;
            // 
            // uLabelArea
            // 
            this.uLabelArea.Location = new System.Drawing.Point(248, 448);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelArea.TabIndex = 29;
            this.uLabelArea.Text = "ultraLabel1";
            // 
            // uComboEquipProcGubun
            // 
            this.uComboEquipProcGubun.Location = new System.Drawing.Point(116, 448);
            this.uComboEquipProcGubun.Name = "uComboEquipProcGubun";
            this.uComboEquipProcGubun.Size = new System.Drawing.Size(132, 21);
            this.uComboEquipProcGubun.TabIndex = 28;
            // 
            // uLabelEquipProcGubun
            // 
            this.uLabelEquipProcGubun.Location = new System.Drawing.Point(12, 448);
            this.uLabelEquipProcGubun.Name = "uLabelEquipProcGubun";
            this.uLabelEquipProcGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipProcGubun.TabIndex = 27;
            this.uLabelEquipProcGubun.Text = "ultraLabel1";
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(372, 52);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchEquipGroup.TabIndex = 18;
            // 
            // uComboSearchLargeType
            // 
            this.uComboSearchLargeType.Location = new System.Drawing.Point(112, 52);
            this.uComboSearchLargeType.MaxLength = 50;
            this.uComboSearchLargeType.Name = "uComboSearchLargeType";
            this.uComboSearchLargeType.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchLargeType.TabIndex = 17;
            this.uComboSearchLargeType.ValueChanged += new System.EventHandler(this.uComboSearchLargeType_ValueChanged);
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(372, 28);
            this.uComboSearchProcess.MaxLength = 50;
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchProcess.TabIndex = 20;
            this.uComboSearchProcess.ValueChanged += new System.EventHandler(this.uComboSearchProcess_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(112, 28);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchEquipLoc.TabIndex = 19;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboStation
            // 
            this.uComboStation.Location = new System.Drawing.Point(372, 4);
            this.uComboStation.MaxLength = 50;
            this.uComboStation.Name = "uComboStation";
            this.uComboStation.Size = new System.Drawing.Size(145, 21);
            this.uComboStation.TabIndex = 16;
            this.uComboStation.Text = "uCombo";
            this.uComboStation.ValueChanged += new System.EventHandler(this.uComboStation_ValueChanged);
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(268, 52);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 15;
            // 
            // uLabelSearchEquipLargeType
            // 
            this.uLabelSearchEquipLargeType.Location = new System.Drawing.Point(8, 52);
            this.uLabelSearchEquipLargeType.Name = "uLabelSearchEquipLargeType";
            this.uLabelSearchEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLargeType.TabIndex = 12;
            // 
            // uLabelSearchProcessGroup
            // 
            this.uLabelSearchProcessGroup.Location = new System.Drawing.Point(268, 28);
            this.uLabelSearchProcessGroup.Name = "uLabelSearchProcessGroup";
            this.uLabelSearchProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcessGroup.TabIndex = 11;
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(8, 28);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 14;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(268, 4);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 13;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(112, 4);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(145, 21);
            this.uComboPlant.TabIndex = 10;
            this.uComboPlant.Text = "전체";
            this.uComboPlant.ValueMember = " ";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(8, 4);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 9;
            // 
            // frmCOM0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(644, 484);
            this.Controls.Add(this.uComboArea);
            this.Controls.Add(this.uLabelArea);
            this.Controls.Add(this.uComboEquipProcGubun);
            this.Controls.Add(this.uLabelEquipProcGubun);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uGridEquip);
            this.Controls.Add(this.ultraGroupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCOM0005";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "설비정보 검색창";
            this.Load += new System.EventHandler(this.frmCOM0005_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquip;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboArea;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
    }
}