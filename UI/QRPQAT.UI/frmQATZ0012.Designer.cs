﻿namespace QRPQAT.UI
{
    partial class frmQATZ0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0012));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchWriteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchWriteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridUTCList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUTCList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(352, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(124, 21);
            this.uComboSearchEquipLoc.TabIndex = 10;
            this.uComboSearchEquipLoc.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(248, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 9;
            this.uLabelSearchEquipLoc.Text = "ultraLabel1";
            // 
            // uDateSearchWriteToDate
            // 
            this.uDateSearchWriteToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteToDate.Location = new System.Drawing.Point(708, 12);
            this.uDateSearchWriteToDate.Name = "uDateSearchWriteToDate";
            this.uDateSearchWriteToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchWriteToDate.TabIndex = 8;
            // 
            // ultraLabel1
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(692, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel1.TabIndex = 7;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchWriteFromDate
            // 
            this.uDateSearchWriteFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteFromDate.Location = new System.Drawing.Point(588, 12);
            this.uDateSearchWriteFromDate.Name = "uDateSearchWriteFromDate";
            this.uDateSearchWriteFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchWriteFromDate.TabIndex = 6;
            // 
            // uLabelSearchWriteDate
            // 
            this.uLabelSearchWriteDate.Location = new System.Drawing.Point(484, 12);
            this.uLabelSearchWriteDate.Name = "uLabelSearchWriteDate";
            this.uLabelSearchWriteDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchWriteDate.TabIndex = 2;
            this.uLabelSearchWriteDate.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(124, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridUTCList
            // 
            this.uGridUTCList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridUTCList.DisplayLayout.Appearance = appearance5;
            this.uGridUTCList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridUTCList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUTCList.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUTCList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridUTCList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUTCList.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridUTCList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridUTCList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridUTCList.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridUTCList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridUTCList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridUTCList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridUTCList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridUTCList.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridUTCList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridUTCList.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUTCList.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridUTCList.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridUTCList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridUTCList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridUTCList.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridUTCList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridUTCList.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridUTCList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridUTCList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridUTCList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridUTCList.Location = new System.Drawing.Point(0, 80);
            this.uGridUTCList.Name = "uGridUTCList";
            this.uGridUTCList.Size = new System.Drawing.Size(1060, 740);
            this.uGridUTCList.TabIndex = 3;
            this.uGridUTCList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUTCList_AfterCellUpdate);
            this.uGridUTCList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUTCList_ClickCellButton);
            this.uGridUTCList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridUTCList_CellChange);
            // 
            // frmQATZ0012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridUTCList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0012";
            this.Load += new System.EventHandler(this.frmQATZ0012_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0012_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0012_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUTCList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUTCList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
    }
}