﻿namespace QRPQAT.UI
{
    partial class frmQATZ0006
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0006));
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckSearchRohsEndDateOver = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchRohsEndDateOver = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchItemName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchItemName = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateRohsEndDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRohsEndDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchRohsEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextCl = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBr = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHBCDD = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCl = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelBr = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHBCDD = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDEHP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDBP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBBP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDEHP = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDBP = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelBBP = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSb = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPFOA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPFOS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSb = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPFOA = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPFOS = new Infragistics.Win.Misc.UltraLabel();
            this.uTextItemNameDetail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelItemNameDetail = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReportNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReportNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPBDEs = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPBDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextItemName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelItemName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckMSDS = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextCr = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHg = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRohsEndDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPBBs = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPb = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCd = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPBBs = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMSDS = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRohsEndDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCr = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHg = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPB = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCd = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckNOLongerBuy = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelNOLongerBuy = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDesc = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchRohsEndDateOver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHBCDD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDEHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDBP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBBP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPFOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPFOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextItemNameDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReportNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPBDEs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMSDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPBBs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckNOLongerBuy)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance3;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDesc);
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchRohsEndDateOver);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRohsEndDateOver);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchItemName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchItemName);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateRohsEndDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateRohsEndDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRohsEndDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 44);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uCheckSearchRohsEndDateOver
            // 
            this.uCheckSearchRohsEndDateOver.Location = new System.Drawing.Point(688, 12);
            this.uCheckSearchRohsEndDateOver.Name = "uCheckSearchRohsEndDateOver";
            this.uCheckSearchRohsEndDateOver.Size = new System.Drawing.Size(17, 20);
            this.uCheckSearchRohsEndDateOver.TabIndex = 255;
            // 
            // uLabelSearchRohsEndDateOver
            // 
            this.uLabelSearchRohsEndDateOver.Location = new System.Drawing.Point(572, 11);
            this.uLabelSearchRohsEndDateOver.Name = "uLabelSearchRohsEndDateOver";
            this.uLabelSearchRohsEndDateOver.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchRohsEndDateOver.TabIndex = 254;
            this.uLabelSearchRohsEndDateOver.Text = "차기교정일 초과";
            // 
            // uTextSearchItemName
            // 
            this.uTextSearchItemName.Location = new System.Drawing.Point(415, 11);
            this.uTextSearchItemName.Name = "uTextSearchItemName";
            this.uTextSearchItemName.Size = new System.Drawing.Size(151, 21);
            this.uTextSearchItemName.TabIndex = 3;
            // 
            // uLabelSearchItemName
            // 
            this.uLabelSearchItemName.Location = new System.Drawing.Point(326, 12);
            this.uLabelSearchItemName.Name = "uLabelSearchItemName";
            this.uLabelSearchItemName.Size = new System.Drawing.Size(83, 20);
            this.uLabelSearchItemName.TabIndex = 253;
            this.uLabelSearchItemName.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(885, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(14, 12);
            this.ultraLabel1.TabIndex = 252;
            this.ultraLabel1.Text = "~";
            this.ultraLabel1.Visible = false;
            // 
            // uTextSearchVendorName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance2;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(216, 12);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(102, 21);
            this.uTextSearchVendorName.TabIndex = 251;
            // 
            // uDateRohsEndDateTo
            // 
            this.uDateRohsEndDateTo.DateTime = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateRohsEndDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRohsEndDateTo.Location = new System.Drawing.Point(898, 12);
            this.uDateRohsEndDateTo.Name = "uDateRohsEndDateTo";
            this.uDateRohsEndDateTo.Size = new System.Drawing.Size(27, 21);
            this.uDateRohsEndDateTo.TabIndex = 162;
            this.uDateRohsEndDateTo.Value = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateRohsEndDateTo.Visible = false;
            // 
            // uDateRohsEndDateFrom
            // 
            this.uDateRohsEndDateFrom.DateTime = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateRohsEndDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRohsEndDateFrom.Location = new System.Drawing.Point(854, 12);
            this.uDateRohsEndDateFrom.Name = "uDateRohsEndDateFrom";
            this.uDateRohsEndDateFrom.Size = new System.Drawing.Size(27, 21);
            this.uDateRohsEndDateFrom.TabIndex = 161;
            this.uDateRohsEndDateFrom.Value = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateRohsEndDateFrom.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(792, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(18, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(771, 16);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(18, 21);
            this.uLabelSearchPlant.TabIndex = 156;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uTextSearchVendorCode
            // 
            appearance1.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance1.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance1;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(110, 12);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(103, 21);
            this.uTextSearchVendorCode.TabIndex = 2;
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(94, 20);
            this.uLabelSearchVendor.TabIndex = 158;
            this.uLabelSearchVendor.Text = "1";
            // 
            // uLabelSearchRohsEndDate
            // 
            this.uLabelSearchRohsEndDate.Location = new System.Drawing.Point(833, 12);
            this.uLabelSearchRohsEndDate.Name = "uLabelSearchRohsEndDate";
            this.uLabelSearchRohsEndDate.Size = new System.Drawing.Size(21, 20);
            this.uLabelSearchRohsEndDate.TabIndex = 194;
            this.uLabelSearchRohsEndDate.Text = "4";
            this.uLabelSearchRohsEndDate.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance4;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 84);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(917, 760);
            this.uGridHeader.TabIndex = 4;
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(915, 678);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(915, 678);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckNOLongerBuy);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelNOLongerBuy);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCl);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextBr);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextHBCDD);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCl);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelBr);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelHBCDD);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDEHP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDBP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextBBP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDEHP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDBP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelBBP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSb);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPFOA);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPFOS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSb);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPFOA);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPFOS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextItemNameDetail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelItemNameDetail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReportNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReportNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPBDEs);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPBDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextItemName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelItemName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendorName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckMSDS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCr);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextHg);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateRohsEndDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPBBs);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPb);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCd);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendorCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPBBs);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMSDS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRohsEndDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCr);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelHg);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPB);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCd);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(909, 658);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextCl
            // 
            this.uTextCl.Location = new System.Drawing.Point(662, 164);
            this.uTextCl.Name = "uTextCl";
            this.uTextCl.Size = new System.Drawing.Size(168, 21);
            this.uTextCl.TabIndex = 276;
            // 
            // uTextBr
            // 
            this.uTextBr.Location = new System.Drawing.Point(370, 164);
            this.uTextBr.Name = "uTextBr";
            this.uTextBr.Size = new System.Drawing.Size(182, 21);
            this.uTextBr.TabIndex = 275;
            // 
            // uTextHBCDD
            // 
            this.uTextHBCDD.Location = new System.Drawing.Point(113, 164);
            this.uTextHBCDD.Name = "uTextHBCDD";
            this.uTextHBCDD.Size = new System.Drawing.Size(146, 21);
            this.uTextHBCDD.TabIndex = 274;
            // 
            // uLabelCl
            // 
            this.uLabelCl.Location = new System.Drawing.Point(559, 164);
            this.uLabelCl.Name = "uLabelCl";
            this.uLabelCl.Size = new System.Drawing.Size(99, 20);
            this.uLabelCl.TabIndex = 277;
            this.uLabelCl.Text = "14";
            // 
            // uLabelBr
            // 
            this.uLabelBr.Location = new System.Drawing.Point(267, 164);
            this.uLabelBr.Name = "uLabelBr";
            this.uLabelBr.Size = new System.Drawing.Size(99, 20);
            this.uLabelBr.TabIndex = 278;
            this.uLabelBr.Text = "13";
            // 
            // uLabelHBCDD
            // 
            this.uLabelHBCDD.Location = new System.Drawing.Point(10, 164);
            this.uLabelHBCDD.Name = "uLabelHBCDD";
            this.uLabelHBCDD.Size = new System.Drawing.Size(99, 20);
            this.uLabelHBCDD.TabIndex = 279;
            this.uLabelHBCDD.Text = "11";
            // 
            // uTextDEHP
            // 
            this.uTextDEHP.Location = new System.Drawing.Point(662, 137);
            this.uTextDEHP.Name = "uTextDEHP";
            this.uTextDEHP.Size = new System.Drawing.Size(168, 21);
            this.uTextDEHP.TabIndex = 270;
            // 
            // uTextDBP
            // 
            this.uTextDBP.Location = new System.Drawing.Point(370, 137);
            this.uTextDBP.Name = "uTextDBP";
            this.uTextDBP.Size = new System.Drawing.Size(182, 21);
            this.uTextDBP.TabIndex = 269;
            // 
            // uTextBBP
            // 
            this.uTextBBP.Location = new System.Drawing.Point(113, 137);
            this.uTextBBP.Name = "uTextBBP";
            this.uTextBBP.Size = new System.Drawing.Size(146, 21);
            this.uTextBBP.TabIndex = 268;
            // 
            // uLabelDEHP
            // 
            this.uLabelDEHP.Location = new System.Drawing.Point(559, 137);
            this.uLabelDEHP.Name = "uLabelDEHP";
            this.uLabelDEHP.Size = new System.Drawing.Size(99, 20);
            this.uLabelDEHP.TabIndex = 271;
            this.uLabelDEHP.Text = "14";
            // 
            // uLabelDBP
            // 
            this.uLabelDBP.Location = new System.Drawing.Point(267, 137);
            this.uLabelDBP.Name = "uLabelDBP";
            this.uLabelDBP.Size = new System.Drawing.Size(99, 20);
            this.uLabelDBP.TabIndex = 272;
            this.uLabelDBP.Text = "13";
            // 
            // uLabelBBP
            // 
            this.uLabelBBP.Location = new System.Drawing.Point(10, 137);
            this.uLabelBBP.Name = "uLabelBBP";
            this.uLabelBBP.Size = new System.Drawing.Size(99, 20);
            this.uLabelBBP.TabIndex = 273;
            this.uLabelBBP.Text = "BBP(1000)";
            // 
            // uTextSb
            // 
            this.uTextSb.Location = new System.Drawing.Point(662, 111);
            this.uTextSb.Name = "uTextSb";
            this.uTextSb.Size = new System.Drawing.Size(168, 21);
            this.uTextSb.TabIndex = 264;
            // 
            // uTextPFOA
            // 
            this.uTextPFOA.Location = new System.Drawing.Point(370, 111);
            this.uTextPFOA.Name = "uTextPFOA";
            this.uTextPFOA.Size = new System.Drawing.Size(182, 21);
            this.uTextPFOA.TabIndex = 263;
            // 
            // uTextPFOS
            // 
            this.uTextPFOS.Location = new System.Drawing.Point(113, 111);
            this.uTextPFOS.Name = "uTextPFOS";
            this.uTextPFOS.Size = new System.Drawing.Size(146, 21);
            this.uTextPFOS.TabIndex = 262;
            // 
            // uLabelSb
            // 
            this.uLabelSb.Location = new System.Drawing.Point(559, 111);
            this.uLabelSb.Name = "uLabelSb";
            this.uLabelSb.Size = new System.Drawing.Size(99, 20);
            this.uLabelSb.TabIndex = 265;
            this.uLabelSb.Text = "14";
            // 
            // uLabelPFOA
            // 
            this.uLabelPFOA.Location = new System.Drawing.Point(267, 111);
            this.uLabelPFOA.Name = "uLabelPFOA";
            this.uLabelPFOA.Size = new System.Drawing.Size(99, 20);
            this.uLabelPFOA.TabIndex = 266;
            this.uLabelPFOA.Text = "13";
            // 
            // uLabelPFOS
            // 
            this.uLabelPFOS.Location = new System.Drawing.Point(10, 111);
            this.uLabelPFOS.Name = "uLabelPFOS";
            this.uLabelPFOS.Size = new System.Drawing.Size(99, 20);
            this.uLabelPFOS.TabIndex = 267;
            this.uLabelPFOS.Text = "11";
            // 
            // uTextItemNameDetail
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextItemNameDetail.Appearance = appearance16;
            this.uTextItemNameDetail.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextItemNameDetail.Location = new System.Drawing.Point(370, 36);
            this.uTextItemNameDetail.Name = "uTextItemNameDetail";
            this.uTextItemNameDetail.Size = new System.Drawing.Size(182, 21);
            this.uTextItemNameDetail.TabIndex = 9;
            // 
            // uLabelItemNameDetail
            // 
            this.uLabelItemNameDetail.Location = new System.Drawing.Point(267, 36);
            this.uLabelItemNameDetail.Name = "uLabelItemNameDetail";
            this.uLabelItemNameDetail.Size = new System.Drawing.Size(99, 20);
            this.uLabelItemNameDetail.TabIndex = 261;
            this.uLabelItemNameDetail.Text = "14";
            // 
            // uTextReportNo
            // 
            this.uTextReportNo.Location = new System.Drawing.Point(113, 190);
            this.uTextReportNo.Name = "uTextReportNo";
            this.uTextReportNo.Size = new System.Drawing.Size(146, 21);
            this.uTextReportNo.TabIndex = 17;
            // 
            // uLabelReportNo
            // 
            this.uLabelReportNo.Location = new System.Drawing.Point(10, 190);
            this.uLabelReportNo.Name = "uLabelReportNo";
            this.uLabelReportNo.Size = new System.Drawing.Size(99, 20);
            this.uLabelReportNo.TabIndex = 259;
            this.uLabelReportNo.Text = "14";
            // 
            // uTextPBDEs
            // 
            this.uTextPBDEs.Location = new System.Drawing.Point(370, 60);
            this.uTextPBDEs.Name = "uTextPBDEs";
            this.uTextPBDEs.Size = new System.Drawing.Size(182, 21);
            this.uTextPBDEs.TabIndex = 12;
            // 
            // uLabelPBDesc
            // 
            this.uLabelPBDesc.Location = new System.Drawing.Point(267, 60);
            this.uLabelPBDesc.Name = "uLabelPBDesc";
            this.uLabelPBDesc.Size = new System.Drawing.Size(99, 20);
            this.uLabelPBDesc.TabIndex = 257;
            this.uLabelPBDesc.Text = "12";
            // 
            // uTextItemName
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextItemName.Appearance = appearance17;
            this.uTextItemName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextItemName.Location = new System.Drawing.Point(113, 36);
            this.uTextItemName.Name = "uTextItemName";
            this.uTextItemName.Size = new System.Drawing.Size(146, 21);
            this.uTextItemName.TabIndex = 8;
            // 
            // uLabelItemName
            // 
            this.uLabelItemName.Location = new System.Drawing.Point(10, 36);
            this.uLabelItemName.Name = "uLabelItemName";
            this.uLabelItemName.Size = new System.Drawing.Size(99, 20);
            this.uLabelItemName.TabIndex = 255;
            this.uLabelItemName.Text = "14";
            // 
            // uTextVendorName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance18;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(459, 12);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(93, 21);
            this.uTextVendorName.TabIndex = 254;
            // 
            // uCheckMSDS
            // 
            this.uCheckMSDS.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckMSDS.Location = new System.Drawing.Point(662, 190);
            this.uCheckMSDS.Name = "uCheckMSDS";
            this.uCheckMSDS.Size = new System.Drawing.Size(17, 20);
            this.uCheckMSDS.TabIndex = 253;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uButtonFileDown);
            this.uGroupBox1.Controls.Add(this.uGridDetail);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Location = new System.Drawing.Point(10, 241);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(885, 406);
            this.uGroupBox1.TabIndex = 252;
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(96, 28);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(82, 28);
            this.uButtonFileDown.TabIndex = 2;
            this.uButtonFileDown.Text = "ultraButton1";
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // uGridDetail
            // 
            this.uGridDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetail.DisplayLayout.Appearance = appearance19;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGridDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGridDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetail.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetail.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGridDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetail.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetail.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.uGridDetail.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.uGridDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetail.DisplayLayout.Override.RowAppearance = appearance29;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.uGridDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetail.Location = new System.Drawing.Point(10, 62);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(865, 332);
            this.uGridDetail.TabIndex = 20;
            this.uGridDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGridDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridDetail_KeyDown);
            this.uGridDetail.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDetail_ClickCellButton);
            this.uGridDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDetail_CellChange);
            this.uGridDetail.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDetail_DoubleClickCell);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(10, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(82, 28);
            this.uButtonDeleteRow.TabIndex = 0;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextCr
            // 
            this.uTextCr.Location = new System.Drawing.Point(662, 84);
            this.uTextCr.Name = "uTextCr";
            this.uTextCr.Size = new System.Drawing.Size(168, 21);
            this.uTextCr.TabIndex = 16;
            // 
            // uTextHg
            // 
            this.uTextHg.Location = new System.Drawing.Point(370, 84);
            this.uTextHg.Name = "uTextHg";
            this.uTextHg.Size = new System.Drawing.Size(182, 21);
            this.uTextHg.TabIndex = 15;
            // 
            // uDateWriteDate
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance31;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DateTime = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(662, 12);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateWriteDate.TabIndex = 7;
            this.uDateWriteDate.Value = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            // 
            // uDateRohsEndDate
            // 
            appearance32.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRohsEndDate.Appearance = appearance32;
            this.uDateRohsEndDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRohsEndDate.DateTime = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            this.uDateRohsEndDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRohsEndDate.Location = new System.Drawing.Point(370, 190);
            this.uDateRohsEndDate.Name = "uDateRohsEndDate";
            this.uDateRohsEndDate.Size = new System.Drawing.Size(86, 21);
            this.uDateRohsEndDate.TabIndex = 18;
            this.uDateRohsEndDate.Value = new System.DateTime(2015, 9, 14, 0, 0, 0, 0);
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(113, 214);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(439, 21);
            this.uTextEtcDesc.TabIndex = 19;
            // 
            // uTextPBBs
            // 
            this.uTextPBBs.Location = new System.Drawing.Point(113, 60);
            this.uTextPBBs.Name = "uTextPBBs";
            this.uTextPBBs.Size = new System.Drawing.Size(146, 21);
            this.uTextPBBs.TabIndex = 11;
            // 
            // uTextPb
            // 
            this.uTextPb.Location = new System.Drawing.Point(662, 60);
            this.uTextPb.Name = "uTextPb";
            this.uTextPb.Size = new System.Drawing.Size(168, 21);
            this.uTextPb.TabIndex = 13;
            // 
            // uTextCd
            // 
            this.uTextCd.Location = new System.Drawing.Point(113, 84);
            this.uTextCd.Name = "uTextCd";
            this.uTextCd.Size = new System.Drawing.Size(146, 21);
            this.uTextCd.TabIndex = 14;
            // 
            // uTextWriteUserName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance33;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(881, 214);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(18, 21);
            this.uTextWriteUserName.TabIndex = 250;
            this.uTextWriteUserName.Visible = false;
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(559, 36);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(99, 20);
            this.uLabelWriteUser.TabIndex = 248;
            this.uLabelWriteUser.Text = "20";
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(559, 12);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(99, 20);
            this.uLabelWriteDate.TabIndex = 245;
            this.uLabelWriteDate.Text = "19";
            // 
            // uComboPlant
            // 
            appearance34.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance34;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(113, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(129, 21);
            this.uComboPlant.TabIndex = 5;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uTextWriteUserID
            // 
            appearance35.BackColor = System.Drawing.Color.White;
            this.uTextWriteUserID.Appearance = appearance35;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.White;
            appearance36.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance36.TextHAlignAsString = "Center";
            editorButton2.Appearance = appearance36;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Visible = false;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton2);
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(662, 36);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(168, 21);
            this.uTextWriteUserID.TabIndex = 10;
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uTextVendorCode
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVendorCode.Appearance = appearance37;
            this.uTextVendorCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance38.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance38.TextHAlignAsString = "Center";
            editorButton3.Appearance = appearance38;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextVendorCode.ButtonsRight.Add(editorButton3);
            this.uTextVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVendorCode.Location = new System.Drawing.Point(370, 12);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.Size = new System.Drawing.Size(86, 21);
            this.uTextVendorCode.TabIndex = 6;
            this.uTextVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextVendorCode_EditorButtonClick);
            // 
            // uLabelPBBs
            // 
            this.uLabelPBBs.Location = new System.Drawing.Point(10, 60);
            this.uLabelPBBs.Name = "uLabelPBBs";
            this.uLabelPBBs.Size = new System.Drawing.Size(99, 20);
            this.uLabelPBBs.TabIndex = 233;
            this.uLabelPBBs.Text = "15";
            // 
            // uLabelMSDS
            // 
            this.uLabelMSDS.Location = new System.Drawing.Point(559, 190);
            this.uLabelMSDS.Name = "uLabelMSDS";
            this.uLabelMSDS.Size = new System.Drawing.Size(99, 20);
            this.uLabelMSDS.TabIndex = 244;
            this.uLabelMSDS.Text = "16";
            // 
            // uLabelRohsEndDate
            // 
            this.uLabelRohsEndDate.Location = new System.Drawing.Point(267, 190);
            this.uLabelRohsEndDate.Name = "uLabelRohsEndDate";
            this.uLabelRohsEndDate.Size = new System.Drawing.Size(99, 20);
            this.uLabelRohsEndDate.TabIndex = 236;
            this.uLabelRohsEndDate.Text = "17";
            // 
            // uLabelCr
            // 
            this.uLabelCr.Location = new System.Drawing.Point(559, 84);
            this.uLabelCr.Name = "uLabelCr";
            this.uLabelCr.Size = new System.Drawing.Size(99, 20);
            this.uLabelCr.TabIndex = 237;
            this.uLabelCr.Text = "14";
            // 
            // uLabelHg
            // 
            this.uLabelHg.Location = new System.Drawing.Point(267, 84);
            this.uLabelHg.Name = "uLabelHg";
            this.uLabelHg.Size = new System.Drawing.Size(99, 20);
            this.uLabelHg.TabIndex = 240;
            this.uLabelHg.Text = "13";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(10, 214);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(99, 20);
            this.uLabelEtcDesc.TabIndex = 232;
            this.uLabelEtcDesc.Text = "18";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(10, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(99, 20);
            this.uLabelPlant.TabIndex = 234;
            this.uLabelPlant.Text = "6";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(267, 12);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(99, 20);
            this.uLabelVendor.TabIndex = 235;
            this.uLabelVendor.Text = "7";
            // 
            // uLabelPB
            // 
            this.uLabelPB.Location = new System.Drawing.Point(559, 60);
            this.uLabelPB.Name = "uLabelPB";
            this.uLabelPB.Size = new System.Drawing.Size(99, 20);
            this.uLabelPB.TabIndex = 239;
            this.uLabelPB.Text = "12";
            // 
            // uLabelCd
            // 
            this.uLabelCd.Location = new System.Drawing.Point(10, 84);
            this.uLabelCd.Name = "uLabelCd";
            this.uLabelCd.Size = new System.Drawing.Size(99, 20);
            this.uLabelCd.TabIndex = 241;
            this.uLabelCd.Text = "11";
            // 
            // uCheckNOLongerBuy
            // 
            this.uCheckNOLongerBuy.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckNOLongerBuy.Location = new System.Drawing.Point(662, 214);
            this.uCheckNOLongerBuy.Name = "uCheckNOLongerBuy";
            this.uCheckNOLongerBuy.Size = new System.Drawing.Size(17, 20);
            this.uCheckNOLongerBuy.TabIndex = 281;
            // 
            // uLabelNOLongerBuy
            // 
            this.uLabelNOLongerBuy.Location = new System.Drawing.Point(559, 215);
            this.uLabelNOLongerBuy.Name = "uLabelNOLongerBuy";
            this.uLabelNOLongerBuy.Size = new System.Drawing.Size(97, 20);
            this.uLabelNOLongerBuy.TabIndex = 280;
            this.uLabelNOLongerBuy.Text = "不再购买";
            // 
            // uLabelDesc
            // 
            this.uLabelDesc.Location = new System.Drawing.Point(711, 12);
            this.uLabelDesc.Name = "uLabelDesc";
            this.uLabelDesc.Size = new System.Drawing.Size(392, 20);
            this.uLabelDesc.TabIndex = 282;
            this.uLabelDesc.Text = "黄色：距离结束日不足一个月；红色：超过结束日；绿色：不再购买";
            // 
            // frmQATZ0006
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0006";
            this.Load += new System.EventHandler(this.frmQATZ0006_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0006_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0006_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0006_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchRohsEndDateOver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHBCDD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDEHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDBP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBBP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPFOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPFOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextItemNameDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReportNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPBDEs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMSDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRohsEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPBBs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckNOLongerBuy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRohsEndDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRohsEndDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRohsEndDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPBBs;
        private Infragistics.Win.Misc.UltraLabel uLabelMSDS;
        private Infragistics.Win.Misc.UltraLabel uLabelRohsEndDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCr;
        private Infragistics.Win.Misc.UltraLabel uLabelHg;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelPB;
        private Infragistics.Win.Misc.UltraLabel uLabelCd;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCd;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCr;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHg;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRohsEndDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPBBs;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPb;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMSDS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchItemName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchItemName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextItemName;
        private Infragistics.Win.Misc.UltraLabel uLabelItemName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPBDEs;
        private Infragistics.Win.Misc.UltraLabel uLabelPBDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReportNo;
        private Infragistics.Win.Misc.UltraLabel uLabelReportNo;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextItemNameDetail;
        private Infragistics.Win.Misc.UltraLabel uLabelItemNameDetail;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchRohsEndDateOver;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRohsEndDateOver;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSb;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPFOA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPFOS;
        private Infragistics.Win.Misc.UltraLabel uLabelSb;
        private Infragistics.Win.Misc.UltraLabel uLabelPFOA;
        private Infragistics.Win.Misc.UltraLabel uLabelPFOS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCl;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBr;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHBCDD;
        private Infragistics.Win.Misc.UltraLabel uLabelCl;
        private Infragistics.Win.Misc.UltraLabel uLabelBr;
        private Infragistics.Win.Misc.UltraLabel uLabelHBCDD;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDEHP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDBP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBBP;
        private Infragistics.Win.Misc.UltraLabel uLabelDEHP;
        private Infragistics.Win.Misc.UltraLabel uLabelDBP;
        private Infragistics.Win.Misc.UltraLabel uLabelBBP;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckNOLongerBuy;
        private Infragistics.Win.Misc.UltraLabel uLabelNOLongerBuy;
        private Infragistics.Win.Misc.UltraLabel uLabelDesc;
    }
}