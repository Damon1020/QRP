﻿namespace QRPQAT.UI
{
    partial class frmQATZ0013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0013));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("up");
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("down");
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchInspectFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectFaultType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcessOperationType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcessOperationType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridInprocAccidentList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxCA = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextInspectResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectResult = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCAAttachFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCAAttachFile = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRegistUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRegistUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRegistUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCAInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCAInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxReason_Action = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextMaterialTreat = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialTreat = new Infragistics.Win.Misc.UltraLabel();
            this.uDateActionDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelActionDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReasonDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReasonDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4ME = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMaterialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckEnviromentFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMethodFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMachineFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckManFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextFilePath = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFilePath = new Infragistics.Win.Misc.UltraLabel();
            this.uTextActionDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelActionDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReasonDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReasonDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDetailList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxAriseInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextMgnNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMgnNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboAriseEquipCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelAriseEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uComboAriseProcessCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelAriseProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInspectFaultType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextManagementNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelManagementNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateIssueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelIssueDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAriseDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInprocAccidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCA)).BeginInit();
            this.uGroupBoxCA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCAAttachFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCAInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxReason_Action)).BeginInit();
            this.uGroupBoxReason_Action.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialTreat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateActionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReasonDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnviromentFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethodFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachineFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckManFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextActionDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReasonDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAriseInfo)).BeginInit();
            this.uGroupBoxAriseInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMgnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManagementNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateIssueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectFaultTypeCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectFaultType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDetailProcessOperationType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDetailProcessOperationType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchInspectFaultTypeCode
            // 
            this.uComboSearchInspectFaultTypeCode.Location = new System.Drawing.Point(712, 36);
            this.uComboSearchInspectFaultTypeCode.Name = "uComboSearchInspectFaultTypeCode";
            this.uComboSearchInspectFaultTypeCode.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchInspectFaultTypeCode.TabIndex = 161;
            this.uComboSearchInspectFaultTypeCode.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectFaultType
            // 
            this.uLabelSearchInspectFaultType.Location = new System.Drawing.Point(608, 36);
            this.uLabelSearchInspectFaultType.Name = "uLabelSearchInspectFaultType";
            this.uLabelSearchInspectFaultType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectFaultType.TabIndex = 160;
            this.uLabelSearchInspectFaultType.Text = "ultraLabel1";
            // 
            // uComboSearchDetailProcessOperationType
            // 
            this.uComboSearchDetailProcessOperationType.Location = new System.Drawing.Point(448, 36);
            this.uComboSearchDetailProcessOperationType.Name = "uComboSearchDetailProcessOperationType";
            this.uComboSearchDetailProcessOperationType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchDetailProcessOperationType.TabIndex = 159;
            this.uComboSearchDetailProcessOperationType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchDetailProcessOperationType
            // 
            this.uLabelSearchDetailProcessOperationType.Location = new System.Drawing.Point(344, 36);
            this.uLabelSearchDetailProcessOperationType.Name = "uLabelSearchDetailProcessOperationType";
            this.uLabelSearchDetailProcessOperationType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDetailProcessOperationType.TabIndex = 158;
            this.uLabelSearchDetailProcessOperationType.Text = "ultraLabel1";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(216, 21);
            this.uComboSearchPackage.TabIndex = 157;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 156;
            this.uLabelSearchPackage.Text = "ultraLabel1";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(712, 12);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchLotNo.TabIndex = 155;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(608, 12);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 154;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(448, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchCustomer.TabIndex = 153;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(344, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 152;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance6;
            this.ultraLabel1.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel1.TabIndex = 151;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchToAriseDate
            // 
            this.uDateSearchToAriseDate.Location = new System.Drawing.Point(233, 12);
            this.uDateSearchToAriseDate.Name = "uDateSearchToAriseDate";
            this.uDateSearchToAriseDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToAriseDate.TabIndex = 150;
            // 
            // uDateSearchFromAriseDate
            // 
            this.uDateSearchFromAriseDate.Location = new System.Drawing.Point(116, 12);
            this.uDateSearchFromAriseDate.Name = "uDateSearchFromAriseDate";
            this.uDateSearchFromAriseDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromAriseDate.TabIndex = 149;
            // 
            // uLabelSearchAriseDate
            // 
            this.uLabelSearchAriseDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchAriseDate.Name = "uLabelSearchAriseDate";
            this.uLabelSearchAriseDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchAriseDate.TabIndex = 148;
            this.uLabelSearchAriseDate.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1044, 4);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1020, 4);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridInprocAccidentList
            // 
            this.uGridInprocAccidentList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInprocAccidentList.DisplayLayout.Appearance = appearance2;
            this.uGridInprocAccidentList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInprocAccidentList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInprocAccidentList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInprocAccidentList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridInprocAccidentList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInprocAccidentList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridInprocAccidentList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInprocAccidentList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInprocAccidentList.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInprocAccidentList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridInprocAccidentList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInprocAccidentList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInprocAccidentList.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInprocAccidentList.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridInprocAccidentList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInprocAccidentList.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInprocAccidentList.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGridInprocAccidentList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridInprocAccidentList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInprocAccidentList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridInprocAccidentList.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridInprocAccidentList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInprocAccidentList.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGridInprocAccidentList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInprocAccidentList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInprocAccidentList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInprocAccidentList.Location = new System.Drawing.Point(0, 100);
            this.uGridInprocAccidentList.Name = "uGridInprocAccidentList";
            this.uGridInprocAccidentList.Size = new System.Drawing.Size(1070, 720);
            this.uGridInprocAccidentList.TabIndex = 2;
            this.uGridInprocAccidentList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridInprocAccidentList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxCA);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDelete);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxReason_Action);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridDetailList);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxAriseInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxCA
            // 
            this.uGroupBoxCA.Controls.Add(this.uTextInspectResult);
            this.uGroupBoxCA.Controls.Add(this.uLabelInspectResult);
            this.uGroupBoxCA.Controls.Add(this.uTextCAAttachFile);
            this.uGroupBoxCA.Controls.Add(this.uLabelCAAttachFile);
            this.uGroupBoxCA.Controls.Add(this.uTextRegistUserName);
            this.uGroupBoxCA.Controls.Add(this.uTextRegistUserID);
            this.uGroupBoxCA.Controls.Add(this.uLabelRegistUser);
            this.uGroupBoxCA.Controls.Add(this.uDateCAInspectDate);
            this.uGroupBoxCA.Controls.Add(this.uLabelCAInspectDate);
            this.uGroupBoxCA.Location = new System.Drawing.Point(8, 508);
            this.uGroupBoxCA.Name = "uGroupBoxCA";
            this.uGroupBoxCA.Size = new System.Drawing.Size(1040, 144);
            this.uGroupBoxCA.TabIndex = 4;
            // 
            // uTextInspectResult
            // 
            this.uTextInspectResult.Location = new System.Drawing.Point(148, 84);
            this.uTextInspectResult.Multiline = true;
            this.uTextInspectResult.Name = "uTextInspectResult";
            this.uTextInspectResult.Size = new System.Drawing.Size(503, 44);
            this.uTextInspectResult.TabIndex = 181;
            // 
            // uLabelInspectResult
            // 
            this.uLabelInspectResult.Location = new System.Drawing.Point(12, 84);
            this.uLabelInspectResult.Name = "uLabelInspectResult";
            this.uLabelInspectResult.Size = new System.Drawing.Size(130, 23);
            this.uLabelInspectResult.TabIndex = 180;
            this.uLabelInspectResult.Text = "ultraLabel2";
            // 
            // uTextCAAttachFile
            // 
            appearance33.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            editorButton1.Appearance = appearance33;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "up";
            appearance34.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            editorButton2.Appearance = appearance34;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "down";
            this.uTextCAAttachFile.ButtonsRight.Add(editorButton1);
            this.uTextCAAttachFile.ButtonsRight.Add(editorButton2);
            this.uTextCAAttachFile.Location = new System.Drawing.Point(148, 55);
            this.uTextCAAttachFile.Name = "uTextCAAttachFile";
            this.uTextCAAttachFile.ReadOnly = true;
            this.uTextCAAttachFile.Size = new System.Drawing.Size(503, 21);
            this.uTextCAAttachFile.TabIndex = 179;
            this.uTextCAAttachFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCAAttachFile_EditorButtonClick);
            // 
            // uLabelCAAttachFile
            // 
            this.uLabelCAAttachFile.Location = new System.Drawing.Point(12, 55);
            this.uLabelCAAttachFile.Name = "uLabelCAAttachFile";
            this.uLabelCAAttachFile.Size = new System.Drawing.Size(130, 23);
            this.uLabelCAAttachFile.TabIndex = 178;
            this.uLabelCAAttachFile.Text = "ultraLabel3";
            // 
            // uTextRegistUserName
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRegistUserName.Appearance = appearance31;
            this.uTextRegistUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRegistUserName.Location = new System.Drawing.Point(263, 28);
            this.uTextRegistUserName.Name = "uTextRegistUserName";
            this.uTextRegistUserName.ReadOnly = true;
            this.uTextRegistUserName.Size = new System.Drawing.Size(114, 21);
            this.uTextRegistUserName.TabIndex = 177;
            // 
            // uTextRegistUserID
            // 
            appearance32.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            editorButton3.Appearance = appearance32;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRegistUserID.ButtonsRight.Add(editorButton3);
            this.uTextRegistUserID.Location = new System.Drawing.Point(148, 28);
            this.uTextRegistUserID.Name = "uTextRegistUserID";
            this.uTextRegistUserID.Size = new System.Drawing.Size(114, 21);
            this.uTextRegistUserID.TabIndex = 176;
            this.uTextRegistUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRegistUserID_KeyDown);
            this.uTextRegistUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRegistUserID_EditorButtonClick);
            // 
            // uLabelRegistUser
            // 
            this.uLabelRegistUser.Location = new System.Drawing.Point(12, 28);
            this.uLabelRegistUser.Name = "uLabelRegistUser";
            this.uLabelRegistUser.Size = new System.Drawing.Size(130, 23);
            this.uLabelRegistUser.TabIndex = 175;
            this.uLabelRegistUser.Text = "ultraLabel2";
            // 
            // uDateCAInspectDate
            // 
            this.uDateCAInspectDate.Location = new System.Drawing.Point(496, 28);
            this.uDateCAInspectDate.Name = "uDateCAInspectDate";
            this.uDateCAInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCAInspectDate.TabIndex = 174;
            // 
            // uLabelCAInspectDate
            // 
            this.uLabelCAInspectDate.Location = new System.Drawing.Point(391, 28);
            this.uLabelCAInspectDate.Name = "uLabelCAInspectDate";
            this.uLabelCAInspectDate.Size = new System.Drawing.Size(100, 23);
            this.uLabelCAInspectDate.TabIndex = 0;
            this.uLabelCAInspectDate.Text = "ultraLabel2";
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(8, 88);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 3;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGroupBoxReason_Action
            // 
            this.uGroupBoxReason_Action.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxReason_Action.Controls.Add(this.uTextMaterialTreat);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelMaterialTreat);
            this.uGroupBoxReason_Action.Controls.Add(this.uDateActionDate);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelActionDate);
            this.uGroupBoxReason_Action.Controls.Add(this.uDateReasonDate);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelReasonDate);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabel4ME);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelCompleteFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckMaterialFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckEnviromentFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckMethodFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckMachineFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uCheckManFlag);
            this.uGroupBoxReason_Action.Controls.Add(this.uTextFilePath);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelFilePath);
            this.uGroupBoxReason_Action.Controls.Add(this.uTextActionDesc);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelActionDesc);
            this.uGroupBoxReason_Action.Controls.Add(this.uTextReasonDesc);
            this.uGroupBoxReason_Action.Controls.Add(this.uLabelReasonDesc);
            this.uGroupBoxReason_Action.Location = new System.Drawing.Point(8, 284);
            this.uGroupBoxReason_Action.Name = "uGroupBoxReason_Action";
            this.uGroupBoxReason_Action.Size = new System.Drawing.Size(1050, 222);
            this.uGroupBoxReason_Action.TabIndex = 2;
            // 
            // uTextMaterialTreat
            // 
            this.uTextMaterialTreat.Location = new System.Drawing.Point(116, 124);
            this.uTextMaterialTreat.Multiline = true;
            this.uTextMaterialTreat.Name = "uTextMaterialTreat";
            this.uTextMaterialTreat.Size = new System.Drawing.Size(660, 43);
            this.uTextMaterialTreat.TabIndex = 177;
            // 
            // uLabelMaterialTreat
            // 
            this.uLabelMaterialTreat.Location = new System.Drawing.Point(12, 128);
            this.uLabelMaterialTreat.Name = "uLabelMaterialTreat";
            this.uLabelMaterialTreat.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialTreat.TabIndex = 176;
            this.uLabelMaterialTreat.Text = "ultraLabel1";
            // 
            // uDateActionDate
            // 
            this.uDateActionDate.Location = new System.Drawing.Point(912, 195);
            this.uDateActionDate.Name = "uDateActionDate";
            this.uDateActionDate.Size = new System.Drawing.Size(100, 21);
            this.uDateActionDate.TabIndex = 175;
            // 
            // uLabelActionDate
            // 
            this.uLabelActionDate.Location = new System.Drawing.Point(808, 195);
            this.uLabelActionDate.Name = "uLabelActionDate";
            this.uLabelActionDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelActionDate.TabIndex = 174;
            this.uLabelActionDate.Text = "ultraLabel1";
            // 
            // uDateReasonDate
            // 
            this.uDateReasonDate.Location = new System.Drawing.Point(916, 32);
            this.uDateReasonDate.Name = "uDateReasonDate";
            this.uDateReasonDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReasonDate.TabIndex = 173;
            // 
            // uLabelReasonDate
            // 
            this.uLabelReasonDate.Location = new System.Drawing.Point(784, 32);
            this.uLabelReasonDate.Name = "uLabelReasonDate";
            this.uLabelReasonDate.Size = new System.Drawing.Size(128, 20);
            this.uLabelReasonDate.TabIndex = 172;
            this.uLabelReasonDate.Text = "ultraLabel1";
            // 
            // uLabel4ME
            // 
            this.uLabel4ME.Location = new System.Drawing.Point(12, 195);
            this.uLabel4ME.Name = "uLabel4ME";
            this.uLabel4ME.Size = new System.Drawing.Size(100, 20);
            this.uLabel4ME.TabIndex = 171;
            this.uLabel4ME.Text = "ultraLabel1";
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(684, 195);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteFlag.TabIndex = 170;
            this.uLabelCompleteFlag.Text = "ultraLabel1";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(784, 195);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(20, 20);
            this.uCheckCompleteFlag.TabIndex = 169;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedChanged);
            // 
            // uCheckMaterialFlag
            // 
            this.uCheckMaterialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMaterialFlag.Location = new System.Drawing.Point(452, 195);
            this.uCheckMaterialFlag.Name = "uCheckMaterialFlag";
            this.uCheckMaterialFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMaterialFlag.TabIndex = 168;
            this.uCheckMaterialFlag.Text = "자재";
            this.uCheckMaterialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckEnviromentFlag
            // 
            this.uCheckEnviromentFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckEnviromentFlag.Location = new System.Drawing.Point(368, 195);
            this.uCheckEnviromentFlag.Name = "uCheckEnviromentFlag";
            this.uCheckEnviromentFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckEnviromentFlag.TabIndex = 167;
            this.uCheckEnviromentFlag.Text = "환경";
            this.uCheckEnviromentFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMethodFlag
            // 
            this.uCheckMethodFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMethodFlag.Location = new System.Drawing.Point(200, 195);
            this.uCheckMethodFlag.Name = "uCheckMethodFlag";
            this.uCheckMethodFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMethodFlag.TabIndex = 166;
            this.uCheckMethodFlag.Text = "방법";
            this.uCheckMethodFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMachineFlag
            // 
            this.uCheckMachineFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMachineFlag.Location = new System.Drawing.Point(284, 195);
            this.uCheckMachineFlag.Name = "uCheckMachineFlag";
            this.uCheckMachineFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMachineFlag.TabIndex = 165;
            this.uCheckMachineFlag.Text = "장비";
            this.uCheckMachineFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckManFlag
            // 
            this.uCheckManFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckManFlag.Location = new System.Drawing.Point(116, 195);
            this.uCheckManFlag.Name = "uCheckManFlag";
            this.uCheckManFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckManFlag.TabIndex = 164;
            this.uCheckManFlag.Text = "사람";
            this.uCheckManFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextFilePath
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFilePath.Appearance = appearance28;
            this.uTextFilePath.BackColor = System.Drawing.Color.Gainsboro;
            appearance29.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance29.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance29;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "UP";
            appearance30.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance30.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance30;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "DOWN";
            this.uTextFilePath.ButtonsRight.Add(editorButton4);
            this.uTextFilePath.ButtonsRight.Add(editorButton5);
            this.uTextFilePath.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextFilePath.Location = new System.Drawing.Point(116, 171);
            this.uTextFilePath.Name = "uTextFilePath";
            this.uTextFilePath.ReadOnly = true;
            this.uTextFilePath.Size = new System.Drawing.Size(900, 21);
            this.uTextFilePath.TabIndex = 163;
            this.uTextFilePath.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFilePath_EditorButtonClick);
            // 
            // uLabelFilePath
            // 
            this.uLabelFilePath.Location = new System.Drawing.Point(12, 171);
            this.uLabelFilePath.Name = "uLabelFilePath";
            this.uLabelFilePath.Size = new System.Drawing.Size(100, 20);
            this.uLabelFilePath.TabIndex = 162;
            this.uLabelFilePath.Text = "ultraLabel1";
            // 
            // uTextActionDesc
            // 
            this.uTextActionDesc.Location = new System.Drawing.Point(116, 76);
            this.uTextActionDesc.Multiline = true;
            this.uTextActionDesc.Name = "uTextActionDesc";
            this.uTextActionDesc.Size = new System.Drawing.Size(660, 43);
            this.uTextActionDesc.TabIndex = 161;
            // 
            // uLabelActionDesc
            // 
            this.uLabelActionDesc.Location = new System.Drawing.Point(12, 80);
            this.uLabelActionDesc.Name = "uLabelActionDesc";
            this.uLabelActionDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelActionDesc.TabIndex = 160;
            this.uLabelActionDesc.Text = "ultraLabel1";
            // 
            // uTextReasonDesc
            // 
            this.uTextReasonDesc.Location = new System.Drawing.Point(116, 28);
            this.uTextReasonDesc.Multiline = true;
            this.uTextReasonDesc.Name = "uTextReasonDesc";
            this.uTextReasonDesc.Size = new System.Drawing.Size(660, 43);
            this.uTextReasonDesc.TabIndex = 159;
            // 
            // uLabelReasonDesc
            // 
            this.uLabelReasonDesc.Location = new System.Drawing.Point(12, 28);
            this.uLabelReasonDesc.Name = "uLabelReasonDesc";
            this.uLabelReasonDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelReasonDesc.TabIndex = 158;
            this.uLabelReasonDesc.Text = "ultraLabel1";
            // 
            // uGridDetailList
            // 
            this.uGridDetailList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetailList.DisplayLayout.Appearance = appearance15;
            this.uGridDetailList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetailList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailList.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGridDetailList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetailList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridDetailList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetailList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetailList.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetailList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridDetailList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetailList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetailList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetailList.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGridDetailList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetailList.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetailList.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGridDetailList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridDetailList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetailList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetailList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridDetailList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetailList.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGridDetailList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetailList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetailList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetailList.Location = new System.Drawing.Point(8, 116);
            this.uGridDetailList.Name = "uGridDetailList";
            this.uGridDetailList.Size = new System.Drawing.Size(1050, 164);
            this.uGridDetailList.TabIndex = 1;
            this.uGridDetailList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDetailList_AfterCellUpdate);
            // 
            // uGroupBoxAriseInfo
            // 
            this.uGroupBoxAriseInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxAriseInfo.Controls.Add(this.uTextMgnNo);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelMgnNo);
            this.uGroupBoxAriseInfo.Controls.Add(this.uComboAriseEquipCode);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelAriseEquip);
            this.uGroupBoxAriseInfo.Controls.Add(this.uComboAriseProcessCode);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelAriseProcess);
            this.uGroupBoxAriseInfo.Controls.Add(this.uComboInspectFaultTypeCode);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelInspectFaultType);
            this.uGroupBoxAriseInfo.Controls.Add(this.uTextManagementNo);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelManagementNo);
            this.uGroupBoxAriseInfo.Controls.Add(this.uComboPlant);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxAriseInfo.Controls.Add(this.uDateIssueDate);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelIssueDate);
            this.uGroupBoxAriseInfo.Controls.Add(this.uDateAriseDate);
            this.uGroupBoxAriseInfo.Controls.Add(this.uLabelAriseDate);
            this.uGroupBoxAriseInfo.Location = new System.Drawing.Point(8, 8);
            this.uGroupBoxAriseInfo.Name = "uGroupBoxAriseInfo";
            this.uGroupBoxAriseInfo.Size = new System.Drawing.Size(1050, 80);
            this.uGroupBoxAriseInfo.TabIndex = 0;
            // 
            // uTextMgnNo
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMgnNo.Appearance = appearance27;
            this.uTextMgnNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMgnNo.Location = new System.Drawing.Point(116, 28);
            this.uTextMgnNo.Name = "uTextMgnNo";
            this.uTextMgnNo.ReadOnly = true;
            this.uTextMgnNo.Size = new System.Drawing.Size(188, 21);
            this.uTextMgnNo.TabIndex = 167;
            // 
            // uLabelMgnNo
            // 
            this.uLabelMgnNo.Location = new System.Drawing.Point(12, 28);
            this.uLabelMgnNo.Name = "uLabelMgnNo";
            this.uLabelMgnNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelMgnNo.TabIndex = 166;
            this.uLabelMgnNo.Text = "ultraLabel1";
            // 
            // uComboAriseEquipCode
            // 
            this.uComboAriseEquipCode.Location = new System.Drawing.Point(948, 28);
            this.uComboAriseEquipCode.Name = "uComboAriseEquipCode";
            this.uComboAriseEquipCode.Size = new System.Drawing.Size(20, 21);
            this.uComboAriseEquipCode.TabIndex = 165;
            this.uComboAriseEquipCode.Text = "ultraComboEditor1";
            this.uComboAriseEquipCode.Visible = false;
            // 
            // uLabelAriseEquip
            // 
            this.uLabelAriseEquip.Location = new System.Drawing.Point(928, 28);
            this.uLabelAriseEquip.Name = "uLabelAriseEquip";
            this.uLabelAriseEquip.Size = new System.Drawing.Size(16, 20);
            this.uLabelAriseEquip.TabIndex = 164;
            this.uLabelAriseEquip.Text = "ultraLabel1";
            this.uLabelAriseEquip.Visible = false;
            // 
            // uComboAriseProcessCode
            // 
            this.uComboAriseProcessCode.Location = new System.Drawing.Point(660, 28);
            this.uComboAriseProcessCode.Name = "uComboAriseProcessCode";
            this.uComboAriseProcessCode.Size = new System.Drawing.Size(188, 21);
            this.uComboAriseProcessCode.TabIndex = 163;
            this.uComboAriseProcessCode.Text = "ultraComboEditor1";
            // 
            // uLabelAriseProcess
            // 
            this.uLabelAriseProcess.Location = new System.Drawing.Point(544, 28);
            this.uLabelAriseProcess.Name = "uLabelAriseProcess";
            this.uLabelAriseProcess.Size = new System.Drawing.Size(112, 20);
            this.uLabelAriseProcess.TabIndex = 162;
            this.uLabelAriseProcess.Text = "ultraLabel1";
            // 
            // uComboInspectFaultTypeCode
            // 
            this.uComboInspectFaultTypeCode.Location = new System.Drawing.Point(660, 52);
            this.uComboInspectFaultTypeCode.Name = "uComboInspectFaultTypeCode";
            this.uComboInspectFaultTypeCode.Size = new System.Drawing.Size(188, 21);
            this.uComboInspectFaultTypeCode.TabIndex = 161;
            this.uComboInspectFaultTypeCode.Text = "ultraComboEditor1";
            // 
            // uLabelInspectFaultType
            // 
            this.uLabelInspectFaultType.Location = new System.Drawing.Point(544, 52);
            this.uLabelInspectFaultType.Name = "uLabelInspectFaultType";
            this.uLabelInspectFaultType.Size = new System.Drawing.Size(112, 20);
            this.uLabelInspectFaultType.TabIndex = 160;
            this.uLabelInspectFaultType.Text = "ultraLabel1";
            // 
            // uTextManagementNo
            // 
            this.uTextManagementNo.Location = new System.Drawing.Point(1004, 52);
            this.uTextManagementNo.Name = "uTextManagementNo";
            this.uTextManagementNo.Size = new System.Drawing.Size(24, 21);
            this.uTextManagementNo.TabIndex = 157;
            this.uTextManagementNo.Visible = false;
            // 
            // uLabelManagementNo
            // 
            this.uLabelManagementNo.Location = new System.Drawing.Point(972, 52);
            this.uLabelManagementNo.Name = "uLabelManagementNo";
            this.uLabelManagementNo.Size = new System.Drawing.Size(28, 20);
            this.uLabelManagementNo.TabIndex = 156;
            this.uLabelManagementNo.Text = "ultraLabel1";
            this.uLabelManagementNo.Visible = false;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(1004, 28);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(24, 21);
            this.uComboPlant.TabIndex = 155;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.Visible = false;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(972, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(28, 20);
            this.uLabelPlant.TabIndex = 154;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uDateIssueDate
            // 
            this.uDateIssueDate.Location = new System.Drawing.Point(424, 52);
            this.uDateIssueDate.Name = "uDateIssueDate";
            this.uDateIssueDate.Size = new System.Drawing.Size(100, 21);
            this.uDateIssueDate.TabIndex = 153;
            // 
            // uLabelIssueDate
            // 
            this.uLabelIssueDate.Location = new System.Drawing.Point(320, 52);
            this.uLabelIssueDate.Name = "uLabelIssueDate";
            this.uLabelIssueDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelIssueDate.TabIndex = 152;
            this.uLabelIssueDate.Text = "ultraLabel1";
            // 
            // uDateAriseDate
            // 
            this.uDateAriseDate.Location = new System.Drawing.Point(424, 28);
            this.uDateAriseDate.Name = "uDateAriseDate";
            this.uDateAriseDate.Size = new System.Drawing.Size(100, 21);
            this.uDateAriseDate.TabIndex = 151;
            // 
            // uLabelAriseDate
            // 
            this.uLabelAriseDate.Location = new System.Drawing.Point(320, 28);
            this.uLabelAriseDate.Name = "uLabelAriseDate";
            this.uLabelAriseDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelAriseDate.TabIndex = 150;
            this.uLabelAriseDate.Text = "ultraLabel1";
            // 
            // frmQATZ0013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridInprocAccidentList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0013";
            this.Load += new System.EventHandler(this.frmQATZ0013_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0013_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0013_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcessOperationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInprocAccidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCA)).EndInit();
            this.uGroupBoxCA.ResumeLayout(false);
            this.uGroupBoxCA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCAAttachFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCAInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxReason_Action)).EndInit();
            this.uGroupBoxReason_Action.ResumeLayout(false);
            this.uGroupBoxReason_Action.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialTreat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateActionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReasonDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnviromentFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethodFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachineFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckManFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextActionDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReasonDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetailList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAriseInfo)).EndInit();
            this.uGroupBoxAriseInfo.ResumeLayout(false);
            this.uGroupBoxAriseInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMgnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManagementNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateIssueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcessOperationType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcessOperationType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectFaultTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectFaultType;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInprocAccidentList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxAriseInfo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectFaultTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectFaultType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextManagementNo;
        private Infragistics.Win.Misc.UltraLabel uLabelManagementNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateIssueDate;
        private Infragistics.Win.Misc.UltraLabel uLabelIssueDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAriseProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseProcess;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetailList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxReason_Action;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReasonDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelReasonDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextActionDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelActionDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFilePath;
        private Infragistics.Win.Misc.UltraLabel uLabelFilePath;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMaterialFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEnviromentFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMethodFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMachineFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckManFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraLabel uLabel4ME;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateActionDate;
        private Infragistics.Win.Misc.UltraLabel uLabelActionDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReasonDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReasonDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAriseEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMgnNo;
        private Infragistics.Win.Misc.UltraLabel uLabelMgnNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialTreat;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialTreat;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxCA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegistUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRegistUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCAInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCAInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegistUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelCAAttachFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCAAttachFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectResult;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectResult;
    }
}