﻿namespace QRPQAT.UI
{
    partial class frmQATZ0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0005));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uOptionSearchCheck = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uDateToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchState = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchManageNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridOutCorrectionList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uDateNextVerity = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uOptionCheck = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRequestDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckRequestFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelRequestFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRequestDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRequestUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRequestUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextCompleteDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCompleteDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCompleteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCompleteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCompleteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCompleteUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextVerityDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVerityDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckVerityFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelVerityFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateVerityDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelVerityDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVerityUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVerityUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVerityUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFile = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFileUpload = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextReceiptDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReceiptDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckReceiptFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelReceiptFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReceiptUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReceiptUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReceiptUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uDateEndVerityDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboVerityType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelVerityType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelState = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNextVerityDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLatelyExpireDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLatelyCorrectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStorageDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUseDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulabelUseDept = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGDDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLatelyVerityDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextModelName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelModelName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakerCompany = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMakerCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVerityNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOutVerityNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelManageNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridOutCorrectionList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateNextVerity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRequestFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).BeginInit();
            this.uGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVerityFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateVerityDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileUpload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceiptFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateEndVerityDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLatelyVerityDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModelName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOutVerityNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSearchCheck);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchState);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchState);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchManageNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSearchMeasureToolCode
            // 
            appearance16.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance16.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance16;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMeasureToolCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchMeasureToolCode.Location = new System.Drawing.Point(416, 8);
            this.uTextSearchMeasureToolCode.MaxLength = 20;
            this.uTextSearchMeasureToolCode.Name = "uTextSearchMeasureToolCode";
            this.uTextSearchMeasureToolCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMeasureToolCode.TabIndex = 6;
            this.uTextSearchMeasureToolCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMeasureToolCode_KeyDown);
            this.uTextSearchMeasureToolCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMeasureToolCode_EditorButtonClick);
            // 
            // uOptionSearchCheck
            // 
            this.uOptionSearchCheck.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSearchCheck.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "M";
            valueListItem1.DisplayText = "설비";
            valueListItem2.DataValue = "E";
            valueListItem2.DisplayText = "계측기";
            this.uOptionSearchCheck.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionSearchCheck.Location = new System.Drawing.Point(280, 12);
            this.uOptionSearchCheck.Name = "uOptionSearchCheck";
            this.uOptionSearchCheck.Size = new System.Drawing.Size(132, 16);
            this.uOptionSearchCheck.TabIndex = 156;
            this.uOptionSearchCheck.TextIndentation = 2;
            this.uOptionSearchCheck.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateToDate
            // 
            this.uDateToDate.Location = new System.Drawing.Point(528, 32);
            this.uDateToDate.Name = "uDateToDate";
            this.uDateToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToDate.TabIndex = 12;
            // 
            // uDateFromDate
            // 
            this.uDateFromDate.Location = new System.Drawing.Point(416, 32);
            this.uDateFromDate.Name = "uDateFromDate";
            this.uDateFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDate.TabIndex = 12;
            // 
            // uComboSearchState
            // 
            this.uComboSearchState.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchState.MaxLength = 6;
            this.uComboSearchState.Name = "uComboSearchState";
            this.uComboSearchState.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchState.TabIndex = 11;
            this.uComboSearchState.Text = "ultraComboEditor1";
            // 
            // uLabelSearchState
            // 
            this.uLabelSearchState.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchState.Name = "uLabelSearchState";
            this.uLabelSearchState.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchState.TabIndex = 10;
            this.uLabelSearchState.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(516, 36);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel1.TabIndex = 8;
            this.ultraLabel1.Text = "~";
            // 
            // uLabelSearchManageNo
            // 
            this.uLabelSearchManageNo.Location = new System.Drawing.Point(280, 32);
            this.uLabelSearchManageNo.Name = "uLabelSearchManageNo";
            this.uLabelSearchManageNo.Size = new System.Drawing.Size(132, 20);
            this.uLabelSearchManageNo.TabIndex = 8;
            this.uLabelSearchManageNo.Text = "ultraLabel1";
            // 
            // uTextSearchMeasureToolName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Appearance = appearance15;
            this.uTextSearchMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Location = new System.Drawing.Point(518, 8);
            this.uTextSearchMeasureToolName.Name = "uTextSearchMeasureToolName";
            this.uTextSearchMeasureToolName.ReadOnly = true;
            this.uTextSearchMeasureToolName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMeasureToolName.TabIndex = 7;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridOutCorrectionList
            // 
            this.uGridOutCorrectionList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridOutCorrectionList.DisplayLayout.Appearance = appearance5;
            this.uGridOutCorrectionList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridOutCorrectionList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridOutCorrectionList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridOutCorrectionList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridOutCorrectionList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridOutCorrectionList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridOutCorrectionList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridOutCorrectionList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridOutCorrectionList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridOutCorrectionList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridOutCorrectionList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridOutCorrectionList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridOutCorrectionList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridOutCorrectionList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridOutCorrectionList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridOutCorrectionList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridOutCorrectionList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridOutCorrectionList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridOutCorrectionList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridOutCorrectionList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridOutCorrectionList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridOutCorrectionList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridOutCorrectionList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridOutCorrectionList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridOutCorrectionList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridOutCorrectionList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridOutCorrectionList.Location = new System.Drawing.Point(0, 100);
            this.uGridOutCorrectionList.Name = "uGridOutCorrectionList";
            this.uGridOutCorrectionList.Size = new System.Drawing.Size(1070, 736);
            this.uGridOutCorrectionList.TabIndex = 2;
            this.uGridOutCorrectionList.Text = "ultraGrid1";
            this.uGridOutCorrectionList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridOutCorrectionList_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateNextVerity);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uOptionCheck);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateEndVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboVerityType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelNextVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLatelyExpireDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLatelyCorrectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStorageDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUseDept);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ulabelUseDept);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGDDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLatelyVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModelName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModelName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakerCompany);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMakerCompany);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVerityNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextOutVerityNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelManageNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uDateNextVerity
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateNextVerity.Appearance = appearance22;
            this.uDateNextVerity.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateNextVerity.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateNextVerity.Location = new System.Drawing.Point(784, 84);
            this.uDateNextVerity.Name = "uDateNextVerity";
            this.uDateNextVerity.Size = new System.Drawing.Size(100, 21);
            this.uDateNextVerity.TabIndex = 194;
            // 
            // uTextMeasureToolCode
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolCode.Appearance = appearance39;
            this.uTextMeasureToolCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance40.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance40.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance40;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMeasureToolCode.ButtonsRight.Add(editorButton2);
            this.uTextMeasureToolCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMeasureToolCode.Location = new System.Drawing.Point(440, 12);
            this.uTextMeasureToolCode.MaxLength = 20;
            this.uTextMeasureToolCode.Name = "uTextMeasureToolCode";
            this.uTextMeasureToolCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMeasureToolCode.TabIndex = 13;
            this.uTextMeasureToolCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextMeasureToolCode_KeyDown);
            this.uTextMeasureToolCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMeasureToolCode_EditorButtonClick);
            // 
            // uOptionCheck
            // 
            appearance30.BackColor = System.Drawing.Color.White;
            this.uOptionCheck.Appearance = appearance30;
            this.uOptionCheck.BackColor = System.Drawing.Color.White;
            this.uOptionCheck.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionCheck.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem3.DataValue = "M";
            valueListItem3.DisplayText = "설비";
            valueListItem4.DataValue = "E";
            valueListItem4.DisplayText = "계측기";
            this.uOptionCheck.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptionCheck.Location = new System.Drawing.Point(312, 16);
            this.uOptionCheck.Name = "uOptionCheck";
            this.uOptionCheck.Size = new System.Drawing.Size(124, 16);
            this.uOptionCheck.TabIndex = 190;
            this.uOptionCheck.TextIndentation = 2;
            this.uOptionCheck.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uTextRequestDesc);
            this.uGroupBox2.Controls.Add(this.uLabelRequestDesc);
            this.uGroupBox2.Controls.Add(this.uCheckRequestFlag);
            this.uGroupBox2.Controls.Add(this.uLabelRequestFlag);
            this.uGroupBox2.Controls.Add(this.uDateRequestDate);
            this.uGroupBox2.Controls.Add(this.uLabelRequestDate);
            this.uGroupBox2.Controls.Add(this.uTextRequestUserName);
            this.uGroupBox2.Controls.Add(this.uTextRequestUserID);
            this.uGroupBox2.Controls.Add(this.uLabelRequestUserID);
            this.uGroupBox2.Location = new System.Drawing.Point(0, 144);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(940, 96);
            this.uGroupBox2.TabIndex = 193;
            // 
            // uTextRequestDesc
            // 
            this.uTextRequestDesc.Location = new System.Drawing.Point(120, 60);
            this.uTextRequestDesc.MaxLength = 200;
            this.uTextRequestDesc.Name = "uTextRequestDesc";
            this.uTextRequestDesc.Size = new System.Drawing.Size(424, 21);
            this.uTextRequestDesc.TabIndex = 189;
            // 
            // uLabelRequestDesc
            // 
            this.uLabelRequestDesc.Location = new System.Drawing.Point(16, 60);
            this.uLabelRequestDesc.Name = "uLabelRequestDesc";
            this.uLabelRequestDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestDesc.TabIndex = 188;
            this.uLabelRequestDesc.Text = "ultraLabel1";
            // 
            // uCheckRequestFlag
            // 
            this.uCheckRequestFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckRequestFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRequestFlag.Location = new System.Drawing.Point(768, 36);
            this.uCheckRequestFlag.Name = "uCheckRequestFlag";
            this.uCheckRequestFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckRequestFlag.TabIndex = 187;
            this.uCheckRequestFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelRequestFlag
            // 
            this.uLabelRequestFlag.Location = new System.Drawing.Point(660, 36);
            this.uLabelRequestFlag.Name = "uLabelRequestFlag";
            this.uLabelRequestFlag.Size = new System.Drawing.Size(104, 20);
            this.uLabelRequestFlag.TabIndex = 186;
            this.uLabelRequestFlag.Text = "ultraLabel1";
            // 
            // uDateRequestDate
            // 
            this.uDateRequestDate.Location = new System.Drawing.Point(444, 36);
            this.uDateRequestDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateRequestDate.Name = "uDateRequestDate";
            this.uDateRequestDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRequestDate.TabIndex = 35;
            // 
            // uLabelRequestDate
            // 
            this.uLabelRequestDate.Location = new System.Drawing.Point(340, 36);
            this.uLabelRequestDate.Name = "uLabelRequestDate";
            this.uLabelRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestDate.TabIndex = 34;
            this.uLabelRequestDate.Text = "ultraLabel2";
            // 
            // uTextRequestUserName
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRequestUserName.Appearance = appearance26;
            this.uTextRequestUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRequestUserName.Location = new System.Drawing.Point(222, 36);
            this.uTextRequestUserName.Name = "uTextRequestUserName";
            this.uTextRequestUserName.ReadOnly = true;
            this.uTextRequestUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRequestUserName.TabIndex = 17;
            // 
            // uTextRequestUserID
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRequestUserID.Appearance = appearance27;
            this.uTextRequestUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance28.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance28;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRequestUserID.ButtonsRight.Add(editorButton3);
            this.uTextRequestUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRequestUserID.Location = new System.Drawing.Point(120, 36);
            this.uTextRequestUserID.MaxLength = 20;
            this.uTextRequestUserID.Name = "uTextRequestUserID";
            this.uTextRequestUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRequestUserID.TabIndex = 16;
            this.uTextRequestUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRequestUserID_KeyDown);
            this.uTextRequestUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRequestUserID_EditorButtonClick);
            // 
            // uLabelRequestUserID
            // 
            this.uLabelRequestUserID.Location = new System.Drawing.Point(16, 36);
            this.uLabelRequestUserID.Name = "uLabelRequestUserID";
            this.uLabelRequestUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestUserID.TabIndex = 15;
            this.uLabelRequestUserID.Text = "ultraLabel1";
            // 
            // uGroupBox5
            // 
            this.uGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox5.Controls.Add(this.uTextCompleteDesc);
            this.uGroupBox5.Controls.Add(this.uLabelCompleteDesc);
            this.uGroupBox5.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox5.Controls.Add(this.uLabelCompleteFlag);
            this.uGroupBox5.Controls.Add(this.uDateCompleteDate);
            this.uGroupBox5.Controls.Add(this.uLabelCompleteDate);
            this.uGroupBox5.Controls.Add(this.uTextCompleteUserName);
            this.uGroupBox5.Controls.Add(this.uTextCompleteUserID);
            this.uGroupBox5.Controls.Add(this.uLabelCompleteUserID);
            this.uGroupBox5.Location = new System.Drawing.Point(0, 456);
            this.uGroupBox5.Name = "uGroupBox5";
            this.uGroupBox5.Size = new System.Drawing.Size(940, 96);
            this.uGroupBox5.TabIndex = 192;
            // 
            // uTextCompleteDesc
            // 
            this.uTextCompleteDesc.Location = new System.Drawing.Point(120, 60);
            this.uTextCompleteDesc.MaxLength = 200;
            this.uTextCompleteDesc.Name = "uTextCompleteDesc";
            this.uTextCompleteDesc.Size = new System.Drawing.Size(424, 21);
            this.uTextCompleteDesc.TabIndex = 189;
            // 
            // uLabelCompleteDesc
            // 
            this.uLabelCompleteDesc.Location = new System.Drawing.Point(16, 60);
            this.uLabelCompleteDesc.Name = "uLabelCompleteDesc";
            this.uLabelCompleteDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteDesc.TabIndex = 188;
            this.uLabelCompleteDesc.Text = "ultraLabel1";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(768, 36);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckCompleteFlag.TabIndex = 187;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(660, 36);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(104, 20);
            this.uLabelCompleteFlag.TabIndex = 186;
            this.uLabelCompleteFlag.Text = "ultraLabel1";
            // 
            // uDateCompleteDate
            // 
            this.uDateCompleteDate.Location = new System.Drawing.Point(444, 36);
            this.uDateCompleteDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateCompleteDate.Name = "uDateCompleteDate";
            this.uDateCompleteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCompleteDate.TabIndex = 35;
            // 
            // uLabelCompleteDate
            // 
            this.uLabelCompleteDate.Location = new System.Drawing.Point(340, 36);
            this.uLabelCompleteDate.Name = "uLabelCompleteDate";
            this.uLabelCompleteDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteDate.TabIndex = 34;
            this.uLabelCompleteDate.Text = "ultraLabel2";
            // 
            // uTextCompleteUserName
            // 
            appearance76.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteUserName.Appearance = appearance76;
            this.uTextCompleteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteUserName.Location = new System.Drawing.Point(222, 36);
            this.uTextCompleteUserName.Name = "uTextCompleteUserName";
            this.uTextCompleteUserName.ReadOnly = true;
            this.uTextCompleteUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCompleteUserName.TabIndex = 17;
            // 
            // uTextCompleteUserID
            // 
            appearance36.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCompleteUserID.Appearance = appearance36;
            this.uTextCompleteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance37.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance37;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCompleteUserID.ButtonsRight.Add(editorButton4);
            this.uTextCompleteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCompleteUserID.Location = new System.Drawing.Point(120, 36);
            this.uTextCompleteUserID.MaxLength = 20;
            this.uTextCompleteUserID.Name = "uTextCompleteUserID";
            this.uTextCompleteUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCompleteUserID.TabIndex = 16;
            this.uTextCompleteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCompleteUserID_KeyDown);
            this.uTextCompleteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uLabelCompleteUserID
            // 
            this.uLabelCompleteUserID.Location = new System.Drawing.Point(16, 36);
            this.uLabelCompleteUserID.Name = "uLabelCompleteUserID";
            this.uLabelCompleteUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteUserID.TabIndex = 15;
            this.uLabelCompleteUserID.Text = "ultraLabel1";
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox4.Controls.Add(this.uTextVerityDesc);
            this.uGroupBox4.Controls.Add(this.uLabelVerityDesc);
            this.uGroupBox4.Controls.Add(this.uCheckVerityFlag);
            this.uGroupBox4.Controls.Add(this.uLabelVerityFlag);
            this.uGroupBox4.Controls.Add(this.uDateVerityDate);
            this.uGroupBox4.Controls.Add(this.uLabelVerityDate);
            this.uGroupBox4.Controls.Add(this.uTextVerityUserName);
            this.uGroupBox4.Controls.Add(this.uTextVerityUserID);
            this.uGroupBox4.Controls.Add(this.uLabelVerityUserID);
            this.uGroupBox4.Controls.Add(this.uLabelFile);
            this.uGroupBox4.Controls.Add(this.uTextFileUpload);
            this.uGroupBox4.Location = new System.Drawing.Point(0, 336);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(940, 116);
            this.uGroupBox4.TabIndex = 191;
            // 
            // uTextVerityDesc
            // 
            this.uTextVerityDesc.Location = new System.Drawing.Point(120, 60);
            this.uTextVerityDesc.MaxLength = 200;
            this.uTextVerityDesc.Name = "uTextVerityDesc";
            this.uTextVerityDesc.Size = new System.Drawing.Size(424, 21);
            this.uTextVerityDesc.TabIndex = 189;
            // 
            // uLabelVerityDesc
            // 
            this.uLabelVerityDesc.Location = new System.Drawing.Point(16, 60);
            this.uLabelVerityDesc.Name = "uLabelVerityDesc";
            this.uLabelVerityDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelVerityDesc.TabIndex = 188;
            this.uLabelVerityDesc.Text = "ultraLabel1";
            // 
            // uCheckVerityFlag
            // 
            this.uCheckVerityFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckVerityFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckVerityFlag.Location = new System.Drawing.Point(768, 36);
            this.uCheckVerityFlag.Name = "uCheckVerityFlag";
            this.uCheckVerityFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckVerityFlag.TabIndex = 187;
            this.uCheckVerityFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelVerityFlag
            // 
            this.uLabelVerityFlag.Location = new System.Drawing.Point(660, 36);
            this.uLabelVerityFlag.Name = "uLabelVerityFlag";
            this.uLabelVerityFlag.Size = new System.Drawing.Size(104, 20);
            this.uLabelVerityFlag.TabIndex = 186;
            this.uLabelVerityFlag.Text = "ultraLabel1";
            // 
            // uDateVerityDate
            // 
            this.uDateVerityDate.Location = new System.Drawing.Point(444, 36);
            this.uDateVerityDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateVerityDate.Name = "uDateVerityDate";
            this.uDateVerityDate.Size = new System.Drawing.Size(100, 21);
            this.uDateVerityDate.TabIndex = 35;
            // 
            // uLabelVerityDate
            // 
            this.uLabelVerityDate.Location = new System.Drawing.Point(340, 36);
            this.uLabelVerityDate.Name = "uLabelVerityDate";
            this.uLabelVerityDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelVerityDate.TabIndex = 34;
            this.uLabelVerityDate.Text = "ultraLabel2";
            // 
            // uTextVerityUserName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityUserName.Appearance = appearance20;
            this.uTextVerityUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityUserName.Location = new System.Drawing.Point(812, 20);
            this.uTextVerityUserName.Name = "uTextVerityUserName";
            this.uTextVerityUserName.ReadOnly = true;
            this.uTextVerityUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextVerityUserName.TabIndex = 17;
            this.uTextVerityUserName.Visible = false;
            // 
            // uTextVerityUserID
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVerityUserID.Appearance = appearance21;
            this.uTextVerityUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVerityUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVerityUserID.Location = new System.Drawing.Point(120, 36);
            this.uTextVerityUserID.MaxLength = 20;
            this.uTextVerityUserID.Name = "uTextVerityUserID";
            this.uTextVerityUserID.Size = new System.Drawing.Size(200, 21);
            this.uTextVerityUserID.TabIndex = 16;
            this.uTextVerityUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextVerityUserID_KeyDown);
            // 
            // uLabelVerityUserID
            // 
            this.uLabelVerityUserID.Location = new System.Drawing.Point(16, 36);
            this.uLabelVerityUserID.Name = "uLabelVerityUserID";
            this.uLabelVerityUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelVerityUserID.TabIndex = 15;
            this.uLabelVerityUserID.Text = "ultraLabel1";
            // 
            // uLabelFile
            // 
            this.uLabelFile.Location = new System.Drawing.Point(16, 84);
            this.uLabelFile.Name = "uLabelFile";
            this.uLabelFile.Size = new System.Drawing.Size(100, 20);
            this.uLabelFile.TabIndex = 23;
            this.uLabelFile.Text = "ultraLabel1";
            // 
            // uTextFileUpload
            // 
            appearance77.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance77.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance77;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "UP";
            appearance29.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance29.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance29;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Key = "Down";
            this.uTextFileUpload.ButtonsRight.Add(editorButton5);
            this.uTextFileUpload.ButtonsRight.Add(editorButton6);
            this.uTextFileUpload.Location = new System.Drawing.Point(120, 84);
            this.uTextFileUpload.MaxLength = 1000;
            this.uTextFileUpload.Name = "uTextFileUpload";
            this.uTextFileUpload.Size = new System.Drawing.Size(424, 21);
            this.uTextFileUpload.TabIndex = 13;
            this.uTextFileUpload.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFileUpload_EditorButtonClick);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uTextReceiptDesc);
            this.uGroupBox3.Controls.Add(this.uLabelReceiptDesc);
            this.uGroupBox3.Controls.Add(this.uCheckReceiptFlag);
            this.uGroupBox3.Controls.Add(this.uLabelReceiptFlag);
            this.uGroupBox3.Controls.Add(this.uDateReceiptDate);
            this.uGroupBox3.Controls.Add(this.uLabelReceiptDate);
            this.uGroupBox3.Controls.Add(this.uTextReceiptUserName);
            this.uGroupBox3.Controls.Add(this.uTextReceiptUserID);
            this.uGroupBox3.Controls.Add(this.uLabelReceiptUserID);
            this.uGroupBox3.Location = new System.Drawing.Point(0, 240);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(940, 96);
            this.uGroupBox3.TabIndex = 190;
            // 
            // uTextReceiptDesc
            // 
            this.uTextReceiptDesc.Location = new System.Drawing.Point(120, 60);
            this.uTextReceiptDesc.MaxLength = 200;
            this.uTextReceiptDesc.Name = "uTextReceiptDesc";
            this.uTextReceiptDesc.Size = new System.Drawing.Size(424, 21);
            this.uTextReceiptDesc.TabIndex = 189;
            // 
            // uLabelReceiptDesc
            // 
            this.uLabelReceiptDesc.Location = new System.Drawing.Point(16, 60);
            this.uLabelReceiptDesc.Name = "uLabelReceiptDesc";
            this.uLabelReceiptDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelReceiptDesc.TabIndex = 188;
            this.uLabelReceiptDesc.Text = "ultraLabel1";
            // 
            // uCheckReceiptFlag
            // 
            this.uCheckReceiptFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckReceiptFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReceiptFlag.Location = new System.Drawing.Point(768, 36);
            this.uCheckReceiptFlag.Name = "uCheckReceiptFlag";
            this.uCheckReceiptFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckReceiptFlag.TabIndex = 187;
            this.uCheckReceiptFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelReceiptFlag
            // 
            this.uLabelReceiptFlag.Location = new System.Drawing.Point(660, 36);
            this.uLabelReceiptFlag.Name = "uLabelReceiptFlag";
            this.uLabelReceiptFlag.Size = new System.Drawing.Size(104, 20);
            this.uLabelReceiptFlag.TabIndex = 186;
            this.uLabelReceiptFlag.Text = "ultraLabel1";
            // 
            // uDateReceiptDate
            // 
            this.uDateReceiptDate.Location = new System.Drawing.Point(444, 36);
            this.uDateReceiptDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateReceiptDate.Name = "uDateReceiptDate";
            this.uDateReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReceiptDate.TabIndex = 35;
            // 
            // uLabelReceiptDate
            // 
            this.uLabelReceiptDate.Location = new System.Drawing.Point(340, 36);
            this.uLabelReceiptDate.Name = "uLabelReceiptDate";
            this.uLabelReceiptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReceiptDate.TabIndex = 34;
            this.uLabelReceiptDate.Text = "ultraLabel2";
            // 
            // uTextReceiptUserName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptUserName.Appearance = appearance23;
            this.uTextReceiptUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptUserName.Location = new System.Drawing.Point(222, 36);
            this.uTextReceiptUserName.Name = "uTextReceiptUserName";
            this.uTextReceiptUserName.ReadOnly = true;
            this.uTextReceiptUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptUserName.TabIndex = 17;
            // 
            // uTextReceiptUserID
            // 
            appearance24.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReceiptUserID.Appearance = appearance24;
            this.uTextReceiptUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance25.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance25.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance25;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReceiptUserID.ButtonsRight.Add(editorButton7);
            this.uTextReceiptUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReceiptUserID.Location = new System.Drawing.Point(120, 36);
            this.uTextReceiptUserID.MaxLength = 20;
            this.uTextReceiptUserID.Name = "uTextReceiptUserID";
            this.uTextReceiptUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptUserID.TabIndex = 16;
            this.uTextReceiptUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReceiptUserID_KeyDown);
            this.uTextReceiptUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uLabelReceiptUserID
            // 
            this.uLabelReceiptUserID.Location = new System.Drawing.Point(16, 36);
            this.uLabelReceiptUserID.Name = "uLabelReceiptUserID";
            this.uLabelReceiptUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelReceiptUserID.TabIndex = 15;
            this.uLabelReceiptUserID.Text = "ultraLabel1";
            // 
            // uDateEndVerityDate
            // 
            this.uDateEndVerityDate.Location = new System.Drawing.Point(784, 60);
            this.uDateEndVerityDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateEndVerityDate.Name = "uDateEndVerityDate";
            this.uDateEndVerityDate.Size = new System.Drawing.Size(100, 21);
            this.uDateEndVerityDate.TabIndex = 33;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 108);
            this.uTextEtcDesc.MaxLength = 200;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(424, 21);
            this.uTextEtcDesc.TabIndex = 32;
            // 
            // uComboVerityType
            // 
            this.uComboVerityType.Location = new System.Drawing.Point(952, 128);
            this.uComboVerityType.MaxLength = 6;
            this.uComboVerityType.Name = "uComboVerityType";
            this.uComboVerityType.Size = new System.Drawing.Size(100, 21);
            this.uComboVerityType.TabIndex = 30;
            this.uComboVerityType.Text = "ultraComboEditor1";
            this.uComboVerityType.Visible = false;
            // 
            // uComboState
            // 
            this.uComboState.Location = new System.Drawing.Point(784, 36);
            this.uComboState.MaxLength = 6;
            this.uComboState.Name = "uComboState";
            this.uComboState.Size = new System.Drawing.Size(100, 21);
            this.uComboState.TabIndex = 30;
            this.uComboState.Text = "ultraComboEditor1";
            // 
            // uLabelVerityType
            // 
            this.uLabelVerityType.Location = new System.Drawing.Point(952, 104);
            this.uLabelVerityType.Name = "uLabelVerityType";
            this.uLabelVerityType.Size = new System.Drawing.Size(100, 20);
            this.uLabelVerityType.TabIndex = 29;
            this.uLabelVerityType.Text = "ultraLabel1";
            this.uLabelVerityType.Visible = false;
            // 
            // uLabelState
            // 
            this.uLabelState.Location = new System.Drawing.Point(660, 36);
            this.uLabelState.Name = "uLabelState";
            this.uLabelState.Size = new System.Drawing.Size(120, 20);
            this.uLabelState.TabIndex = 29;
            this.uLabelState.Text = "ultraLabel1";
            // 
            // uLabelNextVerityDate
            // 
            this.uLabelNextVerityDate.Location = new System.Drawing.Point(660, 84);
            this.uLabelNextVerityDate.Name = "uLabelNextVerityDate";
            this.uLabelNextVerityDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelNextVerityDate.TabIndex = 27;
            this.uLabelNextVerityDate.Text = "ultraLabel2";
            // 
            // uLabelLatelyExpireDate
            // 
            this.uLabelLatelyExpireDate.Location = new System.Drawing.Point(660, 60);
            this.uLabelLatelyExpireDate.Name = "uLabelLatelyExpireDate";
            this.uLabelLatelyExpireDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelLatelyExpireDate.TabIndex = 27;
            this.uLabelLatelyExpireDate.Text = "ultraLabel2";
            // 
            // uLabelLatelyCorrectDate
            // 
            this.uLabelLatelyCorrectDate.Location = new System.Drawing.Point(312, 84);
            this.uLabelLatelyCorrectDate.Name = "uLabelLatelyCorrectDate";
            this.uLabelLatelyCorrectDate.Size = new System.Drawing.Size(124, 20);
            this.uLabelLatelyCorrectDate.TabIndex = 25;
            this.uLabelLatelyCorrectDate.Text = "ultraLabel1";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 108);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 23;
            this.uLabelEtcDesc.Text = "ultraLabel1";
            // 
            // uLabelStorageDate
            // 
            this.uLabelStorageDate.Location = new System.Drawing.Point(12, 84);
            this.uLabelStorageDate.Name = "uLabelStorageDate";
            this.uLabelStorageDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStorageDate.TabIndex = 23;
            this.uLabelStorageDate.Text = "ultraLabel1";
            // 
            // uTextUseDept
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseDept.Appearance = appearance18;
            this.uTextUseDept.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseDept.Location = new System.Drawing.Point(784, 12);
            this.uTextUseDept.Name = "uTextUseDept";
            this.uTextUseDept.ReadOnly = true;
            this.uTextUseDept.Size = new System.Drawing.Size(150, 21);
            this.uTextUseDept.TabIndex = 22;
            // 
            // ulabelUseDept
            // 
            this.ulabelUseDept.Location = new System.Drawing.Point(660, 12);
            this.ulabelUseDept.Name = "ulabelUseDept";
            this.ulabelUseDept.Size = new System.Drawing.Size(120, 20);
            this.ulabelUseDept.TabIndex = 21;
            this.ulabelUseDept.Text = "ultraLabel1";
            // 
            // uTextGDDate
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGDDate.Appearance = appearance32;
            this.uTextGDDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGDDate.Location = new System.Drawing.Point(116, 84);
            this.uTextGDDate.Name = "uTextGDDate";
            this.uTextGDDate.ReadOnly = true;
            this.uTextGDDate.Size = new System.Drawing.Size(100, 21);
            this.uTextGDDate.TabIndex = 20;
            // 
            // uTextLatelyVerityDate
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLatelyVerityDate.Appearance = appearance33;
            this.uTextLatelyVerityDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLatelyVerityDate.Location = new System.Drawing.Point(440, 84);
            this.uTextLatelyVerityDate.Name = "uTextLatelyVerityDate";
            this.uTextLatelyVerityDate.ReadOnly = true;
            this.uTextLatelyVerityDate.Size = new System.Drawing.Size(100, 21);
            this.uTextLatelyVerityDate.TabIndex = 20;
            // 
            // uTextSerialNo
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance34;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(440, 60);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSerialNo.TabIndex = 20;
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(312, 60);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(124, 20);
            this.uLabelSerialNo.TabIndex = 19;
            this.uLabelSerialNo.Text = "ultraLabel1";
            // 
            // uTextModelName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModelName.Appearance = appearance19;
            this.uTextModelName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModelName.Location = new System.Drawing.Point(116, 60);
            this.uTextModelName.Name = "uTextModelName";
            this.uTextModelName.ReadOnly = true;
            this.uTextModelName.Size = new System.Drawing.Size(150, 21);
            this.uTextModelName.TabIndex = 18;
            // 
            // uLabelModelName
            // 
            this.uLabelModelName.Location = new System.Drawing.Point(12, 60);
            this.uLabelModelName.Name = "uLabelModelName";
            this.uLabelModelName.Size = new System.Drawing.Size(100, 20);
            this.uLabelModelName.TabIndex = 17;
            this.uLabelModelName.Text = "ultraLabel1";
            // 
            // uTextMakerCompany
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerCompany.Appearance = appearance35;
            this.uTextMakerCompany.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerCompany.Location = new System.Drawing.Point(440, 36);
            this.uTextMakerCompany.Name = "uTextMakerCompany";
            this.uTextMakerCompany.ReadOnly = true;
            this.uTextMakerCompany.Size = new System.Drawing.Size(150, 21);
            this.uTextMakerCompany.TabIndex = 16;
            // 
            // uLabelMakerCompany
            // 
            this.uLabelMakerCompany.Location = new System.Drawing.Point(312, 36);
            this.uLabelMakerCompany.Name = "uLabelMakerCompany";
            this.uLabelMakerCompany.Size = new System.Drawing.Size(124, 20);
            this.uLabelMakerCompany.TabIndex = 15;
            this.uLabelMakerCompany.Text = "ultraLabel1";
            // 
            // uTextMeasureToolName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Appearance = appearance38;
            this.uTextMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Location = new System.Drawing.Point(542, 12);
            this.uTextMeasureToolName.Name = "uTextMeasureToolName";
            this.uTextMeasureToolName.ReadOnly = true;
            this.uTextMeasureToolName.Size = new System.Drawing.Size(100, 21);
            this.uTextMeasureToolName.TabIndex = 14;
            // 
            // uTextVerityNumber
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityNumber.Appearance = appearance14;
            this.uTextVerityNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityNumber.Location = new System.Drawing.Point(212, 36);
            this.uTextVerityNumber.Name = "uTextVerityNumber";
            this.uTextVerityNumber.ReadOnly = true;
            this.uTextVerityNumber.Size = new System.Drawing.Size(80, 21);
            this.uTextVerityNumber.TabIndex = 11;
            // 
            // uTextOutVerityNo
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOutVerityNo.Appearance = appearance17;
            this.uTextOutVerityNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextOutVerityNo.Location = new System.Drawing.Point(116, 36);
            this.uTextOutVerityNo.Name = "uTextOutVerityNo";
            this.uTextOutVerityNo.ReadOnly = true;
            this.uTextOutVerityNo.Size = new System.Drawing.Size(92, 21);
            this.uTextOutVerityNo.TabIndex = 11;
            // 
            // uLabelManageNo
            // 
            this.uLabelManageNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelManageNo.Name = "uLabelManageNo";
            this.uLabelManageNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelManageNo.TabIndex = 10;
            this.uLabelManageNo.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboPlant.TabIndex = 3;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQATZ0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridOutCorrectionList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0005";
            this.Load += new System.EventHandler(this.frmQATZ0005_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0005_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0005_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridOutCorrectionList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateNextVerity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRequestFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).EndInit();
            this.uGroupBox5.ResumeLayout(false);
            this.uGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            this.uGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVerityFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateVerityDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileUpload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceiptFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateEndVerityDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLatelyVerityDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModelName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOutVerityNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchManageNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchState;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchState;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridOutCorrectionList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOutVerityNo;
        private Infragistics.Win.Misc.UltraLabel uLabelManageNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModelName;
        private Infragistics.Win.Misc.UltraLabel uLabelModelName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerCompany;
        private Infragistics.Win.Misc.UltraLabel uLabelMakerCompany;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboState;
        private Infragistics.Win.Misc.UltraLabel uLabelState;
        private Infragistics.Win.Misc.UltraLabel uLabelLatelyExpireDate;
        private Infragistics.Win.Misc.UltraLabel uLabelLatelyCorrectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelStorageDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseDept;
        private Infragistics.Win.Misc.UltraLabel ulabelUseDept;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLatelyVerityDate;
        private Infragistics.Win.Misc.UltraLabel uLabelFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGDDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileUpload;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboVerityType;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityNumber;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateEndVerityDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteDesc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCompleteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteUserID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityDesc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckVerityFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateVerityDate;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityUserID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptDesc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReceiptFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptUserID;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestDesc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRequestFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRequestDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestUserID;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionCheck;
        private Infragistics.Win.Misc.UltraLabel uLabelNextVerityDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateNextVerity;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSearchCheck;
    }
}