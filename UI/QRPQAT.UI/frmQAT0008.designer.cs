﻿namespace QRPQAT.UI
{
    partial class frmQAT0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQAT0008));
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uOptionSearchCheck = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextSearchMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchModelName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridInVerityH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxComplete = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelConfirmUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uTextConfirmUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextConfirmUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboVerityType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextHumidity = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextTemperature = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uOptionCheck = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextFileUpLoad = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFileName = new Infragistics.Win.Misc.UltraLabel();
            this.uComboVerifyResult = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVerityResult = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNextVerityDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateNextVerifyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextVerifyBasic = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVerityBasic = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHumidity = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTemperature = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCarryInDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVerityDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateVerityDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelVerityUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVerityUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCarryInDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVerityType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMade = new Infragistics.Win.Misc.UltraLabel();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInCorrectNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCodeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridInVerityD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextVerityNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInVerityNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelManageNo = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInVerityH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxComplete)).BeginInit();
            this.uGroupBoxComplete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileUpLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerifyResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateNextVerifyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerifyBasic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateVerityDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCodeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInVerityD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInVerityNo)).BeginInit();
            this.SuspendLayout();
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 12;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uOptionSearchCheck);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchModelName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(840, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel1.TabIndex = 157;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToDate
            // 
            this.uDateToDate.Location = new System.Drawing.Point(852, 12);
            this.uDateToDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateToDate.Name = "uDateToDate";
            this.uDateToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateToDate.TabIndex = 156;
            // 
            // uDateFromDate
            // 
            this.uDateFromDate.Location = new System.Drawing.Point(740, 12);
            this.uDateFromDate.MaskInput = "{LOC}yyyy/mm/dd";
            this.uDateFromDate.Name = "uDateFromDate";
            this.uDateFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDate.TabIndex = 156;
            // 
            // uOptionSearchCheck
            // 
            this.uOptionSearchCheck.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionSearchCheck.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "M";
            valueListItem1.DisplayText = "설비";
            valueListItem2.DataValue = "E";
            valueListItem2.DisplayText = "계측기";
            this.uOptionSearchCheck.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionSearchCheck.Location = new System.Drawing.Point(252, 16);
            this.uOptionSearchCheck.Name = "uOptionSearchCheck";
            this.uOptionSearchCheck.Size = new System.Drawing.Size(132, 20);
            this.uOptionSearchCheck.TabIndex = 155;
            this.uOptionSearchCheck.TextIndentation = 2;
            this.uOptionSearchCheck.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextSearchMeasureToolName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Appearance = appearance2;
            this.uTextSearchMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Location = new System.Drawing.Point(492, 12);
            this.uTextSearchMeasureToolName.Name = "uTextSearchMeasureToolName";
            this.uTextSearchMeasureToolName.ReadOnly = true;
            this.uTextSearchMeasureToolName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMeasureToolName.TabIndex = 14;
            // 
            // uTextSearchMeasureToolCode
            // 
            appearance4.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMeasureToolCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchMeasureToolCode.Location = new System.Drawing.Point(390, 12);
            this.uTextSearchMeasureToolCode.Name = "uTextSearchMeasureToolCode";
            this.uTextSearchMeasureToolCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMeasureToolCode.TabIndex = 13;
            this.uTextSearchMeasureToolCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMeasureToolCode_EditorButtonClick);
            // 
            // uLabelSearchModelName
            // 
            this.uLabelSearchModelName.Location = new System.Drawing.Point(608, 12);
            this.uLabelSearchModelName.Name = "uLabelSearchModelName";
            this.uLabelSearchModelName.Size = new System.Drawing.Size(128, 20);
            this.uLabelSearchModelName.TabIndex = 154;
            this.uLabelSearchModelName.Text = "차기";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uGridInVerityH
            // 
            this.uGridInVerityH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInVerityH.DisplayLayout.Appearance = appearance30;
            this.uGridInVerityH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInVerityH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityH.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInVerityH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGridInVerityH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInVerityH.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridInVerityH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInVerityH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInVerityH.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInVerityH.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridInVerityH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInVerityH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityH.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInVerityH.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridInVerityH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInVerityH.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityH.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridInVerityH.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridInVerityH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInVerityH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridInVerityH.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridInVerityH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInVerityH.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridInVerityH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInVerityH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInVerityH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInVerityH.Location = new System.Drawing.Point(0, 80);
            this.uGridInVerityH.Name = "uGridInVerityH";
            this.uGridInVerityH.Size = new System.Drawing.Size(1060, 690);
            this.uGridInVerityH.TabIndex = 5;
            this.uGridInVerityH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridInVerityH_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 712);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 712);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxComplete);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboVerityType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextHumidity);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTemperature);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uOptionCheck);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextFileUpLoad);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFileName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboVerifyResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelNextVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateNextVerifyDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVerifyBasic);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityBasic);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelHumidity);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTemperature);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDept);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCarryInDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateVerityDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVerityUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCarryInDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVerityType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMade);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInCorrectNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCodeName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVerityNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInVerityNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 692);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxComplete
            // 
            this.uGroupBoxComplete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxComplete.Controls.Add(this.uLabelConfirmDate);
            this.uGroupBoxComplete.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBoxComplete.Controls.Add(this.uLabelConfirmUserID);
            this.uGroupBoxComplete.Controls.Add(this.uTextConfirmUserName);
            this.uGroupBoxComplete.Controls.Add(this.uLabelCompleteFlag);
            this.uGroupBoxComplete.Controls.Add(this.uTextConfirmUserID);
            this.uGroupBoxComplete.Controls.Add(this.uDateConfirmDate);
            this.uGroupBoxComplete.Location = new System.Drawing.Point(12, 592);
            this.uGroupBoxComplete.Name = "uGroupBoxComplete";
            this.uGroupBoxComplete.Size = new System.Drawing.Size(1036, 60);
            this.uGroupBoxComplete.TabIndex = 98;
            // 
            // uLabelConfirmDate
            // 
            this.uLabelConfirmDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelConfirmDate.Name = "uLabelConfirmDate";
            this.uLabelConfirmDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelConfirmDate.TabIndex = 85;
            this.uLabelConfirmDate.Text = "승인일";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(780, 28);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckCompleteFlag.TabIndex = 97;
            // 
            // uLabelConfirmUserID
            // 
            this.uLabelConfirmUserID.Location = new System.Drawing.Point(300, 28);
            this.uLabelConfirmUserID.Name = "uLabelConfirmUserID";
            this.uLabelConfirmUserID.Size = new System.Drawing.Size(120, 20);
            this.uLabelConfirmUserID.TabIndex = 85;
            this.uLabelConfirmUserID.Text = "승인자";
            // 
            // uTextConfirmUserName
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConfirmUserName.Appearance = appearance39;
            this.uTextConfirmUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConfirmUserName.Location = new System.Drawing.Point(528, 28);
            this.uTextConfirmUserName.Name = "uTextConfirmUserName";
            this.uTextConfirmUserName.ReadOnly = true;
            this.uTextConfirmUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextConfirmUserName.TabIndex = 96;
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(656, 28);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(120, 20);
            this.uLabelCompleteFlag.TabIndex = 85;
            this.uLabelCompleteFlag.Text = "최종완료";
            // 
            // uTextConfirmUserID
            // 
            appearance47.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance47.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance47;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextConfirmUserID.ButtonsRight.Add(editorButton2);
            this.uTextConfirmUserID.Location = new System.Drawing.Point(424, 28);
            this.uTextConfirmUserID.Name = "uTextConfirmUserID";
            this.uTextConfirmUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextConfirmUserID.TabIndex = 96;
            this.uTextConfirmUserID.ValueChanged += new System.EventHandler(this.uTextConfirmUserID_ValueChanged);
            this.uTextConfirmUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextConfirmUserID_KeyDown);
            this.uTextConfirmUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextConfirmUserID_EditorButtonClick);
            // 
            // uDateConfirmDate
            // 
            this.uDateConfirmDate.Location = new System.Drawing.Point(136, 28);
            this.uDateConfirmDate.Name = "uDateConfirmDate";
            this.uDateConfirmDate.Size = new System.Drawing.Size(100, 21);
            this.uDateConfirmDate.TabIndex = 95;
            // 
            // uComboVerityType
            // 
            this.uComboVerityType.Location = new System.Drawing.Point(1016, 4);
            this.uComboVerityType.MaxLength = 5;
            this.uComboVerityType.Name = "uComboVerityType";
            this.uComboVerityType.Size = new System.Drawing.Size(32, 21);
            this.uComboVerityType.TabIndex = 94;
            this.uComboVerityType.Text = "ultraComboEditor1";
            this.uComboVerityType.Visible = false;
            // 
            // uTextHumidity
            // 
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Text = "0";
            this.uTextHumidity.ButtonsLeft.Add(editorButton3);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextHumidity.ButtonsRight.Add(spinEditorButton1);
            this.uTextHumidity.Location = new System.Drawing.Point(464, 108);
            this.uTextHumidity.MaxValue = 1000;
            this.uTextHumidity.MinValue = -1000;
            this.uTextHumidity.Name = "uTextHumidity";
            this.uTextHumidity.PromptChar = ' ';
            this.uTextHumidity.Size = new System.Drawing.Size(100, 21);
            this.uTextHumidity.TabIndex = 92;
            this.uTextHumidity.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uTextSpin_EditorSpinButtonClick);
            this.uTextHumidity.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInit_EditorButtonClick);
            // 
            // uTextTemperature
            // 
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Text = "0";
            this.uTextTemperature.ButtonsLeft.Add(editorButton4);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTemperature.ButtonsRight.Add(spinEditorButton2);
            this.uTextTemperature.Location = new System.Drawing.Point(136, 108);
            this.uTextTemperature.MaxValue = 1000;
            this.uTextTemperature.MinValue = -1000;
            this.uTextTemperature.Name = "uTextTemperature";
            this.uTextTemperature.PromptChar = ' ';
            this.uTextTemperature.Size = new System.Drawing.Size(100, 21);
            this.uTextTemperature.TabIndex = 92;
            this.uTextTemperature.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uTextSpin_EditorSpinButtonClick);
            this.uTextTemperature.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInit_EditorButtonClick);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(136, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboPlant.TabIndex = 91;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uOptionCheck
            // 
            this.uOptionCheck.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionCheck.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem3.DataValue = "M";
            valueListItem3.DisplayText = "설비";
            valueListItem4.DataValue = "E";
            valueListItem4.DisplayText = "계측기";
            this.uOptionCheck.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptionCheck.Location = new System.Drawing.Point(324, 16);
            this.uOptionCheck.Name = "uOptionCheck";
            this.uOptionCheck.Size = new System.Drawing.Size(136, 16);
            this.uOptionCheck.TabIndex = 90;
            this.uOptionCheck.TextIndentation = 2;
            this.uOptionCheck.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextFileUpLoad
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFileUpLoad.Appearance = appearance38;
            this.uTextFileUpLoad.BackColor = System.Drawing.Color.Gainsboro;
            appearance15.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance15.TextHAlignAsString = "Center";
            editorButton5.Appearance = appearance15;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "UP";
            appearance37.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance37;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Key = "DOWN";
            this.uTextFileUpLoad.ButtonsRight.Add(editorButton5);
            this.uTextFileUpLoad.ButtonsRight.Add(editorButton6);
            this.uTextFileUpLoad.Location = new System.Drawing.Point(464, 156);
            this.uTextFileUpLoad.MaxLength = 1000;
            this.uTextFileUpLoad.Name = "uTextFileUpLoad";
            this.uTextFileUpLoad.ReadOnly = true;
            this.uTextFileUpLoad.Size = new System.Drawing.Size(304, 21);
            this.uTextFileUpLoad.TabIndex = 89;
            this.uTextFileUpLoad.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFileUpLoad_EditorButtonClick);
            // 
            // uLabelFileName
            // 
            this.uLabelFileName.Location = new System.Drawing.Point(324, 156);
            this.uLabelFileName.Name = "uLabelFileName";
            this.uLabelFileName.Size = new System.Drawing.Size(136, 20);
            this.uLabelFileName.TabIndex = 88;
            this.uLabelFileName.Text = "첨부파일";
            // 
            // uComboVerifyResult
            // 
            this.uComboVerifyResult.Location = new System.Drawing.Point(136, 156);
            this.uComboVerifyResult.MaxLength = 3;
            this.uComboVerifyResult.Name = "uComboVerifyResult";
            this.uComboVerifyResult.Size = new System.Drawing.Size(100, 21);
            this.uComboVerifyResult.TabIndex = 87;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(136, 204);
            this.uTextEtcDesc.MaxLength = 200;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(616, 21);
            this.uTextEtcDesc.TabIndex = 86;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 204);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(120, 20);
            this.uLabelEtcDesc.TabIndex = 85;
            this.uLabelEtcDesc.Text = "비고";
            // 
            // uLabelVerityResult
            // 
            this.uLabelVerityResult.Location = new System.Drawing.Point(12, 156);
            this.uLabelVerityResult.Name = "uLabelVerityResult";
            this.uLabelVerityResult.Size = new System.Drawing.Size(120, 20);
            this.uLabelVerityResult.TabIndex = 84;
            this.uLabelVerityResult.Text = "검증결과";
            // 
            // uLabelNextVerityDate
            // 
            this.uLabelNextVerityDate.Location = new System.Drawing.Point(324, 132);
            this.uLabelNextVerityDate.Name = "uLabelNextVerityDate";
            this.uLabelNextVerityDate.Size = new System.Drawing.Size(136, 20);
            this.uLabelNextVerityDate.TabIndex = 83;
            this.uLabelNextVerityDate.Text = "차기검증일";
            // 
            // uDateNextVerifyDate
            // 
            appearance6.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateNextVerifyDate.Appearance = appearance6;
            this.uDateNextVerifyDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateNextVerifyDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateNextVerifyDate.Location = new System.Drawing.Point(464, 132);
            this.uDateNextVerifyDate.Name = "uDateNextVerifyDate";
            this.uDateNextVerifyDate.Size = new System.Drawing.Size(100, 21);
            this.uDateNextVerifyDate.TabIndex = 82;
            // 
            // uTextVerifyBasic
            // 
            this.uTextVerifyBasic.Location = new System.Drawing.Point(136, 180);
            this.uTextVerifyBasic.MaxLength = 100;
            this.uTextVerifyBasic.Name = "uTextVerifyBasic";
            this.uTextVerifyBasic.Size = new System.Drawing.Size(616, 21);
            this.uTextVerifyBasic.TabIndex = 81;
            // 
            // uLabelVerityBasic
            // 
            this.uLabelVerityBasic.Location = new System.Drawing.Point(12, 180);
            this.uLabelVerityBasic.Name = "uLabelVerityBasic";
            this.uLabelVerityBasic.Size = new System.Drawing.Size(120, 20);
            this.uLabelVerityBasic.TabIndex = 80;
            this.uLabelVerityBasic.Text = "검증근거";
            // 
            // uLabelHumidity
            // 
            this.uLabelHumidity.Location = new System.Drawing.Point(324, 108);
            this.uLabelHumidity.Name = "uLabelHumidity";
            this.uLabelHumidity.Size = new System.Drawing.Size(136, 20);
            this.uLabelHumidity.TabIndex = 77;
            this.uLabelHumidity.Text = "습도";
            // 
            // uLabelTemperature
            // 
            this.uLabelTemperature.Location = new System.Drawing.Point(12, 108);
            this.uLabelTemperature.Name = "uLabelTemperature";
            this.uLabelTemperature.Size = new System.Drawing.Size(120, 20);
            this.uLabelTemperature.TabIndex = 76;
            this.uLabelTemperature.Text = "온도";
            // 
            // uTextDept
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDept.Appearance = appearance14;
            this.uTextDept.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDept.Location = new System.Drawing.Point(672, 84);
            this.uTextDept.Name = "uTextDept";
            this.uTextDept.ReadOnly = true;
            this.uTextDept.Size = new System.Drawing.Size(100, 21);
            this.uTextDept.TabIndex = 75;
            // 
            // uTextCarryInDate
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryInDate.Appearance = appearance3;
            this.uTextCarryInDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryInDate.Location = new System.Drawing.Point(136, 84);
            this.uTextCarryInDate.Name = "uTextCarryInDate";
            this.uTextCarryInDate.ReadOnly = true;
            this.uTextCarryInDate.Size = new System.Drawing.Size(100, 21);
            this.uTextCarryInDate.TabIndex = 74;
            // 
            // uLabelVerityDate
            // 
            this.uLabelVerityDate.Location = new System.Drawing.Point(12, 132);
            this.uLabelVerityDate.Name = "uLabelVerityDate";
            this.uLabelVerityDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelVerityDate.TabIndex = 73;
            this.uLabelVerityDate.Text = "검증일";
            // 
            // uDateVerityDate
            // 
            appearance7.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateVerityDate.Appearance = appearance7;
            this.uDateVerityDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateVerityDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateVerityDate.Location = new System.Drawing.Point(136, 132);
            this.uDateVerityDate.Name = "uDateVerityDate";
            this.uDateVerityDate.Size = new System.Drawing.Size(100, 21);
            this.uDateVerityDate.TabIndex = 72;
            // 
            // uLabelVerityUser
            // 
            this.uLabelVerityUser.Location = new System.Drawing.Point(324, 84);
            this.uLabelVerityUser.Name = "uLabelVerityUser";
            this.uLabelVerityUser.Size = new System.Drawing.Size(136, 20);
            this.uLabelVerityUser.TabIndex = 71;
            this.uLabelVerityUser.Text = "검증자";
            // 
            // uTextName
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextName.Appearance = appearance9;
            this.uTextName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextName.Location = new System.Drawing.Point(568, 84);
            this.uTextName.Name = "uTextName";
            this.uTextName.ReadOnly = true;
            this.uTextName.Size = new System.Drawing.Size(100, 21);
            this.uTextName.TabIndex = 70;
            // 
            // uTextVerityUserID
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVerityUserID.Appearance = appearance28;
            this.uTextVerityUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance26.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance26.TextHAlignAsString = "Center";
            editorButton7.Appearance = appearance26;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextVerityUserID.ButtonsRight.Add(editorButton7);
            this.uTextVerityUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVerityUserID.Location = new System.Drawing.Point(464, 84);
            this.uTextVerityUserID.MaxLength = 20;
            this.uTextVerityUserID.Name = "uTextVerityUserID";
            this.uTextVerityUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextVerityUserID.TabIndex = 69;
            this.uTextVerityUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextVerityUserID_KeyDown);
            this.uTextVerityUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextVerityUserID_EditorButtonClick);
            // 
            // uLabelCarryInDate
            // 
            this.uLabelCarryInDate.Location = new System.Drawing.Point(12, 84);
            this.uLabelCarryInDate.Name = "uLabelCarryInDate";
            this.uLabelCarryInDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelCarryInDate.TabIndex = 68;
            this.uLabelCarryInDate.Text = "입고일";
            // 
            // uTextSerialNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance8;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(464, 36);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSerialNo.TabIndex = 67;
            // 
            // uLabelVerityType
            // 
            this.uLabelVerityType.Location = new System.Drawing.Point(1000, 4);
            this.uLabelVerityType.Name = "uLabelVerityType";
            this.uLabelVerityType.Size = new System.Drawing.Size(12, 20);
            this.uLabelVerityType.TabIndex = 66;
            this.uLabelVerityType.Text = "검증유형";
            this.uLabelVerityType.Visible = false;
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(324, 36);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(136, 20);
            this.uLabelSerialNo.TabIndex = 66;
            this.uLabelSerialNo.Text = "SerialNo";
            // 
            // uTextVendor
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance27;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(464, 60);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(150, 21);
            this.uTextVendor.TabIndex = 65;
            // 
            // uLabelMade
            // 
            this.uLabelMade.Location = new System.Drawing.Point(324, 60);
            this.uLabelMade.Name = "uLabelMade";
            this.uLabelMade.Size = new System.Drawing.Size(136, 20);
            this.uLabelMade.TabIndex = 64;
            this.uLabelMade.Text = "제작사";
            // 
            // uTextModel
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance36;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(136, 60);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(132, 21);
            this.uTextModel.TabIndex = 63;
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(12, 60);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(120, 20);
            this.uLabelModel.TabIndex = 62;
            this.uLabelModel.Text = "Model";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(120, 20);
            this.uLabelPlant.TabIndex = 60;
            this.uLabelPlant.Text = "공장";
            // 
            // uLabelInCorrectNo
            // 
            this.uLabelInCorrectNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelInCorrectNo.Name = "uLabelInCorrectNo";
            this.uLabelInCorrectNo.Size = new System.Drawing.Size(120, 20);
            this.uLabelInCorrectNo.TabIndex = 60;
            this.uLabelInCorrectNo.Text = "관리번호";
            // 
            // uTextCodeName
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCodeName.Appearance = appearance12;
            this.uTextCodeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCodeName.Location = new System.Drawing.Point(568, 12);
            this.uTextCodeName.Name = "uTextCodeName";
            this.uTextCodeName.ReadOnly = true;
            this.uTextCodeName.Size = new System.Drawing.Size(100, 21);
            this.uTextCodeName.TabIndex = 59;
            // 
            // uTextCode
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCode.Appearance = appearance29;
            this.uTextCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance5.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance5.TextHAlignAsString = "Center";
            editorButton8.Appearance = appearance5;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCode.ButtonsRight.Add(editorButton8);
            this.uTextCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCode.Location = new System.Drawing.Point(464, 12);
            this.uTextCode.Name = "uTextCode";
            this.uTextCode.Size = new System.Drawing.Size(100, 21);
            this.uTextCode.TabIndex = 58;
            this.uTextCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCode_EditorButtonClick);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid;
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Controls.Add(this.uGridInVerityD);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 232);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1038, 356);
            this.uGroupBox1.TabIndex = 57;
            // 
            // uButtonDeleteRow
            // 
            appearance72.FontData.BoldAsString = "False";
            this.uButtonDeleteRow.Appearance = appearance72;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 59;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridInVerityD
            // 
            this.uGridInVerityD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInVerityD.DisplayLayout.Appearance = appearance40;
            this.uGridInVerityD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInVerityD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityD.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInVerityD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGridInVerityD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInVerityD.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGridInVerityD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInVerityD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInVerityD.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInVerityD.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGridInVerityD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInVerityD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityD.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInVerityD.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridInVerityD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInVerityD.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInVerityD.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGridInVerityD.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridInVerityD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInVerityD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridInVerityD.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridInVerityD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInVerityD.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridInVerityD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInVerityD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInVerityD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInVerityD.Location = new System.Drawing.Point(12, 36);
            this.uGridInVerityD.Name = "uGridInVerityD";
            this.uGridInVerityD.Size = new System.Drawing.Size(1012, 308);
            this.uGridInVerityD.TabIndex = 46;
            this.uGridInVerityD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridInVerityD_AfterCellUpdate);
            this.uGridInVerityD.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridInVerityD_ClickCellButton);
            this.uGridInVerityD.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridInVerityD_CellChange);
            // 
            // uTextVerityNumber
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityNumber.Appearance = appearance10;
            this.uTextVerityNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVerityNumber.Location = new System.Drawing.Point(212, 36);
            this.uTextVerityNumber.Name = "uTextVerityNumber";
            this.uTextVerityNumber.ReadOnly = true;
            this.uTextVerityNumber.Size = new System.Drawing.Size(72, 21);
            this.uTextVerityNumber.TabIndex = 1;
            // 
            // uTextInVerityNo
            // 
            appearance11.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInVerityNo.Appearance = appearance11;
            this.uTextInVerityNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInVerityNo.Location = new System.Drawing.Point(136, 36);
            this.uTextInVerityNo.Name = "uTextInVerityNo";
            this.uTextInVerityNo.ReadOnly = true;
            this.uTextInVerityNo.Size = new System.Drawing.Size(72, 21);
            this.uTextInVerityNo.TabIndex = 1;
            // 
            // uLabelManageNo
            // 
            this.uLabelManageNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelManageNo.Name = "uLabelManageNo";
            this.uLabelManageNo.Size = new System.Drawing.Size(130, 20);
            this.uLabelManageNo.TabIndex = 20;
            this.uLabelManageNo.Text = "관리번호";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQAT0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGridInVerityH);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0008";
            this.Load += new System.EventHandler(this.frmQAT0008_Load);
            this.Activated += new System.EventHandler(this.frmQAT0008_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQAT0008_FormClosing);
            this.Resize += new System.EventHandler(this.frmQAT0008_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionSearchCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInVerityH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxComplete)).EndInit();
            this.uGroupBoxComplete.ResumeLayout(false);
            this.uGroupBoxComplete.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConfirmUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileUpLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboVerifyResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateNextVerifyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerifyBasic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateVerityDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCodeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridInVerityD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVerityNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInVerityNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInVerityH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInVerityNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchModelName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolCode;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInVerityD;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCodeName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCode;
        private Infragistics.Win.Misc.UltraLabel uLabelManageNo;
        private Infragistics.Win.Misc.UltraLabel uLabelInCorrectNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryInDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelMade;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryInDate;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateVerityDate;
        private Infragistics.Win.Misc.UltraLabel uLabelHumidity;
        private Infragistics.Win.Misc.UltraLabel uLabelTemperature;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerifyBasic;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityBasic;
        private Infragistics.Win.Misc.UltraLabel uLabelNextVerityDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateNextVerifyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDept;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityResult;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboVerifyResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileUpLoad;
        private Infragistics.Win.Misc.UltraLabel uLabelFileName;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionCheck;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionSearchCheck;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextHumidity;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uTextTemperature;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVerityNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelVerityType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboVerityType;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelConfirmUserID;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextConfirmUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextConfirmUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxComplete;
    }
}