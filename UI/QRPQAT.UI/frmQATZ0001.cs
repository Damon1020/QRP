﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : Qual 관리                                             */
/* 프로그램ID   : frmQATZ0001.cs                                        */
/* 프로그램명   : 신제품 Qual 관리                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPQAT.UI
{
    public partial class frmQATZ0001 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        #region 전역변수
        
        // Machine Model 승인정보에서 화면이동하기 위한 전역변수
        private string m_strPlantCode;
        private string m_strQualNo;
        private string m_strMoveFormName;        

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string QualNo
        {
            get { return m_strQualNo; }
            set { m_strQualNo = value; }
        }

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }

        #endregion

        public frmQATZ0001()
        {
            InitializeComponent();
        }

        #region Form 관련 Method, Event

        private void frmQATZ0001_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 여부 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0001_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Set TitleArea Text
            this.titleArea.mfSetLabelText("신제품 Qual 관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Control 초기화 Method 호출
            SetToolAuth();
            InitLabel();
            InitButton();
            InitText();
            InitGrid();
            InitComboBox();
            InitGroupBox();
            InitEtc();

            if (!string.IsNullOrEmpty(MoveFormName))
            {
                if (MoveFormName == "frmQATZ0010")
                {
                    titleArea.TextName = titleArea.TextName + "(Machine Model 승인정보 이동)";
                }
                else if (MoveFormName.Equals("frmINSZ0002"))
                {
                    titleArea.TextName = titleArea.TextName + "(신규자재 인증등록 이동)";
                }

                // 조회 Method 호출
                Search_HeaderInfo(PlantCode, QualNo, m_resSys.GetString("SYS_LANG"));
                Search_PackageList(PlantCode, QualNo, m_resSys.GetString("SYS_LANG"));
                Search_MaterialList(PlantCode, QualNo, m_resSys.GetString("SYS_LANG"));
                Search_EvidenceList(PlantCode, QualNo, m_resSys.GetString("SYS_LANG"));

                this.uComboPlant.Value = PlantCode;
                this.uTextQualNo.Text = QualNo;

                // 완료여부에 따른 컨트롤 활성화 정의 메소드 호출
                if (this.uCheckCompleteFlag.Enabled)
                {
                    SetWritable();
                }
                else
                {
                    SetDisWritable();
                }

                this.uGroupBoxContentsArea.Expanded = true;
            }

            WinGrid wGrid = new WinGrid();
            wGrid.mfLoadGridColumnProperty(this);

            // E-Mail TEST
            //if (m_resSys.GetString("SYS_USERID").Equals("TESTUSER"))
            //    this.uButtonEmailTest.Visible = true;
            //else
            //    this.uButtonEmailTest.Visible = false;

        }

        private void frmQATZ0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                wGrid.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxStdInfo, GroupBoxType.INFO, "기본정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxEvidenceInfo, GroupBoxType.INFO, "Evidence 정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxPackage, GroupBoxType.LIST, "적용제품", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxEmail, GroupBoxType.LIST, "E-mail 송부", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxPCN, GroupBoxType.DETAIL, "PCN 변경 전/후", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                 , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                 , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                wGroupBox.mfSetGroupBox(this.uGroupBoxBomList, GroupBoxType.LIST, "Bom List", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                

                // Set Font
                this.uGroupBoxStdInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxStdInfo.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxEvidenceInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxEvidenceInfo.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxPackage.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxPackage.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxEmail.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxEmail.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxPCN.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxPCN.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxBomList.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxBomList.HeaderAppearance.FontData.SizeInPoints = 9;


                // ContentsArea 접힌 상태로 설정
                this.uGroupBoxContentsArea.Expanded = false;

                this.uGroupBoxBomList.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Max Length 지정
                this.uTextSearchCustomerCode.MaxLength = 10;
                this.uTextCustomerCode.MaxLength = 10;
                this.uTextWriteID.MaxLength = 20;
                this.uTextPurpose.MaxLength = 500;
                this.uTextChangeAfter.MaxLength = 500;
                this.uTextChangeBefore.MaxLength = 500;

                // ID 대문자 입력
                this.uTextWriteID.ImeMode = ImeMode.Disable;
                this.uTextWriteID.CharacterCasing = CharacterCasing.Upper;

                // Multi TextBox 설정
                this.uTextPurpose.Scrollbars = ScrollBars.Vertical;
                this.uTextChangeBefore.Scrollbars = ScrollBars.Vertical;
                this.uTextChangeAfter.Scrollbars = ScrollBars.Vertical;

                // Display Style
                this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;

                // SET DefaultValue
                this.uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // DateTime Editor SET MaskInput
                this.uDateFinalCompleteDate.MaskInput = "yyyy-mm-dd";
                this.uDateWriteDate.MaskInput = "yyyy-mm-dd";
                this.uDateWriteDateFrom.MaskInput = "yyyy-mm-dd";
                this.uDateWrtiteDateTo.MaskInput = "yyyy-mm-dd";

                // DateTime Editor SET DefaultValue
                this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateWriteDateFrom.Value = DateTime.Now.AddDays(-14).ToString("yyyy-MM-dd");
                this.uDateWrtiteDateTo.Value = DateTime.Now.ToString("yyyy-MM-dd");
                //this.uDateFinalCompleteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateFinalCompleteDate.Value = null;

                // SET UseOsThemes
                this.uDateFinalCompleteDate.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
                this.uDateWriteDate.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
                this.uDateWriteDateFrom.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
                this.uDateWrtiteDateTo.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;

                // SET DisplayStyle
                this.uDateFinalCompleteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateWriteDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateWrtiteDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                // 검색조건
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchWriteDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchQualType, "구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 입력
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelQualType, "구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelQualNo, "Qual No", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelWriteDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabel4MDivision, "4M구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPurpose, "목적", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelChangeBefore, "변경 전", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeAfter, "변경 후", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelFinalCompleteDate, "최종승인일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFinalInspectResultFlag, "최종합격여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDel_PackageList, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDel_EvidenceList, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDel_MatList, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonFileDown_EvidenceList, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_FillDown);
                wButton.mfSetButton(this.uButtonDel_Email, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 검색
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                // 입력
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

                // 구분
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCod = clsComCode.mfReadCommonCode("C0021", m_resSys.GetString("SYS_LANG"));

                // 검색
                wCombo.mfSetComboEditor(this.uComboSearchQualType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "ComCode", "ComCodeName", dtComCod);

                // 입력
                wCombo.mfSetComboEditor(this.uComboQualType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtComCod);

                // Package 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                // 검색조건 : Package 콤보박스
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "Package", "ComboName", dtPackage);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드

                // Qual 그리드 일반설정
                wGrid.mfInitGeneralGrid(this.uGridQualList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridQualList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////wGrid.mfSetGridColumn(this.uGridQualList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                //////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "QualNo", "QualNo", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "QualTypeName", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////wGrid.mfSetGridColumn(this.uGridQualList, 0, "CustomerCode", "고객사코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                //////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "CustomerName", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMManFlag", "Man", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMEquipFlag", "Equip", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMMethodFlag", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMEnviroFlag", "Enviro", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMMaterialFlag", "Material", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FourMOthers", "Other", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////wGrid.mfSetGridColumn(this.uGridQualList, 0, "WriteUserID", "등록자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                //////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "Purpose", "목적", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 250, false, false, 500
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FinalInspectResultFlag", "최종합격여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "FinalCompleteDate", "최종승인일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "ChangeBefore", "变更前", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 250, false, false, 500
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQualList, 0, "ChangeAfter", "变更后", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 250, false, false, 500
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridQualList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridQualList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집불가 상태로 설정
                this.uGridQualList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion

                #region Package List

                // Package정보 그리드
                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridPackageList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridPackageList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridPackageList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackageList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridPackageList, 0);

                // Set FontSize
                this.uGridPackageList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPackageList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //DataTable dtPackage = new DataTable();
                //wGrid.mfSetGridColumnValueList(this.uGridPackageList, 0, "Package", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPackage);

                #endregion

                #region Material List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridMatList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridMatList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 40, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGridMatList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridMatList, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatList, 0, "MaterialCode", "원자재 코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatList, 0, "MaterialName", "원자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 180, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 250
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", " ");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridMatList, 0);

                // Set FontSize
                this.uGridMatList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMatList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                DataTable dtMat = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGridMatList, 0, "MaterialCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtMat);

                //DataTable dtCon = new DataTable();
                //wGrid.mfSetGridColumnValueList(this.uGridMatList, 0, "ConsumableTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCon);

                #endregion

                #region Evidence List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEvidenceList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "EvidenceType", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "StartDate", "시작일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar, "", "yyyy-mm-dd", DBNull.Value.ToString());

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "CompleteDate", "완료일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar, "", "yyyy-mm-dd", DBNull.Value.ToString());

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "InspectFlag", "판정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "WriteUserID", "등록자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "WriteUserName", "등록자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "FilePath", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEvidenceList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridEvidenceList, 0);

                // Set FontSize
                this.uGridEvidenceList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEvidenceList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtEvidence = clsCom.mfReadCommonCode("C0060", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridEvidenceList, 0, "EvidenceType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtEvidence);

                DataTable dtInspectFlag = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridEvidenceList, 0, "InspectFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtInspectFlag);

                #endregion

                #region E-Mail List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEmail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEmail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridEmail, 0);

                // Set FontSize
                this.uGridEmail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEmail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                QRPQAT.BL.QATQUL.QATNewQualH qualH = new QRPQAT.BL.QATQUL.QATNewQualH();
                brwChannel.mfCredentials(qualH);

                // Show Progress PopUP
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색조건 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomerCode = this.uTextSearchCustomerCode.Text;
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strWriteDateFrom = Convert.ToDateTime(this.uDateWriteDateFrom.Value).ToString("yyyy-MM-dd");
                string strWriteDateTo = Convert.ToDateTime(this.uDateWrtiteDateTo.Value).ToString("yyyy-MM-dd");
                string strQualType = this.uComboSearchQualType.Value.ToString();
                
                //검색
                DataTable dtHeader = qualH.mfReadQATNewQualH(strPlantCode, strCustomerCode, strPackage, strWriteDateFrom, strWriteDateTo, strQualType, m_resSys.GetString("SYS_LANG"));
                
                //Data Binding
                this.uGridQualList.DataSource = dtHeader;
                this.uGridQualList.DataBind();

                if (dtHeader.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridQualList, 0);
                }

                // Close Progress PopUP
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (this.uGridQualList.Rows.Count == 0)
                {
                    DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (!this.uCheckCompleteFlag.Enabled && this.uCheckCompleteFlag.CheckedValue.Equals(true))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000980", "M000993"
                                            , Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 헤더 필수입력사항 확인

                // 필수사항 입력확인
                ////if (string.IsNullOrEmpty(this.uComboPlant.Value.ToString()) || this.uComboPlant.Value == DBNull.Value)
                ////{
                ////Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                ////    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////    , "확인창", "필수사항 입력확인", "공장을 선택해 주세요."
                ////    , Infragistics.Win.HAlign.Right);

                ////this.uComboPlant.DropDown();
                ////return;
                ////}

                if (string.IsNullOrEmpty(this.uComboQualType.Value.ToString()) || this.uComboQualType.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001223", "M000781"
                                            , Infragistics.Win.HAlign.Right);

                    this.uComboQualType.DropDown();
                    return;
                }
                else if (string.IsNullOrEmpty(this.uTextCustomerCode.Text) || this.uTextCustomerCode.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001223", "M000256"
                                            , Infragistics.Win.HAlign.Right);

                    this.uTextCustomerCode.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(this.uTextWriteID.Text) || this.uTextWriteID.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001223", "M000386"
                                            , Infragistics.Win.HAlign.Right);

                    this.uTextWriteID.Focus();
                    return;
                }
                else if (this.uDateWriteDate.Value == null || this.uDateWriteDate.Value == DBNull.Value || this.uDateWriteDate.Value.ToString() == string.Empty)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001223", "M000385"
                                            , Infragistics.Win.HAlign.Right);

                    this.uDateWriteDate.DropDown();
                    return;
                }

                #endregion
                else
                {
                    #region 세부정보 필수입력 확인

                    // Package List 필수입력 확인
                    if (this.uGridPackageList.Rows.Count > 0)
                    {
                        this.uGridPackageList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                        for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                        {
                            if (string.IsNullOrEmpty(this.uGridPackageList.Rows[i].Cells["Package"].Value.ToString()) || this.uGridPackageList.Rows[i].Cells["Package"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001223",m_resSys.GetString("SYS_LANG"))
                                            , (i + 1).ToString() + msg.GetMessge_Text("M000541",m_resSys.GetString("SYS_LANG"))
                                            , Infragistics.Win.HAlign.Right);

                                this.uGridPackageList.Rows[i].Cells["Package"].Activate();
                                this.uGridPackageList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }
                    }

                    // Material List 필수입력 확인
                    if (this.uGridMatList.Rows.Count > 0)
                    {
                        this.uGridMatList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                        for (int i = 0; i < this.uGridMatList.Rows.Count; i++)
                        {
                            if (string.IsNullOrEmpty(this.uGridMatList.Rows[i].Cells["MaterialCode"].Value.ToString()) || this.uGridMatList.Rows[i].Cells["MaterialCode"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001223",m_resSys.GetString("SYS_LANG"))
                                            , (i + 1).ToString() + msg.GetMessge_Text("M000548",m_resSys.GetString("SYS_LANG"))
                                            , Infragistics.Win.HAlign.Right);

                                this.uGridMatList.Rows[i].Cells["MaterialCode"].Activate();
                                this.uGridMatList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }
                    }

                    ////////// EvidenceList 필수입력 확인
                    ////////if (this.uGridEvidenceList.Rows.Count > 0)
                    ////////{
                    ////////    this.uGridEvidenceList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    ////////    for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                    ////////    {
                    ////////        if (string.IsNullOrEmpty(this.uGridEvidenceList.Rows[i].Cells[""].Value.ToString()) || this.uGridEvidenceList.Rows[i].Cells[""].Value == DBNull.Value)
                    ////////        {
                    ////////            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    ////////                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////////                        , "확인창", "필수사항 입력확인", (i + 1).ToString() + "번째 줄의 를 선택해 주세요."
                    ////////                        , Infragistics.Win.HAlign.Right);

                    ////////            this.uGridEvidenceList.Rows[i].Cells[""].Activate();
                    ////////            this.uGridEvidenceList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    ////////            return;
                    ////////        }
                    ////////    }
                    ////////}
                    for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                    {
                        if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains("/")
                        || this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains("#")
                        || this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains("*")
                        || this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains("<")
                        || this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(">"))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }

                    #endregion

                    // 콤보박스 상태 확인
                    QRPCOM.QRPUI.CommonControl clsCom = new CommonControl();
                    if (!clsCom.mfCheckValidValueBeforSave(this))
                        return;

                    string strMsg = string.Empty;
                    if (this.uCheckCompleteFlag.Checked)
                    {
                        strMsg = "M000983";
                    }
                    else
                    {
                        strMsg = "M000905";
                    }
                    // 저장확인창
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053"
                                        , strMsg, Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        // 저장용 DataTable 설정
                        DataTable dtHeader = Rtn_HeaderInfo();
                        DataTable dtPackageList = Rtn_PackageInfo();
                        DataTable dtMaterialList = Rtn_MaterialInfo();
                        DataTable dtEvidenceList = Rtn_EvidenceInfo();
                        DataTable dtEmail = Rtn_EmailUser();

                        // Connect BL 
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                        QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                        brwChannel.mfCredentials(clsHeader);

                        // 저장 메소드 호출
                        string strErrRtn = clsHeader.mfSaveQATNewQualH(dtHeader
                                                                    , m_resSys.GetString("SYS_USERIP")
                                                                    , m_resSys.GetString("SYS_USERID")
                                                                    , dtPackageList
                                                                    , dtEvidenceList
                                                                    , dtMaterialList
                                                                    , dtEmail);

                        // 저장결과 확인
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            string strQualNo = ErrRtn.mfGetReturnValue(0);

                            // 저장성공시 첨부파일 Upload 메소드 호출
                            FileUpload(strQualNo);

                            // 메일전송 메소드 호출
                            //if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                                SendMail(strQualNo);

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037"
                                                    , "M000953"
                                                    , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037"
                                                    , ErrRtn.ErrMessage
                                                    , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                // 입력사항 확인
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    ////DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                       "확인창", "삭제 정보 확인", "리스트에서 삭제할 정보를 선택해 주세요.",
                    ////                       Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (string.IsNullOrEmpty(this.uTextQualNo.Text) || this.uTextQualNo.Value == DBNull.Value)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001264", "M000626", "M000394",
                                           Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    // 콤보박스 상태 확인
                    QRPCOM.QRPUI.CommonControl clsCom = new CommonControl();
                    if (!clsCom.mfCheckValidValueBeforSave(this))
                        return;

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // Connect BL 
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                        QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                        brwChannel.mfCredentials(clsHeader);

                        // 변수 설정
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string strQualNo = this.uTextQualNo.Text;

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 삭제 메소드 호출
                        string rtMSG = clsHeader.mfDeleteNewQualH(strPlantCode, strQualNo);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrNum == 0)
                        {
                            // 성공시 첨부파일 삭제 메소드 호출
                            FileDelete();

                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M000638", "M000926",
                                               Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M000638", "M000923",
                                                   Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M000638"
                                                    , ErrRtn.ErrMessage
                                                    , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                SetWritable();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridQualList);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Methods...

        #region 일반메소드

        /// <summary>
        /// 첨부파일 삭제 메소드(전체)
        /// </summary>
        private void FileDelete()
        {
            try
            {
                // 첨부파일 저장경로정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0012");

                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                {
                    if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uComboPlant.Value.ToString()) &&
                        this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextQualNo.Text) &&
                        !this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                    {
                        arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString());
                    }
                }

                if (arrFile.Count > 0)
                {
                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드
        /// </summary>
        ///<param name="strQualNo">Qual 관리번호</param>
        private void FileUpload(string strQualNo)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                {
                    if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                    {
                        // 파일이름변경(공장코드+관리번호+순번+화일명)하여 복사하기//
                        FileInfo fileDoc = new FileInfo(this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString());
                        string strUploadFile = fileDoc.DirectoryName + "\\" +
                                               this.uComboPlant.Value.ToString() + "-" + strQualNo + "-" +
                                               this.uGridEvidenceList.Rows[i].RowSelectorNumber.ToString() + "-" + fileDoc.Name;
                        //변경한 화일이 있으면 삭제하기
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);
                        //변경한 화일이름으로 복사하기
                        File.Copy(this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                        arrFile.Add(strUploadFile);
                    }
                }

                // 업로드할 파일이 있는경우 파일 업로드
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0012");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    for (int i = 0; i < arrFile.Count; i++)
                    {
                        File.Delete(arrFile[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 문서 수정 가능
        /// </summary>
        private void SetWritable()
        {
            try
            {
                //Header 수정 불가
                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboQualType.ReadOnly = false;
                this.uComboQualType.Appearance.BackColor = Color.PowderBlue;
                this.uTextCustomerCode.ReadOnly = false;
                this.uTextCustomerCode.Appearance.BackColor = Color.PowderBlue;
                this.uDateWriteDate.ReadOnly = false;
                this.uDateWriteDate.Appearance.BackColor = Color.PowderBlue;
                this.uTextWriteID.ReadOnly = false;
                this.uTextWriteID.Appearance.BackColor = Color.PowderBlue;

                this.uCheckEnvironment.Enabled = true;
                this.uCheckMachine.Enabled = true;
                this.uCheckMan.Enabled = true;
                this.uCheckMaterial.Enabled = true;
                this.uCheckMethod.Enabled = true;
                this.uCheckMOther.Enabled = true;

                this.uDateFinalCompleteDate.ReadOnly = false;
                this.uDateFinalCompleteDate.Appearance.BackColor = Color.White;
                this.uOptionFinalInspectResultFlag.Enabled = true;

                this.uButtonDel_EvidenceList.Enabled = true;
                this.uButtonDel_MatList.Enabled = true;
                this.uButtonDel_PackageList.Enabled = true;
                this.uTextPurpose.ReadOnly = false;
                this.uTextPurpose.Appearance.BackColor = Color.White;
                this.uTextChangeAfter.ReadOnly = false;
                this.uTextChangeAfter.Appearance.BackColor = Color.White;
                this.uTextChangeBefore.ReadOnly = false;
                this.uTextChangeBefore.Appearance.BackColor = Color.White;

                // 편집 가능하게 설정
                this.uGridPackageList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridEvidenceList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridMatList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridEmail.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                // 신규입력 가능하게 설정
                this.uGridPackageList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGridEvidenceList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGridMatList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGridEmail.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 문서 수정 불가능
        /// </summary>
        private void SetDisWritable()
        {
            try
            {
                //Header 수정 불가
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uComboQualType.ReadOnly = true;
                this.uComboQualType.Appearance.BackColor = Color.Gainsboro;
                this.uTextCustomerCode.ReadOnly = true;
                this.uTextCustomerCode.Appearance.BackColor = Color.Gainsboro;
                this.uDateWriteDate.ReadOnly = true;
                this.uDateWriteDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextWriteID.ReadOnly = true;
                this.uTextWriteID.Appearance.BackColor = Color.Gainsboro;

                this.uCheckEnvironment.Enabled = false;
                this.uCheckMachine.Enabled = false;
                this.uCheckMan.Enabled = false;
                this.uCheckMaterial.Enabled = false;
                this.uCheckMethod.Enabled = false;
                this.uCheckMOther.Enabled = false;
                this.uDateFinalCompleteDate.ReadOnly = true;
                this.uDateFinalCompleteDate.Appearance.BackColor = Color.Gainsboro;
                this.uOptionFinalInspectResultFlag.Enabled = false;

                this.uButtonDel_EvidenceList.Enabled = false;
                this.uButtonDel_MatList.Enabled = false;
                this.uButtonDel_PackageList.Enabled = false;
                this.uTextPurpose.ReadOnly = true;
                this.uTextPurpose.Appearance.BackColor = Color.Gainsboro;
                this.uTextChangeAfter.ReadOnly = true;
                this.uTextChangeAfter.Appearance.BackColor = Color.Gainsboro;
                this.uTextChangeBefore.ReadOnly = true;
                this.uTextChangeBefore.Appearance.BackColor = Color.Gainsboro;

                this.uGridPackageList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGridEvidenceList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGridMatList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGridEmail.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                // 편집 가능하게 설정
                this.uGridPackageList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEvidenceList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridMatList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEmail.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 구분 콤보박스 값에 따른 컨트롤 Visible 상태 설정 메소드
        /// </summary>
        /// <param name="strQualTypeCode">구분 콤보박스 값</param>
        private void SetQualTypeState(string strQualTypeCode)
        {
            try
            {
                WinGroupBox wGroupBox = new WinGroupBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (strQualTypeCode.Equals(string.Empty))
                {
                    this.uGroupBoxBomList.Visible = false;
                    this.uGroupBoxPCN.Visible = false;
                    // 관리번호
                    this.uLabelQualNo.Visible = false;
                    this.uTextQualNo.Visible = false;
                    // 4M 구분
                    this.uLabel4MDivision.Visible = false;
                    this.uCheckMan.Visible = false;
                    this.uCheckEnvironment.Visible = false;
                    this.uCheckMachine.Visible = false;
                    this.uCheckMaterial.Visible = false;
                    this.uCheckMethod.Visible = false;
                    this.uCheckMOther.Visible = false;
                    // 변경전
                    this.uLabelChangeBefore.Visible = false;
                    this.uTextChangeBefore.Visible = false;
                    // 변경후
                    this.uLabelChangeAfter.Visible = false;
                    this.uTextChangeAfter.Visible = false;
                    // Material List
                    this.uButtonDel_MatList.Visible = false;
                    this.uGridMatList.Visible = false;

                    // Value 초기화
                    this.uCheckEnvironment.Checked = false;
                    this.uCheckMachine.Checked = false;
                    this.uCheckMan.Checked = false;
                    this.uCheckMaterial.Checked = false;
                    this.uCheckMethod.Checked = false;
                    this.uCheckMOther.Checked = false;
                    this.uTextChangeAfter.Clear();
                    this.uTextChangeBefore.Clear();
                    while (this.uGridMatList.Rows.Count > 0)
                    {
                        this.uGridMatList.Rows[0].Delete(false);
                    }
                }
                else if (strQualTypeCode.Equals("1"))
                {
                    this.uGroupBoxBomList.Visible = true;
                    this.uGroupBoxPCN.Visible = false;

                    // 관리번호
                    this.uLabelQualNo.Visible = true;
                    this.uTextQualNo.Visible = true;
                    // 4M 구분
                    this.uLabel4MDivision.Visible = false;
                    this.uCheckMan.Visible = false;
                    this.uCheckEnvironment.Visible = false;
                    this.uCheckMachine.Visible = false;
                    this.uCheckMaterial.Visible = false;
                    this.uCheckMethod.Visible = false;
                    this.uCheckMOther.Visible = false;
                    // 변경전
                    this.uLabelChangeBefore.Visible = false;
                    this.uTextChangeBefore.Visible = false;
                    // 변경후
                    this.uLabelChangeAfter.Visible = false;
                    this.uTextChangeAfter.Visible = false;
                    // Material List
                    this.uButtonDel_MatList.Visible = true;
                    this.uGridMatList.Visible = true;

                    // Value 초기화
                    this.uCheckEnvironment.Checked = false;
                    this.uCheckMachine.Checked = false;
                    this.uCheckMan.Checked = false;
                    this.uCheckMaterial.Checked = false;
                    this.uCheckMethod.Checked = false;
                    this.uCheckMOther.Checked = false;
                    this.uTextChangeAfter.Clear();
                    this.uTextChangeBefore.Clear();
                }
                else if (strQualTypeCode.Equals("2"))
                {
                    this.uGroupBoxPCN.Visible = true;
                    this.uGroupBoxBomList.Visible = false;


                    // 관리번호
                    this.uLabelQualNo.Visible = true;
                    this.uTextQualNo.Visible = true;
                    // 4M 구분
                    this.uLabel4MDivision.Visible = true;
                    this.uCheckMan.Visible = true;
                    this.uCheckEnvironment.Visible = true;
                    this.uCheckMachine.Visible = true;
                    this.uCheckMaterial.Visible = true;
                    this.uCheckMethod.Visible = true;
                    this.uCheckMOther.Visible = true;
                    // 변경전
                    this.uLabelChangeBefore.Visible = true;
                    this.uTextChangeBefore.Visible = true;
                    // 변경후
                    this.uLabelChangeAfter.Visible = true;
                    this.uTextChangeAfter.Visible = true;
                    // Material List
                    this.uButtonDel_MatList.Visible = false;
                    this.uGridMatList.Visible = false;

                    // Value 초기화
                    while (this.uGridMatList.Rows.Count > 0)
                    {
                        this.uGridMatList.Rows[0].Delete(false);
                    }
                }
                else if (strQualTypeCode.Equals("3"))
                {
                    this.uGroupBoxPCN.Visible = true;
                    this.uGroupBoxBomList.Visible = false;


                    // 관리번호
                    this.uLabelQualNo.Visible = true;
                    this.uTextQualNo.Visible = true;
                    // 4M 구분
                    this.uLabel4MDivision.Visible = true;
                    this.uCheckMan.Visible = true;
                    this.uCheckEnvironment.Visible = true;
                    this.uCheckMachine.Visible = true;
                    this.uCheckMaterial.Visible = true;
                    this.uCheckMethod.Visible = true;
                    this.uCheckMOther.Visible = true;
                    // 변경전
                    this.uLabelChangeBefore.Visible = true;
                    this.uTextChangeBefore.Visible = true;
                    // 변경후
                    this.uLabelChangeAfter.Visible = true;
                    this.uTextChangeAfter.Visible = true;
                    // Material List
                    this.uButtonDel_MatList.Visible = false;
                    this.uGridMatList.Visible = false;

                    // Value 초기화
                    while (this.uGridMatList.Rows.Count > 0)
                    {
                        this.uGridMatList.Rows[0].Delete(false);
                    }
                }
                else if (strQualTypeCode.Equals("4"))
                {
                    this.uGroupBoxPCN.Visible = true;
                    this.uGroupBoxBomList.Visible = false;


                    // 관리번호
                    this.uLabelQualNo.Visible = true;
                    this.uTextQualNo.Visible = true;
                    // 4M 구분
                    this.uLabel4MDivision.Visible = true;
                    this.uCheckMan.Visible = true;
                    this.uCheckEnvironment.Visible = true;
                    this.uCheckMachine.Visible = true;
                    this.uCheckMaterial.Visible = true;
                    this.uCheckMethod.Visible = true;
                    this.uCheckMOther.Visible = true;
                    // 변경전
                    this.uLabelChangeBefore.Visible = true;
                    this.uTextChangeBefore.Visible = true;
                    // 변경후
                    this.uLabelChangeAfter.Visible = true;
                    this.uTextChangeAfter.Visible = true;
                    // Material List
                    this.uButtonDel_MatList.Visible = false;
                    this.uGridMatList.Visible = false;

                    // Value 초기화
                    while (this.uGridMatList.Rows.Count > 0)
                    {
                        this.uGridMatList.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자명 반환 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 Value 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 컨트롤 활성화 메소드 호출
                SetWritable();

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextQualNo.Clear();
                this.uComboQualType.Value = string.Empty;
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextPurpose.Clear();

                this.uCheckEnvironment.Checked = false;
                this.uCheckMachine.Checked = false;
                this.uCheckMan.Checked = false;
                this.uCheckMaterial.Checked = false;
                this.uCheckMethod.Checked = false;
                this.uCheckMOther.Checked = false;

                this.uTextChangeAfter.Clear();
                this.uTextChangeBefore.Clear();

                this.uOptionFinalInspectResultFlag.CheckedIndex = -1;
                //this.uDateFinalCompleteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateFinalCompleteDate.Value = null;
                this.uCheckCompleteFlag.Checked = false;

                while (this.uGridPackageList.Rows.Count > 0)
                {
                    this.uGridPackageList.Rows[0].Delete(false);
                }

                while (this.uGridMatList.Rows.Count > 0)
                {
                    this.uGridMatList.Rows[0].Delete(false);
                }

                while (this.uGridEvidenceList.Rows.Count > 0)
                {
                    this.uGridEvidenceList.Rows[0].Delete(false);
                }

                while (this.uGridEmail.Rows.Count > 0)
                    this.uGridEmail.Rows[0].Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 저장용 데이터 테이블 반환 메소드

        /// <summary>
        /// 헤더정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderInfo()
        {
            DataTable dtHeader = new DataTable();
            try
            {
                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                brwChannel.mfCredentials(clsHeader);

                // 컬럼설정
                dtHeader = clsHeader.mfSetDataInfo();

                // Data Insert
                DataRow drRow = dtHeader.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["QualNo"] = this.uTextQualNo.Text;
                drRow["CustomerCode"] = this.uTextCustomerCode.Text;
                drRow["QualType"] = this.uComboQualType.Value.ToString();
                drRow["WriteUserID"] = this.uTextWriteID.Text;
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["FourMManFlag"] = this.uCheckMan.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FourMEquipFlag"] = this.uCheckMachine.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FourMMethodFlag"] = this.uCheckMethod.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FourMEnviroFlag"] = this.uCheckEnvironment.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FourMMaterialFlag"] = this.uCheckMaterial.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FourMOtherFlag"] = this.uCheckMOther.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["Purpose"] = this.uTextPurpose.Text;
                drRow["ChangeBefore"] = this.uTextChangeBefore.Text;
                drRow["ChangeAfter"] = this.uTextChangeAfter.Text;
                drRow["CompleteFlag"] = this.uCheckCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                if (this.uOptionFinalInspectResultFlag.CheckedIndex.Equals(-1))
                    drRow["FinalInspectResultFlag"] = string.Empty;
                else
                    drRow["FinalInspectResultFlag"] = this.uOptionFinalInspectResultFlag.Value.ToString();
                if (this.uDateFinalCompleteDate.Value == null || this.uDateFinalCompleteDate.Value == DBNull.Value || this.uDateFinalCompleteDate.Value.ToString() == string.Empty)
                    drRow["FinalCompleteDate"] = string.Empty;
                else
                    drRow["FinalCompleteDate"] = Convert.ToDateTime(this.uDateFinalCompleteDate.Value).ToString("yyyy-MM-dd");

                dtHeader.Rows.Add(drRow);

                return dtHeader;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtHeader;
            }
            finally
            {
                dtHeader.Dispose();
            }
        }

        /// <summary>
        /// Pacakge 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_PackageInfo()
        {
            DataTable dtPackageList = new DataTable();
            try
            {
                // ExitEditMode
                this.uGridPackageList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualD), "QATNewQualD");
                QRPQAT.BL.QATQUL.QATNewQualD clsPackage = new QRPQAT.BL.QATQUL.QATNewQualD();
                brwChannel.mfCredentials(clsPackage);

                // Set Column
                dtPackageList = clsPackage.mfSetDataInfo();

                // Set Data
                DataRow drRow;
                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                {
                    if (this.uGridPackageList.Rows[i].Hidden.Equals(false))
                    {
                        drRow = dtPackageList.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["QualNo"] = this.uTextQualNo.Text;
                        drRow["Seq"] = this.uGridPackageList.Rows[i].RowSelectorNumber;
                        drRow["Package"] = this.uGridPackageList.Rows[i].Cells["Package"].Value.ToString();
                        drRow["EtcDesc"] = this.uGridPackageList.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dtPackageList.Rows.Add(drRow);
                    }
                }

                return dtPackageList;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtPackageList;
            }
            finally
            {
                dtPackageList.Dispose();
            }
        }

        /// <summary>
        /// Evidence 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_EvidenceInfo()
        {
            DataTable dtEvidenceList = new DataTable();
            try
            {
                // ExitEditMode
                this.uGridEvidenceList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // Connect BL 
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualASEFile), "QATNewQualASEFile");
                QRPQAT.BL.QATQUL.QATNewQualASEFile clsEvidence = new QRPQAT.BL.QATQUL.QATNewQualASEFile();
                brwChannel.mfCredentials(clsEvidence);

                // Set Column
                dtEvidenceList = clsEvidence.mfSetDataInfo();

                DataRow drRow;
                for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                {
                    if (this.uGridEvidenceList.Rows[i].Hidden.Equals(false))
                    {
                        drRow = dtEvidenceList.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["QualNo"] = this.uTextQualNo.Text;
                        drRow["Seq"] = this.uGridEvidenceList.Rows[i].RowSelectorNumber;
                        drRow["EvidenceType"] = this.uGridEvidenceList.Rows[i].Cells["EvidenceType"].Value.ToString();
                        
                        if (this.uGridEvidenceList.Rows[i].Cells["StartDate"].Value == DBNull.Value ||
                            string.IsNullOrEmpty(this.uGridEvidenceList.Rows[i].Cells["StartDate"].Value.ToString()))
                            drRow["StartDate"] = string.Empty;
                        else
                            drRow["StartDate"] = Convert.ToDateTime(this.uGridEvidenceList.Rows[i].Cells["StartDate"].Value).ToString("yyyy-MM-dd");

                        if (this.uGridEvidenceList.Rows[i].Cells["CompleteDate"].Value == DBNull.Value ||
                            string.IsNullOrEmpty(this.uGridEvidenceList.Rows[i].Cells["CompleteDate"].Value.ToString()))
                            drRow["CompleteDate"] = string.Empty;
                        else
                            drRow["CompleteDate"] = Convert.ToDateTime(this.uGridEvidenceList.Rows[i].Cells["CompleteDate"].Value).ToString("yyyy-MM-dd");

                        drRow["InspectFlag"] = this.uGridEvidenceList.Rows[i].Cells["InspectFlag"].Value.ToString();
                        drRow["WriteUserID"] = this.uGridEvidenceList.Rows[i].Cells["WriteUserID"].Value.ToString();
                        
                        if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString());
                            drRow["FilePath"] = fileDoc.Name;
                        }
                        else
                            drRow["FilePath"] = this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString();

                        drRow["EtcDesc"] = this.uGridEvidenceList.Rows[i].Cells["EtcDesc"].Value.ToString();

                        dtEvidenceList.Rows.Add(drRow);
                    }
                }

                return dtEvidenceList;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtEvidenceList;
            }
            finally
            {
                dtEvidenceList.Dispose();
            }
        }

        /// <summary>
        /// Material 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_MaterialInfo()
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // ExitEditMode
                this.uGridMatList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualMaterial), "QATNewQualMaterial");
                QRPQAT.BL.QATQUL.QATNewQualMaterial clsMaterial = new QRPQAT.BL.QATQUL.QATNewQualMaterial();
                brwChannel.mfCredentials(clsMaterial);

                // Set Column
                dtMaterial = clsMaterial.mfSetDatainfo();

                // Set Data
                DataRow drRow;
                for (int i = 0; i < this.uGridMatList.Rows.Count; i++)
                {
                    if (this.uGridMatList.Rows[i].Hidden.Equals(false))
                    {
                        drRow = dtMaterial.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["QualNo"] = this.uTextQualNo.Text;
                        drRow["Seq"] = this.uGridMatList.Rows[i].RowSelectorNumber;
                        drRow["MaterialCode"] = this.uGridMatList.Rows[i].Cells["MaterialCode"].Value.ToString();
                        drRow["EtcDesc"] = this.uGridMatList.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dtMaterial.Rows.Add(drRow);
                    }
                }

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
                dtMaterial.Dispose();
            }
        }

        /// <summary>
        /// 이메일 송부 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_EmailUser()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualEMailUser), "QATNewQualEMailUser");
                QRPQAT.BL.QATQUL.QATNewQualEMailUser clsEmail = new QRPQAT.BL.QATQUL.QATNewQualEMailUser();
                brwChannel.mfCredentials(clsEmail);

                dtRtn = clsEmail.mfSetDataInfo();

                DataRow drRow;
                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (!this.uGridEmail.Rows[i].Hidden)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["QualNo"] = this.uTextQualNo.Text;
                        drRow["UserID"] = this.uGridEmail.Rows[i].Cells["UserID"].Value.ToString();
                        drRow["EtcDesc"] = this.uGridEmail.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        #endregion

        #region Data 조회 Method

        /// <summary>
        /// 헤더 상세정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderInfo(string strPlantCode, string strQualNo, string strLang)
        {
            try
            {
                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtHeader = clsHeader.mfReadQATNewQualH_Detail(strPlantCode, strQualNo, strLang);

                if (dtHeader.Rows.Count > 0)
                {
                    this.uTextCustomerCode.Text = dtHeader.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtHeader.Rows[0]["CustomerName"].ToString();
                    this.uComboQualType.Value = dtHeader.Rows[0]["QualType"];
                    this.uDateWriteDate.Value = dtHeader.Rows[0]["WriteDate"];
                    this.uTextWriteID.Text = dtHeader.Rows[0]["WriteUserID"].ToString();
                    this.uTextWriteName.Text = dtHeader.Rows[0]["WriteUserName"].ToString();
                    this.uTextPurpose.Text = dtHeader.Rows[0]["PurPose"].ToString();
                    this.uCheckMan.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMManFlag"]);
                    this.uCheckMachine.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMEquipFlag"]);
                    this.uCheckMethod.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMMethodFlag"]);
                    this.uCheckEnvironment.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMEnviroFlag"]);
                    this.uCheckMaterial.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMMaterialFlag"]);
                    this.uCheckMOther.Checked = Convert.ToBoolean(dtHeader.Rows[0]["FourMOtherFlag"]);
                    this.uTextChangeBefore.Text = dtHeader.Rows[0]["ChangeBefore"].ToString();
                    this.uTextChangeAfter.Text = dtHeader.Rows[0]["ChangeAfter"].ToString();
                    if (string.IsNullOrEmpty(dtHeader.Rows[0]["FinalInspectResultFlag"].ToString()))
                        this.uOptionFinalInspectResultFlag.CheckedIndex = -1;
                    else
                        this.uOptionFinalInspectResultFlag.Value = dtHeader.Rows[0]["FinalInspectResultFlag"];
                    if (string.IsNullOrEmpty(dtHeader.Rows[0]["FinalCompleteDate"].ToString()) || dtHeader.Rows[0]["FinalCompleteDate"] == DBNull.Value)
                        this.uDateFinalCompleteDate.Value = null;
                    else
                        this.uDateFinalCompleteDate.Value = dtHeader.Rows[0]["FinalCompleteDate"];

                    this.uCheckCompleteFlag.Enabled = !Convert.ToBoolean(dtHeader.Rows[0]["CompleteFlag"]);
                    this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["CompleteFlag"]);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// PackageList 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_PackageList(string strPlatnCode, string strQualNo, string strLang)
        {
            try
            {
                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualD), "QATNewQualD");
                QRPQAT.BL.QATQUL.QATNewQualD clsPackage = new QRPQAT.BL.QATQUL.QATNewQualD();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackageList = clsPackage.mfReadQATNewQualD(strPlatnCode, strQualNo, strLang);

                this.uGridPackageList.DataSource = dtPackageList;
                this.uGridPackageList.DataBind();

                if (dtPackageList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridPackageList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// EvidenceList 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_EvidenceList(string strPlatnCode, string strQualNo, string strLang)
        {
            try
            {
                // Connect BL 
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualASEFile), "QATNewQualASEFile");
                QRPQAT.BL.QATQUL.QATNewQualASEFile clsEvidence = new QRPQAT.BL.QATQUL.QATNewQualASEFile();
                brwChannel.mfCredentials(clsEvidence);

                DataTable dtEvidenceList = clsEvidence.mfReadQATNewQualASEFile(strPlatnCode, strQualNo, strLang);

                this.uGridEvidenceList.DataSource = dtEvidenceList;
                this.uGridEvidenceList.DataBind();

                if (dtEvidenceList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridEvidenceList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MaterialList 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_MaterialList(string strPlatnCode, string strQualNo, string strLang)
        {
            try
            {
                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualMaterial), "QATNewQualMaterial");
                QRPQAT.BL.QATQUL.QATNewQualMaterial clsMaterial = new QRPQAT.BL.QATQUL.QATNewQualMaterial();
                brwChannel.mfCredentials(clsMaterial);

                DataTable dtMaterialList = clsMaterial.mfReadQATNewQualMaterial(strPlatnCode, strQualNo, strLang);

                this.uGridMatList.DataSource = dtMaterialList;
                this.uGridMatList.DataBind();

                if (dtMaterialList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridMatList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 이메일 송부 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_EmailUser(string strPlantCode, string strQualNo, string strLang)
        {
            try
            {
                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualEMailUser), "QATNewQualEMailUser");
                QRPQAT.BL.QATQUL.QATNewQualEMailUser clsEmail = new QRPQAT.BL.QATQUL.QATNewQualEMailUser();
                brwChannel.mfCredentials(clsEmail);

                DataTable dtEmail = clsEmail.mfReadQATNewQualEMailUser(strPlantCode, strQualNo, strLang);

                this.uGridEmail.SetDataBinding(dtEmail, "");

                if (dtEmail.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    //wGrid.mfSetAutoResizeColWidth(this.uGridEmail, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        #region Events...

        // ContentsArea 상태변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;

                    this.uGridQualList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;

                    for (int i = 0; i < this.uGridQualList.Rows.Count; i++)
                    {
                        this.uGridQualList.Rows[i].Fixed = false;
                    }

                    Clear();

                    this.uGridQualList.Height = 675;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크 이벤트
        private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked)
                {
                    if (this.uOptionFinalInspectResultFlag.CheckedIndex.Equals(-1))
                    {
                        // System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001243"
                                                , "M001162"
                                                , Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                        return;
                    }
                    else if (this.uDateFinalCompleteDate.Value == null || this.uDateFinalCompleteDate.Value == DBNull.Value || string.IsNullOrEmpty(this.uDateFinalCompleteDate.Value.ToString()))
                    {
                        // System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001160"
                                                , "M001161"
                                                , Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                        this.uDateFinalCompleteDate.DropDown();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Button Event

        // EvidenceList 행삭제 버튼 이벤트
        private void uButtonDel_EvidenceList_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    // 첨부파일 저장경로정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0012");

                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridEvidenceList.Rows[i].Cells["Check"].Value).Equals(true))
                        {
                            if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uComboPlant.Value.ToString()) &&
                                this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextQualNo.Text) &&
                                !this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                            {
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString());

                                this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value = string.Empty;
                                this.uGridEvidenceList.Rows[i].Cells["FilePath"].SetValue(string.Empty, false);
                            }
                            this.uGridEvidenceList.Rows[i].Hidden = true;
                        }
                    }

                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                            , arrFile
                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileUploadNoProgView();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // EvidenceList 첨부파일 다운로드 버튼 이벤트
        private void uButtonFileDown_EvidenceList_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridEvidenceList.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\")
                            || this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        // 화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                        // 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0012");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGridEvidenceList.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridEvidenceList.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (!this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\") &&
                                    !string.IsNullOrEmpty(this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString()))
                                {
                                    arrFile.Add(this.uGridEvidenceList.Rows[i].Cells["FilePath"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        if (arrFile.Count > 0)
                        {
                            fileAtt.ShowDialog();
                        }

                        // 폴더 열기
                        System.Diagnostics.Process.Start(strSaveFolder);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Material 행삭제 버튼 이벤트
        private void uButtonDel_MatList_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridMatList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridMatList.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridMatList.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Package 행삭제 버튼 이벤트
        private void uButtonDel_PackageList_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridPackageList.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridPackageList.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region TextBox Event

        // 등록자ID 텍스트박스 버튼클릭 이벤트
        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextWriteID.ReadOnly == false)
                {
                    frmPOP0011 frmUsr = new frmPOP0011();
                    frmUsr.PlantCode = this.uComboPlant.Value.ToString();
                    frmUsr.ShowDialog();

                    this.uTextWriteID.Text = frmUsr.UserID;
                    this.uTextWriteName.Text = frmUsr.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자ID 텍스트박스 사번입력시 명 가져오는 이벤트
        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextWriteID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextWriteID.Text != "")
                        {
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();

                            String strUserID = this.uTextWriteID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextWriteID.Text = "";
                                this.uTextWriteName.Text = "";
                            }
                            else
                            {
                                this.uTextWriteName.Text = strRtnUserName;
                            }
                        }
                    }
                    else if(e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                    {
                        if (this.uTextWriteID.TextLength <= 1 || this.uTextWriteID.SelectedText == this.uTextWriteID.Text)
                        {
                            this.uTextWriteName.Text = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객사코드 텍스트박스 버튼 클릭이벤트
        private void uTextCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextCustomerCode.ReadOnly == false)
                {
                    frmPOP0003 frmCustomer = new frmPOP0003();
                    frmCustomer.ShowDialog();

                    this.uTextCustomerCode.Text = frmCustomer.CustomerCode;
                    this.uTextCustomerName.Text = frmCustomer.CustomerName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 검색조건 : 고객사코드 텍스트박스 버튼클릭 이벤트
        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmCustomer = new frmPOP0003();
                frmCustomer.ShowDialog();

                this.uTextSearchCustomerCode.Text = frmCustomer.CustomerCode;
                this.uTextSearchCustomerName.Text = frmCustomer.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ComboBox Event

        // 구분 콤보박스 값 변경 이벤트
        private void uComboQualType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // 상태에 따른 Visible 상태 설정 메소드 호출
                SetQualTypeState(this.uComboQualType.Value.ToString());
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보박스 값 변경 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                while (this.uGridPackageList.Rows.Count > 0)
                {
                    this.uGridPackageList.Rows[0].Delete(false);
                }

                while (this.uGridMatList.Rows.Count > 0)
                {
                    this.uGridMatList.Rows[0].Delete(false);
                }

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChnnel = new QRPBrowser();

                // 변수 설정
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLang = m_resSys.GetString("SYS_LANG");

                //// 자재
                //brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                //QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                //brwChnnel.mfCredentials(clsMat);

                //DataTable dtMat = clsMat.mfReadMASMaterialComboForNewQual(strPlantCode, strLang);

                //wGrid.mfSetGridColumnValueList(this.uGridMatList, 0, "MaterialCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtMat);

                //자재 종류
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsType = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChnnel.mfCredentials(clsType);

                DataTable dtCon = clsType.mfReadMASConsumableTypeCombo(strLang);
                wGrid.mfSetGridColumnValueList(this.uGridMatList, 0, "ConsumableTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCon);

                // Package
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChnnel.mfCredentials(clsProduct);

                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, strLang);

                wGrid.mfSetGridColumnValueList(this.uGridPackageList, 0, "Package", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPackage);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Grid Event

        /// <summary>
        /// 헤더그리드 더블클릭시 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridQualList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strQualNo = e.Row.Cells["QualNo"].Value.ToString();

                this.uComboPlant.Value = strPlantCode;
                this.uTextQualNo.Text = strQualNo;
                PlantCode = strPlantCode;

                // 조회 메소드 호출
                Search_HeaderInfo(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));
                Search_PackageList(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));
                Search_MaterialList(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));
                Search_EvidenceList(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));
                Search_EmailUser(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));

                // 완료여부에 따른 컨트롤 활성화 정의 메소드 호출
                if (this.uCheckCompleteFlag.Enabled)
                {
                    SetWritable();
                }
                else
                {
                    SetDisWritable();
                }

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // EvidenceList CellButton Event(첨부파일, 등록자ID)
        private void uGridEvidenceList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (e.Cell.Column.Key.Equals("FilePath"))
                    {
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        openFile.Filter = "모든 파일 (*.*)|*.*"; //excel files (*.xlsx)|*.xlsx";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string strFile = openFile.FileName;
                            e.Cell.Value = strFile.ToString();
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("WriteUserID"))
                    {
                        frmPOP0011 frmUsr = new frmPOP0011();
                        frmUsr.PlantCode = this.uComboPlant.Value.ToString();
                        frmUsr.ShowDialog();

                        e.Cell.Value = frmUsr.UserID;
                        e.Cell.Row.Cells["WriteUserName"].Value = frmUsr.UserName;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // EvidenceList Cell Double Click Event
        private void uGridEvidenceList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FilePath")
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        ////DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        ////                "확인창", "입력확인", "다운로드가 가능한 첨부파일이 아닙니다.",
                        ////                Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0012");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());
                        }

                        #region 첨부파일 저장경로 설정 요청에 의한 주석처리
                        /*
                        //화일서버 연결정보 가져오기
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0012");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        if (e.Cell.Value.ToString().Contains(":\\") == false)
                        {
                            arrFile.Add(e.Cell.Value.ToString());
                        }

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();



                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                         * */
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // EvidenceList Key Down 이벤트
        private void uGridEvidenceList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell CurrentCell;

                if (this.uGridEvidenceList.ActiveCell == null)
                    return;
                else
                    CurrentCell = this.uGridEvidenceList.ActiveCell;

                // 아직 작성완료한 상태가 아니면
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (CurrentCell.Column.Key.Equals("FilePath"))
                    {
                        // 삭제를 하는경우
                        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                        {
                            // 아직 첨부파일이 올라간 상태가 아니면 셀 Text만 삭제
                            if (CurrentCell.Value.ToString().Contains(":\\"))
                            {
                                CurrentCell.SetValue(string.Empty, false);
                            }
                            else if (CurrentCell.Value.ToString().Contains(this.uComboPlant.Value.ToString()) &&
                                    CurrentCell.Value.ToString().Contains(this.uTextQualNo.Text))
                            {
                                // 첨부파일이 서버에 올라간경우 첨부파일 삭제
                                //화일서버 연결정보 가져오기
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                                //첨부파일 저장경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0012");

                                //첨부파일 Download하기
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + CurrentCell.Value.ToString());

                                fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrFile
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.mfFileUploadNoProgView();

                                CurrentCell.SetValue(string.Empty, false);
                            }
                        }
                    }
                    else if (CurrentCell.Column.Key.Equals("WriteUserID"))
                    {
                        if (string.IsNullOrEmpty(CurrentCell.Text) || CurrentCell.Value == null || CurrentCell.Value == DBNull.Value)
                        {
                            return;
                        }
                        else
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();

                                String strUserID = CurrentCell.Text;

                                // UserName 검색 함수 호출
                                String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                                if (strRtnUserName == "")
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000621",
                                                Infragistics.Win.HAlign.Right);

                                    CurrentCell.SetValue(string.Empty, false);
                                    CurrentCell.Row.Cells["WriteUserName"].SetValue(string.Empty, false);
                                }
                                else
                                {
                                    CurrentCell.Row.Cells["WriteUserName"].Value = strRtnUserName;
                                }
                            }
                            else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                            {
                                if (CurrentCell.SelLength <= 1 || CurrentCell.SelText == CurrentCell.Text)
                                {
                                    CurrentCell.Row.Cells["WriteUserName"].SetValue(string.Empty, false);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 고객사코드로 검색
        //검색장
        private void uTextSearchCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    string strCustomerCode = this.uTextSearchCustomerCode.Text;

                    DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                    if (dtCustomer.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        this.uTextSearchCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                    }
                }
                else if (e.KeyCode.Equals(Keys.Delete) || e.KeyCode.Equals(Keys.Back))
                {
                    this.uTextSearchCustomerCode.Text = "";
                    this.uTextSearchCustomerName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //헤더 정보
        private void uTextCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    string strCustomerCode = this.uTextCustomerCode.Text;

                    DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                    if (dtCustomer.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                    }
                }
                else if (e.KeyCode.Equals(Keys.Delete) || e.KeyCode.Equals(Keys.Back))
                {
                    this.uTextCustomerCode.Text = "";
                    this.uTextCustomerName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        /// <summary>
        /// 원자재 그리드 셀 변경 이벤트
        /// 자재 종류를 선택하면 해당 자재 종류의 원자재 코드를 선택하고, 원자재 코드를 선택하면 자재명을 뿌려줌
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridMatList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                WinGrid wGrid = new WinGrid();
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");
                if (e.Cell.Column.Key == "ConsumableTypeCode")
                {
                    string strConsumableTypeCode = e.Cell.Value.ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                    QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                    brwChannel.mfCredentials(clsMat);

                    DataTable dtMat = clsMat.mfReadMASMaterialComboForNewQual(strPlantCode, strConsumableTypeCode, strLang);

                    wGrid.mfSetGridCellValueList(this.uGridMatList, e.Cell.Row.Index,"MaterialCode", "", "선택", dtMat);
                }
                else if (e.Cell.Column.Key == "MaterialCode")
                {
                    string strMaterialCode = e.Cell.Value.ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                    QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                    brwChannel.mfCredentials(clsMat);

                    DataTable dtMat = clsMat.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, strLang);

                    if (dtMat.Rows.Count > 0)
                    {
                        e.Cell.Row.Cells["MaterialName"].Value = dtMat.Rows[0]["MaterialName"].ToString();
                    }
                    else
                    {
                        e.Cell.Row.Cells["MaterialName"].Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            { 
            }
        }

        #endregion

        private void uGridMatList_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "MaterialCode" || e.Cell.Column.Key == "ConsumableTypeCode")
                {
                    this.uGridMatList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDel_Email_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridEmail.Rows[i].Cells["Check"].Value))
                            this.uGridEmail.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (e.Cell.Column.Key.Equals("UserID"))
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        if (this.uComboPlant.Value.ToString().Equals(string.Empty) || this.uComboPlant.SelectedIndex.Equals(0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                            return;
                        }

                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboPlant.Value.ToString();
                        frmPOP.ShowDialog();


                        if (frmPOP.UserID == null || frmPOP.UserID.Equals(string.Empty))
                            return;

                        if (this.uComboPlant.Value.ToString() != frmPOP.PlantCode)
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000798",m_resSys.GetString("SYS_LANG"))
                                                        , msg.GetMessge_Text("M000275",m_resSys.GetString("SYS_LANG"))
                                                        , msg.GetMessge_Text("M001254",m_resSys.GetString("SYS_LANG"))
                                                            + this.uComboPlant.Text + msg.GetMessge_Text("M000009",m_resSys.GetString("SYS_LANG"))
                                                        , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["UserID"].Value = string.Empty;
                            e.Cell.Row.Cells["UserName"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptCode"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptName"].Value = string.Empty;
                            e.Cell.Row.Cells["EMail"].Value = string.Empty;
                        }
                        else
                        {
                            e.Cell.Row.Cells["UserID"].Value = frmPOP.UserID;
                            e.Cell.Row.Cells["UserName"].Value = frmPOP.UserName;
                            e.Cell.Row.Cells["DeptName"].Value = frmPOP.DeptName;
                            e.Cell.Row.Cells["DeptCode"].Value = frmPOP.DeptCode;
                            e.Cell.Row.Cells["EMail"].Value = frmPOP.EMail;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (!uCell.Text.Equals(string.Empty))
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                                // BL 연결
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                brwChannel.mfCredentials(clsUser);

                                DataTable dtUserInfo = clsUser.mfReadSYSUser("", uCell.Text, m_resSys.GetString("SYS_LANG"));

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    uCell.Row.Cells["UserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                    uCell.Row.Cells["DeptCode"].Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                                    uCell.Row.Cells["DeptName"].Value = dtUserInfo.Rows[0]["DeptName"].ToString();
                                    uCell.Row.Cells["EMail"].Value = dtUserInfo.Rows[0]["Email"].ToString();
                                }
                                else
                                {
                                    DialogResult Result = new DialogResult();
                                    WinMessageBox msg = new WinMessageBox();
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001095", "M000366", "M000368"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (uCell.Text.Length <= 1 || uCell.SelText == uCell.Text)
                            {
                                uCell.Row.Delete(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridEmail, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 이메일 전송 메소드
        /// </summary>
        private void SendMail(string strQualNo)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                //{
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    DataTable dtUserInfo = GetUserInfo(strPlantCode, this.uTextWriteID.Text);

                    for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                    {

                        if (!string.IsNullOrEmpty(this.uGridEmail.Rows[i].Cells["EMail"].Value.ToString()) &&
                            this.uGridEmail.Rows[i].Cells["Email"].Value.ToString().Contains(@"@bokwang.com"))
                        {

                            #region Ecidence파일첨부
                            #region BL호출 및 연결정보,경로 저장
                            //메일보내기
                            QRPBrowser brwChannel = new QRPBrowser();
                            //첨부파일을 위한 FileServer 연결정보
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                            //메일 발송 첨부파일 경로
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFileDestPath);
                            DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                            //신제품 Qual관리 첨부파일 저장경로
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysTargetFilePath);
                            DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0012");

                            #endregion

                            QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                            ArrayList arrFile = new ArrayList();        // 복사할 파일서버경로및 파일명
                            ArrayList arrAttachFile = new ArrayList();  // 파일을 복사할 경로
                            ArrayList arrFileNonPath = new ArrayList(); // 첨부할 파일명 저장

                            //파일이 첨부되었을 경우
                            if (this.uGridEvidenceList.Rows.Count > 0)
                            {
                                string strFileName = "";
                                for (int j = 0; j < this.uGridEvidenceList.Rows.Count; j++)
                                {
                                    if (!this.uGridEvidenceList.Rows[j].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                                    {
                                        //해당첨부파일 텍스트의 경로가 있는 경우 경로를 뺀 파일명만 찾아서 저장한다.
                                        if (this.uGridEvidenceList.Rows[j].Cells["FilePath"].Value.ToString().Contains(":\\"))
                                        {
                                            FileInfo fileDoc = new FileInfo(this.uGridEvidenceList.Rows[j].Cells["FilePath"].Value.ToString());
                                            strFileName = this.uComboPlant.Value.ToString() + "-" + strQualNo + "-" +
                                                            this.uGridEvidenceList.Rows[j].RowSelectorNumber + "-" + fileDoc.Name;
                                        }
                                        else
                                            strFileName = this.uGridEvidenceList.Rows[j].Cells["FilePath"].Value.ToString();

                                        string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                                        string TargetFile = "QATNewQualFile/" + strFileName;
                                        string NonPathFile = strFileName;


                                        arrAttachFile.Add(TargetFile);
                                        arrFile.Add(DestFile);
                                        arrFileNonPath.Add(NonPathFile);

                                    }
                                }

                            }
                            if (arrAttachFile.Count > 0)
                            {
                                //메일에 첨부할 파일을 서버에서 찾아서 서버에 있는 메일 첨부파일폴더로 복사한다.
                                fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                    , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrAttachFile
                                                                    , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                    , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                      dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            }

                            #endregion

                            brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                            QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                            brwChannel.mfCredentials(clsmail);

                            //첨부파일 메일 첨부파일 경로로 복사
                            if (arrFile.Count != 0)
                            {
                                fileAtt.mfFileUpload_NonAssync();
                            }

                            #region Email Lang

                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strDefault = string.Empty;
                            string[] strName = new string[9];

                            if (strLang.Equals("KOR"))
                            {
                                strDefault = "적용제품,변경전,변경후,[QRP].Qualification 완료 메일,관리번호,구분,고객사,목적,완료일";
                            }
                            else if (strLang.Equals("CHN"))
                            {
                                strDefault = "适用产品,变更前,变更后,[QRP].Qualification 完成 邮箱,管理编号,类别,客户社,目的,完成日";
                            }
                            else if (strLang.Equals("ENG"))
                            {
                                strDefault = "적용제품,변경전,변경후,[QRP].Qualification 완료 메일,관리번호,구분,고객사,목적,완료일";
                            }

                            strName = strDefault.Split(',');
                            #endregion

                            #region 적용제품, 변경전 후

                            //적용제품
                            string strPackage = string.Empty;
                            
                            if (this.uGridPackageList.Rows.Count > 0)
                            {
                                for (int k = 0; k < this.uGridPackageList.Rows.Count; k++)
                                {
                                    if (!this.uGridPackageList.Rows[k].Cells["Package"].Value.ToString().Equals(string.Empty))
                                    {
                                        if (strPackage.Equals(string.Empty))
                                            strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strName[0] +"</SPAN> : " + this.uGridPackageList.Rows[k].Cells["Package"].Value.ToString();
                                        else
                                            strPackage = strPackage + " , " + this.uGridPackageList.Rows[k].Cells["Package"].Value.ToString();
                                    }
                                }
                                if (!strPackage.Equals(string.Empty))
                                    strPackage = strPackage + "</P>";
                            }

                            //변경전,후
                            string strPCNB = "";
                            string strPCNA = "";

                            if (this.uLabelChangeBefore.Visible)
                            {
                                if (this.uComboQualType.Value.ToString().Equals("2") || this.uComboQualType.Text.Equals("PCN")) // 구분콤보가 PCN인경우만 변경전, 후 메일양식에 추가
                                {
                                    strPCNB = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[1] + "</SPAN>&nbsp&nbsp&nbsp&nbsp : " + this.uTextChangeBefore.Text + "</P>";
                                    strPCNA = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strName[2] +"</SPAN>&nbsp&nbsp&nbsp&nbsp : " + this.uTextChangeAfter.Text + "</P>";
                                }
                            }
                            #endregion

                            bool bolRtn = clsmail.mfSendSMTPMail(this.uComboPlant.Value.ToString()
                                                            , dtUserInfo.Rows[0]["EMail"].ToString()    //m_resSys.GetString("SYS_EMAIL")
                                                            , dtUserInfo.Rows[0]["UserName"].ToString()    //m_resSys.GetString("SYS_USERNAME")
                                                            , this.uGridEmail.Rows[i].Cells["EMail"].Value.ToString()       //보내려는 사람 이메일주소
                                                            , strName[3]
                                                            , "<HTML>" +
                                                                "<HEAD>" +
                                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                "</HEAD>" +
                                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[4] + "</SPAN> : " + strQualNo + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[5] + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + this.uComboQualType.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[6] + "</SPAN>&nbsp&nbsp&nbsp&nbsp: " + this.uTextCustomerName.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[7] + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + this.uTextPurpose.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strName[8] + "</SPAN>&nbsp&nbsp&nbsp&nbsp: " + Convert.ToDateTime(this.uDateFinalCompleteDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                strPackage + strPCNB + strPCNA +
                                                                "</BODY>" +
                                                                "</HTML>"
                                                            , arrFileNonPath);



                            if (!bolRtn)
                            {
                                WinMessageBox msg = new WinMessageBox();

                                msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M000083", m_resSys.GetString("SYS_LANG"))
                                        , msg.GetMessge_Text("M001453", m_resSys.GetString("SYS_LANG"))
                                        , msg.GetMessge_Text("M001454", m_resSys.GetString("SYS_LANG"))
                                          + this.uGridEmail.Rows[i].Cells["UserName"].Value.ToString() + msg.GetMessge_Text("M000005", m_resSys.GetString("SYS_LANG")),
                                        Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;

            }
            finally
            {
                dtUser.Dispose();
            }
        }

        /// <summary>
        /// E-Mail Test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonEmailTest_Click(object sender, EventArgs e)
        {
            if (this.uTextQualNo.Text.Equals(string.Empty))
                return;

            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001449", "M001448",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                return;

            SendMail(this.uTextQualNo.Text);


        }



    }
}