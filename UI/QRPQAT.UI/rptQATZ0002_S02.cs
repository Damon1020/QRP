﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace QRPQAT.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_03.
    /// </summary>
    public partial class rptQATZ0002_S02 : DataDynamics.ActiveReports.ActiveReport
    {
        
        public rptQATZ0002_S02()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
        public rptQATZ0002_S02(DataTable dtCondition, int intRow)
        {
            // 화면 비율 맞추기.
            int intCnt;
            DataRow dr;
            if (intRow == 1)   
            {
                if (dtCondition.Rows.Count.Equals(0) || dtCondition.Rows.Count < 6)
                {
                    intCnt = 6 - dtCondition.Rows.Count;

                    for (int i = 0; i < intCnt; i++)
                    {
                        dr = dtCondition.NewRow();
                        dtCondition.Rows.Add(dr);
                    }

                }
                
            }
            else
            {
                if (dtCondition.Rows.Count == 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        dr = dtCondition.NewRow();
                        dtCondition.Rows.Add(dr);
                    }
                }

                if (6 - intRow > 0)
                {
                    // 15 - 11 - 1 = -1
                    intCnt = 6 - intRow - dtCondition.Rows.Count;
                    if (intCnt > 0)
                    {
                        for (int i = 0; i < intCnt; i++)
                        {
                            dr = dtCondition.NewRow();
                            dtCondition.Rows.Add(dr);
                        }
                    }
                }

            }
            this.DataSource = dtCondition;

            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
               
        }
    }
}
