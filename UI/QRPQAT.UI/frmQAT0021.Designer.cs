﻿namespace QRPQAT.UI
{
    partial class frmQAT0021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQAT0021));
            this.uGridMSADataList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchProjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProject = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckAccuracyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckStabilityFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTabData = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uLabelMeasureCount = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTolerance = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAccuracyValue = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPartCount = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepeatCount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRRValueChangeValue = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAccuracyValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTolerance = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPartCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRRValueChangeValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReapeatCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMeasureTool = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlantCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProjectName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProjectCode = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMSADataList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabData)).BeginInit();
            this.uTabData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAccuracyValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPartCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRRValueChangeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReapeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridMSADataList
            // 
            this.uGridMSADataList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMSADataList.DisplayLayout.Appearance = appearance2;
            this.uGridMSADataList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMSADataList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMSADataList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMSADataList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridMSADataList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMSADataList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridMSADataList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMSADataList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMSADataList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMSADataList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridMSADataList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMSADataList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMSADataList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMSADataList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridMSADataList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMSADataList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMSADataList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridMSADataList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridMSADataList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMSADataList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridMSADataList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridMSADataList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMSADataList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridMSADataList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMSADataList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMSADataList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMSADataList.Location = new System.Drawing.Point(0, 80);
            this.uGridMSADataList.Name = "uGridMSADataList";
            this.uGridMSADataList.Size = new System.Drawing.Size(1060, 760);
            this.uGridMSADataList.TabIndex = 5;
            this.uGridMSADataList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridMSADataList_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProjectName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProject);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlantCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uTextSearchProjectName
            // 
            this.uTextSearchProjectName.Location = new System.Drawing.Point(392, 12);
            this.uTextSearchProjectName.Name = "uTextSearchProjectName";
            this.uTextSearchProjectName.Size = new System.Drawing.Size(224, 21);
            this.uTextSearchProjectName.TabIndex = 147;
            // 
            // uLabelSearchProject
            // 
            this.uLabelSearchProject.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchProject.Name = "uLabelSearchProject";
            this.uLabelSearchProject.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchProject.TabIndex = 148;
            this.uLabelSearchProject.Text = "1";
            // 
            // uDateSearchInspectDateTo
            // 
            this.uDateSearchInspectDateTo.Location = new System.Drawing.Point(856, 12);
            this.uDateSearchInspectDateTo.Name = "uDateSearchInspectDateTo";
            this.uDateSearchInspectDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateTo.TabIndex = 128;
            // 
            // uDateSearchInspectDateFrom
            // 
            this.uDateSearchInspectDateFrom.Location = new System.Drawing.Point(740, 12);
            this.uDateSearchInspectDateFrom.Name = "uDateSearchInspectDateFrom";
            this.uDateSearchInspectDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateFrom.TabIndex = 128;
            // 
            // uComboSearchPlantCode
            // 
            this.uComboSearchPlantCode.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlantCode.Name = "uComboSearchPlantCode";
            this.uComboSearchPlantCode.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchPlantCode.TabIndex = 127;
            this.uComboSearchPlantCode.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(624, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchInspectDate.TabIndex = 126;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(840, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 146;
            this.ultraLabel1.Text = "~";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProcessName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckAccuracyFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckStabilityFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabData);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTolerance);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAccuracyValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPartCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRepeatCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRRValueChangeValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAccuracyValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTolerance);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPartCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRRValueChangeValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReapeatCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProcessCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureTool);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterial);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProjectName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProjectCode);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextProcessName
            // 
            appearance75.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Appearance = appearance75;
            this.uTextProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Location = new System.Drawing.Point(744, 12);
            this.uTextProcessName.Name = "uTextProcessName";
            this.uTextProcessName.ReadOnly = true;
            this.uTextProcessName.Size = new System.Drawing.Size(110, 21);
            this.uTextProcessName.TabIndex = 163;
            // 
            // uTextPlantName
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance46;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(436, 12);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(110, 21);
            this.uTextPlantName.TabIndex = 162;
            // 
            // uCheckAccuracyFlag
            // 
            this.uCheckAccuracyFlag.Location = new System.Drawing.Point(892, 108);
            this.uCheckAccuracyFlag.Name = "uCheckAccuracyFlag";
            this.uCheckAccuracyFlag.Size = new System.Drawing.Size(152, 20);
            this.uCheckAccuracyFlag.TabIndex = 161;
            this.uCheckAccuracyFlag.Text = "정확성(Accuracy) 검사";
            // 
            // uCheckStabilityFlag
            // 
            this.uCheckStabilityFlag.Location = new System.Drawing.Point(748, 108);
            this.uCheckStabilityFlag.Name = "uCheckStabilityFlag";
            this.uCheckStabilityFlag.Size = new System.Drawing.Size(140, 20);
            this.uCheckStabilityFlag.TabIndex = 160;
            this.uCheckStabilityFlag.Text = "안정성(Stability)검사";
            // 
            // uTabData
            // 
            this.uTabData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabData.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabData.Location = new System.Drawing.Point(12, 140);
            this.uTabData.Name = "uTabData";
            this.uTabData.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabData.Size = new System.Drawing.Size(1032, 540);
            this.uTabData.TabIndex = 159;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1028, 517);
            // 
            // uLabelMeasureCount
            // 
            this.uLabelMeasureCount.Location = new System.Drawing.Point(304, 36);
            this.uLabelMeasureCount.Name = "uLabelMeasureCount";
            this.uLabelMeasureCount.Size = new System.Drawing.Size(125, 20);
            this.uLabelMeasureCount.TabIndex = 144;
            this.uLabelMeasureCount.Text = "계측기수";
            // 
            // uLabelTolerance
            // 
            this.uLabelTolerance.Location = new System.Drawing.Point(304, 84);
            this.uLabelTolerance.Name = "uLabelTolerance";
            this.uLabelTolerance.Size = new System.Drawing.Size(125, 20);
            this.uLabelTolerance.TabIndex = 158;
            this.uLabelTolerance.Text = "Tol";
            // 
            // uLabelAccuracyValue
            // 
            this.uLabelAccuracyValue.Location = new System.Drawing.Point(12, 84);
            this.uLabelAccuracyValue.Name = "uLabelAccuracyValue";
            this.uLabelAccuracyValue.Size = new System.Drawing.Size(125, 20);
            this.uLabelAccuracyValue.TabIndex = 146;
            this.uLabelAccuracyValue.Text = "정확성";
            // 
            // uLabelPartCount
            // 
            this.uLabelPartCount.Location = new System.Drawing.Point(304, 60);
            this.uLabelPartCount.Name = "uLabelPartCount";
            this.uLabelPartCount.Size = new System.Drawing.Size(125, 20);
            this.uLabelPartCount.TabIndex = 147;
            this.uLabelPartCount.Text = "측정부품";
            // 
            // uLabelRepeatCount
            // 
            this.uLabelRepeatCount.Location = new System.Drawing.Point(12, 60);
            this.uLabelRepeatCount.Name = "uLabelRepeatCount";
            this.uLabelRepeatCount.Size = new System.Drawing.Size(125, 20);
            this.uLabelRepeatCount.TabIndex = 150;
            this.uLabelRepeatCount.Text = "측정반복";
            // 
            // uTextInspectDesc
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectDesc.Appearance = appearance44;
            this.uTextInspectDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectDesc.Location = new System.Drawing.Point(436, 108);
            this.uTextInspectDesc.Name = "uTextInspectDesc";
            this.uTextInspectDesc.ReadOnly = true;
            this.uTextInspectDesc.Size = new System.Drawing.Size(304, 21);
            this.uTextInspectDesc.TabIndex = 155;
            // 
            // uLabelRRValueChangeValue
            // 
            this.uLabelRRValueChangeValue.Location = new System.Drawing.Point(12, 108);
            this.uLabelRRValueChangeValue.Name = "uLabelRRValueChangeValue";
            this.uLabelRRValueChangeValue.Size = new System.Drawing.Size(125, 20);
            this.uLabelRRValueChangeValue.TabIndex = 142;
            this.uLabelRRValueChangeValue.Text = "RR설정";
            // 
            // uTextAccuracyValue
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAccuracyValue.Appearance = appearance71;
            this.uTextAccuracyValue.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAccuracyValue.Location = new System.Drawing.Point(144, 84);
            this.uTextAccuracyValue.Name = "uTextAccuracyValue";
            this.uTextAccuracyValue.ReadOnly = true;
            this.uTextAccuracyValue.Size = new System.Drawing.Size(110, 21);
            this.uTextAccuracyValue.TabIndex = 91;
            // 
            // uTextInspectItemDesc
            // 
            appearance45.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemDesc.Appearance = appearance45;
            this.uTextInspectItemDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemDesc.Location = new System.Drawing.Point(744, 84);
            this.uTextInspectItemDesc.Name = "uTextInspectItemDesc";
            this.uTextInspectItemDesc.ReadOnly = true;
            this.uTextInspectItemDesc.Size = new System.Drawing.Size(228, 21);
            this.uTextInspectItemDesc.TabIndex = 91;
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(860, 60);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(110, 21);
            this.uTextMaterialName.TabIndex = 91;
            // 
            // uTextMaterialCode
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance72;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(744, 60);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextMaterialCode.TabIndex = 91;
            // 
            // uTextProjectName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectName.Appearance = appearance16;
            this.uTextProjectName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectName.Location = new System.Drawing.Point(144, 36);
            this.uTextProjectName.Name = "uTextProjectName";
            this.uTextProjectName.Size = new System.Drawing.Size(110, 21);
            this.uTextProjectName.TabIndex = 91;
            // 
            // uTextTolerance
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTolerance.Appearance = appearance55;
            this.uTextTolerance.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTolerance.Location = new System.Drawing.Point(436, 84);
            this.uTextTolerance.Name = "uTextTolerance";
            this.uTextTolerance.ReadOnly = true;
            this.uTextTolerance.Size = new System.Drawing.Size(110, 21);
            this.uTextTolerance.TabIndex = 91;
            // 
            // uTextPartCount
            // 
            appearance73.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPartCount.Appearance = appearance73;
            this.uTextPartCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPartCount.Location = new System.Drawing.Point(436, 60);
            this.uTextPartCount.Name = "uTextPartCount";
            this.uTextPartCount.ReadOnly = true;
            this.uTextPartCount.Size = new System.Drawing.Size(110, 21);
            this.uTextPartCount.TabIndex = 91;
            // 
            // uTextRRValueChangeValue
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRRValueChangeValue.Appearance = appearance56;
            this.uTextRRValueChangeValue.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRRValueChangeValue.Location = new System.Drawing.Point(144, 108);
            this.uTextRRValueChangeValue.Name = "uTextRRValueChangeValue";
            this.uTextRRValueChangeValue.ReadOnly = true;
            this.uTextRRValueChangeValue.Size = new System.Drawing.Size(110, 21);
            this.uTextRRValueChangeValue.TabIndex = 91;
            // 
            // uTextMeasureCount
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureCount.Appearance = appearance47;
            this.uTextMeasureCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureCount.Location = new System.Drawing.Point(436, 36);
            this.uTextMeasureCount.Name = "uTextMeasureCount";
            this.uTextMeasureCount.ReadOnly = true;
            this.uTextMeasureCount.Size = new System.Drawing.Size(110, 21);
            this.uTextMeasureCount.TabIndex = 91;
            // 
            // uTextReapeatCount
            // 
            appearance76.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReapeatCount.Appearance = appearance76;
            this.uTextReapeatCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReapeatCount.Location = new System.Drawing.Point(144, 60);
            this.uTextReapeatCount.Name = "uTextReapeatCount";
            this.uTextReapeatCount.ReadOnly = true;
            this.uTextReapeatCount.Size = new System.Drawing.Size(110, 21);
            this.uTextReapeatCount.TabIndex = 91;
            // 
            // uTextProjectCode
            // 
            appearance79.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Appearance = appearance79;
            this.uTextProjectCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Location = new System.Drawing.Point(144, 12);
            this.uTextProjectCode.Name = "uTextProjectCode";
            this.uTextProjectCode.ReadOnly = true;
            this.uTextProjectCode.Size = new System.Drawing.Size(110, 21);
            this.uTextProjectCode.TabIndex = 91;
            // 
            // uTextMeasureToolName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Appearance = appearance21;
            this.uTextMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Location = new System.Drawing.Point(860, 36);
            this.uTextMeasureToolName.Name = "uTextMeasureToolName";
            this.uTextMeasureToolName.ReadOnly = true;
            this.uTextMeasureToolName.Size = new System.Drawing.Size(110, 21);
            this.uTextMeasureToolName.TabIndex = 91;
            // 
            // uTextMeasureToolCode
            // 
            appearance80.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolCode.Appearance = appearance80;
            this.uTextMeasureToolCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolCode.Location = new System.Drawing.Point(744, 36);
            this.uTextMeasureToolCode.Name = "uTextMeasureToolCode";
            this.uTextMeasureToolCode.ReadOnly = true;
            this.uTextMeasureToolCode.Size = new System.Drawing.Size(110, 21);
            this.uTextMeasureToolCode.TabIndex = 91;
            // 
            // uLabelProcessCode
            // 
            this.uLabelProcessCode.Location = new System.Drawing.Point(612, 12);
            this.uLabelProcessCode.Name = "uLabelProcessCode";
            this.uLabelProcessCode.Size = new System.Drawing.Size(125, 20);
            this.uLabelProcessCode.TabIndex = 145;
            this.uLabelProcessCode.Text = "6";
            // 
            // uLabelMeasureTool
            // 
            this.uLabelMeasureTool.Location = new System.Drawing.Point(612, 36);
            this.uLabelMeasureTool.Name = "uLabelMeasureTool";
            this.uLabelMeasureTool.Size = new System.Drawing.Size(125, 20);
            this.uLabelMeasureTool.TabIndex = 146;
            this.uLabelMeasureTool.Text = "계측기";
            // 
            // uLabelInspectDesc
            // 
            this.uLabelInspectDesc.Location = new System.Drawing.Point(304, 108);
            this.uLabelInspectDesc.Name = "uLabelInspectDesc";
            this.uLabelInspectDesc.Size = new System.Drawing.Size(125, 20);
            this.uLabelInspectDesc.TabIndex = 153;
            this.uLabelInspectDesc.Text = "설명";
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(612, 60);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(125, 20);
            this.uLabelMaterial.TabIndex = 148;
            this.uLabelMaterial.Text = "자재";
            // 
            // uLabelInspectItem
            // 
            this.uLabelInspectItem.Location = new System.Drawing.Point(612, 84);
            this.uLabelInspectItem.Name = "uLabelInspectItem";
            this.uLabelInspectItem.Size = new System.Drawing.Size(125, 20);
            this.uLabelInspectItem.TabIndex = 152;
            this.uLabelInspectItem.Text = "검사항목";
            // 
            // uLabelPlantCode
            // 
            this.uLabelPlantCode.Location = new System.Drawing.Point(304, 12);
            this.uLabelPlantCode.Name = "uLabelPlantCode";
            this.uLabelPlantCode.Size = new System.Drawing.Size(125, 20);
            this.uLabelPlantCode.TabIndex = 140;
            this.uLabelPlantCode.Text = "5";
            // 
            // uLabelProjectName
            // 
            this.uLabelProjectName.Location = new System.Drawing.Point(12, 36);
            this.uLabelProjectName.Name = "uLabelProjectName";
            this.uLabelProjectName.Size = new System.Drawing.Size(125, 20);
            this.uLabelProjectName.TabIndex = 139;
            this.uLabelProjectName.Text = "4";
            // 
            // uLabelProjectCode
            // 
            this.uLabelProjectCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelProjectCode.Name = "uLabelProjectCode";
            this.uLabelProjectCode.Size = new System.Drawing.Size(125, 20);
            this.uLabelProjectCode.TabIndex = 138;
            this.uLabelProjectCode.Text = "3";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQAT0021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridMSADataList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0021";
            this.Load += new System.EventHandler(this.frmQAT0021_Load);
            this.Activated += new System.EventHandler(this.frmQAT0021_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQAT0021_FormClosing);
            this.Resize += new System.EventHandler(this.frmQAT0021_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMSADataList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabData)).EndInit();
            this.uTabData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAccuracyValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPartCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRRValueChangeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReapeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMSADataList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureTool;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItem;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProjectName;
        private Infragistics.Win.Misc.UltraLabel uLabelProjectCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureCount;
        private Infragistics.Win.Misc.UltraLabel uLabelTolerance;
        private Infragistics.Win.Misc.UltraLabel uLabelRepeatCount;
        private Infragistics.Win.Misc.UltraLabel uLabelPartCount;
        private Infragistics.Win.Misc.UltraLabel uLabelAccuracyValue;
        private Infragistics.Win.Misc.UltraLabel uLabelRRValueChangeValue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTolerance;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPartCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRRValueChangeValue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReapeatCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolName;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabData;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAccuracyValue;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAccuracyFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStabilityFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProjectName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
    }
}