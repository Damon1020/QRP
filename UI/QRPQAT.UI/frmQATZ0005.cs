﻿/*-----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                           */
/* 모듈(분류)명 : 검교정 관리                                            */
/* 프로그램ID   : frmQATZ0005.cs                                         */
/* 프로그램명   : 외부교정 의뢰                                          */
/* 작성자       : 이종호                                                 */
/* 작성일자     : 2011-08-16                                             */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                       */
/*                2011-09-19 : ~~~~~ 추가 (권종구)                       */ 
/*                2011-10-24 : 필수체크수정, 다운로드파일 실행 추가(정결)*/
/*-----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

//--파일업로드를위한 참조
using System.IO;


namespace QRPQAT.UI
{
    public partial class frmQATZ0005 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0005()
        {
            InitializeComponent();
        }

        private void frmQATZ0005_Activated(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 툴바 활성화 여부 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0005_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정

            titleArea.mfSetLabelText("외부교정 의뢰", m_resSys.GetString("SYS_FONTNAME"), 12);

            this.uGroupBoxContentsArea.Expanded = false;

            SetToolAuth();
            // 컨트롤 초기화 Method 호출
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitButton();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "의뢰자 입력", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "접수자 입력", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "교정자 입력", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox5, GroupBoxType.LIST, "완료자 입력", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchManageNo, "교정만료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchState, "상태", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelManageNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityType, "교정유형", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMakerCompany, "제조회사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelModelName, "모델명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelUseDept, "사용부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStorageDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLatelyCorrectDate, "최근교정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLatelyExpireDate, "교정만료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelState, "상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNextVerityDate, "차기교정일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelFile, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestUserID, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestFlag, "의뢰여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptUserID, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptFlag, "접수여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityUserID, "교정자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityDate, "교정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityFlag, "교정여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteUserID, "완료자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteDate, "완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "완료여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                ////System ResourceInfo
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //QRPCOM.QRPUI.WinButton wBtn = new WinButton();

                //wBtn.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장 콤보박스
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 검색조건

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "PlantCode", "PlantName", dtPlant);

                // 입력
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "PlantCode", "PlantName", dtPlant);

                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                // 상태 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtState = clsComCode.mfReadCommonCode("C0024", m_resSys.GetString("SYS_LANG"));

                // 검색조건

                wCombo.mfSetComboEditor(this.uComboSearchState, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "ComCode", "ComCodeName", dtState);

                // 입력
                wCombo.mfSetComboEditor(this.uComboState, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtState);

                //교정유형

                DataTable dtVerityType = clsComCode.mfReadCommonCode("C0040",m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboVerityType,true,false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista,m_resSys.GetString("SYS_FONTNAME")
                    ,true,false,"",true, Infragistics.Win.DropDownResizeHandleStyle.Default,true,100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtVerityType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridOutCorrectionList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "OutVerityNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityNumber", "일련번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityTarget", "교정대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityTargetName", "교정대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityTargetCode", "교정대상코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityTargetCodeName", "교정대상명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "MeasureToolCode", "계측기코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "MeasureToolName", "계측기명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "ModelName", "Model", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "MakerCompany", "제조회사", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "AcquireDeptName", "사용부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "AcquireDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "LatelyVerityDate", "최근교정일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 110, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "EndVerityDate", "교정만료일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 110, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityStatus", "상태", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityStatusName", "상태", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityType", "교정유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "VerityTypeName", "교정유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 6
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOutCorrectionList, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridOutCorrectionList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridOutCorrectionList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridOutCorrectionList.Height = 740;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System ResourceeeInfo;
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // ExPandGroupbox가 펼쳐져잇으면 닫는다
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //Expand안의 컨트롤 초기화
                InitControl();

                if (Convert.ToDateTime(this.uDateFromDate.Value) > Convert.ToDateTime(this.uDateToDate.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000773", Infragistics.Win.HAlign.Right);

                    this.uDateFromDate.DropDown();
                    return;
                }

                //-- 검색 조건 저장 --//
                string strPlantCode = this.uComboSearchPlant.Value.ToString();


                string strTarget = this.uOptionSearchCheck.Value == null ? "" : this.uOptionSearchCheck.Value.ToString();

                string strMeaTool = "";

                if (!strTarget.Equals(string.Empty))
                {
                    if (!this.uTextSearchMeasureToolCode.Text.Equals(string.Empty) && !this.uTextSearchMeasureToolName.Text.Equals(string.Empty))
                        strMeaTool = this.uTextSearchMeasureToolCode.Text;
                }

                string strFromDate = Convert.ToDateTime(this.uDateFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateToDate.Value).ToString("yyyy-MM-dd");
                string strState = this.uComboSearchState.Value.ToString();

                // -- 팝업창 띄우기 -- //
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();

                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                // -- 커서변경 -- //
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                //-- BL호출 --//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.OutVerity), "OutVerity");
                QRPQAT.BL.QATVRT.OutVerity clsOutVerity = new QRPQAT.BL.QATVRT.OutVerity();
                brwChannel.mfCredentials(clsOutVerity);

                // 매서드호출
                DataTable dtOutVerity = clsOutVerity.mfReadOutVerity_Target(strPlantCode, strTarget, strMeaTool, strFromDate, strToDate, strState, m_resSys.GetString("SYS_LANG"));

                // -- 그리드에 바인드 --//
                this.uGridOutCorrectionList.DataSource = dtOutVerity;
                this.uGridOutCorrectionList.DataBind();
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtOutVerity.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridOutCorrectionList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourcesInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수 입력 사항
                //---------------------------------- 필수 입력 사항 확인 ---------------------------------

                if (this.uCheckCompleteFlag.Enabled.Equals(false) && this.uCheckCompleteFlag.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000825", "M000824", Infragistics.Win.HAlign.Right);

                    //Dropdown
                    this.uComboPlant.DropDown();
                    return;

                }

                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    //Dropdown
                    this.uComboPlant.DropDown();
                    return;

                }
                if(this.uTextMeasureToolCode.Text == "" || this.uTextMeasureToolName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001275", Infragistics.Win.HAlign.Right);
                    // Focus
                    this.uTextMeasureToolCode.Focus();
                    return;
                }
                if (this.uDateNextVerity.Value == null || this.uDateNextVerity.Value.ToString().Equals(string.Empty))
                {
                     msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M001230", "M001322", Infragistics.Win.HAlign.Right);

                     //DropDowm
                     this.uDateNextVerity.DropDown();
                     return;
                }
                if (this.uCheckRequestFlag.Checked)
                {
                    if (this.uTextRequestUserID.Text.Equals(string.Empty) || this.uTextRequestUserName.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000842", Infragistics.Win.HAlign.Right);

                        this.uTextRequestUserID.Focus();
                        return;
                    }
                    if (this.uDateRequestDate.Value == null || this.uDateRequestDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000840", Infragistics.Win.HAlign.Right);

                        this.uDateRequestDate.DropDown();
                        return;
                    }
                }
                if (this.uCheckReceiptFlag.Checked)
                {
                    if (this.uTextReceiptUserID.Text.Equals(string.Empty) || this.uTextReceiptUserName.Text.Equals(string.Empty))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001071", Infragistics.Win.HAlign.Right);

                        this.uTextReceiptUserID.Focus();
                        return;
                    }
                    if (this.uDateReceiptDate.Value == null || this.uDateReceiptDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001069", Infragistics.Win.HAlign.Right);

                        this.uDateRequestDate.DropDown();
                        return;
                    }
                }
                if (this.uCheckVerityFlag.Checked)
                {
                    if (this.uTextVerityUserID.Text.Equals(string.Empty))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001277", Infragistics.Win.HAlign.Right);

                        this.uTextVerityUserID.Focus();
                        return;
                    }
                    if (this.uDateVerityDate.Value == null || this.uDateVerityDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001276", Infragistics.Win.HAlign.Right);

                        this.uDateVerityDate.DropDown();
                        return;
                    }
                }
                if (this.uCheckCompleteFlag.Checked)
                {
                    if (this.uTextCompleteUserID.Text.Equals(string.Empty) || this.uTextCompleteUserName.Text.Equals(string.Empty))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001302", Infragistics.Win.HAlign.Right);

                        this.uTextCompleteUserID.Focus();
                        return;
                    }
                    if (this.uDateCompleteDate.Value == null || this.uDateCompleteDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000826", Infragistics.Win.HAlign.Right);

                        this.uDateCompleteDate.DropDown();
                        return;
                    }
                }
                if (this.uTextFileUpload.Text.Contains("/")
                    || this.uTextFileUpload.Text.Contains("#")
                    || this.uTextFileUpload.Text.Contains("*")
                    || this.uTextFileUpload.Text.Contains("<")
                    || this.uTextFileUpload.Text.Contains(">"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                #endregion

                // -------------- BL호출 -------------- //
                QRPCOM.QRPGLO.QRPBrowser brwChannel =new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.OutVerity),"OutVerity");
                QRPQAT.BL.QATVRT.OutVerity clsOutVerity = new QRPQAT.BL.QATVRT.OutVerity();
                brwChannel.mfCredentials(clsOutVerity);

                //-------- 컬럼 셋팅된 매서드 호출 --------//
                DataTable dtOutVerity = clsOutVerity.mfSetDataInfo();

                #region 정보저장
                //-- 데이터 테이블에 값 사입
                DataRow drOutVerity;
                drOutVerity = dtOutVerity.NewRow();
                drOutVerity["PlantCode"] = this.uComboPlant.Value.ToString();
                drOutVerity["OutVerityNo"] = this.uTextOutVerityNo.Text;
                drOutVerity["VerityNumber"] = this.uTextVerityNumber.Text;
                drOutVerity["VerityType"] = this.uComboVerityType.Value.ToString();
                drOutVerity["EtcDesc"] = this.uTextEtcDesc.Text;
                drOutVerity["VerityStatus"] = this.uComboState.Value.ToString();
                drOutVerity["EndVerityDate"] = Convert.ToDateTime(this.uDateEndVerityDate.Value).ToString("yyyy-MM-dd");

                drOutVerity["VerityTarget"] = this.uOptionCheck.Value;
                drOutVerity["VerityTargetCode"] = this.uTextMeasureToolCode.Text;
                drOutVerity["NextVerityDate"] = this.uDateNextVerity.DateTime.Date.ToString("yyyy-MM-dd");
                drOutVerity["LatelyVerityDate"] = this.uTextLatelyVerityDate.Text;

                //의뢰자
                drOutVerity["RequestUserID"] = this.uTextRequestUserID.Text;
                drOutVerity["RequestDate"] = this.uDateRequestDate.Value == null ? "" : this.uDateRequestDate.DateTime.Date.ToString("yyyy-MM-dd");
                drOutVerity["RequestFlag"] = this.uCheckRequestFlag.Checked == true ? "T" : "F";
                drOutVerity["RequestDesc"] = this.uTextRequestDesc.Text;
                //접수자
                drOutVerity["ReceiptUserID"] = this.uTextReceiptUserID.Text;
                drOutVerity["ReceiptDate"] = this.uDateReceiptDate.Value != null ? this.uDateReceiptDate.DateTime.Date.ToString("yyyy-MM-dd") : "";
                drOutVerity["ReceiptFlag"] = this.uCheckReceiptFlag.Checked == true ? "T" : "F";
                drOutVerity["ReceiptDesc"] = this.uTextReceiptDesc.Text;
                //교정자
                drOutVerity["VerityUserID"] = this.uTextVerityUserID.Text;
                drOutVerity["VerityDate"] = this.uDateVerityDate.Value != null ? this.uDateVerityDate.DateTime.Date.ToString("yyyy-MM-dd") : "";
                drOutVerity["VerityFlag"] = this.uCheckVerityFlag.Checked == true ? "T" : "F";
                drOutVerity["VerityDesc"] = this.uTextVerityDesc.Text;
                //완료자
                drOutVerity["CompleteUserID"] = this.uTextCompleteUserID.Text;
                drOutVerity["CompleteDate"] =this.uDateCompleteDate.Value != null ? Convert.ToDateTime(this.uDateCompleteDate.Value).ToString("yyyy-MM-dd") : "";
                drOutVerity["CompleteFlag"] = this.uCheckCompleteFlag.Checked == true ? "T" : "F";
                drOutVerity["CompleteDesc"] = this.uTextCompleteDesc.Text;

                string strServer = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                if (strServer.Contains("10.61.61.71") || strServer.Contains("10.61.61.73"))
                    drOutVerity["SendMDM"] = "T";


                dtOutVerity.Rows.Add(drOutVerity);

                

                //-- 파일업로드 폼을 참조함
                frmCOMFileAttach FileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                //--파일 경로 받을 변수 선언
                string strUpLoadFile = "";

                // 파일첨부란에 :\\ 있을경우 신규나 수정된 파일로인식
                if (this.uTextFileUpload.Text.Contains(":\\"))
                {
                    strUpLoadFile = "NE";

                    FileInfo FileInfo = new FileInfo(this.uTextFileUpload.Text);

                    dtOutVerity.Rows[0]["FileName"] = "-" + FileInfo.Name;
                }
                //경로가 없는경우
                else
                {
                    dtOutVerity.Rows[0]["FileName"] = this.uTextFileUpload.Text;
                }

                #endregion

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                //----- 저장 여부 메세지 박스를 띄움 Yes를 누르면 실행 --------.////
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 팝업창을 띄운다 //
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                    // 커서를 변경한다 .
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 매서드 실행
                    string strErrRtn = clsOutVerity.mfSaveOutVerity(dtOutVerity, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    #region 파일 업로드 정보 저장
                    // 파일 서버 연결정보가져오기 //
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);

                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    //외부교정 의뢰 파일서버의 저장경로 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSystemFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSystemFilePath);

                    DataTable dtSystemFilePath = clsSystemFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0018");


                    if (strUpLoadFile != "")
                    {
                        string strOutVerityNo = ErrRtn.mfGetReturnValue(0);
                        string strVerityNumber = ErrRtn.mfGetReturnValue(1);

                        //파일이름 변경하기 관리번호 + 일련번호 + 파일명으로 하여 복사하기 //
                        FileInfo FileInfo = new FileInfo(this.uTextFileUpload.Text);

                        strUpLoadFile = FileInfo.DirectoryName + "\\" + strOutVerityNo + "-" + strVerityNumber + "-" + FileInfo.Name;

                        //변경한 파일이 있으면 삭제
                        if (File.Exists(strUpLoadFile))
                            File.Delete(strUpLoadFile);

                        //변경한 파일이름으로 복사
                        File.Copy(this.uTextFileUpload.Text, strUpLoadFile);
                        arrFile.Add(strUpLoadFile);

                    }
                    #endregion

                    //-- 커서변경 --//
                    this.MdiParent.Cursor = Cursors.Default;

                    // -- 팝업창 닫기 --
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        if (strUpLoadFile != "")
                        {
                            //upLoad정보 설정
                            FileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                         dtSystemFilePath.Rows[0]["ServerPath"].ToString(),
                                                                         dtSystemFilePath.Rows[0]["FolderName"].ToString(),
                                                                         dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                         dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            FileAtt.ShowDialog();
                        }
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strErr = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErr = msg.GetMessge_Text("M000953",m_resSys.GetString("SYS_LANG"));
                        else
                            strErr = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                     , msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                     , msg.GetMessge_Text("M001037",m_resSys.GetString("SYS_LANG")), strErr,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //ResoureceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // 관리번호가없거나 ExpandGroup박스가 숨겨져있으면 삭제할 정보가 없다 //
                if (this.uTextOutVerityNo.Text == "" || this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled.Equals(false))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000882", "M000993", Infragistics.Win.HAlign.Right);

                    return;
                }

                //-- 삭제를 위한 값 저장 
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strOutVerityNo = this.uTextOutVerityNo.Text;
                string strVerityNumber = this.uTextVerityNumber.Text;

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // -- 팝업창 을 띄운다 --
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                    //-- 커서를 변경한다 --
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.OutVerity), "OutVerity");
                    QRPQAT.BL.QATVRT.OutVerity clsOutVerity = new QRPQAT.BL.QATVRT.OutVerity();
                    brwChannel.mfCredentials(clsOutVerity);

                    //매서드호출
                    string strErrRtn = clsOutVerity.mfDeleteOutVerity(strPlantCode, strOutVerityNo, strVerityNumber);

                    //Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////
                    //-- 커서 기본값으로 변경 --//
                    this.MdiParent.Cursor = Cursors.Default;
                    // - 팝업창을 닫는다 - //
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 처리결과에 따라 메세지박스를 보여준다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                //그리드 
                //-- 전체 행을 선택 하여 삭제한다 --//
                this.uGridOutCorrectionList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridOutCorrectionList.Rows.All);
                this.uGridOutCorrectionList.DeleteSelectedRows(false);

                InitControl();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridOutCorrectionList);

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events...

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 175);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridOutCorrectionList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridOutCorrectionList.Height = 720;

                    for (int i = 0; i < this.uGridOutCorrectionList.Rows.Count; i++)
                    {
                        this.uGridOutCorrectionList.Rows[i].Fixed = false;
                    }
                    InitControl();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 컬럼 변경된것 저장하기 위한 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmQATZ0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        

        /// <summary>
        /// 외부교정 의뢰그리드 더블클릭 시 그리드 값이 ExpandGroupBox 안으로 들어간다
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridOutCorrectionList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                WinGrid grd = new WinGrid();
                //-- 셀이 공백이 아니면 실행 한다 --//
                if (!grd.mfCheckCellDataInRow(this.uGridOutCorrectionList, 0, e.Cell.Row.Index))
                {
                    //-- ExpandGroupBox 가 숨김상태면 펼쳐주고 클릭한 셀의 줄을 고정시킨다 --//
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                        e.Cell.Row.Fixed = true;
                    }
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // -- 팝업창 띄우기 -- //
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();

                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                    // -- 커서변경 -- //
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString(); //공장
                    string strOutVerityNo = e.Cell.Row.Cells["OutVerityNo"].Value.ToString();     //관리번호
                    string strVerityNumber = e.Cell.Row.Cells["VerityNumber"].Value.ToString();    //일련번호
                    string strTarget = e.Cell.Row.Cells["VerityTarget"].Value.ToString();

                    //처리 로직//
                    //-- BL호출 --//
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.OutVerity), "OutVerity");
                    QRPQAT.BL.QATVRT.OutVerity clsOutVerity = new QRPQAT.BL.QATVRT.OutVerity();
                    brwChannel.mfCredentials(clsOutVerity);

                    // 매서드호출
                    DataTable dtOutVerity = clsOutVerity.mfReadOutVerity_Detail(strPlantCode, strOutVerityNo, strVerityNumber, strTarget, m_resSys.GetString("SYS_LANG"));

                    if (dtOutVerity.Rows.Count > 0)
                    {
                        this.uComboPlant.Value = strPlantCode;
                        this.uTextOutVerityNo.Text = strOutVerityNo;
                        this.uTextVerityNumber.Text = strVerityNumber;
                        //                    this.uComboVerityType.Value = e.Cell.Row.Cells["VerityType"].Value.ToString();    //교정유형

                        this.uOptionCheck.Value = dtOutVerity.Rows[0]["VerityTarget"].ToString() == "" ? null : dtOutVerity.Rows[0]["VerityTarget"]; //교정대상
                        this.uTextMeasureToolCode.Text = dtOutVerity.Rows[0]["VerityTargetCode"].ToString(); //계측기코드
                        this.uTextMeasureToolName.Text = dtOutVerity.Rows[0]["VerityTargetCodeName"].ToString(); //계측기명
                        this.uTextMakerCompany.Text = dtOutVerity.Rows[0]["MakerCompany"].ToString();    //제조회사
                        this.uTextUseDept.Text = dtOutVerity.Rows[0]["AcquireDeptName"].ToString();         //사용부서
                        this.uTextModelName.Text = dtOutVerity.Rows[0]["ModelName"].ToString();       //모델명
                        this.uTextSerialNo.Text = dtOutVerity.Rows[0]["SerialNo"].ToString();        //SerialNo
                        this.uComboState.Value = dtOutVerity.Rows[0]["VerityStatus"].ToString();         //상태
                        this.uTextGDDate.Text = dtOutVerity.Rows[0]["AcquireDate"].ToString();          //입고일
                        this.uTextLatelyVerityDate.Text = dtOutVerity.Rows[0]["LatelyVerityDate"].ToString();   //최근교정일
                        this.uDateEndVerityDate.Value = dtOutVerity.Rows[0]["EndVerityDate"].ToString();       //교정만료일
                        this.uTextEtcDesc.Text = dtOutVerity.Rows[0]["EtcDesc"].ToString();            //비고
                        this.uDateNextVerity.Value = dtOutVerity.Rows[0]["NextVerityDate"].ToString(); //차기교정일
                        this.uTextFileUpload.Text = dtOutVerity.Rows[0]["FileName"].ToString();       //첨부파일

                        this.uTextRequestUserID.Text = dtOutVerity.Rows[0]["RequestUserID"].ToString();        //의뢰자
                        this.uTextRequestUserName.Text = dtOutVerity.Rows[0]["RequestUserName"].ToString();         //의뢰자
                        this.uDateRequestDate.Value = dtOutVerity.Rows[0]["RequestDate"].ToString();   //의뢰일
                        this.uTextRequestDesc.Text = dtOutVerity.Rows[0]["RequestDesc"].ToString();      //비고
                        this.uTextReceiptUserID.Text = dtOutVerity.Rows[0]["ReceiptUserID"].ToString(); //접수자
                        this.uTextReceiptUserName.Text = dtOutVerity.Rows[0]["ReceiptUserName"].ToString(); //접수자
                        this.uDateReceiptDate.Value = dtOutVerity.Rows[0]["ReceiptDate"].ToString();   //접수일
                        this.uTextReceiptDesc.Text = dtOutVerity.Rows[0]["ReceiptDesc"].ToString();       //비고
                        this.uTextVerityUserID.Text = dtOutVerity.Rows[0]["VerityUserID"].ToString(); //교정자
                        this.uTextVerityUserName.Text = dtOutVerity.Rows[0]["VerityUserName"].ToString(); //교정자
                        this.uDateVerityDate.Value = dtOutVerity.Rows[0]["VerityDate"].ToString();   //교정일
                        this.uTextVerityDesc.Text = dtOutVerity.Rows[0]["VerityDesc"].ToString();       //비고
                        this.uTextCompleteUserID.Text = dtOutVerity.Rows[0]["CompleteUserID"].ToString(); //완료자
                        this.uTextCompleteUserName.Text = dtOutVerity.Rows[0]["CompleteUserName"].ToString(); //완료자
                        this.uDateCompleteDate.Value = dtOutVerity.Rows[0]["CompleteDate"].ToString();   //완료일
                        this.uTextCompleteDesc.Text = dtOutVerity.Rows[0]["CompleteDesc"].ToString();       //비고

                        this.uCheckRequestFlag.Checked = dtOutVerity.Rows[0]["RequestFlag"].ToString() == "T" ? true : false; //의뢰여부
                        this.uCheckReceiptFlag.Checked = dtOutVerity.Rows[0]["ReceiptFlag"].ToString() == "T" ? true : false; //접수여부
                        this.uCheckVerityFlag.Checked = dtOutVerity.Rows[0]["VerityFlag"].ToString() == "T" ? true : false; //교정여부

                        //완료여부
                        if (dtOutVerity.Rows[0]["CompleteFlag"].ToString() == "T")
                        {
                            this.uCheckCompleteFlag.Enabled = false;
                            this.uCheckCompleteFlag.Checked = true; 
                        }
                        else
                        {
                            this.uCheckCompleteFlag.Enabled = true;
                            this.uCheckCompleteFlag.Checked = false; 
                        }

                        //수정불가처리
                        this.uComboPlant.ReadOnly = true;
                        this.uComboVerityType.ReadOnly = true;
                        this.uTextMeasureToolCode.ReadOnly = true;
                        this.uOptionCheck.Enabled = false;

                            
                    }

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 의뢰자 검색시 ENTER 누를시 사용자 자동 조회 기능
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRequestUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextRequestUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextRequestUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextRequestUserID.Text = "";
                            this.uTextRequestUserName.Text = "";
                        }
                        else
                        {
                            this.uTextRequestUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextRequestUserID.TextLength <= 1 || this.uTextRequestUserID.SelectedText == this.uTextRequestUserID.Text)
                    {
                        this.uTextRequestUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검색조건에 있는 계측기 텍스트의 엔터 버튼을 누를시 자동조회 기능 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
 
        private void uTextSearchMeasureToolCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //-- 백스페이스 나 Delete키를 누를시 해당 텍스가 공백이 된다 --.
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextSearchMeasureToolCode.Text = "";
                    this.uTextSearchMeasureToolName.Text = "";
                }
                ////-- 텍스트 박스에 내용이 있는상태에서 엔터키를 누를시 자동 조회가 된다. --//
                //if (e.KeyData == Keys.Enter && this.uTextSearchMeasureToolCode.Text.Trim() != "")
                //{
                //    //System ResourceInfo
                //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //    //변수 선언 및 값 저장 공장,계측기 코드
                //    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                //    string strMeasureToolCode = this.uTextSearchMeasureToolCode.Text;

                //    // 계측기 정보 가져오기
                //    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                //    QRPMAS.BL.MASQUA.MeasureTool clsMeasureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                //    brwChannel.mfCredentials(clsMeasureTool);

                //    DataTable dtMeasureTool = clsMeasureTool.mfReadMASMeasureToolPopup(strPlantCode, strMeasureToolCode, m_resSys.GetString("SYS_LANG"));

                //    if (dtMeasureTool.Rows.Count != 0)
                //    {
                //        this.uTextSearchMeasureToolName.Text = dtMeasureTool.Rows[0]["MeasureToolName"].ToString();
                //    }
                //    else
                //    {
                //        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                             "처리결과", "조회처리결과", "조회결과가 없습니다.",
                //                             Infragistics.Win.HAlign.Right);
                //        return;
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ExpandGroupBox안에 있는계측기텍스트의 엔터누를 시 자동조회 기능
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextMeasureToolCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //-- 백스페이스 나 Delete키를 누를시 해당 텍스가 공백이 된다 --.
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextMeasureToolCode.Text = "";
                    this.uTextMeasureToolName.Text = "";
                }
                ////-- 텍스트 박스에 내용이 있는상태에서 엔터키를 누를시 자동 조회가 된다. --//
                //if (e.KeyData == Keys.Enter && this.uTextMeasureToolCode.Text.Trim() != "")
                //{

                //    //System ResourceInfo
                //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //    if (this.uComboPlant.Value.ToString().Trim() == "")
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //               "확인창", "필수입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                //        //DropDown
                //        this.uComboPlant.DropDown();
                //        return;

                //    }

                //    //변수 선언 및 값 저장 공장,계측기 코드
                //    string strPlantCode = this.uComboPlant.Value.ToString();
                //    string strMeasureToolCode = this.uTextMeasureToolCode.Text;

                //    // 계측기 정보 가져오기
                //    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                //    QRPMAS.BL.MASQUA.MeasureTool clsMeasureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                //    brwChannel.mfCredentials(clsMeasureTool);

                //    DataTable dtMeasureTool = clsMeasureTool.mfReadMASMeasureToolPopup(strPlantCode, strMeasureToolCode, m_resSys.GetString("SYS_LANG"));

                //    if (dtMeasureTool.Rows.Count != 0)
                //    {
                //        this.uTextMeasureToolName.Text = dtMeasureTool.Rows[0]["MeasureToolName"].ToString();
                //        this.uTextModelName.Text = dtMeasureTool.Rows[0]["ModelName"].ToString();
                //        this.uTextSerialNo.Text = dtMeasureTool.Rows[0]["SerialNo"].ToString();
                //        this.uTextUseDept.Text = dtMeasureTool.Rows[0]["AcquireDeptName"].ToString();
                //        this.uTextMakerCompany.Text = dtMeasureTool.Rows[0]["MakerCompany"].ToString();
                //    }
                //    else
                //    {
                //        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                             "처리결과", "조회처리결과", "조회결과가 없습니다.",
                //                             Infragistics.Win.HAlign.Right);
                //        return;
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 의뢰자
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRequestUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextRequestUserID.Text = frmPOP.UserID;
                this.uTextRequestUserName.Text = frmPOP.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검색조건에 있는 계측기 텍스트박스의 에디터 버튼을 클릭 할 시계측기 정보 팝업을 띄운다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchMeasureToolCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();


                //옵션컨트롤이 선택이 안됐을 경우 메세지박스를 띄운다.
                if (this.uOptionSearchCheck.Value == null || this.uOptionSearchCheck.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000882", "M001275", Infragistics.Win.HAlign.Right);
                    return;
                }

                //-- 공장이 공백일시 메세지 박스를 띄우고 리턴 시킨다.--/
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }



                //--옵션컨트롤의 선택한 DataValue 저장
                string strCheck = this.uOptionSearchCheck.Value.ToString();

                //-- 공장 값저장  코드 , 명 , 변수 선언
                string strPlant = this.uComboSearchPlant.Value.ToString();
                string strCode = "";
                string strName = "";
                string strPOPPlant = "";

                //--계측기를 선택후 버튼 클릭시 팝업창을 띄운다.
                if (strCheck == "E")
                {
                    frmPOP0006 frmTool = new frmPOP0006();
                    frmTool.PlantCode = uComboSearchPlant.Value.ToString();
                    frmTool.ShowDialog();

                    strPOPPlant = frmTool.PlantCode;
                    strCode = frmTool.MeasureToolCode;
                    strName = frmTool.MeasureToolName;

                }
                //--설비를 선택 후 버튼 클릭시 팝업창을 띄운다.
                else if (strCheck == "M")
                {
                    frmPOP0005 frmEquip = new frmPOP0005();
                    frmEquip.PlantCode = uComboSearchPlant.Value.ToString();
                    frmEquip.ShowDialog();

                    strPOPPlant = frmEquip.PlantCode;
                    strCode = frmEquip.EquipCode;
                    strName = frmEquip.EquipName;

                }

                // 선택한 계측기의공장과 콤보 공장이 다를경우
                if (strPOPPlant != "" && strPlant != strPOPPlant)
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001264", "M000882", "M000686", Infragistics.Win.HAlign.Right);

                    return;
                }

                //--각컨트롤에 해당 정보를 넣는다.
                this.uTextSearchMeasureToolCode.Text = strCode;
                this.uTextSearchMeasureToolName.Text = strName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 계측기텍스트박스의 에디터 버튼을 클릭 할 시 계측기 정보 팝업을 띄운다
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextMeasureToolCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                if (this.uTextMeasureToolCode.ReadOnly == false)
                {
                    //옵션컨트롤이 선택이 안됐을 경우 메세지박스를 띄운다.
                    if (this.uOptionCheck.Value == null || this.uOptionCheck.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "M001264", "M000882", "M001275", Infragistics.Win.HAlign.Right);
                        //this.uOptionCheck.Select();
                        return;
                    }
                   
                    //-- 공장이 공백일시 메세지 박스를 띄우고 리턴 시킨다.--/
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                      "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }



                    //--옵션컨트롤의 선택한 DataValue 저장
                    string strCheck = this.uOptionCheck.Value.ToString();

                    //-- 공장 값저장  코드 , 명 , 변수 선언
                    string strPlant = this.uComboPlant.Value.ToString();
                    string strCode = "";
                    string strName = "";
                    string strModel = "";
                    string strSerial = "";
                    string strUseDept = "";
                    string strMakerCompany = "";
                    string strPOPPlant = "";
                    string strInDate = "";
                    string strLastInspectDate = "";

                    //--계측기를 선택후 버튼 클릭시 팝업창을 띄운다.
                    if (strCheck == "E")
                    {
                        frmPOP0006 frmTool = new frmPOP0006();
                        frmTool.PlantCode = uComboSearchPlant.Value.ToString();
                        frmTool.ShowDialog();

                        strPOPPlant = frmTool.PlantCode;
                        strCode = frmTool.MeasureToolCode;
                        strName = frmTool.MeasureToolName;
                        strModel = frmTool.ModelName;
                        strSerial = frmTool.SerialNo;
                        strInDate = frmTool.AcquireDate;
                        strMakerCompany = frmTool.MakerCompany;
                        strLastInspectDate = frmTool.LastInspectDate;
                    }
                    //--설비를 선택 후 버튼 클릭시 팝업창을 띄운다.
                    else if (strCheck == "M")
                    {
                        frmPOP0005 frmEquip = new frmPOP0005();
                        frmEquip.PlantCode = uComboSearchPlant.Value.ToString();
                        frmEquip.ShowDialog();

                        strPOPPlant = frmEquip.PlantCode;
                        strCode = frmEquip.EquipCode;
                        strName = frmEquip.EquipName;
                        strModel = frmEquip.ModelName;
                        strSerial = frmEquip.SerialNo;
                        strInDate = frmEquip.GRDate;
                        strMakerCompany = frmEquip.VendorName;
                    }
           
                    // 선택한 계측기의공장과 콤보 공장이 다를경우
                    if (strPOPPlant != "" && strPlant != strPOPPlant)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000686", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    //--각컨트롤에 해당 정보를 넣는다.
                    this.uTextMeasureToolCode.Text = strCode;
                    this.uTextMeasureToolName.Text = strName;
                    this.uTextModelName.Text = strModel;
                    this.uTextSerialNo.Text = strSerial;
                    this.uTextUseDept.Text = strUseDept;
                    this.uTextMakerCompany.Text = strMakerCompany;
                    this.uTextGDDate.Text = strInDate;
                    this.uTextLatelyVerityDate.Text = strLastInspectDate;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일 경로
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextFileUpload_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (!this.uCheckCompleteFlag.Enabled && this.uCheckCompleteFlag.Checked)
                    return;

                //업로드 이미지 클릭할 경우 업로드 할 파일 경로 창이 뜬다.
                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            //첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001139", "M001141", "M001451",
                                                Infragistics.Win.HAlign.Right);
                            return;
                        }
                        this.uTextFileUpload.Text = strImageFile;

                    }
                }
                else if (e.Button.Key.Equals("Down"))   //다운로드 이미지 클릭 시 해당 텍스트의 첨부화일을 다운받을 수 있다
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (this.uTextFileUpload.Text == "")
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }


                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0018");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();



                    arrFile.Add(this.uTextFileUpload.Text);


                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();



                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFileUpload.Text);

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextReceiptUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReceiptUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextReceiptUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextReceiptUserID.Text = "";
                            this.uTextReceiptUserName.Text = "";
                        }
                        else
                        {
                            this.uTextReceiptUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReceiptUserID.TextLength <= 1 || this.uTextRequestUserID.SelectedText == this.uTextRequestUserID.Text)
                    {
                        this.uTextReceiptUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextVerityUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextVerityUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextVerityUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextVerityUserID.Text = "";
                            this.uTextVerityUserName.Text = "";
                        }
                        else
                        {
                            this.uTextVerityUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextVerityUserID.TextLength <= 1 || this.uTextRequestUserID.SelectedText == this.uTextRequestUserID.Text)
                    {
                        this.uTextVerityUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextCompleteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCompleteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextCompleteUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextCompleteUserID.Text = "";
                            this.uTextCompleteUserName.Text = "";
                        }
                        else
                        {
                            this.uTextCompleteUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextCompleteUserID.TextLength <= 1 || this.uTextRequestUserID.SelectedText == this.uTextRequestUserID.Text)
                    {
                        this.uTextCompleteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmQATZ0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// ExpandGroupBox 안 컨트롤 초기화
        /// </summary>
        private void InitControl()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");            //공장
                if (this.uComboPlant.ReadOnly == true)
                { this.uComboPlant.ReadOnly = false; }
                
                this.uTextOutVerityNo.Text = "";    //관리번호
                this.uTextVerityNumber.Text = "";   //일련번호

                this.uComboVerityType.Value = "";   //교정유형
                if (this.uComboVerityType.ReadOnly == true) 
                { this.uComboVerityType.ReadOnly = false; }

                this.uTextMeasureToolCode.Text = ""; //계측기코드
                if (this.uTextMeasureToolCode.ReadOnly == true)
                { this.uTextMeasureToolCode.ReadOnly = false; }

                this.uTextMeasureToolName.Text = ""; //계측기 명
                this.uTextMakerCompany.Text = ""; //제조회사
                this.uTextUseDept.Text = "";        //사용부서
                this.uTextModelName.Text = "";      //모델명              
                this.uTextSerialNo.Text = "";       //SerialNo
                this.uComboState.Value = "";        //상태
                this.uTextGDDate.Text = "";     //입고일
                this.uTextLatelyVerityDate.Text = "";   //최근교정일
                this.uDateEndVerityDate.Value = DateTime.Now.Date;      //교정만료일
                this.uTextFileUpload.Text = "";     //첨부파일
                this.uTextRequestUserID.Text = ""; //의뢰자
                this.uTextRequestUserName.Text = ""; //의뢰자
                this.uDateRequestDate.Value = null;   //의뢰일
                this.uCheckRequestFlag.Checked = false; //의뢰여부
                this.uTextRequestDesc.Text = "";       //비고
                this.uTextReceiptUserID.Text = ""; //접수자
                this.uTextReceiptUserName.Text = ""; //접수자
                this.uDateReceiptDate.Value = null;   //접수일
                this.uCheckReceiptFlag.Checked = false ; //접수여부
                this.uTextReceiptDesc.Text = "";       //비고
                this.uTextVerityUserID.Text = ""; //교정자
                this.uTextVerityUserName.Text = ""; //교정자
                this.uDateVerityDate.Value = null;   //교정일
                this.uCheckVerityFlag.Checked = false ; //교정여부
                this.uTextVerityDesc.Text = "";       //비고
                this.uTextCompleteUserID.Text = ""; //완료자
                this.uTextCompleteUserName.Text = ""; //완료자
                this.uDateCompleteDate.Value = null;   //완료일
                this.uCheckCompleteFlag.Checked = false ; //완료여부
                this.uCheckCompleteFlag.Enabled = true; //완료여부
                this.uTextCompleteDesc.Text = "";       //비고
                this.uOptionCheck.Enabled = true; //검증대상
                this.uOptionCheck.Value = null; //검증대상
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 POPUP창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                Infragistics.Win.UltraWinEditors.UltraTextEditor utext = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                //사용자정보가 없는경우 Pass
                if (frmPOP.UserID == null || frmPOP.UserID.Equals(string.Empty))
                    return;

                if (utext.Name.Equals("uTextCompleteUserID"))
                {
                    this.uTextCompleteUserID.Text = frmPOP.UserID;
                    this.uTextCompleteUserName.Text = frmPOP.UserName;
                }
                else if (utext.Name.Equals("uTextReceiptUserID"))
                {
                    this.uTextReceiptUserID.Text = frmPOP.UserID;
                    this.uTextReceiptUserName.Text = frmPOP.UserName;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }


    }
}
