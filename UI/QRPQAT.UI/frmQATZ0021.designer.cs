﻿namespace QRPQAT.UI
{
    partial class frmQATZ0021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0021));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridSRRItemList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboTargetCompany = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uCheckM12 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM11 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM10 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM09 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM08 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM07 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM06 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM05 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM04 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM03 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM02 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckM01 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelAcceptCheck = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonLoad = new Infragistics.Win.Misc.UltraButton();
            this.uComboEvaluationPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEvaluationPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTargetCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerProdType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboCustomerProdType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uChartSRR = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uGridSRRList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTargetCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEvaluationPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomerProdType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartSRR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uTextSearchYear
            // 
            this.uTextSearchYear.Location = new System.Drawing.Point(424, 8);
            this.uTextSearchYear.Name = "uTextSearchYear";
            this.uTextSearchYear.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchYear.TabIndex = 13;
            this.uTextSearchYear.Text = "ultraTextEditor1";
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(320, 8);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 12;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(864, 8);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(760, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(112, 8);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchCustomer.TabIndex = 1;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(8, 8);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 0;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 650);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 200);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 650);
            this.uGroupBoxContentsArea.TabIndex = 4;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 630);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGridSRRItemList);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 56);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 574);
            this.ultraGroupBox2.TabIndex = 1;
            // 
            // uGridSRRItemList
            // 
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSRRItemList.DisplayLayout.Appearance = appearance17;
            this.uGridSRRItemList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSRRItemList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridSRRItemList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSRRItemList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSRRItemList.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSRRItemList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridSRRItemList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSRRItemList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSRRItemList.DisplayLayout.Override.CellAppearance = appearance18;
            this.uGridSRRItemList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSRRItemList.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance24.TextHAlignAsString = "Left";
            this.uGridSRRItemList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridSRRItemList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSRRItemList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridSRRItemList.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridSRRItemList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSRRItemList.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.uGridSRRItemList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSRRItemList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSRRItemList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSRRItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSRRItemList.Location = new System.Drawing.Point(3, 0);
            this.uGridSRRItemList.Name = "uGridSRRItemList";
            this.uGridSRRItemList.Size = new System.Drawing.Size(1058, 571);
            this.uGridSRRItemList.TabIndex = 1;
            this.uGridSRRItemList.Text = "ultraGrid1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uComboTargetCompany);
            this.ultraGroupBox1.Controls.Add(this.uCheckM12);
            this.ultraGroupBox1.Controls.Add(this.uCheckM11);
            this.ultraGroupBox1.Controls.Add(this.uCheckM10);
            this.ultraGroupBox1.Controls.Add(this.uCheckM09);
            this.ultraGroupBox1.Controls.Add(this.uCheckM08);
            this.ultraGroupBox1.Controls.Add(this.uCheckM07);
            this.ultraGroupBox1.Controls.Add(this.uCheckM06);
            this.ultraGroupBox1.Controls.Add(this.uCheckM05);
            this.ultraGroupBox1.Controls.Add(this.uCheckM04);
            this.ultraGroupBox1.Controls.Add(this.uCheckM03);
            this.ultraGroupBox1.Controls.Add(this.uCheckM02);
            this.ultraGroupBox1.Controls.Add(this.uCheckM01);
            this.ultraGroupBox1.Controls.Add(this.uLabelAcceptCheck);
            this.ultraGroupBox1.Controls.Add(this.uButtonLoad);
            this.ultraGroupBox1.Controls.Add(this.uComboEvaluationPeriod);
            this.ultraGroupBox1.Controls.Add(this.uLabelEvaluationPeriod);
            this.ultraGroupBox1.Controls.Add(this.uLabelTargetCompany);
            this.ultraGroupBox1.Controls.Add(this.uTextYear);
            this.ultraGroupBox1.Controls.Add(this.uLabelCustomerProdType);
            this.ultraGroupBox1.Controls.Add(this.uComboCustomerProdType);
            this.ultraGroupBox1.Controls.Add(this.uLabelYear);
            this.ultraGroupBox1.Controls.Add(this.uComboPlant);
            this.ultraGroupBox1.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 56);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // uComboTargetCompany
            // 
            this.uComboTargetCompany.Location = new System.Drawing.Point(736, 8);
            this.uComboTargetCompany.Name = "uComboTargetCompany";
            this.uComboTargetCompany.Size = new System.Drawing.Size(200, 21);
            this.uComboTargetCompany.TabIndex = 30;
            this.uComboTargetCompany.Text = "ultraComboEditor1";
            // 
            // uCheckM12
            // 
            this.uCheckM12.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM12.Location = new System.Drawing.Point(816, 32);
            this.uCheckM12.Name = "uCheckM12";
            this.uCheckM12.Size = new System.Drawing.Size(60, 20);
            this.uCheckM12.TabIndex = 29;
            this.uCheckM12.Text = "12月";
            // 
            // uCheckM11
            // 
            this.uCheckM11.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM11.Location = new System.Drawing.Point(752, 32);
            this.uCheckM11.Name = "uCheckM11";
            this.uCheckM11.Size = new System.Drawing.Size(60, 20);
            this.uCheckM11.TabIndex = 28;
            this.uCheckM11.Text = "11月";
            // 
            // uCheckM10
            // 
            this.uCheckM10.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM10.Location = new System.Drawing.Point(688, 32);
            this.uCheckM10.Name = "uCheckM10";
            this.uCheckM10.Size = new System.Drawing.Size(60, 20);
            this.uCheckM10.TabIndex = 27;
            this.uCheckM10.Text = "10月";
            // 
            // uCheckM09
            // 
            this.uCheckM09.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM09.Location = new System.Drawing.Point(624, 32);
            this.uCheckM09.Name = "uCheckM09";
            this.uCheckM09.Size = new System.Drawing.Size(60, 20);
            this.uCheckM09.TabIndex = 26;
            this.uCheckM09.Text = "9月";
            // 
            // uCheckM08
            // 
            this.uCheckM08.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM08.Location = new System.Drawing.Point(560, 32);
            this.uCheckM08.Name = "uCheckM08";
            this.uCheckM08.Size = new System.Drawing.Size(60, 20);
            this.uCheckM08.TabIndex = 25;
            this.uCheckM08.Text = "8月";
            // 
            // uCheckM07
            // 
            this.uCheckM07.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM07.Location = new System.Drawing.Point(496, 32);
            this.uCheckM07.Name = "uCheckM07";
            this.uCheckM07.Size = new System.Drawing.Size(60, 20);
            this.uCheckM07.TabIndex = 24;
            this.uCheckM07.Text = "7月";
            // 
            // uCheckM06
            // 
            this.uCheckM06.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM06.Location = new System.Drawing.Point(432, 32);
            this.uCheckM06.Name = "uCheckM06";
            this.uCheckM06.Size = new System.Drawing.Size(60, 20);
            this.uCheckM06.TabIndex = 23;
            this.uCheckM06.Text = "6月";
            // 
            // uCheckM05
            // 
            this.uCheckM05.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM05.Location = new System.Drawing.Point(368, 32);
            this.uCheckM05.Name = "uCheckM05";
            this.uCheckM05.Size = new System.Drawing.Size(60, 20);
            this.uCheckM05.TabIndex = 22;
            this.uCheckM05.Text = "5月";
            // 
            // uCheckM04
            // 
            this.uCheckM04.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM04.Location = new System.Drawing.Point(304, 32);
            this.uCheckM04.Name = "uCheckM04";
            this.uCheckM04.Size = new System.Drawing.Size(60, 20);
            this.uCheckM04.TabIndex = 21;
            this.uCheckM04.Text = "4月";
            // 
            // uCheckM03
            // 
            this.uCheckM03.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM03.Location = new System.Drawing.Point(240, 32);
            this.uCheckM03.Name = "uCheckM03";
            this.uCheckM03.Size = new System.Drawing.Size(60, 20);
            this.uCheckM03.TabIndex = 20;
            this.uCheckM03.Text = "3月";
            // 
            // uCheckM02
            // 
            this.uCheckM02.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM02.Location = new System.Drawing.Point(176, 32);
            this.uCheckM02.Name = "uCheckM02";
            this.uCheckM02.Size = new System.Drawing.Size(60, 20);
            this.uCheckM02.TabIndex = 19;
            this.uCheckM02.Text = "2月";
            // 
            // uCheckM01
            // 
            this.uCheckM01.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckM01.Location = new System.Drawing.Point(112, 32);
            this.uCheckM01.Name = "uCheckM01";
            this.uCheckM01.Size = new System.Drawing.Size(60, 20);
            this.uCheckM01.TabIndex = 18;
            this.uCheckM01.Text = "1月";
            // 
            // uLabelAcceptCheck
            // 
            this.uLabelAcceptCheck.Location = new System.Drawing.Point(8, 32);
            this.uLabelAcceptCheck.Name = "uLabelAcceptCheck";
            this.uLabelAcceptCheck.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptCheck.TabIndex = 17;
            this.uLabelAcceptCheck.Text = "ultraLabel1";
            // 
            // uButtonLoad
            // 
            this.uButtonLoad.Location = new System.Drawing.Point(944, 4);
            this.uButtonLoad.Name = "uButtonLoad";
            this.uButtonLoad.Size = new System.Drawing.Size(88, 28);
            this.uButtonLoad.TabIndex = 16;
            this.uButtonLoad.Text = "ultraButton1";
            // 
            // uComboEvaluationPeriod
            // 
            this.uComboEvaluationPeriod.Location = new System.Drawing.Point(984, 32);
            this.uComboEvaluationPeriod.Name = "uComboEvaluationPeriod";
            this.uComboEvaluationPeriod.Size = new System.Drawing.Size(20, 21);
            this.uComboEvaluationPeriod.TabIndex = 15;
            this.uComboEvaluationPeriod.Text = "ultraComboEditor1";
            // 
            // uLabelEvaluationPeriod
            // 
            this.uLabelEvaluationPeriod.Location = new System.Drawing.Point(960, 32);
            this.uLabelEvaluationPeriod.Name = "uLabelEvaluationPeriod";
            this.uLabelEvaluationPeriod.Size = new System.Drawing.Size(20, 20);
            this.uLabelEvaluationPeriod.TabIndex = 14;
            this.uLabelEvaluationPeriod.Text = "ultraLabel1";
            // 
            // uLabelTargetCompany
            // 
            this.uLabelTargetCompany.Location = new System.Drawing.Point(632, 8);
            this.uLabelTargetCompany.Name = "uLabelTargetCompany";
            this.uLabelTargetCompany.Size = new System.Drawing.Size(100, 20);
            this.uLabelTargetCompany.TabIndex = 12;
            this.uLabelTargetCompany.Text = "ultraLabel1";
            // 
            // uTextYear
            // 
            this.uTextYear.Location = new System.Drawing.Point(112, 8);
            this.uTextYear.Name = "uTextYear";
            this.uTextYear.Size = new System.Drawing.Size(200, 21);
            this.uTextYear.TabIndex = 11;
            this.uTextYear.Text = "ultraTextEditor1";
            // 
            // uLabelCustomerProdType
            // 
            this.uLabelCustomerProdType.Location = new System.Drawing.Point(320, 8);
            this.uLabelCustomerProdType.Name = "uLabelCustomerProdType";
            this.uLabelCustomerProdType.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerProdType.TabIndex = 10;
            this.uLabelCustomerProdType.Text = "ultraLabel1";
            // 
            // uComboCustomerProdType
            // 
            this.uComboCustomerProdType.Location = new System.Drawing.Point(424, 8);
            this.uComboCustomerProdType.Name = "uComboCustomerProdType";
            this.uComboCustomerProdType.Size = new System.Drawing.Size(200, 21);
            this.uComboCustomerProdType.TabIndex = 9;
            this.uComboCustomerProdType.Text = "ultraComboEditor1";
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(8, 8);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 8;
            this.uLabelYear.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(1036, 32);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboPlant.TabIndex = 7;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(1012, 32);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelPlant.TabIndex = 6;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uChartSRR
            // 
            this.uChartSRR.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartSRR.Axis.PE = paintElement1;
            this.uChartSRR.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartSRR.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartSRR.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartSRR.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X.LineThickness = 1;
            this.uChartSRR.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.X.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.X.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.X.Visible = true;
            this.uChartSRR.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartSRR.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartSRR.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.X2.Labels.Visible = false;
            this.uChartSRR.Axis.X2.LineThickness = 1;
            this.uChartSRR.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.X2.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.X2.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.X2.Visible = false;
            this.uChartSRR.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartSRR.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartSRR.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartSRR.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y.LineThickness = 1;
            this.uChartSRR.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Y.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Y.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.Y.TickmarkInterval = 50;
            this.uChartSRR.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.Y.Visible = true;
            this.uChartSRR.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartSRR.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartSRR.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartSRR.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Y2.Labels.Visible = false;
            this.uChartSRR.Axis.Y2.LineThickness = 1;
            this.uChartSRR.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.Y2.TickmarkInterval = 50;
            this.uChartSRR.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.Y2.Visible = false;
            this.uChartSRR.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartSRR.Axis.Z.Labels.ItemFormatString = "";
            this.uChartSRR.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z.LineThickness = 1;
            this.uChartSRR.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Z.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Z.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.Z.Visible = false;
            this.uChartSRR.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartSRR.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartSRR.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartSRR.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartSRR.Axis.Z2.Labels.Visible = false;
            this.uChartSRR.Axis.Z2.LineThickness = 1;
            this.uChartSRR.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartSRR.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartSRR.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartSRR.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartSRR.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartSRR.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartSRR.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartSRR.Axis.Z2.Visible = false;
            this.uChartSRR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartSRR.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartSRR.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartSRR.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartSRR.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartSRR.Dock = System.Windows.Forms.DockStyle.Top;
            this.uChartSRR.Effects.Effects.Add(gradientEffect1);
            this.uChartSRR.Location = new System.Drawing.Point(0, 80);
            this.uChartSRR.Name = "uChartSRR";
            this.uChartSRR.Size = new System.Drawing.Size(1070, 44);
            this.uChartSRR.TabIndex = 6;
            this.uChartSRR.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartSRR.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uGridSRRList
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSRRList.DisplayLayout.Appearance = appearance2;
            this.uGridSRRList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSRRList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridSRRList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridSRRList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSRRList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSRRList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSRRList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridSRRList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSRRList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSRRList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridSRRList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSRRList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridSRRList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridSRRList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSRRList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridSRRList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridSRRList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSRRList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridSRRList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSRRList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSRRList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSRRList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSRRList.Location = new System.Drawing.Point(0, 124);
            this.uGridSRRList.Name = "uGridSRRList";
            this.uGridSRRList.Size = new System.Drawing.Size(1070, 76);
            this.uGridSRRList.TabIndex = 7;
            this.uGridSRRList.Text = "ultraGrid1";
            // 
            // frmQATZ0021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridSRRList);
            this.Controls.Add(this.uChartSRR);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0021";
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTargetCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEvaluationPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomerProdType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uChartSRR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSRRItemList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelTargetCompany;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextYear;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProdType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomerProdType;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEvaluationPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelEvaluationPeriod;
        private Infragistics.Win.Misc.UltraButton uButtonLoad;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchYear;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.UltraWinChart.UltraChart uChartSRR;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSRRList;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptCheck;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM01;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM02;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM03;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM04;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM11;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM10;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM09;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM08;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM07;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM06;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM05;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckM12;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboTargetCompany;
    }
}