﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 환경유해물질관리                                      */
/* 프로그램ID   : frmQATZ0012.cs                                        */
/* 프로그램명   : UTC관리                                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-15 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQATZ0012 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0012()
        {
            InitializeComponent();
        }

        private void frmQATZ0012_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 여부 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true);
        }

        private void frmQATZ0012_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정
            titleArea.mfSetLabelText("공정 온습도관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화 Method 호출
            InitText();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitText()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchWriteDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장 콤보박스
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 검색조건
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strUserID = m_resSys.GetString("SYS_USERID");
                string strUserName = m_resSys.GetString("SYS_USERNAME");

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridUTCList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "UTCNo", "UTCNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "EquipLocCode", "위치", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", DateTime.Now.ToString("yyyy-MM-dd"));

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "WriteUserID", "등록", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", strUserID);

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", strUserName);

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "Temperature", "온도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "Humidity", "습도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "Particle", "Particle", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridUTCList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridUTCList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsLoc);

                DataTable dtLoc = clsLoc.mfReadLocationCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridUTCList, 0, "EquipLocCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtLoc);

                // SetFont Size
                this.uGridUTCList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridUTCList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈출추가
                wGrid.mfAddRowGrid(this.uGridUTCList, 0);

                this.uGridUTCList.Height = 740;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                // -- 값 저장 -- //
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strFromDate = Convert.ToDateTime(this.uDateSearchWriteFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateSearchWriteToDate.Value).ToString("yyyy-MM-dd");
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();

                // -- 팝업창을 띄운다 -- //
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                // 커서변경 
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UTC), "UTC");
                QRPQAT.BL.QATENV.UTC clsUTC = new QRPQAT.BL.QATENV.UTC();
                brwChannel.mfCredentials(clsUTC);

                // -- 매서드 호출 -- //
                DataTable dtUTC = clsUTC.mfReadUTC(strPlantCode, strEquipLocCode, strFromDate, strToDate);

                // -- - 데이터 바인드 ---//
                this.uGridUTCList.DataSource = dtUTC;
                this.uGridUTCList.DataBind();

                WinGrid wGrid = new WinGrid();

                // 위치콤보박스 설정을 위한 메소드 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsLoc);

                for (int i = 0; i < this.uGridUTCList.Rows.Count; i++)
                {
                    DataTable dtLoc = clsLoc.mfReadLocationCombo(this.uGridUTCList.Rows[i].Cells["PlantCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridCellValueList(this.uGridUTCList, i, "EquipLocCode", "", "", dtLoc);
                    this.uGridUTCList.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridUTCList.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;

                    //this.uGridUTCList.Rows[i].Cells["WriteDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //this.uGridUTCList.Rows[i].Cells["WriteUserName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }

                // -- 커서변경 -- //
                this.MdiParent.Cursor = Cursors.Default;

                // -- 팝업창을 닫는다  --//
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtUTC.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridUTCList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //-------------------------------------- 필 수 입 력 사 항 확 인 ------------------------------------//

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UTC), "UTC");
                QRPQAT.BL.QATENV.UTC clsUTC = new QRPQAT.BL.QATENV.UTC();
                brwChannel.mfCredentials(clsUTC);

                DataTable dtUTC = clsUTC.mfDataSetInfo();

                if (this.uGridUTCList.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridUTCList.Rows.Count; i++)
                    {
                        //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridUTCList.ActiveCell = this.uGridUTCList.Rows[0].Cells[0];

                        //-- 편집이미지가 나타난 줄만 저장 --//
                        if (this.uGridUTCList.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridUTCList.Rows[i].Cells["PlantCode"].Value.ToString() == "" || this.uGridUTCList.Rows[i].Cells["PlantCode"].Value.ToString() == "선택")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);


                                this.uGridUTCList.ActiveCell = this.uGridUTCList.Rows[i].Cells["PlantCode"];
                                this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);

                                return;
                            }
                            else if (this.uGridUTCList.Rows[i].Cells["WriteDate"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 등록일을 선택해주세요.", Infragistics.Win.HAlign.Right);


                                this.uGridUTCList.ActiveCell = this.uGridUTCList.Rows[i].Cells["WriteDate"];
                                this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);

                                return;
                            }
                            else if (this.uGridUTCList.Rows[i].Cells["WriteUserName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 등록자를 입력해주세요.", Infragistics.Win.HAlign.Right);


                                this.uGridUTCList.ActiveCell = this.uGridUTCList.Rows[i].Cells["WriteUserName"];
                                this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }

                            DataRow drUTC;
                            drUTC = dtUTC.NewRow();
                            drUTC["PlantCode"] = this.uGridUTCList.Rows[i].Cells["PlantCode"].Value.ToString();         //공장
                            drUTC["EquipLocCode"] = this.uGridUTCList.Rows[i].Cells["EquipLocCode"].Value.ToString();   //위치
                            drUTC["UTCNo"] = this.uGridUTCList.Rows[i].Cells["UTCNo"].Value.ToString();                 //UTCNo
                            drUTC["Temperature"] = this.uGridUTCList.Rows[i].Cells["Temperature"].Value.ToString();     //온도
                            drUTC["Humidity"] = this.uGridUTCList.Rows[i].Cells["Humidity"].Value.ToString();           //습도
                            drUTC["Particle"] = this.uGridUTCList.Rows[i].Cells["Particle"].Value.ToString();           //Particle
                            drUTC["WriteDate"] = this.uGridUTCList.Rows[i].Cells["WriteDate"].Value.ToString();         //작성일
                            drUTC["WriteUserID"] = this.uGridUTCList.Rows[i].Cells["WriteUserID"].Value.ToString();     //작성자
                            drUTC["EtcDesc"] = this.uGridUTCList.Rows[i].Cells["EtcDesc"].Value.ToString();             //비고
                            dtUTC.Rows.Add(drUTC);

                        }
                    }
                    // 데이터 테이블에 값이 없을경우 메세지박스를 띄운다.
                    if (dtUTC.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }
                

                //------ 저장 여부를 확인하는  메세지 박스를 띄운다 Yes를 누르면 진행하고 No 를 누르면 빠져나간다.----- //
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //-- 팝업창을 띄운다 --//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                    // 커서를 변경한다 //
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 매서드호출
                    string strErrRtn = clsUTC.mfSaveUTC(dtUTC, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // DeCoding 
                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // -- 커서 기본값으로 변환 -- //
                    this.MdiParent.Cursor = Cursors.Default;
                    
                    // -- 팝업창을 닫는다 -- //
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //-- 처리결과에 따라서 메세지 박스를 띄운다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                

                DataTable dtUTC = new DataTable();
                dtUTC.Columns.Add("PlantCode", typeof(string));
                dtUTC.Columns.Add("UTCNo", typeof(string));

                //   -- 그리드가 1줄이상일때 시작
                if (this.uGridUTCList.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridUTCList.ActiveCell = this.uGridUTCList.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridUTCList.Rows.Count; i++)
                    {
                        //-- 체크가 true 된 줄을 찾는다 --//
                        if (Convert.ToBoolean(this.uGridUTCList.Rows[i].Cells["Check"].Value) == true)
                        {
                            if (this.uGridUTCList.Rows[i].Cells["PlantCode"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                            {
                                DataRow drUTC;
                                drUTC = dtUTC.NewRow();
                                drUTC["PlantCode"] = this.uGridUTCList.Rows[i].Cells["PlantCode"].Value.ToString();  //공장
                                drUTC["UTCNo"] = this.uGridUTCList.Rows[i].Cells["UTCNo"].Value.ToString();             //UTCNo
                                dtUTC.Rows.Add(drUTC);
                            }
                        }
                    }
                    //-- 삭제된 정보가 없을 경우 메세지 박스를띄운다.
                    if (dtUTC.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                // -- 삭제여부 메세지 박스를 띄운다 Yes를 누르면 진행한다. -- //
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // -- 팝업창을 띄운다 
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    //-- 커서를 변경한다
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UTC), "UTC");
                    QRPQAT.BL.QATENV.UTC clsUTC = new QRPQAT.BL.QATENV.UTC();
                    brwChannel.mfCredentials(clsUTC);

                    string strErrRtn = clsUTC.mfDeleteUTC(dtUTC);

                    //-- DeCoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    // 커서를 기본값으로 변경한다
                    this.MdiParent.Cursor = Cursors.Default;

                    //팝업창을 닫는다.
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과여부에 따라 메세지 박스를 띄운다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //-- 전체 행을 선택 하여 삭제한다 --//
                this.uGridUTCList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridUTCList.Rows.All);
                this.uGridUTCList.DeleteSelectedRows(false);
                    
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridUTCList);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...


        private void frmQATZ0012_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridUTCList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        //그리드 셀업데이트 이벤트
        private void uGridUTCList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
             try
             {
                 //System ResourceInfo
                 ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                 QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                 // 행자동삭제(기본값 때문에 안됨)
                 if (grd.mfCheckCellDataInRow(this.uGridUTCList, 0, e.Cell.Row.Index))
                     e.Cell.Row.Delete(false);

                 if (e.Cell.Column.Key == "PlantCode")
                 {
                     //위치 DropDown 설정
                     WinGrid wGrid = new WinGrid();

                     string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                     QRPBrowser brwChannel = new QRPBrowser();
                     brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                     QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                     brwChannel.mfCredentials(clsLoc);

                     DataTable dtLocation = clsLoc.mfReadLocationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                     wGrid.mfSetGridCellValueList(this.uGridUTCList, e.Cell.Row.Index, "EquipLocCode", "", "선택", dtLocation);
                 }

                 string strLoginUser = m_resSys.GetString("SYS_USERID");

                 //-- 조회후 수정을 할시 등록자가 로그인사용자로 바뀐다 --//
                 if (e.Cell.Row.Cells["WriteUserName"].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                 {
                     if (e.Cell.Row.Cells["WriteUserID"].Value.ToString() != strLoginUser)
                     {
                         //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                         //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                         //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                         //brwChannel.mfCredentials(clsUser);

                         //DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), strLoginUser, m_resSys.GetString("SYS_LANG"));

                         string strUserName = m_resSys.GetString("SYS_USERNAME");

                         e.Cell.Row.Cells["WriteUserName"].Value = strUserName;
                         e.Cell.Row.Cells["WriteUserID"].Value = strLoginUser;
                     }
                 }

                 ////-- 컬럼이 등록자 일경우 --//
                 //if (e.Cell.Column.Key == "WriteUserName")
                 //{
                 //    // -- 공백이 아닐경우 실행 -- //
                 //    if (e.Cell.Value.ToString().Trim() != "")
                 //    {
                 //        if (e.Cell.Activation != Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                 //        {

                 //            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                 //            string strPlant = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                 //            //-- 공장콤보가 공백이거나 선택이 이면 메세지박스를 띄운다 --//
                 //            if (strPlant == "" || strPlant == "선택")
                 //            {


                 //                msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                 //                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                 //                          "확인창", "입력사항확인", (e.Cell.Row.Index+1) + "번째 열의 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                 //                e.Cell.Value = "";

                 //                //this.uGridUTCList.ActiveCell = e.Cell.Row.Cells["PlantCode"];
                 //                //this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                 //                return;

                 //            }

                 //            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                 //            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                 //            QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                 //            brwChannel.mfCredentials(clsUser);

                 //            string strUserID = e.Cell.Value.ToString();

                 //            DataTable dtUser = clsUser.mfReadSYSUser(strPlant, strUserID, m_resSys.GetString("SYS_LANG"));

                 //            //-- 정보가 존재할 경우 삽입 --//
                 //            if (dtUser.Rows.Count != 0)
                 //            {
                 //                string strUserName = dtUser.Rows[0]["UserName"].ToString();
                 //                e.Cell.Row.Cells["WriteUserID"].Value = strUserID;
                 //                e.Cell.Row.Cells["WriteUserName"].Value = strUserName;

                 //            }
                 //            else
                 //            {
                 //                msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                 //                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                 //                     "확인창", "입력사항확인", "ID가 존재하지 않습니다.", Infragistics.Win.HAlign.Right);

                 //                e.Cell.Value = "";
                 //                //this.uGridUTCList.ActiveCell = e.Cell.Row.Cells["WriteUserName"];
                 //                //this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                 //            }



                 //        }
                 //    }
                 //}
             }
             catch(Exception ex)
             {
                 QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                 frmErr.ShowDialog();
             }
             finally
             {
             }
        }

        //그리드안의 버튼 클릭하였을때 발생(등록자)
        private void uGridUTCList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("WriteUserName"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                    string strPlant = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    if (strPlant == "" || strPlant == "선택")
                    {


                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        this.uGridUTCList.ActiveCell = e.Cell.Row.Cells["PlantCode"];
                        this.uGridUTCList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;

                    }
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = strPlant;
                    frmUser.ShowDialog();

                    if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    e.Cell.Row.Cells["WriteUserID"].Value = frmUser.UserID;
                    e.Cell.Row.Cells["WriteUserName"].Value = frmUser.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 공장콤보박스에 따라 위치콤보박스 설정하는 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtEquipLoc = new DataTable();

                // 위치콤보박스 초기화
                this.uComboSearchEquipLoc.Items.Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    // 위치콤보박스 설정
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    dtEquipLoc = clsLoc.mfReadLocationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "EquipLocCode", "EquipLocName", dtEquipLoc);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
    }
}
