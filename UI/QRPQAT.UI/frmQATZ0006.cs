﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 환경유해물질관리                                      */
/* 프로그램ID   : frmQATZ0006.cs                                        */
/* 프로그램명   : 환경유해물질관리 정보                                 */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-21 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.EnterpriseServices;
// 파일첨부
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;

namespace QRPQAT.UI
{
    public partial class frmQATZ0006 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0006()
        {
            InitializeComponent();
        }

        private void frmQATZ0006_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0006_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("환경유해물질관리 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤초기화
            SetToolAuth();
            InitButton();
            InitComboBox();
            InitEtc();
            InitLabel();
            InitGrid();
            InitGroupBox();

            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmQATZ0006_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }



        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //버튼초기화
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_FillDown);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                //uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                //uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");

                // TextBox 최대입력길이 지정
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchItemName.MaxLength = 50;
                this.uTextVendorCode.MaxLength = 10;
                this.uTextItemName.MaxLength = 50;
                this.uTextReportNo.MaxLength = 50;
                this.uTextPb.MaxLength = 50;
                this.uTextCd.MaxLength = 50;
                this.uTextCr.MaxLength = 50;
                this.uTextPBBs.MaxLength = 50;
                this.uTextHg.MaxLength = 50;
                this.uTextPBDEs.MaxLength = 50;
                this.uTextPFOS.MaxLength = 50;
                this.uTextPFOA.MaxLength = 50;
                this.uTextSb.MaxLength = 50;
                this.uTextBBP.MaxLength = 50;
                this.uTextDBP.MaxLength = 50;
                this.uTextDEHP.MaxLength = 50;
                this.uTextHBCDD.MaxLength = 50;
                this.uTextBr.MaxLength = 50;
                this.uTextCl.MaxLength = 50;
                //PFOA/Sb 
                this.uTextWriteUserID.MaxLength = 100;
                this.uTextEtcDesc.MaxLength = 500;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchItemName, "품목명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRohsEndDate, "Rohs만료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRohsEndDateOver, "Rohs만료일초과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReportNo, "성적서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelItemName, "품목명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelItemNameDetail, "상세품목명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCd, "Cd(100)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPB, "Pb(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPBDesc, "PBDEs(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHg, "Hg(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCr, "Cr6+(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPBBs, "PBBs(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPFOS, "PFOS(900)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPFOA, "PFOA(900)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSb, "Sb(900)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelBBP, "BBP(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDBP, "DBP(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDEHP, "DEHP(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHBCDD, "HBCDD(1000)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelBr, "Br(900)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCl, "Cl(900)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMSDS, "MSDS", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRohsEndDate, "Rohs 만료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteDate, "등록일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNOLongerBuy, "不再购买", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDesc, "黄色：距离结束日不足一个月；红色：超过结束日；绿色：不再购买", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                this.uLabelNOLongerBuy.ForeColor = Color.Green;
               
                //wLabel.mfSetLabel(this.uLabelItemNameDetail, "상세품목명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();

                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                #region Header
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ItemName", "품목명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ItemNameDetail", "상세품목명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReportNo", "성적서번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "RohsEndDate", "Rohs 만료일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "NOLongerBuy", "不再购买", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "FileTitle", "첨부파일제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region Detail

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ItemName", "품목명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ItemNameDetail", "품목명상세", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "FileVarName", "첨부파일명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 300, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "FileVarValue", "첨부파일Binary", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 300, false, true, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Default, "", "", null);

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "DeleteFlag", "삭제여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                

                

                

                #endregion

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "UnGreenMaterialD");
                QRPQAT.BL.QATENV.UnGreenMaterialD clsDetail = new QRPQAT.BL.QATENV.UnGreenMaterialD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtDetail = clsDetail.mfSetDateInfo();

                this.uGridDetail.SetDataBinding(dtDetail, string.Empty);

                //폰트설정
                uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridDetail, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strVendorCode = this.uTextSearchVendorCode.Text;
                string strItemName = this.uTextSearchItemName.Text;
                string strRohsEndDateFrom = Convert.ToDateTime(this.uDateRohsEndDateFrom.Value).ToString("yyyy-MM-dd");
                string strRohsEndDateTo = Convert.ToDateTime(this.uDateRohsEndDateTo.Value).ToString("yyyy-MM-dd");
                string strRohsEndDateOverCheck = this.uCheckSearchRohsEndDateOver.Checked.ToString().ToUpper().Substring(0, 1);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtRtn = clsHeader.mfReadQATUnGreenMaterialH(strPlantCode, strVendorCode, strItemName, strRohsEndDateFrom, strRohsEndDateTo
                                                                , strRohsEndDateOverCheck, m_resSys.GetString("SYS_LANG"));

                this.uGridHeader.DataSource = dtRtn;
                this.uGridHeader.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    //WinGrid grd = new WinGrid();
                    //grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;

                // RohsEndDate가 지난경우 줄에 색상으로 표시해 준다.
                for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                {
                    if (this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value != null && this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value != DBNull.Value &&
                        this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value.ToString() != string.Empty)
                    { 
                        // if(this.uGridHeader.Rows[i].Cells["ItemNameDetail"].Text.Equals("SI-7200DFM") || this.uGridHeader.Rows[i].Cells["ItemNameDetail"].Text.Equals("C194+ PPF") || this.uGridHeader.Rows[i].Cells["ItemNameDetail"].Text.Equals("G770H") || this.uGridHeader.Rows[i].Cells["ItemNameDetail"].Text.Equals("G770H TYPE CD") || this.uGridHeader.Rows[i].Cells["ItemNameDetail"].Text.Equals("SG8500EX") )
                        //{
                        //    this.uGridHeader.Rows[i].Appearance.BackColor = Color.MediumSpringGreen;
                        //}

                        if (this.uGridHeader.Rows[i].Cells["NOLongerBuy"].Text.Equals("T"))
                        {
                            this.uGridHeader.Rows[i].Appearance.BackColor = Color.MediumSpringGreen;
                        }

                         else if (Convert.ToDateTime(this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value).CompareTo(Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))) < 0)
                         {
                             this.uGridHeader.Rows[i].Appearance.BackColor = Color.Red;
                         } 

                        //else if (Convert.ToDateTime(this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value).CompareTo(DateTime.Now.AddDays(14)) <= 0)
                        //{
                        //    this.uGridHeader.Rows[i].Appearance.BackColor = Color.Yellow;
                        //}
                        else if (Convert.ToDateTime(this.uGridHeader.Rows[i].Cells["RohsEndDate"].Value).CompareTo(DateTime.Now.AddDays(30)) <= 0) {
                            this.uGridHeader.Rows[i].Appearance.BackColor = Color.Yellow;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 신규일시 저장된 정보가 있는지 검사
                if (!this.uGroupBoxContentsArea.Expanded)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001047", Infragistics.Win.HAlign.Center);
                    return;
                }

                // 필수 입력 확인
                if (this.uComboPlant.Value == null || this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlant.DropDown();
                    return;
                }
                if (this.uTextVendorCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000166", Infragistics.Win.HAlign.Center);

                    this.uTextVendorCode.Focus();
                    return;
                }
                if (this.uTextItemName.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001209", Infragistics.Win.HAlign.Center);

                    this.uTextItemName.Focus();
                    return;
                }
                if (this.uTextItemNameDetail.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000653", Infragistics.Win.HAlign.Center);

                    this.uTextItemNameDetail.Focus();
                    return;
                }


                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 신규일시 저장된 정보가 있는지 검사
                if (this.uTextItemName.ReadOnly == false)
                {
                    // 이미 저장된 정보가 있는지 조회
                    DataTable dtRtn = clsHeader.mfReadQATUnGreenMaterialHDetail(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text, this.uTextItemName.Text, this.uTextItemNameDetail.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtRtn.Rows.Count > 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000346", "M000845"
                                    , Infragistics.Win.HAlign.Center);
                        return;
                    }
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.No) return;


                // 헤더/상세정보 데이터테이블로 반환하는 메소드 호출
                DataTable dtHeader = RtnHeaderdt();
                DataTable dtDetail = RtnDetaildt();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 저장 Method 호출
                string strErrRtn = clsHeader.mfSaveQATUnGreenMaterialH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail);

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 결과검사
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum == 0)
                {
                    ////// 첨부파일 Upload Method 호출
                    ////FileUpload();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
                else
                {
                    string strLang = m_resSys.GetString("SYS_LANG");

                    string strErrMes = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strErrMes = msg.GetMessge_Text("M000953", strLang);
                    else
                        strErrMes = ErrRtn.ErrMessage;

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strErrMes,
                                            Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000625", "M000645",
                                   Infragistics.Win.HAlign.Right);

                    // 헤더그리드에 정보가 없을시 조회 메소드
                    if (!(this.uGridHeader.Rows.Count > 0))
                        mfSearch();

                    return;
                }
                else
                {
                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000629", "M000278",
                                   Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uTextVendorCode.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "M001264", "M000629", "M000166",
                                    Infragistics.Win.HAlign.Right);

                        this.uTextVendorCode.Focus();
                        return;
                    }
                    else if (this.uTextItemName.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "M001264", "M000629", "M001209",
                                    Infragistics.Win.HAlign.Right);

                        this.uTextItemName.Focus();
                        return;
                    }
                    else
                    {
                        // 삭제여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // 매개변수 설정
                            string strPlantCode = this.uComboPlant.Value.ToString();
                            string strVendorCode = this.uTextVendorCode.Text;
                            string strItemName = this.uTextItemName.Text;
                            string strItemNameDetail = this.uTextItemNameDetail.Text;

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                            QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                            brwChannel.mfCredentials(clsHeader);

                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "UnGreenMaterialD");
                            QRPQAT.BL.QATENV.UnGreenMaterialD clsMaterialD = new QRPQAT.BL.QATENV.UnGreenMaterialD();
                            brwChannel.mfCredentials(clsMaterialD);

                            // 컬럼설정 메소드 호출
                            DataTable dtRtn = clsMaterialD.mfReadQATUnGreenMaterialD_All(strPlantCode, strVendorCode, strItemName, strItemNameDetail);

                            // ProgressBar 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread threadPop = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // 메소드 호출
                            string strErrRtn = clsHeader.mfDeleteQATUnGreenMaterialH_B(strPlantCode,
                                                                                        strVendorCode,
                                                                                        strItemName,
                                                                                        strItemNameDetail,
                                                                                        dtRtn,
                                                                                        m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                            // ProgressBar Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 결과 검사
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000677",
                                                            Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridHeader.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridHeader);
                    if (this.uGridDetail.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridDetail);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000799", "M000333",
                                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextVendorCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextItemName.Text = "";
                this.uTextItemNameDetail.Text = "";
                this.uTextPb.Text = "";
                this.uTextCd.Text = "";
                this.uTextCr.Text = "";
                this.uTextPBBs.Text = "";
                this.uTextHg.Text = "";
                ////this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                ////this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextWriteUserID.Clear();
                this.uTextEtcDesc.Text = "";
                this.uTextReportNo.Text = "";
                this.uTextPBDEs.Text = "";
                this.uTextPFOS.Text = "";
                this.uTextPFOA.Text = "";
                this.uTextSb.Text = "";
                this.uTextBBP.Text = "";
                this.uTextDBP.Text = "";
                this.uTextDEHP.Text = "";
                this.uTextHBCDD.Text = "";
                this.uTextBr.Text = "";
                this.uTextCl.Text = "";
                this.uDateRohsEndDate.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                this.uDateWriteDate.Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");

                this.uCheckMSDS.Checked = false;
                this.uCheckNOLongerBuy.Checked = false;
                while (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Rows[0].Delete(false);
                }

                // PK 편집가능상태로
                this.uComboPlant.Enabled = true;
                this.uTextVendorCode.Enabled = true;
                this.uTextItemName.ReadOnly = false;
                this.uTextItemNameDetail.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHeaderdt()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 컬럼설정 메소드 호출
                dtRtn = clsHeader.mfSetDateInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["VendorCode"] = this.uTextVendorCode.Text;
                drRow["ReportNo"] = this.uTextReportNo.Text;
                drRow["ItemName"] = this.uTextItemName.Text;
                drRow["ItemNameDetail"] = this.uTextItemNameDetail.Text;
                drRow["Pb"] = this.uTextPb.Text;
                drRow["Cd"] = this.uTextCd.Text;
                drRow["Cr"] = this.uTextCr.Text;
                drRow["PBBs"] = this.uTextPBBs.Text;
                drRow["PBDEs"] = this.uTextPBDEs.Text;
                drRow["Hg"] = this.uTextHg.Text;
                drRow["PFOS"] = this.uTextPFOS.Text;
                drRow["PFOA"] = this.uTextPFOA.Text;
                drRow["Sb"] = this.uTextSb.Text;
                drRow["BBP"] = this.uTextBBP.Text;
                drRow["DBP"] = this.uTextDBP.Text;
                drRow["DEHP"] = this.uTextDEHP.Text;
                drRow["HBCDD"] = this.uTextHBCDD.Text;
                drRow["Br"] = this.uTextBr.Text;
                drRow["Cl"] = this.uTextCl.Text;

                drRow["RohsEndDate"] = Convert.ToDateTime(this.uDateRohsEndDate.Value).ToString("yyyy-MM-dd");
                if (this.uCheckMSDS.Checked)
                    drRow["MSDS"] = "T";
                else
                    drRow["MSDS"] = "F";
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;

                drRow["NOLongerBuy"] = this.uCheckNOLongerBuy.CheckedValue;

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnDetaildt()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "UnGreenMaterialD");
                QRPQAT.BL.QATENV.UnGreenMaterialD clsDetail = new QRPQAT.BL.QATENV.UnGreenMaterialD();
                brwChannel.mfCredentials(clsDetail);

                // 컬럼설정 메소드 호출
                dtRtn = clsDetail.mfSetDateInfo();
                DataRow drRow;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    this.uGridDetail.ActiveCell = this.uGridDetail.Rows[0].Cells[0];
                    // 행삭제에 의해 히든처리 된것은 저장하지 않는다
                    ////if (this.uGridDetail.Rows[i].Hidden == false)
                    ////{

                    if (this.uGridDetail.Rows[i].RowSelectorAppearance.Image == null)
                        continue;

                    if (this.uGridDetail.Rows[i].Hidden
                        && this.uGridDetail.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        continue;

                    drRow = dtRtn.NewRow();
                    drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                    drRow["VendorCode"] = this.uTextVendorCode.Text;
                    drRow["ItemName"] = this.uTextItemName.Text;
                    drRow["ItemNameDetail"] = this.uTextItemNameDetail.Text;
                    drRow["Seq"] = this.uGridDetail.Rows[i].Cells["Seq"].Value.ToString();
                    drRow["FileTitle"] = this.uGridDetail.Rows[i].Cells["FileTitle"].Value.ToString();
                    drRow["FileVarName"] = this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString();
                    if (this.uGridDetail.Rows[i].Cells["FileVarValue"].Value != null)
                        drRow["FileVarValue"] = (byte[])this.uGridDetail.Rows[i].Cells["FileVarValue"].Value;
                    drRow["EtcDesc"] = this.uGridDetail.Rows[i].Cells["EtcDesc"].Value.ToString();
                    drRow["DeleteFlag"] = this.uGridDetail.Rows[i].Cells["DeleteFlag"].Value.ToString();
                    dtRtn.Rows.Add(drRow);

                    ////}
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        private void FileUpload()
        {
            try
            {
                // 화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                // 첨부파일 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0014");

                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    // 행삭제가 안된것만 저장
                    if (this.uGridDetail.Rows[i].Hidden == false)
                    {
                        if (this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString().Contains(":\\"))
                        {
                            // 파일 이름변경(공장+거래처코드+품목명+순번+파일명)
                            FileInfo fileDoc = new FileInfo(this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uTextItemName.Text + "-"
                                + this.uGridDetail.Rows[i].RowSelectorNumber.ToString() + "-" + fileDoc.Name;

                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                // 업로드할 파일이 있는경우 파일 업로드
                if (arrFile.Count > 0)
                {
                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        private void Search_HeaderDetail(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 헤더 상세조회 Method 호출
                DataTable dtHeader = clsHeader.mfReadQATUnGreenMaterialHDetail(strPlantCode, strVendorCode, strItemName, strItemNameDetail, m_resSys.GetString("SYS_LANG"));
                if (dtHeader.Rows.Count > 0)
                {
                    this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"].ToString();
                    this.uTextVendorCode.Text = dtHeader.Rows[0]["VendorCode"].ToString();
                    this.uTextVendorName.Text = dtHeader.Rows[0]["VendorName"].ToString();
                    this.uTextReportNo.Text = dtHeader.Rows[0]["ReportNo"].ToString();
                    this.uTextItemName.Text = dtHeader.Rows[0]["ItemName"].ToString();
                    this.uTextItemNameDetail.Text = dtHeader.Rows[0]["ItemNameDetail"].ToString();
                    this.uTextPb.Text = dtHeader.Rows[0]["Pb"].ToString();
                    this.uTextCd.Text = dtHeader.Rows[0]["Cd"].ToString();
                    this.uTextCr.Text = dtHeader.Rows[0]["Cr"].ToString();
                    this.uTextPBBs.Text = dtHeader.Rows[0]["PBBs"].ToString();
                    this.uTextPBDEs.Text = dtHeader.Rows[0]["PBDEs"].ToString();
                    this.uTextHg.Text = dtHeader.Rows[0]["Hg"].ToString();
                    this.uTextPFOS.Text = dtHeader.Rows[0]["PFOS"].ToString();
                    this.uTextPFOA.Text = dtHeader.Rows[0]["PFOA"].ToString();
                    this.uTextSb.Text = dtHeader.Rows[0]["Sb"].ToString();
                    this.uTextBBP.Text = dtHeader.Rows[0]["BBP"].ToString();
                    this.uTextDBP.Text = dtHeader.Rows[0]["DBP"].ToString();
                    this.uTextDEHP.Text = dtHeader.Rows[0]["DEHP"].ToString();
                    this.uTextHBCDD.Text = dtHeader.Rows[0]["HBCDD"].ToString();
                    this.uTextBr.Text = dtHeader.Rows[0]["Br"].ToString();
                    this.uTextCl.Text = dtHeader.Rows[0]["Cl"].ToString();
                    this.uDateRohsEndDate.Value = dtHeader.Rows[0]["RohsEndDate"].ToString();
                    if (dtHeader.Rows[0]["MSDS"].ToString() == "T")
                        this.uCheckMSDS.Checked = true;
                    else
                        this.uCheckMSDS.Checked = false;
                    this.uDateWriteDate.Value = dtHeader.Rows[0]["WriteDate"].ToString();
                    this.uTextWriteUserID.Text = dtHeader.Rows[0]["WriteUserID"].ToString();
                    this.uTextEtcDesc.Text = dtHeader.Rows[0]["EtcDesc"].ToString();

                    if (dtHeader.Rows[0]["NOLongerBuy"].ToString() == "T")
                        this.uCheckNOLongerBuy.Checked = true;
                    else
                        this.uCheckNOLongerBuy.Checked = false;

                    // PK 편집불가상태로
                    this.uComboPlant.Enabled = false;
                    this.uTextVendorCode.Enabled = false;
                    this.uTextItemName.ReadOnly = true;
                    this.uTextItemNameDetail.ReadOnly = true;

                    //管控物质超标预警
                    if (!this.uTextPb.Text.Equals("") && !this.uTextPb.Text.Equals("NA") && !this.uTextPb.Text.Equals("ND") && !this.uTextPb.Text.Equals("Negative") && Convert.ToInt32(this.uTextPb.Text) >= 1000)
                    {
                        this.uTextPb.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextCd.Text.Equals("") && !this.uTextCd.Text.Equals("NA") && !this.uTextCd.Text.Equals("ND") && !this.uTextCd.Text.Equals("Negative") && Convert.ToInt32(this.uTextCd.Text) >= 100)
                    {
                        this.uTextCd.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextCr.Text.Equals("") && !this.uTextCr.Text.Equals("NA") && !this.uTextCr.Text.Equals("ND") && !this.uTextCr.Text.Equals("Negative") && Convert.ToInt32(this.uTextCr.Text) >= 1000)
                    {
                        this.uTextCr.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextPBBs.Text.Equals("") && !this.uTextPBBs.Text.Equals("NA") && !this.uTextPBBs.Text.Equals("ND") && !this.uTextPBBs.Text.Equals("Negative") && Convert.ToInt32(this.uTextPBBs.Text) >= 1000)
                    {
                        this.uTextPBBs.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextPBDEs.Text.Equals("") && !this.uTextPBDEs.Text.Equals("NA") && !this.uTextPBDEs.Text.Equals("ND") && !this.uTextPBDEs.Text.Equals("Negative") && Convert.ToInt32(this.uTextPBDEs.Text) >= 1000)
                    {
                        this.uTextPBDEs.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextHg.Text.Equals("") && !this.uTextHg.Text.Equals("NA") && !this.uTextHg.Text.Equals("ND") && !this.uTextHg.Text.Equals("Negative") && Convert.ToInt32(this.uTextHg.Text) >= 1000)
                    {
                        this.uTextHg.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextBBP.Text.Equals("") && !this.uTextBBP.Text.Equals("NA") && !this.uTextBBP.Text.Equals("ND") && !this.uTextBBP.Text.Equals("Negative") && Convert.ToInt32(this.uTextBBP.Text) >= 1000)
                    {
                        this.uTextBBP.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextDBP.Text.Equals("") && !this.uTextDBP.Text.Equals("NA") && !this.uTextDBP.Text.Equals("ND") && !this.uTextDBP.Text.Equals("Negative") && Convert.ToInt32(this.uTextDBP.Text) >= 1000)
                    {
                        this.uTextDBP.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextDEHP.Text.Equals("") && !this.uTextDEHP.Text.Equals("NA") && !this.uTextDEHP.Text.Equals("ND") && !this.uTextDEHP.Text.Equals("Negative") && Convert.ToInt32(this.uTextDEHP.Text) >= 1000)
                    {
                        this.uTextDEHP.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextHBCDD.Text.Equals("") && !this.uTextHBCDD.Text.Equals("NA") && !this.uTextHBCDD.Text.Equals("ND") && !this.uTextHBCDD.Text.Equals("Negative") && Convert.ToInt32(this.uTextHBCDD.Text) >= 1000)
                    {
                        this.uTextHBCDD.Appearance.BackColor = Color.Pink;
                    }

                    if (!this.uTextBr.Text.Equals("") && !this.uTextBr.Text.Equals("NA") && !this.uTextBr.Text.Equals("ND") && !this.uTextBr.Text.Equals("Negative") && Convert.ToInt32(this.uTextBr.Text) >= 900)
                    {
                        this.uTextBr.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextCl.Text.Equals("") && !this.uTextCl.Text.Equals("NA") && !this.uTextCl.Text.Equals("ND") && !this.uTextCl.Text.Equals("Negative") && Convert.ToInt32(this.uTextCl.Text) >= 900)
                    {
                        this.uTextCl.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextPFOS.Text.Equals("") && !this.uTextPFOS.Text.Equals("NA") && !this.uTextPFOS.Text.Equals("ND") && !this.uTextPFOS.Text.Equals("Negative") && Convert.ToInt32(this.uTextPFOS.Text) >= 900)
                    {
                        this.uTextPFOS.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextPFOA.Text.Equals("") && !this.uTextPFOA.Text.Equals("NA") && !this.uTextPFOA.Text.Equals("ND") && !this.uTextPFOA.Text.Equals("Negative") && Convert.ToInt32(this.uTextPFOA.Text) >= 900)
                    {
                        this.uTextPFOA.Appearance.BackColor = Color.Pink;
                    }
                    if (!this.uTextSb.Text.Equals("") && !this.uTextSb.Text.Equals("NA") && !this.uTextSb.Text.Equals("ND") && !this.uTextSb.Text.Equals("Negative") && Convert.ToInt32(this.uTextSb.Text) >= 900)
                    {
                        this.uTextSb.Appearance.BackColor = Color.Pink;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        private void Search_Detail(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "UnGreenMaterialD");
                QRPQAT.BL.QATENV.UnGreenMaterialD clsDetail = new QRPQAT.BL.QATENV.UnGreenMaterialD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtRtn = clsDetail.mfReadQATUnGreenMaterialD(strPlantCode, strVendorCode, strItemName, strItemNameDetail);


                this.uGridDetail.SetDataBinding(dtRtn, string.Empty);
                ////this.uGridDetail.DataSource = dtRtn;
                ////this.uGridDetail.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDetail, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        // 행 공백일시 자동삭제
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 제목, 첨부파일, 비고가 모두 공백이면 행삭제
                if (e.Cell.Row.Cells["FileTitle"].Value.ToString() == "" &&
                    e.Cell.Row.Cells["FileVarName"].Value.ToString() == "" &&
                    e.Cell.Row.Cells["EtcDesc"].Value.ToString() == "")
                {
                    e.Cell.Row.Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 50;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 750;
                    for (int i = 0; i < uGridHeader.Rows.Count; i++)
                    {
                        uGridHeader.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정시 RowSelector 이미지 설정하는 이벤트
        private void uGridDetail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더 그리드 더블클릭시 헤더상세 / 상세정보 보여주는 이벤트
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // 현재 클릭된 행 고정
                e.Row.Fixed = true;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strVendorCode = e.Row.Cells["VendorCode"].Value.ToString();
                string strItemName = e.Row.Cells["ItemName"].Value.ToString();
                string strItemNameDetail = e.Row.Cells["ItemNameDetail"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 헤더상세 / 첨부파일 조회 Method 호출
                Search_HeaderDetail(strPlantCode, strVendorCode, strItemName, strItemNameDetail);
                Search_Detail(strPlantCode, strVendorCode, strItemName, strItemNameDetail);

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridDetail.Rows[i].Hidden = true;
                        this.uGridDetail.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 팝업창 띄우는 이벤트들..
        // 검색조건 거래처코드 버튼 이벤트
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 거래처 버튼 이벤트
        private void uTextVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextVendorCode.Text = frmPOP.CustomerCode;
                this.uTextVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 텍스트박스 버튼이벤트
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlant.Value.ToString() != "" && this.uComboPlant.Value.ToString() != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M001254" + this.uComboPlant.Text + "M000001"
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextWriteUserID.Text = "";
                    this.uTextWriteUserName.Text = "";
                }
                else
                {
                    this.uTextWriteUserID.Text = frmPOP.UserID;
                    this.uTextWriteUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 등록자 텍스트박스 키다운 이벤트
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextWriteUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteUserID.Text = "";
                            this.uTextWriteUserName.Text = "";
                        }
                        else
                        {
                            this.uTextWriteUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 첨부파일 셀버튼 클릭 이벤트
        private void uGridDetail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "FileVarName")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    //오피스2007부터 확장자가 바뀜 docx, xlsx, pptx
                    //"Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                    //"Word Documents(*.doc;*.docx)|*.doc;*.docx|Excel Worksheets(*.xls;*.xlsx)|*.xls;*.xlsx|PowerPoint Presentations(*.ppt;*.pptx)|*.ppt;*.pptx|Office Files(*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx)|*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx|Text Files(*.txt)|*.txt|Portable Document Format Files(*.pdf)|*.pdf|All Files(*.*)|*.*";
                    openFile.Filter = "All files (*.*)|*.*|Word Documents(*.doc;*.docx)|*.doc;*.docx|Excel Worksheets(*.xls;*.xlsx)|*.xls;*.xlsx|PowerPoint Presentations(*.ppt;*.pptx)|*.ppt;*.pptx|Office Files(*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx)|*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx|Text Files(*.txt)|*.txt|Portable Document Format Files(*.pdf)|*.pdf";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        string strFilePath = openFile.FileName;
                        
                        if (strFilePath.Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(strFilePath);

                            //if (!GetFileSize(fileDoc.Length))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //        "M001053", "M001452", "M001493", Infragistics.Win.HAlign.Right);
                            //    return;
                            //}
                            
                            if (CheckingSpecialText(fileDoc.Name))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);
                                return;
                            }
                            
                            e.Cell.Value = fileDoc.Name;
                            e.Cell.Row.Cells["FileVarValue"].Value = fun_ConvertFileToBinray(strFilePath);

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                            this.uGridDetail.DisplayLayout.Bands[0].AddNew();
                            //e.Cell.Value = strImageFile;
                            //QRPGlobal grdImg = new QRPGlobal();
                            //e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDetail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell uGCell = this.uGridDetail.ActiveCell;
                if (uGCell == null)
                    return;

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (uGCell.Column.Key.Equals("FileVarName"))
                    {
                        byte[] empty = { };
                        uGCell.Value = string.Empty;
                        uGCell.Row.Cells["FileVarValue"].Value = empty;

                        QRPGlobal grdImg = new QRPGlobal();
                        uGCell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 다운로드 이벤트
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (!this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]")
                            || this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString() == string.Empty)
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    int intFileNum = 0;
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                        if (this.uGridDetail.Rows[i].Hidden == false
                            && Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value)
                            && this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]"))
                            intFileNum++;
                    }
                    if (intFileNum == 0)
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";

                        for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                        {
                            if (!this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString().Equals(string.Empty)
                                && Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value)
                                && this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]"))
                            {
                                string strFileName = this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString();
                                byte[] btFileByte = (byte[])this.uGridDetail.Rows[i].Cells["FileVarValue"].Value;

                                System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                                fs.Write(btFileByte, 0, btFileByte.Length);
                                fs.Close();
                            }
                        }

                        // 저장폴더 열기
                        System.Diagnostics.Process p = new System.Diagnostics.Process();
                        p.StartInfo.FileName = strSaveFolder;
                        p.Start();
                    }
                }


                #region 주석처리
                /*
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Contains(":\\")
                            || this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString() == string.Empty)
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "입력확인", "첨부파일이 없는 리스트를 선택했습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "입력확인", "첨부파일을 선택해주세요",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    int intFileNum = 0;
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                        if (this.uGridDetail.Rows[i].Hidden == false && this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString().Contains(":\\") == false)
                            intFileNum++;
                    }
                    if (intFileNum == 0)
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0014");

                        //설비이미지 Upload하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                        {
                            if (this.uGridDetail.Rows[i].Hidden == false && this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString().Contains(":\\") == false)
                                arrFile.Add(this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString());
                        }

                        //Upload정보 설정
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                    }
                }
                 * */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        private void uGridDetail_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FileVarName"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Row.Cells["FileVarValue"].Text.Equals("System.Byte[]"))
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = System.Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = System.Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";

                            string strFileName = e.Cell.Value.ToString();
                            byte[] btFileByte = (byte[])e.Cell.Row.Cells["FileVarValue"].Value;

                            System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                            fs.Write(btFileByte, 0, btFileByte.Length);
                            fs.Close();

                            // 파일 자동실행
                            ////System.Diagnostics.Process p = new System.Diagnostics.Process();
                            ////p.StartInfo.FileName = strSaveFolder + strFileName;
                            ////p.Start();
                            System.Diagnostics.Process.Start(strSaveFolder);
                        }
                    }

                    #region 변경에 의한 주석처리
                    ////WinMessageBox msg = new WinMessageBox();
                    ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    ////// 파일서버에서 불러올수 있는 파일인지 체크
                    ////if (e.Cell.Value.ToString().Contains(":\\"))
                    ////{
                    ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                             "처리결과", "처리결과", "업로드된 첨부화일이 아닙니다.",
                    ////                             Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}
                    ////else
                    ////{
                    ////    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    ////    //화일서버 연결정보 가져오기
                    ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////    brwChannel.mfCredentials(clsSysAccess);
                    ////    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    ////    //첨부파일 저장경로정보 가져오기

                    ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    ////    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    ////    brwChannel.mfCredentials(clsSysFilePath);
                    ////    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0014");

                    ////    //첨부파일 Download하기
                    ////    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ////    ArrayList arrFile = new ArrayList();
                    ////    arrFile.Add(e.Cell.Value.ToString());

                    ////    //Download정보 설정
                    ////    string strExePath = Application.ExecutablePath;
                    ////    int intPos = strExePath.LastIndexOf(@"\");
                    ////    strExePath = strExePath.Substring(0, intPos + 1);

                    ////    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                    ////                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                    ////                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                    ////                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                    ////                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                    ////                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    ////    fileAtt.ShowDialog();

                    ////    // 파일 실행시키기
                    ////    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    ////}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0006_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일을 Byte[] 로 변환하여 반환하는 메소드
        /// </summary>
        /// <param name="strFileFullPath">파일경로</param>
        /// <returns></returns>
        private byte[] fun_ConvertFileToBinray(string strFileFullPath)
        {
            // FileStream 자동 소멸을 위한 Using 구문
            using (System.IO.FileStream fs = new System.IO.FileStream(strFileFullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                int intLength = Convert.ToInt32(fs.Length);
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                byte[] btBuffer = br.ReadBytes(intLength);
                fs.Close();

                return btBuffer;
            }
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }

        /// <summary>
        /// 첨부파일사이즈 판단하여 6M이하면 true 6M초과시 false
        /// </summary>
        /// <param name="byteCount"></param>
        /// <returns></returns>
        private bool GetFileSize(double byteCount)
        {
            try
            {
                
                if (byteCount >= 1073741824.0)
                    return false;//intSize = Convert.ToInt32(byteCount / 1073741824.0);
                else if (byteCount >= 1048576.0)
                {
                    double bdSize = 0;
                    bdSize = byteCount / 1048576.0;
                    if (bdSize > 6)
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }


    }
}
