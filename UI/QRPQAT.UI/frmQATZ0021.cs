﻿/*----------------------------------------------------------------------*/
/* 시스템명     : SRR 점수 등록                                         */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQATZ0021.cs                                        */
/* 프로그램명   : 고객불만 등록/조회                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-12-24                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPQAT.UI
{
    public partial class frmQATZ0021 : Form, QRPCOM.QRPGLO.IToolbar
    {
        public override Size MinimumSize
        {
            get
            {
                Size size = new Size(1070, 850);
                return size;
            }
            set
            {
                Size size = new Size(1070, 850);
                base.MinimumSize = size;
            }
        }

        // Resource 호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        DataTable dtRankInfo = new DataTable();

        public frmQATZ0021()
        {
            InitializeComponent();
            this.Activated += new EventHandler(frmQATZ0021_Activated);
            this.Load += new EventHandler(frmQATZ0021_Load);
            this.FormClosing += new FormClosingEventHandler(frmQATZ0021_FormClosing);
        }

        void frmQATZ0021_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
            wGrid.mfSaveGridColumnProperty(this);
            
            if(dtRankInfo != null)
                dtRankInfo.Dispose();
        }

        void frmQATZ0021_Load(object sender, EventArgs e)
        {
            // 권한설정
            SetToolAuth();

            // 초기화 메소드 호출
            InitEvent();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitEtc();

            // ContentsArea 접힘상태
            this.uGroupBoxContentsArea.Expanded = false;

            // 공장코드 숨김상태로
            this.uComboPlant.Hide();
            this.uComboSearchPlant.Hide();
            this.uLabelPlant.Hide();
            this.uLabelSearchPlant.Hide();
        }

        void frmQATZ0021_Activated(object sender, EventArgs e)
        {
            // 툴바설정
            QRPCOM.QRPGLO.QRPBrowser ToolButton = new QRPCOM.QRPGLO.QRPBrowser();
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            m_resSys.Close();
        }

        #region IToolbar 멤버

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (!this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = true;
                else
                    Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            // ProgressBar 생성
            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!this.uGroupBoxContentsArea.Expanded)
                    return;
                else if (this.uComboPlant.SelectedIndex <= 0)
                    return;
                else if (this.uComboCustomerProdType.SelectedIndex <= 0)
                    return;
                else if (this.uTextYear.Text.Equals(string.Empty))
                    return;
                else if (this.uComboEvaluationPeriod.SelectedIndex <= 0)
                    return;
                else if (this.uComboTargetCompany.SelectedIndex <= 0)
                    return;

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                if (msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {

                    System.Threading.Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // Connection BL
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRHeader), "SRRHeader");
                    using (QRPQAT.BL.QATCLM.SRRHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRHeader())
                    {
                        brwChannel.mfCredentials(clsSRR_H);
                        // 변수 설정
                        string[] strSplit = { "||" };
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string[] strCustomerProdType = this.uComboCustomerProdType.Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                        string strCustomerCode = string.Empty;
                        string strProdType = string.Empty;
                        if (strCustomerProdType.Length >= 1)
                        {
                            strCustomerCode = strCustomerProdType[0];
                            if (strCustomerProdType.Length > 1)
                                strProdType = strCustomerProdType[1];
                        }
                        string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                        string strYear = this.uTextYear.Text;
                        string strTargetCompany = this.uComboTargetCompany.SelectedItem.DisplayText.ToString().TrimEnd().TrimStart();

                        string strErrRtn = clsSRR_H.mfDeleteQATSRRMasterHeader(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod
                            , strTargetCompany, strYear);

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000677",
                                                            Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                            , msg.GetMessge_Text("M000638", m_resSys.GetString("SYS_LANG"))
                                                            , ErrRtn.ErrMessage,
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridSRRList.Rows.Count > 0)
                {
                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridSRRList);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {

            // 프로그래스 팝업창 생성
            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                #region 입력값 확인

                if (this.uComboPlant.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.Focus();
                    this.uComboPlant.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uTextYear.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M000813", Infragistics.Win.HAlign.Right);

                    this.uTextYear.Focus();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboCustomerProdType.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001536", Infragistics.Win.HAlign.Right);

                    this.uComboCustomerProdType.Focus();
                    this.uComboCustomerProdType.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboTargetCompany.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001537", Infragistics.Win.HAlign.Right);

                    this.uComboTargetCompany.Focus();
                    this.uComboTargetCompany.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uGridSRRItemList.Rows.Count.Equals(0))
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001047", Infragistics.Win.HAlign.Right);

                    m_resSys.Close();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this))
                {
                    m_resSys.Close();
                    return;
                }

                #endregion

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    System.Threading.Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region 저장정보 설정

                    this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    this.uGridSRRItemList.UpdateData();

                    // 헤더정보
                    string[] strSplit = { "||" };
                    string[] strCustomerProdType = this.uComboCustomerProdType.Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                    string strCustomerCode = string.Empty;
                    string strProdType = string.Empty;
                    if (strCustomerProdType.Length >= 1)
                    {
                        strCustomerCode = strCustomerProdType[0];

                        if (strCustomerProdType.Length > 1)
                            strProdType = strCustomerProdType[1];
                    }

                    System.Xml.XmlDocument xmlHeader = new System.Xml.XmlDocument();
                    xmlHeader.LoadXml(Set_HeaderXMLInfo());
                    xmlHeader.SelectSingleNode("message/body/PlantCode").InnerText = this.uComboPlant.Value.ToString();
                    xmlHeader.SelectSingleNode("message/body/CustomerCode").InnerText = strCustomerCode;
                    xmlHeader.SelectSingleNode("message/body/ProdType").InnerText = strProdType;
                    xmlHeader.SelectSingleNode("message/body/EvaluationPeriod").InnerText = this.uComboEvaluationPeriod.Value.ToString();
                    xmlHeader.SelectSingleNode("message/body/Year").InnerText = this.uTextYear.Text;
                    xmlHeader.SelectSingleNode("message/body/TargetCompany").InnerText = this.uComboTargetCompany.SelectedItem.DisplayText.TrimEnd().TrimStart();
                    xmlHeader.SelectSingleNode("message/body/M01").InnerText = this.uCheckM01.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M02").InnerText = this.uCheckM02.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M03").InnerText = this.uCheckM03.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M04").InnerText = this.uCheckM04.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M05").InnerText = this.uCheckM05.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M06").InnerText = this.uCheckM06.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M07").InnerText = this.uCheckM07.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M08").InnerText = this.uCheckM08.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M09").InnerText = this.uCheckM09.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M10").InnerText = this.uCheckM10.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M11").InnerText = this.uCheckM11.Checked.ToString().ToUpper().Substring(0, 1);
                    xmlHeader.SelectSingleNode("message/body/M12").InnerText = this.uCheckM12.Checked.ToString().ToUpper().Substring(0, 1);

                    string strXML = string.Empty;
                    
                    // Item 정보 XML 정보로 변환
                    using (DataTable dtItem = ((DataTable)this.uGridSRRItemList.DataSource).Copy())
                    {
                        foreach (DataRow _dr in dtItem.Rows)
                        {
                            for (int i = 0; i < dtItem.Columns.Count; i++)
                            {
                                if (_dr[i] == DBNull.Value)
                                    _dr[i] = string.Empty;
                            }
                        }

                        using (System.IO.MemoryStream msTreamXML = new System.IO.MemoryStream())
                        {
                            dtItem.WriteXml(msTreamXML);
                            msTreamXML.Seek(0, System.IO.SeekOrigin.Begin);
                            using (System.IO.StreamReader srXML = new System.IO.StreamReader(msTreamXML))
                            {
                                #region XML Parsing

                                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                                xmlDoc.Load(srXML);

                                System.Xml.XmlNodeList _NodeList = xmlDoc.SelectNodes("DocumentElement/*");

                                System.Xml.XmlElement xmlPeriodType;

                                foreach (System.Xml.XmlNode _node in _NodeList)
                                {
                                    xmlPeriodType = xmlDoc.CreateElement("PeriodType");
                                    for (int i = 0; i < _node.ChildNodes.Count; i++)
                                    {
                                        System.Xml.XmlNode _nodeChild = _node.ChildNodes[i];

                                        if (!_nodeChild.Name.Equals("EvaluationType") && !_nodeChild.Name.Equals("EvaluationItem") && !_nodeChild.Name.Equals("Score"))
                                        {
                                            xmlPeriodType.AppendChild(_nodeChild);
                                            i--;
                                        }
                                    }
                                    _node.AppendChild(xmlPeriodType);
                                }

                                #endregion

                                //strXML = srXML.ReadToEnd();
                                strXML = xmlDoc.OuterXml.ToString();
                                srXML.Close();
                            }
                            msTreamXML.Close();
                        }
                    }

                    #endregion

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRHeader), "SRRHeader");
                    using (QRPQAT.BL.QATCLM.SRRHeader clsHeader = new QRPQAT.BL.QATCLM.SRRHeader())
                    {
                        brwChannel.mfCredentials(clsHeader);

                        string strErrRtn = clsHeader.mfSaveQATSRRHeader(xmlHeader.OuterXml.ToString()
                            , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), strXML);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                    , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                    , ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                #region 검색조건 확인

                if (this.uComboSearchPlant.SelectedIndex < 0)
                    this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    //this.uComboSearchPlant.SelectedIndex = 0;

                ////if (this.uComboSearchCustomer.SelectedIndex < 0)
                ////    this.uComboSearchCustomer.SelectedIndex = 0;

                if (this.uTextSearchYear.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M000813", Infragistics.Win.HAlign.Right);

                    this.uTextSearchYear.Focus();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboSearchCustomer.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001536", Infragistics.Win.HAlign.Right);

                    this.uComboSearchCustomer.Focus();
                    this.uComboSearchCustomer.DropDown();
                    m_resSys.Close();
                    return;
                }

                #endregion

                // 프로그래스바 생성
                System.Threading.Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                #region 검색조건 변수설정

                // 검색조건 변수 설정
                string[] strSplit = { "||" };
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string[] strCustomerProdType = this.uComboSearchCustomer.Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                string strCustomerCode = string.Empty;
                string strProdType = string.Empty;
                if (strCustomerProdType.Length >= 1)
                {
                    strCustomerCode = strCustomerProdType[0];
                    if (strCustomerProdType.Length > 1)
                        strProdType = strCustomerProdType[1];
                }
                string strYear = this.uTextSearchYear.Text;

                #endregion

                // SET BL Connection
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRHeader), "SRRHeader");
                using (QRPQAT.BL.QATCLM.SRRHeader clsHeader = new QRPQAT.BL.QATCLM.SRRHeader())
                {
                    brwChannel.mfCredentials(clsHeader);

                    string strSearch = clsHeader.mfReadQATSRRHeader(strPlantCode, strCustomerCode, strProdType, strYear, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        if (!string.IsNullOrEmpty(strSearch))
                        {
                            #region Data Parsing .Xml

                            string strEvaluationPeriod = string.Empty;

                            using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                            {
                                using (DataTable dtHeader_temp = new DataTable())
                                {
                                    dtHeader_temp.BeginLoadData();
                                    dtHeader_temp.ReadXml(xmlSR);
                                    dtHeader_temp.EndLoadData();


                                    if (dtHeader_temp.Rows.Count > 0)
                                    {
                                        strEvaluationPeriod = dtHeader_temp.Rows[0]["EvaluationPeriod"].ToString();

                                        // DataTable Parsing
                                        //DataTable dtHeader_bind = Set_HeaderDataInfo(strEvaluationPeriod);

                                        using (DataTable dtHeader_bind = dtHeader_temp.DefaultView.ToTable(true, "PlantCode", "CustomerCode", "ProdType", "EvaluationPeriod", "Year", "TargetCompany"))
                                        {
                                            if (strEvaluationPeriod.Equals("MON"))
                                            {
                                                for (int i = 0; i < 12; i++)
                                                {
                                                    string strColKey = "M" + string.Format("{0:00}", i + 1);
                                                    dtHeader_bind.Columns.Add(strColKey + "_R", typeof(string));
                                                    dtHeader_bind.Columns.Add(strColKey + "_C", typeof(string));
                                                    dtHeader_bind.Columns.Add(strColKey + "_V", typeof(string));

                                                    DataRow[] _drs = dtHeader_temp.Select("PeriodType = '" + strColKey + "'", "Score DESC");
                                                    for (int j = 0; j < _drs.Length; j++)
                                                    {
                                                        if (_drs[j]["Score"].ToString().Equals(string.Empty))
                                                        {
                                                            dtHeader_bind.Rows[j][strColKey + "_R"] = _drs[j]["Rank"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_V"] = string.Empty;
                                                        }
                                                        else
                                                        {
                                                            dtHeader_bind.Rows[j][strColKey + "_R"] = _drs[j]["Rank"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_V"] = _drs[j]["Score"].ToString();
                                                        }
                                                    }
                                                }
                                            }
                                            else if (strEvaluationPeriod.Equals("QUA"))
                                            {
                                                for (int i = 0; i < 4; i++)
                                                {
                                                    string strColKey = "Q" + string.Format("{0:00}", i + 1);
                                                    dtHeader_bind.Columns.Add(strColKey + "_R", typeof(string));
                                                    dtHeader_bind.Columns.Add(strColKey + "_C", typeof(string));
                                                    dtHeader_bind.Columns.Add(strColKey + "_V", typeof(string));

                                                    DataRow[] _drs = dtHeader_temp.Select("PeriodType = '" + strColKey + "'", "Score DESC");
                                                    for (int j = 0; j < _drs.Length; j++)
                                                    {
                                                        if (_drs[j]["Score"].ToString().Equals(string.Empty))
                                                        {
                                                            dtHeader_bind.Rows[j][strColKey + "_R"] = _drs[j]["Rank"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_V"] = string.Empty;
                                                        }
                                                        else
                                                        {
                                                            dtHeader_bind.Rows[j][strColKey + "_R"] = _drs[j]["Rank"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                            dtHeader_bind.Rows[j][strColKey + "_V"] = _drs[j]["Score"].ToString();
                                                        }
                                                    }
                                                }
                                            }
                                            ChangeGrid_Header(strEvaluationPeriod);
                                            //this.uGridSRRList.SetDataBinding(dtHeader_bind.Copy(), string.Empty);
                                            this.uGridSRRList.DataSource = dtHeader_bind.Copy();
                                            this.uGridSRRList.DataBind();
                                        }
                                    }
                                    else
                                    {
                                        if (this.uGridSRRList.DataSource != null)
                                            this.uGridSRRList.DataSource = ((DataTable)this.uGridSRRList.DataSource).Clone();

                                        // Chart 초기화
                                        QRPSTA.STASPC clsSTA = new QRPSTA.STASPC();
                                        clsSTA.mfInitControlChart(this.uChartSRR);
                                        // Legend Clear
                                        this.uChartSRR.CompositeChart.Legends.Clear();
                                    }
                                }
                            }

                            #endregion

                            #region Data Parsing .Binary
                            /*
                            string strEvaluationPeriod = string.Empty;

                            using (DataTable dtHeader_temp = DecompressDataTable(ErrRtn.mfGetReturnValue(0)))
                            {
                                if (dtHeader_temp.Rows.Count > 0)
                                {
                                    strEvaluationPeriod = dtHeader_temp.Rows[0]["EvaluationPeriod"].ToString();

                                    // DataTable Parsing
                                    //DataTable dtHeader_bind = Set_HeaderDataInfo(strEvaluationPeriod);

                                    using (DataTable dtHeader_bind = dtHeader_temp.DefaultView.ToTable(true, "PlantCode", "CustomerCode", "ProdType", "CustomerProdType", "EvaluationPeriod", "Year", "TargetCompany"))
                                    {
                                        dtHeader_bind.Columns.Add("Rank", typeof(Int32));

                                        for (int i = 0; i < dtHeader_bind.Rows.Count; i++)
                                        {
                                            dtHeader_bind.Rows[i]["Rank"] = (i + 1).ToString();
                                        }

                                        if (strEvaluationPeriod.Equals("MON"))
                                        {
                                            for (int i = 0; i < 12; i++)
                                            {
                                                string strColKey = "M" + string.Format("{0:00}", i + 1);
                                                dtHeader_bind.Columns.Add(strColKey + "_C", typeof(string));
                                                dtHeader_bind.Columns.Add(strColKey + "_V", typeof(string));

                                                DataRow[] _drs = dtHeader_temp.Select("PeriodType = '" + strColKey + "'", "Value DESC");
                                                for (int j = 0; j < _drs.Length; j++)
                                                {
                                                    if (_drs[j]["Value"].ToString().Equals(string.Empty))
                                                    {
                                                        dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                        dtHeader_bind.Rows[j][strColKey + "_V"] = string.Empty;
                                                    }
                                                    else
                                                    {
                                                        dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                        dtHeader_bind.Rows[j][strColKey + "_V"] = _drs[j]["Value"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                        else if (strEvaluationPeriod.Equals("QUA"))
                                        {
                                            for (int i = 0; i < 4; i++)
                                            {
                                                string strColKey = "Q" + string.Format("{0:00}", i + 1);
                                                dtHeader_bind.Columns.Add(strColKey + "_C", typeof(string));
                                                dtHeader_bind.Columns.Add(strColKey + "_V", typeof(string));

                                                DataRow[] _drs = dtHeader_temp.Select("PeriodType = '" + strColKey + "'", "Value DESC");
                                                for (int j = 0; j < _drs.Length; j++)
                                                {
                                                    if (_drs[j]["Value"].ToString().Equals(string.Empty))
                                                    {
                                                        dtHeader_bind.Rows[j][strColKey + "_C"] = string.Empty;
                                                        dtHeader_bind.Rows[j][strColKey + "_V"] = string.Empty;
                                                    }
                                                    else
                                                    {
                                                        dtHeader_bind.Rows[j][strColKey + "_C"] = _drs[j]["TargetCompany"].ToString();
                                                        dtHeader_bind.Rows[j][strColKey + "_V"] = _drs[j]["Value"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                        ChangeGrid_Header(strEvaluationPeriod);
                                        //this.uGridSRRList.SetDataBinding(dtHeader_bind.Copy(), string.Empty);
                                        this.uGridSRRList.DataSource = dtHeader_bind.Copy();
                                        this.uGridSRRList.DataBind();
                                    }
                                }
                                else
                                {
                                    this.uGridSRRList.DataSource = ((DataTable)this.uGridSRRList.DataSource).Clone();
                                    
                                    // Chart 초기화
                                    QRPSTA.STASPC clsSTA = new QRPSTA.STASPC();
                                    clsSTA.mfInitControlChart(this.uChartSRR);
                                    // Legend Clear
                                    this.uChartSRR.CompositeChart.Legends.Clear();
                                }
                            }
                            */
                            #endregion

                            if (this.uGridSRRList.Rows.Count.Equals(0))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                wGrid.mfSetAutoResizeColWidth(this.uGridSRRList, 0);

                                #region Chart 그리기

                                string strChartXML = clsHeader.mfReadQATSRRHeader_Chart(strPlantCode, strCustomerCode, strProdType, strYear, m_resSys.GetString("SYS_LANG"));

                                ErrRtn = ErrRtn.mfDecodingErrMessage(strChartXML);
                                if (ErrRtn.ErrNum.Equals(0))
                                {
                                    using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                                    {
                                        using (DataTable dtChart = new DataTable())
                                        {
                                            dtChart.BeginLoadData();
                                            dtChart.ReadXml(xmlSR);
                                            dtChart.EndLoadData();

                                            // 차트 그리기
                                            DrawChart(dtChart, strEvaluationPeriod);
                                        }
                                    }
                                }
                                else
                                {
                                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                                    {
                                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001115", "M001539", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                                            , ErrRtn.SystemInnerException + "<br/>" +
                                                ErrRtn.SystemMessage + "<br/>" +
                                                ErrRtn.SystemStackTrace
                                            , Infragistics.Win.HAlign.Right);
                                    }
                                }

                                #endregion
                            }
                        }
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                            , ErrRtn.SystemInnerException + "<br/>" +
                                ErrRtn.SystemMessage + "<br/>" +
                                ErrRtn.SystemStackTrace
                            , Infragistics.Win.HAlign.Right);
                    }
                }

                // ContentsArea 접힘상태
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                m_resSys.Close();
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        private void InitEvent()
        {
            this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
            this.uComboPlant.ValueChanged += new EventHandler(uComboPlant_ValueChanged);
            this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            this.uButtonLoad.Click += new EventHandler(uButtonLoad_Click);
            this.uTextYear.KeyPress += new KeyPressEventHandler(uTextYear_KeyPress);
            this.uTextSearchYear.KeyPress += new KeyPressEventHandler(uTextSearchYear_KeyPress);
            this.uGridSRRItemList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSRRItemList_AfterCellUpdate);
            this.uGridSRRItemList.KeyPress += new KeyPressEventHandler(uGridSRRItemList_KeyPress);
            //this.uGridSRRList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridSRRList_DoubleClickRow);
            this.uGridSRRList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(uGridSRRList_DoubleClickCell);
            this.uComboCustomerProdType.BeforeDropDown += new CancelEventHandler(uComboCustomerProdType_BeforeDropDown);
            this.uComboSearchCustomer.BeforeDropDown += new CancelEventHandler(uComboSearchCustomer_BeforeDropDown);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                using (QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth())
                {
                    brwChannel.mfCredentials(UAuth);
                    DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                    m_resSys.Close();
                    QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                    Global.mfMakeToolInfoResource(dtAuth);
                    dtAuth.Dispose();
                }
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사 구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchYear, "평가년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelYear, "평가년도", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomerProdType, "고객사 구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEvaluationPeriod, "평가주기", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelTargetCompany, "대상업체", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAcceptCheck, "예상점수여부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinButton wButton = new QRPCOM.QRPUI.WinButton();

                wButton.mfSetButton(this.uButtonLoad, "Load", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Zoom);

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCombo = new QRPCOM.QRPUI.WinComboEditor();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                // 공장콤보 설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                using (QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant())
                {
                    brwChannel.mfCredentials(clsPlant);

                    DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "", "PlantCode", "PlantName", dtPlant);

                    wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "", "PlantCode", "PlantName", dtPlant);

                    dtPlant.Dispose();
                }

                // 평가주기 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                using (QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode())
                {
                    brwChannel.mfCredentials(clsCom);

                    DataTable dtComCode = clsCom.mfReadCommonCode("C0078", m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboEvaluationPeriod, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComCode", "ComCodeName", dtComCode);

                    dtComCode.Dispose();
                }

                // 대상업체
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                using (QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode())
                {
                    brwChannel.mfCredentials(clsUserCom);
                    DataTable dtTargetCompany = clsUserCom.mfReadUserCommonCode("QUA", "U0021", m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboTargetCompany, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComCode", "ComCodeName", dtTargetCompany);

                    dtTargetCompany.Dispose();
                }

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                #region Search List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRRList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "ProdType", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Year", "년도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "EvaluationPeriod", "평가기간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "TargetCompany", "대상업체", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                ////Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup1 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q01", "1분기", 5, 0, 2, 2, false);
                ////Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup2 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q02", "2분기", 7, 0, 2, 2, false);
                ////Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup3 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q03", "3분기", 9, 0, 2, 2, false);
                ////Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup4 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q04", "4분기", 11, 0, 2, 2, false);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q01_Company", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup1);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q01_Score", "점수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup1);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q02_Company", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup2);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q02_Score", "점수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup2);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q03_Company", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup3);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q03_Score", "점수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup3);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q04_Company", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup4);

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q04_Score", "점수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup4);

                //this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowRowLayoutCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.Horizontal;
                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowRowLayoutLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.Horizontal;

                // 조회리스트 편집불가
                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                this.uGridSRRList.DisplayLayout.Bands[0].Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

                #endregion

                #region ItemList

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRRItemList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 기본데이터 바인딩
                this.uGridSRRItemList.SetDataBinding(Set_ItemDataInfo(), string.Empty);

                // 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "EvaluationType", "평가구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "EvaluationItem", "평가항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "Score", "배점", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn.n", "0.0");

                // Score 컬럼 PomptChar 설정
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].PromptChar = ' ';
                // Null Value 설정
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].Nullable = Infragistics.Win.UltraWinGrid.Nullable.Automatic;
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].NullText = "";

                // 헤더 클릭시 Sorting 방지
                this.uGridSRRItemList.DisplayLayout.Bands[0].Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["EvaluationType"].TabStop = false;
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["EvaluationItem"].TabStop = false;
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].TabStop = false;

                // 기존 KeyMapping 삭제
                this.uGridSRRItemList.KeyActionMappings.Clear();
                this.uGridSRRItemList.KeyActionMappings.LoadDefaultMappings();
                ////Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping11 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Enter, Infragistics.Win.UltraWinGrid.UltraGridAction.BelowCell, 0, 0, 0, 0);
                ////Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping12 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Enter, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                ////this.uGridSRRItemList.KeyActionMappings.Add(keyMapping11);
                ////this.uGridSRRItemList.KeyActionMappings.Add(keyMapping12);

                #endregion

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                this.titleArea.mfSetLabelText("SRR 조회/등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                this.uTextSearchYear.Text = DateTime.Now.Year.ToString();
                this.uTextSearchYear.MaxLength = 4;
                this.uTextSearchYear.Appearance.BackColor = Color.PowderBlue;

                this.uTextYear.Appearance.BackColor = Color.PowderBlue;
                this.uTextYear.MaxLength = 4;
                this.uTextYear.Text = DateTime.Now.Year.ToString();

                this.uLabelEvaluationPeriod.Hide();
                this.uComboEvaluationPeriod.Hide();

                this.uTextYear.TabIndex = 1;
                this.uComboCustomerProdType.TabIndex = 2;
                this.uComboTargetCompany.TabIndex = 3;

                this.uChartSRR.Height = 350;
                QRPSTA.STASPC clsChart = new QRPSTA.STASPC();
                clsChart.mfInitControlChart(this.uChartSRR);

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods...

        private string Set_HeaderXMLInfo()
        {
            return "<message><body><PlantCode></PlantCode><CustomerCode></CustomerCode><ProdType></ProdType><EvaluationPeriod></EvaluationPeriod>" + 
                "<TargetCompany></TargetCompany><Year></Year><M01></M01><M02></M02><M03></M03><M04></M04><M05></M05><M06></M06><M07></M07><M08></M08>" +
                "<M09></M09><M10></M10><M11></M11><M12></M12></body></message>";
        }

        // 아이템 데이터 테이블 정보 반환
        private DataTable Set_ItemDataInfo()
        {
            DataTable dtItem = new DataTable();

            dtItem.TableName = "Item";
            dtItem.Columns.AddRange(new DataColumn[] {
                  new DataColumn { ColumnName = "EvaluationType", Caption = "평가구분", DataType = typeof(string), DefaultValue = string.Empty }
                , new DataColumn { ColumnName = "EvaluationItem", Caption = "평가항목", DataType = typeof(string), DefaultValue = string.Empty }
                , new DataColumn { ColumnName = "Score", Caption = "배점", DataType= typeof(string), DefaultValue = string.Empty }
            });

            return dtItem;
        }

        // Control Clear Method
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboCustomerProdType.SelectedIndex = 0;
                this.uTextYear.Clear();
                this.uComboEvaluationPeriod.SelectedIndex = 0;
                this.uComboTargetCompany.Text = "PSTS";

                this.uGridSRRItemList.SetDataBinding(Set_ItemDataInfo(), string.Empty);

                this.uComboPlant.ReadOnly = false;
                this.uComboCustomerProdType.ReadOnly = false;
                this.uTextYear.ReadOnly = false;
                this.uComboEvaluationPeriod.ReadOnly = false;
                this.uComboTargetCompany.ReadOnly = false;

                this.uCheckM01.Checked = false;
                this.uCheckM02.Checked = false;
                this.uCheckM03.Checked = false;
                this.uCheckM04.Checked = false;
                this.uCheckM05.Checked = false;
                this.uCheckM06.Checked = false;
                this.uCheckM07.Checked = false;
                this.uCheckM08.Checked = false;
                this.uCheckM09.Checked = false;
                this.uCheckM10.Checked = false;
                this.uCheckM11.Checked = false;
                this.uCheckM12.Checked = false;

                this.uCheckM01.Text = "1月";
                this.uCheckM02.Text = "2月";
                this.uCheckM03.Text = "3月";
                this.uCheckM04.Text = "4月";
                this.uCheckM05.Text = "5月";
                this.uCheckM06.Text = "6月";
                this.uCheckM07.Text = "7月";
                this.uCheckM08.Text = "8月";
                this.uCheckM09.Text = "9月";
                this.uCheckM10.Text = "10月";
                this.uCheckM11.Text = "11月";
                this.uCheckM12.Text = "12月";

                this.uCheckM05.Visible = true;
                this.uCheckM06.Visible = true;
                this.uCheckM07.Visible = true;
                this.uCheckM08.Visible = true;
                this.uCheckM09.Visible = true;
                this.uCheckM10.Visible = true;
                this.uCheckM11.Visible = true;
                this.uCheckM12.Visible = true;

                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboCustomerProdType.Appearance.BackColor = Color.PowderBlue;
                this.uTextYear.Appearance.BackColor = Color.PowderBlue;
                this.uComboEvaluationPeriod.Appearance.BackColor = Color.PowderBlue;
                this.uComboTargetCompany.Appearance.BackColor = Color.PowderBlue;

                this.uButtonLoad.Show();

                this.uTextYear.Focus();

                dtRankInfo = new DataTable();

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Item List 순번 설정
        private bool SetitemSeqAndCheckValue()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                bool bolCheckValue = true;

                foreach (Infragistics.Win.UltraWinGrid.UltraGridRow uRow in this.uGridSRRItemList.Rows)
                {
                    // Score Data Check
                    if (!(Convert.ToDecimal(uRow.Cells["Score"].Value) > 0m && Convert.ToDecimal(uRow.Cells["Score"].Value) < 100m))
                    {
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000879", "M001535", Infragistics.Win.HAlign.Right);

                        uRow.Cells["Score"].Activate();
                        this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                        bolCheckValue = false;
                        break;
                    }

                    // Set Seq Value
                    uRow.Cells["Seq"].Value = uRow.RowSelectorNumber;
                }

                m_resSys.Close();
                return bolCheckValue;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        // Datatable 압축해제
        private DataTable DecompressDataTable(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[1024];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataTable;
                    }
                }
            }
        }

        // DataSet 압축해제
        private DataSet DecompressDataSet(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[1024];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataSet;
                    }
                }
            }
        }

        // Header List 바인딩후 컬럼 재설정
        private void ChangeGrid_Header(string strEvaluationPeriod)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                #region 공통

                // 그리드 컬럼정보 Clear
                this.uGridSRRList.DataSource = null;
                this.uGridSRRList.DisplayLayout.Bands[0].Groups.Clear();
                this.uGridSRRList.DisplayLayout.Bands[0].ResetColumns();

                this.uGridSRRList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRRList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, "굴림");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "ProdType", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Year", "년도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "EvaluationPeriod", "평가기간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "TargetCompany", "대상업체", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                // 컬럼 resizing 가로만 가능하게
                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowRowLayoutLabelSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.Horizontal;

                // 조회리스트 편집불가
                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // 헤더 클릭시 정렬안되도록
                this.uGridSRRList.DisplayLayout.Bands[0].Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowRowLayoutCellSizing = Infragistics.Win.UltraWinGrid.RowLayoutSizing.None;

                #endregion

                //if (this.uGridSRRList.Rows[0].Cells["EvaluationPeriod"].Value.ToString().Equals("MON"))
                string strLangCompany = "";
                string strLangRank = "";
                string strLangScore = "";

                if (m_resSys.GetString("SYS_LANG") == "CHN")
                {
                    strLangCompany = "供应商名称";
                    strLangRank = "排名";
                    strLangScore = "分数";

                }
                else
                {
                    strLangCompany = "업체명";
                    strLangRank = "순위";
                    strLangScore = "점수";
                }

                if(strEvaluationPeriod.Equals("MON"))
                {
                    #region Month

                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup1 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M01", "1月", 6, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup2 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M02", "2月", 9, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup3 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M03", "3月", 12, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup4 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M04", "4月", 15, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup5 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M05", "5月", 18, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup6 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M06", "6月", 21, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup7 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M07", "7月", 24, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup8 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M08", "8月", 27, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup9 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M09", "9月", 30, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup10 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M10", "10月", 33, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup11 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M11", "11月", 36, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup12 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "M12", "12月", 39, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M01_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M01_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M01_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M02_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M02_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M02_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M03_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M03_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M03_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M04_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup4);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M04_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup4);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M04_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup4);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M05_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup5);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M05_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup5);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M05_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup5);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M06_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup6);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M06_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup6);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M06_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup6);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M07_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup7);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M07_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup7);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M07_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup7);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M08_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup8);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M08_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup8);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M08_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup8);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M09_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup9);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M09_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup9);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M09_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup9);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M10_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup10);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M10_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup10);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M10_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup10);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M11_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup11);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M11_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup11);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M11_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup11);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M12_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup12);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M12_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup12);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "M12_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup12);

                    #endregion
                }
                //else if (this.uGridSRRList.Rows[0].Cells["EvaluationPeriod"].Value.ToString().Equals("QUA"))
                else if(strEvaluationPeriod.Equals("QUA"))
                {
                    #region Quater

                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup1 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q01", "1/4", 6, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup2 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q02", "2/4", 9, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup3 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q03", "3/4", 12, 0, 3, 2, false);
                    Infragistics.Win.UltraWinGrid.UltraGridGroup uGridGroup4 = wGrid.mfSetGridGroup(this.uGridSRRList, 0, "Q04", "4/4", 15, 0, 3, 2, false);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q01_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q01_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q01_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup1);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q02_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q02_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q02_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup2);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q03_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q03_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q03_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup3);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q04_C", strLangCompany, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, uGridGroup4);

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q04_R", strLangRank, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, uGridGroup4);                    

                    wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Q04_V", strLangScore, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGridGroup4);

                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // Item List 바인딩후 컬럼 재설정
        private void ChangeGrid_Item()
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                int intRowidx = this.uGridSRRItemList.Rows.Count - 1;

                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings defaultEditorSettings = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                Infragistics.Win.UltraWinEditors.DefaultEditorOwner defaultEditOwner = new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(defaultEditorSettings);
                Infragistics.Win.EditorWithText editorWithText = new Infragistics.Win.EditorWithText(defaultEditOwner);

                if (this.uGridSRRItemList.DisplayLayout.Bands[0].Columns.Exists("M01"))
                {
                    #region Month

                    for (int i = 1; i <= 12; i++)
                    {
                        string strColKey = "M" + string.Format("{0:00}", i);
                        string strColName = i.ToString() + "月";
                        // 컬럼설정
                        wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, strColKey, strColName, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nn.n", "");

                        // Score 컬럼 PomptChar 설정
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].PromptChar = ' ';
                        // Null Value 설정
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].Nullable = Infragistics.Win.UltraWinGrid.Nullable.EmptyString;
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].NullText = "";

                        this.uGridSRRItemList.Rows[intRowidx - 1].Cells[strColKey].Editor = editorWithText;
                        this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Editor = editorWithText;

                        ////string[] strSplit = {"."};
                        ////string[] strRankValue = this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);

                        ////this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Value = strRankValue[0];

                        ////// 대상업체 STS 인 경우 Tab키로 안넘어가도록
                        ////if (this.uTextTargetCompany.Text.Equals("STS"))
                        ////    this.uGridSRRItemList.Rows[intRowidx - 1].Cells[strColKey].TabStop = Infragistics.Win.DefaultableBoolean.False;
                        ////this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].TabStop = Infragistics.Win.DefaultableBoolean.False;
                    }

                    #endregion
                }
                else if (this.uGridSRRItemList.DisplayLayout.Bands[0].Columns.Exists("Q01"))
                {
                    #region Quater

                    for (int i = 1; i <= 4; i++)
                    {
                        string strColKey = "Q" + string.Format("{0:00}", i);
                        string strColName = i.ToString() + "분기";
                        // 컬럼설정
                        wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, strColKey, strColName, false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nn.n", "");

                        // Score 컬럼 PomptChar 설정
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].PromptChar = ' ';
                        // Null Value 설정
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].Nullable = Infragistics.Win.UltraWinGrid.Nullable.Automatic;
                        this.uGridSRRItemList.DisplayLayout.Bands[0].Columns[strColKey].NullText = "";

                        this.uGridSRRItemList.Rows[intRowidx - 1].Cells[strColKey].Editor = editorWithText;
                        this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Editor = editorWithText;

                        ////string[] strSplit = { "." };
                        ////string[] strRankValue = this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);

                        ////this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].Value = strRankValue[0];

                        ////// 대상업체 STS 인 경우 Tab키로 안넘어가도록
                        ////if(this.uTextTargetCompany.Text.Equals("STS"))
                        ////    this.uGridSRRItemList.Rows[intRowidx - 1].Cells[strColKey].TabStop = Infragistics.Win.DefaultableBoolean.False;
                        ////this.uGridSRRItemList.Rows[intRowidx].Cells[strColKey].TabStop = Infragistics.Win.DefaultableBoolean.False;
                    }

                    #endregion
                }

                #region Rank, Score 행 설정

                // Rank 행 Score컬럼값 공백으로 설정
                this.uGridSRRItemList.Rows[intRowidx].Cells["Score"].Value = string.Empty;

                //// 대상업체 STS 인 경우 Score 행 수정불가 -> 수정가능
                //if (this.uComboTargetCompany.SelectedItem.DisplayText.Equals("STS"))
                //{
                //    //this.uGridSRRItemList.Rows[intRowidx - 1].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                //    ////this.uGridSRRItemList.Rows[intRowidx - 1].Appearance.BackColor = Color.FromArgb(153, 153, 204);
                //    ////this.uGridSRRItemList.Rows[intRowidx - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                //}

                // Score 행 색상변경
                this.uGridSRRItemList.Rows[intRowidx - 1].Appearance.BackColor = Color.FromArgb(153, 153, 204);
                this.uGridSRRItemList.Rows[intRowidx - 1].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // Rank 행 수정불가 -> 수정가능
                //this.uGridSRRItemList.Rows[intRowidx].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                this.uGridSRRItemList.Rows[intRowidx].Appearance.BackColor = Color.FromArgb(153, 153, 204);
                this.uGridSRRItemList.Rows[intRowidx].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Draw SearchChart
        private void DrawChart(DataTable dtChart, string strEvaluationPeriod)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                // Chart 초기화
                QRPSTA.STASPC clsSTA = new QRPSTA.STASPC();
                clsSTA.mfInitControlChart(this.uChartSRR);

                // Legend Clear
                this.uChartSRR.CompositeChart.Legends.Clear();

                #region DataTable 설정

                DataTable dtScore = new DataTable();
                DataTable dtRank = new DataTable();

                double dblMax = 0.0;
                double dblMin = double.MaxValue;

                if (strEvaluationPeriod.Equals("MON"))
                {
                    // Score, Rank 테이블 구분
                    dtScore = dtChart.DefaultView.ToTable(false, "TargetCompany", "M01_S", "M02_S", "M03_S", "M04_S", "M05_S", "M06_S", "M07_S", "M08_S", "M09_S", "M10_S", "M11_S", "M12_S");
                    dtRank = dtChart.DefaultView.ToTable(false, "TargetCompany", "M01_R", "M02_R", "M03_R", "M04_R", "M05_R", "M06_R", "M07_R", "M08_R", "M09_R", "M10_R", "M11_R", "M12_R");

                    // Rank STS 만
                    DataRow[] drSTS = dtRank.Select("TargetCompany = 'PSTS'");

                    dtRank = dtRank.Clone();
                    foreach (DataRow dr in drSTS)
                    {
                        dtRank.ImportRow(dr);
                    }

                    // 컬럼명 변경
                    for (int i = 1; i < dtScore.Columns.Count; i++)
                    {
                        /*
                        if (dblMax < Convert.ToDouble(dtScore.Compute("MAX(" + dtScore.Columns[i].ColumnName + ")", "")))
                            dblMax = Convert.ToDouble(dtScore.Compute("MAX(" + dtScore.Columns[i].ColumnName + ")", ""));

                        if (dblMin > Convert.ToDouble(dtScore.Compute("MIN(" + dtScore.Columns[i].ColumnName + ")", "")))
                            dblMin = Convert.ToDouble(dtScore.Compute("MIN(" + dtScore.Columns[i].ColumnName + ")", ""));
                         * */

                        foreach (DataRow _dr in dtScore.Rows)
                        {
                            if(!_dr[dtScore.Columns[i].ColumnName].Equals(DBNull.Value))
                            {
                                if(dblMax < Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]))
                                    dblMax = Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]);

                                if (dblMin > Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]))
                                    dblMin = Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]);
                            }
                        }

                        dtScore.Columns[i].ColumnName = i.ToString() + "月";
                        dtRank.Columns[i].ColumnName = i.ToString() + "月";
                    }
                }
                else if (strEvaluationPeriod.Equals("QUA"))
                {
                    // Score, Rank 테이블 구분
                    dtScore = dtChart.DefaultView.ToTable(false, "TargetCompany", "Q01_S", "Q02_S", "Q03_S", "Q04_S");
                    dtRank = dtChart.DefaultView.ToTable(false, "TargetCompany", "Q01_R", "Q02_R", "Q03_R", "Q04_R");

                    // Rank STS 만
                    DataRow[] drSTS = dtRank.Select("TargetCompany = 'PSTS'");

                    dtRank = dtRank.Clone();
                    foreach (DataRow dr in drSTS)
                    {
                        dtRank.ImportRow(dr);
                    }

                    // 컬럼명 변경
                    for (int i = 1; i < dtScore.Columns.Count; i++)
                    {
                        /*
                        if (dblMax < Convert.ToDouble(dtScore.Compute("MAX(" + dtScore.Columns[i].ColumnName + ")", "")))
                            dblMax = Convert.ToDouble(dtScore.Compute("MAX(" + dtScore.Columns[i].ColumnName + ")", ""));

                        if (dblMin > Convert.ToDouble(dtScore.Compute("MIN(" + dtScore.Columns[i].ColumnName + ")", "")))
                            dblMin = Convert.ToDouble(dtScore.Compute("MIN(" + dtScore.Columns[i].ColumnName + ")", ""));
                        */

                        foreach (DataRow _dr in dtScore.Rows)
                        {
                            if (!_dr[dtScore.Columns[i].ColumnName].Equals(DBNull.Value))
                            {
                                if (dblMax < Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]))
                                    dblMax = Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]);

                                if (dblMin > Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]))
                                    dblMin = Convert.ToDouble(_dr[dtScore.Columns[i].ColumnName]);
                            }
                        }

                        dtScore.Columns[i].ColumnName = i.ToString() + "분기";
                        dtRank.Columns[i].ColumnName = i.ToString() + "분기";
                    }
                }
                
                #endregion

                #region DrawChart

                // Chart 유형 지정
                this.uChartSRR.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Composite;

                Infragistics.UltraChart.Resources.Appearance.ChartArea chartArea = new Infragistics.UltraChart.Resources.Appearance.ChartArea();
                this.uChartSRR.CompositeChart.ChartAreas.Add(chartArea);

                #region Column Chart

                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisColumn = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisColumn.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
                xAxisColumn.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisColumn.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
                xAxisColumn.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                xAxisColumn.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisColumn.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisColumn.Extent = 25;
                xAxisColumn.Visible = true;
                xAxisColumn.Labels.Visible = false;
                xAxisColumn.Labels.SeriesLabels.Visible = true;

                // Y축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem yAxisColumn = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                yAxisColumn.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
                yAxisColumn.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                yAxisColumn.RangeType = Infragistics.UltraChart.Shared.Styles.AxisRangeType.Custom;
                yAxisColumn.NumericAxisType = Infragistics.UltraChart.Shared.Styles.NumericAxisType.Linear;
                yAxisColumn.Extent = 150;
                yAxisColumn.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                yAxisColumn.Labels.ItemFormatString = "<DATA_VALUE:#,##0.#>";
                yAxisColumn.RangeMin = Math.Truncate(dblMin - dblMin * 0.1);
                yAxisColumn.RangeMax = Math.Ceiling(dblMax + dblMax * 0.1);
                yAxisColumn.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.DataInterval;
                Int32 intInterval = (Int32)(dblMax - dblMin) / 10;
                yAxisColumn.TickmarkInterval = intInterval;
                yAxisColumn.Visible = true;

                chartArea.Axes.Add(xAxisColumn);
                chartArea.Axes.Add(yAxisColumn);

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance stackColLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                stackColLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart;
                stackColLayer.ChartArea = chartArea;
                stackColLayer.AxisX = xAxisColumn;
                stackColLayer.AxisY = yAxisColumn;
                this.uChartSRR.CompositeChart.ChartLayers.Add(stackColLayer);

                // Data 설정
                for (int i = 1; i < dtScore.Columns.Count; i++)
                {
                    Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesCol = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                    seriesCol.Label = dtScore.Columns[i].ColumnName;
                    seriesCol.Data.DataSource = dtScore;
                    seriesCol.Data.LabelColumn = "TargetCompany";
                    seriesCol.Data.ValueColumn = dtScore.Columns[i].ColumnName;
                    this.uChartSRR.CompositeChart.Series.Add(seriesCol);
                    //stackColLayer.SwapRowsAndColumns = true;
                    stackColLayer.Series.Add(seriesCol);
                }

                #endregion

                #region Line Chart

                // X축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem xAxisLine = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                xAxisLine.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X2_Axis;
                xAxisLine.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                xAxisLine.Labels.ItemFormatString = "<ITEM_LABEL>";
                xAxisLine.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
                xAxisLine.MajorGridLines.Visible = false;
                xAxisLine.MinorGridLines.Visible = false;
                xAxisLine.Margin.Far.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                xAxisLine.Margin.Near.MarginType = Infragistics.UltraChart.Shared.Styles.LocationType.Percentage;
                if (strEvaluationPeriod.Equals("MON"))
                {
                    xAxisLine.Margin.Far.Value = 4;
                    xAxisLine.Margin.Near.Value = 4;
                }
                else if (strEvaluationPeriod.Equals("QUA"))
                {
                    xAxisLine.Margin.Far.Value = 6;
                    xAxisLine.Margin.Near.Value = 6;
                }
                xAxisLine.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
                xAxisLine.Visible = false;

                // Y축 설정
                Infragistics.UltraChart.Resources.Appearance.AxisItem yAxisLine = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
                yAxisLine.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y2_Axis;
                yAxisLine.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
                yAxisLine.NumericAxisType = Infragistics.UltraChart.Shared.Styles.NumericAxisType.Linear;
                //yAxisLine.Extent = 150;
                yAxisLine.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.Custom;
                //yAxisLine.Labels.ItemFormatString = "<DATA_VALUE:##0>";
                yAxisLine.Labels.ItemFormatString = string.Format("<DATA_VALUE:{0}>", "{}(0:0.#;0.#");
                yAxisLine.Visible = false;
                yAxisLine.RangeType = Infragistics.UltraChart.Shared.Styles.AxisRangeType.Custom;
                yAxisLine.RangeMax = 0;
                yAxisLine.RangeMin = (dtScore.Rows.Count * -1) - 1;
                yAxisLine.TickmarkInterval = 10;

                chartArea.Axes.Add(xAxisLine);
                chartArea.Axes.Add(yAxisLine);

                // Chart 설정
                Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance lineLayer = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
                lineLayer.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.LineChart;
                lineLayer.ChartArea = chartArea;
                lineLayer.AxisX = xAxisLine;
                lineLayer.AxisY = yAxisLine;
                lineLayer.SwapRowsAndColumns = true;
                this.uChartSRR.CompositeChart.ChartLayers.Add(lineLayer);

                // Data 설정
                for (int i = 1; i < dtRank.Columns.Count; i++)
                {
                    Infragistics.UltraChart.Resources.Appearance.NumericSeries seriesLine = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
                    seriesLine.Label = "Rank";
                    seriesLine.Data.DataSource = dtRank;
                    seriesLine.Data.LabelColumn = "TargetCompany";
                    seriesLine.Data.ValueColumn = dtRank.Columns[i].ColumnName;
                    this.uChartSRR.CompositeChart.Series.Add(seriesLine);
                    lineLayer.Series.Add(seriesLine);
                }

                Infragistics.UltraChart.Resources.Appearance.LineChartAppearance appLineChart = new Infragistics.UltraChart.Resources.Appearance.LineChartAppearance();
                appLineChart.NullHandling = Infragistics.UltraChart.Shared.Styles.NullHandling.DontPlot;

                // 선모양 설정
                Infragistics.UltraChart.Resources.Appearance.LineAppearance appLine = new Infragistics.UltraChart.Resources.Appearance.LineAppearance();
                appLine.IconAppearance.Icon = Infragistics.UltraChart.Shared.Styles.SymbolIcon.Circle;
                appLine.IconAppearance.IconSize = Infragistics.UltraChart.Shared.Styles.SymbolIconSize.Small;
                appLine.LineStyle.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Solid;
                appLine.LineStyle.EndStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;
                appLine.LineStyle.StartStyle = Infragistics.UltraChart.Shared.Styles.LineCapStyle.NoAnchor;
                

                // Text 설정
                Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[] appChartText = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance[12];
                for (int j = 0; j < 12; j++)
                {
                    for (int i = 0; i < appChartText.Length; i++)
                    {
                        appChartText[i] = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance();
                        appChartText[i].HorizontalAlign = StringAlignment.Center;
                        appChartText[i].VerticalAlign = StringAlignment.Far;
                        appChartText[i].FontColor = Color.Black;
                        appChartText[i].ItemFormatString = string.Format("<DATA_VALUE:{0}>", "{}(0:0.#;0.#");
                        appChartText[i].Visible = true;
                        appChartText[i].Row = j;
                        appChartText[i].Column = i;
                        Font font = new Font(m_resSys.GetString("SYS_FONTNAME"), 8, FontStyle.Bold);
                        appChartText[i].ChartTextFont = font;
                        appLineChart.ChartText.Add(appChartText[i]);
                    }
                }
                appLineChart.LineAppearances.Add(appLine);
                lineLayer.ChartTypeAppearance = appLineChart;

                #endregion

                #region 범주 설정

                // Legend 설정
                Infragistics.UltraChart.Resources.Appearance.CompositeLegend Legend = new Infragistics.UltraChart.Resources.Appearance.CompositeLegend();
                Legend.ChartLayers.Add(stackColLayer);
                Legend.ChartLayers.Add(lineLayer);
                Legend.Bounds = new Rectangle(1, 5, 10, 90);
                Legend.BoundsMeasureType = Infragistics.UltraChart.Shared.Styles.MeasureType.Percentage;
                Legend.PE.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.Gradient;
                Legend.PE.FillGradientStyle = Infragistics.UltraChart.Shared.Styles.GradientStyle.ForwardDiagonal;
                Legend.PE.Fill = Color.CornflowerBlue;
                Legend.PE.FillStopColor = Color.Transparent;
                Legend.Border.CornerRadius = 10;
                Legend.Border.Thickness = 0;
                this.uChartSRR.CompositeChart.Legends.Add(Legend);

                #endregion

                // Tooltip 설정
                this.uChartSRR.Tooltips.Display = Infragistics.UltraChart.Shared.Styles.TooltipDisplay.MouseMove;
                this.uChartSRR.Tooltips.Format = Infragistics.UltraChart.Shared.Styles.TooltipStyle.DataValue;

                #region Color Model 설정

                this.uChartSRR.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;


                Color[] ChartColors;
                ChartColors = new Color[] { Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.Blue, Color.Indigo, Color.Violet
                                            , Color.AliceBlue, Color.Aquamarine, Color.Beige, Color.Brown, Color.BurlyWood, Color.CadetBlue
                                            , Color.Chartreuse, Color.Chocolate, Color.CornflowerBlue, Color.Crimson, Color.Cyan, Color.DarkBlue, Color.DarkCyan
                                            , Color.DarkGoldenrod, Color.DarkGray, Color.DarkGreen, Color.DarkKhaki, Color.DarkMagenta, Color.DarkOrange
                                            , Color.DarkRed, Color.DarkSalmon, Color.DarkSlateBlue, Color.DarkTurquoise, Color.DeepPink, Color.DeepSkyBlue
                                            , Color.DimGray, Color.DodgerBlue, Color.ForestGreen, Color.Gold, Color.GreenYellow, Color.HotPink, Color.IndianRed
                                            , Color.Lavender, Color.LawnGreen, Color.LemonChiffon, Color.LightBlue, Color.LightCoral, Color.LightCyan
                                            , Color.LightGreen, Color.LightPink, Color.LightSalmon, Color.LightSeaGreen, Color.LightYellow};
                uChartSRR.ColorModel.CustomPalette = ChartColors; 

                #endregion

                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        #endregion

        #region Events...

        // 검색조건 공장콤보 값변화 이벤트
        void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "REG", m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboSearchCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 공장콤보 값변경 이벤트
        void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "MASTER", m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboCustomerProdType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 검색조건 고객사 구분 콤보 Refresh
        void uComboSearchCustomer_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                return;

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "REG", m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchCustomer.Items.Clear();

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboSearchCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 고객사 구분 콤보 Refresh
        void uComboCustomerProdType_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (this.uComboCustomerProdType.SelectedIndex.Equals(-1))
                return;
            else if (this.uComboCustomerProdType.ReadOnly)
                return;

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "MASTER", m_resSys.GetString("SYS_LANG"));

                    this.uComboCustomerProdType.Items.Clear();

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboCustomerProdType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // Contents Area 접힘상태 변화 이벤트
        void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            if (this.uGroupBoxContentsArea.Expanded)
            {
                this.uGridSRRList.Rows.FixedRows.Clear();
                Clear();

                this.uChartSRR.Show();
            }
            else
            {
                this.uChartSRR.Hide();
            }
        }

        // Load 버튼 클릭 이벤트
        void uButtonLoad_Click(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
            DialogResult result = new DialogResult();

            #region Check Value

            ////if (this.uComboPlant.SelectedIndex <= 0)
            ////    return;
            ////else if (this.uTextYear.Text.Equals(string.Empty) || this.uTextYear.Text.Length < 4)
            ////    return;
            ////else if (this.uComboCustomerProdType.SelectedIndex <= 0)
            ////    return;
            ////else if (this.uTextTargetCompany.Text.Equals(string.Empty))
            ////    return;
            //////else if (this.uComboEvaluationPeriod.SelectedIndex <= 0)
            //////    return;

            if (this.uComboPlant.SelectedIndex <= 0)
            {
                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001234", "M000266", Infragistics.Win.HAlign.Right);

                this.uComboPlant.Focus();
                this.uComboPlant.DropDown();
                m_resSys.Close();
                return;
            }
            else if (this.uTextYear.Text.Equals(string.Empty))
            {
                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001234", "M000813", Infragistics.Win.HAlign.Right);

                this.uTextYear.Focus();
                m_resSys.Close();
                return;
            }
            else if (this.uComboCustomerProdType.SelectedIndex <= 0)
            {
                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001234", "M001536", Infragistics.Win.HAlign.Right);

                this.uComboCustomerProdType.Focus();
                this.uComboCustomerProdType.DropDown();
                m_resSys.Close();
                return;
            }
            else if (this.uComboTargetCompany.SelectedIndex <= 0)
            {
                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001234", "M001537", Infragistics.Win.HAlign.Right);

                this.uComboTargetCompany.Focus();
                this.uComboTargetCompany.DropDown();
                m_resSys.Close();
                return;
            }

            #endregion

            try
            {
                #region Set Search Values

                // 변수 설정
                string[] strSplit = { "||" };
                string strPlantCode = this.uComboPlant.Value.ToString();
                string[] strCustomerProdType = this.uComboCustomerProdType.Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                string strCustomerCode = string.Empty;
                string strProdType = string.Empty;
                if (strCustomerProdType.Length >= 1)
                {
                    strCustomerCode = strCustomerProdType[0];

                    if(strCustomerProdType.Length > 1)
                        strProdType = strCustomerProdType[1];
                }
                //string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                string strEvaluationPeriod = string.Empty;
                string strYear = this.uTextYear.Text;
                string strTargetCompany = this.uComboTargetCompany.SelectedItem.DisplayText.TrimEnd().TrimStart();

                #endregion

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRItem), "SRRItem");
                using (QRPQAT.BL.QATCLM.SRRItem clsSRRItem = new QRPQAT.BL.QATCLM.SRRItem())
                {
                    string strRtn = clsSRRItem.mfReadQATSRRItem(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod
                        , strYear, strTargetCompany, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        if (!string.IsNullOrEmpty(strRtn))
                        {
                            #region Binary
                            /*
                            using (DataTable dtSRRItem = DecompressDataTable(ErrRtn.mfGetReturnValue(0)))
                            {
                                // QRPDB Select 시 OUTPUT 값 못받음
                                ////string strRtnEvaluationPeriod = ErrRtn.mfGetReturnValue(0);
                                ////string strState = ErrRtn.mfGetReturnValue(1);

                                #region Set Score, Rank Row

                                // Score, Rank 행 설정
                                if (!(dtSRRItem.Select("EvaluationItem = 'Score'").Length > 0))
                                {
                                    // Rank, Score 설정
                                    DataRow drScore = dtSRRItem.NewRow();
                                    DataRow drRank = dtSRRItem.NewRow();
                                    drScore["EvaluationType"] = "-";
                                    drScore["EvaluationItem"] = "Score";
                                    if (dtSRRItem.Rows.Count.Equals(0))
                                        drScore["Score"] = "0.0";
                                    else
                                    {
                                        decimal dblTotal = 0.0m;
                                        foreach (DataRow _dr in dtSRRItem.Rows)
                                        {
                                            if (!_dr["Score"].ToString().Equals(string.Empty))
                                                dblTotal += Convert.ToDecimal(_dr["Score"]);
                                        }

                                        drScore["Score"] = dblTotal.ToString();

                                        //drScore["Score"] = dtSRRItem.Compute("SUM(Score)", string.Empty);
                                    }

                                    drRank["EvaluationType"] = "-";
                                    drRank["EvaluationItem"] = "Rank";
                                    drRank["Score"] = "";
                                    if (dtSRRItem.Columns.Contains("M01"))
                                    {
                                        this.uComboEvaluationPeriod.Value = "MON";

                                        ////drScore["M01"] = 0.0m;
                                        ////drScore["M02"] = 0.0m;
                                        ////drScore["M03"] = 0.0m;
                                        ////drScore["M04"] = 0.0m;
                                        ////drScore["M05"] = 0.0m;
                                        ////drScore["M06"] = 0.0m;
                                        ////drScore["M07"] = 0.0m;
                                        ////drScore["M08"] = 0.0m;
                                        ////drScore["M09"] = 0.0m;
                                        ////drScore["M10"] = 0.0m;
                                        ////drScore["M11"] = 0.0m;
                                        ////drScore["M12"] = 0.0m;

                                        ////drRank["M01"] = 0.0m;
                                        ////drRank["M02"] = 0.0m;
                                        ////drRank["M03"] = 0.0m;
                                        ////drRank["M04"] = 0.0m;
                                        ////drRank["M05"] = 0.0m;
                                        ////drRank["M06"] = 0.0m;
                                        ////drRank["M07"] = 0.0m;
                                        ////drRank["M08"] = 0.0m;
                                        ////drRank["M09"] = 0.0m;
                                        ////drRank["M10"] = 0.0m;
                                        ////drRank["M11"] = 0.0m;
                                        ////drRank["M12"] = 0.0m;

                                        drScore["M01"] = string.Empty;
                                        drScore["M02"] = string.Empty;
                                        drScore["M03"] = string.Empty;
                                        drScore["M04"] = string.Empty;
                                        drScore["M05"] = string.Empty;
                                        drScore["M06"] = string.Empty;
                                        drScore["M07"] = string.Empty;
                                        drScore["M08"] = string.Empty;
                                        drScore["M09"] = string.Empty;
                                        drScore["M10"] = string.Empty;
                                        drScore["M11"] = string.Empty;
                                        drScore["M12"] = string.Empty;

                                        drRank["M01"] = string.Empty;
                                        drRank["M02"] = string.Empty;
                                        drRank["M03"] = string.Empty;
                                        drRank["M04"] = string.Empty;
                                        drRank["M05"] = string.Empty;
                                        drRank["M06"] = string.Empty;
                                        drRank["M07"] = string.Empty;
                                        drRank["M08"] = string.Empty;
                                        drRank["M09"] = string.Empty;
                                        drRank["M10"] = string.Empty;
                                        drRank["M11"] = string.Empty;
                                        drRank["M12"] = string.Empty;
                                    }
                                    else if (dtSRRItem.Columns.Contains("Q01"))
                                    {
                                        this.uComboEvaluationPeriod.Value = "QUA";

                                        ////drScore["Q01"] = 0.0m;
                                        ////drScore["Q02"] = 0.0m;
                                        ////drScore["Q03"] = 0.0m;
                                        ////drScore["Q04"] = 0.0m;

                                        ////drRank["Q01"] = 0.0m;
                                        ////drRank["Q02"] = 0.0m;
                                        ////drRank["Q03"] = 0.0m;
                                        ////drRank["Q04"] = 0.0m;

                                        drScore["Q01"] = string.Empty;
                                        drScore["Q02"] = string.Empty;
                                        drScore["Q03"] = string.Empty;
                                        drScore["Q04"] = string.Empty;

                                        drRank["Q01"] = string.Empty;
                                        drRank["Q02"] = string.Empty;
                                        drRank["Q03"] = string.Empty;
                                        drRank["Q04"] = string.Empty;
                                    }

                                    dtSRRItem.Rows.Add(drScore);
                                    dtSRRItem.Rows.Add(drRank);
                                }
                                else
                                {
                                    // 기존데이터 저장된 경우
                                    if (dtSRRItem.Columns.Contains("M01"))
                                        this.uComboEvaluationPeriod.Value = "MON";
                                    else if (dtSRRItem.Columns.Contains("Q01"))
                                        this.uComboEvaluationPeriod.Value = "QUA";
                                }

                                #endregion

                                this.uGridSRRItemList.SetDataBinding(dtSRRItem.Copy(), string.Empty);

                                // 컬럼 설정 메소드 호출
                                ChangeGrid_Item();

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomerProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;
                                this.uTextYear.ReadOnly = true;
                                this.uTextTargetCompany.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomerProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;
                                this.uTextYear.Appearance.BackColor = Color.Gainsboro;
                                this.uTextTargetCompany.Appearance.BackColor = Color.Gainsboro;

                                // 랭크 설정을 위한 정보 조회
                                string strRankInfo = clsSRRItem.mfReadQATSRRItem_ForRank(strPlantCode, strCustomerCode, strProdType
                                    , strYear, strTargetCompany, m_resSys.GetString("SYS_LANG"));

                                ErrRtn = ErrRtn.mfDecodingErrMessage(strRankInfo);
                                if (ErrRtn.ErrNum.Equals(0))
                                {
                                    using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                                    {
                                        dtRankInfo.BeginLoadData();
                                        dtRankInfo.ReadXml(xmlSR);
                                        dtRankInfo.EndLoadData();
                                    }
                                }
                                else
                                {
                                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                                    {
                                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001115", "M001539", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                                            , ErrRtn.SystemInnerException + "<br/>" +
                                                ErrRtn.SystemMessage + "<br/>" +
                                                ErrRtn.SystemStackTrace
                                            , Infragistics.Win.HAlign.Right);
                                    }
                                }
                            }
                            */
                            #endregion

                            #region XML
                            using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                            {
                                using (DataTable dtSRRItem = new DataTable())
                                {
                                    dtSRRItem.BeginLoadData();
                                    dtSRRItem.ReadXml(xmlSR);
                                    dtSRRItem.EndLoadData();
                                    // QRPDB Select 시 OUTPUT 값 못받음
                                    ////string strRtnEvaluationPeriod = ErrRtn.mfGetReturnValue(0);
                                    ////string strState = ErrRtn.mfGetReturnValue(1);

                                    #region Set Score, Rank Row

                                    // Score, Rank 행 설정
                                    if (!(dtSRRItem.Select("EvaluationItem = 'Score'").Length > 0))
                                    {
                                        // Rank, Score 설정
                                        DataRow drScore = dtSRRItem.NewRow();
                                        DataRow drRank = dtSRRItem.NewRow();
                                        drScore["EvaluationType"] = "-";
                                        drScore["EvaluationItem"] = "Score";
                                        if (dtSRRItem.Rows.Count.Equals(0))
                                            drScore["Score"] = "0.0";
                                        else
                                        {
                                            decimal dblTotal = 0.0m;
                                            foreach (DataRow _dr in dtSRRItem.Rows)
                                            {
                                                if (!_dr["Score"].ToString().Equals(string.Empty))
                                                    dblTotal += Convert.ToDecimal(_dr["Score"]);
                                            }

                                            drScore["Score"] = dblTotal.ToString();

                                            //drScore["Score"] = dtSRRItem.Compute("SUM(Score)", string.Empty);
                                        }

                                        drRank["EvaluationType"] = "-";
                                        drRank["EvaluationItem"] = "Rank";
                                        drRank["Score"] = "";
                                        if (dtSRRItem.Columns.Contains("M01"))
                                        {
                                            this.uComboEvaluationPeriod.Value = "MON";

                                            ////drScore["M01"] = 0.0m;
                                            ////drScore["M02"] = 0.0m;
                                            ////drScore["M03"] = 0.0m;
                                            ////drScore["M04"] = 0.0m;
                                            ////drScore["M05"] = 0.0m;
                                            ////drScore["M06"] = 0.0m;
                                            ////drScore["M07"] = 0.0m;
                                            ////drScore["M08"] = 0.0m;
                                            ////drScore["M09"] = 0.0m;
                                            ////drScore["M10"] = 0.0m;
                                            ////drScore["M11"] = 0.0m;
                                            ////drScore["M12"] = 0.0m;

                                            ////drRank["M01"] = 0.0m;
                                            ////drRank["M02"] = 0.0m;
                                            ////drRank["M03"] = 0.0m;
                                            ////drRank["M04"] = 0.0m;
                                            ////drRank["M05"] = 0.0m;
                                            ////drRank["M06"] = 0.0m;
                                            ////drRank["M07"] = 0.0m;
                                            ////drRank["M08"] = 0.0m;
                                            ////drRank["M09"] = 0.0m;
                                            ////drRank["M10"] = 0.0m;
                                            ////drRank["M11"] = 0.0m;
                                            ////drRank["M12"] = 0.0m;

                                            drScore["M01"] = string.Empty;
                                            drScore["M02"] = string.Empty;
                                            drScore["M03"] = string.Empty;
                                            drScore["M04"] = string.Empty;
                                            drScore["M05"] = string.Empty;
                                            drScore["M06"] = string.Empty;
                                            drScore["M07"] = string.Empty;
                                            drScore["M08"] = string.Empty;
                                            drScore["M09"] = string.Empty;
                                            drScore["M10"] = string.Empty;
                                            drScore["M11"] = string.Empty;
                                            drScore["M12"] = string.Empty;

                                            drRank["M01"] = string.Empty;
                                            drRank["M02"] = string.Empty;
                                            drRank["M03"] = string.Empty;
                                            drRank["M04"] = string.Empty;
                                            drRank["M05"] = string.Empty;
                                            drRank["M06"] = string.Empty;
                                            drRank["M07"] = string.Empty;
                                            drRank["M08"] = string.Empty;
                                            drRank["M09"] = string.Empty;
                                            drRank["M10"] = string.Empty;
                                            drRank["M11"] = string.Empty;
                                            drRank["M12"] = string.Empty;

                                            this.uCheckM01.Text = "1월";
                                            this.uCheckM02.Text = "2월";
                                            this.uCheckM03.Text = "3월";
                                            this.uCheckM04.Text = "4월";
                                            this.uCheckM05.Text = "5월";
                                            this.uCheckM06.Text = "6월";
                                            this.uCheckM07.Text = "7월";
                                            this.uCheckM08.Text = "8월";
                                            this.uCheckM09.Text = "9월";
                                            this.uCheckM10.Text = "10월";
                                            this.uCheckM11.Text = "11월";
                                            this.uCheckM12.Text = "12월";

                                            this.uCheckM05.Visible = true;
                                            this.uCheckM06.Visible = true;
                                            this.uCheckM07.Visible = true;
                                            this.uCheckM08.Visible = true;
                                            this.uCheckM09.Visible = true;
                                            this.uCheckM10.Visible = true;
                                            this.uCheckM11.Visible = true;
                                            this.uCheckM12.Visible = true;
                                        }
                                        else if (dtSRRItem.Columns.Contains("Q01"))
                                        {
                                            this.uComboEvaluationPeriod.Value = "QUA";

                                            ////drScore["Q01"] = 0.0m;
                                            ////drScore["Q02"] = 0.0m;
                                            ////drScore["Q03"] = 0.0m;
                                            ////drScore["Q04"] = 0.0m;

                                            ////drRank["Q01"] = 0.0m;
                                            ////drRank["Q02"] = 0.0m;
                                            ////drRank["Q03"] = 0.0m;
                                            ////drRank["Q04"] = 0.0m;

                                            drScore["Q01"] = string.Empty;
                                            drScore["Q02"] = string.Empty;
                                            drScore["Q03"] = string.Empty;
                                            drScore["Q04"] = string.Empty;

                                            drRank["Q01"] = string.Empty;
                                            drRank["Q02"] = string.Empty;
                                            drRank["Q03"] = string.Empty;
                                            drRank["Q04"] = string.Empty;

                                            this.uCheckM01.Text = "1분기";
                                            this.uCheckM02.Text = "2분기";
                                            this.uCheckM03.Text = "3분기";
                                            this.uCheckM04.Text = "4분기";

                                            this.uCheckM05.Visible = false;
                                            this.uCheckM06.Visible = false;
                                            this.uCheckM07.Visible = false;
                                            this.uCheckM08.Visible = false;
                                            this.uCheckM09.Visible = false;
                                            this.uCheckM10.Visible = false;
                                            this.uCheckM11.Visible = false;
                                            this.uCheckM12.Visible = false;

                                        }

                                        dtSRRItem.Rows.Add(drScore);
                                        dtSRRItem.Rows.Add(drRank);
                                    }
                                    else
                                    {
                                        // 기존데이터 저장된 경우
                                        if (dtSRRItem.Columns.Contains("M01"))
                                            this.uComboEvaluationPeriod.Value = "MON";
                                        else if (dtSRRItem.Columns.Contains("Q01"))
                                            this.uComboEvaluationPeriod.Value = "QUA";
                                    }

                                    #endregion

                                    this.uGridSRRItemList.SetDataBinding(dtSRRItem.Copy(), string.Empty);

                                    // 컬럼 설정 메소드 호출
                                    ChangeGrid_Item();

                                    this.uComboPlant.ReadOnly = true;
                                    this.uComboCustomerProdType.ReadOnly = true;
                                    this.uComboEvaluationPeriod.ReadOnly = true;
                                    this.uTextYear.ReadOnly = true;
                                    this.uComboTargetCompany.ReadOnly = true;

                                    this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboCustomerProdType.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;
                                    this.uTextYear.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboTargetCompany.Appearance.BackColor = Color.Gainsboro;

                                    // 랭크 설정을 위한 정보 조회
                                    string strRankInfo = clsSRRItem.mfReadQATSRRItem_ForRank(strPlantCode, strCustomerCode, strProdType
                                        , strYear, strTargetCompany, m_resSys.GetString("SYS_LANG"));

                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRankInfo);
                                    if (ErrRtn.ErrNum.Equals(0))
                                    {
                                        using (System.IO.StringReader xmlRank = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                                        {
                                            dtRankInfo.BeginLoadData();
                                            dtRankInfo.ReadXml(xmlRank);
                                            dtRankInfo.EndLoadData();
                                        }
                                    }
                                    else
                                    {
                                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                                        {
                                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001135", "M001115", "M001539", Infragistics.Win.HAlign.Right);
                                        }
                                        else
                                        {
                                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                                                , ErrRtn.SystemInnerException + "<br/>" +
                                                    ErrRtn.SystemMessage + "<br/>" +
                                                    ErrRtn.SystemStackTrace
                                                , Infragistics.Win.HAlign.Right);
                                        }
                                    }
                                }
                            }

                            #endregion

                            if (this.uGridSRRItemList.Rows.Count.Equals(0))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                
                                this.uGridSRRItemList.Focus();
                                if (this.uComboEvaluationPeriod.Value.ToString().Equals("MON"))
                                    this.uGridSRRItemList.Rows[0].Cells["M01"].Activate();
                                else if(this.uComboEvaluationPeriod.Value.ToString().Equals("QUA"))
                                    this.uGridSRRItemList.Rows[0].Cells["Q01"].Activate();
                                this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                            , ErrRtn.SystemInnerException + "<br/>" +
                                ErrRtn.SystemMessage + "<br/>" +
                                ErrRtn.SystemStackTrace
                            , Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 년도 텍스트박스 숫자만 입력가능(검색조건)
        void uTextSearchYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBox = sender as Infragistics.Win.UltraWinEditors.UltraTextEditor;
            if (!(e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                if (!(char.IsDigit(e.KeyChar)))
                {
                    e.Handled = true;
                }
            }
        }

        // 년도 텍스트박스 숫자만 입력가능
        void uTextYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBox = sender as Infragistics.Win.UltraWinEditors.UltraTextEditor;
            if (!(e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                if (!(char.IsDigit(e.KeyChar)))
                {
                    e.Handled = true;
                }
            }
        }

        // SRR 상세정보 그리드 숫자만 입력가능 -> MaskInput 안먹음;;;;;;
        void uGridSRRItemList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (this.uGridSRRItemList.ActiveCell == null)
                    return;

                int intActiveRowIdx = this.uGridSRRItemList.ActiveCell.Row.Index;
                int intActiveColIdx = this.uGridSRRItemList.ActiveCell.Column.Index;

                // 마지막줄 체크
                if (this.uGridSRRItemList.Rows.Count.Equals(intActiveRowIdx + 1))
                {
                    // 마지막 컬럼 체크
                    if (this.uGridSRRItemList.DisplayLayout.Bands[0].Columns.Count.Equals(intActiveColIdx + 1))
                    {
                        if (this.uGridSRRItemList.DisplayLayout.Bands[0].Columns.Exists("M01"))
                            this.uGridSRRItemList.Rows[0].Cells["M01"].Activate();
                        else if (this.uGridSRRItemList.DisplayLayout.Bands[0].Columns.Exists("Q01"))
                            this.uGridSRRItemList.Rows[0].Cells["Q01"].Activate();
                    }
                    else
                    {
                        this.uGridSRRItemList.Rows[0].Cells[intActiveColIdx + 1].Activate();
                    }
                }
                else
                {
                    this.uGridSRRItemList.Rows[intActiveRowIdx + 1].Cells[intActiveColIdx].Activate();
                }
                this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            }
            else if (!(e.KeyChar == Convert.ToChar(Keys.Back)) && !(e.KeyChar == '.'))
            {
                if (!(char.IsDigit(e.KeyChar)))
                {
                    e.Handled = true;
                }
            }
        }

        // SRR 상세정보 Cell Update 이벤트
        void uGridSRRItemList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            this.uGridSRRItemList.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSRRItemList_AfterCellUpdate);
            try
            {
                if (!e.Cell.Column.Key.Equals("EvaluationType") &&
                    !e.Cell.Column.Key.Equals("EvaluationItem") &&
                    !e.Cell.Column.Key.Equals("Score"))
                {
                    string strColKey = e.Cell.Column.Key;
                    this.uGridSRRItemList.UpdateData();
                    DataTable dtItem = (DataTable)this.uGridSRRItemList.DataSource;

                    int intRowIdx = dtItem.Rows.Count - 1;

                    // Rank, Score 행이 아닌경우 Total 점수 계산
                    if (e.Cell.Row.Index < (intRowIdx - 1))
                    {
                        ////// 에러
                        ////dtItem.Rows[intRowIdx - 1][strColKey] = dtItem.Compute("SUM(" + strColKey + ")", "EvaluationItem <> 'Score' AND EvaluationItem <> 'Rank' AND " + strColKey + " <> ''");

                        ////// 에러
                        ////DataRow[] _drs = dtItem.Select("EvaluationItem <> 'Score' AND EvaluationItem <> 'Rank' AND " + strColKey + " <> ''");
                        ////dtItem.Rows[intRowIdx - 1][strColKey] = _drs.Sum(row => row.Field<decimal>(strColKey));

                        decimal dblTotal = 0.0m;
                        DataRow[] _drs = dtItem.Select("EvaluationItem <> 'Score' AND EvaluationItem <> 'Rank' AND " + strColKey + " <> ''");
                        //foreach (DataRow _dr in dtItem.Rows)
                        foreach (DataRow _dr in _drs)
                        {
                            if (!_dr[strColKey].ToString().Equals(string.Empty) &&
                                !_dr["EvaluationItem"].ToString().Equals("Score") &&
                                !_dr["EvaluationItem"].ToString().Equals("Rank"))
                                dblTotal += Convert.ToDecimal(_dr[strColKey]);
                        }

                        if (_drs.Length > 0)
                            dtItem.Rows[intRowIdx - 1][strColKey] = dblTotal.ToString();
                        else
                        {
                            dtItem.Rows[intRowIdx - 1][strColKey] = string.Empty;
                            dtItem.Rows[intRowIdx][strColKey] = string.Empty;
                        }
                    }

                    // Rank 행이 아닌경우 Rank 계산
                    //if (e.Cell.Row.Index < intRowIdx) ==> 저장후 Rank 계산 SP 돌기때문에 입력한대로 Rank 저장 불가
                    //{
                        if (!dtItem.Rows[intRowIdx - 1][strColKey].ToString().Equals(string.Empty))
                        {
                            // Rank 처리
                            decimal dblScore = Convert.ToDecimal(dtItem.Rows[intRowIdx - 1][strColKey]);

                            int intRank = 1;
                            if (dtRankInfo.Rows.Count > 0)
                            {
                                if (dtRankInfo.Columns.Contains("PeriodType"))
                                {
                                    DataRow[] drRanks = dtRankInfo.Select("PeriodType = '" + strColKey + "'");
                                    foreach (DataRow _drRk in drRanks)
                                    {
                                        if (_drRk["Value"].ToString().Equals(string.Empty))
                                            continue;
                                        else if (Convert.ToDecimal(_drRk["Value"]) > dblScore)
                                            intRank++;
                                    }

                                    dtItem.Rows[intRowIdx][strColKey] = intRank.ToString();
                                }
                                else
                                {
                                    MessageBox.Show("RankInfo PeriodType Column not exists");
                                }
                            }
                            else
                            {
                                dtItem.Rows[intRowIdx][strColKey] = intRank.ToString();
                            }
                        //}
                    }

                    #region Rank 기존소스 주석처리 -> 분기값의 순위가 아니고 다른업체와 Total 점수로 Rank 계산됨.
                    /*
                    object[] Score = dtItem.Rows[intRowIdx - 1].ItemArray;

                    if (this.uComboEvaluationPeriod.Value.ToString().Equals("MON"))
                    {
                        int[] Rank = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

                        for (int i = 3; i < Score.Length; i++)
                        {
                            for (int j = 3; j < Score.Length; j++)
                            {
                                if (decimal.Parse(Score[i].ToString()) < decimal.Parse(Score[j].ToString()))
                                    Rank[i - 3]++;
                            }
                        }

                        for (int i = 3; i < Score.Length; i++)
                        {
                            string strCol = "M" + string.Format("{0:00}", i - 2);
                            dtItem.Rows[intRowIdx][strCol] = Rank[i - 3];
                        }
                    }
                    else if(this.uComboEvaluationPeriod.Value.ToString().Equals("QUA"))
                    {
                        int[] Rank = { 1, 1, 1, 1 };

                        for (int i = 3; i < Score.Length; i++)
                        {
                            for (int j = 3; j < Score.Length; j++)
                            {
                                if (decimal.Parse(Score[i].ToString()) < decimal.Parse(Score[j].ToString()))
                                    Rank[i - 3]++;
                            }
                        }

                        for (int i = 3; i < Score.Length; i++)
                        {
                            string strCol = "Q" + string.Format("{0:00}", i  - 2);
                            dtItem.Rows[intRowIdx][strCol] = Rank[i - 3];
                        }
                    }
                     * */
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                this.uGridSRRItemList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSRRItemList_AfterCellUpdate);
            }
        }

        void uGridSRRList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                // 분기 or 월 컬럼 클릭시에만
                if (e.Cell.Column.Header.Group != null) //&& !e.Cell.Value.ToString().Equals(string.Empty))
                {
                    // Fix Cliked Row
                    e.Cell.Row.Fixed = true;

                    string strColTargetCompany = e.Cell.Column.Key.Substring(0, 4) + "C";

                    // 변수 설정
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strCustomerCode = e.Cell.Row.Cells["CustomerCode"].Value.ToString();
                    string strProdType = e.Cell.Row.Cells["ProdType"].Value.ToString();
                    string strEvaluationPeriod = e.Cell.Row.Cells["EvaluationPeriod"].Value.ToString();
                    string strYear = e.Cell.Row.Cells["Year"].Value.ToString();
                    string strTargetCompany = e.Cell.Row.Cells[strColTargetCompany].Value.ToString();

                    // Connect BL
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRHeader), "SRRHeader");
                    using (QRPQAT.BL.QATCLM.SRRHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRHeader())
                    {
                        brwChannel.mfCredentials(clsSRR_H);

                        string strSearch = clsSRR_H.mfReadQATSRRHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
                            , strEvaluationPeriod, strTargetCompany, strYear, m_resSys.GetString("SYS_LANG"));

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            #region Binary 이용
                            /*
                            using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
                            {
                                // Header
                                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                this.uComboCustomerProdType.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString() + "||" +
                                                                    dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                this.uTextYear.Text = dsSRR_Info.Tables["Header"].Rows[0]["Year"].ToString();
                                this.uTextTargetCompany.Text = dsSRR_Info.Tables["Header"].Rows[0]["TargetCompany"].ToString();

                                // Item
                                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                if (this.uGridSRRItemList.Rows.Count > 0)
                                {
                                    // 그리드 컬렴명 재설정
                                    ChangeGrid_Item();

                                    // 컬럼 폭 자동설정
                                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);

                                    // 랭크 설정을 위한 정보 조회
                                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRItem), "SRRItem");
                                    using (QRPQAT.BL.QATCLM.SRRItem clsSRRItem = new QRPQAT.BL.QATCLM.SRRItem())
                                    {
                                        brwChannel.mfCredentials(clsSRRItem);

                                        string strRankInfo = clsSRRItem.mfReadQATSRRItem_ForRank(strPlantCode, strCustomerCode, strProdType
                                            , strYear, strTargetCompany, m_resSys.GetString("SYS_LANG"));

                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strRankInfo);
                                        if (ErrRtn.ErrNum.Equals(0))
                                        {
                                            using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                                            {
                                                dtRankInfo.BeginLoadData();
                                                dtRankInfo.ReadXml(xmlSR);
                                                dtRankInfo.EndLoadData();
                                            }
                                        }
                                    }
                                }

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomerProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;
                                this.uTextYear.ReadOnly = true;
                                this.uTextTargetCompany.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomerProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;
                                this.uTextYear.Appearance.BackColor = Color.Gainsboro;
                                this.uTextTargetCompany.Appearance.BackColor = Color.Gainsboro;

                                this.uButtonLoad.Hide();

                                this.uGroupBoxContentsArea.Expanded = true;
                            }
                            */
                            #endregion

                            #region XML

                            using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                            {
                                using (DataSet dsSRR_Info = new DataSet())
                                {
                                    foreach (DataTable dt in dsSRR_Info.Tables)
                                        dt.BeginLoadData();
                                    dsSRR_Info.ReadXml(xmlSR);
                                    foreach (DataTable dt in dsSRR_Info.Tables)
                                        dt.EndLoadData();

                                    // Header
                                    this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                    this.uComboCustomerProdType.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString() + "||" +
                                                                        dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                    this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                    this.uTextYear.Text = dsSRR_Info.Tables["Header"].Rows[0]["Year"].ToString();
                                    this.uComboTargetCompany.Text = dsSRR_Info.Tables["Header"].Rows[0]["TargetCompany"].ToString();
                                    this.uCheckM01.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M01"].ToString());
                                    this.uCheckM02.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M02"].ToString());
                                    this.uCheckM03.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M03"].ToString());
                                    this.uCheckM04.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M04"].ToString());
                                    this.uCheckM05.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M05"].ToString());
                                    this.uCheckM06.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M06"].ToString());
                                    this.uCheckM07.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M07"].ToString());
                                    this.uCheckM08.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M08"].ToString());
                                    this.uCheckM09.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M09"].ToString());
                                    this.uCheckM10.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M10"].ToString());
                                    this.uCheckM11.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M11"].ToString());
                                    this.uCheckM12.Checked = Convert.ToBoolean(dsSRR_Info.Tables["Header"].Rows[0]["M12"].ToString());

                                    // Item
                                    this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                    if (this.uGridSRRItemList.Rows.Count > 0)
                                    {
                                        // 그리드 컬렴명 재설정
                                        ChangeGrid_Item();

                                        if (dsSRR_Info.Tables["Item"].Columns.Contains("M01"))
                                        {
                                            this.uCheckM01.Text = "1월";
                                            this.uCheckM02.Text = "2월";
                                            this.uCheckM03.Text = "3월";
                                            this.uCheckM04.Text = "4월";
                                            this.uCheckM05.Text = "5월";
                                            this.uCheckM06.Text = "6월";
                                            this.uCheckM07.Text = "7월";
                                            this.uCheckM08.Text = "8월";
                                            this.uCheckM09.Text = "9월";
                                            this.uCheckM10.Text = "10월";
                                            this.uCheckM11.Text = "11월";
                                            this.uCheckM12.Text = "12월";

                                            this.uCheckM05.Visible = true;
                                            this.uCheckM06.Visible = true;
                                            this.uCheckM07.Visible = true;
                                            this.uCheckM08.Visible = true;
                                            this.uCheckM09.Visible = true;
                                            this.uCheckM10.Visible = true;
                                            this.uCheckM11.Visible = true;
                                            this.uCheckM12.Visible = true;
                                        }
                                        else if (dsSRR_Info.Tables["Item"].Columns.Contains("Q01"))
                                        {
                                            this.uCheckM01.Text = "1분기";
                                            this.uCheckM02.Text = "2분기";
                                            this.uCheckM03.Text = "3분기";
                                            this.uCheckM04.Text = "4분기";

                                            this.uCheckM05.Visible = false;
                                            this.uCheckM06.Visible = false;
                                            this.uCheckM07.Visible = false;
                                            this.uCheckM08.Visible = false;
                                            this.uCheckM09.Visible = false;
                                            this.uCheckM10.Visible = false;
                                            this.uCheckM11.Visible = false;
                                            this.uCheckM12.Visible = false;
                                        }

                                        // 컬럼 폭 자동설정
                                        QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                        wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);

                                        // 랭크 설정을 위한 정보 조회
                                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRItem), "SRRItem");
                                        using (QRPQAT.BL.QATCLM.SRRItem clsSRRItem = new QRPQAT.BL.QATCLM.SRRItem())
                                        {
                                            brwChannel.mfCredentials(clsSRRItem);

                                            string strRankInfo = clsSRRItem.mfReadQATSRRItem_ForRank(strPlantCode, strCustomerCode, strProdType
                                                , strYear, strTargetCompany, m_resSys.GetString("SYS_LANG"));

                                            ErrRtn = ErrRtn.mfDecodingErrMessage(strRankInfo);
                                            if (ErrRtn.ErrNum.Equals(0))
                                            {
                                                using (System.IO.StringReader xmlRank = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                                                {
                                                    dtRankInfo.BeginLoadData();
                                                    dtRankInfo.ReadXml(xmlRank);
                                                    dtRankInfo.EndLoadData();
                                                }
                                            }
                                        }
                                    }

                                    this.uComboPlant.ReadOnly = true;
                                    this.uComboCustomerProdType.ReadOnly = true;
                                    this.uComboEvaluationPeriod.ReadOnly = true;
                                    this.uTextYear.ReadOnly = true;
                                    this.uComboTargetCompany.ReadOnly = true;

                                    this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboCustomerProdType.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;
                                    this.uTextYear.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboTargetCompany.Appearance.BackColor = Color.Gainsboro;

                                    this.uButtonLoad.Hide();

                                    this.uGroupBoxContentsArea.Expanded = true;
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                            DialogResult result = new DialogResult();
                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                                , ErrRtn.SystemInnerException + "<br/>" +
                                    ErrRtn.SystemMessage + "<br/>" +
                                    ErrRtn.SystemStackTrace
                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }


            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 조회리스트 더블클릭 이벤트
        void uGridSRRList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            ////// SystemInfo ResourceSet
            ////System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            ////try
            ////{
            ////    // Fix Cliked Row
            ////    e.Row.Fixed = true;

            ////    // 변수 설정
            ////    string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
            ////    string strCustomerCode = e.Row.Cells["CustomerCode"].Value.ToString();
            ////    string strProdType = e.Row.Cells["ProdType"].Value.ToString();
            ////    string strEvaluationPeriod = e.Row.Cells["EvaluationPeriod"].Value.ToString();
            ////    string strYear = e.Row.Cells["Year"].Value.ToString();
            ////    string strTargetCompany = e.Row.Cells["TargetCompany"].Value.ToString();

            ////    // Connect BL
            ////    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
            ////    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRHeader), "SRRHeader");
            ////    using (QRPQAT.BL.QATCLM.SRRHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRHeader())
            ////    {
            ////        brwChannel.mfCredentials(clsSRR_H);

            ////        string strSearch = clsSRR_H.mfReadQATSRRHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
            ////            , strEvaluationPeriod, strTargetCompany, strYear, m_resSys.GetString("SYS_LANG"));

            ////        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

            ////        if (ErrRtn.ErrNum.Equals(0))
            ////        {
            ////            using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
            ////            {
            ////                // Header
            ////                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
            ////                this.uComboCustomerProdType.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString() + "||" +
            ////                                                    dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
            ////                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
            ////                this.uTextYear.Text = dsSRR_Info.Tables["Header"].Rows[0]["Year"].ToString();
            ////                this.uTextTargetCompany.Text = dsSRR_Info.Tables["Header"].Rows[0]["TargetCompany"].ToString();                            

            ////                // Item
            ////                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

            ////                if (this.uGridSRRItemList.Rows.Count > 0)
            ////                {
            ////                    // 그리드 컬렴명 재설정
            ////                    ChangeGrid_Item();

            ////                    // 컬럼 폭 자동설정
            ////                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
            ////                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
            ////                }

            ////                this.uComboPlant.ReadOnly = true;
            ////                this.uComboCustomerProdType.ReadOnly = true;
            ////                this.uComboEvaluationPeriod.ReadOnly = true;
            ////                this.uTextYear.ReadOnly = true;
            ////                this.uTextTargetCompany.ReadOnly = true;

            ////                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
            ////                this.uComboCustomerProdType.Appearance.BackColor = Color.Gainsboro;
            ////                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;
            ////                this.uTextYear.Appearance.BackColor = Color.Gainsboro;
            ////                this.uTextTargetCompany.Appearance.BackColor = Color.Gainsboro;

            ////                this.uButtonLoad.Hide();

            ////                this.uGroupBoxContentsArea.Expanded = true;
            ////            }
            ////        }
            ////        else
            ////        {
            ////            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
            ////            DialogResult result = new DialogResult();
            ////            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
            ////                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
            ////                , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
            ////                , ErrRtn.SystemInnerException + "<br/>" +
            ////                    ErrRtn.SystemMessage + "<br/>" +
            ////                    ErrRtn.SystemStackTrace
            ////                , Infragistics.Win.HAlign.Right);
            ////        }
            ////    }
            ////}
            ////catch (System.Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////    m_resSys.Close();
            ////}
        }

        #endregion
    }
}
