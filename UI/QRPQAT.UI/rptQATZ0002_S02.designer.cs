﻿namespace QRPQAT.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_03.
    /// </summary>
    partial class rptQATZ0002_S02
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptQATZ0002_S02));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textEtc = new DataDynamics.ActiveReports.TextBox();
            this.text = new DataDynamics.ActiveReports.TextBox();
            this.textSpec = new DataDynamics.ActiveReports.TextBox();
            this.textParameter = new DataDynamics.ActiveReports.TextBox();
            this.label14 = new DataDynamics.ActiveReports.Label();
            this.label15 = new DataDynamics.ActiveReports.Label();
            this.label16 = new DataDynamics.ActiveReports.Label();
            this.label17 = new DataDynamics.ActiveReports.Label();
            this.label13 = new DataDynamics.ActiveReports.Label();
            this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textParameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textEtc,
            this.text,
            this.textSpec,
            this.textParameter});
            this.detail.Height = 0.39375F;
            this.detail.Name = "detail";
            // 
            // textEtc
            // 
            this.textEtc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEtc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEtc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEtc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textEtc.DataField = "EtcDesc";
            this.textEtc.Height = 0.4F;
            this.textEtc.Left = 5.531F;
            this.textEtc.Name = "textEtc";
            this.textEtc.Style = "font-size: 7.5pt; vertical-align: middle";
            this.textEtc.Text = null;
            this.textEtc.Top = 0F;
            this.textEtc.Width = 1.656F;
            // 
            // text
            // 
            this.text.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.text.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.text.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.text.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.text.DataField = "WorkCondition";
            this.text.Height = 0.4F;
            this.text.Left = 3.383F;
            this.text.Name = "text";
            this.text.Style = "font-size: 7.5pt; vertical-align: middle";
            this.text.Text = null;
            this.text.Top = 0F;
            this.text.Width = 2.148F;
            // 
            // textSpec
            // 
            this.textSpec.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpec.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpec.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpec.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSpec.DataField = "SpecStandard";
            this.textSpec.Height = 0.4F;
            this.textSpec.Left = 1.332F;
            this.textSpec.Name = "textSpec";
            this.textSpec.Style = "font-size: 7.5pt; vertical-align: middle";
            this.textSpec.Text = null;
            this.textSpec.Top = 0F;
            this.textSpec.Width = 2.051F;
            // 
            // textParameter
            // 
            this.textParameter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.textParameter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textParameter.DataField = "Parameter";
            this.textParameter.Height = 0.4F;
            this.textParameter.Left = 0F;
            this.textParameter.Name = "textParameter";
            this.textParameter.Style = "font-size: 7.5pt; vertical-align: middle";
            this.textParameter.Text = null;
            this.textParameter.Top = 0F;
            this.textParameter.Width = 1.332F;
            // 
            // label14
            // 
            this.label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Height = 0.2F;
            this.label14.HyperLink = null;
            this.label14.Left = 0F;
            this.label14.Name = "label14";
            this.label14.Style = "font-weight: bold; text-align: center";
            this.label14.Text = "PARAMETER";
            this.label14.Top = 0.281F;
            this.label14.Width = 1.332F;
            // 
            // label15
            // 
            this.label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Height = 0.2F;
            this.label15.HyperLink = null;
            this.label15.Left = 1.332F;
            this.label15.Name = "label15";
            this.label15.Style = "font-weight: bold; text-align: center";
            this.label15.Text = "SPEC 기준 / 장비사양";
            this.label15.Top = 0.281F;
            this.label15.Width = 2.051F;
            // 
            // label16
            // 
            this.label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label16.Height = 0.2F;
            this.label16.HyperLink = null;
            this.label16.Left = 3.383F;
            this.label16.Name = "label16";
            this.label16.Style = "font-weight: bold; text-align: center";
            this.label16.Text = "가  동  조  건\r\n";
            this.label16.Top = 0.281F;
            this.label16.Width = 2.148F;
            // 
            // label17
            // 
            this.label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Height = 0.2F;
            this.label17.HyperLink = null;
            this.label17.Left = 5.531F;
            this.label17.Name = "label17";
            this.label17.Style = "font-weight: bold; text-align: center";
            this.label17.Text = "비  고\r\n";
            this.label17.Top = 0.281F;
            this.label17.Width = 1.656001F;
            // 
            // label13
            // 
            this.label13.Height = 0.2F;
            this.label13.HyperLink = null;
            this.label13.Left = 0F;
            this.label13.Name = "label13";
            this.label13.Style = "";
            this.label13.Text = "4 . 가동 조건";
            this.label13.Top = 0.03F;
            this.label13.Width = 1F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label13});
            this.groupHeader1.Height = 0.481F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // rptEQUZ0002_S02
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.207834F;
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textParameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.Label label14;
        private DataDynamics.ActiveReports.Label label15;
        private DataDynamics.ActiveReports.Label label16;
        private DataDynamics.ActiveReports.Label label17;
        private DataDynamics.ActiveReports.Label label13;
        private DataDynamics.ActiveReports.TextBox textEtc;
        private DataDynamics.ActiveReports.TextBox text;
        private DataDynamics.ActiveReports.TextBox textSpec;
        private DataDynamics.ActiveReports.TextBox textParameter;
        private DataDynamics.ActiveReports.GroupHeader groupHeader1;
        private DataDynamics.ActiveReports.GroupFooter groupFooter1;
    }
}
