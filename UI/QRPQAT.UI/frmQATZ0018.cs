﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 고객관리                                              */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQATZ0018.cs                                        */
/* 프로그램명   : SRR 고객만족도                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-07-17                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                                                                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQATZ0018 : Form,IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0018()
        {
            InitializeComponent();
        }

        private void frmQATZ0018_Activated(object sender, EventArgs e)
        {
            // 툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0018_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmQATZ0018_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    //
                    //uGroupBoxContentsArea.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    //uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    //uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0018_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region Init
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Title 설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("SRR 고객만족도정보", m_resSys.GetString("SYS_FONTNAME"), 12);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxSEC, GroupBoxType.DETAIL, "SEC", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxMAC, GroupBoxType.DETAIL, "MAC", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxQual, GroupBoxType.INFO, "품질지수", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxYIeld, GroupBoxType.INFO, "제품수율", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "품질활동", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxSEC.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxSEC.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxMAC.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxMAC.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxQual.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxQual.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxYIeld.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxYIeld.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStdNumber, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelWriteDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelSPeriodQuarter, "기간(분기)", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSQualRun, "Qual Run", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSQualitySystem, "품질System", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSResponse, "고객대응", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSAccident, "사건사고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLowYield, "Low Yield", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSICN, "ICN", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSExecution, "집행율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSQualityAction, "품질활동", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSInspect, "자주검사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSAssyYield, "Assy Yield", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSDCTestYield, "DC Test Yield", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSScore, "점수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSLanking, "순위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelClaim, "Claim", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMPeriodMonth, "기간(월)", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMQualityLevel, "Quality level ", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMQualitySystem, "Quality System", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMCooperation, "Cooperation ", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMResponse, "Response ", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMPenaltyAward, "Penalty Award ", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMScore, "점수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMLanking, "순위", m_resSys.GetString("SYS_FONTNAME"), true, false);
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();
                // SearchArea Plant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);


                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                clsCustomer.Dispose();
                DataTable dtCustom = dtCustomer.Select("CustomerCode = 'SEC' OR CustomerCode = 'MAC'").CopyToDataTable();

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "CustomerCode", "CustomerName", dtCustom);

                wCombo.mfSetComboEditor(this.uComboCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustom);

                // DropDown 설정
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0072", m_resSys.GetString("SYS_LANG"));
                clsCom.Dispose();
                wCombo.mfSetComboEditor(this.uComboSPeriodQuarter, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCom);


                ArrayList arrKey = new ArrayList();
                ArrayList arrValue = new ArrayList();
                for (int i = 1; i < 13; i++)
                {
                    arrKey.Add(i);
                    arrValue.Add(i.ToString() + "月");
                }
                string strMonth = DateTime.Now.Date.Month.ToString();
                wCombo.mfSetComboEditor(this.uComboMPeriodMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, strMonth, arrKey, arrValue);
                    
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 List

                // 고객불만 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRR, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSRR, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "ManageNo", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "CumtomerCode", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "PeriodQuarter", "기간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "PeriodQuarterName", "기간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "Score", "점수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "WriteDate", "등록일 ", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "WriteUserID", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRR, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

               
                // Grid 편집불가 상태로
                this.uGridSRR.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion


                //폰트설정
                this.uGridSRR.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSRR.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Text Clear
        /// </summary>
        private void InitText()
        {
            try
            {
                this.uTextManageNo.Clear();
                this.uTextPlantCode.Clear();
                this.uComboCustomer.Value = "";
                this.uDateWrite.Value = DateTime.Now;
                this.uTextWriteUserID.Clear();
                this.uTextWriteUserName.Clear();


                this.uDateWrite.ReadOnly = false;
                this.uComboCustomer.ReadOnly = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region IToolbar 멤버

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                if (this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = false;

                #region 필수입력사항
                if (this.uComboSearchPlant.Value == null || this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000274", "M000266"
                                , Infragistics.Win.HAlign.Right);

                    //this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uDateSearchFromDate.Value == null || this.uDateSearchFromDate.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000384"
                                , Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }
                if (this.uDateSearchToDate.Value == null || this.uDateSearchToDate.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000384"
                                , Infragistics.Win.HAlign.Right);

                    this.uDateSearchToDate.DropDown();
                    return;
                }
                if (this.uDateSearchFromDate.DateTime.Date > this.uDateSearchToDate.DateTime.Date)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000210"
                                , Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromDate.DropDown();
                    return;
                }
                #endregion

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomer = this.uComboSearchCustomer.Value == null ? "" : this.uComboSearchCustomer.Value.ToString();
                string strFromDate = this.uDateSearchFromDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToDate = this.uDateSearchToDate.DateTime.Date.ToString("yyyy-MM-dd");

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRR), "SRR");
                QRPQAT.BL.QATCLM.SRR clsSRR = new QRPQAT.BL.QATCLM.SRR();
                brwChannel.mfCredentials(clsSRR);

                DataTable dtSRR = clsSRR.mfReadSRR(strPlantCode, strCustomer, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                this.uGridSRR.DataSource = dtSRR;
                this.uGridSRR.DataBind();

                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtSRR.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSRR, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;
                string strLang = m_resSys.GetString("SYS_LANG");

     
                if (!mfChk(m_resSys.GetString("SYS_LANG")))
                    return;

                if (msg.mfSetMessageBox(MessageBoxType.YesNo,500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    DataTable dtRtn = RtnSaveInfo();

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRR), "SRR");
                    QRPQAT.BL.QATCLM.SRR clsSRR = new QRPQAT.BL.QATCLM.SRR();
                    brwChannel.mfCredentials(clsSRR);

                    string strErrRtn = clsSRR.mfSaveSRR(dtRtn, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMsg = "";
                        
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = msg.GetMessge_Text("M000953",strLang);
                        else
                            strMsg = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                      , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , msg.GetMessge_Text("M001135", strLang)
                                                      , msg.GetMessge_Text("M001037", strLang), strMsg
                                                      , Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;
                string strLang = m_resSys.GetString("SYS_LANG");
                


                if (!this.uGroupBoxContentsArea.Expanded 
                    || this.uTextPlantCode.Text.Equals(string.Empty)
                    || this.uTextManageNo.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000394"
                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                string strPlantCode = this.uTextPlantCode.Text;
                string strManageNo = this.uTextManageNo.Text;


                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRR), "SRR");
                    QRPQAT.BL.QATCLM.SRR clsSRR = new QRPQAT.BL.QATCLM.SRR();
                    brwChannel.mfCredentials(clsSRR);

                    string strErrRtn = clsSRR.mfDeleteSRR(strPlantCode, strManageNo);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000676",strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001135",strLang)
                                                    , msg.GetMessge_Text("M000638",strLang), strMes
                                                    , Infragistics.Win.HAlign.Right);
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                InitText();

                this.uTextPlantCode.Text = m_resSys.GetString("SYS_PLANTCODE");

                if (!this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = true;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ReosurceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGridSRR.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000811", "M000812", Infragistics.Win.HAlign.Right);

                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridSRR);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion

        #region Event

        /// <summary>
        /// Contents GroupBox 활성 비활성에 따른 위치변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSRR.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridSRR.Height = 760;
                    for (int i = 0; i < uGridSRR.Rows.Count; i++)
                    {
                        uGridSRR.Rows[i].Fixed = false;
                    }

                    // 컨트롤 초기화
                    //Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 고객사 변경에 따른 MAC, SEC 그룹박스 활성 비활성
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboCustomer_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboCustomer.Value == null)
                    return;

                string strCode = this.uComboCustomer.Value.ToString();

                string strText = this.uComboCustomer.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboCustomer.Items.Count; i++)
                {
                    if (strCode.Equals(this.uComboCustomer.Items[i].DataValue.ToString()) && strText.Equals(this.uComboCustomer.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (strChk.Equals(string.Empty))
                    return;

                if (strCode.Equals("MAC"))
                {
                    mfVisibleYN(this.uGroupBoxMAC, true);
                    mfVisibleYN(this.uGroupBoxSEC, false);
                    this.uComboMPeriodMonth.Value = DateTime.Now.Date.Month.ToString();

                }
                else if (strCode.Equals("SEC"))
                {
                    mfVisibleYN(this.uGroupBoxMAC, false);
                    mfVisibleYN(this.uGroupBoxSEC, true);

                }
                else
                {
                    mfVisibleYN(this.uGroupBoxMAC, false);
                    mfVisibleYN(this.uGroupBoxSEC, false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 헤더 더블 클릭 시 상세정보 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridSRR_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strManageNo = e.Row.Cells["ManageNo"].Value.ToString();

                if (!this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = true;

                e.Row.Fixed = true;

                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                SearchHeader_Detail(strPlantCode, strManageNo, m_resSys.GetString("SYS_LANG"));

                this.uComboCustomer.ReadOnly = true;
                this.uDateWrite.ReadOnly = true;

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 입력 ID 변경시 명 Clear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextWriteUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextWriteUserName.Text.Equals(string.Empty))
                this.uTextWriteUserName.Clear();
        }

        /// <summary>
        /// 사용자 KeyDown 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!e.KeyData.Equals(Keys.Enter))
                    return;

                //생성자 공장코드 저장
                string strUserID = this.uTextWriteUserID.Text;
                string strPlantCode = this.uTextPlantCode.Text;

                if (strUserID.Equals(string.Empty) || strPlantCode.Equals(string.Empty))
                    return;

                WinMessageBox msg = new WinMessageBox();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                DataTable dtRtn = RtnUserIfo(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                //정비사를 선택한 경우 설비그룹선택은 초기화한다.
                if (dtRtn.Rows.Count > 0)
                    this.uTextWriteUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);
                    this.uTextWriteUserName.Clear();
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uTextPlantCode.Text;
                if (strPlant.Equals(String.Empty))
                {
                    //msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                    //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                           "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    
                    return;
                }
                frmPOP0011 frmUser = new frmPOP0011();
                //공장정보 frmUser에 보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.UserID == null || frmUser.UserID.Equals(string.Empty))
                    return;

                //텍스트에 삽입
                this.uTextWriteUserID.Text = frmUser.UserID;
                this.uTextWriteUserName.Text = frmUser.UserName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자동 Sum
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uNumMBoxSum_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //거래처정보가 없는경우 Pass
                if(this.uComboCustomer.Value == null || this.uComboCustomer.Value.ToString().Equals(string.Empty))
                    return;

                //Event발생 Control저장
                Infragistics.Win.UltraWinEditors.UltraNumericEditor uNum = (Infragistics.Win.UltraWinEditors.UltraNumericEditor)sender;

                //정보가 없는경우 Pass
                if (uNum.Value == null)
                    return;

                if (Convert.ToDouble(uNum.Value) > Convert.ToDouble(uNum.MaxValue))
                {

                    uNum.ValueChanged -= new System.EventHandler(this.uNumMBoxSum_ValueChanged);
                    uNum.Value = 0;
                    uNum.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
                }

                //전체 Sum을 저장할 변수 선언
                double dblSum = 0;
                //거래처 코드에따라 해당하는 Score정보는 저장함.
                string strCustomer = this.uComboCustomer.Value.ToString();
                if (strCustomer.Equals("SEC"))
                {

                    dblSum = Convert.ToDouble(uNumSQualRun.Value) + Convert.ToDouble(uNumSQualitySystem.Value) + Convert.ToDouble(uNumSResponse.Value)
                           + Convert.ToDouble(uNumSAccident.Value) + Convert.ToDouble(uNumSLowYield.Value) + Convert.ToDouble(uNumSICN.Value)
                           + Convert.ToDouble(uNumSExecution.Value) + Convert.ToDouble(uNumSQualityAction.Value) + Convert.ToDouble(uNumSInspect.Value)
                           + Convert.ToDouble(uNumSAssyYield.Value) + Convert.ToDouble(uNumSDCTestYield.Value) + Convert.ToDouble(uNumClaim.Value);

                    //Score의 Max정보보다 큰경우 메세지 출력 후 해당컨트롤 0으로 초기화
                    if (dblSum > Convert.ToDouble(this.uNumSScore.MaxValue))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        
                       DialogResult result= msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000882", "M001508", Infragistics.Win.HAlign.Right);
                        uNum.Value = 0;
                        uNumMBoxSum_ValueChanged(uNum, e);
                        this.uNumSScore.Focus();
                        return;
                    }
                        
                    this.uNumSScore.Value = dblSum;
                }
                else if (strCustomer.Equals("MAC"))
                {
                    dblSum = Convert.ToDouble(uNumMQualityLevel.Value) + Convert.ToDouble(uNumMQualitySystem.Value) + Convert.ToDouble(uNumMCooperation.Value)
                           + Convert.ToDouble(uNumMResponse.Value) + Convert.ToDouble(uNumMPenaltyAward.Value);

                    //Score의 Max정보보다 큰경우 메세지 출력 후 해당컨트롤 0으로 초기화
                    if (dblSum > Convert.ToDouble(this.uNumMScore.MaxValue))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult result=msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000882", "M001508", Infragistics.Win.HAlign.Right);
                        uNum.Value = 0;
                        uNumMBoxSum_ValueChanged(uNum, e);
                        this.uNumMScore.Focus();
                        return;
                    }

                    this.uNumMScore.Value = dblSum;
                }
                else
                    return;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Method

        /// <summary>
        /// MAC,SEC 그룹박스 활성, 비활성
        /// </summary>
        /// <param name="Ctrl">그룹박스</param>
        /// <param name="bolVisible">활성,비활성</param>
        private void mfVisibleYN(Control Ctrl,bool bolVisible)
        {
            try
            {

                foreach (Control C in Ctrl.Controls)
                {
                    C.Visible = bolVisible;

                    if (bolVisible)
                        continue;
                    
                    if (C.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraTextEditor")
                        ((Infragistics.Win.UltraWinEditors.UltraTextEditor)C).Clear();
                    else if (C.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraComboEditor")
                        ((Infragistics.Win.UltraWinEditors.UltraComboEditor)C).Value = "";
                    else if (C.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor")
                        ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)C).Value = DateTime.Now;
                    else if (C.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraNumericEditor")
                        ((Infragistics.Win.UltraWinEditors.UltraNumericEditor)C).Value = 0;
                }

                Ctrl.Visible = bolVisible;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 헤더 상세정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">사용언어</param>
        private void SearchHeader_Detail(string strPlantCode,string strManageNo,string strLang)
        {
            try
            {
                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRR), "SRR");
                QRPQAT.BL.QATCLM.SRR clsSRR = new QRPQAT.BL.QATCLM.SRR();
                brwChannel.mfCredentials(clsSRR);

                DataTable dtHeader = clsSRR.mfReadSRR_Detail(strPlantCode, strManageNo, strLang);

                clsSRR.Dispose();

                if (dtHeader.Rows.Count == 0)
                    return;

                this.uTextPlantCode.Text = dtHeader.Rows[0]["PlantCode"].ToString();
                this.uTextManageNo.Text = dtHeader.Rows[0]["ManageNo"].ToString();
                this.uComboCustomer.Value = dtHeader.Rows[0]["CumtomerCode"];
                this.uDateWrite.Value = dtHeader.Rows[0]["WriteDate"];
                this.uTextWriteUserID.Text = dtHeader.Rows[0]["WriteUserID"].ToString();
                this.uTextWriteUserName.Text = dtHeader.Rows[0]["WriteUserName"].ToString();
                this.uComboSPeriodQuarter.Value = dtHeader.Rows[0]["SPeriodQuarter"];
                this.uNumSAccident.Value = dtHeader.Rows[0]["SAccident"];
                this.uNumSLowYield.Value = dtHeader.Rows[0]["SLowYield"];
                this.uNumSICN.Value = dtHeader.Rows[0]["SICN"];
                this.uNumSAssyYield.Value = dtHeader.Rows[0]["SAssyYield"];
                this.uNumSDCTestYield.Value = dtHeader.Rows[0]["SAssyYield"];
                this.uNumSQualRun.Value = dtHeader.Rows[0]["SQualRun"];
                this.uNumSQualitySystem.Value = dtHeader.Rows[0]["SQualRun"];
                this.uNumSResponse.Value = dtHeader.Rows[0]["SResponse"];
                this.uNumSExecution.Value = dtHeader.Rows[0]["SExecution"];
                this.uNumSQualityAction.Value = dtHeader.Rows[0]["SQualityAction"];
                this.uNumSInspect.Value = dtHeader.Rows[0]["SInspect"];
                this.uNumSScore.Value = dtHeader.Rows[0]["SScore"];
                this.uNumSLanking.Value = dtHeader.Rows[0]["SLanking"];
                this.uComboMPeriodMonth.Value = dtHeader.Rows[0]["MPeriodMonth"];
                this.uNumMQualityLevel.Value = dtHeader.Rows[0]["MQualityLevel"];
                this.uNumMQualitySystem.Value = dtHeader.Rows[0]["MQualitySystem"];
                this.uNumMCooperation.Value = dtHeader.Rows[0]["MCooperation"];
                this.uNumMResponse.Value = dtHeader.Rows[0]["MResponse"];
                this.uNumMPenaltyAward.Value = dtHeader.Rows[0]["MPenaltyAward"];
                this.uNumMScore.Value = dtHeader.Rows[0]["MScore"];
                this.uNumMLanking.Value = dtHeader.Rows[0]["MLanking"];

                this.uNumClaim.Value = dtHeader.Rows[0]["SClaim"];      // 2012-11-16 추가



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        /// <summary>
        /// SRR 저장정보
        /// </summary>
        /// <returns></returns>
        private DataTable RtnSaveInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRR), "SRR");
                QRPQAT.BL.QATCLM.SRR clsSRR = new QRPQAT.BL.QATCLM.SRR();
                brwChannel.mfCredentials(clsSRR);

                dtRtn = clsSRR.mfSetDataInfo();
                clsSRR.Dispose();
                
                DataRow drRow = dtRtn.NewRow();

                drRow["PlantCode"] = this.uTextPlantCode.Text;
                drRow["ManageNo"] = this.uTextManageNo.Text;
                drRow["CumtomerCode"] = this.uComboCustomer.Value.ToString();
                drRow["WriteDate"] = this.uDateWrite.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["SPeriodQuarter"] = this.uComboSPeriodQuarter.Value;
                drRow["SAccident"] = this.uNumSAccident.Value;
                drRow["SLowYield"] = this.uNumSLowYield.Value;
                drRow["SICN"] = this.uNumSICN.Value;
                drRow["SAssyYield"] = this.uNumSAssyYield.Value;
                drRow["SDCTestYield"] = this.uNumSDCTestYield.Value;
                drRow["SQualRun"] = this.uNumSQualRun.Value;
                drRow["SQualitySystem"] = this.uNumSQualitySystem.Value;
                drRow["SResponse"] = this.uNumSResponse.Value;
                drRow["SExecution"] = this.uNumSExecution.Value;
                drRow["SQualityAction"] = this.uNumSQualityAction.Value;
                drRow["SInspect"] = this.uNumSInspect.Value;
                drRow["SScore"] = this.uNumSScore.Value;
                drRow["SLanking"] = this.uNumSLanking.Value;
                drRow["MPeriodMonth"] = this.uComboMPeriodMonth.Value;
                drRow["MQualityLevel"] = this.uNumMQualityLevel.Value;
                drRow["MQualitySystem"] = this.uNumMQualitySystem.Value;
                drRow["MCooperation"] = this.uNumMCooperation.Value;
                drRow["MResponse"] = this.uNumMResponse.Value;
                drRow["MPenaltyAward"] = this.uNumMPenaltyAward.Value;
                drRow["MScore"] = this.uNumMScore.Value;
                drRow["MLanking"] = this.uNumMLanking.Value;

                drRow["SClaim"] = this.uNumClaim.Value;         // 2012-11-16 추가

                dtRtn.Rows.Add(drRow);


                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 유저정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private DataTable RtnUserIfo(string strPlantCode,string strUserID,string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtRtn = clsUser.mfReadSYSUser(strPlantCode, strUserID, strLang);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose(); 
            }
        }


        /// <summary>
        /// 저장시 필수 입력사항
        /// </summary>
        /// <returns></returns>
        private bool mfChk(string strLang)
        {
            try
            {
                System.Windows.Forms.DialogResult result;
                WinMessageBox msg = new WinMessageBox();
                if (!this.uGroupBoxContentsArea.Expanded || this.uTextPlantCode.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000962", "M001047"
                                , Infragistics.Win.HAlign.Right);
                    return false;
                }
                if (this.uComboCustomer.Value == null || this.uComboCustomer.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M001473"
                                , Infragistics.Win.HAlign.Right);

                    this.uComboCustomer.DropDown();
                    return false;
                }


                // 고객사 콤보
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(strLang);
                clsCustomer.Dispose();
                string strCustomer = this.uComboCustomer.Value.ToString();
                if (dtCustomer.Select("CustomerCode = '" + strCustomer + "'").Count() == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M001473"
                                , Infragistics.Win.HAlign.Right);

                    this.uComboCustomer.DropDown();
                    return false;
                }
                if (strCustomer.Equals("SEC"))
                    if (this.uComboSPeriodQuarter.Value == null || this.uComboSPeriodQuarter.Value.ToString().Equals(string.Empty))
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000342"
                                , Infragistics.Win.HAlign.Right);
                        this.uComboSPeriodQuarter.DropDown();
                        return false;
                    }
                if (strCustomer.Equals("MAC"))
                    if (this.uComboMPeriodMonth.Value == null || this.uComboMPeriodMonth.Value.ToString().Equals(string.Empty))
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000342"
                                , Infragistics.Win.HAlign.Right);
                        this.uComboMPeriodMonth.DropDown();
                        return false;
                    }

                if (this.uDateWrite.Value == null || this.uDateWrite.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000384"
                                , Infragistics.Win.HAlign.Right);
                    this.uDateWrite.DropDown();
                    return false;
                }
                if (this.uTextWriteUserID.Text.Equals(string.Empty) || this.uTextWriteUserName.Text.Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                               , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                               , "M001264", "M001230", "M000386"
                               , Infragistics.Win.HAlign.Right);
                    this.uTextWriteUserID.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        #endregion







    }
}
