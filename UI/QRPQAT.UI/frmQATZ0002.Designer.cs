﻿namespace QRPQAT.UI
{
    partial class frmQATZ0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0002));
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBoxTool = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelMold = new Infragistics.Win.Misc.UltraLabel();
            this.uGridMoldTool = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSetup = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSetup = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSetup = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uOptionCertiResultCode = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uCheckMESTFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateCertiAdmitDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCertiDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextCertiAdmitName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiChargeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCertiAdmitID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiChargeID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDeny = new Infragistics.Win.Misc.UltraButton();
            this.uTextAdmitStatusCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDelRow = new Infragistics.Win.Misc.UltraButton();
            this.uGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateCertiReqDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateCertiReqDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCertiReaDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipSearchName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipCertiReqTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSerchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridSubEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextMDMTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlantName = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckTechStandardFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckWorkStandardFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckEquipStandardFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCertiReqTypeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiReqDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCertiReqDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerial = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxTool)).BeginInit();
            this.uGroupBoxTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMoldTool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSetup)).BeginInit();
            this.uGroupBoxSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSetup)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCertiResultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiAdmitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiChargeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiAdmitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiChargeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitStatusCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiReqDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiReqDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipSearchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCertiReqTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSerchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSubEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMDMTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTechStandardFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStandardFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEquipStandardFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCertiReqTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGroupBoxTool);
            this.ultraTabPageControl1.Controls.Add(this.uGroupBoxSetup);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(886, 416);
            // 
            // uGroupBoxTool
            // 
            this.uGroupBoxTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxTool.Controls.Add(this.uLabelMold);
            this.uGroupBoxTool.Controls.Add(this.uGridMoldTool);
            this.uGroupBoxTool.Location = new System.Drawing.Point(444, 11);
            this.uGroupBoxTool.Name = "uGroupBoxTool";
            this.uGroupBoxTool.Size = new System.Drawing.Size(430, 440);
            this.uGroupBoxTool.TabIndex = 3;
            // 
            // uLabelMold
            // 
            this.uLabelMold.Location = new System.Drawing.Point(10, 12);
            this.uLabelMold.Name = "uLabelMold";
            this.uLabelMold.Size = new System.Drawing.Size(117, 20);
            this.uLabelMold.TabIndex = 99;
            // 
            // uGridMoldTool
            // 
            this.uGridMoldTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMoldTool.DisplayLayout.Appearance = appearance75;
            this.uGridMoldTool.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMoldTool.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMoldTool.DisplayLayout.GroupByBox.Appearance = appearance76;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMoldTool.DisplayLayout.GroupByBox.BandLabelAppearance = appearance77;
            this.uGridMoldTool.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance78.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance78.BackColor2 = System.Drawing.SystemColors.Control;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMoldTool.DisplayLayout.GroupByBox.PromptAppearance = appearance78;
            this.uGridMoldTool.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMoldTool.DisplayLayout.MaxRowScrollRegions = 1;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMoldTool.DisplayLayout.Override.ActiveCellAppearance = appearance79;
            appearance80.BackColor = System.Drawing.SystemColors.Highlight;
            appearance80.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMoldTool.DisplayLayout.Override.ActiveRowAppearance = appearance80;
            this.uGridMoldTool.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMoldTool.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMoldTool.DisplayLayout.Override.CardAreaAppearance = appearance81;
            appearance82.BorderColor = System.Drawing.Color.Silver;
            appearance82.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMoldTool.DisplayLayout.Override.CellAppearance = appearance82;
            this.uGridMoldTool.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMoldTool.DisplayLayout.Override.CellPadding = 0;
            appearance83.BackColor = System.Drawing.SystemColors.Control;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMoldTool.DisplayLayout.Override.GroupByRowAppearance = appearance83;
            appearance84.TextHAlignAsString = "Left";
            this.uGridMoldTool.DisplayLayout.Override.HeaderAppearance = appearance84;
            this.uGridMoldTool.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMoldTool.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            this.uGridMoldTool.DisplayLayout.Override.RowAppearance = appearance85;
            this.uGridMoldTool.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMoldTool.DisplayLayout.Override.TemplateAddRowAppearance = appearance86;
            this.uGridMoldTool.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMoldTool.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMoldTool.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMoldTool.Location = new System.Drawing.Point(10, 40);
            this.uGridMoldTool.Name = "uGridMoldTool";
            this.uGridMoldTool.Size = new System.Drawing.Size(413, 357);
            this.uGridMoldTool.TabIndex = 0;
            // 
            // uGroupBoxSetup
            // 
            this.uGroupBoxSetup.Controls.Add(this.uLabelSetup);
            this.uGroupBoxSetup.Controls.Add(this.uGridSetup);
            this.uGroupBoxSetup.Location = new System.Drawing.Point(9, 11);
            this.uGroupBoxSetup.Name = "uGroupBoxSetup";
            this.uGroupBoxSetup.Size = new System.Drawing.Size(432, 440);
            this.uGroupBoxSetup.TabIndex = 2;
            // 
            // uLabelSetup
            // 
            this.uLabelSetup.Location = new System.Drawing.Point(10, 12);
            this.uLabelSetup.Name = "uLabelSetup";
            this.uLabelSetup.Size = new System.Drawing.Size(117, 20);
            this.uLabelSetup.TabIndex = 99;
            // 
            // uGridSetup
            // 
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSetup.DisplayLayout.Appearance = appearance87;
            this.uGridSetup.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSetup.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance88.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSetup.DisplayLayout.GroupByBox.Appearance = appearance88;
            appearance89.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSetup.DisplayLayout.GroupByBox.BandLabelAppearance = appearance89;
            this.uGridSetup.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSetup.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            this.uGridSetup.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSetup.DisplayLayout.MaxRowScrollRegions = 1;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSetup.DisplayLayout.Override.ActiveCellAppearance = appearance91;
            appearance92.BackColor = System.Drawing.SystemColors.Highlight;
            appearance92.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSetup.DisplayLayout.Override.ActiveRowAppearance = appearance92;
            this.uGridSetup.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSetup.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSetup.DisplayLayout.Override.CardAreaAppearance = appearance93;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            appearance94.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSetup.DisplayLayout.Override.CellAppearance = appearance94;
            this.uGridSetup.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSetup.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSetup.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.uGridSetup.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGridSetup.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSetup.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGridSetup.DisplayLayout.Override.RowAppearance = appearance97;
            this.uGridSetup.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSetup.DisplayLayout.Override.TemplateAddRowAppearance = appearance98;
            this.uGridSetup.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSetup.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSetup.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSetup.Location = new System.Drawing.Point(10, 40);
            this.uGridSetup.Name = "uGridSetup";
            this.uGridSetup.Size = new System.Drawing.Size(415, 357);
            this.uGridSetup.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGroupBox4);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-8571, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(886, 416);
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox4.Controls.Add(this.uButtonDown);
            this.uGroupBox4.Controls.Add(this.uLabel21);
            this.uGroupBox4.Controls.Add(this.uLabel20);
            this.uGroupBox4.Controls.Add(this.uLabel19);
            this.uGroupBox4.Controls.Add(this.uGrid4);
            this.uGroupBox4.Controls.Add(this.uGrid2);
            this.uGroupBox4.Controls.Add(this.uGrid3);
            this.uGroupBox4.Location = new System.Drawing.Point(-3, -3);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(889, 429);
            this.uGroupBox4.TabIndex = 109;
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(569, 8);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(89, 24);
            this.uButtonDown.TabIndex = 104;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(453, 12);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(110, 20);
            this.uLabel21.TabIndex = 98;
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(10, 219);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(202, 20);
            this.uLabel20.TabIndex = 98;
            // 
            // uLabel19
            // 
            this.uLabel19.Location = new System.Drawing.Point(10, 12);
            this.uLabel19.Name = "uLabel19";
            this.uLabel19.Size = new System.Drawing.Size(110, 20);
            this.uLabel19.TabIndex = 98;
            // 
            // uGrid4
            // 
            this.uGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance16;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance25.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(449, 36);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(430, 175);
            this.uGrid4.TabIndex = 0;
            this.uGrid4.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid4_DoubleClickCell);
            // 
            // uGrid2
            // 
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance40;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(10, 36);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(432, 175);
            this.uGrid2.TabIndex = 0;
            // 
            // uGrid3
            // 
            this.uGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance28;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance29;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance30;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance32;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance34;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            appearance35.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance35;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance36.BackColor = System.Drawing.SystemColors.Control;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance36;
            appearance37.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance38;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance39;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(7, 244);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(869, 165);
            this.uGrid3.TabIndex = 0;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uOptionCertiResultCode);
            this.ultraTabPageControl3.Controls.Add(this.uCheckMESTFlag);
            this.ultraTabPageControl3.Controls.Add(this.uCheckCompleteFlag);
            this.ultraTabPageControl3.Controls.Add(this.uDateCertiAdmitDate);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel5);
            this.ultraTabPageControl3.Controls.Add(this.uDateCertiDate);
            this.ultraTabPageControl3.Controls.Add(this.uTextCertiAdmitName);
            this.ultraTabPageControl3.Controls.Add(this.uTextCertiChargeName);
            this.ultraTabPageControl3.Controls.Add(this.uLabel18);
            this.ultraTabPageControl3.Controls.Add(this.uTextCertiAdmitID);
            this.ultraTabPageControl3.Controls.Add(this.uTextCertiChargeID);
            this.ultraTabPageControl3.Controls.Add(this.uLabel17);
            this.ultraTabPageControl3.Controls.Add(this.uLabel16);
            this.ultraTabPageControl3.Controls.Add(this.uLabel15);
            this.ultraTabPageControl3.Controls.Add(this.uButtonDeny);
            this.ultraTabPageControl3.Controls.Add(this.uTextAdmitStatusCode);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel7);
            this.ultraTabPageControl3.Controls.Add(this.uTextRejectReason);
            this.ultraTabPageControl3.Controls.Add(this.ultraLabel6);
            this.ultraTabPageControl3.Controls.Add(this.uLabel8);
            this.ultraTabPageControl3.Controls.Add(this.uButtonDelRow);
            this.ultraTabPageControl3.Controls.Add(this.uGrid5);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(886, 416);
            // 
            // uOptionCertiResultCode
            // 
            appearance126.TextVAlignAsString = "Middle";
            this.uOptionCertiResultCode.Appearance = appearance126;
            this.uOptionCertiResultCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionCertiResultCode.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem2.DataValue = "OK";
            valueListItem2.DisplayText = "합격";
            valueListItem3.DataValue = "NG";
            valueListItem3.DisplayText = "불합격";
            this.uOptionCertiResultCode.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem3});
            this.uOptionCertiResultCode.Location = new System.Drawing.Point(638, 385);
            this.uOptionCertiResultCode.Name = "uOptionCertiResultCode";
            this.uOptionCertiResultCode.Size = new System.Drawing.Size(120, 20);
            this.uOptionCertiResultCode.TabIndex = 135;
            // 
            // uCheckMESTFlag
            // 
            this.uCheckMESTFlag.Enabled = false;
            this.uCheckMESTFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMESTFlag.Location = new System.Drawing.Point(768, 361);
            this.uCheckMESTFlag.Name = "uCheckMESTFlag";
            this.uCheckMESTFlag.Size = new System.Drawing.Size(106, 20);
            this.uCheckMESTFlag.TabIndex = 134;
            this.uCheckMESTFlag.Text = "MES전송여부";
            this.uCheckMESTFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckMESTFlag.Visible = false;
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(785, 384);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(89, 20);
            this.uCheckCompleteFlag.TabIndex = 133;
            this.uCheckCompleteFlag.Text = "작성 완료";
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateCertiAdmitDate
            // 
            appearance65.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCertiAdmitDate.Appearance = appearance65;
            this.uDateCertiAdmitDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCertiAdmitDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCertiAdmitDate.Location = new System.Drawing.Point(422, 385);
            this.uDateCertiAdmitDate.Name = "uDateCertiAdmitDate";
            this.uDateCertiAdmitDate.Size = new System.Drawing.Size(103, 21);
            this.uDateCertiAdmitDate.TabIndex = 131;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(315, 385);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(103, 20);
            this.ultraLabel5.TabIndex = 132;
            // 
            // uDateCertiDate
            // 
            appearance73.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCertiDate.Appearance = appearance73;
            this.uDateCertiDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCertiDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCertiDate.Location = new System.Drawing.Point(422, 361);
            this.uDateCertiDate.Name = "uDateCertiDate";
            this.uDateCertiDate.Size = new System.Drawing.Size(103, 21);
            this.uDateCertiDate.TabIndex = 124;
            // 
            // uTextCertiAdmitName
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiAdmitName.Appearance = appearance52;
            this.uTextCertiAdmitName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiAdmitName.Location = new System.Drawing.Point(206, 385);
            this.uTextCertiAdmitName.Name = "uTextCertiAdmitName";
            this.uTextCertiAdmitName.ReadOnly = true;
            this.uTextCertiAdmitName.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiAdmitName.TabIndex = 121;
            // 
            // uTextCertiChargeName
            // 
            appearance68.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiChargeName.Appearance = appearance68;
            this.uTextCertiChargeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiChargeName.Location = new System.Drawing.Point(206, 361);
            this.uTextCertiChargeName.Name = "uTextCertiChargeName";
            this.uTextCertiChargeName.ReadOnly = true;
            this.uTextCertiChargeName.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiChargeName.TabIndex = 120;
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(10, 385);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(103, 20);
            this.uLabel18.TabIndex = 128;
            // 
            // uTextCertiAdmitID
            // 
            appearance64.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCertiAdmitID.Appearance = appearance64;
            this.uTextCertiAdmitID.BackColor = System.Drawing.Color.PowderBlue;
            appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
            appearance3.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCertiAdmitID.ButtonsRight.Add(editorButton1);
            this.uTextCertiAdmitID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCertiAdmitID.Location = new System.Drawing.Point(117, 385);
            this.uTextCertiAdmitID.Name = "uTextCertiAdmitID";
            this.uTextCertiAdmitID.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiAdmitID.TabIndex = 123;
            this.uTextCertiAdmitID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCertiAdmitID_KeyDown);
            this.uTextCertiAdmitID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCertiAdmitID_EditorButtonClick);
            // 
            // uTextCertiChargeID
            // 
            appearance69.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCertiChargeID.Appearance = appearance69;
            this.uTextCertiChargeID.BackColor = System.Drawing.Color.PowderBlue;
            appearance70.Image = ((object)(resources.GetObject("appearance70.Image")));
            appearance70.TextHAlignAsString = "Center";
            editorButton2.Appearance = appearance70;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCertiChargeID.ButtonsRight.Add(editorButton2);
            this.uTextCertiChargeID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCertiChargeID.Location = new System.Drawing.Point(117, 361);
            this.uTextCertiChargeID.Name = "uTextCertiChargeID";
            this.uTextCertiChargeID.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiChargeID.TabIndex = 122;
            this.uTextCertiChargeID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCertiChargeID_KeyDown);
            this.uTextCertiChargeID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCertiChargeID_EditorButtonClick);
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(315, 361);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(103, 20);
            this.uLabel17.TabIndex = 126;
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(10, 361);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(103, 20);
            this.uLabel16.TabIndex = 125;
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(531, 385);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(103, 20);
            this.uLabel15.TabIndex = 127;
            // 
            // uButtonDeny
            // 
            this.uButtonDeny.Location = new System.Drawing.Point(823, 330);
            this.uButtonDeny.Name = "uButtonDeny";
            this.uButtonDeny.Size = new System.Drawing.Size(55, 28);
            this.uButtonDeny.TabIndex = 119;
            this.uButtonDeny.Text = "반려";
            this.uButtonDeny.Click += new System.EventHandler(this.uButtonDeny_Click);
            // 
            // uTextAdmitStatusCode
            // 
            appearance63.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitStatusCode.Appearance = appearance63;
            this.uTextAdmitStatusCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitStatusCode.Location = new System.Drawing.Point(116, 334);
            this.uTextAdmitStatusCode.Name = "uTextAdmitStatusCode";
            this.uTextAdmitStatusCode.ReadOnly = true;
            this.uTextAdmitStatusCode.Size = new System.Drawing.Size(120, 21);
            this.uTextAdmitStatusCode.TabIndex = 117;
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(9, 334);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(103, 20);
            this.ultraLabel7.TabIndex = 118;
            // 
            // uTextRejectReason
            // 
            this.uTextRejectReason.Location = new System.Drawing.Point(356, 334);
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.Size = new System.Drawing.Size(463, 21);
            this.uTextRejectReason.TabIndex = 116;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(249, 334);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(103, 20);
            this.ultraLabel6.TabIndex = 115;
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(10, 12);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(103, 20);
            this.uLabel8.TabIndex = 114;
            this.uLabel8.Text = "3";
            // 
            // uButtonDelRow
            // 
            this.uButtonDelRow.Location = new System.Drawing.Point(120, 12);
            this.uButtonDelRow.Name = "uButtonDelRow";
            this.uButtonDelRow.Size = new System.Drawing.Size(89, 24);
            this.uButtonDelRow.TabIndex = 1;
            this.uButtonDelRow.Text = "ultraButton1";
            this.uButtonDelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);
            // 
            // uGrid5
            // 
            this.uGrid5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid5.DisplayLayout.Appearance = appearance102;
            this.uGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance99.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance99.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance99.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance99.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.GroupByBox.Appearance = appearance99;
            appearance100.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance100;
            this.uGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance101.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance101.BackColor2 = System.Drawing.SystemColors.Control;
            appearance101.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance101.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance101;
            this.uGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            appearance110.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance110;
            appearance105.BackColor = System.Drawing.SystemColors.Highlight;
            appearance105.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance105;
            this.uGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.CardAreaAppearance = appearance104;
            appearance103.BorderColor = System.Drawing.Color.Silver;
            appearance103.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid5.DisplayLayout.Override.CellAppearance = appearance103;
            this.uGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance107.BackColor = System.Drawing.SystemColors.Control;
            appearance107.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance107.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance107.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance107;
            appearance109.TextHAlignAsString = "Left";
            this.uGrid5.DisplayLayout.Override.HeaderAppearance = appearance109;
            this.uGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance108.BackColor = System.Drawing.SystemColors.Window;
            appearance108.BorderColor = System.Drawing.Color.Silver;
            this.uGrid5.DisplayLayout.Override.RowAppearance = appearance108;
            this.uGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance106;
            this.uGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid5.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid5.Location = new System.Drawing.Point(10, 36);
            this.uGrid5.Name = "uGrid5";
            this.uGrid5.Size = new System.Drawing.Size(865, 292);
            this.uGrid5.TabIndex = 0;
            this.uGrid5.Text = "ultraGrid1";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateCertiReqDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateCertiReqDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel9);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCertiReaDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextEquipSearchName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchEquipCode);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipCertiReqTypeCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSerchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateCertiReqDateTo
            // 
            this.uDateCertiReqDateTo.Location = new System.Drawing.Point(497, 36);
            this.uDateCertiReqDateTo.Name = "uDateCertiReqDateTo";
            this.uDateCertiReqDateTo.Size = new System.Drawing.Size(86, 21);
            this.uDateCertiReqDateTo.TabIndex = 157;
            // 
            // uDateCertiReqDateFrom
            // 
            this.uDateCertiReqDateFrom.Location = new System.Drawing.Point(398, 36);
            this.uDateCertiReqDateFrom.Name = "uDateCertiReqDateFrom";
            this.uDateCertiReqDateFrom.Size = new System.Drawing.Size(86, 21);
            this.uDateCertiReqDateFrom.TabIndex = 156;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(483, 40);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(14, 12);
            this.ultraLabel9.TabIndex = 158;
            this.ultraLabel9.Text = "~";
            // 
            // uLabelSearchCertiReaDate
            // 
            this.uLabelSearchCertiReaDate.Location = new System.Drawing.Point(278, 36);
            this.uLabelSearchCertiReaDate.Name = "uLabelSearchCertiReaDate";
            this.uLabelSearchCertiReaDate.Size = new System.Drawing.Size(117, 20);
            this.uLabelSearchCertiReaDate.TabIndex = 80;
            // 
            // uTextEquipSearchName
            // 
            appearance66.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipSearchName.Appearance = appearance66;
            this.uTextEquipSearchName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipSearchName.Location = new System.Drawing.Point(782, 12);
            this.uTextEquipSearchName.Name = "uTextEquipSearchName";
            this.uTextEquipSearchName.ReadOnly = true;
            this.uTextEquipSearchName.Size = new System.Drawing.Size(120, 21);
            this.uTextEquipSearchName.TabIndex = 79;
            // 
            // uTextSearchEquipCode
            // 
            appearance67.Image = ((object)(resources.GetObject("appearance67.Image")));
            appearance67.TextHAlignAsString = "Center";
            editorButton3.Appearance = appearance67;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchEquipCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchEquipCode.Location = new System.Drawing.Point(675, 12);
            this.uTextSearchEquipCode.Name = "uTextSearchEquipCode";
            this.uTextSearchEquipCode.Size = new System.Drawing.Size(103, 21);
            this.uTextSearchEquipCode.TabIndex = 79;
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(555, 12);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(117, 20);
            this.ultraLabel3.TabIndex = 78;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(10, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(117, 20);
            this.ultraLabel2.TabIndex = 78;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(278, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(117, 20);
            this.ultraLabel1.TabIndex = 78;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(10, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(117, 20);
            this.uLabelPlant.TabIndex = 78;
            this.uLabelPlant.Text = "공장";
            // 
            // uComboEquipCertiReqTypeCode
            // 
            this.uComboEquipCertiReqTypeCode.Location = new System.Drawing.Point(130, 36);
            this.uComboEquipCertiReqTypeCode.Name = "uComboEquipCertiReqTypeCode";
            this.uComboEquipCertiReqTypeCode.Size = new System.Drawing.Size(123, 21);
            this.uComboEquipCertiReqTypeCode.TabIndex = 77;
            this.uComboEquipCertiReqTypeCode.Text = "ultraComboEditor1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(398, 12);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchProcess.TabIndex = 77;
            this.uComboSearchProcess.Text = "ultraComboEditor1";
            // 
            // uComboSerchPlant
            // 
            this.uComboSerchPlant.Location = new System.Drawing.Point(130, 12);
            this.uComboSerchPlant.Name = "uComboSerchPlant";
            this.uComboSerchPlant.Size = new System.Drawing.Size(123, 21);
            this.uComboSerchPlant.TabIndex = 77;
            this.uComboSerchPlant.Text = "ultraComboEditor1";
            this.uComboSerchPlant.ValueChanged += new System.EventHandler(this.uComboSerchPlant_ValueChanged);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance4;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(917, 720);
            this.uGrid1.TabIndex = 3;
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(911, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl3);
            this.uTab.Location = new System.Drawing.Point(14, 207);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(890, 442);
            this.uTab.TabIndex = 104;
            ultraTab2.Key = "Set";
            ultraTab2.TabPage = this.ultraTabPageControl1;
            ultraTab2.Text = "Setup현황";
            ultraTab3.Key = "Req";
            ultraTab3.TabPage = this.ultraTabPageControl2;
            ultraTab3.Text = "인증의뢰사항";
            ultraTab4.Key = "App";
            ultraTab4.TabPage = this.ultraTabPageControl3;
            ultraTab4.Text = "인증평가사항";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab2,
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(886, 416);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uTextModel);
            this.uGroupBox2.Controls.Add(this.uGridSubEquip);
            this.uGroupBox2.Controls.Add(this.uTextMDMTFlag);
            this.uGroupBox2.Controls.Add(this.uTextPlantCode);
            this.uGroupBox2.Controls.Add(this.uTextPlantName);
            this.uGroupBox2.Controls.Add(this.uLabelPlantName);
            this.uGroupBox2.Controls.Add(this.uCheckTechStandardFlag);
            this.uGroupBox2.Controls.Add(this.uCheckWorkStandardFlag);
            this.uGroupBox2.Controls.Add(this.uCheckEquipStandardFlag);
            this.uGroupBox2.Controls.Add(this.uTextDocCode);
            this.uGroupBox2.Controls.Add(this.ultraLabel4);
            this.uGroupBox2.Controls.Add(this.uTextEquipCertiReqTypeCode);
            this.uGroupBox2.Controls.Add(this.uTextCertiReqDate);
            this.uGroupBox2.Controls.Add(this.uTextCertiReqName);
            this.uGroupBox2.Controls.Add(this.uTextCertiReqID);
            this.uGroupBox2.Controls.Add(this.uTextCertiReqDept);
            this.uGroupBox2.Controls.Add(this.uLabel5);
            this.uGroupBox2.Controls.Add(this.uLabel4);
            this.uGroupBox2.Controls.Add(this.uLabel3);
            this.uGroupBox2.Controls.Add(this.uLabel6);
            this.uGroupBox2.Controls.Add(this.uTextObject);
            this.uGroupBox2.Controls.Add(this.uTextEquipName);
            this.uGroupBox2.Controls.Add(this.uTextSerial);
            this.uGroupBox2.Controls.Add(this.uTextEquipCode);
            this.uGroupBox2.Controls.Add(this.uTextPackage);
            this.uGroupBox2.Controls.Add(this.uTextVendorName);
            this.uGroupBox2.Controls.Add(this.uTextProcess);
            this.uGroupBox2.Controls.Add(this.uLabel7);
            this.uGroupBox2.Controls.Add(this.uLabel10);
            this.uGroupBox2.Controls.Add(this.uLabel14);
            this.uGroupBox2.Controls.Add(this.uLabel13);
            this.uGroupBox2.Controls.Add(this.uLabelPackage);
            this.uGroupBox2.Controls.Add(this.uLabel9);
            this.uGroupBox2.Location = new System.Drawing.Point(10, 16);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(894, 185);
            this.uGroupBox2.TabIndex = 103;
            // 
            // uTextModel
            // 
            appearance125.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance125;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(610, 152);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(21, 21);
            this.uTextModel.TabIndex = 122;
            this.uTextModel.Visible = false;
            // 
            // uGridSubEquip
            // 
            appearance115.BackColor = System.Drawing.SystemColors.Window;
            appearance115.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSubEquip.DisplayLayout.Appearance = appearance115;
            this.uGridSubEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSubEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance112.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance112.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance112.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSubEquip.DisplayLayout.GroupByBox.Appearance = appearance112;
            appearance113.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSubEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance113;
            this.uGridSubEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance114.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance114.BackColor2 = System.Drawing.SystemColors.Control;
            appearance114.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance114.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSubEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance114;
            this.uGridSubEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSubEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance123.BackColor = System.Drawing.SystemColors.Window;
            appearance123.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSubEquip.DisplayLayout.Override.ActiveCellAppearance = appearance123;
            appearance118.BackColor = System.Drawing.SystemColors.Highlight;
            appearance118.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSubEquip.DisplayLayout.Override.ActiveRowAppearance = appearance118;
            this.uGridSubEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSubEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance117.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSubEquip.DisplayLayout.Override.CardAreaAppearance = appearance117;
            appearance116.BorderColor = System.Drawing.Color.Silver;
            appearance116.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSubEquip.DisplayLayout.Override.CellAppearance = appearance116;
            this.uGridSubEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSubEquip.DisplayLayout.Override.CellPadding = 0;
            appearance120.BackColor = System.Drawing.SystemColors.Control;
            appearance120.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance120.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance120.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance120.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSubEquip.DisplayLayout.Override.GroupByRowAppearance = appearance120;
            appearance122.TextHAlignAsString = "Left";
            this.uGridSubEquip.DisplayLayout.Override.HeaderAppearance = appearance122;
            this.uGridSubEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSubEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance121.BackColor = System.Drawing.SystemColors.Window;
            appearance121.BorderColor = System.Drawing.Color.Silver;
            this.uGridSubEquip.DisplayLayout.Override.RowAppearance = appearance121;
            this.uGridSubEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance119.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSubEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance119;
            this.uGridSubEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSubEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSubEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSubEquip.Location = new System.Drawing.Point(662, 16);
            this.uGridSubEquip.Name = "uGridSubEquip";
            this.uGridSubEquip.Size = new System.Drawing.Size(226, 156);
            this.uGridSubEquip.TabIndex = 121;
            this.uGridSubEquip.Text = "ultraGrid1";
            // 
            // uTextMDMTFlag
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMDMTFlag.Appearance = appearance58;
            this.uTextMDMTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMDMTFlag.Location = new System.Drawing.Point(562, 156);
            this.uTextMDMTFlag.Name = "uTextMDMTFlag";
            this.uTextMDMTFlag.ReadOnly = true;
            this.uTextMDMTFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextMDMTFlag.TabIndex = 120;
            this.uTextMDMTFlag.Visible = false;
            // 
            // uTextPlantCode
            // 
            appearance111.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance111;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(583, 156);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(17, 21);
            this.uTextPlantCode.TabIndex = 119;
            // 
            // uTextPlantName
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance55;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(391, 16);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(120, 21);
            this.uTextPlantName.TabIndex = 117;
            this.uTextPlantName.Visible = false;
            // 
            // uLabelPlantName
            // 
            this.uLabelPlantName.Location = new System.Drawing.Point(264, 16);
            this.uLabelPlantName.Name = "uLabelPlantName";
            this.uLabelPlantName.Size = new System.Drawing.Size(123, 20);
            this.uLabelPlantName.TabIndex = 118;
            this.uLabelPlantName.Text = "3";
            this.uLabelPlantName.Visible = false;
            // 
            // uCheckTechStandardFlag
            // 
            this.uCheckTechStandardFlag.Location = new System.Drawing.Point(319, 160);
            this.uCheckTechStandardFlag.Name = "uCheckTechStandardFlag";
            this.uCheckTechStandardFlag.Size = new System.Drawing.Size(89, 20);
            this.uCheckTechStandardFlag.TabIndex = 116;
            this.uCheckTechStandardFlag.Text = "기술표준";
            // 
            // uCheckWorkStandardFlag
            // 
            this.uCheckWorkStandardFlag.Location = new System.Drawing.Point(230, 160);
            this.uCheckWorkStandardFlag.Name = "uCheckWorkStandardFlag";
            this.uCheckWorkStandardFlag.Size = new System.Drawing.Size(86, 20);
            this.uCheckWorkStandardFlag.TabIndex = 115;
            this.uCheckWorkStandardFlag.Text = "작업표준";
            // 
            // uCheckEquipStandardFlag
            // 
            this.uCheckEquipStandardFlag.Location = new System.Drawing.Point(141, 160);
            this.uCheckEquipStandardFlag.Name = "uCheckEquipStandardFlag";
            this.uCheckEquipStandardFlag.Size = new System.Drawing.Size(86, 20);
            this.uCheckEquipStandardFlag.TabIndex = 114;
            this.uCheckEquipStandardFlag.Text = "설비표준";
            // 
            // uTextDocCode
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocCode.Appearance = appearance54;
            this.uTextDocCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocCode.Location = new System.Drawing.Point(137, 16);
            this.uTextDocCode.Name = "uTextDocCode";
            this.uTextDocCode.ReadOnly = true;
            this.uTextDocCode.Size = new System.Drawing.Size(120, 21);
            this.uTextDocCode.TabIndex = 112;
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(10, 16);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(123, 20);
            this.ultraLabel4.TabIndex = 113;
            this.ultraLabel4.Text = "3";
            // 
            // uTextEquipCertiReqTypeCode
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCertiReqTypeCode.Appearance = appearance57;
            this.uTextEquipCertiReqTypeCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCertiReqTypeCode.Location = new System.Drawing.Point(137, 64);
            this.uTextEquipCertiReqTypeCode.Name = "uTextEquipCertiReqTypeCode";
            this.uTextEquipCertiReqTypeCode.ReadOnly = true;
            this.uTextEquipCertiReqTypeCode.Size = new System.Drawing.Size(120, 21);
            this.uTextEquipCertiReqTypeCode.TabIndex = 106;
            // 
            // uTextCertiReqDate
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqDate.Appearance = appearance72;
            this.uTextCertiReqDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqDate.Location = new System.Drawing.Point(137, 40);
            this.uTextCertiReqDate.Name = "uTextCertiReqDate";
            this.uTextCertiReqDate.ReadOnly = true;
            this.uTextCertiReqDate.Size = new System.Drawing.Size(120, 21);
            this.uTextCertiReqDate.TabIndex = 104;
            // 
            // uTextCertiReqName
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqName.Appearance = appearance71;
            this.uTextCertiReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqName.Location = new System.Drawing.Point(480, 40);
            this.uTextCertiReqName.Name = "uTextCertiReqName";
            this.uTextCertiReqName.ReadOnly = true;
            this.uTextCertiReqName.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiReqName.TabIndex = 107;
            // 
            // uTextCertiReqID
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqID.Appearance = appearance59;
            this.uTextCertiReqID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqID.Location = new System.Drawing.Point(391, 40);
            this.uTextCertiReqID.Name = "uTextCertiReqID";
            this.uTextCertiReqID.ReadOnly = true;
            this.uTextCertiReqID.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiReqID.TabIndex = 105;
            // 
            // uTextCertiReqDept
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqDept.Appearance = appearance53;
            this.uTextCertiReqDept.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCertiReqDept.Location = new System.Drawing.Point(569, 40);
            this.uTextCertiReqDept.Name = "uTextCertiReqDept";
            this.uTextCertiReqDept.ReadOnly = true;
            this.uTextCertiReqDept.Size = new System.Drawing.Size(86, 21);
            this.uTextCertiReqDept.TabIndex = 108;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(10, 64);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(123, 20);
            this.uLabel5.TabIndex = 111;
            this.uLabel5.Text = "5";
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(264, 40);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(123, 20);
            this.uLabel4.TabIndex = 110;
            this.uLabel4.Text = "4";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(10, 40);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(123, 20);
            this.uLabel3.TabIndex = 109;
            this.uLabel3.Text = "3";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(10, 88);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(123, 20);
            this.uLabel6.TabIndex = 93;
            this.uLabel6.Text = "6";
            // 
            // uTextObject
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextObject.Appearance = appearance2;
            this.uTextObject.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextObject.Location = new System.Drawing.Point(137, 136);
            this.uTextObject.Name = "uTextObject";
            this.uTextObject.ReadOnly = true;
            this.uTextObject.Size = new System.Drawing.Size(367, 21);
            this.uTextObject.TabIndex = 79;
            // 
            // uTextEquipName
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance60;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(480, 64);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(120, 21);
            this.uTextEquipName.TabIndex = 79;
            // 
            // uTextSerial
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerial.Appearance = appearance61;
            this.uTextSerial.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerial.Location = new System.Drawing.Point(391, 88);
            this.uTextSerial.Name = "uTextSerial";
            this.uTextSerial.ReadOnly = true;
            this.uTextSerial.Size = new System.Drawing.Size(120, 21);
            this.uTextSerial.TabIndex = 79;
            // 
            // uTextEquipCode
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance62;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(391, 64);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(86, 21);
            this.uTextEquipCode.TabIndex = 79;
            // 
            // uTextPackage
            // 
            appearance74.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance74;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(391, 112);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(120, 21);
            this.uTextPackage.TabIndex = 79;
            // 
            // uTextVendorName
            // 
            appearance124.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance124;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(137, 112);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(120, 21);
            this.uTextVendorName.TabIndex = 79;
            // 
            // uTextProcess
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Appearance = appearance56;
            this.uTextProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Location = new System.Drawing.Point(137, 88);
            this.uTextProcess.Name = "uTextProcess";
            this.uTextProcess.ReadOnly = true;
            this.uTextProcess.Size = new System.Drawing.Size(120, 21);
            this.uTextProcess.TabIndex = 79;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(264, 64);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(123, 20);
            this.uLabel7.TabIndex = 94;
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(264, 88);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(123, 20);
            this.uLabel10.TabIndex = 101;
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(10, 159);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(123, 20);
            this.uLabel14.TabIndex = 95;
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(10, 136);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(123, 20);
            this.uLabel13.TabIndex = 98;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(264, 112);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(123, 20);
            this.uLabelPackage.TabIndex = 100;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(10, 112);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(123, 20);
            this.uLabel9.TabIndex = 100;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(1048, 518);
            // 
            // frmQATZ0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0002";
            this.Load += new System.EventHandler(this.frmQATZ0002_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0002_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0002_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0002_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxTool)).EndInit();
            this.uGroupBoxTool.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMoldTool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSetup)).EndInit();
            this.uGroupBoxSetup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSetup)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionCertiResultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiAdmitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiChargeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiAdmitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiChargeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitStatusCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiReqDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCertiReqDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipSearchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipCertiReqTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSerchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSubEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMDMTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTechStandardFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStandardFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEquipStandardFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCertiReqTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCertiReqDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipCertiReqTypeCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSerchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipSearchName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchEquipCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCertiReqTypeCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiReqID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiReqDept;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEquipStandardFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckTechStandardFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWorkStandardFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.Misc.UltraLabel uLabel19;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxTool;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMoldTool;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSetup;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSetup;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid5;
        private Infragistics.Win.Misc.UltraButton uButtonDelRow;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMESTFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCertiAdmitDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCertiDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiAdmitName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiChargeName;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiAdmitID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCertiChargeID;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraButton uButtonDeny;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitStatusCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabelMold;
        private Infragistics.Win.Misc.UltraLabel uLabelSetup;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionCertiResultCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCertiReaDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCertiReqDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCertiReqDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMDMTFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSubEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
    }
}