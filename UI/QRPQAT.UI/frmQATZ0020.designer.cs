﻿namespace QRPQAT.UI
{
    partial class frmQATZ0020
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0020));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridSRRItemList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextTotal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTotal = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboEvaluationPeriod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEvaluationPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProdType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSRRList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboProdType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEvaluationPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProdType)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(424, 8);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(320, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(112, 8);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchCustomer.TabIndex = 1;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(8, 8);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 0;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 700);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 150);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 700);
            this.uGroupBoxContentsArea.TabIndex = 2;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 680);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGridSRRItemList);
            this.ultraGroupBox2.Controls.Add(this.ultraGroupBox3);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 60);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 620);
            this.ultraGroupBox2.TabIndex = 1;
            // 
            // uGridSRRItemList
            // 
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSRRItemList.DisplayLayout.Appearance = appearance17;
            this.uGridSRRItemList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSRRItemList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRItemList.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridSRRItemList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSRRItemList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSRRItemList.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSRRItemList.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridSRRItemList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSRRItemList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSRRItemList.DisplayLayout.Override.CellAppearance = appearance18;
            this.uGridSRRItemList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSRRItemList.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRItemList.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance24.TextHAlignAsString = "Left";
            this.uGridSRRItemList.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGridSRRItemList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSRRItemList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridSRRItemList.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridSRRItemList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSRRItemList.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.uGridSRRItemList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSRRItemList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSRRItemList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSRRItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSRRItemList.Location = new System.Drawing.Point(3, 36);
            this.uGridSRRItemList.Name = "uGridSRRItemList";
            this.uGridSRRItemList.Size = new System.Drawing.Size(1058, 581);
            this.uGridSRRItemList.TabIndex = 1;
            this.uGridSRRItemList.Text = "ultraGrid1";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uTextTotal);
            this.ultraGroupBox3.Controls.Add(this.uLabelTotal);
            this.ultraGroupBox3.Controls.Add(this.uButtonDelete);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1058, 36);
            this.ultraGroupBox3.TabIndex = 0;
            // 
            // uTextTotal
            // 
            this.uTextTotal.Location = new System.Drawing.Point(201, 8);
            this.uTextTotal.Name = "uTextTotal";
            this.uTextTotal.Size = new System.Drawing.Size(200, 21);
            this.uTextTotal.TabIndex = 13;
            this.uTextTotal.Text = "ultraTextEditor2";
            // 
            // uLabelTotal
            // 
            this.uLabelTotal.Location = new System.Drawing.Point(97, 8);
            this.uLabelTotal.Name = "uLabelTotal";
            this.uLabelTotal.Size = new System.Drawing.Size(100, 20);
            this.uLabelTotal.TabIndex = 12;
            this.uLabelTotal.Text = "ultraLabel2";
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(4, 4);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 0;
            this.uButtonDelete.Text = "ultraButton1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uComboProdType);
            this.ultraGroupBox1.Controls.Add(this.uComboEvaluationPeriod);
            this.ultraGroupBox1.Controls.Add(this.uLabelEvaluationPeriod);
            this.ultraGroupBox1.Controls.Add(this.uLabelProdType);
            this.ultraGroupBox1.Controls.Add(this.uComboCustomer);
            this.ultraGroupBox1.Controls.Add(this.uLabelCustomer);
            this.ultraGroupBox1.Controls.Add(this.uComboPlant);
            this.ultraGroupBox1.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 60);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // uComboEvaluationPeriod
            // 
            this.uComboEvaluationPeriod.Location = new System.Drawing.Point(112, 32);
            this.uComboEvaluationPeriod.Name = "uComboEvaluationPeriod";
            this.uComboEvaluationPeriod.Size = new System.Drawing.Size(200, 21);
            this.uComboEvaluationPeriod.TabIndex = 13;
            this.uComboEvaluationPeriod.Text = "ultraComboEditor1";
            // 
            // uLabelEvaluationPeriod
            // 
            this.uLabelEvaluationPeriod.Location = new System.Drawing.Point(8, 32);
            this.uLabelEvaluationPeriod.Name = "uLabelEvaluationPeriod";
            this.uLabelEvaluationPeriod.Size = new System.Drawing.Size(100, 20);
            this.uLabelEvaluationPeriod.TabIndex = 12;
            this.uLabelEvaluationPeriod.Text = "ultraLabel1";
            // 
            // uLabelProdType
            // 
            this.uLabelProdType.Location = new System.Drawing.Point(320, 8);
            this.uLabelProdType.Name = "uLabelProdType";
            this.uLabelProdType.Size = new System.Drawing.Size(100, 20);
            this.uLabelProdType.TabIndex = 10;
            this.uLabelProdType.Text = "ultraLabel1";
            // 
            // uComboCustomer
            // 
            this.uComboCustomer.Location = new System.Drawing.Point(112, 8);
            this.uComboCustomer.Name = "uComboCustomer";
            this.uComboCustomer.Size = new System.Drawing.Size(200, 21);
            this.uComboCustomer.TabIndex = 9;
            this.uComboCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(8, 8);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 8;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(856, 8);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboPlant.TabIndex = 7;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(752, 8);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 6;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGridSRRList
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSRRList.DisplayLayout.Appearance = appearance2;
            this.uGridSRRList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSRRList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridSRRList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRRList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridSRRList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSRRList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSRRList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSRRList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridSRRList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSRRList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSRRList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridSRRList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSRRList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRRList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridSRRList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridSRRList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSRRList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridSRRList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridSRRList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSRRList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridSRRList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSRRList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSRRList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSRRList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSRRList.Location = new System.Drawing.Point(0, 80);
            this.uGridSRRList.Name = "uGridSRRList";
            this.uGridSRRList.Size = new System.Drawing.Size(1070, 70);
            this.uGridSRRList.TabIndex = 3;
            this.uGridSRRList.Text = "ultraGrid1";
            // 
            // uComboProdType
            // 
            this.uComboProdType.Location = new System.Drawing.Point(424, 8);
            this.uComboProdType.Name = "uComboProdType";
            this.uComboProdType.Size = new System.Drawing.Size(200, 21);
            this.uComboProdType.TabIndex = 11;
            this.uComboProdType.Text = "ultraComboEditor1";
            // 
            // frmQATZ0020
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridSRRList);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0020";
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEvaluationPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRRList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProdType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSRRList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEvaluationPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelEvaluationPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelProdType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTotal;
        private Infragistics.Win.Misc.UltraLabel uLabelTotal;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSRRItemList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProdType;
    }
}