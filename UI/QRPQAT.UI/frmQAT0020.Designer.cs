﻿namespace QRPQAT.UI
{
    partial class frmQAT0020
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton4 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton5 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQAT0020));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumRRValueChangeValue = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckRRValueUseFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckRRValueChangeFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumAccuracyValue = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckStabilityFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckAccuracyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumLowerSpec = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumUpperSpec = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uComboUnitCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSpecRange = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUnitCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uNumPartCount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboMeasureCount = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboRepeatCount = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMeasureName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMeasureCount = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPartCount = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepeatCount = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckRNRDataInputFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckRNRAnalysisFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextInspectDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uComboProcessCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectItemDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMeasureToolCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItemDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlantCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProjectName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProjectCode = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchProjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProject = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRRValueChangeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRRValueUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRRValueChangeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumAccuracyValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumLowerSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumUpperSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSpecRange)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumPartCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMeasureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRNRDataInputFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRNRAnalysisFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProjectName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox3);
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1040, 442);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uNumRRValueChangeValue);
            this.uGroupBox3.Controls.Add(this.uCheckRRValueUseFlag);
            this.uGroupBox3.Controls.Add(this.uCheckRRValueChangeFlag);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 144);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1016, 108);
            this.uGroupBox3.TabIndex = 157;
            // 
            // uNumRRValueChangeValue
            // 
            appearance49.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumRRValueChangeValue.Appearance = appearance49;
            this.uNumRRValueChangeValue.BackColor = System.Drawing.Color.PowderBlue;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Text = "0";
            this.uNumRRValueChangeValue.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRRValueChangeValue.ButtonsRight.Add(spinEditorButton1);
            this.uNumRRValueChangeValue.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRRValueChangeValue.Location = new System.Drawing.Point(288, 48);
            this.uNumRRValueChangeValue.Name = "uNumRRValueChangeValue";
            this.uNumRRValueChangeValue.PromptChar = ' ';
            this.uNumRRValueChangeValue.Size = new System.Drawing.Size(100, 21);
            this.uNumRRValueChangeValue.TabIndex = 160;
            this.uNumRRValueChangeValue.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumRRValueChangeValue_EditorSpinButtonClick);
            this.uNumRRValueChangeValue.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumRRValueChangeValue_EditorButtonClick);
            // 
            // uCheckRRValueUseFlag
            // 
            this.uCheckRRValueUseFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckRRValueUseFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRRValueUseFlag.Location = new System.Drawing.Point(12, 48);
            this.uCheckRRValueUseFlag.Name = "uCheckRRValueUseFlag";
            this.uCheckRRValueUseFlag.Size = new System.Drawing.Size(130, 20);
            this.uCheckRRValueUseFlag.TabIndex = 154;
            this.uCheckRRValueUseFlag.Text = "R&R 설정값사용";
            this.uCheckRRValueUseFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckRRValueUseFlag.CheckedChanged += new System.EventHandler(this.uCheckRRValueUseFlag_CheckedChanged);
            // 
            // uCheckRRValueChangeFlag
            // 
            this.uCheckRRValueChangeFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckRRValueChangeFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRRValueChangeFlag.Location = new System.Drawing.Point(152, 48);
            this.uCheckRRValueChangeFlag.Name = "uCheckRRValueChangeFlag";
            this.uCheckRRValueChangeFlag.Size = new System.Drawing.Size(130, 20);
            this.uCheckRRValueChangeFlag.TabIndex = 154;
            this.uCheckRRValueChangeFlag.Text = "% R＆R값 변경";
            this.uCheckRRValueChangeFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckRRValueChangeFlag.CheckedChanged += new System.EventHandler(this.uCheckRRValueChangeFlag_CheckedChanged);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uNumAccuracyValue);
            this.uGroupBox2.Controls.Add(this.uCheckStabilityFlag);
            this.uGroupBox2.Controls.Add(this.uCheckAccuracyFlag);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 16);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1016, 108);
            this.uGroupBox2.TabIndex = 156;
            // 
            // uNumAccuracyValue
            // 
            appearance47.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumAccuracyValue.Appearance = appearance47;
            this.uNumAccuracyValue.BackColor = System.Drawing.Color.PowderBlue;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Text = "0";
            this.uNumAccuracyValue.ButtonsLeft.Add(editorButton2);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumAccuracyValue.ButtonsRight.Add(spinEditorButton2);
            this.uNumAccuracyValue.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumAccuracyValue.Location = new System.Drawing.Point(288, 48);
            this.uNumAccuracyValue.Name = "uNumAccuracyValue";
            this.uNumAccuracyValue.PromptChar = ' ';
            this.uNumAccuracyValue.Size = new System.Drawing.Size(100, 21);
            this.uNumAccuracyValue.TabIndex = 157;
            this.uNumAccuracyValue.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumAccuracyValue_EditorSpinButtonClick);
            this.uNumAccuracyValue.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumAccuracyValue_EditorButtonClick);
            // 
            // uCheckStabilityFlag
            // 
            this.uCheckStabilityFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckStabilityFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckStabilityFlag.Location = new System.Drawing.Point(12, 48);
            this.uCheckStabilityFlag.Name = "uCheckStabilityFlag";
            this.uCheckStabilityFlag.Size = new System.Drawing.Size(130, 20);
            this.uCheckStabilityFlag.TabIndex = 156;
            this.uCheckStabilityFlag.Text = "안정성(Stability)";
            this.uCheckStabilityFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckAccuracyFlag
            // 
            this.uCheckAccuracyFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckAccuracyFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckAccuracyFlag.Location = new System.Drawing.Point(152, 48);
            this.uCheckAccuracyFlag.Name = "uCheckAccuracyFlag";
            this.uCheckAccuracyFlag.Size = new System.Drawing.Size(130, 20);
            this.uCheckAccuracyFlag.TabIndex = 155;
            this.uCheckAccuracyFlag.Text = "정확성(Accuracy)";
            this.uCheckAccuracyFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckAccuracyFlag.CheckedChanged += new System.EventHandler(this.uCheckAccuracyFlag_CheckedChanged);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uNumLowerSpec);
            this.uGroupBox1.Controls.Add(this.uNumUpperSpec);
            this.uGroupBox1.Controls.Add(this.uComboUnitCode);
            this.uGroupBox1.Controls.Add(this.uComboSpecRange);
            this.uGroupBox1.Controls.Add(this.ultraLabel6);
            this.uGroupBox1.Controls.Add(this.uLabelUnitCode);
            this.uGroupBox1.Controls.Add(this.uLabelSpec);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 272);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1016, 108);
            this.uGroupBox1.TabIndex = 155;
            // 
            // uNumLowerSpec
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumLowerSpec.Appearance = appearance14;
            this.uNumLowerSpec.BackColor = System.Drawing.Color.PowderBlue;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Text = "0";
            this.uNumLowerSpec.ButtonsLeft.Add(editorButton3);
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumLowerSpec.ButtonsRight.Add(spinEditorButton3);
            this.uNumLowerSpec.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumLowerSpec.Location = new System.Drawing.Point(128, 44);
            this.uNumLowerSpec.Name = "uNumLowerSpec";
            this.uNumLowerSpec.PromptChar = ' ';
            this.uNumLowerSpec.Size = new System.Drawing.Size(100, 21);
            this.uNumLowerSpec.TabIndex = 162;
            this.uNumLowerSpec.AfterExitEditMode += new System.EventHandler(this.uNumLowerSpec_AfterExitEditMode);
            this.uNumLowerSpec.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumLowerSpec_EditorSpinButtonClick);
            this.uNumLowerSpec.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumLowerSpec_EditorButtonClick);
            // 
            // uNumUpperSpec
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uNumUpperSpec.Appearance = appearance15;
            this.uNumUpperSpec.BackColor = System.Drawing.Color.PowderBlue;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Text = "0";
            this.uNumUpperSpec.ButtonsLeft.Add(editorButton4);
            spinEditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumUpperSpec.ButtonsRight.Add(spinEditorButton4);
            this.uNumUpperSpec.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumUpperSpec.Location = new System.Drawing.Point(244, 44);
            this.uNumUpperSpec.Name = "uNumUpperSpec";
            this.uNumUpperSpec.PromptChar = ' ';
            this.uNumUpperSpec.Size = new System.Drawing.Size(100, 21);
            this.uNumUpperSpec.TabIndex = 161;
            this.uNumUpperSpec.AfterExitEditMode += new System.EventHandler(this.uNumUpperSpec_AfterExitEditMode);
            this.uNumUpperSpec.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumUpperSpec_EditorSpinButtonClick);
            this.uNumUpperSpec.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumUpperSpec_EditorButtonClick);
            // 
            // uComboUnitCode
            // 
            this.uComboUnitCode.Location = new System.Drawing.Point(572, 44);
            this.uComboUnitCode.Name = "uComboUnitCode";
            this.uComboUnitCode.Size = new System.Drawing.Size(120, 21);
            this.uComboUnitCode.TabIndex = 155;
            this.uComboUnitCode.Text = "ultraComboEditor2";
            // 
            // uComboSpecRange
            // 
            this.uComboSpecRange.Location = new System.Drawing.Point(348, 44);
            this.uComboSpecRange.Name = "uComboSpecRange";
            this.uComboSpecRange.Size = new System.Drawing.Size(100, 21);
            this.uComboSpecRange.TabIndex = 155;
            this.uComboSpecRange.Text = "ultraComboEditor2";
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(229, 48);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(12, 12);
            this.ultraLabel6.TabIndex = 154;
            this.ultraLabel6.Text = "-";
            // 
            // uLabelUnitCode
            // 
            this.uLabelUnitCode.Location = new System.Drawing.Point(456, 44);
            this.uLabelUnitCode.Name = "uLabelUnitCode";
            this.uLabelUnitCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelUnitCode.TabIndex = 152;
            this.uLabelUnitCode.Text = "단위";
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 44);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 152;
            this.uLabelSpec.Text = "부품";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uNumPartCount);
            this.ultraTabPageControl2.Controls.Add(this.uGrid2);
            this.ultraTabPageControl2.Controls.Add(this.uComboMeasureCount);
            this.ultraTabPageControl2.Controls.Add(this.uComboRepeatCount);
            this.ultraTabPageControl2.Controls.Add(this.uLabelMeasureName);
            this.ultraTabPageControl2.Controls.Add(this.uLabelMeasureCount);
            this.ultraTabPageControl2.Controls.Add(this.uLabelPartCount);
            this.ultraTabPageControl2.Controls.Add(this.uLabelRepeatCount);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1040, 442);
            // 
            // uNumPartCount
            // 
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Text = "0";
            this.uNumPartCount.ButtonsLeft.Add(editorButton5);
            spinEditorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumPartCount.ButtonsRight.Add(spinEditorButton5);
            this.uNumPartCount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumPartCount.Location = new System.Drawing.Point(432, 12);
            this.uNumPartCount.Name = "uNumPartCount";
            this.uNumPartCount.PromptChar = ' ';
            this.uNumPartCount.Size = new System.Drawing.Size(100, 21);
            this.uNumPartCount.TabIndex = 163;
            this.uNumPartCount.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumPartCount_EditorSpinButtonClick);
            this.uNumPartCount.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumPartCount_EditorButtonClick);
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance34;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance31;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance32;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance33.BackColor2 = System.Drawing.SystemColors.Control;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance33;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance42;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance36;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            appearance35.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance35;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance41.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance41;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance40;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 56);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1016, 376);
            this.uGrid2.TabIndex = 155;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellChange);
            // 
            // uComboMeasureCount
            // 
            this.uComboMeasureCount.Location = new System.Drawing.Point(684, 12);
            this.uComboMeasureCount.Name = "uComboMeasureCount";
            this.uComboMeasureCount.Size = new System.Drawing.Size(144, 21);
            this.uComboMeasureCount.TabIndex = 153;
            this.uComboMeasureCount.Text = "ultraComboEditor4";
            this.uComboMeasureCount.ValueChanged += new System.EventHandler(this.uComboMeasureCount_ValueChanged);
            // 
            // uComboRepeatCount
            // 
            this.uComboRepeatCount.Location = new System.Drawing.Point(136, 12);
            this.uComboRepeatCount.Name = "uComboRepeatCount";
            this.uComboRepeatCount.Size = new System.Drawing.Size(144, 21);
            this.uComboRepeatCount.TabIndex = 153;
            this.uComboRepeatCount.Text = "ultraComboEditor4";
            // 
            // uLabelMeasureName
            // 
            this.uLabelMeasureName.Location = new System.Drawing.Point(12, 36);
            this.uLabelMeasureName.Name = "uLabelMeasureName";
            this.uLabelMeasureName.Size = new System.Drawing.Size(120, 20);
            this.uLabelMeasureName.TabIndex = 152;
            this.uLabelMeasureName.Text = "계측명";
            // 
            // uLabelMeasureCount
            // 
            this.uLabelMeasureCount.Location = new System.Drawing.Point(560, 12);
            this.uLabelMeasureCount.Name = "uLabelMeasureCount";
            this.uLabelMeasureCount.Size = new System.Drawing.Size(120, 20);
            this.uLabelMeasureCount.TabIndex = 152;
            this.uLabelMeasureCount.Text = "계측수";
            // 
            // uLabelPartCount
            // 
            this.uLabelPartCount.Location = new System.Drawing.Point(308, 12);
            this.uLabelPartCount.Name = "uLabelPartCount";
            this.uLabelPartCount.Size = new System.Drawing.Size(120, 20);
            this.uLabelPartCount.TabIndex = 152;
            this.uLabelPartCount.Text = "부품수";
            // 
            // uLabelRepeatCount
            // 
            this.uLabelRepeatCount.Location = new System.Drawing.Point(12, 12);
            this.uLabelRepeatCount.Name = "uLabelRepeatCount";
            this.uLabelRepeatCount.Size = new System.Drawing.Size(120, 20);
            this.uLabelRepeatCount.TabIndex = 152;
            this.uLabelRepeatCount.Text = "반복";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.TabIndex = 10;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboProcessCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProcessCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItemDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProjectName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProjectCode);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox4.Controls.Add(this.uCheckRNRDataInputFlag);
            this.uGroupBox4.Controls.Add(this.uCheckRNRAnalysisFlag);
            this.uGroupBox4.Controls.Add(this.uDateInspectDate);
            this.uGroupBox4.Controls.Add(this.uTextInspectDesc);
            this.uGroupBox4.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox4.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox4.Controls.Add(this.uLabelInspectDesc);
            this.uGroupBox4.Controls.Add(this.uLabelInspectDate);
            this.uGroupBox4.Controls.Add(this.uLabelInspectUser);
            this.uGroupBox4.Location = new System.Drawing.Point(8, 116);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(1032, 84);
            this.uGroupBox4.TabIndex = 158;
            // 
            // uCheckRNRDataInputFlag
            // 
            this.uCheckRNRDataInputFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRNRDataInputFlag.Location = new System.Drawing.Point(596, 28);
            this.uCheckRNRDataInputFlag.Name = "uCheckRNRDataInputFlag";
            this.uCheckRNRDataInputFlag.Size = new System.Drawing.Size(184, 20);
            this.uCheckRNRDataInputFlag.TabIndex = 158;
            this.uCheckRNRDataInputFlag.Text = "계측기데이터 입력여부";
            this.uCheckRNRDataInputFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckRNRAnalysisFlag
            // 
            this.uCheckRNRAnalysisFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckRNRAnalysisFlag.Location = new System.Drawing.Point(596, 52);
            this.uCheckRNRAnalysisFlag.Name = "uCheckRNRAnalysisFlag";
            this.uCheckRNRAnalysisFlag.Size = new System.Drawing.Size(184, 20);
            this.uCheckRNRAnalysisFlag.TabIndex = 157;
            this.uCheckRNRAnalysisFlag.Text = "R&R결과 분석여부";
            this.uCheckRNRAnalysisFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateInspectDate
            // 
            appearance23.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDate.Appearance = appearance23;
            this.uDateInspectDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateInspectDate.Location = new System.Drawing.Point(136, 28);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDate.TabIndex = 156;
            // 
            // uTextInspectDesc
            // 
            this.uTextInspectDesc.Location = new System.Drawing.Point(136, 52);
            this.uTextInspectDesc.Name = "uTextInspectDesc";
            this.uTextInspectDesc.Size = new System.Drawing.Size(436, 21);
            this.uTextInspectDesc.TabIndex = 155;
            // 
            // uTextInspectUserName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance25;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(472, 28);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserName.TabIndex = 91;
            // 
            // uTextInspectUserID
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectUserID.Appearance = appearance27;
            this.uTextInspectUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance26.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance26.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance26;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton6);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(368, 28);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 90;
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uLabelInspectDesc
            // 
            this.uLabelInspectDesc.Location = new System.Drawing.Point(20, 52);
            this.uLabelInspectDesc.Name = "uLabelInspectDesc";
            this.uLabelInspectDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectDesc.TabIndex = 153;
            this.uLabelInspectDesc.Text = "설명";
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(20, 28);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectDate.TabIndex = 149;
            this.uLabelInspectDate.Text = "검사일";
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(252, 28);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectUser.TabIndex = 151;
            this.uLabelInspectUser.Text = "검사자";
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Location = new System.Drawing.Point(12, 216);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1044, 468);
            this.uTab.TabIndex = 157;
            ultraTab7.Key = "Item";
            ultraTab7.TabPage = this.ultraTabPageControl1;
            ultraTab7.Text = "측정항목 및 부품정보";
            ultraTab8.Key = "Measure";
            ultraTab8.TabPage = this.ultraTabPageControl2;
            ultraTab8.Text = "계측기 설정정보";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab7,
            ultraTab8});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1040, 442);
            // 
            // uComboProcessCode
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcessCode.Appearance = appearance22;
            this.uComboProcessCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcessCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboProcessCode.Location = new System.Drawing.Point(500, 36);
            this.uComboProcessCode.Name = "uComboProcessCode";
            this.uComboProcessCode.Size = new System.Drawing.Size(108, 21);
            this.uComboProcessCode.TabIndex = 127;
            this.uComboProcessCode.Text = "ultraComboEditor1";
            // 
            // uComboPlantCode
            // 
            appearance24.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlantCode.Appearance = appearance24;
            this.uComboPlantCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlantCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlantCode.Location = new System.Drawing.Point(500, 12);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(108, 21);
            this.uComboPlantCode.TabIndex = 127;
            this.uComboPlantCode.Text = "ultraComboEditor1";
            this.uComboPlantCode.ValueChanged += new System.EventHandler(this.uComboPlantCode_ValueChanged);
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(612, 60);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(110, 21);
            this.uTextMaterialName.TabIndex = 91;
            // 
            // uTextProjectName
            // 
            appearance44.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProjectName.Appearance = appearance44;
            this.uTextProjectName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProjectName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProjectName.Location = new System.Drawing.Point(128, 36);
            this.uTextProjectName.Name = "uTextProjectName";
            this.uTextProjectName.Size = new System.Drawing.Size(224, 21);
            this.uTextProjectName.TabIndex = 91;
            // 
            // uTextProjectCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Appearance = appearance20;
            this.uTextProjectCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Location = new System.Drawing.Point(128, 12);
            this.uTextProjectCode.Name = "uTextProjectCode";
            this.uTextProjectCode.ReadOnly = true;
            this.uTextProjectCode.Size = new System.Drawing.Size(110, 21);
            this.uTextProjectCode.TabIndex = 91;
            // 
            // uTextMeasureToolName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Appearance = appearance21;
            this.uTextMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Location = new System.Drawing.Point(244, 60);
            this.uTextMeasureToolName.Name = "uTextMeasureToolName";
            this.uTextMeasureToolName.ReadOnly = true;
            this.uTextMeasureToolName.Size = new System.Drawing.Size(108, 21);
            this.uTextMeasureToolName.TabIndex = 91;
            // 
            // uLabelProcessCode
            // 
            this.uLabelProcessCode.Location = new System.Drawing.Point(384, 36);
            this.uLabelProcessCode.Name = "uLabelProcessCode";
            this.uLabelProcessCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelProcessCode.TabIndex = 145;
            this.uLabelProcessCode.Text = "6";
            // 
            // uTextInspectItemDesc
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemDesc.Appearance = appearance28;
            this.uTextInspectItemDesc.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemDesc.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectItemDesc.Location = new System.Drawing.Point(128, 84);
            this.uTextInspectItemDesc.Name = "uTextInspectItemDesc";
            this.uTextInspectItemDesc.Size = new System.Drawing.Size(224, 21);
            this.uTextInspectItemDesc.TabIndex = 90;
            // 
            // uTextMaterialCode
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialCode.Appearance = appearance29;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance19.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance19;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMaterialCode.ButtonsRight.Add(editorButton7);
            this.uTextMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMaterialCode.Location = new System.Drawing.Point(500, 60);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.Size = new System.Drawing.Size(108, 21);
            this.uTextMaterialCode.TabIndex = 90;
            this.uTextMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextMaterialCode_KeyDown);
            this.uTextMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMaterialCode_EditorButtonClick);
            // 
            // uTextMeasureToolCode
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolCode.Appearance = appearance30;
            this.uTextMeasureToolCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance17.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance17.TextHAlignAsString = "Center";
            editorButton8.Appearance = appearance17;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMeasureToolCode.ButtonsRight.Add(editorButton8);
            this.uTextMeasureToolCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMeasureToolCode.Location = new System.Drawing.Point(128, 60);
            this.uTextMeasureToolCode.Name = "uTextMeasureToolCode";
            this.uTextMeasureToolCode.Size = new System.Drawing.Size(110, 21);
            this.uTextMeasureToolCode.TabIndex = 90;
            this.uTextMeasureToolCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMeasureToolCode_EditorButtonClick);
            // 
            // uLabelMeasureToolCode
            // 
            this.uLabelMeasureToolCode.Location = new System.Drawing.Point(12, 60);
            this.uLabelMeasureToolCode.Name = "uLabelMeasureToolCode";
            this.uLabelMeasureToolCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelMeasureToolCode.TabIndex = 146;
            this.uLabelMeasureToolCode.Text = "계측기";
            // 
            // uLabelMaterialCode
            // 
            this.uLabelMaterialCode.Location = new System.Drawing.Point(384, 60);
            this.uLabelMaterialCode.Name = "uLabelMaterialCode";
            this.uLabelMaterialCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelMaterialCode.TabIndex = 148;
            this.uLabelMaterialCode.Text = "자재";
            // 
            // uLabelInspectItemDesc
            // 
            this.uLabelInspectItemDesc.Location = new System.Drawing.Point(12, 84);
            this.uLabelInspectItemDesc.Name = "uLabelInspectItemDesc";
            this.uLabelInspectItemDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectItemDesc.TabIndex = 152;
            this.uLabelInspectItemDesc.Text = "검사항목";
            // 
            // uLabelPlantCode
            // 
            this.uLabelPlantCode.Location = new System.Drawing.Point(384, 12);
            this.uLabelPlantCode.Name = "uLabelPlantCode";
            this.uLabelPlantCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlantCode.TabIndex = 140;
            this.uLabelPlantCode.Text = "5";
            // 
            // uLabelProjectName
            // 
            this.uLabelProjectName.Location = new System.Drawing.Point(12, 36);
            this.uLabelProjectName.Name = "uLabelProjectName";
            this.uLabelProjectName.Size = new System.Drawing.Size(110, 20);
            this.uLabelProjectName.TabIndex = 139;
            this.uLabelProjectName.Text = "4";
            // 
            // uLabelProjectCode
            // 
            this.uLabelProjectCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelProjectCode.Name = "uLabelProjectCode";
            this.uLabelProjectCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelProjectCode.TabIndex = 138;
            this.uLabelProjectCode.Text = "3";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance2;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1060, 740);
            this.uGrid1.TabIndex = 9;
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProjectName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProject);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 8;
            // 
            // uDateSearchInspectDateTo
            // 
            this.uDateSearchInspectDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateTo.Location = new System.Drawing.Point(860, 12);
            this.uDateSearchInspectDateTo.Name = "uDateSearchInspectDateTo";
            this.uDateSearchInspectDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateTo.TabIndex = 128;
            // 
            // uDateSearchInspectDateFrom
            // 
            this.uDateSearchInspectDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchInspectDateFrom.Location = new System.Drawing.Point(744, 12);
            this.uDateSearchInspectDateFrom.Name = "uDateSearchInspectDateFrom";
            this.uDateSearchInspectDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectDateFrom.TabIndex = 128;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchPlant.TabIndex = 127;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uTextSearchProjectName
            // 
            this.uTextSearchProjectName.Location = new System.Drawing.Point(392, 12);
            this.uTextSearchProjectName.Name = "uTextSearchProjectName";
            this.uTextSearchProjectName.Size = new System.Drawing.Size(224, 21);
            this.uTextSearchProjectName.TabIndex = 91;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchProject
            // 
            this.uLabelSearchProject.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchProject.Name = "uLabelSearchProject";
            this.uLabelSearchProject.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchProject.TabIndex = 92;
            this.uLabelSearchProject.Text = "1";
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(628, 12);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchInspectDate.TabIndex = 126;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(844, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 146;
            this.ultraLabel1.Text = "~";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQAT0020
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0020";
            this.Load += new System.EventHandler(this.frmQAT0020_Load);
            this.Activated += new System.EventHandler(this.frmQAT0020_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQAT0020_FormClosing);
            this.Resize += new System.EventHandler(this.frmQAT0020_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRRValueChangeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRRValueUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRRValueChangeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumAccuracyValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumLowerSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumUpperSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSpecRange)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumPartCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMeasureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRepeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            this.uGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRNRDataInputFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckRNRAnalysisFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProjectName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRRValueUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRRValueChangeFlag;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectDesc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolName;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItemDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProjectName;
        private Infragistics.Win.Misc.UltraLabel uLabelProjectCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProjectName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProject;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabelUnitCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSpecRange;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureName;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureCount;
        private Infragistics.Win.Misc.UltraLabel uLabelPartCount;
        private Infragistics.Win.Misc.UltraLabel uLabelRepeatCount;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMeasureCount;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRepeatCount;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStabilityFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAccuracyFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRNRDataInputFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckRNRAnalysisFlag;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumAccuracyValue;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRRValueChangeValue;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumLowerSpec;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumUpperSpec;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumPartCount;
    }
}