﻿namespace QRPQAT.UI
{
    partial class frmQAT0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQAT0002));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckSearchCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchIssueTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchIssueType = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextInspectComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectComment = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectResultFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInspectResultFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonEmailPopUp = new Infragistics.Win.Misc.UltraButton();
            this.uLabelReasonCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckReasonCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextMaterialDisposal = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialDisposal = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uLabel4D8D = new Infragistics.Win.Misc.UltraLabel();
            this.uDateDueDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDueDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextActionDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelClaimCancelFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckClaimCancelFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelActionDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCompleteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReasonDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReasonDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDelete4D8D = new Infragistics.Win.Misc.UltraButton();
            this.uGrid4D8DList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uCheckMaterialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckEnviromentFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMethodFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMachineFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckManFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboFourMOneE = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelFourMOneE = new Infragistics.Win.Misc.UltraLabel();
            this.uComboImputeDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelImputeDept = new Infragistics.Win.Misc.UltraLabel();
            this.uComboImputeProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelImputeProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelComment = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSampleReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSampleReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uComboDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDel_Email = new Infragistics.Win.Misc.UltraButton();
            this.uLabelReceipComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckReceiptComplete = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGridEmail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPakcage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEmail = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProductCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextFaultTypeDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextAttachmentFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachmentFileName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerAnalysisResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerAnalysisResult = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uLabelFaultType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboDetectProcessCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelDetectProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboIssueTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelIssueType = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReceiptUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReceiptUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReceiptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextClaimNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelClaimNo = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchIssueTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectResultFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReasonCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialDisposal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextActionDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckClaimCancelFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReasonDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4D8DList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnviromentFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethodFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachineFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckManFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboFourMOneE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboImputeDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboImputeProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSampleReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceiptComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPakcage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAttachmentFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerAnalysisResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDetectProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboIssueTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextClaimNo)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchComplete);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchIssueTypeCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchIssueType);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToReceiptDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromReceiptDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchReceiptDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(436, 12);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(216, 21);
            this.uComboSearchPackage.TabIndex = 2;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(332, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 186;
            this.uLabelSearchPackage.Text = "ultraLabel1";
            // 
            // uCheckSearchCompleteFlag
            // 
            this.uCheckSearchCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckSearchCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSearchCompleteFlag.Location = new System.Drawing.Point(776, 36);
            this.uCheckSearchCompleteFlag.Name = "uCheckSearchCompleteFlag";
            this.uCheckSearchCompleteFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckSearchCompleteFlag.TabIndex = 6;
            this.uCheckSearchCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelSearchComplete
            // 
            this.uLabelSearchComplete.Location = new System.Drawing.Point(672, 36);
            this.uLabelSearchComplete.Name = "uLabelSearchComplete";
            this.uLabelSearchComplete.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchComplete.TabIndex = 150;
            this.uLabelSearchComplete.Text = "ultraLabel1";
            // 
            // uComboSearchIssueTypeCode
            // 
            this.uComboSearchIssueTypeCode.Location = new System.Drawing.Point(115, 36);
            this.uComboSearchIssueTypeCode.Name = "uComboSearchIssueTypeCode";
            this.uComboSearchIssueTypeCode.Size = new System.Drawing.Size(199, 21);
            this.uComboSearchIssueTypeCode.TabIndex = 3;
            this.uComboSearchIssueTypeCode.Text = "ultraComboEditor1";
            // 
            // uLabelSearchIssueType
            // 
            this.uLabelSearchIssueType.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchIssueType.Name = "uLabelSearchIssueType";
            this.uLabelSearchIssueType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchIssueType.TabIndex = 148;
            this.uLabelSearchIssueType.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(535, 36);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel1.TabIndex = 147;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchToReceiptDate
            // 
            this.uDateSearchToReceiptDate.Location = new System.Drawing.Point(553, 36);
            this.uDateSearchToReceiptDate.Name = "uDateSearchToReceiptDate";
            this.uDateSearchToReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToReceiptDate.TabIndex = 5;
            // 
            // uDateSearchFromReceiptDate
            // 
            this.uDateSearchFromReceiptDate.Location = new System.Drawing.Point(436, 36);
            this.uDateSearchFromReceiptDate.Name = "uDateSearchFromReceiptDate";
            this.uDateSearchFromReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromReceiptDate.TabIndex = 4;
            // 
            // uLabelSearchReceiptDate
            // 
            this.uLabelSearchReceiptDate.Location = new System.Drawing.Point(332, 36);
            this.uLabelSearchReceiptDate.Name = "uLabelSearchReceiptDate";
            this.uLabelSearchReceiptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchReceiptDate.TabIndex = 10;
            this.uLabelSearchReceiptDate.Text = "ultraLabel1";
            // 
            // uTextSearchCustomerName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance3;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(218, 12);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerName.TabIndex = 4;
            // 
            // uTextSearchCustomerCode
            // 
            appearance4.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(115, 12);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerCode.TabIndex = 1;
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 2;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1011, 4);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(52, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(955, 4);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(52, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance5;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 100);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(1070, 720);
            this.uGridHeader.TabIndex = 2;
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 670);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 670);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 650);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uTextInspectComment);
            this.uGroupBox3.Controls.Add(this.uLabelInspectComment);
            this.uGroupBox3.Controls.Add(this.uComboInspectResultFlag);
            this.uGroupBox3.Controls.Add(this.uLabelInspectResultFlag);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox3.Controls.Add(this.uLabelInspectUser);
            this.uGroupBox3.Controls.Add(this.uDateInspectDate);
            this.uGroupBox3.Controls.Add(this.uLabelInspectDate);
            this.uGroupBox3.Controls.Add(this.uLabelComplete);
            this.uGroupBox3.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 547);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1045, 100);
            this.uGroupBox3.TabIndex = 2;
            // 
            // uTextInspectComment
            // 
            this.uTextInspectComment.Location = new System.Drawing.Point(423, 28);
            this.uTextInspectComment.Multiline = true;
            this.uTextInspectComment.Name = "uTextInspectComment";
            this.uTextInspectComment.Size = new System.Drawing.Size(600, 65);
            this.uTextInspectComment.TabIndex = 40;
            // 
            // uLabelInspectComment
            // 
            this.uLabelInspectComment.Location = new System.Drawing.Point(320, 28);
            this.uLabelInspectComment.Name = "uLabelInspectComment";
            this.uLabelInspectComment.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectComment.TabIndex = 196;
            this.uLabelInspectComment.Text = "ultraLabel1";
            // 
            // uComboInspectResultFlag
            // 
            this.uComboInspectResultFlag.Location = new System.Drawing.Point(115, 74);
            this.uComboInspectResultFlag.Name = "uComboInspectResultFlag";
            this.uComboInspectResultFlag.Size = new System.Drawing.Size(124, 21);
            this.uComboInspectResultFlag.TabIndex = 39;
            this.uComboInspectResultFlag.Text = "ultraComboEditor1";
            // 
            // uLabelInspectResultFlag
            // 
            this.uLabelInspectResultFlag.Location = new System.Drawing.Point(12, 74);
            this.uLabelInspectResultFlag.Name = "uLabelInspectResultFlag";
            this.uLabelInspectResultFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectResultFlag.TabIndex = 194;
            this.uLabelInspectResultFlag.Text = "ultraLabel1";
            // 
            // uTextInspectUserName
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance57;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(217, 51);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(94, 21);
            this.uTextInspectUserName.TabIndex = 193;
            // 
            // uTextInspectUserID
            // 
            appearance58.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance58.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance58;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton2);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(115, 51);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 38;
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(12, 51);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectUser.TabIndex = 191;
            this.uLabelInspectUser.Text = "점검자";
            // 
            // uDateInspectDate
            // 
            this.uDateInspectDate.Location = new System.Drawing.Point(115, 28);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDate.TabIndex = 37;
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectDate.TabIndex = 189;
            this.uLabelInspectDate.Text = "ultraLabel1";
            // 
            // uLabelComplete
            // 
            this.uLabelComplete.Location = new System.Drawing.Point(664, 4);
            this.uLabelComplete.Name = "uLabelComplete";
            this.uLabelComplete.Size = new System.Drawing.Size(16, 20);
            this.uLabelComplete.TabIndex = 186;
            this.uLabelComplete.Text = "ultraLabel1";
            this.uLabelComplete.Visible = false;
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(680, 4);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckCompleteFlag.TabIndex = 184;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.Visible = false;
            this.uCheckCompleteFlag.CheckedChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedChanged);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uButtonEmailPopUp);
            this.uGroupBox2.Controls.Add(this.uLabelReasonCompleteFlag);
            this.uGroupBox2.Controls.Add(this.uCheckReasonCompleteFlag);
            this.uGroupBox2.Controls.Add(this.uTextMaterialDisposal);
            this.uGroupBox2.Controls.Add(this.uLabelMaterialDisposal);
            this.uGroupBox2.Controls.Add(this.uButtonFileDown);
            this.uGroupBox2.Controls.Add(this.uLabel4D8D);
            this.uGroupBox2.Controls.Add(this.uDateDueDate);
            this.uGroupBox2.Controls.Add(this.uLabelDueDate);
            this.uGroupBox2.Controls.Add(this.uTextActionDesc);
            this.uGroupBox2.Controls.Add(this.uLabelClaimCancelFlag);
            this.uGroupBox2.Controls.Add(this.uCheckClaimCancelFlag);
            this.uGroupBox2.Controls.Add(this.uDateCompleteDate);
            this.uGroupBox2.Controls.Add(this.uLabelActionDesc);
            this.uGroupBox2.Controls.Add(this.uLabelCompleteDate);
            this.uGroupBox2.Controls.Add(this.uTextReasonDesc);
            this.uGroupBox2.Controls.Add(this.uLabelReasonDesc);
            this.uGroupBox2.Controls.Add(this.uButtonDelete4D8D);
            this.uGroupBox2.Controls.Add(this.uGrid4D8DList);
            this.uGroupBox2.Controls.Add(this.uCheckMaterialFlag);
            this.uGroupBox2.Controls.Add(this.uCheckEnviromentFlag);
            this.uGroupBox2.Controls.Add(this.uCheckMethodFlag);
            this.uGroupBox2.Controls.Add(this.uCheckMachineFlag);
            this.uGroupBox2.Controls.Add(this.uCheckManFlag);
            this.uGroupBox2.Controls.Add(this.uComboFourMOneE);
            this.uGroupBox2.Controls.Add(this.uLabelFourMOneE);
            this.uGroupBox2.Controls.Add(this.uComboImputeDept);
            this.uGroupBox2.Controls.Add(this.uLabelImputeDept);
            this.uGroupBox2.Controls.Add(this.uComboImputeProcess);
            this.uGroupBox2.Controls.Add(this.uLabelImputeProcess);
            this.uGroupBox2.Controls.Add(this.uTextComment);
            this.uGroupBox2.Controls.Add(this.uLabelComment);
            this.uGroupBox2.Controls.Add(this.uDateSampleReceiptDate);
            this.uGroupBox2.Controls.Add(this.uLabelSampleReceiptDate);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 290);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1045, 260);
            this.uGroupBox2.TabIndex = 1;
            // 
            // uButtonEmailPopUp
            // 
            this.uButtonEmailPopUp.Location = new System.Drawing.Point(220, 200);
            this.uButtonEmailPopUp.Name = "uButtonEmailPopUp";
            this.uButtonEmailPopUp.Size = new System.Drawing.Size(115, 28);
            this.uButtonEmailPopUp.TabIndex = 33;
            this.uButtonEmailPopUp.Text = "메일수신자";
            this.uButtonEmailPopUp.Click += new System.EventHandler(this.uButtonEmailPopUp_Click);
            // 
            // uLabelReasonCompleteFlag
            // 
            this.uLabelReasonCompleteFlag.Location = new System.Drawing.Point(524, 236);
            this.uLabelReasonCompleteFlag.Name = "uLabelReasonCompleteFlag";
            this.uLabelReasonCompleteFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelReasonCompleteFlag.TabIndex = 201;
            this.uLabelReasonCompleteFlag.Text = "ultraLabel1";
            // 
            // uCheckReasonCompleteFlag
            // 
            this.uCheckReasonCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckReasonCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReasonCompleteFlag.Location = new System.Drawing.Point(628, 236);
            this.uCheckReasonCompleteFlag.Name = "uCheckReasonCompleteFlag";
            this.uCheckReasonCompleteFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckReasonCompleteFlag.TabIndex = 34;
            this.uCheckReasonCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckReasonCompleteFlag.CheckedChanged += new System.EventHandler(this.uCheckReasonCompleteFlag_CheckedChanged);
            // 
            // uTextMaterialDisposal
            // 
            this.uTextMaterialDisposal.Location = new System.Drawing.Point(628, 172);
            this.uTextMaterialDisposal.Multiline = true;
            this.uTextMaterialDisposal.Name = "uTextMaterialDisposal";
            this.uTextMaterialDisposal.Size = new System.Drawing.Size(400, 57);
            this.uTextMaterialDisposal.TabIndex = 24;
            // 
            // uLabelMaterialDisposal
            // 
            this.uLabelMaterialDisposal.Location = new System.Drawing.Point(524, 172);
            this.uLabelMaterialDisposal.Name = "uLabelMaterialDisposal";
            this.uLabelMaterialDisposal.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialDisposal.TabIndex = 198;
            this.uLabelMaterialDisposal.Text = "ultraLabel1";
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(12, 104);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(100, 28);
            this.uButtonFileDown.TabIndex = 21;
            this.uButtonFileDown.Text = "ultraButton1";
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // uLabel4D8D
            // 
            this.uLabel4D8D.Location = new System.Drawing.Point(12, 52);
            this.uLabel4D8D.Name = "uLabel4D8D";
            this.uLabel4D8D.Size = new System.Drawing.Size(100, 20);
            this.uLabel4D8D.TabIndex = 196;
            this.uLabel4D8D.Text = "ultraLabel1";
            // 
            // uDateDueDate
            // 
            this.uDateDueDate.Location = new System.Drawing.Point(115, 204);
            this.uDateDueDate.Name = "uDateDueDate";
            this.uDateDueDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDueDate.TabIndex = 32;
            // 
            // uLabelDueDate
            // 
            this.uLabelDueDate.Location = new System.Drawing.Point(12, 204);
            this.uLabelDueDate.Name = "uLabelDueDate";
            this.uLabelDueDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDueDate.TabIndex = 194;
            this.uLabelDueDate.Text = "ultraLabel1";
            // 
            // uTextActionDesc
            // 
            this.uTextActionDesc.Location = new System.Drawing.Point(628, 112);
            this.uTextActionDesc.Multiline = true;
            this.uTextActionDesc.Name = "uTextActionDesc";
            this.uTextActionDesc.Size = new System.Drawing.Size(400, 57);
            this.uTextActionDesc.TabIndex = 23;
            // 
            // uLabelClaimCancelFlag
            // 
            this.uLabelClaimCancelFlag.Location = new System.Drawing.Point(912, 236);
            this.uLabelClaimCancelFlag.Name = "uLabelClaimCancelFlag";
            this.uLabelClaimCancelFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelClaimCancelFlag.TabIndex = 188;
            this.uLabelClaimCancelFlag.Text = "ultraLabel1";
            // 
            // uCheckClaimCancelFlag
            // 
            this.uCheckClaimCancelFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckClaimCancelFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckClaimCancelFlag.Location = new System.Drawing.Point(1016, 236);
            this.uCheckClaimCancelFlag.Name = "uCheckClaimCancelFlag";
            this.uCheckClaimCancelFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckClaimCancelFlag.TabIndex = 36;
            this.uCheckClaimCancelFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateCompleteDate
            // 
            this.uDateCompleteDate.Location = new System.Drawing.Point(752, 236);
            this.uDateCompleteDate.Name = "uDateCompleteDate";
            this.uDateCompleteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCompleteDate.TabIndex = 35;
            // 
            // uLabelActionDesc
            // 
            this.uLabelActionDesc.Location = new System.Drawing.Point(524, 112);
            this.uLabelActionDesc.Name = "uLabelActionDesc";
            this.uLabelActionDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelActionDesc.TabIndex = 192;
            this.uLabelActionDesc.Text = "ultraLabel1";
            // 
            // uLabelCompleteDate
            // 
            this.uLabelCompleteDate.Location = new System.Drawing.Point(647, 236);
            this.uLabelCompleteDate.Name = "uLabelCompleteDate";
            this.uLabelCompleteDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteDate.TabIndex = 18;
            this.uLabelCompleteDate.Text = "ultraLabel1";
            // 
            // uTextReasonDesc
            // 
            this.uTextReasonDesc.Location = new System.Drawing.Point(628, 52);
            this.uTextReasonDesc.Multiline = true;
            this.uTextReasonDesc.Name = "uTextReasonDesc";
            this.uTextReasonDesc.Size = new System.Drawing.Size(400, 57);
            this.uTextReasonDesc.TabIndex = 22;
            // 
            // uLabelReasonDesc
            // 
            this.uLabelReasonDesc.Location = new System.Drawing.Point(524, 52);
            this.uLabelReasonDesc.Name = "uLabelReasonDesc";
            this.uLabelReasonDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelReasonDesc.TabIndex = 190;
            this.uLabelReasonDesc.Text = "ultraLabel1";
            // 
            // uButtonDelete4D8D
            // 
            this.uButtonDelete4D8D.Location = new System.Drawing.Point(12, 76);
            this.uButtonDelete4D8D.Name = "uButtonDelete4D8D";
            this.uButtonDelete4D8D.Size = new System.Drawing.Size(100, 28);
            this.uButtonDelete4D8D.TabIndex = 20;
            this.uButtonDelete4D8D.Text = "ultraButton1";
            this.uButtonDelete4D8D.Click += new System.EventHandler(this.uButtonDelete4D8D_Click);
            // 
            // uGrid4D8DList
            // 
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4D8DList.DisplayLayout.Appearance = appearance59;
            this.uGrid4D8DList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4D8DList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4D8DList.DisplayLayout.GroupByBox.Appearance = appearance60;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4D8DList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance61;
            this.uGrid4D8DList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance62.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance62.BackColor2 = System.Drawing.SystemColors.Control;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4D8DList.DisplayLayout.GroupByBox.PromptAppearance = appearance62;
            this.uGrid4D8DList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4D8DList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4D8DList.DisplayLayout.Override.ActiveCellAppearance = appearance63;
            appearance64.BackColor = System.Drawing.SystemColors.Highlight;
            appearance64.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4D8DList.DisplayLayout.Override.ActiveRowAppearance = appearance64;
            this.uGrid4D8DList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4D8DList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4D8DList.DisplayLayout.Override.CardAreaAppearance = appearance65;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            appearance66.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4D8DList.DisplayLayout.Override.CellAppearance = appearance66;
            this.uGrid4D8DList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4D8DList.DisplayLayout.Override.CellPadding = 0;
            appearance67.BackColor = System.Drawing.SystemColors.Control;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4D8DList.DisplayLayout.Override.GroupByRowAppearance = appearance67;
            appearance68.TextHAlignAsString = "Left";
            this.uGrid4D8DList.DisplayLayout.Override.HeaderAppearance = appearance68;
            this.uGrid4D8DList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4D8DList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4D8DList.DisplayLayout.Override.RowAppearance = appearance69;
            this.uGrid4D8DList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance70.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4D8DList.DisplayLayout.Override.TemplateAddRowAppearance = appearance70;
            this.uGrid4D8DList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4D8DList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4D8DList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4D8DList.Location = new System.Drawing.Point(115, 52);
            this.uGrid4D8DList.Name = "uGrid4D8DList";
            this.uGrid4D8DList.Size = new System.Drawing.Size(404, 92);
            this.uGrid4D8DList.TabIndex = 188;
            this.uGrid4D8DList.Text = "ultraGrid1";
            this.uGrid4D8DList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid4D8DList_KeyDown);
            this.uGrid4D8DList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4D8DList_ClickCellButton);
            this.uGrid4D8DList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid4D8DList_DoubleClickCell);
            // 
            // uCheckMaterialFlag
            // 
            this.uCheckMaterialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMaterialFlag.Location = new System.Drawing.Point(268, 180);
            this.uCheckMaterialFlag.Name = "uCheckMaterialFlag";
            this.uCheckMaterialFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMaterialFlag.TabIndex = 29;
            this.uCheckMaterialFlag.Text = "자재";
            this.uCheckMaterialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckEnviromentFlag
            // 
            this.uCheckEnviromentFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckEnviromentFlag.Location = new System.Drawing.Point(428, 180);
            this.uCheckEnviromentFlag.Name = "uCheckEnviromentFlag";
            this.uCheckEnviromentFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckEnviromentFlag.TabIndex = 31;
            this.uCheckEnviromentFlag.Text = "환경";
            this.uCheckEnviromentFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMethodFlag
            // 
            this.uCheckMethodFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMethodFlag.Location = new System.Drawing.Point(348, 180);
            this.uCheckMethodFlag.Name = "uCheckMethodFlag";
            this.uCheckMethodFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMethodFlag.TabIndex = 30;
            this.uCheckMethodFlag.Text = "방법";
            this.uCheckMethodFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMachineFlag
            // 
            this.uCheckMachineFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMachineFlag.Location = new System.Drawing.Point(188, 180);
            this.uCheckMachineFlag.Name = "uCheckMachineFlag";
            this.uCheckMachineFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckMachineFlag.TabIndex = 28;
            this.uCheckMachineFlag.Text = "장비";
            this.uCheckMachineFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckManFlag
            // 
            this.uCheckManFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckManFlag.Location = new System.Drawing.Point(115, 180);
            this.uCheckManFlag.Name = "uCheckManFlag";
            this.uCheckManFlag.Size = new System.Drawing.Size(68, 20);
            this.uCheckManFlag.TabIndex = 27;
            this.uCheckManFlag.Text = "사람";
            this.uCheckManFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uComboFourMOneE
            // 
            this.uComboFourMOneE.Location = new System.Drawing.Point(968, 4);
            this.uComboFourMOneE.Name = "uComboFourMOneE";
            this.uComboFourMOneE.Size = new System.Drawing.Size(59, 21);
            this.uComboFourMOneE.TabIndex = 179;
            this.uComboFourMOneE.Text = "ultraComboEditor1";
            this.uComboFourMOneE.Visible = false;
            // 
            // uLabelFourMOneE
            // 
            this.uLabelFourMOneE.Location = new System.Drawing.Point(12, 180);
            this.uLabelFourMOneE.Name = "uLabelFourMOneE";
            this.uLabelFourMOneE.Size = new System.Drawing.Size(100, 20);
            this.uLabelFourMOneE.TabIndex = 178;
            this.uLabelFourMOneE.Text = "ultraLabel1";
            // 
            // uComboImputeDept
            // 
            this.uComboImputeDept.Location = new System.Drawing.Point(348, 156);
            this.uComboImputeDept.Name = "uComboImputeDept";
            this.uComboImputeDept.Size = new System.Drawing.Size(168, 21);
            this.uComboImputeDept.TabIndex = 26;
            this.uComboImputeDept.Text = "ultraComboEditor1";
            // 
            // uLabelImputeDept
            // 
            this.uLabelImputeDept.Location = new System.Drawing.Point(244, 156);
            this.uLabelImputeDept.Name = "uLabelImputeDept";
            this.uLabelImputeDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelImputeDept.TabIndex = 176;
            this.uLabelImputeDept.Text = "ultraLabel1";
            // 
            // uComboImputeProcess
            // 
            this.uComboImputeProcess.Location = new System.Drawing.Point(115, 156);
            this.uComboImputeProcess.Name = "uComboImputeProcess";
            this.uComboImputeProcess.Size = new System.Drawing.Size(124, 21);
            this.uComboImputeProcess.TabIndex = 25;
            this.uComboImputeProcess.Text = "ultraComboEditor1";
            // 
            // uLabelImputeProcess
            // 
            this.uLabelImputeProcess.Location = new System.Drawing.Point(12, 156);
            this.uLabelImputeProcess.Name = "uLabelImputeProcess";
            this.uLabelImputeProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelImputeProcess.TabIndex = 174;
            this.uLabelImputeProcess.Text = "ultraLabel1";
            // 
            // uTextComment
            // 
            this.uTextComment.Location = new System.Drawing.Point(360, 28);
            this.uTextComment.Name = "uTextComment";
            this.uTextComment.Size = new System.Drawing.Size(668, 21);
            this.uTextComment.TabIndex = 19;
            // 
            // uLabelComment
            // 
            this.uLabelComment.Location = new System.Drawing.Point(255, 28);
            this.uLabelComment.Name = "uLabelComment";
            this.uLabelComment.Size = new System.Drawing.Size(100, 20);
            this.uLabelComment.TabIndex = 156;
            this.uLabelComment.Text = "ultraLabel1";
            // 
            // uDateSampleReceiptDate
            // 
            this.uDateSampleReceiptDate.Location = new System.Drawing.Point(115, 28);
            this.uDateSampleReceiptDate.Name = "uDateSampleReceiptDate";
            this.uDateSampleReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSampleReceiptDate.TabIndex = 18;
            // 
            // uLabelSampleReceiptDate
            // 
            this.uLabelSampleReceiptDate.Location = new System.Drawing.Point(12, 28);
            this.uLabelSampleReceiptDate.Name = "uLabelSampleReceiptDate";
            this.uLabelSampleReceiptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSampleReceiptDate.TabIndex = 18;
            this.uLabelSampleReceiptDate.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uButtonOK);
            this.uGroupBox1.Controls.Add(this.uComboDept);
            this.uGroupBox1.Controls.Add(this.uButtonDel_Email);
            this.uGroupBox1.Controls.Add(this.uLabelReceipComplete);
            this.uGroupBox1.Controls.Add(this.uCheckReceiptComplete);
            this.uGroupBox1.Controls.Add(this.uGridEmail);
            this.uGroupBox1.Controls.Add(this.uComboCustomerProductSpec);
            this.uGroupBox1.Controls.Add(this.uComboPackage);
            this.uGroupBox1.Controls.Add(this.uComboCustomer);
            this.uGroupBox1.Controls.Add(this.uTextCustomerCode);
            this.uGroupBox1.Controls.Add(this.uTextPakcage);
            this.uGroupBox1.Controls.Add(this.uLabelProduct);
            this.uGroupBox1.Controls.Add(this.uTextCustomerProductSpec);
            this.uGroupBox1.Controls.Add(this.uLabelEmail);
            this.uGroupBox1.Controls.Add(this.uLabelCustomerProductSpec);
            this.uGroupBox1.Controls.Add(this.uComboProductCode);
            this.uGroupBox1.Controls.Add(this.uTextFaultTypeDesc);
            this.uGroupBox1.Controls.Add(this.uLabelPackage);
            this.uGroupBox1.Controls.Add(this.uComboFaultTypeCode);
            this.uGroupBox1.Controls.Add(this.uTextAttachmentFileName);
            this.uGroupBox1.Controls.Add(this.uLabelAttachmentFileName);
            this.uGroupBox1.Controls.Add(this.uTextCustomerAnalysisResult);
            this.uGroupBox1.Controls.Add(this.uLabelCustomerAnalysisResult);
            this.uGroupBox1.Controls.Add(this.uGridDetail);
            this.uGroupBox1.Controls.Add(this.uButtonDelete);
            this.uGroupBox1.Controls.Add(this.uLabelFaultType);
            this.uGroupBox1.Controls.Add(this.uComboDetectProcessCode);
            this.uGroupBox1.Controls.Add(this.uLabelDetectProcess);
            this.uGroupBox1.Controls.Add(this.uComboIssueTypeCode);
            this.uGroupBox1.Controls.Add(this.uLabelIssueType);
            this.uGroupBox1.Controls.Add(this.uDateReceiptDate);
            this.uGroupBox1.Controls.Add(this.uLabelReceiptDate);
            this.uGroupBox1.Controls.Add(this.uTextCustomerName);
            this.uGroupBox1.Controls.Add(this.uLabelCustomer);
            this.uGroupBox1.Controls.Add(this.uTextReceiptUserName);
            this.uGroupBox1.Controls.Add(this.uTextReceiptUserID);
            this.uGroupBox1.Controls.Add(this.uLabelReceiptUser);
            this.uGroupBox1.Controls.Add(this.uComboPlantCode);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uTextClaimNo);
            this.uGroupBox1.Controls.Add(this.uLabelClaimNo);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 10);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1045, 280);
            this.uGroupBox1.TabIndex = 0;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(964, 100);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(68, 28);
            this.uButtonOK.TabIndex = 201;
            this.uButtonOK.Text = "확인";
            // 
            // uComboDept
            // 
            this.uComboDept.Location = new System.Drawing.Point(868, 104);
            this.uComboDept.Name = "uComboDept";
            this.uComboDept.Size = new System.Drawing.Size(92, 21);
            this.uComboDept.TabIndex = 200;
            // 
            // uButtonDel_Email
            // 
            this.uButtonDel_Email.Location = new System.Drawing.Point(749, 100);
            this.uButtonDel_Email.Name = "uButtonDel_Email";
            this.uButtonDel_Email.Size = new System.Drawing.Size(87, 28);
            this.uButtonDel_Email.TabIndex = 88;
            this.uButtonDel_Email.Text = "ultraButton1";
            this.uButtonDel_Email.Click += new System.EventHandler(this.uButtonDel_Email_Click);
            // 
            // uLabelReceipComplete
            // 
            this.uLabelReceipComplete.Location = new System.Drawing.Point(560, 252);
            this.uLabelReceipComplete.Name = "uLabelReceipComplete";
            this.uLabelReceipComplete.Size = new System.Drawing.Size(156, 20);
            this.uLabelReceipComplete.TabIndex = 198;
            this.uLabelReceipComplete.Text = "ultraLabel1";
            // 
            // uCheckReceiptComplete
            // 
            this.uCheckReceiptComplete.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckReceiptComplete.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReceiptComplete.Location = new System.Drawing.Point(724, 252);
            this.uCheckReceiptComplete.Name = "uCheckReceiptComplete";
            this.uCheckReceiptComplete.Size = new System.Drawing.Size(16, 20);
            this.uCheckReceiptComplete.TabIndex = 17;
            this.uCheckReceiptComplete.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uGridEmail
            // 
            this.uGridEmail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEmail.DisplayLayout.Appearance = appearance17;
            this.uGridEmail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEmail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGridEmail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridEmail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEmail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEmail.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEmail.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGridEmail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEmail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEmail.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGridEmail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEmail.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGridEmail.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridEmail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEmail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGridEmail.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGridEmail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEmail.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGridEmail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEmail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEmail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEmail.Location = new System.Drawing.Point(645, 116);
            this.uGridEmail.Name = "uGridEmail";
            this.uGridEmail.Size = new System.Drawing.Size(391, 96);
            this.uGridEmail.TabIndex = 0;
            this.uGridEmail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_AfterCellUpdate);
            this.uGridEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEmail_KeyDown);
            this.uGridEmail.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_ClickCellButton);
            // 
            // uComboCustomerProductSpec
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomerProductSpec.Appearance = appearance29;
            this.uComboCustomerProductSpec.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomerProductSpec.Location = new System.Drawing.Point(703, 76);
            this.uComboCustomerProductSpec.Name = "uComboCustomerProductSpec";
            this.uComboCustomerProductSpec.Size = new System.Drawing.Size(240, 21);
            this.uComboCustomerProductSpec.TabIndex = 14;
            this.uComboCustomerProductSpec.Text = "ultraComboEditor1";
            this.uComboCustomerProductSpec.ValueChanged += new System.EventHandler(this.uComboCustomerProductSpec_ValueChanged);
            // 
            // uComboPackage
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPackage.Appearance = appearance30;
            this.uComboPackage.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPackage.Location = new System.Drawing.Point(703, 52);
            this.uComboPackage.Name = "uComboPackage";
            this.uComboPackage.Size = new System.Drawing.Size(240, 21);
            this.uComboPackage.TabIndex = 13;
            this.uComboPackage.Text = "ultraComboEditor1";
            this.uComboPackage.ValueChanged += new System.EventHandler(this.uComboPackage_ValueChanged);
            // 
            // uComboCustomer
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomer.Appearance = appearance31;
            this.uComboCustomer.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomer.Location = new System.Drawing.Point(703, 28);
            this.uComboCustomer.Name = "uComboCustomer";
            this.uComboCustomer.Size = new System.Drawing.Size(240, 21);
            this.uComboCustomer.TabIndex = 12;
            this.uComboCustomer.Text = "ultraComboEditor1";
            this.uComboCustomer.ValueChanged += new System.EventHandler(this.uComboCustomer_ValueChanged);
            // 
            // uTextCustomerCode
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance32;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(917, 104);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerCode.TabIndex = 193;
            this.uTextCustomerCode.Visible = false;
            // 
            // uTextPakcage
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPakcage.Appearance = appearance33;
            this.uTextPakcage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPakcage.Location = new System.Drawing.Point(965, 104);
            this.uTextPakcage.Name = "uTextPakcage";
            this.uTextPakcage.ReadOnly = true;
            this.uTextPakcage.Size = new System.Drawing.Size(21, 21);
            this.uTextPakcage.TabIndex = 192;
            this.uTextPakcage.Visible = false;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(588, 4);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(112, 20);
            this.uLabelProduct.TabIndex = 191;
            this.uLabelProduct.Text = "제품코드";
            // 
            // uTextCustomerProductSpec
            // 
            appearance34.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerProductSpec.Appearance = appearance34;
            this.uTextCustomerProductSpec.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerProductSpec.Location = new System.Drawing.Point(893, 104);
            this.uTextCustomerProductSpec.Name = "uTextCustomerProductSpec";
            this.uTextCustomerProductSpec.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerProductSpec.TabIndex = 190;
            this.uTextCustomerProductSpec.Visible = false;
            this.uTextCustomerProductSpec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCustomerProductSpec_KeyDown);
            // 
            // uLabelEmail
            // 
            this.uLabelEmail.Location = new System.Drawing.Point(645, 104);
            this.uLabelEmail.Name = "uLabelEmail";
            this.uLabelEmail.Size = new System.Drawing.Size(100, 20);
            this.uLabelEmail.TabIndex = 189;
            this.uLabelEmail.Text = "이메일리스트";
            // 
            // uLabelCustomerProductSpec
            // 
            this.uLabelCustomerProductSpec.Location = new System.Drawing.Point(588, 76);
            this.uLabelCustomerProductSpec.Name = "uLabelCustomerProductSpec";
            this.uLabelCustomerProductSpec.Size = new System.Drawing.Size(112, 20);
            this.uLabelCustomerProductSpec.TabIndex = 189;
            this.uLabelCustomerProductSpec.Text = "고객사제품코드";
            // 
            // uComboProductCode
            // 
            appearance35.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProductCode.Appearance = appearance35;
            this.uComboProductCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProductCode.Location = new System.Drawing.Point(703, 4);
            this.uComboProductCode.Name = "uComboProductCode";
            this.uComboProductCode.Size = new System.Drawing.Size(240, 21);
            this.uComboProductCode.TabIndex = 11;
            this.uComboProductCode.Text = "ultraComboEditor1";
            this.uComboProductCode.ValueChanged += new System.EventHandler(this.uComboProductCode_ValueChanged);
            // 
            // uTextFaultTypeDesc
            // 
            this.uTextFaultTypeDesc.Location = new System.Drawing.Point(115, 76);
            this.uTextFaultTypeDesc.Name = "uTextFaultTypeDesc";
            this.uTextFaultTypeDesc.Size = new System.Drawing.Size(464, 21);
            this.uTextFaultTypeDesc.TabIndex = 169;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(588, 52);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(112, 20);
            this.uLabelPackage.TabIndex = 166;
            this.uLabelPackage.Text = "Package";
            // 
            // uComboFaultTypeCode
            // 
            this.uComboFaultTypeCode.Location = new System.Drawing.Point(964, 80);
            this.uComboFaultTypeCode.Name = "uComboFaultTypeCode";
            this.uComboFaultTypeCode.Size = new System.Drawing.Size(72, 21);
            this.uComboFaultTypeCode.TabIndex = 165;
            this.uComboFaultTypeCode.Text = "ultraComboEditor1";
            this.uComboFaultTypeCode.Visible = false;
            // 
            // uTextAttachmentFileName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAttachmentFileName.Appearance = appearance36;
            this.uTextAttachmentFileName.BackColor = System.Drawing.Color.Gainsboro;
            appearance37.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance37;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Key = "UP";
            appearance38.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance38.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance38;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "DOWN";
            this.uTextAttachmentFileName.ButtonsRight.Add(editorButton3);
            this.uTextAttachmentFileName.ButtonsRight.Add(editorButton4);
            this.uTextAttachmentFileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAttachmentFileName.Location = new System.Drawing.Point(724, 228);
            this.uTextAttachmentFileName.Name = "uTextAttachmentFileName";
            this.uTextAttachmentFileName.ReadOnly = true;
            this.uTextAttachmentFileName.Size = new System.Drawing.Size(300, 21);
            this.uTextAttachmentFileName.TabIndex = 16;
            this.uTextAttachmentFileName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAttachmentFileName_KeyDown);
            this.uTextAttachmentFileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAttachmentFileName_EditorButtonClick);
            // 
            // uLabelAttachmentFileName
            // 
            this.uLabelAttachmentFileName.Location = new System.Drawing.Point(560, 228);
            this.uLabelAttachmentFileName.Name = "uLabelAttachmentFileName";
            this.uLabelAttachmentFileName.Size = new System.Drawing.Size(156, 20);
            this.uLabelAttachmentFileName.TabIndex = 162;
            this.uLabelAttachmentFileName.Text = "ultraLabel1";
            // 
            // uTextCustomerAnalysisResult
            // 
            this.uTextCustomerAnalysisResult.Location = new System.Drawing.Point(199, 228);
            this.uTextCustomerAnalysisResult.Multiline = true;
            this.uTextCustomerAnalysisResult.Name = "uTextCustomerAnalysisResult";
            this.uTextCustomerAnalysisResult.Size = new System.Drawing.Size(350, 48);
            this.uTextCustomerAnalysisResult.TabIndex = 15;
            // 
            // uLabelCustomerAnalysisResult
            // 
            this.uLabelCustomerAnalysisResult.Location = new System.Drawing.Point(12, 228);
            this.uLabelCustomerAnalysisResult.Name = "uLabelCustomerAnalysisResult";
            this.uLabelCustomerAnalysisResult.Size = new System.Drawing.Size(180, 20);
            this.uLabelCustomerAnalysisResult.TabIndex = 160;
            this.uLabelCustomerAnalysisResult.Text = "Customer Anlysis Result";
            // 
            // uGridDetail
            // 
            this.uGridDetail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetail.DisplayLayout.Appearance = appearance39;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.GroupByBox.Appearance = appearance40;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.uGridDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.uGridDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetail.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetail.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.uGridDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetail.DisplayLayout.Override.CellAppearance = appearance46;
            this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetail.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance48.TextHAlignAsString = "Left";
            this.uGridDetail.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.uGridDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetail.DisplayLayout.Override.RowAppearance = appearance49;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.uGridDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetail.Location = new System.Drawing.Point(12, 116);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(628, 96);
            this.uGridDetail.TabIndex = 159;
            this.uGridDetail.Text = "ultraGrid1";
            this.uGridDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDetail_AfterCellUpdate);
            this.uGridDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDetail_CellChange);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 100);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(87, 28);
            this.uButtonDelete.TabIndex = 158;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uLabelFaultType
            // 
            this.uLabelFaultType.Location = new System.Drawing.Point(12, 76);
            this.uLabelFaultType.Name = "uLabelFaultType";
            this.uLabelFaultType.Size = new System.Drawing.Size(100, 20);
            this.uLabelFaultType.TabIndex = 154;
            this.uLabelFaultType.Text = "불량명";
            // 
            // uComboDetectProcessCode
            // 
            this.uComboDetectProcessCode.Location = new System.Drawing.Point(992, 36);
            this.uComboDetectProcessCode.Name = "uComboDetectProcessCode";
            this.uComboDetectProcessCode.Size = new System.Drawing.Size(44, 21);
            this.uComboDetectProcessCode.TabIndex = 153;
            this.uComboDetectProcessCode.Text = "ultraComboEditor1";
            this.uComboDetectProcessCode.Visible = false;
            // 
            // uLabelDetectProcess
            // 
            this.uLabelDetectProcess.Location = new System.Drawing.Point(964, 36);
            this.uLabelDetectProcess.Name = "uLabelDetectProcess";
            this.uLabelDetectProcess.Size = new System.Drawing.Size(24, 20);
            this.uLabelDetectProcess.TabIndex = 152;
            this.uLabelDetectProcess.Text = "ultraLabel1";
            this.uLabelDetectProcess.Visible = false;
            // 
            // uComboIssueTypeCode
            // 
            appearance51.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboIssueTypeCode.Appearance = appearance51;
            this.uComboIssueTypeCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboIssueTypeCode.Location = new System.Drawing.Point(384, 28);
            this.uComboIssueTypeCode.Name = "uComboIssueTypeCode";
            this.uComboIssueTypeCode.Size = new System.Drawing.Size(196, 21);
            this.uComboIssueTypeCode.TabIndex = 8;
            // 
            // uLabelIssueType
            // 
            this.uLabelIssueType.Location = new System.Drawing.Point(280, 28);
            this.uLabelIssueType.Name = "uLabelIssueType";
            this.uLabelIssueType.Size = new System.Drawing.Size(100, 20);
            this.uLabelIssueType.TabIndex = 150;
            this.uLabelIssueType.Text = "발행구분";
            // 
            // uDateReceiptDate
            // 
            this.uDateReceiptDate.Location = new System.Drawing.Point(115, 52);
            this.uDateReceiptDate.Name = "uDateReceiptDate";
            this.uDateReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReceiptDate.TabIndex = 9;
            // 
            // uLabelReceiptDate
            // 
            this.uLabelReceiptDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelReceiptDate.Name = "uLabelReceiptDate";
            this.uLabelReceiptDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReceiptDate.TabIndex = 16;
            this.uLabelReceiptDate.Text = "접수일";
            // 
            // uTextCustomerName
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance52;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(941, 104);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerName.TabIndex = 15;
            this.uTextCustomerName.Visible = false;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(588, 28);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(112, 20);
            this.uLabelCustomer.TabIndex = 13;
            this.uLabelCustomer.Text = "고객";
            // 
            // uTextReceiptUserName
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptUserName.Appearance = appearance53;
            this.uTextReceiptUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptUserName.Location = new System.Drawing.Point(485, 52);
            this.uTextReceiptUserName.Name = "uTextReceiptUserName";
            this.uTextReceiptUserName.ReadOnly = true;
            this.uTextReceiptUserName.Size = new System.Drawing.Size(94, 21);
            this.uTextReceiptUserName.TabIndex = 12;
            // 
            // uTextReceiptUserID
            // 
            appearance54.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReceiptUserID.Appearance = appearance54;
            this.uTextReceiptUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance55.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance55.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance55;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReceiptUserID.ButtonsRight.Add(editorButton5);
            this.uTextReceiptUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReceiptUserID.Location = new System.Drawing.Point(384, 52);
            this.uTextReceiptUserID.Name = "uTextReceiptUserID";
            this.uTextReceiptUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptUserID.TabIndex = 10;
            this.uTextReceiptUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReceiptUserID_KeyDown);
            this.uTextReceiptUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReceiptUserID_EditorButtonClick);
            // 
            // uLabelReceiptUser
            // 
            this.uLabelReceiptUser.Location = new System.Drawing.Point(280, 52);
            this.uLabelReceiptUser.Name = "uLabelReceiptUser";
            this.uLabelReceiptUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelReceiptUser.TabIndex = 10;
            this.uLabelReceiptUser.Text = "접수자ID";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(1000, 8);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(36, 21);
            this.uComboPlantCode.TabIndex = 9;
            this.uComboPlantCode.Text = "ultraComboEditor1";
            this.uComboPlantCode.Visible = false;
            this.uComboPlantCode.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(976, 8);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(24, 20);
            this.uLabelPlant.TabIndex = 8;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uTextClaimNo
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextClaimNo.Appearance = appearance56;
            this.uTextClaimNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextClaimNo.Location = new System.Drawing.Point(115, 28);
            this.uTextClaimNo.Name = "uTextClaimNo";
            this.uTextClaimNo.ReadOnly = true;
            this.uTextClaimNo.Size = new System.Drawing.Size(148, 21);
            this.uTextClaimNo.TabIndex = 7;
            // 
            // uLabelClaimNo
            // 
            this.uLabelClaimNo.Location = new System.Drawing.Point(12, 28);
            this.uLabelClaimNo.Name = "uLabelClaimNo";
            this.uLabelClaimNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelClaimNo.TabIndex = 6;
            this.uLabelClaimNo.Text = "발행번호";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQAT0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0002";
            this.Load += new System.EventHandler(this.frmQAT0002_Load);
            this.Activated += new System.EventHandler(this.frmQAT0002_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQAT0002_FormClosing);
            this.Resize += new System.EventHandler(this.frmQAT0002_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchIssueTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectResultFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReasonCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialDisposal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextActionDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckClaimCancelFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReasonDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4D8DList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnviromentFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethodFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachineFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckManFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboFourMOneE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboImputeDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboImputeProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSampleReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceiptComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPakcage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultTypeDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAttachmentFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerAnalysisResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDetectProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboIssueTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextClaimNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchComplete;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchIssueTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchIssueType;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextClaimNo;
        private Infragistics.Win.Misc.UltraLabel uLabelClaimNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboIssueTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelIssueType;
        private Infragistics.Win.Misc.UltraLabel uLabelFaultType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDetectProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDetectProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAttachmentFileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachmentFileName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerAnalysisResult;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerAnalysisResult;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSampleReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSampleReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextComment;
        private Infragistics.Win.Misc.UltraLabel uLabelComment;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboFourMOneE;
        private Infragistics.Win.Misc.UltraLabel uLabelFourMOneE;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboImputeDept;
        private Infragistics.Win.Misc.UltraLabel uLabelImputeDept;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboImputeProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelImputeProcess;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCompleteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboFaultTypeCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultTypeDesc;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckClaimCancelFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProductCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMaterialFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEnviromentFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMethodFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMachineFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckManFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelClaimCancelFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPakcage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomerProductSpec;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4D8DList;
        private Infragistics.Win.Misc.UltraButton uButtonDelete4D8D;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextActionDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelActionDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReasonDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelReasonDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDueDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDueDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectComment;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectComment;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectResultFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectResultFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabel4D8D;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.Misc.UltraLabel uLabelReceipComplete;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReceiptComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialDisposal;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialDisposal;
        private Infragistics.Win.Misc.UltraLabel uLabelReasonCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReasonCompleteFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEmail;
        private Infragistics.Win.Misc.UltraButton uButtonDel_Email;
        private Infragistics.Win.Misc.UltraLabel uLabelEmail;
        private Infragistics.Win.Misc.UltraButton uButtonEmailPopUp;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDept;
    }
}