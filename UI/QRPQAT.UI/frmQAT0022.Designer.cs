﻿namespace QRPQAT.UI
{
    partial class frmQAT0022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQAT0022));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonReCalc = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRNRPercent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAppraiserPercent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipmentPercent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRNRValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAppraiserVariation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipmentVariation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteCalc = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateRnRAnalysisDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateInspectDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateInspectDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uCheckAccuracyFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckStabilityFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelUnitCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUnitCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLowerSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUpperSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDeviceUnit = new Infragistics.Win.Misc.UltraLabel();
            this.uTextHiddenPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAccuracyValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTolerance = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPartCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRRValue = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRepeatCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProjectCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRNRPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraiserPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipmentPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRNRValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraiserVariation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipmentVariation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRnRAnalysisDate)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).BeginInit();
            this.uGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHiddenPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).BeginInit();
            this.uTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAccuracyValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPartCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRRValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepeatCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonReCalc);
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox2);
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox1);
            this.ultraTabPageControl1.Controls.Add(this.uCheckCompleteCalc);
            this.ultraTabPageControl1.Controls.Add(this.uDateRnRAnalysisDate);
            this.ultraTabPageControl1.Controls.Add(this.uLabel20);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1036, 478);
            // 
            // uButtonReCalc
            // 
            this.uButtonReCalc.Location = new System.Drawing.Point(12, 4);
            this.uButtonReCalc.Name = "uButtonReCalc";
            this.uButtonReCalc.Size = new System.Drawing.Size(98, 28);
            this.uButtonReCalc.TabIndex = 159;
            this.uButtonReCalc.Text = "재분석";
            this.uButtonReCalc.Click += new System.EventHandler(this.uButtonReCalc_Click);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 150);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1012, 324);
            this.uGroupBox2.TabIndex = 3;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance24;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance32;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance25;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance31.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 12);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(988, 304);
            this.uGrid2.TabIndex = 1;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextRNRPercent);
            this.uGroupBox1.Controls.Add(this.uTextAppraiserPercent);
            this.uGroupBox1.Controls.Add(this.uTextEquipmentPercent);
            this.uGroupBox1.Controls.Add(this.uTextRNRValue);
            this.uGroupBox1.Controls.Add(this.uTextAppraiserVariation);
            this.uGroupBox1.Controls.Add(this.uTextEquipmentVariation);
            this.uGroupBox1.Controls.Add(this.uLabel29);
            this.uGroupBox1.Controls.Add(this.uLabel26);
            this.uGroupBox1.Controls.Add(this.uLabel23);
            this.uGroupBox1.Controls.Add(this.uLabel28);
            this.uGroupBox1.Controls.Add(this.uLabel25);
            this.uGroupBox1.Controls.Add(this.uLabel22);
            this.uGroupBox1.Controls.Add(this.uLabel27);
            this.uGroupBox1.Controls.Add(this.uLabel24);
            this.uGroupBox1.Controls.Add(this.uLabel21);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 34);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1012, 108);
            this.uGroupBox1.TabIndex = 3;
            // 
            // uTextRNRPercent
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRNRPercent.Appearance = appearance52;
            this.uTextRNRPercent.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRNRPercent.Location = new System.Drawing.Point(680, 76);
            this.uTextRNRPercent.Name = "uTextRNRPercent";
            this.uTextRNRPercent.ReadOnly = true;
            this.uTextRNRPercent.Size = new System.Drawing.Size(108, 21);
            this.uTextRNRPercent.TabIndex = 1;
            // 
            // uTextAppraiserPercent
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAppraiserPercent.Appearance = appearance49;
            this.uTextAppraiserPercent.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAppraiserPercent.Location = new System.Drawing.Point(404, 76);
            this.uTextAppraiserPercent.Name = "uTextAppraiserPercent";
            this.uTextAppraiserPercent.ReadOnly = true;
            this.uTextAppraiserPercent.Size = new System.Drawing.Size(108, 21);
            this.uTextAppraiserPercent.TabIndex = 1;
            // 
            // uTextEquipmentPercent
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipmentPercent.Appearance = appearance48;
            this.uTextEquipmentPercent.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipmentPercent.Location = new System.Drawing.Point(124, 76);
            this.uTextEquipmentPercent.Name = "uTextEquipmentPercent";
            this.uTextEquipmentPercent.ReadOnly = true;
            this.uTextEquipmentPercent.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipmentPercent.TabIndex = 1;
            // 
            // uTextRNRValue
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRNRValue.Appearance = appearance51;
            this.uTextRNRValue.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRNRValue.Location = new System.Drawing.Point(680, 52);
            this.uTextRNRValue.Name = "uTextRNRValue";
            this.uTextRNRValue.ReadOnly = true;
            this.uTextRNRValue.Size = new System.Drawing.Size(108, 21);
            this.uTextRNRValue.TabIndex = 1;
            // 
            // uTextAppraiserVariation
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAppraiserVariation.Appearance = appearance50;
            this.uTextAppraiserVariation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAppraiserVariation.Location = new System.Drawing.Point(404, 52);
            this.uTextAppraiserVariation.Name = "uTextAppraiserVariation";
            this.uTextAppraiserVariation.ReadOnly = true;
            this.uTextAppraiserVariation.Size = new System.Drawing.Size(108, 21);
            this.uTextAppraiserVariation.TabIndex = 1;
            // 
            // uTextEquipmentVariation
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipmentVariation.Appearance = appearance47;
            this.uTextEquipmentVariation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipmentVariation.Location = new System.Drawing.Point(124, 52);
            this.uTextEquipmentVariation.Name = "uTextEquipmentVariation";
            this.uTextEquipmentVariation.ReadOnly = true;
            this.uTextEquipmentVariation.Size = new System.Drawing.Size(108, 21);
            this.uTextEquipmentVariation.TabIndex = 1;
            // 
            // uLabel29
            // 
            this.uLabel29.Location = new System.Drawing.Point(568, 76);
            this.uLabel29.Name = "uLabel29";
            this.uLabel29.Size = new System.Drawing.Size(108, 20);
            this.uLabel29.TabIndex = 0;
            this.uLabel29.Text = "ultraLabel4";
            // 
            // uLabel26
            // 
            this.uLabel26.Location = new System.Drawing.Point(292, 76);
            this.uLabel26.Name = "uLabel26";
            this.uLabel26.Size = new System.Drawing.Size(108, 20);
            this.uLabel26.TabIndex = 0;
            this.uLabel26.Text = "ultraLabel4";
            // 
            // uLabel23
            // 
            this.uLabel23.Location = new System.Drawing.Point(12, 76);
            this.uLabel23.Name = "uLabel23";
            this.uLabel23.Size = new System.Drawing.Size(108, 20);
            this.uLabel23.TabIndex = 0;
            this.uLabel23.Text = "ultraLabel4";
            // 
            // uLabel28
            // 
            this.uLabel28.Location = new System.Drawing.Point(568, 52);
            this.uLabel28.Name = "uLabel28";
            this.uLabel28.Size = new System.Drawing.Size(108, 20);
            this.uLabel28.TabIndex = 0;
            this.uLabel28.Text = "ultraLabel4";
            // 
            // uLabel25
            // 
            this.uLabel25.Location = new System.Drawing.Point(292, 52);
            this.uLabel25.Name = "uLabel25";
            this.uLabel25.Size = new System.Drawing.Size(108, 20);
            this.uLabel25.TabIndex = 0;
            this.uLabel25.Text = "ultraLabel4";
            // 
            // uLabel22
            // 
            this.uLabel22.Location = new System.Drawing.Point(12, 52);
            this.uLabel22.Name = "uLabel22";
            this.uLabel22.Size = new System.Drawing.Size(108, 20);
            this.uLabel22.TabIndex = 0;
            this.uLabel22.Text = "ultraLabel4";
            // 
            // uLabel27
            // 
            this.uLabel27.Location = new System.Drawing.Point(568, 28);
            this.uLabel27.Name = "uLabel27";
            this.uLabel27.Size = new System.Drawing.Size(240, 20);
            this.uLabel27.TabIndex = 0;
            this.uLabel27.Text = "ultraLabel4";
            // 
            // uLabel24
            // 
            this.uLabel24.Location = new System.Drawing.Point(292, 28);
            this.uLabel24.Name = "uLabel24";
            this.uLabel24.Size = new System.Drawing.Size(216, 20);
            this.uLabel24.TabIndex = 0;
            this.uLabel24.Text = "ultraLabel4";
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(12, 28);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(216, 20);
            this.uLabel21.TabIndex = 0;
            this.uLabel21.Text = "ultraLabel4";
            // 
            // uCheckCompleteCalc
            // 
            this.uCheckCompleteCalc.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteCalc.Location = new System.Drawing.Point(128, 12);
            this.uCheckCompleteCalc.Name = "uCheckCompleteCalc";
            this.uCheckCompleteCalc.Size = new System.Drawing.Size(168, 20);
            this.uCheckCompleteCalc.TabIndex = 2;
            this.uCheckCompleteCalc.Text = "R&R결과 분석완료";
            this.uCheckCompleteCalc.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateRnRAnalysisDate
            // 
            this.uDateRnRAnalysisDate.Location = new System.Drawing.Point(408, 12);
            this.uDateRnRAnalysisDate.Name = "uDateRnRAnalysisDate";
            this.uDateRnRAnalysisDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRnRAnalysisDate.TabIndex = 1;
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(304, 12);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(100, 20);
            this.uLabel20.TabIndex = 0;
            this.uLabel20.Text = "완료일";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGroupBox5);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1036, 482);
            // 
            // uGroupBox5
            // 
            this.uGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox5.Controls.Add(this.uGrid3);
            this.uGroupBox5.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox5.Name = "uGroupBox5";
            this.uGroupBox5.Size = new System.Drawing.Size(1012, 456);
            this.uGroupBox5.TabIndex = 0;
            // 
            // uGrid3
            // 
            this.uGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance36;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance33;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance34;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance35.BackColor2 = System.Drawing.SystemColors.Control;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance35;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance45;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance38;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            appearance37.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance37;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance43.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance42;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(16, 32);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(984, 404);
            this.uGrid3.TabIndex = 191;
            this.uGrid3.Text = "ultraGrid1";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateInspectDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateInspectDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateInspectDateTo
            // 
            this.uDateInspectDateTo.Location = new System.Drawing.Point(904, 12);
            this.uDateInspectDateTo.Name = "uDateInspectDateTo";
            this.uDateInspectDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDateTo.TabIndex = 154;
            // 
            // uDateInspectDateFrom
            // 
            this.uDateInspectDateFrom.Location = new System.Drawing.Point(788, 12);
            this.uDateInspectDateFrom.Name = "uDateInspectDateFrom";
            this.uDateInspectDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDateFrom.TabIndex = 153;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchPlant.TabIndex = 152;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uTextSearchMeasureToolName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Appearance = appearance14;
            this.uTextSearchMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMeasureToolName.Location = new System.Drawing.Point(528, 13);
            this.uTextSearchMeasureToolName.Name = "uTextSearchMeasureToolName";
            this.uTextSearchMeasureToolName.ReadOnly = true;
            this.uTextSearchMeasureToolName.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchMeasureToolName.TabIndex = 149;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 147;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextSearchMeasureToolCode
            // 
            appearance15.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance15.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMeasureToolCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchMeasureToolCode.Location = new System.Drawing.Point(404, 12);
            this.uTextSearchMeasureToolCode.Name = "uTextSearchMeasureToolCode";
            this.uTextSearchMeasureToolCode.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchMeasureToolCode.TabIndex = 148;
            this.uTextSearchMeasureToolCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextMeasureToolCode_KeyDown);
            this.uTextSearchMeasureToolCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMeasureToolCode_EditorButtonClick);
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(288, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(110, 20);
            this.uLabel1.TabIndex = 150;
            this.uLabel1.Text = "1";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(672, 12);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(110, 20);
            this.uLabel2.TabIndex = 151;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(888, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel1.TabIndex = 155;
            this.ultraLabel1.Text = "~";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance2;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckAccuracyFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckStabilityFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateInspectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSpecRange);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLowerSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUpperSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDeviceUnit);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextHiddenPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabControl1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel15);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel16);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel17);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel14);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel13);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel18);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAccuracyValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProcessName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTolerance);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPartCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRRValue);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRepeatCount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProjectCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel6);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel10);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel8);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel12);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel11);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel9);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel3);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uCheckAccuracyFlag
            // 
            this.uCheckAccuracyFlag.Location = new System.Drawing.Point(516, 84);
            this.uCheckAccuracyFlag.Name = "uCheckAccuracyFlag";
            this.uCheckAccuracyFlag.Size = new System.Drawing.Size(128, 20);
            this.uCheckAccuracyFlag.TabIndex = 204;
            // 
            // uCheckStabilityFlag
            // 
            this.uCheckStabilityFlag.Location = new System.Drawing.Point(160, 84);
            this.uCheckStabilityFlag.Name = "uCheckStabilityFlag";
            this.uCheckStabilityFlag.Size = new System.Drawing.Size(128, 20);
            this.uCheckStabilityFlag.TabIndex = 203;
            // 
            // uLabelUnitCode
            // 
            this.uLabelUnitCode.Location = new System.Drawing.Point(368, 108);
            this.uLabelUnitCode.Name = "uLabelUnitCode";
            this.uLabelUnitCode.Size = new System.Drawing.Size(144, 20);
            this.uLabelUnitCode.TabIndex = 202;
            this.uLabelUnitCode.Text = "단위";
            // 
            // uTextUnitCode
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Appearance = appearance55;
            this.uTextUnitCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Location = new System.Drawing.Point(516, 108);
            this.uTextUnitCode.Name = "uTextUnitCode";
            this.uTextUnitCode.ReadOnly = true;
            this.uTextUnitCode.Size = new System.Drawing.Size(144, 21);
            this.uTextUnitCode.TabIndex = 201;
            // 
            // uDateInspectDate
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.Appearance = appearance58;
            this.uDateInspectDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.Location = new System.Drawing.Point(816, 12);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.ReadOnly = true;
            this.uDateInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDate.TabIndex = 200;
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(668, 12);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(144, 20);
            this.uLabelInspectDate.TabIndex = 199;
            this.uLabelInspectDate.Text = "검사일";
            // 
            // uTextSpecRange
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Appearance = appearance53;
            this.uTextSpecRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Location = new System.Drawing.Point(306, 107);
            this.uTextSpecRange.Name = "uTextSpecRange";
            this.uTextSpecRange.ReadOnly = true;
            this.uTextSpecRange.Size = new System.Drawing.Size(57, 21);
            this.uTextSpecRange.TabIndex = 198;
            // 
            // uTextLowerSpec
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLowerSpec.Appearance = appearance46;
            this.uTextLowerSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLowerSpec.Location = new System.Drawing.Point(160, 107);
            this.uTextLowerSpec.Name = "uTextLowerSpec";
            this.uTextLowerSpec.ReadOnly = true;
            this.uTextLowerSpec.Size = new System.Drawing.Size(74, 21);
            this.uTextLowerSpec.TabIndex = 197;
            // 
            // uTextUpperSpec
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperSpec.Appearance = appearance57;
            this.uTextUpperSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUpperSpec.Location = new System.Drawing.Point(235, 107);
            this.uTextUpperSpec.Name = "uTextUpperSpec";
            this.uTextUpperSpec.ReadOnly = true;
            this.uTextUpperSpec.Size = new System.Drawing.Size(70, 21);
            this.uTextUpperSpec.TabIndex = 196;
            // 
            // uLabelDeviceUnit
            // 
            this.uLabelDeviceUnit.Location = new System.Drawing.Point(12, 108);
            this.uLabelDeviceUnit.Name = "uLabelDeviceUnit";
            this.uLabelDeviceUnit.Size = new System.Drawing.Size(144, 20);
            this.uLabelDeviceUnit.TabIndex = 195;
            this.uLabelDeviceUnit.Text = "부품규격";
            // 
            // uTextHiddenPlantCode
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextHiddenPlantCode.Appearance = appearance71;
            this.uTextHiddenPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextHiddenPlantCode.Location = new System.Drawing.Point(984, 7);
            this.uTextHiddenPlantCode.Name = "uTextHiddenPlantCode";
            this.uTextHiddenPlantCode.ReadOnly = true;
            this.uTextHiddenPlantCode.Size = new System.Drawing.Size(63, 21);
            this.uTextHiddenPlantCode.TabIndex = 194;
            this.uTextHiddenPlantCode.Visible = false;
            // 
            // uTabControl1
            // 
            this.uTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl3);
            this.uTabControl1.Location = new System.Drawing.Point(12, 184);
            this.uTabControl1.Name = "uTabControl1";
            this.uTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabControl1.Size = new System.Drawing.Size(1040, 504);
            this.uTabControl1.TabIndex = 193;
            ultraTab1.Key = "RR";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "R&R분석";
            ultraTab3.Key = "AN";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "안정성/정확성 분석";
            this.uTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1036, 478);
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(12, 130);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(144, 20);
            this.uLabel15.TabIndex = 180;
            this.uLabel15.Text = "계측기수";
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(668, 108);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(144, 20);
            this.uLabel16.TabIndex = 192;
            this.uLabel16.Text = "Tol";
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(668, 84);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(144, 20);
            this.uLabel17.TabIndex = 183;
            this.uLabel17.Text = "정확성";
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(368, 130);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(144, 20);
            this.uLabel14.TabIndex = 184;
            this.uLabel14.Text = "측정부품";
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(668, 132);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(144, 20);
            this.uLabel13.TabIndex = 187;
            this.uLabel13.Text = "측정반복";
            // 
            // uTextInspectDesc
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectDesc.Appearance = appearance44;
            this.uTextInspectDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectDesc.Location = new System.Drawing.Point(516, 60);
            this.uTextInspectDesc.Name = "uTextInspectDesc";
            this.uTextInspectDesc.ReadOnly = true;
            this.uTextInspectDesc.Size = new System.Drawing.Size(504, 21);
            this.uTextInspectDesc.TabIndex = 191;
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(12, 152);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(144, 20);
            this.uLabel18.TabIndex = 179;
            this.uLabel18.Text = "RR설정";
            // 
            // uTextAccuracyValue
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAccuracyValue.Appearance = appearance54;
            this.uTextAccuracyValue.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAccuracyValue.Location = new System.Drawing.Point(816, 84);
            this.uTextAccuracyValue.Name = "uTextAccuracyValue";
            this.uTextAccuracyValue.ReadOnly = true;
            this.uTextAccuracyValue.Size = new System.Drawing.Size(100, 21);
            this.uTextAccuracyValue.TabIndex = 167;
            // 
            // uTextInspectItemDesc
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemDesc.Appearance = appearance59;
            this.uTextInspectItemDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemDesc.Location = new System.Drawing.Point(516, 154);
            this.uTextInspectItemDesc.Name = "uTextInspectItemDesc";
            this.uTextInspectItemDesc.ReadOnly = true;
            this.uTextInspectItemDesc.Size = new System.Drawing.Size(110, 21);
            this.uTextInspectItemDesc.TabIndex = 167;
            this.uTextInspectItemDesc.Visible = false;
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(258, 60);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(96, 21);
            this.uTextMaterialName.TabIndex = 161;
            // 
            // uTextMaterialCode
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance72;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(160, 60);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(96, 21);
            this.uTextMaterialCode.TabIndex = 162;
            // 
            // uTextProjectName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectName.Appearance = appearance16;
            this.uTextProjectName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectName.Location = new System.Drawing.Point(516, 12);
            this.uTextProjectName.Name = "uTextProjectName";
            this.uTextProjectName.Size = new System.Drawing.Size(144, 21);
            this.uTextProjectName.TabIndex = 159;
            // 
            // uTextProcessName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Appearance = appearance20;
            this.uTextProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Location = new System.Drawing.Point(516, 36);
            this.uTextProcessName.Name = "uTextProcessName";
            this.uTextProcessName.ReadOnly = true;
            this.uTextProcessName.Size = new System.Drawing.Size(144, 21);
            this.uTextProcessName.TabIndex = 160;
            // 
            // uTextTolerance
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTolerance.Appearance = appearance62;
            this.uTextTolerance.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTolerance.Location = new System.Drawing.Point(816, 108);
            this.uTextTolerance.Name = "uTextTolerance";
            this.uTextTolerance.ReadOnly = true;
            this.uTextTolerance.Size = new System.Drawing.Size(100, 21);
            this.uTextTolerance.TabIndex = 165;
            // 
            // uTextPartCount
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPartCount.Appearance = appearance60;
            this.uTextPartCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPartCount.Location = new System.Drawing.Point(516, 130);
            this.uTextPartCount.Name = "uTextPartCount";
            this.uTextPartCount.ReadOnly = true;
            this.uTextPartCount.Size = new System.Drawing.Size(144, 21);
            this.uTextPartCount.TabIndex = 166;
            // 
            // uTextRRValue
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRRValue.Appearance = appearance56;
            this.uTextRRValue.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRRValue.Location = new System.Drawing.Point(160, 152);
            this.uTextRRValue.Name = "uTextRRValue";
            this.uTextRRValue.ReadOnly = true;
            this.uTextRRValue.Size = new System.Drawing.Size(144, 21);
            this.uTextRRValue.TabIndex = 170;
            // 
            // uTextMeasureCount
            // 
            appearance75.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureCount.Appearance = appearance75;
            this.uTextMeasureCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureCount.Location = new System.Drawing.Point(160, 130);
            this.uTextMeasureCount.Name = "uTextMeasureCount";
            this.uTextMeasureCount.ReadOnly = true;
            this.uTextMeasureCount.Size = new System.Drawing.Size(144, 21);
            this.uTextMeasureCount.TabIndex = 164;
            // 
            // uTextRepeatCount
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepeatCount.Appearance = appearance61;
            this.uTextRepeatCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepeatCount.Location = new System.Drawing.Point(816, 132);
            this.uTextRepeatCount.Name = "uTextRepeatCount";
            this.uTextRepeatCount.ReadOnly = true;
            this.uTextRepeatCount.Size = new System.Drawing.Size(100, 21);
            this.uTextRepeatCount.TabIndex = 172;
            // 
            // uTextPlantName
            // 
            appearance78.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance78;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(160, 36);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(144, 21);
            this.uTextPlantName.TabIndex = 174;
            // 
            // uTextProjectCode
            // 
            appearance79.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Appearance = appearance79;
            this.uTextProjectCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProjectCode.Location = new System.Drawing.Point(160, 12);
            this.uTextProjectCode.Name = "uTextProjectCode";
            this.uTextProjectCode.ReadOnly = true;
            this.uTextProjectCode.Size = new System.Drawing.Size(144, 21);
            this.uTextProjectCode.TabIndex = 169;
            // 
            // uTextMeasureToolName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Appearance = appearance21;
            this.uTextMeasureToolName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolName.Location = new System.Drawing.Point(920, 36);
            this.uTextMeasureToolName.Name = "uTextMeasureToolName";
            this.uTextMeasureToolName.ReadOnly = true;
            this.uTextMeasureToolName.Size = new System.Drawing.Size(100, 21);
            this.uTextMeasureToolName.TabIndex = 168;
            // 
            // uTextMeasureToolCode
            // 
            appearance80.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolCode.Appearance = appearance80;
            this.uTextMeasureToolCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMeasureToolCode.Location = new System.Drawing.Point(816, 36);
            this.uTextMeasureToolCode.Name = "uTextMeasureToolCode";
            this.uTextMeasureToolCode.ReadOnly = true;
            this.uTextMeasureToolCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMeasureToolCode.TabIndex = 171;
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(368, 36);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(144, 20);
            this.uLabel6.TabIndex = 181;
            this.uLabel6.Text = "6";
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(668, 36);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(144, 20);
            this.uLabel7.TabIndex = 182;
            this.uLabel7.Text = "계측기";
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(368, 60);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(144, 20);
            this.uLabel10.TabIndex = 190;
            this.uLabel10.Text = "설명";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 60);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(144, 20);
            this.uLabel8.TabIndex = 185;
            this.uLabel8.Text = "자재";
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(368, 84);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(144, 20);
            this.uLabel12.TabIndex = 186;
            this.uLabel12.Text = "정확성Flag";
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(12, 84);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(144, 20);
            this.uLabel11.TabIndex = 188;
            this.uLabel11.Text = "안정성";
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(368, 154);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(144, 20);
            this.uLabel9.TabIndex = 189;
            this.uLabel9.Text = "검사항목";
            this.uLabel9.Visible = false;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 36);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(144, 20);
            this.uLabel5.TabIndex = 178;
            this.uLabel5.Text = "5";
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(368, 12);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(144, 20);
            this.uLabel4.TabIndex = 177;
            this.uLabel4.Text = "4";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 12);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(144, 20);
            this.uLabel3.TabIndex = 176;
            this.uLabel3.Text = "3";
            // 
            // frmQAT0022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0022";
            this.Load += new System.EventHandler(this.frmQAT0022_Load);
            this.Activated += new System.EventHandler(this.frmQAT0022_Activated);
            this.Resize += new System.EventHandler(this.frmQAT0022_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRNRPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraiserPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipmentPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRNRValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraiserVariation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipmentVariation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRnRAnalysisDate)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).EndInit();
            this.uGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAccuracyFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckStabilityFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLowerSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUpperSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHiddenPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).EndInit();
            this.uTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAccuracyValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPartCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRRValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepeatCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProjectCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectDesc;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTolerance;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPartCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRRValue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepeatCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProjectCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRnRAnalysisDate;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAccuracyValue;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipmentPercent;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipmentVariation;
        private Infragistics.Win.Misc.UltraLabel uLabel23;
        private Infragistics.Win.Misc.UltraLabel uLabel22;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteCalc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRNRPercent;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAppraiserPercent;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRNRValue;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAppraiserVariation;
        private Infragistics.Win.Misc.UltraLabel uLabel29;
        private Infragistics.Win.Misc.UltraLabel uLabel26;
        private Infragistics.Win.Misc.UltraLabel uLabel28;
        private Infragistics.Win.Misc.UltraLabel uLabel25;
        private Infragistics.Win.Misc.UltraLabel uLabel27;
        private Infragistics.Win.Misc.UltraLabel uLabel24;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHiddenPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLowerSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUpperSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelDeviceUnit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRange;
        private Infragistics.Win.Misc.UltraButton uButtonReCalc;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckStabilityFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAccuracyFlag;
    }
}