﻿/*----------------------------------------------------------------------*/
/* 系统名       : 品质保证管理                                          */
/* 模块（分类） : 环境有害物质管理                                      */
/* 程序ID       : frmQATZ0022.cs                                        */
/* 应用程序名   : CAS NO 管理                                 */
/* 作者         : 石超                                                */
/* 日期         : 2014-07-28                                            */                   
/*                                     */
/*----------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.EnterpriseServices;
// 파일첨부
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;

using QRPDB;
using System.Data.SqlClient;


namespace QRPQAT.UI
{
    public partial class frmQATZ0022 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0022()
        {
            InitializeComponent();
        }

        private void frmQATZ0022_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0022_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("CAS NO 管理", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤초기화
            SetToolAuth();
            InitComboBox();
            InitEtc();
            InitLabel();
            InitGrid();
            InitGroupBox();

            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmQATZ0022_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }



        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값

                this.uTextSearchCasNO.MaxLength = 500;
                this.uTextSearchSubstances.MaxLength = 500;
                this.uTextCasNO.MaxLength = 500;
                this.uTextSubstances.MaxLength = 500;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchCasNO, "CasNO", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchSubstances, "Substances", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCasNO, "CasNO", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSubstances, "Substances", m_resSys.GetString("SYS_FONTNAME"), true, true);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                #region Header
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "Seq", "Seq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "CasNO", "CasNO", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "Substances", "Substances", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "WriteUserName", "登录者", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "WriteDate", "登录时间", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "ModifyUserName", "修改者", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                 wGrid.mfSetGridColumn(this.uGridHeader, 0, "ModifyDate", "修改时间", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "RohsEndDate", "Rohs 만료일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                #endregion

                #region Detail

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "CasNO", "CasNO", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Substances", "Substances", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Seq", "Seq", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                                              

                #endregion

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "UnGreenMaterialD");
                QRPQAT.BL.QATENV.UnGreenMaterialD clsDetail = new QRPQAT.BL.QATENV.UnGreenMaterialD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtDetail = clsDetail.mfSetDateInfo();

                this.uGridDetail.SetDataBinding(dtDetail, string.Empty);

                //폰트설정
                uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridDetail, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strCasNO = this.uTextSearchCasNO.Text;
                string strSubstances = this.uTextSearchSubstances.Text;

                // BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
                //QRPQAT.BL.QATENV.CasNO clsHeader = new QRPQAT.BL.QATENV.CasNO();
                //brwChannel.mfCredentials(clsHeader);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtRtn = clsHeader.mfReadQATCasNO( strCasNO, strSubstances);
                this.uGridHeader.DataSource = dtRtn;
                this.uGridHeader.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 신규일시 저장된 정보가 있는지 검사
                if (!this.uGroupBoxContentsArea.Expanded)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001047", Infragistics.Win.HAlign.Center);
                    return;
                }

                // 필수 입력 확인


                if (this.uTextCasNO.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001568", Infragistics.Win.HAlign.Center);

                    this.uTextCasNO.Focus();
                    return;
                }
                if (this.uTextSubstances.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001569", Infragistics.Win.HAlign.Center);

                    this.uTextSubstances.Focus();
                    return;
                }


                // BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
                //QRPQAT.BL.QATENV.CasNO clsHeader = new QRPQAT.BL.QATENV.CasNO();
                //brwChannel.mfCredentials(clsHeader);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                //콤보박스 선택값 Validation Check//////////
                //QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                //if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.No) return;


                // 헤더/상세정보 데이터테이블로 반환하는 메소드 호출
                DataTable dtHeader = RtnHeaderdt();
                //DataTable dtDetail = RtnDetaildt();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 저장 Method 호출
                string strErrRtn = clsHeader.mfSaveQATCasNO(dtHeader, m_resSys.GetString("SYS_USERID"));

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 결과검사
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum == 0)
                {
                    ////// 첨부파일 Upload Method 호출
                    ////FileUpload();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
                else
                {
                    string strLang = m_resSys.GetString("SYS_LANG");

                    string strErrMes = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strErrMes = msg.GetMessge_Text("M000953", strLang);
                    else
                        strErrMes = ErrRtn.ErrMessage;

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strErrMes,
                                            Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000625", "M000645",
                                   Infragistics.Win.HAlign.Right);

                    // 헤더그리드에 정보가 없을시 조회 메소드
                    if (!(this.uGridHeader.Rows.Count > 0))
                        mfSearch();

                    return;
                }
                else
                {
                    // 필수입력사항 확인

                     if (this.uTextCasNO.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "M001264", "M000629", "M001209",
                                    Infragistics.Win.HAlign.Right);

                        this.uTextCasNO.Focus();
                        return;
                    }
                    else
                    {
                        // 삭제여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // 매개변수 설정
                            string strItemName = this.uTextCasNO.Text;
                            string strItemNameDetail = this.uTextSubstances.Text;

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
                            QRPQAT.BL.QATENV.CasNO clsHeader = new QRPQAT.BL.QATENV.CasNO();
                            brwChannel.mfCredentials(clsHeader);

                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialD), "CasNO");
                            QRPQAT.BL.QATENV.CasNO clsMaterialD = new QRPQAT.BL.QATENV.CasNO();
                            brwChannel.mfCredentials(clsMaterialD);

                            // 컬럼설정 메소드 호출
                           // DataTable dtRtn = clsMaterialD.mfReadQATUnGreenMaterialD_All(strPlantCode, strVendorCode, strItemName, strItemNameDetail);

                            // ProgressBar 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread threadPop = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // 메소드 호출
                            //string strErrRtn = clsHeader.mfDeleteQATUnGreenMaterialH_B(strPlantCode,
                            //                                                            strVendorCode,
                            //                                                            strItemName,
                            //                                                            strItemNameDetail,
                            //                                                            dtRtn,
                            //                                                            m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                            // ProgressBar Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 결과 검사
                            QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                            //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000677",
                                                            Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridHeader.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridHeader);
                    if (this.uGridDetail.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridDetail);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000799", "M000333",
                                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextCasNO.Text = "";
                this.uTextSubstances.Text = "";
                this.uTextSeq.Text = "";

                while (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Rows[0].Delete(false);
                }

                // PK 편집가능상태로
                this.uTextCasNO.ReadOnly = false;
                this.uTextSubstances.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHeaderdt()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
                //QRPQAT.BL.QATENV.CasNO clsHeader = new QRPQAT.BL.QATENV.CasNO();
                //brwChannel.mfCredentials(clsHeader);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                // 컬럼설정 메소드 호출
                dtRtn = clsHeader.mfSetDateInfo_CasNO();

                DataRow drRow = dtRtn.NewRow();
                
                drRow["CasNO"] = this.uTextCasNO.Text;
                drRow["Substances"] = this.uTextSubstances.Text;
                drRow["Seq"] = this.uTextSeq.Text;
                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }


        /// <summary>
        /// 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        //private void Search_HeaderDetail(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        //{
        //    try
        //    {
        //        // SystemInfo ReSourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //        // BL 연결
        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
        //        QRPQAT.BL.QATENV.CasNO clsHeader = new QRPQAT.BL.QATENV.CasNO();
        //        brwChannel.mfCredentials(clsHeader);

        //        // 헤더 상세조회 Method 호출
        //        DataTable dtHeader = clsHeader.mfReadQATUnGreenMaterialHDetail(strPlantCode, strVendorCode, strItemName, strItemNameDetail, m_resSys.GetString("SYS_LANG"));
        //        if (dtHeader.Rows.Count > 0)
        //        {

        //            this.uTextCasNO.Text = dtHeader.Rows[0]["ItemName"].ToString();
        //            this.uTextSubstances.Text = dtHeader.Rows[0]["ItemNameDetail"].ToString();

        //            // PK 편집불가상태로
        //            this.uTextCasNO.ReadOnly = true;
        //            this.uTextSubstances.ReadOnly = true;

        //            //管控物质超标预警
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        private void Search_Detail(string strCasNO)  
        {
            try
            {
                // BL 연결
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.CasNO), "CasNO");
                //QRPQAT.BL.QATENV.CasNO clsDetail = new QRPQAT.BL.QATENV.CasNO();
                //brwChannel.mfCredentials(clsDetail);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.UnGreenMaterialH), "UnGreenMaterialH");
                QRPQAT.BL.QATENV.UnGreenMaterialH clsHeader = new QRPQAT.BL.QATENV.UnGreenMaterialH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtRtn = clsHeader.mfReadQATCasNO(strCasNO, "");
                if (dtRtn.Rows.Count>0)
                {
                    this.uTextCasNO.Text = dtRtn.Rows[0]["CasNO"].ToString();
                    this.uTextSubstances.Text = dtRtn.Rows[0]["Substances"].ToString();
                    this.uTextSeq.Text = dtRtn.Rows[0]["Seq"].ToString();
                }

                this.uGridDetail.SetDataBinding(dtRtn, string.Empty);
                ////this.uGridDetail.DataSource = dtRtn;
                ////this.uGridDetail.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDetail, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        // 행 공백일시 자동삭제
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 제목, 첨부파일, 비고가 모두 공백이면 행삭제
                if (e.Cell.Row.Cells["FileTitle"].Value.ToString() == "" &&
                    e.Cell.Row.Cells["FileVarName"].Value.ToString() == "" &&
                    e.Cell.Row.Cells["EtcDesc"].Value.ToString() == "")
                {
                    e.Cell.Row.Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 50;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 750;
                    for (int i = 0; i < uGridHeader.Rows.Count; i++)
                    {
                        uGridHeader.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정시 RowSelector 이미지 설정하는 이벤트
        private void uGridDetail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더 그리드 더블클릭시 헤더상세 / 상세정보 보여주는 이벤트
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // 현재 클릭된 행 고정
                e.Row.Fixed = true;  

                string strCasNO = e.Row.Cells["CasNO"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 헤더상세 / 첨부파일 조회 Method 호출
               // Search_HeaderDetail(strPlantCode, strVendorCode, strItemName, strItemNameDetail);
                Search_Detail(strCasNO);

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridDetail.Rows[i].Hidden = true;
                        this.uGridDetail.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion


        // 그리드 첨부파일 셀버튼 클릭 이벤트
        private void uGridDetail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "FileVarName")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    //오피스2007부터 확장자가 바뀜 docx, xlsx, pptx
                    //"Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                    //"Word Documents(*.doc;*.docx)|*.doc;*.docx|Excel Worksheets(*.xls;*.xlsx)|*.xls;*.xlsx|PowerPoint Presentations(*.ppt;*.pptx)|*.ppt;*.pptx|Office Files(*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx)|*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx|Text Files(*.txt)|*.txt|Portable Document Format Files(*.pdf)|*.pdf|All Files(*.*)|*.*";
                    openFile.Filter = "All files (*.*)|*.*|Word Documents(*.doc;*.docx)|*.doc;*.docx|Excel Worksheets(*.xls;*.xlsx)|*.xls;*.xlsx|PowerPoint Presentations(*.ppt;*.pptx)|*.ppt;*.pptx|Office Files(*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx)|*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx|Text Files(*.txt)|*.txt|Portable Document Format Files(*.pdf)|*.pdf";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        string strFilePath = openFile.FileName;
                        
                        if (strFilePath.Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(strFilePath);

                            //if (!GetFileSize(fileDoc.Length))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //        "M001053", "M001452", "M001493", Infragistics.Win.HAlign.Right);
                            //    return;
                            //}
                            
                            if (CheckingSpecialText(fileDoc.Name))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);
                                return;
                            }
                            
                            e.Cell.Value = fileDoc.Name;
                            e.Cell.Row.Cells["FileVarValue"].Value = fun_ConvertFileToBinray(strFilePath);

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                            this.uGridDetail.DisplayLayout.Bands[0].AddNew();
                            //e.Cell.Value = strImageFile;
                            //QRPGlobal grdImg = new QRPGlobal();
                            //e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDetail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell uGCell = this.uGridDetail.ActiveCell;
                if (uGCell == null)
                    return;

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (uGCell.Column.Key.Equals("FileVarName"))
                    {
                        byte[] empty = { };
                        uGCell.Value = string.Empty;
                        uGCell.Row.Cells["FileVarValue"].Value = empty;

                        QRPGlobal grdImg = new QRPGlobal();
                        uGCell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 다운로드 이벤트
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (!this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]")
                            || this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString() == string.Empty)
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    int intFileNum = 0;
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                        if (this.uGridDetail.Rows[i].Hidden == false
                            && Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value)
                            && this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]"))
                            intFileNum++;
                    }
                    if (intFileNum == 0)
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";

                        for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                        {
                            if (!this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString().Equals(string.Empty)
                                && Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value)
                                && this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Equals("System.Byte[]"))
                            {
                                string strFileName = this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString();
                                byte[] btFileByte = (byte[])this.uGridDetail.Rows[i].Cells["FileVarValue"].Value;

                                System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                                fs.Write(btFileByte, 0, btFileByte.Length);
                                fs.Close();
                            }
                        }

                        // 저장폴더 열기
                        System.Diagnostics.Process p = new System.Diagnostics.Process();
                        p.StartInfo.FileName = strSaveFolder;
                        p.Start();
                    }
                }


                #region 주석처리
                /*
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGridDetail.Rows[i].Cells["FileVarValue"].Value.ToString().Contains(":\\")
                            || this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString() == string.Empty)
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "입력확인", "첨부파일이 없는 리스트를 선택했습니다.",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "입력확인", "첨부파일을 선택해주세요",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    int intFileNum = 0;
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                        if (this.uGridDetail.Rows[i].Hidden == false && this.uGridDetail.Rows[i].Cells["FileVarName"].Value.ToString().Contains(":\\") == false)
                            intFileNum++;
                    }
                    if (intFileNum == 0)
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0014");

                        //설비이미지 Upload하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                        {
                            if (this.uGridDetail.Rows[i].Hidden == false && this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString().Contains(":\\") == false)
                                arrFile.Add(this.uGridDetail.Rows[i].Cells["FilePathName"].Value.ToString());
                        }

                        //Upload정보 설정
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                    }
                }
                 * */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGridDetail_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FileVarName"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Row.Cells["FileVarValue"].Text.Equals("System.Byte[]"))
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = System.Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = System.Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";

                            string strFileName = e.Cell.Value.ToString();
                            byte[] btFileByte = (byte[])e.Cell.Row.Cells["FileVarValue"].Value;

                            System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                            fs.Write(btFileByte, 0, btFileByte.Length);
                            fs.Close();

                            // 파일 자동실행
                            ////System.Diagnostics.Process p = new System.Diagnostics.Process();
                            ////p.StartInfo.FileName = strSaveFolder + strFileName;
                            ////p.Start();
                            System.Diagnostics.Process.Start(strSaveFolder);
                        }
                    }

                    #region 변경에 의한 주석처리
                    ////WinMessageBox msg = new WinMessageBox();
                    ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    ////// 파일서버에서 불러올수 있는 파일인지 체크
                    ////if (e.Cell.Value.ToString().Contains(":\\"))
                    ////{
                    ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                             "처리결과", "처리결과", "업로드된 첨부화일이 아닙니다.",
                    ////                             Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}
                    ////else
                    ////{
                    ////    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    ////    //화일서버 연결정보 가져오기
                    ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////    brwChannel.mfCredentials(clsSysAccess);
                    ////    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    ////    //첨부파일 저장경로정보 가져오기

                    ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    ////    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    ////    brwChannel.mfCredentials(clsSysFilePath);
                    ////    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0014");

                    ////    //첨부파일 Download하기
                    ////    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ////    ArrayList arrFile = new ArrayList();
                    ////    arrFile.Add(e.Cell.Value.ToString());

                    ////    //Download정보 설정
                    ////    string strExePath = Application.ExecutablePath;
                    ////    int intPos = strExePath.LastIndexOf(@"\");
                    ////    strExePath = strExePath.Substring(0, intPos + 1);

                    ////    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                    ////                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                    ////                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                    ////                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                    ////                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                    ////                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    ////    fileAtt.ShowDialog();

                    ////    // 파일 실행시키기
                    ////    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    ////}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0022_Resize(object sender, EventArgs e) 
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일을 Byte[] 로 변환하여 반환하는 메소드
        /// </summary>
        /// <param name="strFileFullPath">파일경로</param>
        /// <returns></returns>
        private byte[] fun_ConvertFileToBinray(string strFileFullPath)
        {
            // FileStream 자동 소멸을 위한 Using 구문
            using (System.IO.FileStream fs = new System.IO.FileStream(strFileFullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                int intLength = Convert.ToInt32(fs.Length);
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                byte[] btBuffer = br.ReadBytes(intLength);
                fs.Close();

                return btBuffer;
            }
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }

        /// <summary>
        /// 첨부파일사이즈 판단하여 6M이하면 true 6M초과시 false
        /// </summary>
        /// <param name="byteCount"></param>
        /// <returns></returns>
        private bool GetFileSize(double byteCount)
        {
            try
            {
                
                if (byteCount >= 1073741824.0)
                    return false;//intSize = Convert.ToInt32(byteCount / 1073741824.0);
                else if (byteCount >= 1048576.0)
                {
                    double bdSize = 0;
                    bdSize = byteCount / 1048576.0;
                    if (bdSize > 6)
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        //[AutoComplete]
        //public DataTable mfReadQATCasNO(string strCasNO, string strSubstances)
        //{
        //    DataTable dtRtn = new DataTable();
        //    string strConnection = "server = 10.61.61.203 ; database= QRP_PSTS_DEV ;uid= qrpusr ;pwd= pstsint ";

        //    SqlConnection SqlCon = new SqlConnection(strConnection);

        //    SqlCon.Open();
        //    MessageBox.Show(SqlCon.State.ToString());

        //    //SQLS sql = new SQLS();
        //    try
        //    {
        //        //sql.mfConnect();
        //        DataTable dtParam = mfSetParamDataTable();
        //        mfAddParamDataRow(dtParam, "@i_strCasNO", ParameterDirection.Input, SqlDbType.VarChar, strCasNO, 100);
        //        mfAddParamDataRow(dtParam, "@i_strSubstances", ParameterDirection.Input, SqlDbType.VarChar, strSubstances, 500);

        //        dtRtn = mfExecReadStoredProc(SqlCon, "up_Select_QATCasNO", dtParam);
        //        return dtRtn;
        //    }
        //    catch (Exception ex)
        //    {
        //        return dtRtn;
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        //sql.mfDisConnect();
        //        SqlCon.Close();
        //        SqlCon.Dispose();
        //        dtRtn.Dispose();
        //    }
        //}

        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, string strValue, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, string strValue)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        public DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;

                dt.Clear();
                dt = null;

                dc = null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        public string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            QRPDB.TransErrRtn Result = new QRPDB.TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }

        public string mfExecTransStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            QRPDB.TransErrRtn Result = new QRPDB.TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    //m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }

        public DataTable mfExecReadStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();
                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }
                        m_SqlCmd.Parameters.Add(param);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

    }
}
