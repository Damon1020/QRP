﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 공정환경관리                                             */
/* 프로그램ID   : frmQATZ0015.cs                                        */
/* 프로그램명   : 정전기 예방점검 관리                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPQAT.UI
{
    public partial class frmQATZ0015 : Form, IToolbar
    {
        // Resource 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0015()
        {
            InitializeComponent();
        }

        #region Form Events...
        private void frmQATZ0015_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0015_FormClosed(object sender, FormClosedEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmQATZ0015_Load(object sender, EventArgs e)
        {
            SetToolAuth();

            InitButton();
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitEtc();
            InitComboBox();

            this.uGroupBoxContentsArea.Expanded = false;
        }

        private void frmQATZ0015_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();

            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 필수입력사항 확인
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                    return;
                else if (this.uTextManageNo.Text.Equals(string.Empty) || this.uTextPlantCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000644", Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.StaticCheckH), "StaticCheckH");
                        QRPQAT.BL.QATENV.StaticCheckH clsH = new QRPQAT.BL.QATENV.StaticCheckH();
                        brwChannel.mfCredentials(clsH);

                        // 변수설정
                        string strPlantCode = this.uTextPlantCode.Text;
                        string strManageNo = this.uTextManageNo.Text;

                        // ProgressBar 생성
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 삭제메소드 실행
                        string strErrRtn = clsH.mfDeleteStaticCheckH_All(strPlantCode, strManageNo);

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 첨부파일 삭제 메소드 호출
                            FileDelete();

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang)
                                                        , ErrRtn.ErrMessage,
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridStaticCheck.Rows.Count > 0)
                {
                    WinGrid wGRid = new WinGrid();
                    wGRid.mfDownLoadGridToExcel(this.uGridStaticCheck);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                
                // 필수 입력사항 확인
                if (this.uGroupBoxContentsArea.Expanded == false)
                    return;
                else if (this.uTextCheckUserID.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001231", "M001463"
                                                    , Infragistics.Win.HAlign.Right);

                    this.uTextCheckUserID.Focus();
                    return;
                }
                else if (this.uDateCheckDate.Value == null || this.uDateCheckDate.Value == DBNull.Value || this.uDateCheckDate.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001231", "M001462"
                                                    , Infragistics.Win.HAlign.Right);

                    this.uDateCheckDate.Focus();
                    this.uDateCheckDate.DropDown();
                    return;
                }
                else if (this.uComboEquipLoc.Value == null || this.uComboEquipLoc.Value == DBNull.Value || this.uComboEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001231", "M001466"
                                                    , Infragistics.Win.HAlign.Right);

                    this.uComboEquipLoc.Focus();
                    this.uComboEquipLoc.DropDown();
                    return;
                }
                else
                {
                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this))
                        return;

                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장정보 데이터 테이블 반환 메소드 호출
                        DataTable dtHeader = Rtn_SaveInfo_Header();
                        DataTable dtDetail = Rtn_SaveInfo_Detail();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.StaticCheckH), "StaticCheckH");
                        QRPQAT.BL.QATENV.StaticCheckH clsH = new QRPQAT.BL.QATENV.StaticCheckH();
                        brwChannel.mfCredentials(clsH);

                        string strErrRtn = clsH.mfSaveStaticCheckH(dtHeader, dtDetail, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과확인
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // 관리번호 저장
                            string strRtnManageNo = ErrRtn.mfGetReturnValue(0);
                            // 첨부파일 저장/삭제
                            FileUpload(strRtnManageNo);

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                    , ErrRtn.ErrMessage,
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();

            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                DateTime _date_YoF = new DateTime(DateTime.Now.Year, 1, 1);

                // 검색조건 설정
                string strPlantCode = this.uComboSearchPlant.Value == null ? string.Empty : this.uComboSearchPlant.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value == null ? string.Empty : this.uComboSearchEquipLoc.Value.ToString();
                string strCheckDateFrom = this.uDateSearchCheckDateFrom.Value == null ? _date_YoF.ToString("yyyy-MM-dd") : Convert.ToDateTime(this.uDateSearchCheckDateFrom.Value).ToString("yyyy-MM-dd");
                string strCheckDateTo = this.uDateSearchCheckDateTo.Value == null ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(this.uDateSearchCheckDateTo.Value).ToString("yyyy-MM-dd");

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.StaticCheckH), "StaticCheckH");
                QRPQAT.BL.QATENV.StaticCheckH clsH = new QRPQAT.BL.QATENV.StaticCheckH();
                brwChannel.mfCredentials(clsH);

                // 프로그래스바 생성
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtHeader_S = clsH.mfReadStaticCheckH_Search(strPlantCode, strEquipLocCode, strCheckDateFrom, strCheckDateTo, m_resSys.GetString("SYS_LANG"));

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGridStaticCheck.SetDataBinding(dtHeader_S, string.Empty);

                if (dtHeader_S.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridStaticCheck, 0);
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonFileDown, "FileDown", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_FillDown);

                this.uButtonDelete.Click += new EventHandler(uButtonDelete_Click);
                this.uButtonFileDown.Click += new EventHandler(uButtonFileDown_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGroupBox wGroupBox = new QRPCOM.QRPUI.WinGroupBox();

                ////this.uGroupBoxDetail.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
                ////this.uGroupBoxDetail.Text = string.Empty;
                wGroupBox.mfSetGroupBox(this.uGroupBoxDetail, GroupBoxType.INFO, "상세정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBoxDetail.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                Infragistics.Win.Misc.UltraLabel[] uLabels = {this.uLabelCheckDate, this.uLabelCheckUser, this.uLabelEtcDesc
                                                             , this.uLabelEuipLoc, this.uLabelInsResultFIle, this.uLabelManageNo
                                                             , this.uLabelPlant, this.uLabelSearchCheckDate, this.uLabelSearchEquipLoc
                                                             , this.uLabelSearchPlant, this.uLabelSpec
                                                             , this.uLabel01, this.uLabel02, this.uLabel03, this.uLabel04, this.uLabel05
                                                             , this.uLabel06, this.uLabel07, this.uLabel08, this.uLabel09, this.uLabel10
                                                             , this.uLabel11, this.uLabel12, this.uLabel13, this.uLabel14, this.uLabel15
                                                             , this.uLabel16, this.uLabel17, this.uLabel18, this.uLabel19, this.uLabel20
                                                             , this.uLabel21, this.uLabel22, this.uLabel23, this.uLabel24, this.uLabel25
                                                             , this.uLabel26, this.uLabel27, this.uLabel28, this.uLabel29, this.uLabel30
                                                             , this.uLabel31, this.uLabel32, this.uLabel33, this.uLabel34, this.uLabel35
                                                             , this.uLabel36, this.uLabel37, this.uLabel38, this.uLabel39, this.uLabel40
                                                             , this.uLabel41, this.uLabel42, this.uLabel43, this.uLabel44, this.uLabel45
                                                             , this.uLabel46, this.uLabel47, this.uLabel48, this.uLabel49, this.uLabel50
                                                             , this.uLabel51, this.uLabel52, this.uLabel53, this.uLabel54, this.uLabel55
                                                             , this.uLabel56, this.uLabel57, this.uLabel58, this.uLabel59, this.uLabel60
                                                             , this.uLabel61, this.uLabel62, this.uLabel63, this.uLabel64, this.uLabel65
                                                             , this.uLabel66, this.uLabel67, this.uLabel68, this.uLabel69, this.uLabel70
                                                             , this.uLabel71, this.uLabel72, this.uLabel73};

                for (int i = 0; i < uLabels.Length; i++)
                {
                    wLabel.mfSetLabel(uLabels[i], uLabels[i].Text, m_resSys.GetString("SYS_FONTNAME"), true, false);
                }

                this.uLabelEuipLoc.Appearance.ForeColor = Color.DarkRed;
                this.uLabelCheckDate.Appearance.ForeColor = Color.DarkRed;
                this.uLabelCheckUser.Appearance.ForeColor = Color.DarkRed;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinComboEditor wCombo = new QRPCOM.QRPUI.WinComboEditor();

                // 공장
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), string.Empty, string.Empty, "PlantCode", "PlantName", dtPlant);


                // 결과 콤보
                Infragistics.Win.UltraWinEditors.UltraComboEditor[] uCombos = {this.uComboInsEquipItem1, this.uComboInsEquipItem2
                                                                          , this.uComboInsEquipItem3, this.uComboInsEquipItem4, this.uComboInsEquipItem5
                                                                          , this.uComboInsResItem1, this.uComboInsResItem2, this.uComboInsResItem3
                                                                          , this.uComboInsWorkTableItem1, this.uComboInsWorkTableItem2
                                                                          , this.uComboInsWorkTableItem3, this.uComboInsWorkTableItem4};

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                for(int i=0; i<uCombos.Length; i++)
                {
                    wCombo.mfSetComboEditor(uCombos[i], false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                        , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "ComCode", "ComCodeName", dtCom);
                }


                // 현재 로그인사용자로 공장조건 고정 --> 공장변경가능으로 변경시 공장콤보 변경 이벤트로 이동
                // 위치정보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsEquLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsEquLoc);

                DataTable dtEquLoc = clsEquLoc.mfReadLocationCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "EquipLocCode", "EquipLocName", dtEquLoc);

                wCombo.mfSetComboEditor(this.uComboEquipLoc, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.None
                    , true, 500, Infragistics.Win.HAlign.Left, string.Empty, string.Empty, string.Empty, "EquipLocCode", "EquipLocName", dtEquLoc);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                #region 조회 그리드

                wGrid.mfInitGeneralGrid(this.uGridStaticCheck, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                    , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false, Infragistics.Win.DefaultableBoolean.False
                    , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "ManageNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                ////wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "EquipLocCode", "측정위치", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "EquipLocName", "측정위치", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "CheckDate", "점검일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "CheckUserName", "점검자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "DocSpec", "관련Spec", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridStaticCheck, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                // FontSize 설정
                this.uGridStaticCheck.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridStaticCheck.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집불가 상태로
                this.uGridStaticCheck.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                this.uGridStaticCheck.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridStaticCheck_DoubleClickRow);

                #endregion

                #region Detail그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                    , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true, Infragistics.Win.DefaultableBoolean.False
                    , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, string.Empty, string.Empty, "false");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, string.Empty, string.Empty, "0");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "InspectItem", "점검대상", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 300
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ActionDesc", "조치내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 300
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ActionFile", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, string.Empty, string.Empty, string.Empty);

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ActionDate", "조치일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar, string.Empty, string.Empty, DateTime.Now.ToString("yyyy-MM-dd"));

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ActionUserID", "조치자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, string.Empty, string.Empty, m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "ActionUserName", "조치자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, string.Empty, string.Empty, m_resSys.GetString("SYS_USERNAME"));

                // Font 설정
                this.uGridDetail.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDetail.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridDetail_CellChange);
                this.uGridDetail.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridDetail_ClickCellButton);
                this.uGridDetail.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(uGridDetail_DoubleClickCell);
                this.uGridDetail.KeyDown += new KeyEventHandler(uGridDetail_KeyDown);

                #endregion

                wGrid.mfLoadGridColumnProperty(this);
            }
            catch (System.Exception ex)
            {
                
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("정전기 예방점검 실시", m_resSys.GetString("SYS_FONTNAME"), 12);

                this.uComboSearchPlant.Hide();
                this.uLabelSearchPlant.Hide();

                this.uDateSearchCheckDateFrom.MaskInput = "yyyy-mm-dd";
                this.uDateSearchCheckDateTo.MaskInput = "yyyy-mm-dd";
                this.uDateCheckDate.MaskInput = "yyyy-mm-dd hh:mm:ss";

                DateTime dateT = new DateTime(DateTime.Now.Year, 1, 1);
                this.uDateSearchCheckDateFrom.Value = dateT.ToString("yyyy-MM-dd");
                this.uDateSearchCheckDateTo.Value = DateTime.Now.ToString("yyyy-mm-dd");

                this.uTextCheckUserID.MaxLength = 20;
                this.uTextCheckUserID.ImeMode = ImeMode.Disable;
                this.uTextCheckUserID.CharacterCasing = CharacterCasing.Upper;

                this.uTextEtcDesc.MaxLength = 2000;

                this.uTextInsEtcItem1.MaxLength = 50;
                this.uTextInsEtcItem2.MaxLength = 50;
                this.uTextInsEtcItem3.MaxLength = 50;
                this.uTextInsEtcItem4.MaxLength = 50;
                this.uTextInsEtcItem5.MaxLength = 50;
                this.uTextInsEtcItem6.MaxLength = 50;
                this.uTextInsEtcItem7.MaxLength = 50;
                this.uTextInsEtcItem8.MaxLength = 50;
                this.uTextInsEtcItem9.MaxLength = 50;
                this.uTextInsEtcItem10.MaxLength = 50;
                this.uTextInsEtcItem11.MaxLength = 50;
                this.uTextInsEtcItem12.MaxLength = 50;
                this.uTextInsEtcItem13.MaxLength = 50;
                this.uTextInsEtcItem14.MaxLength = 50;
                this.uTextInsEtcItem15.MaxLength = 50;
                this.uTextInsEtcItem16.MaxLength = 50;
                this.uTextInsEtcItem17.MaxLength = 50;
                this.uTextInsEtcItem18.MaxLength = 50;
                this.uTextInsEtcItem19.MaxLength = 50;
                this.uTextInsEtcItem20.MaxLength = 50;

                this.uTextInsResultFile.ButtonsRight["Up"].Appearance.Image = Properties.Resources.btn_Fileupload;
                this.uTextInsResultFile.ButtonsRight["Up"].Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;
                this.uTextInsResultFile.ButtonsRight["Up"].Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
                this.uTextInsResultFile.ButtonsRight["Down"].Appearance.Image = Properties.Resources.btn_Filedownload;
                this.uTextInsResultFile.ButtonsRight["Down"].Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;
                this.uTextInsResultFile.ButtonsRight["Down"].Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
                this.uTextInsResultFile.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uTextInsResultFile.ReadOnly = true;
                this.uTextInsResultFile.Appearance.BackColor = Color.Gainsboro;

                // TextBox Event
                this.uTextCheckUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextCheckUserID_EditorButtonClick);
                this.uTextCheckUserID.KeyDown += new KeyEventHandler(uTextCheckUserID_KeyDown);
                this.uTextInsResultFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextInsResultFile_EditorButtonClick);

                // Set TabIndex
                this.uComboSearchEquipLoc.TabIndex = 1;
                this.uDateSearchCheckDateFrom.TabIndex = 2;
                this.uDateSearchCheckDateTo.TabIndex = 3;

                this.uTextManageNo.TabStop = false;
                this.uTextCheckUserID.TabIndex = 1;
                this.uTextCheckUserName.TabStop = false;
                this.uDateCheckDate.TabIndex = 2;
                this.uComboEquipLoc.TabIndex = 3;
                this.uTextSpec.TabIndex = 4;
                this.uTextPlantCode.TabStop = false;
                this.uComboInsEquipItem1.TabIndex = 5;
                this.uComboInsEquipItem2.TabIndex = 6;
                this.uComboInsEquipItem3.TabIndex = 7;
                this.uComboInsEquipItem4.TabIndex = 8;
                this.uComboInsEquipItem5.TabIndex = 9;
                this.uComboInsWorkTableItem1.TabIndex = 10;
                this.uComboInsWorkTableItem2.TabIndex = 11;
                this.uComboInsWorkTableItem3.TabIndex = 12;
                this.uComboInsWorkTableItem4.TabIndex = 13;
                this.uComboInsResItem1.TabIndex = 14;
                this.uComboInsResItem2.TabIndex = 15;
                this.uComboInsResItem3.TabIndex = 16;
                this.uTextInsEtcItem1.TabIndex = 17;
                this.uTextInsEtcItem2.TabIndex = 18;
                this.uTextInsEtcItem3.TabIndex = 19;
                this.uTextInsEtcItem4.TabIndex = 20;
                this.uTextInsEtcItem5.TabIndex = 21;
                this.uTextInsEtcItem6.TabIndex = 22;
                this.uTextInsEtcItem7.TabIndex = 23;
                this.uTextInsEtcItem8.TabIndex = 24;
                this.uTextInsEtcItem9.TabIndex = 25;
                this.uTextInsEtcItem10.TabIndex = 26;
                this.uTextInsEtcItem11.TabIndex = 27;
                this.uTextInsEtcItem12.TabIndex = 28;
                this.uTextInsEtcItem13.TabIndex = 29;
                this.uTextInsEtcItem14.TabIndex = 30;
                this.uTextInsEtcItem15.TabIndex = 31;
                this.uTextInsEtcItem16.TabIndex = 32;
                this.uTextInsEtcItem17.TabIndex = 33;
                this.uTextInsEtcItem18.TabIndex = 34;
                this.uTextInsEtcItem19.TabIndex = 35;
                this.uTextInsEtcItem20.TabIndex = 36;
                this.uTextInsResultFile.TabIndex = 37;
                this.uTextEtcDesc.TabIndex = 38;

                this.uLabelPlant.Hide();
                this.uTextPlantCode.Hide();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods...

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 ID, 이름 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        private void FileUpload(string strManageNo)
        {
            try
            {
                // 첨부파일 저장경로정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                //화일서버 연결정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                // 삭제/저장 첨부파일 설정
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile_Save = new System.Collections.ArrayList();
                System.Collections.ArrayList arrFile_Del = new System.Collections.ArrayList();

                // 점겸결과 첨부파일
                if (this.uTextInsResultFile.Text.Contains(":\\"))
                {
                    System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextInsResultFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                                                        + strManageNo + "-"
                                                                        + "Result" + "-"
                                                                        + fileDoc.Name;

                    if (System.IO.File.Exists(strUploadFile))
                        System.IO.File.Delete(strUploadFile);

                    System.IO.File.Copy(this.uTextInsResultFile.Text, strUploadFile);
                    arrFile_Save.Add(strUploadFile);
                }

                // 이상발생에 대한 조치사항 첨부파일
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (this.uGridDetail.Rows[i].Hidden.Equals(true))
                    {
                        if (this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(this.uTextPlantCode.Text + "-" +
                                                                                                this.uTextManageNo.Text + "-"))
                        {
                            arrFile_Del.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString());
                        }
                    }
                    else
                    {
                        if (this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                                                                + strManageNo + "-"
                                                                                + this.uGridDetail.Rows[i].RowSelectorNumber.ToString() + "-"
                                                                                + fileDoc.Name;

                            if (System.IO.File.Exists(strUploadFile))
                                System.IO.File.Delete(strUploadFile);

                            System.IO.File.Copy(this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString(), strUploadFile);
                            arrFile_Save.Add(strUploadFile);
                        }
                    }
                }

                // 저장정보 있는경우 첨부파일 저장
                if (arrFile_Save.Count > 0)
                {
                    fileAtt.mfInitSetSystemFileInfo(arrFile_Save, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


                // 삭제정보 있는경우 첨부파일 삭제
                if (arrFile_Del.Count > 0)
                {
                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile_Del
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();
                }

                for (int i = 0; i < arrFile_Save.Count; i++)
                {
                    System.IO.File.Delete(arrFile_Save[i].ToString());
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 삭제 메소드(전체)
        /// </summary>
        private void FileDelete()
        {
            try
            {
                // 첨부파일 저장경로정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                // 점검결과 첨부파일
                if (this.uTextInsResultFile.Text.Contains(this.uTextPlantCode.Text) &&
                    this.uTextInsResultFile.Text.Contains(this.uTextManageNo.Text) &&
                    !this.uTextInsResultFile.Text.Contains(":\\"))
                {
                    arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextInsResultFile.Text);
                }

                // 이상조치 첨부파일
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(this.uTextPlantCode.Text) &&
                        this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(this.uTextManageNo.Text) &&
                        !this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\"))
                    {
                        arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString());
                    }
                }

                if (arrFile.Count > 0)
                {
                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextManageNo.Clear();
                this.uTextCheckUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCheckUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uDateCheckDate.Value = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                this.uComboEquipLoc.Value = string.Empty;
                this.uTextSpec.Clear();
                this.uTextPlantCode.Text = m_resSys.GetString("SYS_PLANTCODE");

                this.uComboInsEquipItem1.Value = string.Empty;
                this.uComboInsEquipItem2.Value = string.Empty;
                this.uComboInsEquipItem3.Value = string.Empty;
                this.uComboInsEquipItem4.Value = string.Empty;
                this.uComboInsEquipItem5.Value = string.Empty;
                this.uComboInsWorkTableItem1.Value = string.Empty;
                this.uComboInsWorkTableItem2.Value = string.Empty;
                this.uComboInsWorkTableItem3.Value = string.Empty;
                this.uComboInsWorkTableItem4.Value = string.Empty;
                this.uComboInsResItem1.Value = string.Empty;
                this.uComboInsResItem2.Value = string.Empty;
                this.uComboInsResItem3.Value = string.Empty;

                this.uTextInsEtcItem1.Clear();
                this.uTextInsEtcItem2.Clear();
                this.uTextInsEtcItem3.Clear();
                this.uTextInsEtcItem4.Clear();
                this.uTextInsEtcItem5.Clear();
                this.uTextInsEtcItem6.Clear();
                this.uTextInsEtcItem7.Clear();
                this.uTextInsEtcItem8.Clear();
                this.uTextInsEtcItem9.Clear();
                this.uTextInsEtcItem10.Clear();
                this.uTextInsEtcItem11.Clear();
                this.uTextInsEtcItem12.Clear();
                this.uTextInsEtcItem13.Clear();
                this.uTextInsEtcItem14.Clear();
                this.uTextInsEtcItem15.Clear();
                this.uTextInsEtcItem16.Clear();
                this.uTextInsEtcItem17.Clear();
                this.uTextInsEtcItem18.Clear();
                this.uTextInsEtcItem19.Clear();
                this.uTextInsEtcItem20.Clear();

                this.uTextInsResultFile.Clear();
                this.uTextEtcDesc.Clear();

                this.uGridDetail.SetDataBinding(SetDataInfo_Deail(), string.Empty);

                this.uTextCheckUserID.Focus();
                this.uTextCheckUserID.SelectAll();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정_헤더
        /// </summary>
        /// <returns></returns>
        private DataTable SetDataInfo_Header()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("CheckDate", typeof(string));
                dtRtn.Columns.Add("CheckTime", typeof(string));
                dtRtn.Columns.Add("CheckUserID", typeof(string));
                dtRtn.Columns.Add("DocSpec", typeof(string));
                dtRtn.Columns.Add("EquipLocCode", typeof(string));
                dtRtn.Columns.Add("InsEquipITem1", typeof(string));
                dtRtn.Columns.Add("InsEquipITem2", typeof(string));
                dtRtn.Columns.Add("InsEquipITem3", typeof(string));
                dtRtn.Columns.Add("InsEquipITem4", typeof(string));
                dtRtn.Columns.Add("InsEquipITem5", typeof(string));
                dtRtn.Columns.Add("InsWorkTableItem1", typeof(string));
                dtRtn.Columns.Add("InsWorkTableItem2", typeof(string));
                dtRtn.Columns.Add("InsWorkTableItem3", typeof(string));
                dtRtn.Columns.Add("InsWorkTableItem4", typeof(string));
                dtRtn.Columns.Add("InsResItem1", typeof(string));
                dtRtn.Columns.Add("InsResItem2", typeof(string));
                dtRtn.Columns.Add("InsResItem3", typeof(string));
                dtRtn.Columns.Add("InsEtcItem1", typeof(string));
                dtRtn.Columns.Add("InsEtcItem2", typeof(string));
                dtRtn.Columns.Add("InsEtcItem3", typeof(string));
                dtRtn.Columns.Add("InsEtcItem4", typeof(string));
                dtRtn.Columns.Add("InsEtcItem5", typeof(string));
                dtRtn.Columns.Add("InsEtcItem6", typeof(string));
                dtRtn.Columns.Add("InsEtcItem7", typeof(string));
                dtRtn.Columns.Add("InsEtcItem8", typeof(string));
                dtRtn.Columns.Add("InsEtcItem9", typeof(string));
                dtRtn.Columns.Add("InsEtcItem10", typeof(string));
                dtRtn.Columns.Add("InsEtcItem11", typeof(string));
                dtRtn.Columns.Add("InsEtcItem12", typeof(string));
                dtRtn.Columns.Add("InsEtcItem13", typeof(string));
                dtRtn.Columns.Add("InsEtcItem14", typeof(string));
                dtRtn.Columns.Add("InsEtcItem15", typeof(string));
                dtRtn.Columns.Add("InsEtcItem16", typeof(string));
                dtRtn.Columns.Add("InsEtcItem17", typeof(string));
                dtRtn.Columns.Add("InsEtcItem18", typeof(string));
                dtRtn.Columns.Add("InsEtcItem19", typeof(string));
                dtRtn.Columns.Add("InsEtcItem20", typeof(string));
                dtRtn.Columns.Add("InsResultFile", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정_상세
        /// </summary>
        /// <returns></returns>
        private DataTable SetDataInfo_Deail()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("Check", typeof(bool));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("InspectItem", typeof(string));
                dtRtn.Columns.Add("ActionDesc", typeof(string));
                dtRtn.Columns.Add("ActionFile", typeof(string));
                dtRtn.Columns.Add("ActionDate", typeof(string));
                dtRtn.Columns.Add("ActionUserID", typeof(string));

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 저장용 헤더 데이터 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_SaveInfo_Header()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn = SetDataInfo_Header();
                DataRow drH = dtRtn.NewRow();
                drH["PlantCode"] = this.uTextPlantCode.Text;
                drH["ManageNo"] = this.uTextManageNo.Text;
                drH["CheckDate"] = Convert.ToDateTime(this.uDateCheckDate.Value).ToString("yyyy-MM-dd hh:mm:ss");
                //drH["CheckTime"] = string.Empty;
                drH["CheckUserID"] = this.uTextCheckUserID.Text;
                drH["DocSpec"] = this.uTextSpec.Text;
                drH["EquipLocCode"] = this.uComboEquipLoc.Value.ToString();
                drH["InsEquipItem1"] = this.uComboInsEquipItem1.Value.ToString();
                drH["InsEquipItem2"] = this.uComboInsEquipItem2.Value.ToString();
                drH["InsEquipItem3"] = this.uComboInsEquipItem3.Value.ToString();
                drH["InsEquipItem4"] = this.uComboInsEquipItem4.Value.ToString();
                drH["InsEquipItem5"] = this.uComboInsEquipItem5.Value.ToString();
                drH["InsWorkTableItem1"] = this.uComboInsWorkTableItem1.Value.ToString();
                drH["InsWorkTableItem2"] = this.uComboInsWorkTableItem2.Value.ToString();
                drH["InsWorkTableItem3"] = this.uComboInsWorkTableItem3.Value.ToString();
                drH["InsWorkTableItem4"] = this.uComboInsWorkTableItem4.Value.ToString();
                drH["InsResItem1"] = this.uComboInsResItem1.Value.ToString();
                drH["InsResItem2"] = this.uComboInsResItem2.Value.ToString();
                drH["InsResItem3"] = this.uComboInsResItem3.Value.ToString();
                drH["InsEtcItem1"] = this.uTextInsEtcItem1.Text;
                drH["InsEtcItem2"] = this.uTextInsEtcItem2.Text;
                drH["InsEtcItem3"] = this.uTextInsEtcItem3.Text;
                drH["InsEtcItem4"] = this.uTextInsEtcItem4.Text;
                drH["InsEtcItem5"] = this.uTextInsEtcItem5.Text;
                drH["InsEtcItem6"] = this.uTextInsEtcItem6.Text;
                drH["InsEtcItem7"] = this.uTextInsEtcItem7.Text;
                drH["InsEtcItem8"] = this.uTextInsEtcItem8.Text;
                drH["InsEtcItem9"] = this.uTextInsEtcItem9.Text;
                drH["InsEtcItem10"] = this.uTextInsEtcItem10.Text;
                drH["InsEtcItem11"] = this.uTextInsEtcItem11.Text;
                drH["InsEtcItem12"] = this.uTextInsEtcItem12.Text;
                drH["InsEtcItem13"] = this.uTextInsEtcItem13.Text;
                drH["InsEtcItem14"] = this.uTextInsEtcItem14.Text;
                drH["InsEtcItem15"] = this.uTextInsEtcItem15.Text;
                drH["InsEtcItem16"] = this.uTextInsEtcItem16.Text;
                drH["InsEtcItem17"] = this.uTextInsEtcItem17.Text;
                drH["InsEtcItem18"] = this.uTextInsEtcItem18.Text;
                drH["InsEtcItem19"] = this.uTextInsEtcItem19.Text;
                drH["InsEtcItem20"] = this.uTextInsEtcItem20.Text;
                if (this.uTextInsResultFile.Text.Contains(":\\") && this.uTextManageNo.Text != "")
                {
                    System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextInsResultFile.Text);
                    drH["InsResultFile"] = this.uTextPlantCode.Text + "-" + this.uTextManageNo.Text + "-" + "Result-" + fileDoc.Name;
                }
                else if (this.uTextInsResultFile.Text.Contains(":\\") && this.uTextManageNo.Text == "")
                {
                    System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextInsResultFile.Text);
                    drH["InsResultFile"] = fileDoc.Name;
                }
                else
                {
                    drH["InsResultFile"] = this.uTextInsResultFile.Text;
                }
                drH["EtcDesc"] = this.uTextEtcDesc.Text;
                dtRtn.Rows.Add(drH);
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 저장용 상세정보 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_SaveInfo_Detail()
        {
            DataTable dtRtn = new DataTable();
            try
            {                
                this.uGridDetail.UpdateData();
                ((DataTable)this.uGridDetail.DataSource).AcceptChanges();
                DataTable dtOrigin = (DataTable)this.uGridDetail.DataSource;

                DataTable dtTemp = dtOrigin.Copy();
                dtRtn = dtOrigin.Clone();
                DataRow[] _drSaves = dtTemp.Select("Seq <> 0");

                foreach (DataRow _dr in _drSaves)
                {
                    if (_dr["ActionFile"].ToString().Contains(":\\"))
                    {
                        System.IO.FileInfo fileDoc = new System.IO.FileInfo(_dr["ActionFile"].ToString());
                        _dr["ActionFile"] = fileDoc.Name;
                    }
                    dtRtn.ImportRow(_dr);
                }

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 헤더 상세정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderData(string strPlantCode, string strManageNo, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.StaticCheckH), "StaticCheckH");
                QRPQAT.BL.QATENV.StaticCheckH clsH = new QRPQAT.BL.QATENV.StaticCheckH();
                brwChannel.mfCredentials(clsH);

                DataTable dtHeader = clsH.mfReadStaticCheckH_Detail(strPlantCode, strManageNo, strLang);
                if (dtHeader.Rows.Count > 0)
                {
                    this.uTextPlantCode.Text = dtHeader.Rows[0]["PlantCode"].ToString();
                    this.uTextManageNo.Text = dtHeader.Rows[0]["ManageNo"].ToString();
                    this.uDateCheckDate.Value = Convert.ToDateTime(dtHeader.Rows[0]["CheckDate"]).ToString("yyyy-MM-dd hh:mm:ss");
                    this.uTextCheckUserID.Text = dtHeader.Rows[0]["CheckUserID"].ToString();
                    this.uTextCheckUserName.Text = dtHeader.Rows[0]["CheckUserName"].ToString();
                    this.uTextSpec.Text = dtHeader.Rows[0]["DocSpec"].ToString();
                    this.uComboEquipLoc.Value = dtHeader.Rows[0]["EquipLocCode"].ToString();
                    this.uComboInsEquipItem1.Value = dtHeader.Rows[0]["InsEquipItem1"].ToString();
                    this.uComboInsEquipItem2.Value = dtHeader.Rows[0]["InsEquipItem2"].ToString();
                    this.uComboInsEquipItem3.Value = dtHeader.Rows[0]["InsEquipItem3"].ToString();
                    this.uComboInsEquipItem4.Value = dtHeader.Rows[0]["InsEquipItem4"].ToString();
                    this.uComboInsEquipItem5.Value = dtHeader.Rows[0]["InsEquipItem5"].ToString();
                    this.uComboInsWorkTableItem1.Value = dtHeader.Rows[0]["InsWorkTableItem1"].ToString();
                    this.uComboInsWorkTableItem2.Value = dtHeader.Rows[0]["InsWorkTableItem2"].ToString();
                    this.uComboInsWorkTableItem3.Value = dtHeader.Rows[0]["InsWorkTableItem3"].ToString();
                    this.uComboInsWorkTableItem4.Value = dtHeader.Rows[0]["InsWorkTableItem4"].ToString();
                    this.uComboInsResItem1.Value = dtHeader.Rows[0]["InsResItem1"].ToString();
                    this.uComboInsResItem2.Value = dtHeader.Rows[0]["InsResItem2"].ToString();
                    this.uComboInsResItem3.Value = dtHeader.Rows[0]["InsResItem3"].ToString();
                    this.uTextInsEtcItem1.Text = dtHeader.Rows[0]["InsEtcItem1"].ToString();
                    this.uTextInsEtcItem2.Text = dtHeader.Rows[0]["InsEtcItem2"].ToString();
                    this.uTextInsEtcItem3.Text = dtHeader.Rows[0]["InsEtcItem3"].ToString();
                    this.uTextInsEtcItem4.Text = dtHeader.Rows[0]["InsEtcItem4"].ToString();
                    this.uTextInsEtcItem5.Text = dtHeader.Rows[0]["InsEtcItem5"].ToString();
                    this.uTextInsEtcItem6.Text = dtHeader.Rows[0]["InsEtcItem6"].ToString();
                    this.uTextInsEtcItem7.Text = dtHeader.Rows[0]["InsEtcItem7"].ToString();
                    this.uTextInsEtcItem8.Text = dtHeader.Rows[0]["InsEtcItem8"].ToString();
                    this.uTextInsEtcItem9.Text = dtHeader.Rows[0]["InsEtcItem9"].ToString();
                    this.uTextInsEtcItem10.Text = dtHeader.Rows[0]["InsEtcItem10"].ToString();
                    this.uTextInsEtcItem11.Text = dtHeader.Rows[0]["InsEtcItem11"].ToString();
                    this.uTextInsEtcItem12.Text = dtHeader.Rows[0]["InsEtcItem12"].ToString();
                    this.uTextInsEtcItem13.Text = dtHeader.Rows[0]["InsEtcItem13"].ToString();
                    this.uTextInsEtcItem14.Text = dtHeader.Rows[0]["InsEtcItem14"].ToString();
                    this.uTextInsEtcItem15.Text = dtHeader.Rows[0]["InsEtcItem15"].ToString();
                    this.uTextInsEtcItem16.Text = dtHeader.Rows[0]["InsEtcItem16"].ToString();
                    this.uTextInsEtcItem17.Text = dtHeader.Rows[0]["InsEtcItem17"].ToString();
                    this.uTextInsEtcItem18.Text = dtHeader.Rows[0]["InsEtcItem18"].ToString();
                    this.uTextInsEtcItem19.Text = dtHeader.Rows[0]["InsEtcItem19"].ToString();
                    this.uTextInsEtcItem20.Text = dtHeader.Rows[0]["InsEtcItem20"].ToString();
                    this.uTextInsResultFile.Text = dtHeader.Rows[0]["InsResultFile"].ToString();
                    this.uTextEtcDesc.Text = dtHeader.Rows[0]["EtcDesc"].ToString();
                }
                else
                {

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Search_DetailData(string strPlantCode, string strManageNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATENV.StaticCheckD), "StaticCheckD");
                QRPQAT.BL.QATENV.StaticCheckD clsD = new QRPQAT.BL.QATENV.StaticCheckD();
                brwChannel.mfCredentials(clsD);

                DataTable dtDetail = clsD.mfReadStaticCheckD(strPlantCode, strManageNo, strLang);

                this.uGridDetail.SetDataBinding(dtDetail, string.Empty);

                if (dtDetail.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDetail, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridStaticCheck.Height = 90;
                }
                else
                {
                    Point point = new Point(0, 830);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridStaticCheck.Height = 750;
                    for (int i = 0; i < uGridStaticCheck.Rows.Count; i++)
                    {
                        uGridStaticCheck.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 점검자 팝업 이벤트
        void uTextCheckUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frmUser.ShowDialog();

                this.uTextCheckUserID.Text = frmUser.UserID;
                this.uTextCheckUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 점검자 TextBox 키이벤트
        void uTextCheckUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (!this.uTextCheckUserID.Equals(string.Empty))
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        String strUserID = this.uTextCheckUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(m_resSys.GetString("SYS_PLANTCODE"), strUserID);

                        if (strRtnUserName.Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextCheckUserID.Clear();
                            this.uTextCheckUserName.Clear();
                        }

                        else
                        {
                            this.uTextCheckUserName.Text = strRtnUserName;
                        }
                    }
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextCheckUserID.TextLength <= 1 || this.uTextCheckUserID.SelectedText == this.uTextCheckUserID.Text)
                    {
                        this.uTextCheckUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 점검결과 첨부파일 버튼 이벤트
        void uTextInsResultFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    // 업로드할 파일 찾는 다이얼로그 창
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        // 기존등록된파일 있으면 첨부파일 삭제
                        // 점검결과 첨부파일
                        if (this.uTextInsResultFile.Text.Contains(this.uTextPlantCode.Text) &&
                            this.uTextInsResultFile.Text.Contains(this.uTextManageNo.Text) &&
                            !this.uTextInsResultFile.Text.Contains(":\\"))
                        {
                            // 첨부파일 저장경로정보 가져오기
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextInsResultFile.Text);

                            if (arrFile.Count > 0)
                            {
                                //화일서버 연결정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                                fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrFile
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.mfFileUploadNoProgView();
                            }
                        }

                        this.uTextInsResultFile.Text = openFile.FileName;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    if (!this.uTextManageNo.Text.Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        // 파일서버에서 불러올수 있는 파일인지 체크
                        if (this.uTextInsResultFile.Text.Contains(":\\"))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001135", "M000359",
                                                     Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else
                        {
                            System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                            saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                            saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                            saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                            saveFolder.Description = "Download Folder";

                            if (saveFolder.ShowDialog() == DialogResult.OK)
                            {
                                string strSaveFolder = saveFolder.SelectedPath + "\\";

                                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                                //화일서버 연결정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                                //첨부파일 저장경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                                //첨부파일 Download하기
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                arrFile.Add(this.uTextInsResultFile.Text);

                                fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder,
                                                                       dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.ShowDialog();

                                // 파일 실행시키기
                                System.Diagnostics.Process.Start(strSaveFolder + "\\" + this.uTextInsResultFile.Text);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 다운로드 버튼 이벤트
        void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridDetail.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\")
                            || this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        // 화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        // 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                        for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (!this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\") &&
                                    !string.IsNullOrEmpty(this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString()))
                                {
                                    arrFile.Add(this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        if (arrFile.Count > 0)
                        {
                            fileAtt.ShowDialog();
                        }

                        // 폴더 열기
                        System.Diagnostics.Process.Start(strSaveFolder);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 이벤트
        void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value))
                    {
                        if (!this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\") &&
                            !this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Equals(string.Empty))
                        {
                            this.uGridDetail.Rows[i].Cells["Seq"].Value = 0;
                            this.uGridDetail.Rows[i].Hidden = true;
                        }
                        else
                        {
                            this.uGridDetail.Rows[i].Delete(false);
                            i--;
                        }
                    }
                    else
                        this.uGridDetail.Rows[i].Cells["Seq"].Value = this.uGridDetail.Rows[i].RowSelectorNumber;
                }

                #region 첨부파일 삭제 주석처리

                    /*
                    this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    // 첨부파일 저장경로정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value))
                        {
                            if (this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(this.uTextPlantCode.Text) &&
                                this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(this.uTextManageNo.Text) &&
                                !this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString().Contains(":\\"))
                            {
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridDetail.Rows[i].Cells["ActionFile"].Value.ToString());

                                this.uGridDetail.Rows[i].Cells["ActionFile"].Value = string.Empty;
                                this.uGridDetail.Rows[i].Cells["ActionFile"].SetValue(string.Empty, false);
                            }
                            this.uGridDetail.Rows[i].Cells["Seq"].Value = 0;
                            this.uGridDetail.Rows[i].Hidden = true;
                        }
                    }

                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                            , arrFile
                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileUploadNoProgView();
                    }
                     * */

                    #endregion
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 상세그리드 Change 이벤트
        void uGridDetail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 조회 List Row DoubleClick 시 해당 상세정보 조회
        void uGridStaticCheck_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 클릭된 행 고정
                e.Row.Fixed = true;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strManageNo = e.Row.Cells["ManageNo"].Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");

                // 프로그래스 팝업창 생성
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                Search_HeaderData(strPlantCode, strManageNo, strLang);
                Search_DetailData(strPlantCode, strManageNo, strLang);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
        }

        // 상세정보 그리드 첨부파일 버튼 클릭 이벤트
        void uGridDetail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ActionFile"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*"; //"Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        e.Cell.Value = openFile.FileName;
                        uGridDetail_CellChange(sender, e);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uGridDetail_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.ToString().Equals("ActionFile"))
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0032");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                            arrFile.Add(e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uGridDetail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridDetail.ActiveCell == null)
                    return;
                else if (this.uGridDetail.ActiveCell.Column.Key.Equals("ActionUserID"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                        string strPlantCode = this.uTextPlantCode.Text;
                        string strUserID = this.uGridDetail.ActiveCell.Value.ToString();

                        string strUserName = GetUserName(strPlantCode, strUserID);
                        if (strUserName.Equals(string.Empty))
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001113", "M000621"
                                                        , Infragistics.Win.HAlign.Right);
                        }
                        this.uGridDetail.ActiveCell.Row.Cells["ActionUserName"].Value = strUserName;
                    }
                    else if (e.KeyCode == Keys.Back)
                    {
                        this.uGridDetail.ActiveCell.Value = string.Empty;
                        this.uGridDetail.ActiveCell.Row.Cells["ActionUserName"].Value = string.Empty;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    } 
}
