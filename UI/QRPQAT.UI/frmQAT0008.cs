﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 검교정 관리                                           */
/* 프로그램ID   : frmQAT0008.cs                                         */
/* 프로그램명   : 검교정 등록                                           */
/* 작성자       : 최영진                                                */
/* 작성일자     : 2011-07-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-16 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
//--파일업로드를위한 참조
using System.IO;

namespace QRPQAT.UI
{
    public partial class frmQAT0008 : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmQAT0008()
        {
            InitializeComponent();
        }

        private void frmQAT0008_Activated(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, true, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQAT0008_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("내부검증 등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            // 초기화 Method
            InitText();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitGroupBox();
            InitButton();

            // ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트박스 초기화
        /// </summary>
        private void InitText()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                //검색조건 Label
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                wLabel.mfSetLabel(this.uLabelSearchModelName, "차기검증일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //등록상세 Label
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);               
                wLabel.mfSetLabel(this.uLabelInCorrectNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityType, "검증유형", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelModel, "Model", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMade, "제작사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCarryInDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityUser, "검증자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelVerityDate, "검증일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelTemperature, "온도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHumidity, "습도", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVerityBasic, "검증근거", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNextVerityDate, "차기교정일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelVerityResult, "검증결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFileName, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelConfirmDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelConfirmUserID, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "최종완료", m_resSys.GetString("SYS_FONTNAME"), true, false); 
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "PlantCode", "PlantName", dtPlant);

                //기본값 지정
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                // -- 검증결과 콤보 --
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtComCode = clsCommonCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboVerifyResult, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtComCode);

                //-- 검증유형 콤보
                DataTable dtComType = clsCommonCode.mfReadCommonCode("C0040", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboVerityType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "선택", "ComCode", "ComCodeName", dtComType);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 검교정 등록 정보 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridInVerityH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "InVerityNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityNumber", "일련번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityTarget", "검증대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityTargetName", "검증대상", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityType", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityTargetCode", "검증대상코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityTargetName", "검증대상명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityDate", "검증일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityUserID", "검증자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityUserName", "검증자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "DeptName", "검증자부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityResult", "검증결과", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "VerityBasic", "검증근거", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "Temperature", "온도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "Humidity", "습도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "NextVerityDate", "차기검증일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "MakerCompany", "제작회사", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "ModelName", "모델명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityH, 0, "FileName", "FileName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridInVerityH, 0);

                // 폰트 설정
                this.uGridInVerityH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridInVerityH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridInVerityD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "MeasureToolCode", "사용계측기코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "VerityTargetName", "사용계측기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "MakerCompany", "제작회사", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "ModelName", "MODEL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "LastInspectDate", "최근교정일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInVerityD, 0, "InspectNextDate", "차기교정일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");     
                
                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridInVerityD, 0);
               
                // 폰트 설정
                this.uGridInVerityD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridInVerityD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridInVerityD.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "사용계측기", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBoxComplete, GroupBoxType.INFO, "승인정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxComplete.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxComplete.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar 기능

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //system ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                //-- 검색하기전 ExpandGroup박스를 닫는다 --//
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //--관리번호에 값이 있을경우 초기화 시킨다 --//
                if (this.uTextInVerityNo.Text != "")
                {
                    InitExpand();
                }

                //--FromDate가 ToDate보다 클시 메세지박스
                if (Convert.ToDateTime(this.uDateFromDate.Value).Date > Convert.ToDateTime(this.uDateToDate.Value).Date)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000774", Infragistics.Win.HAlign.Right);

                    this.uDateFromDate.DropDown();
                    return;
                }


                //-- 검색하기위해 값 변수에 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strTargetCode = "";
                string strTarget = "";

                if (this.uOptionSearchCheck.Value != null)
                {
                    strTarget = this.uOptionSearchCheck.Value.ToString();
                }

                //-- 코드나 이름중에 하나라도 공백이면 검색조건을 공백으로 보낸다 -- //
                if (this.uTextSearchMeasureToolCode.Text == "" || this.uTextSearchMeasureToolName.Text == "")
                {
                    strTargetCode = "";
                }
                else
                {
                    strTargetCode = this.uTextCode.Text;
                }

                string strFromDate = Convert.ToDateTime(this.uDateFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateToDate.Value).ToString("yyyy-MM-dd");


                //---팝업창을 띄운다 ---//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //---커서를변경한다 --//
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                //-- BL 호출 --
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityH), "InVerityH");
                QRPQAT.BL.QATVRT.InVerityH clsInVerityH = new QRPQAT.BL.QATVRT.InVerityH();
                brwChannel.mfCredentials(clsInVerityH);

                //매서드 호출
                DataTable dtInVerity = clsInVerityH.mfReadInVerityH(strPlantCode, strTarget, strTargetCode, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //--- 그리드에 데이터 바인드
                this.uGridInVerityH.DataSource = dtInVerity;
                this.uGridInVerityH.DataBind();

                /////////////

                //--커서를 기본값으로 변경 --//
                this.MdiParent.Cursor = Cursors.Default;

                //--팝업창을 닫는다 --//
                m_ProgressPopup.mfCloseProgressPopup(this);


                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtInVerity.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInVerityH, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //SystemResource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력 사항 확인
                //--------------------- 필수 입력 사항 확인 -----------------------//
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);

                    
                    return;
                }
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000824", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uComboPlant.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                if(this.uOptionCheck.CheckedIndex.Equals(-1))  //this.uOptionCheck.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000176", Infragistics.Win.HAlign.Right);

                    return;
                }
                if(this.uTextCode.Text.Trim() == "" || this.uTextCodeName.Text =="" )
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000177", Infragistics.Win.HAlign.Right);

                    this.uTextCode.Focus();
                    return;
                }
                //else if(this.uComboVerityType.Value.ToString().Trim() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검사유형을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uComboVerityType.DropDown();
                //    return;
                //}
                if (this.uDateVerityDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000225", Infragistics.Win.HAlign.Right);

                    this.uDateVerityDate.DropDown();
                    return;
                }
                if (this.uTextVerityUserID.Text == "" || this.uTextName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000226", Infragistics.Win.HAlign.Right);

                    this.uTextVerityUserID.Focus();
                    return;
                }
                if(this.uDateNextVerifyDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M001128", Infragistics.Win.HAlign.Right);

                    this.uDateNextVerifyDate.DropDown();
                    return;
                }

                if (this.uCheckCompleteFlag.Checked)
                {
                    if (this.uComboVerifyResult.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001273", Infragistics.Win.HAlign.Right);

                        this.uComboVerifyResult.DropDown();
                        return;
                    }

                    if (this.uTextConfirmUserID.Text.Equals(string.Empty) || this.uTextConfirmUserName.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000770", Infragistics.Win.HAlign.Right);

                        this.uTextConfirmUserID.Focus();
                        return;
                    }
                    if (this.uDateConfirmDate.Value == null || this.uDateConfirmDate.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001299", Infragistics.Win.HAlign.Right);

                        this.uDateConfirmDate.DropDown();
                        return;
                    }
                }
                if (this.uTextFileUpLoad.Text.Contains("/")
                    || this.uTextFileUpLoad.Text.Contains("#")
                    || this.uTextFileUpLoad.Text.Contains("*")
                    || this.uTextFileUpLoad.Text.Contains("<")
                    || this.uTextFileUpLoad.Text.Contains(">"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }


                #endregion

                #region 정보저장
                //----- 헤더 값 저장 ---//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityH), "InVerityH");
                QRPQAT.BL.QATVRT.InVerityH clsInVerityH = new QRPQAT.BL.QATVRT.InVerityH();
                brwChannel.mfCredentials(clsInVerityH);

                DataTable dtInVerityH = clsInVerityH.mfDataSetInfo();

                DataRow drH;

                drH = dtInVerityH.NewRow();

                drH["PlantCode"] = this.uComboPlant.Value.ToString();
                drH["InVerityNo"] = this.uTextInVerityNo.Text;
                drH["VerityNumber"] = this.uTextVerityNumber.Text;
                drH["VerityTarget"] = this.uOptionCheck.Value.ToString();
                //drH["VerityType"] = this.uComboVerityType.Value.ToString();
                drH["VerityTargetCode"] = this.uTextCode.Text;
                drH["VerityUserID"] = this.uTextVerityUserID.Text;
                drH["VerityDate"] = Convert.ToDateTime(this.uDateVerityDate.Value).ToString("yyyy-MM-dd");
                drH["Temperature"] = this.uTextTemperature.Value.ToString();
                drH["Humidity"] = this.uTextHumidity.Value.ToString();
                drH["NextVerityDate"] = Convert.ToDateTime(this.uDateNextVerifyDate.Value).ToString("yyyy-MM-dd");
                drH["VerityBasic"] = this.uTextVerifyBasic.Text;
                drH["VerityResult"] = this.uComboVerifyResult.Value.ToString();
                drH["EtcDesc"] = this.uTextEtcDesc.Text;
                drH["ConfirmDate"] = this.uDateConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                
                if (!this.uTextConfirmUserID.Text.Equals(string.Empty) && !this.uTextConfirmUserName.Text.Equals(string.Empty))
                    drH["ConfirmUserID"] = this.uTextConfirmUserID.Text;

                string strServer = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                if (strServer.Contains("10.61.61.71") || strServer.Contains("10.61.61.73"))
                    drH["SendMDM"] = "T";

                drH["CompleteFlag"] = this.uCheckCompleteFlag.Checked == true ? "T" : "F";

                dtInVerityH.Rows.Add(drH);


                //첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                string strUploadFile = "";

                //-- 정보내용중 :\\ 있는경우 새로등록되거나 변경된 파일인식
                if (this.uTextFileUpLoad.Text.Contains(":\\"))
                {

                    strUploadFile = "NE";
                    FileInfo fileDoc = new FileInfo(this.uTextFileUpLoad.Text);

                    dtInVerityH.Rows[0]["FileName"] = "-" + fileDoc.Name;

                }
                //경로가 없는 경우
                else
                    dtInVerityH.Rows[0]["FileName"] = this.uTextFileUpLoad.Text;

                



                //-------------상세 저장 -----------//
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityD), "InVerityD");
                QRPQAT.BL.QATVRT.InVerityD clsInVerityD = new QRPQAT.BL.QATVRT.InVerityD();
                brwChannel.mfCredentials(clsInVerityD);

                DataTable dtInVerityD = clsInVerityD.mfSetDataInfo();

                DataTable dtInVerityDel = clsInVerityD.mfSetDataInfo();

                if (this.uGridInVerityD.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridInVerityD.Rows.Count; i++)
                    {
                         //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridInVerityD.ActiveCell = this.uGridInVerityD.Rows[0].Cells[0];
                        
                        if (this.uGridInVerityD.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridInVerityD.Rows[i].Hidden == false)
                            {
                                DataRow drD;
                                drD = dtInVerityD.NewRow();
                                drD["Seq"] = this.uGridInVerityD.Rows[i].Cells["Seq"].Value.ToString();
                                drD["MeasureToolCode"] = this.uGridInVerityD.Rows[i].Cells["MeasureToolCode"].Value.ToString();
                                dtInVerityD.Rows.Add(drD);
                            }
                            else
                            {
                                if (this.uGridInVerityD.Rows[i].Cells["Seq"].Value.ToString() != "0")
                                {
                                    DataRow drDel;
                                    drDel = dtInVerityDel.NewRow();
                                    drDel["Seq"] = this.uGridInVerityD.Rows[i].Cells["Seq"].Value.ToString();
                                    drDel["MeasureToolCode"] = this.uGridInVerityD.Rows[i].Cells["MeasureToolCode"].Value.ToString();
                                    drDel["InverityNo"] = this.uTextInVerityNo.Text;
                                    drDel["VerityNumber"] = this.uTextVerityNumber.Text;
                                    dtInVerityDel.Rows.Add(drDel);
                                }
                            }
                        }
                    }
                }

                #endregion

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////

                // --------------- 저장 여부 메세지 박스 ----------------

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //--- 팝업창을 띄운다 ---//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    //--------커서변경---------//
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    string strErrRtn = clsInVerityH.mfSaveInVerityH(dtInVerityH, dtInVerityD, dtInVerityDel, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));                    
                    /////////////
                    
                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // -------- //
                    #region 첨부화일정보저장
                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    //내부검증등록 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0017");

                    
                    if (strUploadFile != "")
                    {
                        string strGetValue = ErrRtn.mfGetReturnValue(0);
                        string strGetValue1 = ErrRtn.mfGetReturnValue(1);

                        //화일이름변경(관리번호+일련번호+화일명)하여 복사하기//
                        FileInfo fileDoc = new FileInfo(this.uTextFileUpLoad.Text);

                        strUploadFile = fileDoc.DirectoryName + "\\" + strGetValue + "-" + strGetValue1 + "-" + fileDoc.Name;
                    
                        //변경한 화일이 있으면 삭제하기
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                        //변경한 화일이름으로 복사하기
                        File.Copy(this.uTextFileUpLoad.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }

                    #endregion

                    //------커서변경 -----//
                    this.MdiParent.Cursor = Cursors.Default;

                    // 팝업창을 닫는다 //
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    
                   
                    // 처리결과에 따라 메세지를 띄운다 //
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        if (strUploadFile != "")
                        {
                            //Upload정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();
                        }
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strErr = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErr = "M000953";
                        else
                            strErr = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", strErr,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //SystemResource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항
                //--------------------- 필수 입력 사항 확인 -----------------------//
                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextInVerityNo.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);


                    return;
                }
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000824", Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uComboPlant.Value.ToString().Trim() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                //else if (this.uOptionCheck.Value == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검사대상을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    return;
                //}
                //else if (this.uTextCode.Text.Trim() == "" || this.uTextCodeName.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검사대상을 입력해주세요", Infragistics.Win.HAlign.Right);

                //    this.uTextCode.Focus();
                //    return;
                //}
                //else if (this.uComboVerityType.Value.ToString().Trim() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검사유형을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uComboVerityType.DropDown();
                //    return;
                //}
                //else if (this.uDateVerityDate.Value.ToString() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검증일을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uDateVerityDate.DropDown();
                //    return;
                //}
                //else if (this.uTextVerityUserID.Text == "" || this.uTextName.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "검증자를 입력해주세요", Infragistics.Win.HAlign.Right);

                //    this.uTextVerityUserID.Focus();
                //    return;
                //}
                //else if (this.uDateNextVerifyDate.Value.ToString() == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //              "확인창", "필수입력사항확인", "차기검증일을 선택해주세요", Infragistics.Win.HAlign.Right);

                //    this.uDateNextVerifyDate.DropDown();
                //    return;
                //}

                #endregion

                // 삭제를 위한 값 저장 //
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strInVerityNo = this.uTextInVerityNo.Text;
                string strVerityNumber = this.uTextVerityNumber.Text;

                //----삭제 여부를 묻는 메세지박스를 띄운다 -----//
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //-----팝업창을 띄운다 -----//
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");

                    //---커서를 변경한다..
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    //--BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityH), "InVerityH");
                    QRPQAT.BL.QATVRT.InVerityH clsInVerityH = new QRPQAT.BL.QATVRT.InVerityH();
                    brwChannel.mfCredentials(clsInVerityH);

                    string strErrRtn = clsInVerityH.mfDeleteInVerity(strPlantCode, strInVerityNo, strVerityNumber);

                    /////////////

                    //---DeCodeing ---//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--커서를 기본값으로 변경한다.
                    this.MdiParent.Cursor = Cursors.Default;
                    //-- 팝업창을 닫는다.--
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라 메세지 박스를 보여준다.
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        mfCreate();
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                InitExpand();

                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {

                    this.uGroupBoxContentsArea.Expanded = true;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {
                return;

                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "출력중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                ////처리 로직//

                ///////////////

                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridInVerityH);
                grd.mfDownLoadGridToExcel(this.uGridInVerityD);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 간단한 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInVerityH.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInVerityH.Height = 740;
                    for (int i = 0; i < uGridInVerityH.Rows.Count; i++)
                    {
                        uGridInVerityH.Rows[i].Fixed = false;
                    }
                    InitExpand();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridInVerityD_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 자동 행삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridInVerityD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void uGridInVerityD_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        #endregion

        #region 긴 이벤트

        //검색조건에 있는 검증대상텍스트의 에디터버튼을 클릭하였을 때
        private void uTextSearchMeasureToolCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

               

                //옵션컨트롤이 선택이 안됐을 경우 메세지박스를 띄운다.
                if (this.uOptionSearchCheck.Value == null || this.uOptionSearchCheck.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000882", "M000224", Infragistics.Win.HAlign.Right);
                    //this.uOptionCheck.Select();
                    return;
                }

                //--옵션컨트롤의 선택한 DataValue 저장
                string strCheck = this.uOptionSearchCheck.Value.ToString();

                //--  코드 , 명 , 변수 선언
                
                string strCode = "";
                string strName = "";
                string strPOPPlant = "";
                //--계측기를 선택후 버튼 클릭시 팝업창을 띄운다.
                if (strCheck == "E")
                {
                    frmPOP0006 frmTool = new frmPOP0006();
                    frmTool.PlantCode = uComboSearchPlant.Value.ToString();
                    frmTool.ShowDialog();

                    strPOPPlant = frmTool.PlantCode;
                    strCode = frmTool.MeasureToolCode;
                    strName = frmTool.MeasureToolName;

                }
                //--설비를 선택 후 버튼 클릭시 팝업창을 띄운다.
                else if (strCheck == "M")
                {
                    frmPOP0005 frmEquip = new frmPOP0005();
                    frmEquip.PlantCode = uComboSearchPlant.Value.ToString();
                    frmEquip.ShowDialog();

                    strPOPPlant = frmEquip.PlantCode;
                    strCode = frmEquip.EquipCode;
                    strName = frmEquip.EquipName;

                }

                if (this.uComboSearchPlant.Value.ToString() != strPOPPlant)
                {
                    if (this.uComboSearchPlant.Value.ToString() != "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000882", "M000909", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

                //--각컨트롤에 해당 정보를 넣는다.
                this.uTextSearchMeasureToolCode.Text = strCode;
                this.uTextSearchMeasureToolName.Text = strName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //Expand안의 검증대상텍스트의 에디터버튼을 클릭하였을 때
        private void uTextCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uTextCode.ReadOnly == false)
                {

                    //옵션컨트롤이 선택이 안됐을 경우 메세지박스를 띄운다.
                    if (this.uOptionCheck.Value == null || this.uOptionCheck.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                     "M001264", "M000882", "M000224", Infragistics.Win.HAlign.Right);
                        //this.uOptionCheck.Select();
                        return;
                    }

                    //--옵션컨트롤의 선택한 DataValue 저장
                    string strCheck = this.uOptionCheck.Value.ToString();

                    //-- 공장이 공백일시 메세지 박스를 띄우고 리턴 시킨다.--/
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                      "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }

                    //-- 공장 값저장  코드 , 명 , 변수 선언
                    string strPlant = this.uComboPlant.Value.ToString();
                    string strCode = "";
                    string strName = "";
                    string strModel = "";
                    string strSerial = "";
                    string strInDate = "";
                    string strMakerCompany = "";
                    string strPOPPlant = "";
                    //--계측기를 선택후 버튼 클릭시 팝업창을 띄운다.
                    if (strCheck == "E")
                    {
                        frmPOP0006 frmTool = new frmPOP0006();
                        frmTool.PlantCode = uComboSearchPlant.Value.ToString();
                        frmTool.ShowDialog();

                        strPOPPlant = frmTool.PlantCode;
                        strCode = frmTool.MeasureToolCode;
                        strName = frmTool.MeasureToolName;
                        strModel = frmTool.ModelName;
                        strSerial = frmTool.SerialNo;
                        strInDate = frmTool.AcquireDate;
                        strMakerCompany = frmTool.MakerCompany;
                    }
                    //--설비를 선택 후 버튼 클릭시 팝업창을 띄운다.
                    else if (strCheck == "M")
                    {
                        frmPOP0005 frmEquip = new frmPOP0005();
                        frmEquip.PlantCode = uComboSearchPlant.Value.ToString();
                        frmEquip.ShowDialog();

                        strPOPPlant = frmEquip.PlantCode;
                        strCode = frmEquip.EquipCode;
                        strName = frmEquip.EquipName;
                        strModel = frmEquip.ModelName;
                        strSerial = frmEquip.SerialNo;
                        strInDate = frmEquip.GRDate;
                        strMakerCompany = frmEquip.VendorName;
                    }

                    // 선택한 검증대상의공장과 콤보 공장이 다를경우
                    if (strPOPPlant != "" && strPlant != strPOPPlant)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    //--각컨트롤에 해당 정보를 넣는다.
                    this.uTextCode.Text = strCode;
                    this.uTextCodeName.Text = strName;
                    this.uTextModel.Text = strModel;
                    this.uTextSerialNo.Text = strSerial;
                    this.uTextCarryInDate.Text = strInDate;
                    this.uTextVendor.Text = strMakerCompany;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        //왼쪽버튼을 클릭하였을때 0으로바뀐다.
        private void uTextInit_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 현재 년도로 초기화
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //스핀버튼을 클릭하였을때 증,감소가 이루어진다.
        private void uTextSpin_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && 0<intTemp) //Int32.MinValue
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //헤더 그리드를 더블 클릭하였을때 발생한다.
        private void uGridInVerityH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //클릭한 셀이 공백이 아닐경우 
                if (grd.mfCheckCellDataInRow(this.uGridInVerityH, 0, e.Cell.Row.Index))
                    return;

                //-- Expandbox가 닫혀져있으면 펼친다 --
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    //---선택한줄 고정 ---//
                    e.Cell.Row.Fixed = true;
                }
                    
                //--그리드 셀의 값을 추출하여 각컨트롤에 넣는다.
                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();            //공장
                string strInVerityNo = e.Cell.Row.Cells["InVerityNo"].Value.ToString();        //검증번호
                string strVerityNumber = e.Cell.Row.Cells["VerityNumber"].Value.ToString();    //일련번호

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityH), "InVerityH");
                QRPQAT.BL.QATVRT.InVerityH clsInVerityH = new QRPQAT.BL.QATVRT.InVerityH();
                brwChannel.mfCredentials(clsInVerityH);

                DataTable dtInVerityH = clsInVerityH.mfReadInVerityH_Detail(strPlantCode, strInVerityNo, strVerityNumber, m_resSys.GetString("SYS_LANG"));

                if (dtInVerityH.Rows.Count <= 0)
                    return;
                    
                //---------내부검증 헤더 ------------//
                this.uComboPlant.Value = strPlantCode;                                              //공장
                this.uTextInVerityNo.Text = strInVerityNo;                                          //검증번호
                this.uTextVerityNumber.Text = strVerityNumber;                                      //일련번호
                this.uOptionCheck.Value = dtInVerityH.Rows[0]["VerityTarget"].ToString();        //검증대상
                //this.uComboVerityType.Value = e.Cell.Row.Cells["VerityType"].Value.ToString();          //검증유형
                this.uTextCode.Text = dtInVerityH.Rows[0]["VerityTargetCode"].ToString();        //검증대상코드
                this.uTextCodeName.Text = dtInVerityH.Rows[0]["VerityTargetName"].ToString();    //검증대상명
                this.uDateVerityDate.Value = dtInVerityH.Rows[0]["VerityDate"].ToString();       //검증일
                this.uTextVerityUserID.Text = dtInVerityH.Rows[0]["VerityUserID"].ToString();    //검증자
                this.uTextName.Text = dtInVerityH.Rows[0]["VerityUserName"].ToString();          //검증자명
                this.uTextDept.Text = dtInVerityH.Rows[0]["DeptName"].ToString();                //검증자부서
                this.uComboVerifyResult.Value = dtInVerityH.Rows[0]["VerityResult"].ToString();  //검증결과
                this.uTextVerifyBasic.Text = dtInVerityH.Rows[0]["VerityBasic"].ToString();      //검증근거
                this.uTextEtcDesc.Text = dtInVerityH.Rows[0]["EtcDesc"].ToString();              //비고
                this.uTextTemperature.Value = dtInVerityH.Rows[0]["Temperature"].ToString();      //온도
                this.uTextHumidity.Value = dtInVerityH.Rows[0]["Humidity"].ToString();           //습도
                this.uDateNextVerifyDate.Value = dtInVerityH.Rows[0]["NextVerityDate"].ToString();//차기검증일
                this.uTextVendor.Text = dtInVerityH.Rows[0]["MakerCompany"].ToString();          //제작회사
                this.uTextCarryInDate.Text = dtInVerityH.Rows[0]["GRDate"].ToString();           //입고일
                this.uTextModel.Text = dtInVerityH.Rows[0]["ModelName"].ToString();              //모델명
                this.uTextSerialNo.Text = dtInVerityH.Rows[0]["SerialNo"].ToString();            //serialNo
                this.uTextFileUpLoad.Text = dtInVerityH.Rows[0]["FileName"].ToString();          //파일경로

                this.uTextConfirmUserID.Text = dtInVerityH.Rows[0]["ConfirmUserID"].ToString();
                this.uTextConfirmUserName.Text = dtInVerityH.Rows[0]["ConfirmUserName"].ToString();
                this.uDateConfirmDate.Value = dtInVerityH.Rows[0]["ConfirmDate"].ToString();

                if (dtInVerityH.Rows[0]["CompleteFlag"].ToString().Equals("T"))
                {
                    this.uCheckCompleteFlag.Enabled = false;
                    this.uCheckCompleteFlag.Checked = true;

                }
                else
                {
                    this.uCheckCompleteFlag.Enabled = true;
                    this.uCheckCompleteFlag.Checked = false;
                }

                //--공장콤보 , 설비&계측기 입력불가 처리 --//
                this.uComboPlant.ReadOnly = true;
                this.uOptionCheck.Enabled = false;
                this.uOptionCheck.Appearance.BackColor = Color.White;
                this.uTextCode.ReadOnly = true;
                this.uComboVerityType.ReadOnly = true;


                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATVRT.InVerityD), "InVerityD");
                QRPQAT.BL.QATVRT.InVerityD clsInVerityD = new QRPQAT.BL.QATVRT.InVerityD();
                brwChannel.mfCredentials(clsInVerityD);

                //-------- 내부검증 상세 -------------///
                DataTable dtInVerityD = clsInVerityD.mfReadInVerityD(strPlantCode, strInVerityNo, strVerityNumber, m_resSys.GetString("SYS_LANG"));

                this.uGridInVerityD.DataSource = dtInVerityD;
                this.uGridInVerityD.DataBind();

                if (dtInVerityD.Rows.Count > 0)
                {
                    grd.mfSetAutoResizeColWidth(this.uGridInVerityD, 0);
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용계측기 버튼을 클릭 하였을 때 발생함
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridInVerityD_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
               
                //공장이 공백이였을 때 메세지 박스를 띄운다.
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }

                // 계측기 팝업창을 띄운다 //
                frmPOP0006 frmTool = new frmPOP0006();
                frmTool.PlantCode = uComboPlant.Value.ToString();
                frmTool.ShowDialog();

                // 공장과 선택한 계측기의 공장이 틀리면 메세지 박스를 띄운다.
                if (this.uComboPlant.Value.ToString() != frmTool.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M001230", "M000269", Infragistics.Win.HAlign.Right);


                    return;
                }

                // 해당 셀에 값을 넣는다.
                e.Cell.Row.Cells["MeasureToolCode"].Value = frmTool.MeasureToolCode;
                e.Cell.Row.Cells["VerityTargetName"].Value = frmTool.MeasureToolName;
                e.Cell.Row.Cells["MakerCompany"].Value = frmTool.MakerCompany;
                e.Cell.Row.Cells["ModelName"].Value = frmTool.ModelName;
                e.Cell.Row.Cells["SerialNo"].Value = frmTool.SerialNo;
                e.Cell.Row.Cells["LastInspectDate"].Value = frmTool.LastInspectDate;
                e.Cell.Row.Cells["InspectNextDate"].Value = frmTool.InspectNextDate;

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검증자 에디트버튼을 클릭하였을때 유저정보창을 띄운다
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextVerityUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPQAT.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextVerityUserID.Text = frmUser.UserID;
                this.uTextName.Text = frmUser.UserName;
                this.uTextDept.Text = frmUser.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검증자 아이디를 입력 후 엔터를 누르면 이름 자동입력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextVerityUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    this.uTextName.Text = "";
                    this.uTextDept.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    //검증자 공장코드 저장
                    string strVerityUserID = this.uTextVerityUserID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strVerityUserID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextVerityUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000280", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strVerityUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextName.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDept.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001115", "M000883", Infragistics.Win.HAlign.Right);
                        this.uTextName.Clear();
                        this.uTextDept.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uTextConfirmUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextConfirmUserName.Text.Equals(string.Empty))
                this.uTextConfirmUserName.Clear();
        }

        /// <summary>
        /// 승인자 직접입력조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextConfirmUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //아이디지울시 이름도 지움
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                    this.uTextConfirmUserName.Clear();
                

                if (e.KeyData == Keys.Enter)
                {
                    //검증자 공장코드 저장
                    string strUserID = this.uTextConfirmUserID.Text;
                    string strPlantCode = this.uComboPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strUserID.Equals(string.Empty))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextConfirmUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000280", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboPlant.DropDown();
                        return;
                    }

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                        this.uTextConfirmUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001115", "M000883", Infragistics.Win.HAlign.Right);
                        this.uTextConfirmUserName.Clear();
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 승인자 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextConfirmUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                QRPQAT.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextConfirmUserID.Text = frmUser.UserID;
                this.uTextConfirmUserName.Text = frmUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        ///  첨부파일 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextFileUpLoad_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;

                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            //첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001139", "M001141", "M001451",
                                                Infragistics.Win.HAlign.Right);
                            return;
                        }

                        this.uTextFileUpLoad.Text = strImageFile;

                    }
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextFileUpLoad.Text.Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0017");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextFileUpLoad.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFileUpLoad.Text);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void frmQAT0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uTextCode.Text = "";
                this.uTextCodeName.Text = "";
                this.uOptionCheck.Value = null;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmQAT0008_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 행삭제 버튼을 클릭하였을 때 발생
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                //-- 그리드 정보가 있을시 체크컬럼에 체크가된 줄만 찾아서 줄을 숨김다 --//
                if (this.uGridInVerityD.Rows.Count > 0)
                {
                    for (int i = 0;i< this.uGridInVerityD.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridInVerityD.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridInVerityD.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        /// <summary>
        /// ExpandBox안에 텍스트초기화와 그리드초기화
        /// </summary>
        private void InitExpand()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.ReadOnly = false;
                this.uOptionCheck.Enabled = true;
                this.uTextCode.ReadOnly = false;
                this.uComboVerityType.ReadOnly = false;
                //---텍스트박스

                this.uTextInVerityNo.Text = "";
                this.uTextVerityNumber.Text = "";

                this.uTextCode.Text = "";
                this.uTextCodeName.Text = "";

                this.uTextModel.Text = "";
                this.uTextSerialNo.Text = "";
                this.uTextCarryInDate.Text = "";
                this.uTextVendor.Text = "";


                this.uTextVerityUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextDept.Text = m_resSys.GetString("SYS_DEPTNAME");

                this.uTextVerifyBasic.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextFileUpLoad.Text = "";
                

                this.uTextTemperature.Value = 0;
                this.uTextHumidity.Value = 0;

                this.uDateConfirmDate.Value = DateTime.Now.Date;
                this.uTextConfirmUserID.Clear();
                this.uTextConfirmUserName.Clear();
                this.uCheckCompleteFlag.Checked = false;
                this.uCheckCompleteFlag.Enabled = true;

                //-- Date콤보
                this.uDateVerityDate.Value = DateTime.Now;
                this.uDateNextVerifyDate.Value = DateTime.Now;

                //--콤보
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboVerityType.Value = "";
                this.uComboVerifyResult.Value = "";

                //그리드 
                //-- 전체 행을 선택 하여 삭제한다 --//
                this.uGridInVerityD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridInVerityD.Rows.All);
                this.uGridInVerityD.DeleteSelectedRows(false); 
               
                //검증유형 콤보 삭제에 따른 처리(Visible = false)
                this.uComboVerityType.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }

    }
}
       
       
