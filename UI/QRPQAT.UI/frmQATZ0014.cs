﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmQATZ0014.cs                                         */
/* 프로그램명   : 계측기정보등록                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

// 파일첨부
using System.IO;

namespace QRPQAT.UI
{
    public partial class frmQATZ0014 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0014()
        {
            InitializeComponent();
        }

        private void frmQATZ0014_Activated(object sender, EventArgs e)
        {
            // ToolBar 활성화

            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0014_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            titleArea.mfSetLabelText("계측기정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitValue();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitGroupBox();
            // ExtandableGroupBox 설정
            this.uGroupBoxContentsArea.Expanded = false;

            // 그리드 높이 설정
            this.uGridMeasureToolList.Height = 730;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// TextBox 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                this.uTextAcquireReason.MaxLength = 500;
                this.uTextMakerCompany.MaxLength = 50;
                this.uTextMakerNo.MaxLength = 50;
                this.uTextMeasureToolCode.MaxLength = 10;
                this.uTextMeasureToolName.MaxLength = 50;
                this.uTextMeasureToolNameCh.MaxLength = 50;
                this.uTextMeasureToolNameEn.MaxLength = 50;
                this.uTextModelName.MaxLength = 100;
                this.uTextSerialNo.MaxLength = 50;
                this.uTextSpec.MaxLength = 100;

                this.uTextSearchMeasureToolCode.MaxLength = 10;
                this.uTextSearchMeasureToolName.MaxLength = 50;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMTType, "계측기분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchInspectNextDate, "차기교정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchAcquireDept, "취득부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMeasurename, "기기명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMeasureToolCode, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchInspectNextDateOver, "차기교정일초과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                label.mfSetLabel(uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMTLType, "계측기대분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMTMType, "계측기중분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMTType, "계측기분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMeasureToolCode, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMeasureToolName, "기기명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMeasureToolNameCh, "기기명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMeasureToolNameEn, "기기명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelModelName, "모델명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMakerCompany, "제작회사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMakerNo, "제작번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelAcquireDate, "취득일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelAcquireAmt, "취득금액", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSerialNo, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelAcquireDeptCode, "취득부서", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelAcquireReason, "취득근거", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelInspectPeriod, "검교정주기", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelInspectPeriodUnitCode, "검교정단위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelInspectNextDate, "차기교정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelLastInspectDate, "마지막교정일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelDiscardDate, "폐기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelDiscardFlag, "폐기여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelDiscardReason, "폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMTFileName, "파일첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 2012-11-16 권종구 추가
                label.mfSetLabel(uLabelClass, "Class", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPurpose, "Purpose", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelResult, "검증결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelAllowance, "공차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                // 2012-11-16
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region Header

                // InitGeneralGrid
                grd.mfInitGeneralGrid(this.uGridMeasureToolList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MeasureToolCode", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MeasureToolName", "기기명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MeasureToolNameCh", "계측기명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                //    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MeasureToolNameEn", "계측기명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                //    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MTTypeCode", "계측기분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MTTypeName", "계측기분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                //    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                //    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "WriteUserID", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                //    , 200, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");


                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "ModelName", "모델명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MakerCompany", "제작회사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "AcquireDate", "취득일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                //    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "AcquireDeptName", "취득부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "MakerNo", "제작번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                //    , 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "LastInspectDate", "마지막교정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "InspectNextDate", "차기교정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "Purpose", "Purpose", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                   , 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                   , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "Class", "Class", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                   , 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                   , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "InspectPeriod", "검교정주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "InspectPeriodUnitCode", "검교정단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "Allowance", "공차", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "Result", "검증결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false
                    , 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");  // 2012-12-04 추가

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "DiscardDate", "폐기일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false
                //    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "DiscardFlag", "폐기여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                //    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //, "", "", "");

                //grd.mfSetGridColumn(this.uGridMeasureToolList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                //    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                //    , "", "", "");

                //폰트설정
                uGridMeasureToolList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMeasureToolList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //////// Set UseFlag DropDown
                //////QRPBrowser brwChannel = new QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //////QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //////brwChannel.mfCredentials(clsComCode);

                //////DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMeasureToolList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                //////// Set Discard DropDown
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //////QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //////brwChannel.mfCredentials(clsComCode);

                //////DataTable dtDiscardFlag = clsComCode.mfReadCommonCode("C0004", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMeasureToolList, 0, "DiscardFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtDiscardFlag);

                //////// Set Plant DropDown
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //////brwChannel.mfCredentials(clsPlant);

                //////ValueList = new DataTable();
                //////ValueList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMeasureToolList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", ValueList);

                #endregion

                #region History

                // InitGeneralGrid
                grd.mfInitGeneralGrid(this.uGridHistory, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridHistory, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "GubunName", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "MeasureToolCode", "계측기코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                   Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "CompleteDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "UserID", "검증인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "UserName", "검증인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "Result", "검,교정결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor combo = new WinComboEditor();
                DataTable dt = new DataTable();

                // Plant BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea PlantComboBox
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dt);
                // ContentsArea PlantComboBox
                combo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dt);

                // UseFlag ComboBox
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "T", "", "선택"
                    , "ComCode", "ComCodeName", dt);

                // Discard ComboBox
                dt = clsComCode.mfReadCommonCode("C0004", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboDiscardFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "F", "", "선택"
                    , "ComCode", "ComCodeName", dt);

                // PeriodUnit ComboBox
                dt = clsComCode.mfReadCommonCode("C0003", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboInspectPeriodUnitCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);
                // 계측기 대분류
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUComCode);

                dt = clsUComCode.mfReadUserCommonCode("QUA", "U0009", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboMTLType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);
                //취득 부서

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsdept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsdept);

                dt = clsdept.mfReadSYSDeptForCombo(this.uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboAcquireDeptCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "DeptCode", "DeptName", dt);

                // --------- 2012-11-16 추가 ---- //
                // 검증결과 합/부 콤보박스 
                dt = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboResult, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);

                // Class 콤보박스 
                dt = clsComCode.mfReadCommonCode("C0079", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboClass, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);
                // --------- 2012-11-16 -------- //

                clsComCode.Dispose();
                clsdept.Dispose();
                clsPlant.Dispose();
                dt.Dispose();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBoxHistory, GroupBoxType.LIST, "이력", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxHistory.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxHistory.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 이벤트

        /// <summary>
        /// SearchArea PlantComboBox 값에 따라 MTTypeComboBox 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtMTType = new DataTable();
                DataTable dtDept = new DataTable();

                // ComboBox Clear
                this.uComboSearchMTType.Items.Clear();
                this.uComboSearchAcquireDeptCode.Items.Clear();

                if (strPlantCode != "")
                {
                    // BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                    QRPMAS.BL.MASQUA.MTType mtType = new QRPMAS.BL.MASQUA.MTType();
                    brwChannel.mfCredentials(mtType);

                    dtMTType = mtType.mfReadMASMTTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // AcquireDeptCode ComboBox
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                    QRPSYS.BL.SYSUSR.Dept clsdept = new QRPSYS.BL.SYSUSR.Dept();
                    brwChannel.mfCredentials(clsdept);

                    dtDept = clsdept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                combo.mfSetComboEditor(this.uComboSearchMTType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "MTTypeCode", "MTTypeName", dtMTType);

                // 취득부서

                combo.mfSetComboEditor(this.uComboSearchAcquireDeptCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "DeptCode", "DeptName", dtDept);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ContentsArea PlantComboBox 값에 따라 MTTypeComboBox 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();
                String strPlantCode = this.uComboPlant.Value.ToString();
                DataTable dtMTType = new DataTable();
                DataTable dtDept = new DataTable();

                // ComboBox Clear
                this.uComboMTType.Items.Clear();
                this.uComboAcquireDeptCode.Items.Clear();

                if (strPlantCode != "")
                {
                    // BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                    QRPMAS.BL.MASQUA.MTType mtType = new QRPMAS.BL.MASQUA.MTType();
                    brwChannel.mfCredentials(mtType);

                    dtMTType = mtType.mfReadMASMTTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // AcquireDeptCode ComboBox
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                    QRPSYS.BL.SYSUSR.Dept clsdept = new QRPSYS.BL.SYSUSR.Dept();
                    brwChannel.mfCredentials(clsdept);

                    dtDept = clsdept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }
                combo.mfSetComboEditor(this.uComboMTType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "MTTypeCode", "MTTypeName", dtMTType);

                combo.mfSetComboEditor(this.uComboAcquireDeptCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        ////---------------------------------------------------------20111018 안병도 수정 begin
        /// <summary>
        /// ContentsArea uComboMTLType 값에 따라 uComboMTMType 값 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboMTLType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor combo = new WinComboEditor();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strMTLTypeCode = this.uComboMTLType.Value.ToString();

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUComCode);

                DataTable dtMTMTypeCode = clsUComCode.mfReadUserCommonCodeMMType("QUA", "U0010", strMTLTypeCode, m_resSys.GetString("SYS_LANG"));
                //                grd.mfSetGridCellValueList(this.uGridMttypeGroupList, e.Cell.Row.Index, "MTMTypeCode", "", "선택", dtMTMTypeCode);
                combo.mfSetComboEditor(this.uComboMTMType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtMTMTypeCode);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        ////---------------------------------------------------------20111018 안병도 수정 begin
        /// <summary>
        /// ContentsArea uComboMTMType 값에 따라 uComboMTType 값 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboMTMType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strMTMTypeCode = this.uComboMTMType.Value.ToString();
                DataTable dtMTType = new DataTable();


                // ComboBox Clear
                this.uComboMTType.Items.Clear();


                if (strMTMTypeCode != "")
                {
                    // BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                    QRPMAS.BL.MASQUA.MTType mtType = new QRPMAS.BL.MASQUA.MTType();
                    brwChannel.mfCredentials(mtType);

                    dtMTType = mtType.mfReadMASMTTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"), strMTMTypeCode);

                }
                combo.mfSetComboEditor(this.uComboMTType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "MTTypeCode", "MTTypeName", dtMTType);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }


        ////---------------------------------------------------------20111011 안병도 수정 begin
        /// <summary>
        /// ContentsArea uComboMTType 값에 따라 uNumInspectPeriod,uComboInspectPeriodUnitCode 값 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboMTType_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                //------------------------


                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strMTTypeCode = this.uComboMTType.Value.ToString();

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                QRPMAS.BL.MASQUA.MeasureTool measureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                brwChannel.mfCredentials(measureTool);

                DataTable dtMeasureTool = measureTool.mfReadMASMTInspectPeriod(strPlantCode, strMTTypeCode);

                for (int i = 0; i < dtMeasureTool.Rows.Count; i++)
                {

                    this.uNumInspectPeriod.Value = dtMeasureTool.Rows[i]["InspectPeriod"].ToString();
                    this.uComboInspectPeriodUnitCode.Value = dtMeasureTool.Rows[i]["InspectPeriodUnitCode"].ToString();

                }

                // PK 편집 불가 생태로

                this.uNumInspectPeriod.ReadOnly = true;
                this.uComboInspectPeriodUnitCode.ReadOnly = true;
                //--------------------------

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        ////---------------------------------------------------------20111011 안병도 수정 end

        /// <summary>
        /// Grid Double Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridMeasureToolList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                String strMTTypeCode = e.Cell.Row.Cells["MTTypeCode"].Value.ToString();
                String strMeasureToolCode = e.Cell.Row.Cells["MeasureToolCode"].Value.ToString();

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                QRPMAS.BL.MASQUA.MeasureTool measureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                brwChannel.mfCredentials(measureTool);

                DataTable dtMeasureTool = measureTool.mfReadMASMeasureTool_Detail(strPlantCode, strMTTypeCode, strMeasureToolCode);
               
                for (int i = 0; i < dtMeasureTool.Rows.Count; i++)
                {



                    this.uComboPlant.Value = dtMeasureTool.Rows[i]["PlantCode"].ToString();
                    this.uTextMeasureToolCode.Text = dtMeasureTool.Rows[i]["MeasureToolCode"].ToString();
                    this.uTextMeasureToolName.Text = dtMeasureTool.Rows[i]["MeasureToolName"].ToString();
                    this.uTextMeasureToolNameCh.Text = dtMeasureTool.Rows[i]["MeasureToolNameCh"].ToString();
                    this.uTextMeasureToolNameEn.Text = dtMeasureTool.Rows[i]["MeasureToolNameEn"].ToString();

                    this.uComboMTLType.Value = dtMeasureTool.Rows[i]["MTLTypeCode"].ToString();
                    this.uComboMTMType.Value = dtMeasureTool.Rows[i]["MTMTypeCode"].ToString();
                    this.uComboMTType.Value = dtMeasureTool.Rows[i]["MTTypeCode"].ToString();
                    this.uTextSpec.Text = dtMeasureTool.Rows[i]["Spec"].ToString();
                    this.uTextWriteID.Text = dtMeasureTool.Rows[i]["WriteUserID"].ToString();
                    // UserName 검색 함수 호출
                    this.uTextWriteName.Text = GetUserName(this.uComboPlant.Value.ToString(), this.uTextWriteID.Text);
                    this.uTextModelName.Text = dtMeasureTool.Rows[i]["ModelName"].ToString();
                    this.uTextMakerCompany.Text = dtMeasureTool.Rows[i]["MakerCompany"].ToString();
                    this.uTextMakerNo.Text = dtMeasureTool.Rows[i]["MakerNo"].ToString();
                    this.uDateAcquireDate.Value = dtMeasureTool.Rows[i]["AcquireDate"].ToString();
                    if (dtMeasureTool.Rows[i]["AcquireAmt"] != null || dtMeasureTool.Rows[i]["AcquireAmt"] != DBNull.Value || dtMeasureTool.Rows[i]["AcquireAmt"].ToString() != string.Empty)
                        this.uNumAcquireAmt.Value = dtMeasureTool.Rows[i]["AcquireAmt"].ToString();
                    else
                        this.uNumAcquireAmt.Value = 0;
                    this.uTextSerialNo.Text = dtMeasureTool.Rows[i]["SerialNo"].ToString();
                    this.uComboAcquireDeptCode.Value = dtMeasureTool.Rows[i]["AcquireDeptCode"].ToString();
                    this.uTextAcquireReason.Text = dtMeasureTool.Rows[i]["AcquireReason"].ToString();
                    if (dtMeasureTool.Rows[i]["InspectPeriod"] != null)
                        this.uNumInspectPeriod.Value = dtMeasureTool.Rows[i]["InspectPeriod"].ToString();
                    else
                        this.uNumInspectPeriod.Value = 0;
                    this.uComboInspectPeriodUnitCode.Value = dtMeasureTool.Rows[i]["InspectPeriodUnitCode"].ToString();
                    this.uDateInspectNextDate.Value = dtMeasureTool.Rows[i]["InspectNextDate"].ToString();
                    this.uDateLastInspectDate.Value = dtMeasureTool.Rows[i]["LastInspectDate"].ToString();
                    this.uDateDiscardDate.Value = dtMeasureTool.Rows[i]["DiscardDate"].ToString();
                    this.uComboDiscardFlag.Value = dtMeasureTool.Rows[i]["DiscardFlag"].ToString();
                    this.richTextEditor.SetDocumentText(dtMeasureTool.Rows[i]["DiscardReason"].ToString());
                    this.uTextMTFileName.Text = dtMeasureTool.Rows[i]["MTFileName"].ToString();
                    this.uComboUseFlag.Value = dtMeasureTool.Rows[i]["UseFlag"].ToString();

                    // 2012-11-16 추가
                    this.uComboClass.Value = dtMeasureTool.Rows[i]["Class"];                    // Class
                    this.uComboResult.Value = dtMeasureTool.Rows[i]["Result"];                  // 검증결과
                    this.uTextCustomer.Text = dtMeasureTool.Rows[i]["Customer"].ToString();     // 고객
                    this.uTextAllowance.Text = dtMeasureTool.Rows[i]["Allowance"].ToString();   // 공차
                    this.uTextPurpose.Text = dtMeasureTool.Rows[i]["Purpose"].ToString();       // Purpose
                    // 2012-11-16


                }
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                // 이력조회
                DataTable dtMoveHist = measureTool.mfReadMASMeasureTool_MoveHist(strPlantCode, strMeasureToolCode, m_resSys.GetString("SYS_LANG"));

                this.uGridHistory.DataSource = dtMoveHist;
                this.uGridHistory.DataBind();

                // PK 편집 불가 생태로

                this.uComboPlant.ReadOnly = true;
                this.uComboMTLType.ReadOnly = true;
                this.uComboMTMType.ReadOnly = true;
                this.uComboMTType.ReadOnly = true;
                this.uTextMeasureToolCode.ReadOnly = true;
                this.uTextMeasureToolCode.Appearance.BackColor = Color.Gainsboro;
                this.uNumInspectPeriod.ReadOnly = true;
                this.uComboInspectPeriodUnitCode.ReadOnly = true;

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// uGroupBoxContentsArea 펼침상태 변화 이벤트

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 150);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridMeasureToolList.Height = 40;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridMeasureToolList.Height = 715;

                    for (int i = 0; i < uGridMeasureToolList.Rows.Count; i++)
                    {
                        uGridMeasureToolList.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 조회를 위한 변수

                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();
                string strsearchMTTypeCode = this.uComboSearchMTType.Value.ToString();
                string strSearchMeasureToolCode = this.uTextSearchMeasureToolCode.Text;
                string strSearchMeasureToolName = this.uTextSearchMeasureToolName.Text;
                string strSearchAcquireDeptCode = this.uComboSearchAcquireDeptCode.Value.ToString();
                string strSearchInspectNextDateFrom = Convert.ToDateTime(this.uDateSearchstrInspectNextDateFrom.Value).ToString("yyyy-MM-dd");
                string strSearchInspectNextDateTo = Convert.ToDateTime(this.uDateSearchInspectNextDateTo.Value).ToString("yyyy-MM-dd");
                string strSearchNextDateOverCheck = this.uCheckSearchInspectNextDateOver.Checked.ToString().ToUpper().Substring(0, 1);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                QRPMAS.BL.MASQUA.MeasureTool measureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                brwChannel.mfCredentials(measureTool);

                // Method 호출
                DataTable dtMeasureTool = measureTool.mfReadMASMeasureTool(strSearchPlantCode, strsearchMTTypeCode, strSearchMeasureToolCode
                                                                          , strSearchMeasureToolName, strSearchAcquireDeptCode
                                                                          , strSearchInspectNextDateFrom, strSearchInspectNextDateTo
                                                                          , strSearchNextDateOverCheck, m_resSys.GetString("SYS_LANG"));

                // Binding
                this.uGridMeasureToolList.DataSource = dtMeasureTool;
                this.uGridMeasureToolList.DataBind();

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridMeasureToolList, 0);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtMeasureTool.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                // ContentGroupBox 접은상태로

                this.uGroupBoxContentsArea.Expanded = false;

                for (int i = 0; i < this.uGridMeasureToolList.Rows.Count; i++)
                {
                    if (this.uGridMeasureToolList.Rows[i].Cells["InspectNextDate"].Value != null && this.uGridMeasureToolList.Rows[i].Cells["InspectNextDate"].Value != DBNull.Value &&
                        this.uGridMeasureToolList.Rows[i].Cells["InspectNextDate"].Value.ToString() != string.Empty)
                    {
                        if (Convert.ToDateTime(this.uGridMeasureToolList.Rows[i].Cells["InspectNextDate"].Value).CompareTo(Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))) < 0)
                        {
                            this.uGridMeasureToolList.Rows[i].Appearance.BackColor = Color.Salmon;
                        }
                        else if (Convert.ToDateTime(this.uGridMeasureToolList.Rows[i].Cells["InspectNextDate"].Value).CompareTo(DateTime.Now.AddDays(14)) <= 0)
                        {
                            this.uGridMeasureToolList.Rows[i].Appearance.BackColor = Color.Yellow;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);

                    //this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    //BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                    QRPMAS.BL.MASQUA.MeasureTool measureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                    brwChannel.mfCredentials(measureTool);

                    // 매개변수로 넘겨줄 DataTable 설정
                    DataTable dtMeaureTool = measureTool.mfSetDataInfo();

                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboMTType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000246", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboMTType.DropDown();
                        return;
                    }
                    //else if (this.uTextMeasureToolCode.Text == "")
                    //{
                    //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                    , "확인창", "필수입력사항 확인", "계측기코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                    //    // Focus
                    //    this.uTextMeasureToolCode.Focus();
                    //    return;
                    //}
                    else if (this.uTextMeasureToolName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000245", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextMeasureToolName.Focus();
                        return;
                    }
                    ////else if (this.uTextMeasureToolNameCh.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "계측기명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextMeasureToolNameCh.Focus();
                    ////    return;
                    ////}
                    ////else if (this.uTextMeasureToolNameEn.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "계측기명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextMeasureToolNameEn.Focus();
                    ////    return;
                    ////}
                    else if (this.uComboUseFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000619", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboUseFlag.DropDown();
                        return;
                    }
                    else if (this.uComboDiscardFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001193", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboDiscardFlag.DropDown();
                        return;
                    }
                    else if (this.uComboAcquireDeptCode.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001176", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboAcquireDeptCode.DropDown();
                        return;
                    }
                    else if (this.uTextMTFileName.Text.Contains("/")
                        || this.uTextMTFileName.Text.Contains("#")
                        || this.uTextMTFileName.Text.Contains("*")
                        || this.uTextMTFileName.Text.Contains("<")
                        || this.uTextMTFileName.Text.Contains(">"))
                    {
                        // 첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                          , "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else if (this.uDateInspectNextDate.Value == null || this.uDateInspectNextDate.Value.ToString().Equals(string.Empty))
                    {
                        // 차기교정일을 선택해주세요.
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001322", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uDateInspectNextDate.DropDown();
                        return;

                    }
                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        DataRow row = dtMeaureTool.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["MeasureToolCode"] = this.uTextMeasureToolCode.Text;
                        row["MeasureToolName"] = this.uTextMeasureToolName.Text;
                        row["MeasureToolNameCh"] = this.uTextMeasureToolNameCh.Text;
                        row["MeasureToolNameEn"] = this.uTextMeasureToolNameEn.Text;
                        row["MTTypeCode"] = this.uComboMTType.Value.ToString();
                        row["Spec"] = this.uTextSpec.Text;
                        row["WriteUserID"] = this.uTextWriteID.Text;
                        row["ModelName"] = this.uTextModelName.Text;
                        row["MakerCompany"] = this.uTextMakerCompany.Text;
                        row["MakerNo"] = this.uTextMakerNo.Text;
                        if (this.uDateAcquireDate.Value != null)
                            row["AcquireDate"] = this.uDateAcquireDate.DateTime.Date.ToString("yyyy-MM-dd");
                        row["AcquireAmt"] = this.uNumAcquireAmt.Value.ToString();
                        row["SerialNo"] = this.uTextSerialNo.Text;
                        row["AcquireDeptCode"] = this.uComboAcquireDeptCode.Value.ToString();
                        row["AcquireReason"] = this.uTextAcquireReason.Text;
                        row["InspectPeriod"] = this.uNumInspectPeriod.Value.ToString();
                        row["InspectPeriodUnitCode"] = this.uComboInspectPeriodUnitCode.Value.ToString();
                        if (this.uDateInspectNextDate.Value != null)
                            row["InspectNextDate"] = this.uDateInspectNextDate.DateTime.Date.ToString("yyyy-MM-dd");
                        if (this.uDateLastInspectDate.Value != null)
                            row["LastInspectDate"] = this.uDateLastInspectDate.DateTime.Date.ToString("yyyy-MM-dd");
                        if (this.uDateDiscardDate.Value != null)
                            row["DiscardDate"] = this.uDateDiscardDate.DateTime.Date.ToString("yyyy-MM-dd");
                        row["DiscardFlag"] = this.uComboDiscardFlag.Value.ToString();
                        row["DiscardReason"] = this.richTextEditor.GetDocumentText();
                        // 파일 이름 설정
                        if (this.uTextMTFileName.Text.Contains(":\\") && this.uTextMeasureToolCode.Text != "")
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextMTFileName.Text);
                            row["MTFileName"] = this.uComboPlant.Value.ToString() + "-" + this.uTextMeasureToolCode.Text + "-" + fileDoc.Name;
                        }
                        else if (this.uTextMTFileName.Text.Contains(":\\") && this.uTextMeasureToolCode.Text == "")
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextMTFileName.Text);
                            row["MTFileName"] = fileDoc.Name;
                        }
                        else
                        {
                            row["MTFileName"] = this.uTextMTFileName.Text;
                        }
                        row["UseFlag"] = this.uComboUseFlag.Value.ToString();

                        // 2012-11-16 추가
                        row["Class"] = this.uComboClass.Value.ToString().Trim();      // Class
                        row["Result"] = this.uComboResult.Value.ToString().Trim();    // 검증결과
                        row["Customer"] = this.uTextCustomer.Text.Trim();  // 고객
                        row["Allowance"] = this.uTextAllowance.Text.Trim();// 공차
                        row["Purpose"] = this.uTextPurpose.Text.Trim();    // Purpose
                        // 2012-11-16

                        dtMeaureTool.Rows.Add(row);
                    }


                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 처리로직.... //
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 호출
                        string rtMSG = measureTool.mfSaveMASMeasureTool(dtMeaureTool, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝//

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        DialogResult DResult = new DialogResult();
                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {

                            // FileUpload 메소드 호출
                            FileUpload();

                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000633", Infragistics.Win.HAlign.Right);

                    //this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    //BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                    QRPMAS.BL.MASQUA.MeasureTool measureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                    brwChannel.mfCredentials(measureTool);

                    // 매개변수로 넘겨줄 DataTable 설정
                    DataTable dtMeaureTool = measureTool.mfSetDataInfo();

                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboMTType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000246", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboMTType.DropDown();
                        return;
                    }
                    else if (this.uTextMeasureToolCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000248", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextMeasureToolCode.Focus();
                        return;
                    }
                    else
                    {
                        DataRow row = dtMeaureTool.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["MTTypeCode"] = this.uComboMTType.Value.ToString();
                        row["MeasureToolCode"] = this.uTextMeasureToolCode.Text;
                        dtMeaureTool.Rows.Add(row);
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 처리 로직 //
                        // Call Delete Method
                        string rtMSG = measureTool.mfDeleteMASMeasureTool(dtMeaureTool);

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝 //

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        DialogResult DResult = new DialogResult();
                        // 삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000926",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000923",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 생성버튼
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화

                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfExcel()
        {
            try
            {
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridMeasureToolList);
            }
            catch
            {
            }
            finally
            {
            }
        }

        // 초기화 버튼 클릭 이벤트

        private void uNumAcquireAmt_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 0으로 초기화

                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 증가/감소 버튼 클릭 이벤트

        private void uNumAcquireAmt_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장

                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입

                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입

                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        //private void uNumAcquireAmt_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        //{
        //    try
        //    {
        //        Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
        //        if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
        //        {
        //            (int)ed.Value += 1;
        //        }
        //        else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && ed.Value > 0)
        //        {
        //            (int)ed.Value -= 1;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// Control 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextMeasureToolCode.Text = "";
                this.uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextMeasureToolName.Text = "";
                this.uTextMeasureToolNameCh.Text = "";
                this.uTextMeasureToolNameEn.Text = "";
                this.uComboMTLType.Value = "";
                this.uComboMTMType.Value = "";
                this.uComboMTType.Value = "";
                this.uTextSpec.Text = "";
                this.uTextModelName.Text = "";
                this.uTextMakerCompany.Text = "";
                this.uTextMakerNo.Text = "";
                this.uDateAcquireDate.Value = DateTime.Now;
                this.uNumAcquireAmt.Value = 0;
                this.uTextSerialNo.Text = "";
                this.uComboAcquireDeptCode.Value = "";
                this.uTextAcquireReason.Text = "";
                this.uNumInspectPeriod.Value = 0;
                this.uComboInspectPeriodUnitCode.Value = "";
                this.uDateInspectNextDate.Value = DateTime.Now;
                this.uDateLastInspectDate.Value = null;
                this.uDateDiscardDate.Value = null;
                this.uComboDiscardFlag.Value = "F";
                this.richTextEditor.Clear();
                this.uComboUseFlag.Value = "T";
                this.uTextMTFileName.Clear();

                // 2012-11-16 추가
                this.uComboClass.Value = string.Empty;  // Class
                this.uComboResult.Value = string.Empty; // 검증결과
                this.uTextCustomer.Clear();             // 고객
                this.uTextAllowance.Clear();            // 공차
                this.uTextPurpose.Clear();              // Purpose
                // 2012-11-16

                // PK 편집 가능 생태로

                this.uComboPlant.ReadOnly = false;
                this.uComboMTLType.ReadOnly = false;
                this.uComboMTMType.ReadOnly = false;
                this.uComboMTType.ReadOnly = false;

                // 계측기 코드는 자동 채번이기때문에 입력불가로 표시
                this.uTextMeasureToolCode.ReadOnly = true;
                this.uTextMeasureToolCode.Appearance.BackColor = Color.PowderBlue;

                if (this.uGridHistory.Rows.Count > 0)
                {
                    this.uGridHistory.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridHistory.Rows.All);
                    this.uGridHistory.DeleteSelectedRows(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0014_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uNumInspectPeriod_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장

                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입

                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입

                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uNumInspectPeriod_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 0으로 초기화

                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextWriteID.Text;
                    WinMessageBox msg = new WinMessageBox();

                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteName.Text = "";
                            this.uTextWriteID.Text = "";
                        }
                        else
                        {
                            this.uTextWriteName.Text = strRtnUserName;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frmPOP.ShowDialog();

                this.uTextWriteID.Text = frmPOP.UserID;
                this.uTextWriteName.Text = frmPOP.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
      
        /// <summary>
        /// UserName 받는 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strWriteID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strWriteID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strWriteID, m_resSys.GetString("SYS_LANG"));

                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }

                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드

        /// </summary>
        /// <param name="strClaimNo">발행번호</param>
        private void FileUpload()
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                // AttachmentFile
                if (this.uTextMTFileName.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+계측기코드+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextMTFileName.Text);
                    string strMeasureToolCode = this.uTextMeasureToolCode.Text;
                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + strMeasureToolCode + "-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextMTFileName.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }


                // 업로드할 파일이 있는경우 파일 업로드

                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0019");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 파일다운 UP/DOWN 버튼 이벤트

        // Attachment FIle
        private void uTextMTFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "UP")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            //첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001139", "M001141", "M001451",
                                                Infragistics.Win.HAlign.Right);
                            return;
                        }
                        this.uTextMTFileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "DOWN")
                {
                    if (this.uTextMTFileName.Text.Equals(string.Empty))
                        return;

                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextMTFileName.Text.Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0019");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextMTFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기

                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextMTFileName.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uDateDiscardDate_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uDateDiscardDate.Value == null)
                {
                    this.uComboDiscardFlag.Value = "F";
                }
                else
                {
                    this.uComboDiscardFlag.Value = "T";
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void frmQATZ0014_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 더블 클릭 시 다운로드 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridHistory_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (!e.Cell.Column.Key.Equals("FileName"))
                    return;

                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strFileName = e.Cell.Value.ToString();

                // 파일서버에서 불러올수 있는 파일인지 체크
                if (strFileName.Equals(string.Empty) || strFileName.Contains(":\\"))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    string strFileFath = "";
                    if (e.Cell.Row.Cells["Gubun"].Value.ToString().Equals("1")) //내부교정 파일경로
                        strFileFath = "D0017";
                    else if (e.Cell.Row.Cells["Gubun"].Value.ToString().Equals("2")) //외부교정 파일경로
                        strFileFath = "D0018";

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), strFileFath);

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(strFileName);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + strFileName);


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }
    }
}
