﻿namespace QRPQAT.UI
{
    partial class frmQATZ0018
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0018));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGridSRR = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxMAC = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelMLanking = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMScore = new Infragistics.Win.Misc.UltraLabel();
            this.uNumMLanking = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMPenaltyAward = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMScore = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMQualitySystem = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMResponse = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMQualityLevel = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumMCooperation = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelMPenaltyAward = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMResponse = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMCooperation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMQualitySystem = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMQualityLevel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMPeriodMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMPeriodMonth = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxSEC = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumSLanking = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSScore = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSResponse = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSQualitySystem = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelSLanking = new Infragistics.Win.Misc.UltraLabel();
            this.uNumSQualRun = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelSScore = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSResponse = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSQualitySystem = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSQualRun = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxYIeld = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumSDCTestYield = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSAssyYield = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelSDCTestYield = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSAssyYield = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSInspect = new Infragistics.Win.Misc.UltraLabel();
            this.uNumSInspect = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSQualityAction = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSExecution = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelSQualityAction = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSExecution = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxQual = new Infragistics.Win.Misc.UltraGroupBox();
            this.uNumClaim = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSICN = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSLowYield = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumSAccident = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelClaim = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSICN = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLowYield = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSAccident = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSPeriodQuarter = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSPeriodQuarter = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateWrite = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextManageNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStdNumber = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMAC)).BeginInit();
            this.uGroupBoxMAC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMLanking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMPenaltyAward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMQualitySystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMResponse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMQualityLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMCooperation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMPeriodMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSEC)).BeginInit();
            this.uGroupBoxSEC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSLanking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSResponse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualitySystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxYIeld)).BeginInit();
            this.uGroupBoxYIeld.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSDCTestYield)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSAssyYield)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSInspect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualityAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSExecution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxQual)).BeginInit();
            this.uGroupBoxQual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumClaim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSICN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSLowYield)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSAccident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPeriodQuarter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateSearchToDate
            // 
            this.uDateSearchToDate.Location = new System.Drawing.Point(516, 12);
            this.uDateSearchToDate.Name = "uDateSearchToDate";
            this.uDateSearchToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDate.TabIndex = 3;
            // 
            // uDateSearchFromDate
            // 
            this.uDateSearchFromDate.Location = new System.Drawing.Point(404, 12);
            this.uDateSearchFromDate.Name = "uDateSearchFromDate";
            this.uDateSearchFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDate.TabIndex = 2;
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchCustomer.MaxLength = 100;
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchCustomer.TabIndex = 1;
            // 
            // ultraLabel3
            // 
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance2;
            this.ultraLabel3.Location = new System.Drawing.Point(504, 20);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel3.TabIndex = 1;
            this.ultraLabel3.Text = "~";
            this.ultraLabel3.Visible = false;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(288, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelSearchDate.TabIndex = 1;
            this.uLabelSearchDate.Text = "등록일";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(112, 20);
            this.uLabelSearchCustomer.TabIndex = 1;
            this.uLabelSearchCustomer.Text = "고객";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(792, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Text = "공장";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(896, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 0;
            this.uComboSearchPlant.Visible = false;
            // 
            // uGridSRR
            // 
            this.uGridSRR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSRR.DisplayLayout.Appearance = appearance6;
            this.uGridSRR.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSRR.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRR.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRR.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridSRR.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSRR.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridSRR.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSRR.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSRR.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSRR.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridSRR.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSRR.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSRR.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSRR.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridSRR.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSRR.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSRR.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridSRR.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridSRR.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSRR.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridSRR.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridSRR.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSRR.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridSRR.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSRR.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSRR.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSRR.Location = new System.Drawing.Point(0, 80);
            this.uGridSRR.Name = "uGridSRR";
            this.uGridSRR.Size = new System.Drawing.Size(1070, 760);
            this.uGridSRR.TabIndex = 2;
            this.uGridSRR.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridSRR_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxMAC);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxSEC);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateWrite);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextManageNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStdNumber);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxMAC
            // 
            this.uGroupBoxMAC.Controls.Add(this.uLabelMLanking);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMScore);
            this.uGroupBoxMAC.Controls.Add(this.uNumMLanking);
            this.uGroupBoxMAC.Controls.Add(this.uNumMPenaltyAward);
            this.uGroupBoxMAC.Controls.Add(this.uNumMScore);
            this.uGroupBoxMAC.Controls.Add(this.uNumMQualitySystem);
            this.uGroupBoxMAC.Controls.Add(this.uNumMResponse);
            this.uGroupBoxMAC.Controls.Add(this.uNumMQualityLevel);
            this.uGroupBoxMAC.Controls.Add(this.uNumMCooperation);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMPenaltyAward);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMResponse);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMCooperation);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMQualitySystem);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMQualityLevel);
            this.uGroupBoxMAC.Controls.Add(this.uLabelMPeriodMonth);
            this.uGroupBoxMAC.Controls.Add(this.uComboMPeriodMonth);
            this.uGroupBoxMAC.Location = new System.Drawing.Point(12, 64);
            this.uGroupBoxMAC.Name = "uGroupBoxMAC";
            this.uGroupBoxMAC.Size = new System.Drawing.Size(1040, 108);
            this.uGroupBoxMAC.TabIndex = 5;
            // 
            // uLabelMLanking
            // 
            this.uLabelMLanking.Location = new System.Drawing.Point(700, 72);
            this.uLabelMLanking.Name = "uLabelMLanking";
            this.uLabelMLanking.Size = new System.Drawing.Size(112, 20);
            this.uLabelMLanking.TabIndex = 1;
            this.uLabelMLanking.Text = "순위";
            // 
            // uLabelMScore
            // 
            this.uLabelMScore.Location = new System.Drawing.Point(700, 48);
            this.uLabelMScore.Name = "uLabelMScore";
            this.uLabelMScore.Size = new System.Drawing.Size(112, 20);
            this.uLabelMScore.TabIndex = 1;
            this.uLabelMScore.Text = "점수";
            // 
            // uNumMLanking
            // 
            this.uNumMLanking.Location = new System.Drawing.Point(816, 72);
            this.uNumMLanking.MinValue = 0;
            this.uNumMLanking.Name = "uNumMLanking";
            this.uNumMLanking.PromptChar = ' ';
            this.uNumMLanking.Size = new System.Drawing.Size(100, 21);
            this.uNumMLanking.TabIndex = 14;
            // 
            // uNumMPenaltyAward
            // 
            this.uNumMPenaltyAward.Location = new System.Drawing.Point(584, 76);
            this.uNumMPenaltyAward.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMPenaltyAward.Name = "uNumMPenaltyAward";
            this.uNumMPenaltyAward.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMPenaltyAward.PromptChar = ' ';
            this.uNumMPenaltyAward.Size = new System.Drawing.Size(100, 21);
            this.uNumMPenaltyAward.TabIndex = 12;
            this.uNumMPenaltyAward.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumMScore
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uNumMScore.Appearance = appearance22;
            this.uNumMScore.BackColor = System.Drawing.Color.Gainsboro;
            this.uNumMScore.Location = new System.Drawing.Point(816, 48);
            this.uNumMScore.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMScore.Name = "uNumMScore";
            this.uNumMScore.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMScore.PromptChar = ' ';
            this.uNumMScore.ReadOnly = true;
            this.uNumMScore.Size = new System.Drawing.Size(100, 21);
            this.uNumMScore.TabIndex = 13;
            // 
            // uNumMQualitySystem
            // 
            this.uNumMQualitySystem.Location = new System.Drawing.Point(364, 52);
            this.uNumMQualitySystem.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMQualitySystem.Name = "uNumMQualitySystem";
            this.uNumMQualitySystem.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMQualitySystem.PromptChar = ' ';
            this.uNumMQualitySystem.Size = new System.Drawing.Size(100, 21);
            this.uNumMQualitySystem.TabIndex = 9;
            this.uNumMQualitySystem.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumMResponse
            // 
            this.uNumMResponse.Location = new System.Drawing.Point(364, 76);
            this.uNumMResponse.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMResponse.Name = "uNumMResponse";
            this.uNumMResponse.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMResponse.PromptChar = ' ';
            this.uNumMResponse.Size = new System.Drawing.Size(100, 21);
            this.uNumMResponse.TabIndex = 11;
            this.uNumMResponse.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumMQualityLevel
            // 
            this.uNumMQualityLevel.Location = new System.Drawing.Point(128, 52);
            this.uNumMQualityLevel.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMQualityLevel.Name = "uNumMQualityLevel";
            this.uNumMQualityLevel.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMQualityLevel.PromptChar = ' ';
            this.uNumMQualityLevel.Size = new System.Drawing.Size(100, 21);
            this.uNumMQualityLevel.TabIndex = 8;
            this.uNumMQualityLevel.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumMCooperation
            // 
            this.uNumMCooperation.Location = new System.Drawing.Point(128, 76);
            this.uNumMCooperation.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumMCooperation.Name = "uNumMCooperation";
            this.uNumMCooperation.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumMCooperation.PromptChar = ' ';
            this.uNumMCooperation.Size = new System.Drawing.Size(100, 21);
            this.uNumMCooperation.TabIndex = 10;
            this.uNumMCooperation.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelMPenaltyAward
            // 
            this.uLabelMPenaltyAward.Location = new System.Drawing.Point(468, 76);
            this.uLabelMPenaltyAward.Name = "uLabelMPenaltyAward";
            this.uLabelMPenaltyAward.Size = new System.Drawing.Size(112, 20);
            this.uLabelMPenaltyAward.TabIndex = 1;
            this.uLabelMPenaltyAward.Text = "penalty &Award ";
            // 
            // uLabelMResponse
            // 
            this.uLabelMResponse.Location = new System.Drawing.Point(248, 76);
            this.uLabelMResponse.Name = "uLabelMResponse";
            this.uLabelMResponse.Size = new System.Drawing.Size(112, 20);
            this.uLabelMResponse.TabIndex = 1;
            this.uLabelMResponse.Text = "response ";
            // 
            // uLabelMCooperation
            // 
            this.uLabelMCooperation.Location = new System.Drawing.Point(12, 76);
            this.uLabelMCooperation.Name = "uLabelMCooperation";
            this.uLabelMCooperation.Size = new System.Drawing.Size(112, 20);
            this.uLabelMCooperation.TabIndex = 1;
            this.uLabelMCooperation.Text = "cooperation ";
            // 
            // uLabelMQualitySystem
            // 
            this.uLabelMQualitySystem.Location = new System.Drawing.Point(248, 52);
            this.uLabelMQualitySystem.Name = "uLabelMQualitySystem";
            this.uLabelMQualitySystem.Size = new System.Drawing.Size(112, 20);
            this.uLabelMQualitySystem.TabIndex = 1;
            this.uLabelMQualitySystem.Text = "quality system";
            // 
            // uLabelMQualityLevel
            // 
            this.uLabelMQualityLevel.Location = new System.Drawing.Point(12, 52);
            this.uLabelMQualityLevel.Name = "uLabelMQualityLevel";
            this.uLabelMQualityLevel.Size = new System.Drawing.Size(112, 20);
            this.uLabelMQualityLevel.TabIndex = 1;
            this.uLabelMQualityLevel.Text = "Quality level ";
            // 
            // uLabelMPeriodMonth
            // 
            this.uLabelMPeriodMonth.Location = new System.Drawing.Point(12, 28);
            this.uLabelMPeriodMonth.Name = "uLabelMPeriodMonth";
            this.uLabelMPeriodMonth.Size = new System.Drawing.Size(112, 20);
            this.uLabelMPeriodMonth.TabIndex = 1;
            this.uLabelMPeriodMonth.Text = "기간(월)";
            // 
            // uComboMPeriodMonth
            // 
            this.uComboMPeriodMonth.Location = new System.Drawing.Point(128, 28);
            this.uComboMPeriodMonth.MaxLength = 10;
            this.uComboMPeriodMonth.Name = "uComboMPeriodMonth";
            this.uComboMPeriodMonth.Size = new System.Drawing.Size(100, 21);
            this.uComboMPeriodMonth.TabIndex = 7;
            // 
            // uGroupBoxSEC
            // 
            this.uGroupBoxSEC.Controls.Add(this.uNumSLanking);
            this.uGroupBoxSEC.Controls.Add(this.uNumSScore);
            this.uGroupBoxSEC.Controls.Add(this.uNumSResponse);
            this.uGroupBoxSEC.Controls.Add(this.uNumSQualitySystem);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSLanking);
            this.uGroupBoxSEC.Controls.Add(this.uNumSQualRun);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSScore);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSResponse);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSQualitySystem);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSQualRun);
            this.uGroupBoxSEC.Controls.Add(this.uGroupBoxYIeld);
            this.uGroupBoxSEC.Controls.Add(this.uGroupBox2);
            this.uGroupBoxSEC.Controls.Add(this.uGroupBoxQual);
            this.uGroupBoxSEC.Controls.Add(this.uComboSPeriodQuarter);
            this.uGroupBoxSEC.Controls.Add(this.uLabelSPeriodQuarter);
            this.uGroupBoxSEC.Location = new System.Drawing.Point(12, 64);
            this.uGroupBoxSEC.Name = "uGroupBoxSEC";
            this.uGroupBoxSEC.Size = new System.Drawing.Size(1040, 240);
            this.uGroupBoxSEC.TabIndex = 5;
            // 
            // uNumSLanking
            // 
            this.uNumSLanking.Location = new System.Drawing.Point(816, 204);
            this.uNumSLanking.MinValue = 0;
            this.uNumSLanking.Name = "uNumSLanking";
            this.uNumSLanking.PromptChar = ' ';
            this.uNumSLanking.Size = new System.Drawing.Size(100, 21);
            this.uNumSLanking.TabIndex = 20;
            // 
            // uNumSScore
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uNumSScore.Appearance = appearance23;
            this.uNumSScore.BackColor = System.Drawing.Color.Gainsboro;
            this.uNumSScore.Location = new System.Drawing.Point(816, 180);
            this.uNumSScore.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSScore.Name = "uNumSScore";
            this.uNumSScore.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSScore.PromptChar = ' ';
            this.uNumSScore.ReadOnly = true;
            this.uNumSScore.Size = new System.Drawing.Size(100, 21);
            this.uNumSScore.TabIndex = 19;
            // 
            // uNumSResponse
            // 
            this.uNumSResponse.Location = new System.Drawing.Point(816, 28);
            this.uNumSResponse.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSResponse.Name = "uNumSResponse";
            this.uNumSResponse.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSResponse.PromptChar = ' ';
            this.uNumSResponse.Size = new System.Drawing.Size(100, 21);
            this.uNumSResponse.TabIndex = 10;
            this.uNumSResponse.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSQualitySystem
            // 
            this.uNumSQualitySystem.Location = new System.Drawing.Point(592, 28);
            this.uNumSQualitySystem.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSQualitySystem.Name = "uNumSQualitySystem";
            this.uNumSQualitySystem.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSQualitySystem.PromptChar = ' ';
            this.uNumSQualitySystem.Size = new System.Drawing.Size(100, 21);
            this.uNumSQualitySystem.TabIndex = 9;
            this.uNumSQualitySystem.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelSLanking
            // 
            this.uLabelSLanking.Location = new System.Drawing.Point(700, 204);
            this.uLabelSLanking.Name = "uLabelSLanking";
            this.uLabelSLanking.Size = new System.Drawing.Size(112, 20);
            this.uLabelSLanking.TabIndex = 1;
            this.uLabelSLanking.Text = "순위";
            // 
            // uNumSQualRun
            // 
            this.uNumSQualRun.Location = new System.Drawing.Point(360, 28);
            this.uNumSQualRun.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSQualRun.Name = "uNumSQualRun";
            this.uNumSQualRun.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSQualRun.PromptChar = ' ';
            this.uNumSQualRun.Size = new System.Drawing.Size(100, 21);
            this.uNumSQualRun.TabIndex = 8;
            this.uNumSQualRun.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelSScore
            // 
            this.uLabelSScore.Location = new System.Drawing.Point(700, 180);
            this.uLabelSScore.Name = "uLabelSScore";
            this.uLabelSScore.Size = new System.Drawing.Size(112, 20);
            this.uLabelSScore.TabIndex = 1;
            this.uLabelSScore.Text = "점수";
            // 
            // uLabelSResponse
            // 
            this.uLabelSResponse.Location = new System.Drawing.Point(700, 28);
            this.uLabelSResponse.Name = "uLabelSResponse";
            this.uLabelSResponse.Size = new System.Drawing.Size(112, 20);
            this.uLabelSResponse.TabIndex = 1;
            this.uLabelSResponse.Text = "고객대응";
            // 
            // uLabelSQualitySystem
            // 
            this.uLabelSQualitySystem.Location = new System.Drawing.Point(476, 28);
            this.uLabelSQualitySystem.Name = "uLabelSQualitySystem";
            this.uLabelSQualitySystem.Size = new System.Drawing.Size(112, 20);
            this.uLabelSQualitySystem.TabIndex = 1;
            this.uLabelSQualitySystem.Text = "품질SYSTEM";
            // 
            // uLabelSQualRun
            // 
            this.uLabelSQualRun.Location = new System.Drawing.Point(244, 28);
            this.uLabelSQualRun.Name = "uLabelSQualRun";
            this.uLabelSQualRun.Size = new System.Drawing.Size(112, 20);
            this.uLabelSQualRun.TabIndex = 1;
            this.uLabelSQualRun.Text = "qual run ";
            // 
            // uGroupBoxYIeld
            // 
            this.uGroupBoxYIeld.Controls.Add(this.uNumSDCTestYield);
            this.uGroupBoxYIeld.Controls.Add(this.uNumSAssyYield);
            this.uGroupBoxYIeld.Controls.Add(this.uLabelSDCTestYield);
            this.uGroupBoxYIeld.Controls.Add(this.uLabelSAssyYield);
            this.uGroupBoxYIeld.Location = new System.Drawing.Point(12, 172);
            this.uGroupBoxYIeld.Name = "uGroupBoxYIeld";
            this.uGroupBoxYIeld.Size = new System.Drawing.Size(456, 56);
            this.uGroupBoxYIeld.TabIndex = 3;
            // 
            // uNumSDCTestYield
            // 
            this.uNumSDCTestYield.Location = new System.Drawing.Point(348, 28);
            this.uNumSDCTestYield.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSDCTestYield.Name = "uNumSDCTestYield";
            this.uNumSDCTestYield.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSDCTestYield.PromptChar = ' ';
            this.uNumSDCTestYield.Size = new System.Drawing.Size(100, 21);
            this.uNumSDCTestYield.TabIndex = 18;
            this.uNumSDCTestYield.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSAssyYield
            // 
            this.uNumSAssyYield.Location = new System.Drawing.Point(128, 28);
            this.uNumSAssyYield.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSAssyYield.Name = "uNumSAssyYield";
            this.uNumSAssyYield.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSAssyYield.PromptChar = ' ';
            this.uNumSAssyYield.Size = new System.Drawing.Size(100, 21);
            this.uNumSAssyYield.TabIndex = 17;
            this.uNumSAssyYield.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelSDCTestYield
            // 
            this.uLabelSDCTestYield.Location = new System.Drawing.Point(232, 28);
            this.uLabelSDCTestYield.Name = "uLabelSDCTestYield";
            this.uLabelSDCTestYield.Size = new System.Drawing.Size(112, 20);
            this.uLabelSDCTestYield.TabIndex = 1;
            this.uLabelSDCTestYield.Text = "DC TEST YIELD";
            // 
            // uLabelSAssyYield
            // 
            this.uLabelSAssyYield.Location = new System.Drawing.Point(12, 28);
            this.uLabelSAssyYield.Name = "uLabelSAssyYield";
            this.uLabelSAssyYield.Size = new System.Drawing.Size(112, 20);
            this.uLabelSAssyYield.TabIndex = 1;
            this.uLabelSAssyYield.Text = "ASSY YIELD\r\n";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uLabelSInspect);
            this.uGroupBox2.Controls.Add(this.uNumSInspect);
            this.uGroupBox2.Controls.Add(this.uNumSQualityAction);
            this.uGroupBox2.Controls.Add(this.uNumSExecution);
            this.uGroupBox2.Controls.Add(this.uLabelSQualityAction);
            this.uGroupBox2.Controls.Add(this.uLabelSExecution);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 112);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(675, 56);
            this.uGroupBox2.TabIndex = 3;
            // 
            // uLabelSInspect
            // 
            this.uLabelSInspect.Location = new System.Drawing.Point(456, 28);
            this.uLabelSInspect.Name = "uLabelSInspect";
            this.uLabelSInspect.Size = new System.Drawing.Size(112, 20);
            this.uLabelSInspect.TabIndex = 1;
            this.uLabelSInspect.Text = "자주검사";
            // 
            // uNumSInspect
            // 
            this.uNumSInspect.Location = new System.Drawing.Point(572, 28);
            this.uNumSInspect.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSInspect.Name = "uNumSInspect";
            this.uNumSInspect.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSInspect.PromptChar = ' ';
            this.uNumSInspect.Size = new System.Drawing.Size(100, 21);
            this.uNumSInspect.TabIndex = 16;
            this.uNumSInspect.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSQualityAction
            // 
            this.uNumSQualityAction.Location = new System.Drawing.Point(348, 28);
            this.uNumSQualityAction.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSQualityAction.Name = "uNumSQualityAction";
            this.uNumSQualityAction.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSQualityAction.PromptChar = ' ';
            this.uNumSQualityAction.Size = new System.Drawing.Size(100, 21);
            this.uNumSQualityAction.TabIndex = 15;
            this.uNumSQualityAction.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSExecution
            // 
            this.uNumSExecution.Location = new System.Drawing.Point(128, 28);
            this.uNumSExecution.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSExecution.Name = "uNumSExecution";
            this.uNumSExecution.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSExecution.PromptChar = ' ';
            this.uNumSExecution.Size = new System.Drawing.Size(100, 21);
            this.uNumSExecution.TabIndex = 14;
            this.uNumSExecution.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelSQualityAction
            // 
            this.uLabelSQualityAction.Location = new System.Drawing.Point(232, 28);
            this.uLabelSQualityAction.Name = "uLabelSQualityAction";
            this.uLabelSQualityAction.Size = new System.Drawing.Size(112, 20);
            this.uLabelSQualityAction.TabIndex = 1;
            this.uLabelSQualityAction.Text = "품질활동";
            // 
            // uLabelSExecution
            // 
            this.uLabelSExecution.Location = new System.Drawing.Point(12, 28);
            this.uLabelSExecution.Name = "uLabelSExecution";
            this.uLabelSExecution.Size = new System.Drawing.Size(112, 20);
            this.uLabelSExecution.TabIndex = 1;
            this.uLabelSExecution.Text = "집행율";
            // 
            // uGroupBoxQual
            // 
            this.uGroupBoxQual.Controls.Add(this.uNumClaim);
            this.uGroupBoxQual.Controls.Add(this.uNumSICN);
            this.uGroupBoxQual.Controls.Add(this.uNumSLowYield);
            this.uGroupBoxQual.Controls.Add(this.uNumSAccident);
            this.uGroupBoxQual.Controls.Add(this.uLabelClaim);
            this.uGroupBoxQual.Controls.Add(this.uLabelSICN);
            this.uGroupBoxQual.Controls.Add(this.uLabelLowYield);
            this.uGroupBoxQual.Controls.Add(this.uLabelSAccident);
            this.uGroupBoxQual.Location = new System.Drawing.Point(12, 52);
            this.uGroupBoxQual.Name = "uGroupBoxQual";
            this.uGroupBoxQual.Size = new System.Drawing.Size(912, 56);
            this.uGroupBoxQual.TabIndex = 3;
            // 
            // uNumClaim
            // 
            this.uNumClaim.Location = new System.Drawing.Point(804, 28);
            this.uNumClaim.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumClaim.Name = "uNumClaim";
            this.uNumClaim.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumClaim.PromptChar = ' ';
            this.uNumClaim.Size = new System.Drawing.Size(100, 21);
            this.uNumClaim.TabIndex = 13;
            this.uNumClaim.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSICN
            // 
            this.uNumSICN.Location = new System.Drawing.Point(576, 28);
            this.uNumSICN.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSICN.Name = "uNumSICN";
            this.uNumSICN.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSICN.PromptChar = ' ';
            this.uNumSICN.Size = new System.Drawing.Size(100, 21);
            this.uNumSICN.TabIndex = 13;
            this.uNumSICN.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSLowYield
            // 
            this.uNumSLowYield.Location = new System.Drawing.Point(348, 28);
            this.uNumSLowYield.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSLowYield.Name = "uNumSLowYield";
            this.uNumSLowYield.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSLowYield.PromptChar = ' ';
            this.uNumSLowYield.Size = new System.Drawing.Size(100, 21);
            this.uNumSLowYield.TabIndex = 12;
            this.uNumSLowYield.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uNumSAccident
            // 
            this.uNumSAccident.Location = new System.Drawing.Point(128, 28);
            this.uNumSAccident.MaskInput = "nnnnnnnnnn.nnn";
            this.uNumSAccident.Name = "uNumSAccident";
            this.uNumSAccident.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumSAccident.PromptChar = ' ';
            this.uNumSAccident.Size = new System.Drawing.Size(100, 21);
            this.uNumSAccident.TabIndex = 11;
            this.uNumSAccident.ValueChanged += new System.EventHandler(this.uNumMBoxSum_ValueChanged);
            // 
            // uLabelClaim
            // 
            this.uLabelClaim.Location = new System.Drawing.Point(688, 28);
            this.uLabelClaim.Name = "uLabelClaim";
            this.uLabelClaim.Size = new System.Drawing.Size(112, 20);
            this.uLabelClaim.TabIndex = 1;
            this.uLabelClaim.Text = "Claim";
            // 
            // uLabelSICN
            // 
            this.uLabelSICN.Location = new System.Drawing.Point(460, 28);
            this.uLabelSICN.Name = "uLabelSICN";
            this.uLabelSICN.Size = new System.Drawing.Size(112, 20);
            this.uLabelSICN.TabIndex = 1;
            this.uLabelSICN.Text = "ICN";
            // 
            // uLabelLowYield
            // 
            this.uLabelLowYield.Location = new System.Drawing.Point(232, 28);
            this.uLabelLowYield.Name = "uLabelLowYield";
            this.uLabelLowYield.Size = new System.Drawing.Size(112, 20);
            this.uLabelLowYield.TabIndex = 1;
            this.uLabelLowYield.Text = "Low Yield";
            // 
            // uLabelSAccident
            // 
            this.uLabelSAccident.Location = new System.Drawing.Point(12, 28);
            this.uLabelSAccident.Name = "uLabelSAccident";
            this.uLabelSAccident.Size = new System.Drawing.Size(112, 20);
            this.uLabelSAccident.TabIndex = 1;
            this.uLabelSAccident.Text = "사건사고";
            // 
            // uComboSPeriodQuarter
            // 
            this.uComboSPeriodQuarter.Location = new System.Drawing.Point(128, 28);
            this.uComboSPeriodQuarter.MaxLength = 10;
            this.uComboSPeriodQuarter.Name = "uComboSPeriodQuarter";
            this.uComboSPeriodQuarter.Size = new System.Drawing.Size(100, 21);
            this.uComboSPeriodQuarter.TabIndex = 7;
            // 
            // uLabelSPeriodQuarter
            // 
            this.uLabelSPeriodQuarter.Location = new System.Drawing.Point(12, 28);
            this.uLabelSPeriodQuarter.Name = "uLabelSPeriodQuarter";
            this.uLabelSPeriodQuarter.Size = new System.Drawing.Size(112, 20);
            this.uLabelSPeriodQuarter.TabIndex = 1;
            this.uLabelSPeriodQuarter.Text = "기간(분기)";
            // 
            // uTextWriteUserID
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteUserID.Appearance = appearance18;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance19.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance19;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton1);
            this.uTextWriteUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(380, 36);
            this.uTextWriteUserID.MaxLength = 20;
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserID.TabIndex = 7;
            this.uTextWriteUserID.ValueChanged += new System.EventHandler(this.uTextWriteUserID_ValueChanged);
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uDateWrite
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWrite.Appearance = appearance20;
            this.uDateWrite.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWrite.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWrite.Location = new System.Drawing.Point(128, 36);
            this.uDateWrite.Name = "uDateWrite";
            this.uDateWrite.Size = new System.Drawing.Size(100, 21);
            this.uDateWrite.TabIndex = 6;
            // 
            // uTextWriteUserName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance15;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(484, 36);
            this.uTextWriteUserName.MaxLength = 20;
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserName.TabIndex = 2;
            // 
            // uTextManageNo
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Appearance = appearance17;
            this.uTextManageNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Location = new System.Drawing.Point(128, 12);
            this.uTextManageNo.MaxLength = 20;
            this.uTextManageNo.Name = "uTextManageNo";
            this.uTextManageNo.ReadOnly = true;
            this.uTextManageNo.Size = new System.Drawing.Size(100, 21);
            this.uTextManageNo.TabIndex = 4;
            // 
            // uTextPlantCode
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance16;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(1020, 8);
            this.uTextPlantCode.MaxLength = 10;
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(24, 21);
            this.uTextPlantCode.TabIndex = 2;
            this.uTextPlantCode.Visible = false;
            // 
            // uComboCustomer
            // 
            appearance21.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomer.Appearance = appearance21;
            this.uComboCustomer.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboCustomer.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboCustomer.Location = new System.Drawing.Point(380, 12);
            this.uComboCustomer.MaxLength = 100;
            this.uComboCustomer.Name = "uComboCustomer";
            this.uComboCustomer.Size = new System.Drawing.Size(144, 21);
            this.uComboCustomer.TabIndex = 5;
            this.uComboCustomer.ValueChanged += new System.EventHandler(this.uComboCustomer_ValueChanged);
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(264, 36);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(112, 20);
            this.uLabelWriteUser.TabIndex = 1;
            this.uLabelWriteUser.Text = "등록자";
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelWriteDate.TabIndex = 1;
            this.uLabelWriteDate.Text = "등록일";
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(264, 12);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(112, 20);
            this.uLabelCustomer.TabIndex = 1;
            this.uLabelCustomer.Text = "고객";
            // 
            // uLabelStdNumber
            // 
            this.uLabelStdNumber.Location = new System.Drawing.Point(12, 12);
            this.uLabelStdNumber.Name = "uLabelStdNumber";
            this.uLabelStdNumber.Size = new System.Drawing.Size(112, 20);
            this.uLabelStdNumber.TabIndex = 1;
            this.uLabelStdNumber.Text = "관리번호";
            // 
            // frmQATZ0018
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridSRR);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0018";
            this.Load += new System.EventHandler(this.frmQATZ0018_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0018_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0018_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0018_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSRR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxMAC)).EndInit();
            this.uGroupBoxMAC.ResumeLayout(false);
            this.uGroupBoxMAC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMLanking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMPenaltyAward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMQualitySystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMResponse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMQualityLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumMCooperation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMPeriodMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSEC)).EndInit();
            this.uGroupBoxSEC.ResumeLayout(false);
            this.uGroupBoxSEC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSLanking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSResponse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualitySystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxYIeld)).EndInit();
            this.uGroupBoxYIeld.ResumeLayout(false);
            this.uGroupBoxYIeld.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSDCTestYield)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSAssyYield)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSInspect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSQualityAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSExecution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxQual)).EndInit();
            this.uGroupBoxQual.ResumeLayout(false);
            this.uGroupBoxQual.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumClaim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSICN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSLowYield)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumSAccident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSPeriodQuarter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSRR;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextManageNo;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWrite;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomer;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxMAC;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSEC;
        private Infragistics.Win.Misc.UltraLabel uLabelSPeriodQuarter;
        private Infragistics.Win.Misc.UltraLabel uLabelSQualRun;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxYIeld;
        private Infragistics.Win.Misc.UltraLabel uLabelSDCTestYield;
        private Infragistics.Win.Misc.UltraLabel uLabelSAssyYield;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxQual;
        private Infragistics.Win.Misc.UltraLabel uLabelSICN;
        private Infragistics.Win.Misc.UltraLabel uLabelLowYield;
        private Infragistics.Win.Misc.UltraLabel uLabelSAccident;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSPeriodQuarter;
        private Infragistics.Win.Misc.UltraLabel uLabelSQualitySystem;
        private Infragistics.Win.Misc.UltraLabel uLabelMLanking;
        private Infragistics.Win.Misc.UltraLabel uLabelMScore;
        private Infragistics.Win.Misc.UltraLabel uLabelMPenaltyAward;
        private Infragistics.Win.Misc.UltraLabel uLabelMResponse;
        private Infragistics.Win.Misc.UltraLabel uLabelMCooperation;
        private Infragistics.Win.Misc.UltraLabel uLabelMQualitySystem;
        private Infragistics.Win.Misc.UltraLabel uLabelMQualityLevel;
        private Infragistics.Win.Misc.UltraLabel uLabelMPeriodMonth;
        private Infragistics.Win.Misc.UltraLabel uLabelSLanking;
        private Infragistics.Win.Misc.UltraLabel uLabelSScore;
        private Infragistics.Win.Misc.UltraLabel uLabelSResponse;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraLabel uLabelSInspect;
        private Infragistics.Win.Misc.UltraLabel uLabelSQualityAction;
        private Infragistics.Win.Misc.UltraLabel uLabelSExecution;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSAccident;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSResponse;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSQualitySystem;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSQualRun;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSDCTestYield;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSAssyYield;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSICN;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSLowYield;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMPenaltyAward;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMQualitySystem;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMResponse;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMQualityLevel;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMCooperation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMPeriodMonth;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSLanking;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSScore;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSInspect;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSQualityAction;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumSExecution;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMLanking;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumMScore;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumClaim;
        private Infragistics.Win.Misc.UltraLabel uLabelClaim;
    }
}