﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : R&R관리                                               */
/* 프로그램ID   : frmQAT0021.cs                                         */
/* 프로그램명   : 계측기R&R 분석                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQAT0022 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmQAT0022()
        {
            InitializeComponent();
        }

        private void frmQAT0022_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQAT0022_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitComboBox();
            InitText();
            InitTab();
            InitLabel();
            InitGrid();
            InitGroupBox();
            //uGroupBoxContentsArea숨기기
            uGroupBoxContentsArea.Expanded = false;
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        private void InitTab()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabControl1, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None, m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("MSA R&&R 분석", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel1, "계측장비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel2, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabel3, "프로젝트코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel4, "프로젝트명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel5, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel6, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "계측기코드 / 명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel8, "자재코드 / 명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel9, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel10, "검사설명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel11, "안정성(Stability)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel12, "정확성(Accurcy)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel13, "측정반복수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel14, "측정부품수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel15, "계측기(측정자)수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel16, "Tolerance", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel17, "정확성값", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel18, "R&&R설정값", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUnitCode, "단위", m_resSys.GetString("SYS_FONTMAME"), true, false);
                lbl.mfSetLabel(this.uLabelDeviceUnit, "부품단위", m_resSys.GetString("SYSFONTNAME"), true, false);

                //lbl.mfSetLabel(this.uLabel19, "재분석", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel20, "완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel21, "Equipment Variation", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel22, "E.V.", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel23, "%E.V.", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel24, "Appraiser Variartion", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel25, "A.V.", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel26, "%A.V.", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel27, "Repeatability & Reproducibility", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel28, "R&&R", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel29, "%R&&R", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // SystemInfo Resource 변수 선언
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonReCalc, "재분석", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--상세결과
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                   , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--안정성/정확성
                grd.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                   , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProjectCode", "프로젝트코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProjectName", "프로젝트명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MeasureToolName", "계측기명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectItemDesc", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--상세결과
                grd.mfSetGridColumn(this.uGrid2, 0, "ItemSeq", "ItemSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "MachineName", "계측기(측정자)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "EquipmentVariation", "E.V.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "EquipmentPercent", "%E.V.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "AppraiserVariation", "A.V.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "AppraiserPercent", "%A.V.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RNRValue", "R&R.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "RNRPercent", "%R&R.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--안정성/정확성
                grd.mfSetGridColumn(this.uGrid3, 0, "ItemSeq", "ItemSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "MachineName", "계측기(측정자)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "StabilityValue", "안전성", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "AccuracyValue", "정확성", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGrid1, 0);
                //grd.mfAddRowGrid(this.uGrid2, 0);
                //grd.mfAddRowGrid(this.uGrid3, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "정밀도/재현성 분석값", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "R&R 분석결과", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "상세결과", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                //    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                //    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox5, GroupBoxType.DETAIL, "안전성/정확성", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                //uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //TextBox 초기화
        private void InitValue()
        {
            try
            {
                this.uTextPlantName.Text = "";
                this.uTextProjectName.Text = "";
                this.uTextProjectCode.Text = "";
                this.uTextMeasureToolCode.Text = "";
                this.uTextMeasureToolName.Text = "";
                //this.uTextAccuracyFlag.Text = "";
                this.uCheckAccuracyFlag.Checked = false;
                this.uCheckAccuracyFlag.Enabled = false;
                //this.uTextStabilityFlag.Text = "";
                this.uCheckStabilityFlag.Checked = false;
                this.uCheckStabilityFlag.Enabled = false;
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextRepeatCount.Text = "";
                this.uTextPartCount.Text = "";
                this.uTextInspectItemDesc.Text = "";
                this.uTextMeasureCount.Text = "";
                this.uTextTolerance.Text = "";
                this.uTextAccuracyValue.Text = "";
                this.uTextRRValue.Text = "";
                this.uTextInspectDesc.Text = "";
                this.uCheckCompleteCalc.Checked = false;


                //////그리드 행 삭제
                ////while (this.uGrid1.Rows.Count > 0)
                ////{
                ////    this.uGrid1.Rows[0].Delete(false);
                ////}
                ////while (this.uGrid2.Rows.Count > 0)
                ////{
                ////    this.uGrid2.Rows[0].Delete(false);
                ////}
                ////while (this.uGrid3.Rows.Count > 0)
                ////{
                ////    this.uGrid3.Rows[0].Delete(false);
                ////}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        #endregion

        #region 툴바기능
        public void mfSearch()
        {
            try
            {
                //그룹박스가 펼져져있으면 접는다.
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsMSAH = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsMSAH);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //Parameter 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strMeasureToolCode = this.uTextSearchMeasureToolCode.Text;
                string strInspectDateFrom = Convert.ToDateTime(this.uDateInspectDateFrom.Value).ToString("yyyy-MM-dd");
                string strInspectDateTo = Convert.ToDateTime(this.uDateInspectDateTo.Value).ToString("yyyy-MM-dd");
                //검색, 데이터테이블 설정
                DataTable dtHeader = clsMSAH.mfReadQATMSAHRaR(strPlantCode, strMeasureToolCode, strInspectDateFrom, strInspectDateTo, m_resSys.GetString("SYS_LANG"));
                //DataBind
                this.uGrid1.DataSource = dtHeader;

                this.uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                //검사결과가 없을경우
                if (this.uGrid1.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M000202", "M000204", Infragistics.Win.HAlign.Right);

                    return;
                }
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001022", "M001044", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if(this.uCheckCompleteCalc.Enabled == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001022", "M000601", Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    //Header BL
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                    QRPQAT.BL.QATMSA.MSAH clsMSAH = new QRPQAT.BL.QATMSA.MSAH();
                    brwChannel.mfCredentials(clsMSAH);
                    //Detail BL
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAD), "MSAD");
                    QRPQAT.BL.QATMSA.MSAD clsMSAD = new QRPQAT.BL.QATMSA.MSAD();
                    brwChannel.mfCredentials(clsMSAD);


                    DataTable dtHeader = clsMSAH.mfSetDataInfoRegist();
                    DataTable dtDetail = clsMSAD.mfSetDataInfoRegist();
                    DataRow dr = dtHeader.NewRow();

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    dr["PlantCode"] = this.uTextHiddenPlantCode.Text;
                    dr["ProjectCode"] = this.uTextProjectCode.Text;
                    if (this.uCheckCompleteCalc.Checked == true)
                    {
                        dr["RNRAnalysisFlag"] = "T";
                    }
                    else
                    {
                        dr["RNRAnalysisFlag"] = "F";
                    }
                    dr["RnRAnalysisDate"] = this.uDateRnRAnalysisDate.Value.ToString();
                    dr["EquipmentVariation"] = this.uTextEquipmentVariation.Text;
                    dr["EquipmentPercent"] = this.uTextEquipmentPercent.Text;
                    dr["AppraiserVariation"] = this.uTextAppraiserVariation.Text;
                    dr["AppraiserPercent"] = this.uTextAppraiserPercent.Text;
                    dr["RNRValue"] = this.uTextRNRValue.Text;
                    dr["RNRPercent"] = this.uTextRNRPercent.Text;
                    //dr["RnRAnalysisRemark"] = this.uTextRnRAnalysisRemark.Text;

                    dtHeader.Rows.Add(dr);

                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        dr = dtDetail.NewRow();
                        dr["PlantCode"] = this.uTextHiddenPlantCode.Text;
                        dr["ProjectCode"] = this.uTextProjectCode.Text;
                        dr["ItemSeq"] = this.uGrid2.Rows[i].Cells["ItemSeq"].Value;
                        dr["EquipmentVariation"] = this.uGrid2.Rows[i].Cells["EquipmentVariation"].Value;
                        dr["EquipmentPercent"] = this.uGrid2.Rows[i].Cells["EquipmentPercent"].Value;
                        dr["AppraiserVariation"] = this.uGrid2.Rows[i].Cells["AppraiserVariation"].Value;
                        dr["AppraiserPercent"] = this.uGrid2.Rows[i].Cells["AppraiserPercent"].Value;
                        dr["RNRValue"] = this.uGrid2.Rows[i].Cells["RNRValue"].Value;
                        dr["RNRPercent"] = this.uGrid2.Rows[i].Cells["RNRPercent"].Value;
                        dr["AccuracyValue"] = this.uGrid3.Rows[i].Cells["AccuracyValue"].Value;
                        dr["StabilityValue"] = this.uGrid3.Rows[i].Cells["StabilityValue"].Value;

                        dtDetail.Rows.Add(dr);
                    }
                    

                    string strErrRtn = clsMSAH.mfSaveQATMSAHRnR(dtHeader, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtDetail);

                    // 팦업창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과에 따른 메세지 박스
                    if (ErrRtn.ErrNum == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);

                        // 리스트 갱신
                        mfSearch();
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 140);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 40;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmQAT0022_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 계측기 검색
        //개측기 검색 팝업 호출
        private void uTextMeasureToolCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0006 frmMeasure = new frmPOP0006();
                frmMeasure.PlantCode = this.uComboSearchPlant.Value.ToString();
                frmMeasure.ShowDialog();

                this.uTextSearchMeasureToolCode.Text = frmMeasure.MeasureToolCode;
                this.uTextSearchMeasureToolName.Text = frmMeasure.MeasureToolName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //개측기코드에서 BackSpace or Delete키 눌릴경우 코드와 이름을 같이 삭제
        private void uTextMeasureToolCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextSearchMeasureToolCode.Text = "";
                    this.uTextSearchMeasureToolName.Text = "";
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 상세 검색
        /// <summary>
        /// Header Grid 검색 메서드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //텍스트박스 초기화
                InitValue();
                //선택 Row 고정
                e.Row.Fixed = true;

                ResourceSet m_sysRes = new ResourceSet(SysRes.SystemInfoRes);
                //BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsMSAH = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsMSAH);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //Parameter 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strProjectCode = e.Row.Cells["ProjectCode"].Value.ToString();
                //검색, DataTable 설정
                DataTable dtHeader = clsMSAH.mfReadQATMSAHDetail(strPlantCode, strProjectCode, m_sysRes.GetString("SYS_LANG"));

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtHeader.Rows.Count > 0)
                {
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                    }
                }
                //결과값 메칭
                this.uTextHiddenPlantCode.Text = dtHeader.Rows[0]["PlantCode"].ToString();
                this.uTextPlantName.Text = dtHeader.Rows[0]["PlantName"].ToString();
                this.uTextProjectCode.Text = dtHeader.Rows[0]["ProjectCode"].ToString();
                this.uTextProjectName.Text = dtHeader.Rows[0]["ProjectName"].ToString();
                this.uTextProcessName.Text = dtHeader.Rows[0]["ProcessName"].ToString();
                this.uTextMeasureToolCode.Text = dtHeader.Rows[0]["MeasureToolCode"].ToString();
                this.uTextMeasureToolName.Text = dtHeader.Rows[0]["MeasureToolName"].ToString();
                this.uTextMaterialCode.Text = dtHeader.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtHeader.Rows[0]["MaterialName"].ToString();
                //this.uTextAccuracyFlag.Text = dtHeader.Rows[0]["AccuracyFlag"].ToString();
                this.uCheckAccuracyFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["AccuracyFlag"]);
                this.uTextAccuracyValue.Text = dtHeader.Rows[0]["AccuracyValue"].ToString();
                //this.uTextStabilityFlag.Text = dtHeader.Rows[0]["StabilityFlag"].ToString();
                this.uCheckStabilityFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["StabilityFlag"]);
                this.uTextRepeatCount.Text = dtHeader.Rows[0]["RepeatCount"].ToString();
                this.uTextPartCount.Text = dtHeader.Rows[0]["PartCount"].ToString();
               // this.uTextInspectItemDesc.Text = dtHeader.Rows[0]["InspectItemDesc"].ToString();
                this.uTextMeasureCount.Text = dtHeader.Rows[0]["MeasureCount"].ToString();
                this.uTextInspectDesc.Text = dtHeader.Rows[0]["InspectDesc"].ToString();
                this.uTextUnitCode.Text = dtHeader.Rows[0]["UnitCode"].ToString();

                if (dtHeader.Rows[0]["UpperSpec"] != DBNull.Value)
                    this.uTextUpperSpec.Text = dtHeader.Rows[0]["UpperSpec"].ToString();
                else
                    this.uTextUpperSpec.Text = "0";
                if (dtHeader.Rows[0]["LowerSpec"] != DBNull.Value)
                    this.uTextLowerSpec.Text = dtHeader.Rows[0]["LowerSpec"].ToString();
                else
                    this.uTextLowerSpec.Text = "0";
                this.uTextSpecRange.Text = dtHeader.Rows[0]["SpecRange"].ToString();

                this.uTextTolerance.Text = dtHeader.Rows[0]["Tolerance"].ToString();

                this.uTextRRValue.Text = dtHeader.Rows[0]["RRValueChangeValue"].ToString();
                this.uTextEquipmentVariation.Text = dtHeader.Rows[0]["EquipmentVariation"].ToString();
                this.uTextEquipmentPercent.Text = dtHeader.Rows[0]["EquipmentPercent"].ToString();
                this.uTextAppraiserVariation.Text = dtHeader.Rows[0]["AppraiserVariation"].ToString();
                this.uTextAppraiserPercent.Text = dtHeader.Rows[0]["AppraiserPercent"].ToString();
                this.uTextRNRValue.Text = dtHeader.Rows[0]["RNRValue"].ToString();
                this.uTextRNRPercent.Text = dtHeader.Rows[0]["RNRPercent"].ToString();
                //this.uTextRnRAnalysisRemark.Text = dtHeader.Rows[0]["RnRAnalysisRemark"].ToString();
                this.uCheckCompleteCalc.Checked = Convert.ToBoolean(dtHeader.Rows[0]["RNRAnalysisFlag"]);
                if (this.uCheckCompleteCalc.Checked == true)
                {
                    this.uCheckCompleteCalc.Checked = true;
                    this.uCheckCompleteCalc.Enabled = false;
                }
                else
                {
                    this.uCheckCompleteCalc.Checked = false;
                    this.uCheckCompleteCalc.Enabled = true;
                }

                //분석 완료 체크에 따른 분석결과 수정/불가 변경
                //if (this.uCheck1.Checked == true)
                //{
                //    this.uTextRnRAnalysisRemark.ReadOnly = true;
                //    this.uTextRnRAnalysisRemark.Appearance.BackColor = Color.Gainsboro;
                //}
                //else
                //{
                //    this.uTextRnRAnalysisRemark.ReadOnly = false;
                //    this.uTextRnRAnalysisRemark.Appearance.BackColor = Color.White;
                //}
                
                //MSAD 검색
                DataTable dtMSAD = SearchMSAD(strPlantCode, strProjectCode);

                if (dtMSAD.Rows.Count > 0)
                {
                    //상세결과 분석 그리드
                    this.uGrid2.DataSource = dtMSAD;
                    this.uGrid2.DataBind();
                    //안정성/정확성 그리드
                    this.uGrid3.DataSource = dtMSAD;
                    this.uGrid3.DataBind();
                }
                else
                {
                    CalcRnR(strPlantCode, strProjectCode);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// MSAD검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <returns></returns>
        private DataTable SearchMSAD(string strPlantCode, string strProjectCode)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAD), "MSAD");
                QRPQAT.BL.QATMSA.MSAD clsMSAD = new QRPQAT.BL.QATMSA.MSAD();
                brwChannel.mfCredentials(clsMSAD);

                dtRtn = clsMSAD.mfReadQATMSAD_RnR(strPlantCode, strProjectCode);

                return dtRtn;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
                throw (ex);
            }
            finally
            { }
        }

        private void CalcRnR(string strPlantCode, string strProjectCode)
        {
            try
            {
                int intMachineNumber = Convert.ToInt32(uTextMeasureCount.Text);     //계측기수
                int intPartNumber = Convert.ToInt32(uTextPartCount.Text);           //부품수
                int intRepeatNumber = Convert.ToInt32(uTextRepeatCount.Text);       //반복수
                int intCount = 0;
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAData), "MSAData");
                QRPQAT.BL.QATMSA.MSAData clsData = new QRPQAT.BL.QATMSA.MSAData();
                brwChannel.mfCredentials(clsData);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "분석중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtData = clsData.mfReadQATMSAData(strPlantCode, strProjectCode, 0, m_resSys.GetString("SYS_LANG"));
                double[,,] arrExpData = new double[intMachineNumber, intPartNumber, intRepeatNumber];

                for (int i = 0; i < intMachineNumber; i++)
                {
                    for (int j = 0; j < intPartNumber; j++)
                    {
                        for (int k = 0; k < intRepeatNumber; k++)
                        {
                            arrExpData[i, j, k] = Convert.ToDouble(dtData.Rows[intCount]["MeasureValue"].ToString());
                            intCount++;
                        }
                    }
                }
                QRPSTA.STAMSA msaRnR = new QRPSTA.STAMSA();

                msaRnR.mfSetDataArraySize(intMachineNumber, intPartNumber, intRepeatNumber, Convert.ToDouble(this.uTextLowerSpec.Text), Convert.ToDouble(this.uTextUpperSpec.Text), arrExpData, Convert.ToDouble(uTextAccuracyValue.Text));
                
                //안정성 및 정확성 체크값에 맞게 인자를 넘겨주도록 수정
                msaRnR.mfCalcRnRAnalysis(this.uCheckStabilityFlag.Checked, this.uCheckAccuracyFlag.Checked);

                uTextEquipmentVariation.Text = msaRnR.EV.ToString("#,###.####");
                uTextEquipmentPercent.Text = msaRnR.EVPercent.ToString("#,###.####");
                uTextAppraiserVariation.Text = msaRnR.AV.ToString("#,###.####");
                uTextAppraiserPercent.Text = msaRnR.AVPercent.ToString("#,###.####");
                uTextRNRValue.Text = msaRnR.RnR.ToString("#,###.####");
                uTextRNRPercent.Text = msaRnR.RnRPercent.ToString("#,###.####");

                //기기별 EV, %EV, AV, %AV, RnR, %RnR
                //기기별로 Loop돌리기
                //msaRnR.arrEVWithoutMachine, msaRnR.arrEVPercentWithoutMachine
                //msaRnR.arrAVWithoutMachine, msaRnR.arrAVPercentWithoutMachine
                //msaRnR.arrRnRWithoutMachine, msaRnR.arrRnRPercentWithoutMachine
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["EquipmentVariation"].Value = msaRnR.arrEVWithoutMachine[i];
                    this.uGrid2.Rows[i].Cells["EquipmentPercent"].Value = msaRnR.arrEVPercentWithoutMachine[i];
                    this.uGrid2.Rows[i].Cells["AppraiserVariation"].Value = msaRnR.arrAVWithoutMachine[i];
                    this.uGrid2.Rows[i].Cells["AppraiserPercent"].Value = msaRnR.arrAVPercentWithoutMachine[i];
                    this.uGrid2.Rows[i].Cells["RNRValue"].Value = msaRnR.arrRnRWithoutMachine[i];
                    this.uGrid2.Rows[i].Cells["RNRPercent"].Value = msaRnR.arrRnRPercentWithoutMachine[i];
                    this.uGrid3.Rows[i].Cells["StabilityValue"].Value = msaRnR.arrStability[i];
                    this.uGrid3.Rows[i].Cells["AccuracyValue"].Value = msaRnR.arrAccuracy[i];
                }

                //기기별 안정성, 정확성
                //msaRnR.arrStability, msaRnR.arrAccuracy

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        #endregion

        private void uButtonReCalc_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //내용이 없는경우 Pass
                if (this.uTextProjectCode.Text.Equals(string.Empty) || this.uTextHiddenPlantCode.Text.Equals(string.Empty))
                    return;

                if (this.uCheckCompleteCalc.Enabled == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001007", "M000602", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {   
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001007", "M000116", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        CalcRnR(uTextHiddenPlantCode.Text, uTextProjectCode.Text);

                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M001007", "M000115", Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
    }
}
