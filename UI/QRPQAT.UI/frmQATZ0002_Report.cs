﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.VisualBasic;
using System.Collections;
using System.IO;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;

namespace QRPQAT.UI
{
    public partial class frmQATZ0002_Report : Form
    {
        public frmQATZ0002_Report()
        {
            InitializeComponent();
        }
        public frmQATZ0002_Report(DataTable dtCertiH, DataTable dtSetupCondition, DataTable dtCertiItem, DataTable dtMainSubEquip)
        {
            InitializeComponent();
            InitVewer();
            WriteReport(dtCertiH, dtSetupCondition, dtCertiItem, dtMainSubEquip);
        }
        private void InitVewer()
        {
            #region Viewer 초기화
            rptViewDoc.Width = this.Width;
            rptViewDoc.Height = this.Height;
            rptViewDoc.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            #endregion
        }

        private void WriteReport(DataTable dtCertiH, DataTable dtSetupCondition, DataTable dtCertiItem, DataTable dtMainSubEquip)
        {
            #region 리포트 그리기

            //설비인증요청 헤더 정보조회 매서드 실행

            
            //뷰어에있는 Document 초기화
            rptViewDoc.Document = null;

            //설비인증요청보고서 그리기

            rptQATZ0002_Main rptEquipCerti = new rptQATZ0002_Main(dtSetupCondition, dtCertiItem, dtMainSubEquip);
            rptEquipCerti.DataSource = dtCertiH;

            rptEquipCerti.Run();
            rptViewDoc.Document = rptEquipCerti.Document;

            SaveReport_Export("E");

            #endregion
        }

        /// <summary>
        /// 리포트 PDF로 변환하여 지정된 경로에 저장시킨다.
        /// </summary>
        private void SaveReport_Export(string strGubun)
        {
            try
            {
                //
                object export = null;
                export = new PdfExport();

                //Report디렉토리지정
                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                //디렉토리 유무확인
                DirectoryInfo di = new DirectoryInfo(m_strQRPBrowserPath);

                //지정된경로에 폴더가 없을 경우 폴더생성
                if (di.Exists.Equals(false))
                {
                    di.Create();
                }

                if (strGubun.Equals("S"))
                {
                    //파일여부 체크 존재한다면 
                    if (File.Exists(m_strQRPBrowserPath + "Setup.pdf"))
                    {
                        File.Delete(m_strQRPBrowserPath + "Setup.pdf");
                    }
                    //((PdfExport)export).Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Setup.pdf");

                    PdfExport pd = new PdfExport();
                    pd.Security.Permissions = PdfPermissions.AllowModifyAnnotations;
                    pd.Security.Permissions = PdfPermissions.AllowAssembly;
                    pd.Security.Permissions = PdfPermissions.AllowCopy;
                    pd.Security.Permissions = PdfPermissions.AllowModifyContents;
                    pd.Security.Permissions = PdfPermissions.AllowAccessibleReaders;

                    pd.Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Setup.pdf");
                    pd.Dispose();

                }
                else
                {
                    //파일여부 체크 존재한다면 
                    if (File.Exists(m_strQRPBrowserPath + "Req.pdf"))
                    {
                        File.Delete(m_strQRPBrowserPath + "Req.pdf");
                    }
                    //((PdfExport)export).Export(this.rptViewDoc.Document, m_strQRPBrowserPath  + "Req.pdf");

                    PdfExport pd = new PdfExport();
                    pd.Security.Permissions = PdfPermissions.AllowModifyAnnotations;
                    pd.Security.Permissions = PdfPermissions.AllowAssembly;
                    pd.Security.Permissions = PdfPermissions.AllowCopy;
                    pd.Security.Permissions = PdfPermissions.AllowModifyContents;
                    pd.Security.Permissions = PdfPermissions.AllowAccessibleReaders;
                    pd.Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Req.pdf");
                    pd.Dispose();
                }

                //엑셀변환
                //export = new XlsExport();
                //((XlsExport)export).Export(this.rptViewDoc.Document, "C:\\Users\\Jong\\Desktop\\버림\\넴.xls");

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
    }
}
