﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 고객관리                                              */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQAT0002_P1.cs                                      */
/* 프로그램명   : 고객불만관리 Mail등록 팝업창                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-07-09                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                                                                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPQAT.UI
{
    public partial class frmQAT0002_P1 : Form
    {
        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;

        private string strClaimNo;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string ClaimNo
        {
            get { return strClaimNo; }
            set { strClaimNo = value; }
        }


        public frmQAT0002_P1()
        {
            InitializeComponent();
        }

        private void frmQAT0002_P1_Load(object sender, EventArgs e)
        {
            InitGird();
            InitCombo();
            InitButton();
            InitGroupBox();

            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfSetFormLanguage(this);

            Search_Email(PlantCode, ClaimNo, "F");
            
        }

        #region Init
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGird()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                #region Email

                wGrid.mfInitGeneralGrid(this.uGridMailList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.None
                   , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridMailList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMailList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridMailList, 0);


                this.uGridMailList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridMailList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonRowDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonSave, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);
                wButton.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonOK.Click += new EventHandler(uButtonOK_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }

        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxEmail, GroupBoxType.INFO, "수신인등록", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                this.uGroupBoxEmail.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxEmail.HeaderAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitCombo()
        {
            try
            {
                #region dtDept
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                DataTable dtDept = new DataTable();

                // 공정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                // 귀책부서
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                dtDept = clsDept.mfReadSYSDeptForCombo(this.strPlantCode.ToString(), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, true, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                                        , "DeptCode", "DeptName", dtDept);
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        /// <summary>
        /// 작성완료 Mail 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strGubun"> W: 작성완료메일리스트 , F : 분석완료 메일리스트</param>
        /// <param name="strLang">언어</param>
        private void Search_Email(string strPlantCode, string strClaimNo, string strGubun)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimEMailUser), "CustomerClaimEMailUser");
                QRPQAT.BL.QATCLM.CustomerClaimEMailUser clsEmail = new QRPQAT.BL.QATCLM.CustomerClaimEMailUser();
                brwChannel.mfCredentials(clsEmail);

                DataTable dtRtn = clsEmail.mfReadQATCustomerClaimEMailUser(strPlantCode, strClaimNo, strGubun, m_resSys.GetString("SYS_LANG"));

                this.uGridMailList.DataSource = dtRtn;
                this.uGridMailList.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMailList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region SearchUser
        private void uGridEmail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                if (e.Cell.Column.Key.Equals("UserID"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (PlantCode.Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    string strLang = m_resSys.GetString("SYS_LANG");
                    if (PlantCode != frmPOP.PlantCode)
                    {
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang), msg.GetMessge_Text("M001254", strLang) + strPlantCode + msg.GetMessge_Text("M000009", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["UserID"].Value = string.Empty;
                        e.Cell.Row.Cells["UserName"].Value = string.Empty;
                        e.Cell.Row.Cells["DeptCode"].Value = string.Empty;
                        e.Cell.Row.Cells["DeptName"].Value = string.Empty;
                        e.Cell.Row.Cells["EMail"].Value = string.Empty;
                    }
                    else
                    {
                        e.Cell.Row.Cells["UserID"].Value = frmPOP.UserID;
                        e.Cell.Row.Cells["UserName"].Value = frmPOP.UserName;
                        e.Cell.Row.Cells["DeptName"].Value = frmPOP.DeptName;
                        e.Cell.Row.Cells["DeptCode"].Value = frmPOP.DeptCode;
                        e.Cell.Row.Cells["EMail"].Value = frmPOP.EMail;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridMailList.ActiveCell == null)
                    return;


                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridMailList.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (!uCell.Text.Equals(string.Empty))
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                                // BL 연결
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                brwChannel.mfCredentials(clsUser);

                                DataTable dtUserInfo = clsUser.mfReadSYSUser("", uCell.Text, m_resSys.GetString("SYS_LANG"));

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    uCell.Row.Cells["UserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                    uCell.Row.Cells["DeptCode"].Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                                    uCell.Row.Cells["DeptName"].Value = dtUserInfo.Rows[0]["DeptName"].ToString();
                                    uCell.Row.Cells["EMail"].Value = dtUserInfo.Rows[0]["Email"].ToString();
                                }
                                else
                                {
                                    DialogResult Result = new DialogResult();
                                    WinMessageBox msg = new WinMessageBox();
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001095", "M000366", "M000368"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridMailList.ActiveCell;
                        if (uCell.Text.Equals(string.Empty))
                            return;

                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (uCell.Text.Length <= 1 || uCell.SelText == uCell.Text)
                            {
                                uCell.Row.Delete(false);
                            }
                        }
                    }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uGridEmail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                    e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridMailList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridMailList.Rows.Count == 0)
                {
                    //저장 할 정보가 없습니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M001047", Infragistics.Win.HAlign.Right);
                    return;
                }

                this.uGridMailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimEMailUser), "CustomerClaimEMailUser");
                QRPQAT.BL.QATCLM.CustomerClaimEMailUser clsMail = new QRPQAT.BL.QATCLM.CustomerClaimEMailUser();
                brwChannel.mfCredentials(clsMail);

                DataTable dtRtn = clsMail.mfSetDataInfo();
                DataRow drRow;

                for (int i = 0; i < this.uGridMailList.Rows.Count; i++)
                {
                    if (this.uGridMailList.Rows[i].Hidden)
                        continue;

                    if (this.uGridMailList.Rows[i].RowSelectorAppearance.Image == null)
                        continue;

                    if (this.uGridMailList.Rows[i].GetCellValue("UserID").ToString().Equals(string.Empty) 
                        || this.uGridMailList.Rows[i].GetCellValue("UserName").ToString().Equals(string.Empty))
                    {
                        //ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001227", "M000054", Infragistics.Win.HAlign.Right);

                        this.uGridMailList.ActiveCell = this.uGridMailList.Rows[i].Cells["UserID"];
                        this.uGridMailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }

                    drRow = dtRtn.NewRow();

                    drRow["Gubun"] = "F";
                    drRow["Seq"] = this.uGridMailList.Rows[i].RowSelectorNumber;
                    drRow["MailUserID"] = this.uGridMailList.Rows[i].GetCellValue("UserID");
                    drRow["EtcDesc"] = this.uGridMailList.Rows[i].GetCellValue("EtcDesc");

                    dtRtn.Rows.Add(drRow);
                }

                //if (dtRtn.Rows.Count == 0)
                //{
                //    //저장 할 정보가 없습니다.
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                               , "M001264", "M001228", "M001047", Infragistics.Win.HAlign.Right);
                //    return;
                //}

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                   "M001264", "M001053", "M000936",
                                   Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = clsMail.mfSaveQATCustomerClaimEMailUser(dtRtn, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), PlantCode, ClaimNo, "F");

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    /////////////

                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        this.Close();
                        //Search_Email(PlantCode, ClaimNo, "F");
                    }
                    else
                    {
                        string strMsg = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = msg.GetMessge_Text("M000953",m_resSys.GetString("SYS_LANG"));
                        else
                            strMsg = ErrRtn.ErrMessage;
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001037",m_resSys.GetString("SYS_LANG")), strMsg,
                                                     Infragistics.Win.HAlign.Right);
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uButtonRowDelete_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.uGridMailList.Rows.Count; i++)
            {
                if (this.uGridMailList.Rows[i].Hidden)
                    continue;

                if (Convert.ToBoolean(this.uGridMailList.Rows[i].Cells["Check"].Value))
                    this.uGridMailList.Rows[i].Hidden = true;
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {

                // 부서정보 콤보정보가 0인경우 Return
                if (this.uComboDept.Items.Count == 0)
                    return;

                // 부서코드 저장
                //string strDept = this.uComboDept.Value == null ? string.Empty : this.uComboDept.Value.ToString();
                string strDept = string.Empty; // "'1010','1008'";//,'D','E','H'";


                //Combo MultiSelect 선택된 정보 저장방법
                for (int i = 0; i < this.uComboDept.Items.Count; i++)
                {
                    if (this.uComboDept.Items[i].CheckState == CheckState.Checked)
                    {
                        if (strDept.Equals(string.Empty))
                            strDept = "'" + this.uComboDept.Items[i].DataValue.ToString() + "'";
                        else
                            strDept = strDept + ", '" + this.uComboDept.Items[i].DataValue.ToString() + "'";

                    }
                }

                // 공백이면 Return
                if (strDept.Equals(string.Empty))
                    return;

                // 사용자 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자 정보 조회 매서드 실행
                DataTable dtUser = clsUser.mfReadSYSUser_InDept(PlantCode, strDept, m_resSys.GetString("SYS_LANG"));

                // Resource 해제
                m_resSys.Close();
                clsUser.Dispose();

                // 유저정보중 Email정보가 있는 정보확인
                if (dtUser.Select("Email <> ''").Count() != 0)
                    dtUser = dtUser.Select("Email <> ''").CopyToDataTable();
                else
                    dtUser.Clear();

                // Email정보가 있는 정보만 Grid에 Bind
                this.uGridMailList.DataSource = dtUser;
                this.uGridMailList.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
