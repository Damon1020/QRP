﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 신뢰성 검사 관리                                      */
/* 프로그램ID   : frmQATZ0011.cs                                        */
/* 프로그램명   : 기본분석관리                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-20 : ~~~~~ 추가 (권종구)                      */
/* 수정이력     : 2011-10-26 : 전체 수정 (이종호)                       */
/*                2012-02-24 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
//--파일업로드를위한 참조
using System.IO;


namespace QRPQAT.UI
{
    public partial class frmQATZ0011 : Form, IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0011()
        {
            InitializeComponent();
        }

        private void frmQATZ0011_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 여부 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        private void frmQATZ0011_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                else
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void frmQATZ0011_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정
            titleArea.mfSetLabelText("기본분석 관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Control 초기화 Method
            SetToolAuth();
            InitLabel();
            InitGroupBox();
            InitComboBox();
            InitGrid();
            InitEtc();
            AcceptAnalyzeInfo();

            // 그리드 데이터 Load
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                //this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                // 등록자 기본값 설정
                this.uTextReqUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextReqUserName.Text = m_resSys.GetString("SYS_USERNAME");

                // MaxLength 지정
                this.uTextReqUserID.MaxLength = 20;
                this.uTextAnalysisObject.MaxLength = 100;
                this.uTextComment.MaxLength = 200;                
                this.uTextLotNo.MaxLength = 50;
                this.uTextAnalyzeResult.MaxLength = 200;

                //this.uTextSearchProductCode.MaxLength = 20;

                this.uTextReqUserID.ImeMode = ImeMode.Disable;
                this.uTextReqUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextLotNo.ImeMode = ImeMode.Disable;
                this.uTextLotNo.CharacterCasing = CharacterCasing.Upper;
                this.uTextAnalyzeUserID.ImeMode = ImeMode.Disable;
                this.uTextAnalyzeUserID.CharacterCasing = CharacterCasing.Upper;

                // 품질부서의 경우만 보이도록 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                    {
                        this.uLabelReceipFinishFlag.Visible = true;
                        this.uCheckReceipFinishFlag.Visible = true;
                        this.uLabelReceipReturnFlag.Visible = true;
                        this.uCheckReceipReturnFlag.Visible = true;
                    }
                    else
                    {
                        this.uLabelReceipFinishFlag.Visible = false;
                        this.uCheckReceipFinishFlag.Visible = false;
                        this.uLabelReceipReturnFlag.Visible = false;
                        this.uCheckReceipReturnFlag.Visible = false;
                    }
                }
                else
                {
                    this.uLabelReceipFinishFlag.Visible = false;
                    this.uCheckReceipFinishFlag.Visible = false;
                    this.uLabelReceipReturnFlag.Visible = false;
                    this.uCheckReceipReturnFlag.Visible = false;
                }

                ////if (m_resSys.GetString("SYS_DEPTCODE").Equals("S5100"))
                ////{
                ////    this.uLabelReceipFinishFlag.Visible = true;
                ////    this.uCheckReceipFinishFlag.Visible = true;
                ////    this.uLabelReceipReturnFlag.Visible = true;
                ////    this.uCheckReceipReturnFlag.Visible = true;
                ////}
                ////else
                ////{
                ////    this.uLabelReceipFinishFlag.Visible = false;
                ////    this.uCheckReceipFinishFlag.Visible = false;
                ////    this.uLabelReceipReturnFlag.Visible = false;
                ////    this.uCheckReceipReturnFlag.Visible = false;
                ////}

                this.uNumReqQty.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeBoth;
                this.uNumReqQty.MinValue = 0;
                this.uNumReqQty.AutoSize = true;
                this.uNumReqQty.MaskInput = "nn,nnn";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxRequestInfo, GroupBoxType.INFO, "의뢰정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxGeneral, GroupBoxType.DETAIL, "일반분석", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxFault, GroupBoxType.DETAIL, "불량분석", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxAcceptAnalyzeInfo, GroupBoxType.INFO, "접수/분석 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxRequestInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxRequestInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxGeneral.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxGeneral.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxFault.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxFault.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxAcceptAnalyzeInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxAcceptAnalyzeInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // ContentsArea 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchOccurDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelManageNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRequestDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAnalysisDivision, "분석구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAnalysisPurpose, "분석목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestAmount, "의뢰수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile1, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelComplete, "분석의뢰완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReceipReturnFlag, "접수반려", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceipFinishFlag, "접수완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAnalyzeResult, "분석결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAnalyzeUser, "분석담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile2, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAnalysisComplete, "분석완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelIssueType, "Issue유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장
                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                ////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                ////brwChannel.mfCredentials(clsPlant);

                ////DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                ////// 검색조건공장콤보
                ////wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                ////    , "", "", "전체", "PlantCode", "PlantName", dtPlant);

                ////// 입력공장콤보
                ////wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                ////    , "", "", "선택", "PlantCode", "PlantName", dtPlant);

                // 분석구분
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtAnalyzeDivision = clsComCode.mfReadCommonCode("C0027", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboAnalysisType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtAnalyzeDivision);

                // 검색조건 고객
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerCombo = new DataTable();
                if (dtCustomer.Rows.Count > 0)
                    dtCustomerCombo = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "전체", "CustomerCode", "CustomerName", dtCustomerCombo);

                //검색조건 - Package 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                                       , "Package", "ComboName", dtPackage);

                DataTable dtIssueType = clsComCode.mfReadCommonCode("C0063", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboIssueType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtIssueType);

                // 입력용
                //wCombo.mfSetComboEditor(this.uTextPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                //                       , "Package", "ComboName", dtPackage);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridBasicAnalysis, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정                                
                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "BasicNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "AnalysisTypeName", "분석구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ReqDate", "의뢰일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ReqQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "IssueType", "Issue유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "AnalyzeUserName", "분석담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ReceipFinishDate", "접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "AnalysisCompleteDate", "완료일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridBasicAnalysis, 0, "ReqState", "진행상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // Set FontSize
                this.uGridBasicAnalysis.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridBasicAnalysis.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                // 그리드 수정불가 상태로
                this.uGridBasicAnalysis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // MaskInput 설정
                this.uGridBasicAnalysis.DisplayLayout.Bands[0].Columns["ReqQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeBoth;
                this.uGridBasicAnalysis.DisplayLayout.Bands[0].Columns["ReqQty"].PromptChar = ' ';
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //의뢰일 From 이 To 보다 크면 메세지 박스를 보여준다. //
                if (Convert.ToDateTime(this.uDateSearchOccurFromDate.Value) > Convert.ToDateTime(this.uDateSearchOccurToDate.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000200", Infragistics.Win.HAlign.Right);

                    this.uDateSearchOccurFromDate.DropDown();
                    return;
                }

                //--공장, 제품코드 , 의뢰일 저장 
                //string strPlantCode = this.uComboSearchPlant.Value.ToString();
                //string strProduct = this.uTextSearchProductCode.Text;
                //string strFromDate = Convert.ToDateTime(this.uDateSearchOccurFromDate.Value).ToString("yyyy-MM-dd");
                //string strToDate = Convert.ToDateTime(this.uDateSearchOccurToDate.Value).ToString("yyyy-MM-dd");

                //string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strFromDate = Convert.ToDateTime(this.uDateSearchOccurFromDate.Value).ToString("yyyy-MM-dd");
                string strToDate = Convert.ToDateTime(this.uDateSearchOccurToDate.Value).ToString("yyyy-MM-dd");
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strReqUserID = this.uTextSearchReqUserID.Text;
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                QRPQAT.BL.QATREL.BasicAnalysis clsBasicAnalysis = new QRPQAT.BL.QATREL.BasicAnalysis();

                // 팝업 창을 띄운다 .//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // -- 기본분석관리 조회 매서드 실행 
                DataTable dtBasicAnalysis = clsBasicAnalysis.mfReadBasicAnalysis(strPlantCode, strPackage, strCustomerCode, strReqUserID, strFromDate, strToDate, m_resSys.GetString("SYS_LANG"));

                //그리드에 데이터 바인드
                this.uGridBasicAnalysis.DataSource = dtBasicAnalysis;
                this.uGridBasicAnalysis.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                System.Windows.Forms.DialogResult result;
                if (dtBasicAnalysis.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridBasicAnalysis, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    QRPCOM.QRPUI.CommonControl a = new CommonControl();
                    if (!a.mfCheckValidValueBeforSave(this))
                        return;

                    if (!CheckSave())
                        return;
                    else
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        string strMes = "M000936";
                        //분석의뢰시 Package정보 검색 후 메세지 알림
                        if (this.uCheckAnalysisReqFlag.Enabled)
                        {
                             brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            QRPMAS.BL.MASMAT.Product clsPack = new QRPMAS.BL.MASMAT.Product();
                            brwChannel.mfCredentials(clsPack);
                            DataTable dtPack = clsPack.mfReadMASProductWithPackageCombo(m_resSys.GetString("SYS_PLANTCODE"), this.uTextPackage.Text, m_resSys.GetString("SYS_LANG"));

                            if (!(dtPack.Rows.Count > 0))
                            {
                                strMes = "M001321";
                            }
                        }
                        if (this.uTextReqFileName.Text.Contains("/")
                            || this.uTextReqFileName.Text.Contains("#")
                            || this.uTextReqFileName.Text.Contains("*")
                            || this.uTextReqFileName.Text.Contains("<")
                            || this.uTextReqFileName.Text.Contains(">"))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                              , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        if (this.uTextResultFileName.Text.Contains("/")
                            || this.uTextResultFileName.Text.Contains("#")
                            || this.uTextResultFileName.Text.Contains("*")
                            || this.uTextResultFileName.Text.Contains("<")
                            || this.uTextResultFileName.Text.Contains(">"))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                              , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }

                        //if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                    "M001264", "M001053", strMes,
                        //                    Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        //{
                            
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                            QRPQAT.BL.QATREL.BasicAnalysis clsBA = new QRPQAT.BL.QATREL.BasicAnalysis();
                            brwChannel.mfCredentials(clsBA);

                            DataTable dtSave = clsBA.mfDataSetInfo();
                            DataRow drRow = dtSave.NewRow();
                            //drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                            drRow["PlantCode"] = m_resSys.GetString("SYS_PLANTCODE");
                            drRow["BasicNo"] = this.uTextBasicNo.Text;
                            drRow["Package"] = this.uTextPackage.Text; 
                            drRow["ReqUserID"] = this.uTextReqUserID.Text;
                            drRow["ReqDate"] = Convert.ToDateTime(this.uDateRequestDate.Value).ToString("yyyy-MM-dd");
                            drRow["AnalysisType"] = this.uComboAnalysisType.Value.ToString();
                            drRow["AnalysisObject"] = this.uTextAnalysisObject.Text;

                            drRow["SEMFlag"] = this.uCheckSEM.Checked == true ? "T" : "F";
                            drRow["FTIRFlag"] = this.uCheckFTIR.Checked == true ? "T" : "F";
                            drRow["MoireFlag"] = this.uCheckMoire.Checked == true ? "T" : "F";
                            drRow["CurveFlag"] = this.uCheckCurve.Checked == true ? "T" : "F";
                            drRow["TracerFlag"] = this.uCheckTracer.Checked == true ? "T" : "F";
                            drRow["TDRFlag1"] = this.uCheckTDR1.Checked == true ? "T" : "F";
                            drRow["EtcFlag1"] = this.uCheckEtc1.Checked == true ? "T" : "F";
                            drRow["SATFlag"] = this.uCheckSAT.Checked == true ? "T" : "F";
                            drRow["XrayFlag"] = this.uCheckXray.Checked == true ? "T" : "F";
                            drRow["DECAPFlag"] = this.uCheckDECAP.Checked == true ? "T" : "F";
                            drRow["CrossSectionFlag"] = this.uCheckCrossSection.Checked == true ? "T" : "F";
                            drRow["CurveTracerFlag"] = this.uCheckCurveTracer.Checked == true ? "T" : "F";
                            drRow["TDRFlag2"] = this.uCheckTDR2.Checked == true ? "T" : "F";
                            drRow["EtcFlag2"] = this.uCheckEtc2.Checked == true ? "T" : "F";

                            drRow["LotNo"] = this.uTextLotNo.Text.ToUpper();
                            drRow["ReqQty"] = this.uNumReqQty.Value;
                            drRow["Comment"] = this.uTextComment.Text;
                            drRow["CustomerCode"] = this.uTextCustomerCode.Text;
                            drRow["CustomerName"] = this.uTextCustomerName.Text;
                            if (this.uTextReqFileName.Text.Contains(":\\"))
                            {
                                FileInfo FileInfo = new FileInfo(this.uTextReqFileName.Text); // 파일경로에 대한 정보를 읽는다
                                drRow["ReqFileName"] = "-Req-" + FileInfo.Name; //의뢰정보 첨부파일
                            }
                            else
                                drRow["ReqFileName"] = this.uTextReqFileName.Text; //의뢰정보 첨부파일

                            drRow["AnalysisReqFlag"] = this.uCheckAnalysisReqFlag.Checked == true ? "T" : "F";
                            drRow["ReceipReturnFlag"] = this.uCheckReceipReturnFlag.Checked == true ? "T" : "F";
                            drRow["ReceipFinishFlag"] = this.uCheckReceipFinishFlag.Checked == true ? "T" : "F";
                            drRow["AnalysisResult"] = this.uTextAnalyzeResult.Text;
                            drRow["AnalyzeUserID"] = this.uTextAnalyzeUserID.Text.ToUpper();
                            if (this.uTextResultFileName.Text.Contains(":\\"))
                            {
                                FileInfo FileInfo = new FileInfo(this.uTextResultFileName.Text); //파일경로에 대한 정보를 읽는다
                                drRow["ResultFileName"] = "-Ana-" + FileInfo.Name; //접수 분석 정보 첨부파일
                            }
                            else
                                drRow["ResultFileName"] = this.uTextResultFileName.Text;

                            if(this.uCheckEtc1.Checked)
                                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                            else if(this.uCheckEtc2.Checked)
                                drRow["EtcDesc"] = this.uTextEtcDesc2.Text;

                            drRow["Exterior1"] = this.uCheckExterior1.Checked == true ? "T" : "F";
                            drRow["Exterior2"] = this.uCheckExterior2.Checked == true ? "T" : "F";
                            drRow["ReceipFinishDate"] = this.uDateReceipFinish.Value == null ? "": this.uDateReceipFinish.DateTime.Date.ToString("yyyy-MM-dd");
                            drRow["ReceipFinishTime"] = this.uDateTimeReceipFinish.Value == null? "" : this.uDateTimeReceipFinish.DateTime.ToString("HH:mm:ss");
                            drRow["AnalysisCompleteFlag"] = this.uCheckAnalysisCompleteFlag.Checked == true ? "T" : "F";
                            drRow["AnalysisCompleteDate"] = this.uDateAnalysisComplete.Value == null ? "" : this.uDateAnalysisComplete.DateTime.Date.ToString("yyyy-MM-dd");
                            drRow["AnalysisCompleteTime"] = this.uDateTimeAnalysisComplete.Value == null ? "" :this.uDateTimeAnalysisComplete.DateTime.ToString("HH:mm:ss");
                            drRow["IssueType"] = this.uComboIssueType.Value.ToString();

                            dtSave.Rows.Add(drRow);

                            //  Show ProgressPopup
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            //   저장 매서드 호출
                            string strErrRtn = clsBA.mfSaveBasicAnalysis(dtSave, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                            // Close ProgressPopup
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // Decoding
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 결과검사
                            if (ErrRtn.ErrNum.Equals(0))
                            {
                                // 성공시 첨부파일 업로드 메소드 호출
                                FileUpload(ErrRtn.mfGetReturnValue(0));

                                //분석완료일 경우 의뢰자 분석담당자 메일 송신
                                if (this.uCheckAnalysisCompleteFlag.Checked && this.uCheckAnalysisCompleteFlag.Enabled)
                                {
                                    SendMail(ErrRtn.mfGetReturnValue(0), this.uTextAnalyzeUserID.Text, "Ana", this.uTextResultFileName);
                                    SendMail(ErrRtn.mfGetReturnValue(0), this.uTextReqUserID.Text, "Req", this.uTextReqFileName);
                                    
                                }

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001135", "M001037", "M000930",
                                                            Infragistics.Win.HAlign.Right);

                                mfSearch();
                            }
                            else
                            {
                                string strMessage = "";
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                    strMessage = "M000953";
                                else
                                    strMessage = ErrRtn.ErrMessage;
                                
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                             "M001135", "M001037", strMessage,
                                                             Infragistics.Win.HAlign.Right);
                            }
                        //}
                    }

                    #region 기존소스 주석처리
                    /*
                    //--- 분석 구분에 따라 값을 저장한다 --//
                    string strSEMSATFlag = "";
                    string strFTIRXrayFlag = "";
                    string strMoireDECAPFlag = "";
                    string strCurveCroSecFlag = "";
                    string strTracerCTFlag = "";
                    string strTDRFlag = "";
                    string strEtcFlag = "";

                    // 일반분석일 경우
                    if (this.uComboAnalysisType.Value.ToString() == "1")
                    {
                        strSEMSATFlag = this.uCheckSEM.Checked.ToString().ToUpper().Substring(0, 1);
                        strFTIRXrayFlag = this.uCheckFTIR.Checked.ToString().ToUpper().Substring(0, 1);
                        strMoireDECAPFlag = this.uCheckMoire.Checked.ToString().ToUpper().Substring(0, 1);
                        strCurveCroSecFlag = this.uCheckCurve.Checked.ToString().ToUpper().Substring(0, 1);
                        strTracerCTFlag = this.uCheckTracer.Checked.ToString().Substring(0, 1);
                        strTDRFlag = this.uCheckTDR1.Checked.ToString().ToUpper().Substring(0, 1);
                        strEtcFlag = this.uCheckEtc1.Checked.ToString().ToUpper().Substring(0, 1);
                    }
                    //불량분석일 경우
                    else if (this.uComboAnalysisType.Value.ToString() == "2")
                    {
                        strSEMSATFlag = this.uCheckSAT.Checked.ToString().ToUpper().Substring(0, 1);
                        strFTIRXrayFlag = this.uCheckXray.Checked.ToString().ToUpper().Substring(0, 1);
                        strMoireDECAPFlag = this.uCheckDECAP.Checked.ToString().ToUpper().Substring(0, 1);
                        strCurveCroSecFlag = this.uCheckCrossSection.Checked.ToString().ToUpper().Substring(0, 1);
                        strTracerCTFlag = this.uCheckCurveTracer.Checked.ToString().ToUpper().Substring(0, 1);
                        strTDRFlag = this.uCheckTDR2.Checked.ToString().ToUpper().Substring(0, 1);
                        strEtcFlag = this.uCheckEtc2.Checked.ToString().ToUpper().Substring(0, 1);
                    }
                    //-- 기본분석관리 BL 호출 --
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                    QRPQAT.BL.QATREL.BasicAnalysis clsBasicAnalysis = new QRPQAT.BL.QATREL.BasicAnalysis();
                    brwChannel.mfCredentials(clsBasicAnalysis);

                    //--- DatacolumnsSet 매서드 호출
                    DataTable dtBasicAnalysis = clsBasicAnalysis.mfDataSetInfo();


                    //----------------------------------------- 저장할 데이터 값 저장 ----------------------------------------------//
                    DataRow drBasicAnalysis;
                    drBasicAnalysis = dtBasicAnalysis.NewRow();
                    ////drBasicAnalysis["PlantCode"] = this.uComboPlant.Value.ToString();       //공장
                    drBasicAnalysis["PlantCode"] = m_resSys.GetString("SYS_PLANTCODE");         //공장
                    drBasicAnalysis["BasicNo"] = this.uTextBasicNo.Text;                        //관리번호                    
                    drBasicAnalysis["Pakcage"] = this.uComboPackage.Value.ToString();           //Package
                    drBasicAnalysis["ReqUserID"] = this.uTextReqUserID.Text; ;                  //의뢰자
                    drBasicAnalysis["ReqDate"] = Convert.ToDateTime(this.uDateRequestDate.Value).ToString("yyyy-MM-dd"); //의뢰일
                    drBasicAnalysis["AnalysisType"] = this.uComboAnalysisType.Value.ToString(); //분석구분
                    drBasicAnalysis["AnalysisObject"] = this.uTextAnalysisObject.Text;          //분석목적

                    //-- 분석구분에 따라 값이 다르다 --//
                    drBasicAnalysis["SEMSATFlag"] = strSEMSATFlag;
                    drBasicAnalysis["FTIRXrayFlag"] = strFTIRXrayFlag;
                    drBasicAnalysis["MoireDECAPFlag"] = strMoireDECAPFlag;
                    drBasicAnalysis["CurveCroSecFlag"] = strCurveCroSecFlag;
                    drBasicAnalysis["TracerCTFlag"] = strTracerCTFlag;
                    drBasicAnalysis["TDRFlag"] = strTDRFlag;
                    drBasicAnalysis["EtcFlag"] = strEtcFlag;

                    drBasicAnalysis["LotNo"] = this.uTextLotNo.Text.Trim();                     //LotNo
                    drBasicAnalysis["ReqQty"] = this.uNumReqQty.Value.ToString();               //의뢰수량
                    drBasicAnalysis["Comment"] = this.uTextComment.Text.Trim();                 //코멘트

                    drBasicAnalysis["AnalysisReqFlag"] = this.uCheckAnalysisReqFlag.Checked.ToString().Substring(0, 1); //분석의뢰완료
                    drBasicAnalysis["ReceipReturnFlag"] = this.uCheckReceipReturnFlag.Checked.ToString().Substring(0, 1); //접수반려여부
                    drBasicAnalysis["ReceipFinishFlag"] = this.uCheckReceipFinishFlag.Checked.ToString().Substring(0, 1); //접수완료 여부
                    drBasicAnalysis["AnalysisResult"] = this.uTextAnalyzeResult.Text.Trim();    //분석결과

                    dtBasicAnalysis.Rows.Add(drBasicAnalysis);

                    string strUpLoadReq = "";  //(의뢰정보)파일이 신규나 수정, 수정안됨 여부변수
                    string strUpLoadAna = "";  //(접수/분석정보)파일이 신규나 수정, 수정안됨 여부변수

                    //(의뢰정보) 첨부파일텍스트에 경로(:\\)가 있는경우 신규 혹은 수정으로 인식//
                    if (this.uTextReqFileName.Text.Contains(":\\"))
                    {
                        strUpLoadReq = "Req";      //신규,수정여부
                        FileInfo FileInfo = new FileInfo(this.uTextReqFileName.Text); // 파일경로에 대한 정보를 읽는다

                        dtBasicAnalysis.Rows[0]["ReqFileName"] = "-Req-" + FileInfo.Name; //의뢰정보 첨부파일
                    }
                    // 경로가 없는경우 그냥 저장
                    else
                    {
                        dtBasicAnalysis.Rows[0]["ReqFileName"] = this.uTextReqFileName.Text; //의뢰정보 첨부파일
                    }

                    //(접수/분석정보) 첨부파일텍스트에 경로(:\\)가 있는경우 신규 혹은 수정으로 인식//
                    if (this.uTextResultFileName.Text.Contains(":\\"))
                    {
                        strUpLoadAna = "Ana";   //신규,수정여부
                        FileInfo FileInfo = new FileInfo(this.uTextResultFileName.Text); //파일경로에 대한 정보를 읽는다

                        dtBasicAnalysis.Rows[0]["ResultFileName"] = "-Ana-" + FileInfo.Name; //접수 분석 정보 첨부파일
                    }
                    // 경로가 없는경우 그냥 저장
                    else
                    {
                        dtBasicAnalysis.Rows[0]["ResultFileName"] = this.uTextResultFileName.Text; //접수 분석 정보 첨부파일
                    }

                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;
                    ///////////////////////////////////////////

                    //----------------------------- 저장 여부 메세지 박스 Yes를 클릭하면 저장 이된다. -----------------------------------//
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        //  팝업창을 띄운다 //
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        //    커서 변경  //
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //   저장 매서드 호출 //
                        string strErrRtn = clsBasicAnalysis.mfSaveBasicAnalysis(dtBasicAnalysis, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // Decoding / /
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //-- 파일정보가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSystemAccessInfo);

                        DataTable dtSysAccInfo = clsSystemAccessInfo.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        // -- 파일 저장 경로 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSystemFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSystemFilePath);

                        DataTable dtSysFilePath = clsSystemFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0015");

                        //  ---첨부파일이 수정되거나 신규면 파일을 업로드한다
                        ArrayList arrFile = new ArrayList();
                        string strBasicNo = "";
                        if (strUpLoadReq != "" || strUpLoadAna != "")
                        {
                            strBasicNo = ErrRtn.mfGetReturnValue(0);
                            if (strBasicNo != "")
                            {
                                if (strUpLoadReq != "")
                                {
                                    // 파일이름변경(관리번호-Req-제목)하여 복사하기
                                    FileInfo FileInfo = new FileInfo(this.uTextReqFileName.Text);

                                    strUpLoadReq = FileInfo.DirectoryName + "\\" + strBasicNo + "-Req-" + FileInfo.Name;

                                    //변경한파일있으면삭제
                                    if (File.Exists(strUpLoadReq))
                                        File.Delete(strUpLoadReq);

                                    //변경한파일이름으로복사
                                    File.Copy(this.uTextReqFileName.Text, strUpLoadReq);
                                    arrFile.Add(strUpLoadReq);
                                }
                                if (strUpLoadAna != "")
                                {
                                    // 파일이름변경(관리번호-Req-제목)하여 복사하기
                                    FileInfo FileInfo = new FileInfo(this.uTextResultFileName.Text);

                                    strUpLoadAna = FileInfo.DirectoryName + "\\" + strBasicNo + "-Ana-" + FileInfo.Name;

                                    //변경한파일있으면삭제
                                    if (File.Exists(strUpLoadAna))
                                        File.Delete(strUpLoadAna);

                                    //변경한파일이름으로복사
                                    File.Copy(this.uTextResultFileName.Text, strUpLoadAna);
                                    arrFile.Add(strUpLoadAna);
                                }
                            }
                        }

                        //  커서 기본값으로 변경 .//
                        this.MdiParent.Cursor = Cursors.Default;

                        //팝업창을 닫는다 //
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 처리 결과에 따라서 메세지 박스를 보낸다 //
                        System.Windows.Forms.DialogResult result;
                        if (ErrRtn.ErrNum == 0)
                        {
                            if (strBasicNo != "")
                            {
                                if (strUpLoadReq != "" || strUpLoadAna != "")
                                {
                                    frmCOMFileAttach frmFileAttach = new frmCOMFileAttach();

                                    //Upload정보 설정
                                    frmFileAttach.mfInitSetSystemFileInfo(arrFile, "", dtSysAccInfo.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtSysFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtSysFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccInfo.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccInfo.Rows[0]["AccessPassword"].ToString());
                                    frmFileAttach.ShowDialog();

                                }
                            }
                            result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                        Infragistics.Win.HAlign.Right);

                            mfSearch();
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                         "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                         Infragistics.Win.HAlign.Right);
                        }
                    }
                     * */
                    #endregion
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // 관리번호가 공백이나 ExapndGroupBox가 숨겨져있는상태면 정보가 없는걸로 인식
                if (this.uTextBasicNo.Text == "" || this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);

                }

                //string strPlantCode = this.uComboPlant.Value.ToString();
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                string strBasicNo = this.uTextBasicNo.Text;

                //---- 삭제 여부 메세지를 띄운다 ----///
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //  팝업창을 띄운다
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    // 커서를 변경한다
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //기본분석관리 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                    QRPQAT.BL.QATREL.BasicAnalysis clsBasicAnalysis = new QRPQAT.BL.QATREL.BasicAnalysis();
                    brwChannel.mfCredentials(clsBasicAnalysis);

                    //  삭제매서드 호출
                    string strErrRtn = clsBasicAnalysis.mfDeleteBasicAnalysis(strPlantCode, strBasicNo);

                    // DeCoding ..
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //커서를 기본값으로 변경한다.
                    this.MdiParent.Cursor = Cursors.Default;
                    // 팝업창을 닫는다.
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    //처리결과에 따라서 메세지박스를 띄운다
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);

                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    Clear();
                }
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridBasicAnalysis.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridBasicAnalysis);
                }

                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        // 분석구분 값에 따라 그룹박스 보여주기 위한 이벤트
        private void uComboAnalysisDivision_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboAnalysisType.Value == null)
                {
                    return;
                }
                else if (this.uComboAnalysisType.Value.ToString() == "")
                {
                    // 체크박스 그룹박스 숨김 상태로
                    this.uGroupBoxGeneral.Hide();
                    this.uGroupBoxFault.Hide();

                    this.uCheckSEM.Checked = false;
                    this.uCheckFTIR.Checked = false;
                    this.uCheckMoire.Checked = false;
                    this.uCheckCurve.Checked = false;
                    this.uCheckTracer.Checked = false;
                    this.uCheckTDR1.Checked = false;
                    this.uCheckExterior1.Checked = false;
                    this.uCheckEtc1.Checked = false;
                    this.uTextEtcDesc.Clear();

                    this.uCheckSAT.Checked = false;
                    this.uCheckXray.Checked = false;
                    this.uCheckDECAP.Checked = false;
                    this.uCheckCrossSection.Checked = false;
                    this.uCheckCurveTracer.Checked = false;
                    this.uCheckTDR2.Checked = false;
                    this.uCheckExterior2.Checked = false;
                    this.uCheckEtc2.Checked = false;
                    this.uTextEtcDesc2.Clear();
                }
                else if (this.uComboAnalysisType.Value.ToString() == "1")
                {
                    // 일반분석 그룹박스 보이게
                    this.uGroupBoxGeneral.Show();
                    this.uGroupBoxFault.Hide();

                    this.uCheckSAT.Checked = false;
                    this.uCheckXray.Checked = false;
                    this.uCheckDECAP.Checked = false;
                    this.uCheckCrossSection.Checked = false;
                    this.uCheckCurveTracer.Checked = false;
                    this.uCheckTDR2.Checked = false;
                    this.uCheckExterior2.Checked = false;
                    this.uCheckEtc2.Checked = false;
                    this.uTextEtcDesc2.Clear();
                }
                else if (this.uComboAnalysisType.Value.ToString() == "2")
                {
                    // 불량분석 그룹박스 보이게
                    this.uGroupBoxGeneral.Hide();
                    this.uGroupBoxFault.Show();

                    this.uCheckSEM.Checked = false;
                    this.uCheckFTIR.Checked = false;
                    this.uCheckMoire.Checked = false;
                    this.uCheckCurve.Checked = false;
                    this.uCheckTracer.Checked = false;
                    this.uCheckTDR1.Checked = false;
                    this.uCheckExterior1.Checked = false;
                    this.uCheckEtc1.Checked = false;
                    this.uTextEtcDesc.Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0011_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // ExpandableGroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 175);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridBasicAnalysis.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridBasicAnalysis.Height = 720;

                    for (int i = 0; i < this.uGridBasicAnalysis.Rows.Count; i++)
                    {
                        this.uGridBasicAnalysis.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 첨부파일
        // 의뢰정보 첨부파일 버튼이벤트
        private void uTextReqFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        this.uTextReqFileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextReqFileName.Text.Contains(":\\") || this.uTextReqFileName.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_resSys.GetString("SYS_PLANTCODE"), "D0015");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextReqFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextReqFileName.Text);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        // 접수/분석 첨부파일 버튼 이벤트
        private void uTextResultFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        this.uTextResultFileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextResultFileName.Text.Contains(":\\") || this.uTextResultFileName.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_resSys.GetString("SYS_PLANTCODE"), "D0015");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextResultFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextResultFileName.Text);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 기존첨부파일 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextReqFileName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!this.uTextReqFileName.Enabled)
                    return;

                // 기존등록되어 있는 폴더를 삭제할경우 (파일 경로가 없고 Delete나 Backspase 키입력시 기존첨부파일을 삭제한다.)
                if(e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();


                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001323", "M001297", Infragistics.Win.HAlign.Right);

                    if (Result.Equals(DialogResult.No))
                        return;

                    if (!this.uTextReqFileName.Text.Contains(":\\"))
                    {
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrDelDWFile = new ArrayList();

                        //FileServer 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        arrDelDWFile.Add("QATBasicAnalysisFile\\" + this.uTextReqFileName.Text);
                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                            , arrDelDWFile
                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.mfFileUploadNoProgView();
                        this.uTextReqFileName.Clear();
                    }
                    else
                    {
                        this.uTextReqFileName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 기존분석정보첨부파일 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextResultFileName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!this.uCheckAnalysisCompleteFlag.Enabled)
                    return;

               

                // 기존등록되어 있는 폴더를 삭제할경우 (파일 경로가 없고 Delete나 Backspase 키입력시 기존첨부파일을 삭제한다.)
                if (e.KeyData.Equals(Keys.Delete) || e.KeyData.Equals(Keys.Back))
                {
                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();


                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001323", "M001297", Infragistics.Win.HAlign.Right);

                    if (Result.Equals(DialogResult.No))
                        return;

                    if (!this.uTextResultFileName.Text.Contains(":\\"))
                    {
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrDelDWFile = new ArrayList();

                        //FileServer 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        arrDelDWFile.Add("QATBasicAnalysisFile\\" + this.uTextResultFileName.Text);
                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                            , arrDelDWFile
                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.mfFileUploadNoProgView();
                        this.uTextResultFileName.Clear();
                    }
                    else
                    {
                        this.uTextResultFileName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 사용자조회
        private void uTextSearchReqUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchReqUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextSearchReqUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(m_resSys.GetString("SYS_PLANTCODE"), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextSearchReqUserID.Text = "";
                            this.uTextSearchReqUserName.Text = "";
                        }

                        else
                        {
                            this.uTextSearchReqUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchReqUserID.TextLength <= 1 || this.uTextSearchReqUserID.SelectedText == this.uTextSearchReqUserID.Text)
                    {
                        this.uTextSearchReqUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchReqUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                QRPQAT.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextSearchReqUserID.Text = frmUser.UserID;
                this.uTextSearchReqUserName.Text = frmUser.UserName;
                //this.uComboPlant.Value = frmUser.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자명 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        private void uTextAnalyzeUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                QRPQAT.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextAnalyzeUserID.Text = frmUser.UserID;
                this.uTextAnalyzeUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAnalyzeUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAnalyzeUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAnalyzeUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(m_resSys.GetString("SYS_PLANTCODE"), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAnalyzeUserID.Text = "";
                            this.uTextAnalyzeUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAnalyzeUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextAnalyzeUserID.TextLength <= 1 || this.uTextAnalyzeUserID.SelectedText == this.uTextAnalyzeUserID.Text)
                    {
                        this.uTextAnalyzeUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        //왼쪽버튼을 클릭하였을때 0으로바뀐다.
        private void uNumReqQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 0으로 초기화
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //스핀버튼을 클릭하였을때 증,감소가 이루어진다.
        private void uNumReqQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uNumReqQty.Focus();
                    uNumReqQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uNumReqQty.Focus();
                    uNumReqQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 팝업창 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = strPlantCode;
                frmPOP.ShowDialog();

                //선택한 정보의 제품코드와 제품명을 해당컨트롤에 넣는다.
                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // ExpandGroupBox 안의 의뢰자 에디트버튼을 클릭하며 사용자 정보팝업창을띄운다. 
        private void uTextRequestUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");//this.uComboPlant.Value.ToString();
                
                QRPQAT.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextReqUserID.Text = frmUser.UserID;
                this.uTextReqUserName.Text = frmUser.UserName;
                this.uComboPlant.Value = frmUser.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //의뢰자 ID텍스트박스에 키가 입력되면 이벤트가 발생한다.
        private void uTextRequestUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReqUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextReqUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(m_resSys.GetString("SYS_PLANTCODE"), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextReqUserID.Text = "";
                            this.uTextReqUserName.Text = "";
                        }

                        else
                        {
                            this.uTextReqUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReqUserID.TextLength <= 1 || this.uTextReqUserID.SelectedText == this.uTextReqUserID.Text)
                    {
                        this.uTextReqUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //기본분석관리 그리드의 더블 클릭 셀정보가 공백이아닐 시 클릭한 셀의 줄정보를 가져온다 // 
        private void uGridBasicAnalysis_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                e.Cell.Row.Fixed = true;
                //공장,관리번호저장
                string strPlantcode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strBasicNo = e.Cell.Row.Cells["BasicNo"].Value.ToString();

                // 조회 메소드 호출
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                QRPQAT.BL.QATREL.BasicAnalysis clsBA = new QRPQAT.BL.QATREL.BasicAnalysis();
                brwChannel.mfCredentials(clsBA);

                DataTable dtBA = clsBA.mfReadBasicAnalysis_Detail(strPlantcode, strBasicNo, m_resSys.GetString("SYS_LANG"));

                if (dtBA.Rows.Count > 0)
                {
                    this.uTextBasicNo.Text = strBasicNo;
                    this.uTextPackage.Text = dtBA.Rows[0]["Package"].ToString();
                    this.uTextCustomerCode.Text = dtBA.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtBA.Rows[0]["CustomerName"].ToString();
                    this.uTextReqUserID.Text = dtBA.Rows[0]["ReqUserID"].ToString();
                    this.uTextReqUserName.Text = dtBA.Rows[0]["ReqUserName"].ToString();
                    this.uDateRequestDate.Value = Convert.ToDateTime(dtBA.Rows[0]["ReqDate"]).ToString("yyyy-MM-dd");
                    this.uComboAnalysisType.Value = dtBA.Rows[0]["AnalysisType"].ToString();
                    this.uTextAnalysisObject.Text = dtBA.Rows[0]["AnalysisObject"].ToString();
                    this.uCheckSEM.Checked = Convert.ToBoolean(dtBA.Rows[0]["SEMFlag"]);
                    this.uCheckFTIR.Checked = Convert.ToBoolean(dtBA.Rows[0]["FTIRFlag"]);
                    this.uCheckMoire.Checked = Convert.ToBoolean(dtBA.Rows[0]["MoireFlag"]);
                    this.uCheckCurve.Checked = Convert.ToBoolean(dtBA.Rows[0]["CurveFlag"]);
                    this.uCheckTracer.Checked = Convert.ToBoolean(dtBA.Rows[0]["TracerFlag"]);
                    this.uCheckTDR1.Checked = Convert.ToBoolean(dtBA.Rows[0]["TDRFlag1"]);
                    this.uCheckEtc1.Checked = Convert.ToBoolean(dtBA.Rows[0]["EtcFlag1"]);
                    this.uCheckSAT.Checked = Convert.ToBoolean(dtBA.Rows[0]["SATFlag"]);
                    this.uCheckXray.Checked = Convert.ToBoolean(dtBA.Rows[0]["XrayFlag"]);
                    this.uCheckDECAP.Checked = Convert.ToBoolean(dtBA.Rows[0]["DECAPFlag"]); ;
                    this.uCheckCrossSection.Checked = Convert.ToBoolean(dtBA.Rows[0]["CrossSectionFlag"]);
                    this.uCheckCurveTracer.Checked = Convert.ToBoolean(dtBA.Rows[0]["CurveTracerFlag"]);
                    this.uCheckTDR2.Checked = Convert.ToBoolean(dtBA.Rows[0]["TDRFlag2"]);
                    this.uCheckEtc2.Checked = Convert.ToBoolean(dtBA.Rows[0]["EtcFlag2"]);
                    if (this.uCheckEtc1.Checked)
                        this.uTextEtcDesc.Text = dtBA.Rows[0]["EtcDesc"].ToString();
                    else if (this.uCheckEtc2.Checked)
                        this.uTextEtcDesc2.Text = dtBA.Rows[0]["EtcDesc"].ToString();
                    
                    this.uTextLotNo.Text = dtBA.Rows[0]["LotNo"].ToString();
                    this.uNumReqQty.Value = Convert.ToInt32(dtBA.Rows[0]["ReqQty"]);
                    this.uTextComment.Text = dtBA.Rows[0]["Comment"].ToString();
                    this.uTextReqFileName.Text = dtBA.Rows[0]["ReqFileName"].ToString();
                    this.uCheckAnalysisReqFlag.Checked = Convert.ToBoolean(dtBA.Rows[0]["AnalysisReqFlag"]);
                    this.uCheckReceipReturnFlag.Checked = Convert.ToBoolean(dtBA.Rows[0]["ReceipReturnFlag"]);
                    this.uCheckReceipFinishFlag.Checked = Convert.ToBoolean(dtBA.Rows[0]["ReceipFinishFlag"]);
                    this.uTextAnalyzeResult.Text = dtBA.Rows[0]["AnalysisResult"].ToString();
                    this.uTextAnalyzeUserID.Text = dtBA.Rows[0]["AnalyzeUserID"].ToString();
                    this.uTextAnalyzeUserName.Text = dtBA.Rows[0]["AnalyzeUserName"].ToString();
                    this.uTextResultFileName.Text = dtBA.Rows[0]["ResultFileName"].ToString();

                    this.uDateReceipFinish.Value = dtBA.Rows[0]["ReceipFinishDate"].ToString();
                    this.uDateTimeReceipFinish.Value = dtBA.Rows[0]["ReceipFinishTime"].ToString();
                    this.uCheckAnalysisCompleteFlag.Checked = dtBA.Rows[0]["AnalysisCompleteFlag"].ToString() == "T" ? true : false;
                    this.uDateAnalysisComplete.Value = dtBA.Rows[0]["AnalysisCompleteDate"].ToString();
                    this.uDateTimeAnalysisComplete.Value = dtBA.Rows[0]["AnalysisCompleteTime"].ToString();
                    this.uComboIssueType.Value = dtBA.Rows[0]["IssueType"];

                    this.uCheckExterior1.Checked = dtBA.Rows[0]["ExteriorFlag1"].ToString() == "T" ? true : false;
                    this.uCheckExterior2.Checked = dtBA.Rows[0]["ExteriorFlag2"].ToString() == "T" ? true : false;

                    if (this.uCheckAnalysisReqFlag.Checked)
                    {
                        this.uCheckAnalysisReqFlag.Enabled = false;
                        this.uTextPackage.ReadOnly = true;
                        AnalysisReqEnabled();
                    }
                    if (this.uCheckReceipFinishFlag.Checked)
                        this.uCheckReceipFinishFlag.Enabled = false;

                    if (this.uCheckAnalysisCompleteFlag.Checked)
                        this.uCheckAnalysisCompleteFlag.Enabled = false;
                }
                else
                {
                    ////WinMessageBox msg = new WinMessageBox();
                    ////DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                   "조회결과", "상세정보 조회결과", "조회중 오류가 발생했습니다.",
                    ////                   Infragistics.Win.HAlign.Right);
                }
                #region 기존소스 주석처리
                /*
                //this.uComboPlant.Value = strPlantcode;                   //공장
                //this.uComboPlant.ReadOnly = true;

                this.uTextBasicNo.Text = strBasicNo;                      //관리번호
                //this.uTextProductCode.Text = e.Cell.Row.Cells["ProductCode"].Value.ToString();              //제품코드
                //this.uTextProductName.Text = e.Cell.Row.Cells["ProductName"].Value.ToString();


                this.uDateRequestDate.Value = e.Cell.Row.Cells["ReqDate"].Value.ToString();                 //의뢰일
                this.uComboAnalysisType.Value = e.Cell.Row.Cells["AnalysisType"].Value.ToString();          //분석구분

                this.uTextCustomerCode.Text = e.Cell.Row.Cells["CustomerCode"].Value.ToString();
                this.uTextCustomerName.Text = e.Cell.Row.Cells["CustomerName"].Value.ToString();
                this.uTextLotNo.Text = e.Cell.Row.Cells["LotNo"].Value.ToString();                         //LotNo
                this.uNumReqQty.Value = e.Cell.Row.Cells["ReqQty"].Value.ToString();                      //의뢰수량

                this.uCheckComplete.Checked = Convert.ToBoolean(e.Cell.Row.Cells["AnalysisReqFlag"].Value);                //분석의뢰완료 
                this.uCheckAcceptReturn.Checked = Convert.ToBoolean(e.Cell.Row.Cells["ReceipReturnFlag"].Value);            //접수반려여부 
                this.uCheckAcceptComplete.Checked = Convert.ToBoolean(e.Cell.Row.Cells["ReceipFinishFlag"].Value);          //접수완료 여부 


                // 기본분석관리 BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.BasicAnalysis), "BasicAnalysis");
                QRPQAT.BL.QATREL.BasicAnalysis clsBasicAnalysis = new QRPQAT.BL.QATREL.BasicAnalysis();
                brwChannel.mfCredentials(clsBasicAnalysis);

                // 상세조회 매서드 호출
                DataTable dtBasicAnalysis = clsBasicAnalysis.mfReadBasicAnalysis(strPlantcode, strBasicNo);

                // 조회값이 0 이상일때  컬럼 수 대로 for문을 돈다 Row[0]번째 줄의 각 컬럼 정보중 T 가있으면 True F 가있으면 False 로바꾼다 //
                if (dtBasicAnalysis.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBasicAnalysis.Columns.Count; i++)
                    {
                        if (dtBasicAnalysis.Rows[0][i].ToString() == "T")
                        {
                            dtBasicAnalysis.Rows[0][i] = "True";
                        }
                        else if (dtBasicAnalysis.Rows[0][i].ToString() == "F")
                        {
                            dtBasicAnalysis.Rows[0][i] = "False";
                        }
                    }
                }

                this.uTextComment.Text = dtBasicAnalysis.Rows[0]["Comment"].ToString();                        //코멘트
                this.uTextReqFileName.Text = dtBasicAnalysis.Rows[0]["ReqFileName"].ToString();
                this.uTextResultFileName.Text = dtBasicAnalysis.Rows[0]["ResultFileName"].ToString();
                this.uTextAnalyzeResult.Text = dtBasicAnalysis.Rows[0]["AnalysisResult"].ToString();                  //분석결과
                this.uTextReqUserID.Text = dtBasicAnalysis.Rows[0]["ReqUserID"].ToString();                  //의뢰자
                this.uTextReqUserName.Text = dtBasicAnalysis.Rows[0]["ReqUserName"].ToString();
                this.uTextAnalysisObject.Text = dtBasicAnalysis.Rows[0]["AnalysisObject"].ToString();                 //분석목적


                //--- 분석 구분의 값에 따라서 체크박스들이 바뀐다.
                if (e.Cell.Row.Cells["AnalysisType"].Value.ToString() == "1")
                {
                    this.uCheckSEM.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["SEMFlag"].ToString());
                    this.uCheckFTIR.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["FTIRFlag"].ToString());
                    this.uCheckMoire.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["MoireFlag"].ToString());
                    this.uCheckCurve.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["CurveFlag"].ToString());
                    this.uCheckTracer.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["TracerFlag"].ToString());
                    this.uCheckTDR1.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["TDRFlag1"].ToString());
                    this.uCheckEtc1.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["EtcFlag1"].ToString());
                }
                else if (e.Cell.Row.Cells["AnalysisType"].Value.ToString() == "2")
                {
                    this.uCheckSAT.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["SATFlag"].ToString());
                    this.uCheckXray.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["XrayFlag"].ToString());
                    this.uCheckDECAP.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["DECAPFlag"].ToString());
                    this.uCheckCrossSection.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["CrossSectionFlag"].ToString());
                    this.uCheckCurveTracer.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["CurveTracerFlag"].ToString());
                    this.uCheckTDR2.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["TDRFlag2"].ToString());
                    this.uCheckEtc2.Checked = Convert.ToBoolean(dtBasicAnalysis.Rows[0]["EtcFlag2"].ToString());
                }
                else
                {
                    this.uCheckSEM.Checked = false;
                    this.uCheckFTIR.Checked = false;
                    this.uCheckMoire.Checked = false;
                    this.uCheckCurve.Checked = false;
                    this.uCheckTracer.Checked = false;
                    this.uCheckTDR1.Checked = false;
                    this.uCheckEtc1.Checked = false;

                    this.uCheckSAT.Checked = false;
                    this.uCheckXray.Checked = false;
                    this.uCheckDECAP.Checked = false;
                    this.uCheckCrossSection.Checked = false;
                    this.uCheckCurveTracer.Checked = false;
                    this.uCheckTDR2.Checked = false;
                    this.uCheckEtc2.Checked = false;
                }
                 * */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Method
        /// <summary>
        /// ExpandGroupBox안의 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장
                this.uComboPlant.ReadOnly = false;

                //this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextBasicNo.Text = "";                        //관리번호
                //this.uTextProductCode.Text = "";                    //제품코드
                this.uTextPackage.Text = string.Empty;
                this.uTextSearchProductName.Text = "";
                this.uDateRequestDate.Value = DateTime.Now.ToString("yyyy-MM-dd");         //의뢰일
                this.uComboAnalysisType.Value = "";                 //분석구분
                this.uTextAnalysisObject.Text = "";                 //분석목적

                this.uCheckSEM.Checked = false;
                this.uCheckFTIR.Checked = false;
                this.uCheckMoire.Checked = false;
                this.uCheckCurve.Checked = false;
                this.uCheckTracer.Checked = false;
                this.uCheckTDR1.Checked = false;
                this.uCheckEtc1.Checked= false;

                this.uCheckSAT.Checked = false;
                this.uCheckXray.Checked = false;
                this.uCheckDECAP.Checked  = false;
                this.uCheckCrossSection.Checked = false;
                this.uCheckCurveTracer.Checked = false;
                this.uCheckTDR2.Checked = false;
                this.uCheckEtc2.Checked = false;

                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextLotNo.Text = "";                          //LotNo
                this.uNumReqQty.Value = 0;                         //의뢰수량
                this.uTextComment.Text = "";                        //코멘트

                this.uTextReqFileName.Text = "";
                this.uTextResultFileName.Text = "";

                this.uTextAnalyzeUserID.Clear();
                this.uTextAnalyzeUserName.Clear();

                this.uCheckAnalysisReqFlag.Checked = false;                //분석의뢰완료
                this.uCheckReceipReturnFlag.Checked = false;            //접수반려여부
                this.uCheckReceipFinishFlag.Checked = false;          //접수완료 여부
                this.uTextAnalyzeResult.Text = "";                  //분석결과

                this.uCheckAnalysisReqFlag.Enabled = true;
                this.uTextPackage.ReadOnly = false;

                this.uDateReceipFinish.Value = null;
                this.uDateTimeReceipFinish.Value = null;

                this.uDateAnalysisComplete.Value = null;
                this.uDateTimeAnalysisComplete.Value = null;

                this.uComboIssueType.SelectedIndex = 0;

                this.uCheckReceipFinishFlag.Enabled = true;
                this.uCheckAnalysisCompleteFlag.Enabled = true;
                this.uCheckAnalysisCompleteFlag.Checked = false;

                foreach (Control ctrl in this.uGroupBoxRequestInfo.Controls)
                {
                    ctrl.Enabled = true;
                }

                this.uTextReqUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextReqUserName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 분석의뢰완료된 정보 수정불가처리
        /// </summary>
        private void AnalysisReqEnabled()
        {
            try
            {
                 foreach (Control ctrl in this.uGroupBoxRequestInfo.Controls)
                {
                    ctrl.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        /// <param name="strBasicNo">관리번호</param>
        private void FileUpload(string strBasicNo)
        {
            try
            {
                //  ---첨부파일이 수정되거나 신규면 파일을 업로드한다
                ArrayList arrFile = new ArrayList();
                //string strBasicNo = ErrRtn.mfGetReturnValue(0);
                string strUploadFile = string.Empty;

                if (!strBasicNo.Equals(string.Empty))
                {
                    if (this.uTextReqFileName.Text.Contains(":\\"))
                    {
                        // 파일이름변경(관리번호-Req-제목)하여 복사하기
                        FileInfo FileInfo = new FileInfo(this.uTextReqFileName.Text);

                        strUploadFile = FileInfo.DirectoryName + "\\" + strBasicNo + "-Req-" + FileInfo.Name;

                        //변경한파일있으면삭제
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                        //변경한파일이름으로복사
                        File.Copy(this.uTextReqFileName.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }
                    if (this.uTextResultFileName.Text.Contains(":\\"))
                    {
                        // 파일이름변경(관리번호-Req-제목)하여 복사하기
                        FileInfo FileInfo = new FileInfo(this.uTextResultFileName.Text);

                        strUploadFile = FileInfo.DirectoryName + "\\" + strBasicNo + "-Ana-" + FileInfo.Name;

                        //변경한파일있으면삭제
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                        //변경한파일이름으로복사
                        File.Copy(this.uTextResultFileName.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }

                    if (arrFile.Count > 0)
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        //-- 파일정보가져오기
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSystemAccessInfo = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSystemAccessInfo);

                        DataTable dtSysAccInfo = clsSystemAccessInfo.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        // -- 파일 저장 경로 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSystemFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSystemFilePath);

                        DataTable dtSysFilePath = clsSystemFilePath.mfReadSystemFilePathDetail(m_resSys.GetString("SYS_PLANTCODE"), "D0015");

                        frmCOMFileAttach frmFileAttach = new frmCOMFileAttach();
                        //Upload정보 설정
                        frmFileAttach.mfInitSetSystemFileInfo(arrFile, "", dtSysAccInfo.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtSysFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtSysFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccInfo.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccInfo.Rows[0]["AccessPassword"].ToString());
                        frmFileAttach.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 접수/분석내용 권한
        /// </summary>
        private void AcceptAnalyzeInfo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                        uGroupBoxAcceptAnalyzeInfo.Visible = true;
                    else
                        uGroupBoxAcceptAnalyzeInfo.Visible = false;


                }
                else
                {
                    uGroupBoxAcceptAnalyzeInfo.Visible = false;
                }

                ////if (!m_resSys.GetString("SYS_DEPTCODE").Equals("S5100"))
                ////{
                ////    foreach (Control ctrl in this.uGroupBoxAcceptAnalyzeInfo.Controls)
                ////    {
                ////        ctrl.Visible = false;
                ////    }
                ////    this.uGroupBoxAcceptAnalyzeInfo.Visible = false;
                ////}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 필수입력사항체크
        /// </summary>
        /// <returns></returns>
        private bool CheckSave()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();


                if (this.uGroupBoxAcceptAnalyzeInfo.Visible.Equals(false) && this.uCheckReceipFinishFlag.Checked)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001296", "M001294", Infragistics.Win.HAlign.Right);

                    return false;
                }
                if (this.uCheckAnalysisCompleteFlag.Enabled.Equals(false) && this.uCheckAnalysisCompleteFlag.Checked)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001293", "M001292", Infragistics.Win.HAlign.Right);

                    return false;
                }
                else if (this.uTextReqUserID.Text.Trim() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000842", Infragistics.Win.HAlign.Right);

                    this.uTextReqUserID.Focus();
                    return false;
                }
                else if (this.uDateRequestDate.Value.ToString() == "" || this.uDateRequestDate.Value.Equals(null))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000840", Infragistics.Win.HAlign.Right);

                    this.uDateRequestDate.DropDown();
                    return false;
                }
                else if (this.uTextPackage.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001264", "M001230", "M000097", Infragistics.Win.HAlign.Right);

                    //this.uTextPackage.DropDown();
                    this.uTextPackage.Focus();
                    return false;
                }
                else if (this.uCheckReceipReturnFlag.Checked)
                {
                    if (!this.uCheckAnalysisReqFlag.Checked)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001295", Infragistics.Win.HAlign.Right);

                        this.uCheckAnalysisReqFlag.Focus();
                        return false;
                    }
                }
                if (this.uCheckReceipFinishFlag.Checked)
                {
                    if (!this.uCheckAnalysisReqFlag.Checked)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001295", Infragistics.Win.HAlign.Right);

                        this.uCheckAnalysisReqFlag.Focus();
                        return false;
                    }

                    if (this.uCheckReceipFinishFlag.Enabled)
                    {
                        this.uDateReceipFinish.Value = DateTime.Now;
                        this.uDateTimeReceipFinish.Value = DateTime.Now;
                    }
                }
                if (this.uCheckAnalysisCompleteFlag.Checked)
                {
                    if (!this.uCheckReceipFinishFlag.Checked)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001318", Infragistics.Win.HAlign.Right);

                        this.uCheckReceipFinishFlag.Focus();
                        return false;
                    }
                    if (this.uTextAnalyzeUserID.Text.Equals(string.Empty) || this.uTextAnalyzeUserName.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001291", Infragistics.Win.HAlign.Right);

                        this.uTextAnalyzeUserID.Focus();
                        return false;
                    }
                    if (this.uCheckAnalysisCompleteFlag.Enabled)
                    {
                        this.uDateAnalysisComplete.Value = DateTime.Now;
                        this.uDateTimeAnalysisComplete.Value = DateTime.Now;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// 메일 송신(첨부파일 첨부)
        /// </summary>
        /// <param name="stdBasicNum"></param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strHeder">첨부파일헤더 (Req : 분석의뢰, Ana : 분석결과첨부파일)</param>
        /// <param name="Ctrl">첨부파일텍스트</param>
        /// <returns>성공 실패</returns>
        private bool SendMail(string stdBasicNum,string strUserID,string strHeder,Control Ctrl)
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                QRPBrowser brwChannel = new QRPBrowser();


                // 사용자정보 가져오기(사용자메일주소 조회)
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                // 메일보내기
                if (dtUser.Rows.Count > 0)
                {
                    #region BL호출 및 연결정보,경로 저장
                    //첨부파일을 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //메일 발송 첨부파일 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    //기본분석관리 첨부파일 저장경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysTargetFilePath);
                    DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0015");

                    #endregion

                    QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                    ArrayList arrFile = new ArrayList();        // 복사할 파일서버경로및 파일명
                    ArrayList arrAttachFile = new ArrayList();  // 파일을 복사할 경로
                    ArrayList arrFileNonPath = new ArrayList(); // 첨부할 파일명 저장

                    //파일이 첨부되었을 경우
                    if (!Ctrl.Text.Equals(string.Empty))
                    {
                        string strFileName = "";

                        //해당첨부파일 텍스트의 경로가 있는 경우 경로를 뺀 파일명만 찾아서 저장한다.
                        if (Ctrl.Text.Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(Ctrl.Text);
                            strFileName = stdBasicNum + "-" + strHeder + "-" + fileDoc.Name;
                        }
                        else
                            strFileName = Ctrl.Text;


                        string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                        string TargetFile = "QATBasicAnalysisFile/" + stdBasicNum + "/" +strFileName;
                        string NonPathFile = strFileName;


                        arrAttachFile.Add(TargetFile);
                        arrFile.Add(DestFile);
                        arrFileNonPath.Add(NonPathFile);

                        //메일에 첨부할 파일을 서버에서 찾아서 서버에 있는 메일 첨부파일폴더로 복사한다.
                        fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                    , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrAttachFile
                                                                    , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                    , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                      dtSysAccess.Rows[0]["AccessPassword"].ToString());

                    }

                    brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                    QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                    brwChannel.mfCredentials(clsmail);

                    for (int i = 0; i < dtUser.Rows.Count; i++)
                    {
                        if (dtUser.Rows[i]["Email"].ToString().Equals(string.Empty))
                            break;

                        if (arrFile.Count != 0)
                        {
                            fileAtt.mfFileUpload_NonAssync();
                        }

                        bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                        , m_resSys.GetString("SYS_EMAIL")
                                                        , dtUser.Rows[i]["UserName"].ToString()
                                                        , dtUser.Rows[i]["Email"].ToString()//보내려는 사람 이메일주소
                                                        , "[STS].관리번호[" + stdBasicNum + "] , Package[" + this.uTextPackage.Text + "], 분석완료하였습니다."
                                                        , "[STS] 기본분석관리 :  관리번호[" + stdBasicNum + "], Package[" + this.uTextPackage.Text +
                                                            "분석완료하였습니다."
                                                        , arrFileNonPath);
                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();

                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M000083", "M000365"
                                    , "M000404",
                                    Infragistics.Win.HAlign.Right);
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        #endregion

        ////private void uComboPlant_ValueChanged(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        this.uTextCustomerCode.Text = "";
        ////        this.uTextCustomerName.Text = "";
        ////        //this.uTextProductCode.Text = "";
        ////        //this.uTextProductName.Text = "";
        ////        this.uTextReqUserID.Clear();
        ////        this.uTextReqUserName.Clear();
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}



        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }


    }
}

