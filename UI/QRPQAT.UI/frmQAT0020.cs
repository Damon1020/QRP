﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : R&R관리                                               */
/* 프로그램ID   : frmQAT0020.cs                                         */
/* 프로그램명   : 계측기관리 등록                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-26 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQAT0020 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmQAT0020()
        {
            InitializeComponent();
        }

        private void frmQAT0020_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQAT0020_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTab();
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitEtc();

            //uGroupBoxContentsArea숨기기
            uGroupBoxContentsArea.Expanded = false;

            // CheckBox 편집불가
            this.uCheckRNRDataInputFlag.Enabled = false;
            this.uCheckRNRAnalysisFlag.Enabled = false;
            this.uCheckRNRDataInputFlag.Appearance.BackColorDisabled = Color.White;
            this.uCheckRNRDataInputFlag.Appearance.ForeColorDisabled = Color.Black;
            this.uCheckRNRAnalysisFlag.Appearance.BackColorDisabled = Color.White;
            this.uCheckRNRAnalysisFlag.Appearance.ForeColorDisabled = Color.Black;


            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmQAT0020_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 컨트롤초기화

        private void InitTab()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None, m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("MSA관리 등록", m_resSys.GetString("SYS_FONTNAME"), 12);
                
                // MAXLength 지정
                this.uTextSearchProjectName.MaxLength = 200;
                this.uTextProjectName.MaxLength = 200;
                this.uTextMeasureToolCode.MaxLength = 10;
                this.uTextMaterialCode.MaxLength = 20;
                this.uTextInspectItemDesc.MaxLength = 100;
                this.uTextInspectUserID.MaxLength = 20;
                this.uTextInspectDesc.MaxLength = 200;

                // 기본값 지정
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uNumAccuracyValue.Visible = false;
                this.uNumRRValueChangeValue.Visible = false;

                this.uNumAccuracyValue.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
                this.uNumAccuracyValue.MaskInput = "{double:5.5}";
                this.uNumRRValueChangeValue.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
                this.uNumRRValueChangeValue.MaskInput = "{double:5.5}";
                this.uNumUpperSpec.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
                this.uNumUpperSpec.MaskInput = "{double:5.5}";
                this.uNumLowerSpec.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
                this.uNumLowerSpec.MaskInput = "{double:5.5}";
                this.uNumPartCount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Integer;
                this.uNumPartCount.MaskInput = "nnnnn";
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchProject, "프로젝트명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelProjectCode, "프로젝트코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProjectName, "프로젝트명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelPlantCode, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelProcessCode, "공정", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMeasureToolCode, "계측기", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelInspectItemDesc, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelInspectDesc, "검사설명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                lbl.mfSetLabel(this.uLabelSpec, "부품규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUnitCode, "단위", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelRepeatCount, "측정반복수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPartCount, "측정부품수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMeasureCount, "측정계측기수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMeasureName, "측정계측기명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // SpecRange
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtCom = clsComCode.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSpecRange, false, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", ""
                    , "ComCode", "ComCodeName", dtCom);

                // 편집불가
                this.uComboSpecRange.ReadOnly = true;

                // 단위
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();

                wCombo.mfSetComboEditor(this.uComboUnitCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "UnitCode", "UnitName", dtUnit);

                // 측정반복수
                ArrayList arrCode = new ArrayList();
                ArrayList arrName = new ArrayList();

                arrCode.Add(2);
                arrCode.Add(3);
                arrName.Add(2);
                arrName.Add(3);

                wCombo.mfSetComboEditor(this.uComboRepeatCount, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "2", arrCode, arrName);

                // 측정계측기수
                arrCode.Add(4);
                arrCode.Add(5);
                arrName.Add(4);
                arrName.Add(5);

                wCombo.mfSetComboEditor(this.uComboMeasureCount, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "2", arrCode, arrName);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProjectCode", "프로젝트코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProjectName", "프로젝트명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MeasureToolName", "계측기명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "InspectItemDesc", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--계측기(측정자)명
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.Yes, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--계측기(측정자)명
                grd.mfSetGridColumn(this.uGrid2, 0, "ItemSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGrid2, 0, "MachineName", "측정기기명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox4, GroupBoxType.INFO, "계획정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "측정항목정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "R&R 설정정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "검사부품정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProjectName = this.uTextSearchProjectName.Text;
                string strInspectDateFrom = Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value).ToString("yyyy-MM-dd");
                string strInspectDateTo = Convert.ToDateTime(this.uDateSearchInspectDateTo.Value).ToString("yyyy-MM-dd");

                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsHeader);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsHeader.mfReadQATMSAH(strPlantCode, strProjectName, strInspectDateFrom, strInspectDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dtSearch;
                this.uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else
                {
                    // 필수입력사항 체크
                    if (CheckRequire())
                    {
                        // 저장여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // 저장정보 데이터 테이블로 반환하는 메소드 호출
                            DataTable dtHeader = Rtn_HeaderDataTable();
                            DataTable dtDetail = Rtn_DetailDataTable();

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                            QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                            brwChannel.mfCredentials(clsHeader);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsHeader.mfSaveQATMSAH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uComboPlantCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uTextProjectCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000388", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 매개변수설정
                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        string strProjectCode = this.uTextProjectCode.Text;

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                        QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                        brwChannel.mfCredentials(clsHeader);

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsHeader.mfDeleteQATMSAH(strPlantCode, strProjectCode);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGrid1.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                    if (this.uGrid2.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGrid2);
                    }
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                ClearControls(ultraExpandableGroupBoxPanel1);
                ClearControls(this.uGroupBox1);
                ClearControls(this.uGroupBox2);
                ClearControls(this.uGroupBox3);
                ClearControls(this.uGroupBox4);
                ClearControls(this.ultraTabPageControl2);

                // 기본값 설정
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uComboRepeatCount.Value = "2";
                this.uComboMeasureCount.Value = "2";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 Clear Method
        /// </summary>
        /// <param name="c"></param>
        private void ClearControls(Control c)
        {
            try
            {
                foreach (Control Ctrl in c.Controls)
                {
                    if (Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraLabel" || Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraButton" ||
                        Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraGroupBox" || Ctrl.GetType().ToString() != "Infragistics.Win.UltraWinTabControl.UltraTabControl")
                    {
                        switch (Ctrl.GetType().ToString())
                        {
                            case "Infragistics.Win.UltraWinEditors.UltraTextEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraTextEditor)Ctrl).Clear();
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraComboEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraComboEditor)Ctrl).Value = "";
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)Ctrl).Value = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraNumericEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraNumericEditor)Ctrl).Value = 0.0;
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraCheckEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)Ctrl).Checked = false;
                                break;
                            //case "Infragistics.Win.UltraWinGrid.UltraGrid":
                            //    while (((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows.Count > 0)
                            //    {
                            //        ((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows[0].Delete(false);
                            //    }
                            //    break;
                            default:
                                //if (Ctrl.Controls.Count > 0)
                                //    ClearControls(Ctrl);
                                break;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 부품규격에 따라 범위 콤보박스 값 지정하는 Method
        /// </summary>
        private void SetSpecRange()
        {
            try
            {
                if (Convert.ToDecimal(this.uNumLowerSpec.Value) > 0.0m && Convert.ToDecimal(this.uNumUpperSpec.Value) == 0.0m)
                {
                    this.uComboSpecRange.Value = "L";
                }
                else if (Convert.ToDecimal(this.uNumLowerSpec.Value) == 0.0m && Convert.ToDecimal(this.uNumUpperSpec.Value) > 0.0m)
                {
                    this.uComboSpecRange.Value = "U";
                }
                else
                {
                    this.uComboSpecRange.Value = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// NumericEditor 왼쪽버튼(0) 클릭시 Value 값 0으로 만드는 Method
        /// </summary>
        /// <param name="sender"></param>
        private void SetNumericEditorZeroButton(object sender)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// NumericEditor SpinButton Method
        /// </summary>
        /// <param name="uNumEditor">NumericEditor 이름</param>
        /// <param name="e"></param>
        private void NumericEditorSpinButton(Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumEditor, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 필수사항 체크 Method
        /// </summary>
        /// <returns></returns>
        private bool CheckRequire()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlantCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return false;
                }
                else if (this.uTextProjectName.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001330", Infragistics.Win.HAlign.Center);

                    this.uTextProjectName.Focus();
                    return false;
                }
                else if (this.uComboProcessCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000293", Infragistics.Win.HAlign.Center);

                    this.uComboProcessCode.DropDown();
                    return false;
                }
                else if (this.uTextMeasureToolCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000248", Infragistics.Win.HAlign.Center);

                    this.uTextMeasureToolCode.Focus();
                    return false;
                }
                else if (this.uTextMaterialCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000975", Infragistics.Win.HAlign.Center);

                    this.uTextMaterialCode.Focus();
                    return false;
                }
                else if (this.uTextInspectItemDesc.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001272", Infragistics.Win.HAlign.Center);

                    this.uTextInspectItemDesc.Focus();
                    return false;
                }
                else if (this.uTextInspectUserID.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000197", Infragistics.Win.HAlign.Center);

                    this.uTextInspectUserID.Focus();
                    return false;
                }
                else if (this.uDateInspectDate.Value == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000185", Infragistics.Win.HAlign.Center);

                    this.uDateInspectDate.DropDown();
                    return false;
                }
                else if (this.uCheckAccuracyFlag.Checked == true && Convert.ToDecimal(this.uNumAccuracyValue.Value) == 0.0m)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001082", Infragistics.Win.HAlign.Center);

                    this.uNumAccuracyValue.Focus();
                    return false;
                }
                else if (this.uCheckRRValueChangeFlag.Checked == true && Convert.ToDecimal(this.uNumRRValueChangeValue.Value) == 0.0m)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000011", Infragistics.Win.HAlign.Center);

                    this.uNumRRValueChangeValue.Focus();
                    return false;
                }
                else
                {
                    if (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            if (this.uGrid2.Rows[i].Cells["MachineName"].Value.ToString() == "")
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000881",m_resSys.GetString("SYS_LANG"))
                                            , (i + 1).ToString() + msg.GetMessge_Text("M000551",m_resSys.GetString("SYS_LANG")), Infragistics.Win.HAlign.Center);

                                this.uGrid2.Rows[i].Cells["MachineName"].Activate();
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터 테이브롤 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsHeader);

                dtRtn = clsHeader.mfSetDataInfoRegist();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ProjectCode"] = this.uTextProjectCode.Text;
                drRow["ProjectName"] = this.uTextProjectName.Text;
                drRow["ProcessCode"] = this.uComboProcessCode.Value.ToString();
                drRow["MeasureToolCode"] = this.uTextMeasureToolCode.Text;
                drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                drRow["InspectItemDesc"] = this.uTextInspectItemDesc.Text;
                drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                drRow["InspectDesc"] = this.uTextInspectDesc.Text;
                drRow["StabilityFlag"] = this.uCheckStabilityFlag.CheckedValue;
                drRow["AccuracyFlag"] = this.uCheckAccuracyFlag.CheckedValue;
                drRow["AccuracyValue"] = Convert.ToDecimal(this.uNumAccuracyValue.Value);
                drRow["RRValueUseFlag"] = this.uCheckRRValueUseFlag.CheckedValue;
                drRow["RRValueChangeFlag"] = this.uCheckRRValueChangeFlag.CheckedValue;
                drRow["RRValueChangeValue"] = Convert.ToDecimal(this.uNumRRValueChangeValue.Value);
                if (this.uNumUpperSpec.Value != null)
                    drRow["UpperSpec"] = Convert.ToDecimal(this.uNumUpperSpec.Value);
                else
                    drRow["UpperSpec"] = 0.0m;
                if (this.uNumLowerSpec.Value != null)
                    drRow["LowerSpec"] = Convert.ToDecimal(this.uNumLowerSpec.Value);
                else
                    drRow["LowerSpec"] = 0.0m;
                drRow["SpecRange"] = this.uComboSpecRange.Value.ToString();
                drRow["UnitCode"] = this.uComboUnitCode.Value.ToString();
                drRow["RepeatCount"] = this.uComboRepeatCount.Value;
                drRow["PartCount"] = Convert.ToInt32(this.uNumPartCount.Value);
                drRow["MeasureCount"] = this.uComboMeasureCount.Value;
                drRow["RNRDataInputFlag"] = this.uCheckRNRDataInputFlag.CheckedValue;
                drRow["RNRAnalysisFlag"] = this.uCheckRNRAnalysisFlag.CheckedValue;

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 데이터테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_DetailDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAD), "MSAD");
                QRPQAT.BL.QATMSA.MSAD clsDetail = new QRPQAT.BL.QATMSA.MSAD();
                brwChannel.mfCredentials(clsDetail);

                dtRtn = clsDetail.mfSetDataInfoRegist();

                this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    DataRow drRow = dtRtn.NewRow();
                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                    drRow["ProjectCode"] = this.uTextProjectCode.Text;
                    drRow["ItemSeq"] = this.uGrid2.Rows[i].RowSelectorNumber;
                    drRow["MachineName"] = this.uGrid2.Rows[i].Cells["MachineName"].Value.ToString();
                    drRow["EtcDesc"] = this.uGrid2.Rows[i].Cells["EtcDesc"].Value.ToString();
                    dtRtn.Rows.Add(drRow);
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상세정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        private void Search_HeaderDetail(string strPlantCode, string strProjectCode)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtRtn = clsHeader.mfReadQATMSAHDetail(strPlantCode, strProjectCode, m_resSys.GetString("SYS_LANG"));

                this.uComboPlantCode.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                this.uTextProjectCode.Text = dtRtn.Rows[0]["ProjectCode"].ToString();
                this.uTextProjectName.Text = dtRtn.Rows[0]["ProjectName"].ToString();
                this.uComboProcessCode.Value = dtRtn.Rows[0]["ProcessCode"].ToString();
                this.uTextMeasureToolCode.Text = dtRtn.Rows[0]["MeasureToolCode"].ToString();
                this.uTextMeasureToolName.Text = dtRtn.Rows[0]["MeasureToolName"].ToString();
                this.uTextMaterialCode.Text = dtRtn.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtRtn.Rows[0]["MaterialName"].ToString();
                this.uTextInspectItemDesc.Text = dtRtn.Rows[0]["InspectItemDesc"].ToString();
                this.uDateInspectDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                this.uTextInspectUserID.Text = dtRtn.Rows[0]["InspectUserID"].ToString();
                this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();
                this.uTextInspectDesc.Text = dtRtn.Rows[0]["InspectDesc"].ToString();
                this.uCheckStabilityFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["StabilityFlag"]);
                this.uCheckAccuracyFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["AccuracyFlag"]);
                this.uNumAccuracyValue.Value = Convert.ToDecimal(dtRtn.Rows[0]["AccuracyValue"]);
                this.uCheckRRValueUseFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["RRValueUseFlag"]);
                this.uCheckRRValueChangeFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["RRValueChangeFlag"]);
                this.uNumRRValueChangeValue.Value = Convert.ToDecimal(dtRtn.Rows[0]["RRValueChangeValue"]);
                if (dtRtn.Rows[0]["UpperSpec"] != DBNull.Value)
                    this.uNumUpperSpec.Value = Convert.ToDecimal(dtRtn.Rows[0]["UpperSpec"]);
                else
                    this.uNumUpperSpec.Value = 0.0m;
                if (dtRtn.Rows[0]["LowerSpec"] != DBNull.Value)
                    this.uNumLowerSpec.Value = Convert.ToDecimal(dtRtn.Rows[0]["LowerSpec"]);
                else
                    dtRtn.Rows[0]["LowerSpec"] = 0.0m;
                this.uComboSpecRange.Value = dtRtn.Rows[0]["SpecRange"].ToString();
                this.uComboUnitCode.Value = dtRtn.Rows[0]["UnitCode"].ToString();
                this.uComboRepeatCount.Value = dtRtn.Rows[0]["RepeatCount"].ToString();
                this.uNumPartCount.Value = Convert.ToInt32(dtRtn.Rows[0]["PartCount"]);
                this.uComboMeasureCount.Value = dtRtn.Rows[0]["MeasureCount"].ToString();
                this.uCheckRNRDataInputFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["RNRDataInputFlag"]);
                this.uCheckRNRAnalysisFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["RNRAnalysisFlag"]);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        private void Search_Detail(string strPlantCode, string strProjectCode)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAD), "MSAD");
                QRPQAT.BL.QATMSA.MSAD clsDetail = new QRPQAT.BL.QATMSA.MSAD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtRtn = clsDetail.mfReadQATMSADRegist(strPlantCode, strProjectCode);

                this.uGrid2.DataSource = dtRtn;
                this.uGrid2.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 740;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행 자동삭제
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀수정시 RowSelector 이미지 변화
        private void uGrid2_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 순번
                e.Cell.Row.Cells["ItemSeq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 정확성 체크 이벤트
        private void uCheckAccuracyFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckAccuracyFlag.Checked == true)
                {
                    this.uNumAccuracyValue.Visible = true;
                }
                else
                {
                    this.uNumAccuracyValue.Visible = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // R&R 설정갑 사용 체크 이벤트
        private void uCheckRRValueUseFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (uCheckRRValueUseFlag.Checked == true)
                {
                    this.uCheckRRValueChangeFlag.Checked = false;
                    this.uNumRRValueChangeValue.Value = 0.0m;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // R&R 값 변경 체크 이벤트
        private void uCheckRRValueChangeFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckRRValueChangeFlag.Checked == true)
                {
                    this.uNumRRValueChangeValue.Visible = true;
                    this.uCheckRRValueUseFlag.Checked = false;
                }
                else
                {
                    this.uNumRRValueChangeValue.Visible = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보박스 값 변경 이벤트
        private void uComboPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboPlantCode.Value.ToString();
                DataTable dtProcess = new DataTable();

                this.uComboProcessCode.Items.Clear();

                if (strPlantCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboProcessCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 측정계측기수 콤보의 값만큼 그리드 행 지정하는 이벤트
        private void uComboMeasureCount_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtData = new DataTable();
                dtData.Columns.Add("ItemSeq", typeof(Int32));
                dtData.Columns.Add("MachineName", typeof(string));
                dtData.Columns.Add("EtcDesc", typeof(string));

                int intCount = Convert.ToInt32(this.uComboMeasureCount.Value);
                for (int i = 0; i < intCount; i++)
                {
                    DataRow drRow = dtData.NewRow();
                    dtData.Rows.Add(drRow);
                }

                this.uGrid2.DataSource = dtData;
                this.uGrid2.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 팝업창
        // 계측기 팝업창
        private void uTextMeasureToolCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0006 frmPOP = new frmPOP0006();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextMeasureToolCode.Text = frmPOP.MeasureToolCode;
                    this.uTextMeasureToolName.Text = frmPOP.MeasureToolName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000247", "M001331" + this.uComboPlantCode.Text + "M001269"
                                                , Infragistics.Win.HAlign.Right);
                    
                    this.uTextMeasureToolCode.Clear();
                    this.uTextMeasureToolName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자재 팝업창
        private void uTextMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextMaterialCode.Text = frmPOP.MaterialCode;
                    this.uTextMaterialName.Text = frmPOP.MaterialName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000188", "M001257" + this.uComboPlantCode.Text + "M001269"
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextMaterialCode.Clear();
                    this.uTextMaterialName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 팝업창
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextInspectUserID.Text = frmPOP.UserID;
                    this.uTextInspectUserName.Text = frmPOP.UserName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000188", "M001254" + this.uComboPlantCode.Text + "M000001"
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextInspectUserID.Clear();
                    this.uTextInspectUserName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 키다운 이벤트
        // 자재텍스트박스 키다운 이벤트
        private void uTextMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboPlantCode.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlantCode.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlantCode.Value.ToString();
                            String strMaterialCode = this.uTextMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextMaterialCode.Text = "";
                                this.uTextMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextMaterialCode.TextLength <= 1 || this.uTextMaterialCode.SelectedText == this.uTextMaterialCode.Text)
                    {
                        this.uTextMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 텍스트박스 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextInspectUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID.Text = "";
                            this.uTextInspectUserName.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 규격하한 입력시 범위 정하는 이벤트
        private void uNumUpperSpec_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                SetSpecRange();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 규격상한 입력시 범위 정하는 이벤트
        private void uNumLowerSpec_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                SetSpecRange();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Numeric 에디터 버튼 이벤트
        // 정확성 Numeric에디터 0버튼 클릭 이벤트
        private void uNumAccuracyValue_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 정확성 Numeric에디터 스핀버튼 이벤트
        private void uNumAccuracyValue_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumAccuracyValue, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // R&R 값 변경 Numeric에디터 0버튼 클릭 이벤트
        private void uNumRRValueChangeValue_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // R&R 값 변경 Numeric에디터 스핀버튼 클릭 이벤트
        private void uNumRRValueChangeValue_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumRRValueChangeValue, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 규격상한Numeric에디터 0버튼 클릭 이벤트
        private void uNumUpperSpec_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 규격상한Numeric에디터 스핀버튼 클릭 이벤트
        private void uNumUpperSpec_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumUpperSpec, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 규격하한 Numeric에디터 0버튼 클릭 이벤트
        private void uNumLowerSpec_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 규격하한 Numeric에디터 스핀버튼 클릭 이벤트
        private void uNumLowerSpec_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumLowerSpec, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 측정부품수 Numeric에디터 0버튼 클릭 이벤트
        private void uNumPartCount_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 측정부품수 Numeric에디터 스핀버튼 클릭 이벤트
        private void uNumPartCount_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumPartCount, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 헤더 그리드 더블클릭시 상세정보 조회 이벤트
        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 클릭된 행 고정
                e.Row.Fixed = true;

                // PK 수정불가
                this.uComboPlantCode.Enabled = false;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strProjectCode = e.Row.Cells["ProjectCode"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 메소드 호출
                Search_HeaderDetail(strPlantCode, strProjectCode);
                Search_Detail(strPlantCode, strProjectCode);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void frmQAT0020_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
