﻿namespace QRPQAT.UI
{
    partial class frmQAT0002_P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.uGroupBoxEmail = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uComboDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonRowDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridMailList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonSave = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCancel = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEmail)).BeginInit();
            this.uGroupBoxEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMailList)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxEmail
            // 
            this.uGroupBoxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxEmail.Controls.Add(this.uButtonOK);
            this.uGroupBoxEmail.Controls.Add(this.uComboDept);
            this.uGroupBoxEmail.Controls.Add(this.uButtonRowDelete);
            this.uGroupBoxEmail.Controls.Add(this.uGridMailList);
            this.uGroupBoxEmail.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxEmail.Name = "uGroupBoxEmail";
            this.uGroupBoxEmail.Size = new System.Drawing.Size(426, 220);
            this.uGroupBoxEmail.TabIndex = 0;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(322, 28);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(79, 28);
            this.uButtonOK.TabIndex = 203;
            this.uButtonOK.Text = "확인";
            // 
            // uComboDept
            // 
            this.uComboDept.Location = new System.Drawing.Point(209, 28);
            this.uComboDept.Name = "uComboDept";
            this.uComboDept.Size = new System.Drawing.Size(107, 21);
            this.uComboDept.TabIndex = 202;
            // 
            // uButtonRowDelete
            // 
            this.uButtonRowDelete.Location = new System.Drawing.Point(10, 28);
            this.uButtonRowDelete.Name = "uButtonRowDelete";
            this.uButtonRowDelete.Size = new System.Drawing.Size(75, 28);
            this.uButtonRowDelete.TabIndex = 2;
            this.uButtonRowDelete.Click += new System.EventHandler(this.uButtonRowDelete_Click);
            // 
            // uGridMailList
            // 
            this.uGridMailList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMailList.DisplayLayout.Appearance = appearance1;
            this.uGridMailList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMailList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMailList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMailList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridMailList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMailList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridMailList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMailList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMailList.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMailList.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridMailList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMailList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMailList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMailList.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridMailList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMailList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMailList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridMailList.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridMailList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMailList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridMailList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridMailList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMailList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridMailList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMailList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMailList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMailList.Location = new System.Drawing.Point(10, 36);
            this.uGridMailList.Name = "uGridMailList";
            this.uGridMailList.Size = new System.Drawing.Size(405, 172);
            this.uGridMailList.TabIndex = 0;
            this.uGridMailList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_AfterCellUpdate);
            this.uGridMailList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEmail_KeyDown);
            this.uGridMailList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_ClickCellButton);
            // 
            // uButtonSave
            // 
            this.uButtonSave.Location = new System.Drawing.Point(264, 228);
            this.uButtonSave.Name = "uButtonSave";
            this.uButtonSave.Size = new System.Drawing.Size(75, 28);
            this.uButtonSave.TabIndex = 1;
            this.uButtonSave.Click += new System.EventHandler(this.uButtonSave_Click);
            // 
            // uButtonCancel
            // 
            this.uButtonCancel.Location = new System.Drawing.Point(343, 228);
            this.uButtonCancel.Name = "uButtonCancel";
            this.uButtonCancel.Size = new System.Drawing.Size(75, 28);
            this.uButtonCancel.TabIndex = 2;
            this.uButtonCancel.Click += new System.EventHandler(this.uButtonCancel_Click);
            // 
            // frmQAT0002_P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(423, 272);
            this.Controls.Add(this.uButtonCancel);
            this.Controls.Add(this.uButtonSave);
            this.Controls.Add(this.uGroupBoxEmail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQAT0002_P1";
            this.Load += new System.EventHandler(this.frmQAT0002_P1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEmail)).EndInit();
            this.uGroupBoxEmail.ResumeLayout(false);
            this.uGroupBoxEmail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMailList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxEmail;
        private Infragistics.Win.Misc.UltraButton uButtonSave;
        private Infragistics.Win.Misc.UltraButton uButtonCancel;
        private Infragistics.Win.Misc.UltraButton uButtonRowDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMailList;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDept;
    }
}