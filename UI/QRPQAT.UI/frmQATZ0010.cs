﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : Qual 관리                                             */
/* 프로그램ID   : frmQATZ0010.cs                                        */
/* 프로그램명   : Machine Qaul 관리                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPQAT.UI
{
    public partial class frmQATZ0010 : Form, IToolbar
    {
        // Resource 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0010()
        {
            InitializeComponent();
        }

        private void frmQATZ0010_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0010_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("Machine Model 승인정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();            
            InitComboBox();
            InitGrid();

            this.uLabelSearchPlant.Visible = false;
            this.uComboSearchPlant.Visible = false;
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipType, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
       
        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboSearchEquipGroup.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                this.uComboSearchEquipType.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridAdmit, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                //wGrid.mfSetGridColumn(this.uGridAdmit, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "CustomerCode", "고객", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "Stack", "차수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "MACHINEGROUPACTIONTYPE", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "EquipTypeName", "설비유형명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "EquipGroupCode", "설비그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "MODEL", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "MAKER", "MAKER", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "AdmitFlag", "승인여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "Y");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "Reason", "审批原因", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 200
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "QualNo", "Qual바로가기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "SaveFlag", "저장여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "N");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "MDMTFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridAdmit, 0, "WriteDate", "작성일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime, "", "", DateTime.Now.ToString("yyyy-MM-dd"));



                // Set FontSize
                this.uGridAdmit.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridAdmit.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // 공장
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                // 고객
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "CustomerCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCustomer);

                // Package
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "Package", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPackage);

                // 설비유형
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                brwChannel.mfCredentials(clsEquipType);
                DataTable dtEquipType = clsEquipType.mfReadEquipTypeCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                // 일부 설비유형 - > 전체 설비유형으로 2012-11-22
                ////for (int i = 0; i < dtEquipType.Rows.Count; i++)
                ////{
                ////    if (!(dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BG") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SW") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SP") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("DA") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("WB") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("MD") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BA") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("ST") ||
                ////        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("GS")))
                ////    {
                ////        dtEquipType.Rows.Remove(dtEquipType.Rows[i]);
                ////        i--;
                ////    }
                ////}
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "MACHINEGROUPACTIONTYPE", Infragistics.Win.ValueListDisplayStyle.DataValue, "", "선택", dtEquipType);

                // 설비그룹
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                //brwChannel.mfCredentials(clsEquipGroup);
                //DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Info(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "EquipGroupCode", Infragistics.Win.ValueListDisplayStyle.DataValue, "", "선택", dtEquipGroup);

                DataTable dtEquipGroup = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "EquipGroupCode", Infragistics.Win.ValueListDisplayStyle.DataValue, "", "선택", dtEquipGroup);

                //승인여부 그리드 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtAdmitFlag = clsCom.mfReadCommonCode("C0077", m_resSys.GetString("SYS_LANG"));
                clsCom.Dispose();
                //DataTable dtAdmitFlag = new DataTable();
                //dtAdmitFlag.Columns.Add("Code", typeof(string));
                //dtAdmitFlag.Columns.Add("Name", typeof(string));
                //DataRow drRow = dtAdmitFlag.NewRow();
                //drRow["Code"] = "Y";
                //drRow["Name"] = "승인";
                //dtAdmitFlag.Rows.Add(drRow);

                //DataRow drRow1 = dtAdmitFlag.NewRow();
                //drRow1["Code"] = "N";
                //drRow1["Name"] = "미승인";
                //dtAdmitFlag.Rows.Add(drRow1);

                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "AdmitFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtAdmitFlag);

                // ReadOnly Cell Appearance 설정
                this.uGridAdmit.DisplayLayout.Bands[0].Override.ReadOnlyCellAppearance.BackColor = Color.Gainsboro;

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridAdmit, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        // 조회
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                WinGrid wGrid = new WinGrid();

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomerCode = this.uTextSearchCustomerCode.Text.Trim();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strStack = "";

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.MachineModelAdmit), "MachineModelAdmit");
                QRPQAT.BL.QATQUL.MachineModelAdmit clsAdmit = new QRPQAT.BL.QATQUL.MachineModelAdmit();
                brwChannel.mfCredentials(clsAdmit);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 메소드 호출
                DataTable dtSearch = clsAdmit.mfReadQATMahcineAdmitList(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strEquipTypeCode, strStack, m_resSys.GetString("SYS_LANG"));

                // 설비그룹 DropDown 설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);
                DataTable dtEquipGroup = clsEquipGroup.mfReadEquipTypeGroupCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "EquipGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "선택", "선택", dtEquipGroup);

                this.uGridAdmit.DataSource = dtSearch;
                this.uGridAdmit.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    wGrid.mfSetAutoResizeColWidth(this.uGridAdmit, 0);

                    for (int i = 0; i < this.uGridAdmit.Rows.Count; i++)
                    {
                        this.uGridAdmit.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["CustomerCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["Package"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["Stack"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["WriteDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridAdmit.Rows[i].Cells["Reason"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        // 전송 안된 부분은 붉은색으로 표시
                        if (this.uGridAdmit.Rows[i].Cells["MDMTFlag"].Value.ToString().Equals("F"))
                        {
                            this.uGridAdmit.Rows[i].CellAppearance.BackColor = Color.Salmon;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.MachineModelAdmit), "MachineModelAdmit");
                QRPQAT.BL.QATQUL.MachineModelAdmit clsMMAdmit = new QRPQAT.BL.QATQUL.MachineModelAdmit();
                brwChannel.mfCredentials(clsMMAdmit);

                DataTable dtMMAdmit = clsMMAdmit.mfSetDataInfo();
                DataRow drRow;
                string strLang = m_resSys.GetString("SYS_LANG");
                #region 필수입력사항 체크 && 저장데이터 설정

                // 필수입력사항 확인
                for (int i = 0; i < this.uGridAdmit.Rows.Count; i++)
                {
                    if (this.uGridAdmit.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001014",strLang)
                                                , (i + 1).ToString() + msg.GetMessge_Text("M000543",strLang)
                                                , Infragistics.Win.HAlign.Right);

                        this.uGridAdmit.Rows[i].Cells["PlantCode"].Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else if (this.uGridAdmit.Rows[i].Cells["CustomerCode"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001014",strLang)
                                                , (i + 1).ToString() + msg.GetMessge_Text("M000542",strLang)
                                                , Infragistics.Win.HAlign.Right);

                        this.uGridAdmit.Rows[i].Cells["CustomerCode"].Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else if (this.uGridAdmit.Rows[i].Cells["Package"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001014",strLang)
                                                , (i + 1).ToString() + msg.GetMessge_Text("M000541",strLang)
                                                , Infragistics.Win.HAlign.Right);

                        this.uGridAdmit.Rows[i].Cells["Package"].Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else if (this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Value.ToString().Equals(string.Empty) || this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Value.ToString().Equals("선택"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001014",strLang)
                                                , (i + 1).ToString() + msg.GetMessge_Text("M000547",strLang)
                                                , Infragistics.Win.HAlign.Right);

                        this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    
                    else if (this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString().Equals(string.Empty) || this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString().Equals("선택"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang)
                                                , msg.GetMessge_Text("M001014",strLang)
                                                , (i + 1).ToString() + msg.GetMessge_Text("M000546",strLang)
                                                , Infragistics.Win.HAlign.Right);

                        this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else
                    {
                        // 수정된 행만 저장
                        if (this.uGridAdmit.Rows[i].RowSelectorAppearance.Image != null || this.uGridAdmit.Rows[i].Cells["MDMTFlag"].Value.ToString().Equals("F"))
                        {
                            //PK 확인(이미 저장된 건은 다시 신규로 저장 못하게 막는 용도 / 사용 안함)
                            if (this.uGridAdmit.Rows[i].Cells["SaveFlag"].Value.ToString().Equals("N"))
                            {
                                string strPlantCode = this.uGridAdmit.Rows[i].Cells["PlantCode"].Value.ToString();
                                string strCustomerCode = this.uGridAdmit.Rows[i].Cells["CustomerCode"].Value.ToString();
                                string strPackage = this.uGridAdmit.Rows[i].Cells["Package"].Value.ToString();
                                string strEquipTypeCode = this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Value.ToString();
                                string strEquipGroupCode = this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString();
                                string strStack = this.uGridAdmit.Rows[i].Cells["Stack"].Value.ToString();

                                DataTable dtCheck = clsMMAdmit.mfReadQATMahcineAdmitList(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strEquipTypeCode, strStack,  m_resSys.GetString("SYS_LANG"));
                                if (dtCheck.Rows.Count > 0)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001053",strLang)
                                            , (i + 1).ToString() + msg.GetMessge_Text("M000463",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                    return;
                                }
                            }

                            drRow = dtMMAdmit.NewRow();
                            drRow["PlantCode"] = this.uGridAdmit.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["CustomerCode"] = this.uGridAdmit.Rows[i].Cells["CustomerCode"].Value.ToString();
                            drRow["Package"] = this.uGridAdmit.Rows[i].Cells["Package"].Value.ToString();
                            drRow["Stack"] = this.uGridAdmit.Rows[i].Cells["Stack"].Value.ToString();
                            drRow["MACHINEGROUPACTIONTYPE"] = this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Value.ToString();
                            drRow["EquipGroupCode"] = this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString();
                            drRow["AdmitFlag"] = this.uGridAdmit.Rows[i].Cells["AdmitFlag"].Value.ToString();
                            drRow["QualNo"] = this.uGridAdmit.Rows[i].Cells["QualNo"].Value.ToString();
                            drRow["WriteDate"] = this.uGridAdmit.Rows[i].Cells["WriteDate"].Value.ToString();
                            drRow["Reason"] = this.uGridAdmit.Rows[i].Cells["Reason"].Value.ToString();

                            //신규로 같은값을 저장하려 할때 막음(PK변경으로 삭제)
                            for (int j = 0; j < dtMMAdmit.Rows.Count; j++)
                            {
                                if (dtMMAdmit.Rows[j]["PlantCode"].ToString().Equals(drRow["PlantCode"].ToString())
                                        && dtMMAdmit.Rows[j]["CustomerCode"].ToString().Equals(drRow["CustomerCode"].ToString())
                                        && dtMMAdmit.Rows[j]["Package"].ToString().Equals(drRow["Package"].ToString())
                                        && dtMMAdmit.Rows[j]["Stack"].ToString().Equals(drRow["Stack"].ToString())
                                        && dtMMAdmit.Rows[j]["MACHINEGROUPACTIONTYPE"].ToString().Equals(drRow["MACHINEGROUPACTIONTYPE"].ToString())
                                        && dtMMAdmit.Rows[j]["EquipGroupCode"].ToString().Equals(drRow["EquipGroupCode"].ToString()))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , msg.GetMessge_Text("M001264",strLang)
                                                                , msg.GetMessge_Text("M001053",strLang)
                                                                , (i + 1).ToString() + msg.GetMessge_Text("M000821",strLang)
                                                                , Infragistics.Win.HAlign.Right);
                                    return;
                                }
                            }
                            
                            dtMMAdmit.Rows.Add(drRow);
                        }
                    }
                }

                #endregion

                if (!(dtMMAdmit.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001009"
                                                , "M001047"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 저장 메소드 호출
                    string strErrRtn = clsMMAdmit.mfSaveQATMahcineAdmitList(dtMMAdmit, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    // 팦업창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 결과 확인
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과에 따른 메세지 박스
                    if (ErrRtn.ErrNum == 0)
                    {
                        // 저장 완료휴 MDM 전송
                        for (int i = 0; i < dtMMAdmit.Rows.Count; i++)
                        {
                            string strPlantCode = dtMMAdmit.Rows[i]["PlantCode"].ToString();
                            string strCustomerCode = dtMMAdmit.Rows[i]["CustomerCode"].ToString();
                            string strPackage = dtMMAdmit.Rows[i]["Package"].ToString();
                            string strEquipGroupCode = dtMMAdmit.Rows[i]["EquipGroupCode"].ToString();
                            string strStack = dtMMAdmit.Rows[i]["Stack"].ToString();
                            string strAdmitFlag = dtMMAdmit.Rows[i]["AdmitFlag"].ToString();

                            clsMMAdmit.mfSaveQATMachineModelAdmit_MDM(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strAdmitFlag, strStack);
                            //mfSaveQATMachineModelAdmit_MDM(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strAdmitFlag,strStack);
                        }
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);

                         //리스트 갱신
                        mfSearch();
                        //리스트 갱신 대신 저장이 끝나면 수정불가
                        //for (int i = 0; i < this.uGridAdmit.Rows.Count; i++)
                        //{
                        //    this.uGridAdmit.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //    this.uGridAdmit.Rows[i].Cells["CustomerCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //    this.uGridAdmit.Rows[i].Cells["Package"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //    this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //    this.uGridAdmit.Rows[i].Cells["MACHINEGROUPACTIONTYPE"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //}
                    }
                    else
                    {
                        string strMes = "";

                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;


                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135",strLang)
                                            , msg.GetMessge_Text("M001037",strLang), strMes
                                            , Infragistics.Win.HAlign.Right);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 삭제
        public void mfDelete()
        {
            ////////////try
            ////////////{
            ////////////    // SystemInfo ResourceSet
            ////////////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////////////    DialogResult Result = new DialogResult();
            ////////////    WinMessageBox msg = new WinMessageBox();

            ////////////    this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

            ////////////    // BL 연결
            ////////////    QRPBrowser brwChannel = new QRPBrowser();
            ////////////    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.MachineModelAdmit), "MachineModelAdmit");
            ////////////    QRPQAT.BL.QATQUL.MachineModelAdmit clsMMAdmit = new QRPQAT.BL.QATQUL.MachineModelAdmit();
            ////////////    brwChannel.mfCredentials(clsMMAdmit);

            ////////////    DataTable dtMMAdmit = clsMMAdmit.mfSetDataInfo();
            ////////////    DataRow drRow;

            ////////////    #region 필수입력사항 체크 && 삭제데이터 설정

            ////////////    // 필수입력사항 확인
            ////////////    for (int i = 0; i < this.uGridAdmit.Rows.Count; i++)
            ////////////    {
            ////////////        if (Convert.ToBoolean(this.uGridAdmit.Rows[i].Cells["Check"].Value) && this.uGridAdmit.Rows[i].Cells["SaveFlag"].Value.ToString().Equals("Y"))
            ////////////        {
            ////////////            // 선택된 행만
            ////////////            if (this.uGridAdmit.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////////////                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////////////                                        , "확인창", "삭제 필수 입력사항 확인"
            ////////////                                        , (i + 1).ToString() + "번째 줄의 공장을 선택해 주세요."
            ////////////                                        , Infragistics.Win.HAlign.Center);

            ////////////                this.uGridAdmit.Rows[i].Cells["PlantCode"].Activate();
            ////////////                this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
            ////////////                return;
            ////////////            }
            ////////////            else if (this.uGridAdmit.Rows[i].Cells["CustomerCode"].Value.ToString().Equals(string.Empty))
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////////////                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////////////                                        , "확인창", "삭제 필수 입력사항 확인"
            ////////////                                        , (i + 1).ToString() + "번째 줄의 고객사를 선택해 주세요."
            ////////////                                        , Infragistics.Win.HAlign.Center);

            ////////////                this.uGridAdmit.Rows[i].Cells["CustomerCode"].Activate();
            ////////////                this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
            ////////////                return;
            ////////////            }
            ////////////            else if (this.uGridAdmit.Rows[i].Cells["Package"].Value.ToString().Equals(string.Empty))
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////////////                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////////////                                        , "확인창", "삭제 필수 입력사항 확인"
            ////////////                                        , (i + 1).ToString() + "번째 줄의 Package를 선택해 주세요."
            ////////////                                        , Infragistics.Win.HAlign.Center);

            ////////////                this.uGridAdmit.Rows[i].Cells["Package"].Activate();
            ////////////                this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
            ////////////                return;
            ////////////            }
            ////////////            else if (this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString().Equals(string.Empty))
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////////////                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////////////                                        , "확인창", "삭제 필수 입력사항 확인"
            ////////////                                        , (i + 1).ToString() + "번째 줄의 설비그룹을 선택해 주세요."
            ////////////                                        , Infragistics.Win.HAlign.Center);

            ////////////                this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Activate();
            ////////////                this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
            ////////////                return;
            ////////////            }
            ////////////            else
            ////////////            {
            ////////////                drRow = dtMMAdmit.NewRow();
            ////////////                drRow["PlantCode"] = this.uGridAdmit.Rows[i].Cells["PlantCode"].Value.ToString();
            ////////////                drRow["CustomerCode"] = this.uGridAdmit.Rows[i].Cells["CustomerCode"].Value.ToString();
            ////////////                drRow["Package"] = this.uGridAdmit.Rows[i].Cells["Package"].Value.ToString();
            ////////////                drRow["EquipGroupCode"] = this.uGridAdmit.Rows[i].Cells["EquipGroupCode"].Value.ToString();
            ////////////                dtMMAdmit.Rows.Add(drRow);

            ////////////            }
            ////////////        }
            ////////////    }

            ////////////    #endregion

            ////////////    if (!(dtMMAdmit.Rows.Count > 0))
            ////////////    {
            ////////////        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////////////                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////////////                                    , "확인창", "삭제 데이터 확인"
            ////////////                                    , "삭제할 정보가 없습니다.<br/>" +
            ////////////                                        "삭제할 정보를 선택해주세요"
            ////////////                                    , Infragistics.Win.HAlign.Center);

            ////////////        for (int i = 0; i < this.uGridAdmit.Rows.Count; i++)
            ////////////        {
            ////////////            if (this.uGridAdmit.Rows[i].Cells["Check"].Value.Equals(true))
            ////////////            {
            ////////////                if (this.uGridAdmit.Rows[i].Cells["SaveFlag"].Value.ToString().Equals("N"))
            ////////////                {
            ////////////                    this.uGridAdmit.Rows[i].Delete(false);
            ////////////                }
            ////////////            }
            ////////////        }
            ////////////            return;
            ////////////    }

            ////////////    // 삭제여부를 묻는다
            ////////////    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////////////                    "확인창", "삭제확인", "입력한 정보를 삭제하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
            ////////////    {
            ////////////        // ProgressBar 생성
            ////////////        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            ////////////        Thread threadPop = m_ProgressPopup.mfStartThread();
            ////////////        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
            ////////////        this.MdiParent.Cursor = Cursors.WaitCursor;

            ////////////        // 삭제메소드 호출
            ////////////        string strErrRtn = clsMMAdmit.mfDeleteQATMahcineAdmitList(dtMMAdmit);

            ////////////        // ProgressBar Close
            ////////////        this.MdiParent.Cursor = Cursors.Default;
            ////////////        m_ProgressPopup.mfCloseProgressPopup(this);

            ////////////        // 결과 검사
            ////////////        TransErrRtn ErrRtn = new TransErrRtn();
            ////////////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
            ////////////        if (ErrRtn.ErrNum == 0)
            ////////////        {
            ////////////            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////////////                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////////////                                        "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
            ////////////                                        Infragistics.Win.HAlign.Right);

            ////////////            // 리스트 갱신
            ////////////            mfSearch();
            ////////////        }
            ////////////        else
            ////////////        {
            ////////////            if (ErrRtn.ErrMessage.Equals(string.Empty))
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////////////                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////////////                                            "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
            ////////////                                            Infragistics.Win.HAlign.Right);
            ////////////            }
            ////////////            else
            ////////////            {
            ////////////                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////////////                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////////////                                            "처리결과", "삭제처리결과", ErrRtn.ErrMessage,
            ////////////                                            Infragistics.Win.HAlign.Center);
            ////////////            }
            ////////////        }
            ////////////    }
            ////////////}
            ////////////catch (Exception ex)
            ////////////{
            ////////////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////////////    frmErr.ShowDialog();
            ////////////}
            ////////////finally
            ////////////{
            ////////////}
        }

        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridAdmit.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridAdmit);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
       
        #region 1.검색조건 Event, Method

        // 고객사(검색조건)
        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객사코드 입력 후 엔터시 고객사명을 보여준다.
        private void uTextSearchCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //코드지우면 이름도 지움
                if (e.KeyData == Keys.Delete || e.KeyData == Keys.Back)
                {
                    this.uTextSearchCustomerCode.Text = "";
                    this.uTextSearchCustomerName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    string strCustomerCode = this.uTextSearchCustomerCode.Text;
                    if (!strCustomerCode.Equals(string.Empty))
                    {
                        //BL 호출
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                        QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                        brwChannel.mfCredentials(clsCustomer);

                        DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                        if (dtCustomer.Rows.Count > 0)
                        {
                            this.uTextSearchCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001115", "M000889", Infragistics.Win.HAlign.Right);
                            this.uTextSearchCustomerName.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장코드 변경시 Package, 설비그룹콤보 재설정
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboSearchPlant.Value == null)
                    return;
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor wCombo = new WinComboEditor();

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    // 기존 데이터 삭제
                    //this.uComboSearchEquipGroup.Items.Clear();
                    this.uComboSearchPackage.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();

                    // 검색조건 : Package 콤보박스
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsPackage);
                    DataTable dtPackage = clsPackage.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                        , "Package", "ComboName", dtPackage);

                    // 설비유형
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                    QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                    brwChannel.mfCredentials(clsEquipType);
                    DataTable dtEquipType = clsEquipType.mfReadEquipTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    // 설비유형은 BG, SW, SP, DA, WB, MD, BA, ST, GS만 나오도록 수정
                    // 1. for문이용후 삭제하는방법
                    //for (int i = 0; i < dtEquipType.Rows.Count; i++)
                    //{
                    //    if (!(dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BG") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SW") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SP") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("DA") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("WB") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("MD") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BA") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("ST") ||
                    //        dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("GS")))
                    //    {
                    //        dtEquipType.Rows.Remove(dtEquipType.Rows[i]);
                    //        i--;
                    //    }
                    //}                    
                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                        , "EquipTypeCode", "EquipTypeName", dtEquipType);

                    ////// 2. Select문 사용하는 방법
                    ////string strFilter = "EquipTypeCode = 'BG' OR EquipTypeCode = 'SW' OR EquipTypeCode = 'SP' OR EquipTypeCode = 'DA' OR" +
                    ////        "EquipTypeCode = 'WB' OR EquipTypeCode = 'MD' OR EquipTypeCode = 'BA' OR EquipTypeCode = 'ST' OR" +
                    ////        "EquipTypeCode = 'GS'";
                    ////DataTable dtEquipTypeBind = dtEquipType.Clone();
                    ////DataRow[] _dr = dtEquipType.Select(strFilter);
                    ////for (int i = 0; i < _dr.Length; i++)
                    ////{
                    ////    dtEquipTypeBind.ImportRow(_dr[i]);
                    ////}
                    ////wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    ////    , "EquipTypeCode", "EquipTypeName", dtEquipTypeBind);

                    ////// 검색조건 : 설비그룹
                    ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    ////QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    ////brwChannel.mfCredentials(clsEquipGroup);
                    ////DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Info(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    ////wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    ////    , "EquipGroupCode", "EquipGroupName", dtEquipGroup);                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        // 그리드 업데이트시 이벤트
        private void uGridAdmit_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                ////if(wGrid.mfCheckCellDataInRow(this.uGridAdmit, 0, e.Cell.Row.Index))
                ////    e.Cell.Row.Delete(false);

                if(e.Cell.Column.Key.Equals("PlantCode"))       // 공장코드 변경시 설비그룹, Package DropDown 재설정
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strPlantCode = e.Cell.Value.ToString();

                    // Package 콤보박스
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsPackage);
                    DataTable dtPackage = clsPackage.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridCellValueList(this.uGridAdmit, e.Cell.Row.Index, "Package", "", "선택", dtPackage);

                    // 설비유형
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                    QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                    brwChannel.mfCredentials(clsEquipType);
                    DataTable dtEquipType = clsEquipType.mfReadEquipTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    // 설비유형은 BG, SW, SP, DA, WB, MD, BA, ST, GS만 나오도록 수정
                    // 1. for문이용후 삭제하는방법
                    for (int i = 0; i < dtEquipType.Rows.Count; i++)
                    {
                        if (!(dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BG") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SW") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("SP") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("DA") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("WB") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("MD") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("BA") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("ST") ||
                            dtEquipType.Rows[i]["EquipTypeCode"].ToString().Equals("GS")))
                        {
                            dtEquipType.Rows.Remove(dtEquipType.Rows[i]);
                            i--;
                        }
                    }
                    wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "MACHINEGROUPACTIONTYPE", Infragistics.Win.ValueListDisplayStyle.DataValue, "선택", "선택", dtEquipType);

                    //// 설비그룹
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    //brwChannel.mfCredentials(clsEquipGroup);
                    //DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //wGrid.mfSetGridCellValueList(this.uGridAdmit, e.Cell.Row.Index, "EquipGroupCode", "", "선택", dtEquipGroup);

                    DataTable dtEquipGroup = new DataTable();
                    wGrid.mfSetGridCellValueList(this.uGridAdmit, e.Cell.Row.Index, "EquipGroupCode", "", "선택", dtEquipGroup);
                }
            
                else if (e.Cell.Column.Key.Equals("MACHINEGROUPACTIONTYPE")) // 설비유형 변경시 선택한 설비유형에 해당하는 설비그룹 정보만 보여준다.
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipTypeCode = e.Cell.Row.Cells["MACHINEGROUPACTIONTYPE"].Value.ToString();

                    QRPBrowser brwChannel = new QRPBrowser();

                    ////// 설비유형
                    ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                    ////QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                    ////brwChannel.mfCredentials(clsEquipType);
                    ////DataTable dtEquipType = clsEquipType.mfReadEquipTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    ////wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "MACHINEGROUPACTIONTYPE", Infragistics.Win.ValueListDisplayStyle.DataValue, "선택", "선택", dtEquipType);

                    // 설비그룹                    
                    e.Cell.Row.Cells["EquipGroupCode"].Value = "";//"선택";
                    e.Cell.Row.Cells["EquipGroupName"].Value = "";
                    e.Cell.Row.Cells["MODEL"].Value = "";

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChannel.mfCredentials(clsEquipGroup);
                    DataTable dtEquipGroup = clsEquipGroup.mfReadEquipTypeGroupCombo(strPlantCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));                                        
                    //wGrid.mfSetGridColumnValueList(this.uGridAdmit, 0, "EquipGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "선택", "선택", dtEquipGroup);
                    wGrid.mfSetGridCellValueList(this.uGridAdmit, e.Cell.Row.Index, "EquipGroupCode", "", "선택", dtEquipGroup);
                    if (dtEquipGroup.Rows.Count == 0)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001098", "M000668", Infragistics.Win.HAlign.Right);

                        e.Cell.Activate();
                        this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
                else if (e.Cell.Column.Key.Equals("EquipGroupCode"))        // 설비그룹 변경시 설비유형, MODEL 명 설정
                {
                    if (e.Cell.Value.ToString() != "" || !e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                        QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                        brwChannel.mfCredentials(clsEquipGroup);
                        DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroup_Info(e.Cell.Row.Cells["PlantCode"].Value.ToString()
                                                                                , e.Cell.Value.ToString()
                                                                                , m_resSys.GetString("SYS_LANG"));

                        if (dtEquipGroup.Rows.Count > 0)
                        {
                            e.Cell.Row.Cells["EquipGroupName"].Value = dtEquipGroup.Rows[0]["EquipGroupName"].ToString();
                            //e.Cell.Row.Cells["MACHINEGROUPACTIONTYPE"].Value = dtEquipGroup.Rows[0]["MACHINEGROUPACTIONTYPE"].ToString();
                            e.Cell.Row.Cells["MODEL"].Value = dtEquipGroup.Rows[0]["MODEL"].ToString();
                            e.Cell.Row.Cells["MAKER"].Value = dtEquipGroup.Rows[0]["MAKER"].ToString();
                        }
                    }
                    else
                    {
                        e.Cell.Row.Cells["EquipGroupName"].Value = "";
                        e.Cell.Row.Cells["MODEL"].Value = "";
                        e.Cell.Row.Cells["MAKER"].Value = "";
                    }
                }
                else if (e.Cell.Column.Key.Equals("Package"))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strPackage = e.Cell.Text.ToString();

                    // Package 콤보박스
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsPackage);


                    DataTable dtPackage = clsPackage.mfReadMASProductWithPackageCombo(strPlantCode, strPackage, m_resSys.GetString("SYS_LANG"));

                    if (dtPackage.Rows.Count == 0)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000962", "M000099", Infragistics.Win.HAlign.Right);

                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 수정시 RowSelector 이미지 변경
        private void uGridAdmit_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 Dropdown 리스트 닫힐때 이벤트
        private void uGridAdmit_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                WinMessageBox msg = new WinMessageBox();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.MachineModelAdmit), "MachineModelAdmit");
                QRPQAT.BL.QATQUL.MachineModelAdmit clsAdmit = new QRPQAT.BL.QATQUL.MachineModelAdmit();
                brwChannel.mfCredentials(clsAdmit);

                if (e.Cell.Column.Key.Equals("EquipGroupCode") && e.Cell.Value.ToString().Equals(String.Empty))
                {
                    this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strCustomerCode = e.Cell.Row.Cells["CustomerCode"].Value.ToString();
                    string strPackage = e.Cell.Row.Cells["Package"].Value.ToString();
                    string strEquipGroupCode = e.Cell.Row.Cells["EquipGroupCode"].Value.ToString();
                    string strEquipTypeCode = e.Cell.Row.Cells["MACHINEGROUPACTIONTYPE"].Value.ToString();
                    string strStack = "-";
                    if (e.Cell.Row.Cells["Stack"].Value.ToString().Equals(string.Empty))
                    {
                        strStack = string.Empty;
                        e.Cell.Row.Cells["Stack"].Value = "-";
                    }
                    else
                    {
                        strStack = e.Cell.Row.Cells["Stack"].Value.ToString();
                    }
                    DataTable dtAdmit = clsAdmit.mfReadQATMahcineAdmitList(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strEquipTypeCode, strStack, m_resSys.GetString("SYS_LANG"));
                    if (dtAdmit.Rows.Count > 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000962", "M000898", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
                if (e.Cell.Column.Key.Equals("MACHINEGROUPACTIONTYPE"))
                {
                    this.uGridAdmit.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                }
                if (e.Cell.Column.Key.Equals("Package"))
                {

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strPackage = e.Cell.Text.ToString();

                    // Package 콤보박스
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsPackage);
                    

                    DataTable dtPackage = clsPackage.mfReadMASProductWithPackageCombo(strPlantCode, strPackage , m_resSys.GetString("SYS_LANG"));

                    if (dtPackage.Rows.Count == 0) 
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000962", "M000099", Infragistics.Win.HAlign.Right);

                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시 QualNo가 있으면 신제품 QualNo 화면으로 이동
        private void uGridAdmit_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (e.Row == null)
                    return;

                if (!string.IsNullOrEmpty(e.Row.Cells["QualNo"].Value.ToString()) && e.Row.Cells["QualNo"].Value != DBNull.Value)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                    string strQualNo = e.Row.Cells["QualNo"].Value.ToString();

                    // Connect BL
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                    QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                    brwChannel.mfCredentials(clsHeader);

                    DataTable dtHeader = clsHeader.mfReadQATNewQualH_Detail(strPlantCode, strQualNo, m_resSys.GetString("SYS_LANG"));

                    if (dtHeader.Rows.Count > 0)
                    {
                        frmQATZ0001 frmQAT = new frmQATZ0001();

                        // 폼에 전달할 값 설정
                        frmQAT.PlantCode = strPlantCode;
                        frmQAT.QualNo = strQualNo;
                        frmQAT.MoveFormName = this.Name;

                        CommonControl cControl = new CommonControl();
                        Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //해당화면의 탭이 있는경우 해당 탭 닫기
                        if (uTabMenu.Tabs.Exists("QRPQAT" + "," + "frmQATZ0001"))
                        {
                            uTabMenu.Tabs["QRPQAT" + "," + "frmQATZ0001"].Close();
                        }
                        //탭에 추가
                        uTabMenu.Tabs.Add("QRPQAT" + "," + "frmQATZ0001", "신제품 Qual 관리");
                        uTabMenu.SelectedTab = uTabMenu.Tabs["QRPQAT,frmQATZ0001"];

                        //호출시 화면 속성 설정
                        frmQAT.AutoScroll = true;
                        frmQAT.MdiParent = this.MdiParent;
                        frmQAT.ControlBox = false;
                        frmQAT.Dock = DockStyle.Fill;
                        frmQAT.FormBorderStyle = FormBorderStyle.None;
                        frmQAT.WindowState = FormWindowState.Normal;
                        frmQAT.Text = "신제품 Qual 관리";
                        frmQAT.Show();
                    }
                    else
                    {
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000110",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000838",m_resSys.GetString("SYS_LANG")) +
                                                    msg.GetMessge_Text("M000111",m_resSys.GetString("SYS_LANG"))
                                                , Infragistics.Win.HAlign.Center);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboSearchEquipType.Value == null)
                    return;
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor wCombo = new WinComboEditor();

                    // 기존항목 삭제
                    this.uComboSearchEquipGroup.Items.Clear();

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();

                    // 검색조건 : 설비그룹
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChannel.mfCredentials(clsEquipGroup);
                    DataTable dtEquipGroup = clsEquipGroup.mfReadEquipTypeGroupCombo(strPlantCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));
                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                        , "EquipGroupCode", "EquipGroupName", dtEquipGroup);    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
